<?php
namespace Admin;

use Base\Module\BaseModule;
use Admin\Container\Container;

class Module extends BaseModule {

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig() {
        return parent::composeServiceConfig(new Container());
    }

}
