<?php
namespace Admin\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;

/**
 * Description of CemiterioRepository
 *
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class CemiterioRepository extends BaseRepository implements RepositoryInterface {

    /**
     * 
     * @param array $filtros
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function busca($filtros, $limit, $offset, $order = array(), $total = false) {
        try {
            //Monta a query dql buscando os dados ou buscando o total de acordo com os filtros
            if ($total) {
                $dql = "SELECT count(c) total FROM Admin\Entity\Cemiterio c JOIN c.tipo t WHERE c.excluido = 0 ";
            } else {
                $dql = "SELECT c FROM Admin\Entity\Cemiterio c JOIN c.tipo t WHERE c.excluido = 0 ";
            }
            $parametros = array();

            //Aplica os filtros
            if (!empty($filtros['busca'])) {
                $dql .= " AND (LOWER(c.nome) LIKE :busca OR LOWER(t.nome) LIKE :busca) ";
                $parametros['busca'] = '%' . $filtros['busca'] . '%';
            }
            
            if ($filtros['status'] !== "") {
                $dql .= " AND c.ativo = :status ";
                $parametros['status'] = $filtros['status'];
            }

            if (count($order)) {
                $dql .= " ORDER BY {$order['field']} {$order['order']} ";
            }

            //Retorna os dados para a busca ou o total encontrado de acordo com os filtros
            if ($total) {
                $result = $this->getEntityManager()->createQuery($dql)->setParameters($parametros)->getResult();
                return $result[0]['total'];
                
            } else {
                
                //Cria e executa query
                $query = $this->getEntityManager()->createQuery($dql);
                $query->setMaxResults($limit);
                $query->setFirstResult($offset);
                $query->setParameters($parametros);
                return $query->getResult();
            }
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }
    public function buscaArrayInputFilter($filtro) {
         try {
            
             $dql = "SELECT txt_nome
                       FROM tb_cemiterio
                      WHERE id_tipo=".$filtro;

            
            $connection = $this->getEntityManager()->getConnection();

            if ($connection) {
            
            $stmt = $connection->query($dql);
                
            $res ="";
            foreach ($stmt->fetchAll() as $var) {
                
            $res.="'".$var['txt_nome']."',";
                
            }
            $res = substr($res,0,-1);
            
                return "[".$res."]";
            }

        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }
    
    public function buscarCemiterio($filtros){
        try {
            //Monta a query dql buscando os dados ou buscando o total de acordo com os filtros
           
                $dql = "SELECT c FROM Admin\Entity\Cemiterio c JOIN c.tipo t WHERE c.excluido = 0 AND c.tipo = 1";
           
            $parametros = array();

            //Aplica os filtros
            if (!empty($filtros['busca'])) {
                $dql .= " AND (LOWER(c.nome) = :busca)";
                $parametros['busca'] = $filtros['busca'];
            }
            
                //Cria e executa query
                $query = $this->getEntityManager()->createQuery($dql);
                $query->setParameters($parametros);
                return $query->getResult();
          
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }
    public function buscarCrematorio($filtros){
        try {
            //Monta a query dql buscando os dados ou buscando o total de acordo com os filtros
           
                $dql = "SELECT c FROM Admin\Entity\Cemiterio c JOIN c.tipo t WHERE c.excluido = 0 AND c.tipo = 2";
           
            $parametros = array();

            //Aplica os filtros
            if (!empty($filtros['busca'])) {
                $dql .= " AND (LOWER(c.nome) = :busca)";
                $parametros['busca'] = $filtros['busca'];
            }
            
                //Cria e executa query
                $query = $this->getEntityManager()->createQuery($dql);
                $query->setParameters($parametros);
                return $query->getResult();
          
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }
}
