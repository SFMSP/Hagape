<?php
namespace Admin\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;
use Admin\Entity\Estoque;

/**
 * Description of EstoqueRepository
 *
 * @autor Tais Azevedo <tais.azevedo@jointevnologia.com>
 */
class EstoqueRepository extends BaseRepository implements RepositoryInterface {

    /**
     * 
     * @param array $filtros
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function busca($filtros, $limit, $offset, $order = array(), $total = false) {
        try {
            //Monta a query dql buscando os dados ou buscando o total de acordo com os filtros
            if ($total) {
                $dql = "SELECT count(e) total FROM Admin\Entity\Estoque e WHERE e.excluido = 0 ";
            } else {
                $dql = "SELECT e FROM Admin\Entity\Estoque e WHERE e.excluido = 0 ";
            }
            $parametros = array();

            //Aplica os filtros
            if (!empty($filtros['busca'])) {
                $dql .= " AND (LOWER(e.nome) LIKE :busca) ";
                $parametros['busca'] = '%' . $filtros['busca'] . '%';
            }
            
            //Aplica os filtros
            if (!empty($filtros['tipo'])) {
                $dql .= " AND (LOWER(e.tipo) LIKE :tipo) ";
                $parametros['tipo'] = '%' . $filtros['tipo'] . '%';
            }

            if ($filtros['status'] !== "") {
                $dql .= " AND e.ativo = :status ";
                $parametros['status'] = $filtros['status'];
            }

            if (count($order)) {
                $dql .= " ORDER BY e.{$order['field']} {$order['order']} ";
            }

            //Retorna os dados para a busca ou o total encontrado de acordo com os filtros
            if ($total) {
                $result = $this->getEntityManager()->createQuery($dql)->setParameters($parametros)->getResult();
                return $result[0]['total'];
                
            } else {
                
                //Cria e executa query
                $query = $this->getEntityManager()->createQuery($dql);
                $query->setMaxResults($limit);
                $query->setFirstResult($offset);
                $query->setParameters($parametros);
                return $query->getResult();
            }
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }
    
    /**
     * Método que retorna o estoque central
     * 
     * @return type
     */
    public function getEstoqueCentral(){
        return $this->findOneBy(array('tipo' => Estoque::ESTOQUE_CENTRAL));
    }
    
    public function verificaEstoqueCentral($estoque){
        return $this->findOneBy(array('id' => $estoque, 'tipo' => Estoque::ESTOQUE_CENTRAL)) ? true : false;
    }

}
