<?php
namespace Admin\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;

/**
 * Description of AgenciaRepository
 *
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class AgenciaRepository extends BaseRepository implements RepositoryInterface {

    /**
     * 
     * @param array $filtros
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function busca($filtros, $limit = null, $offset = null, $order = array(), $total = false) {
        try {
            //Monta a query dql buscando os dados ou buscando o total de acordo com os filtros
            if ($total) {
                $dql = "SELECT count(a) total FROM Admin\Entity\Agencia a WHERE a.excluido = 0 ";
            } else {
                $dql = "SELECT a FROM Admin\Entity\Agencia a WHERE a.excluido = 0 ";
            }
            $parametros = array();

            //Aplica os filtros
            if (!empty($filtros['busca'])) {
                $dql .= " AND LOWER(a.nome) LIKE :busca ";
                $parametros['busca'] = '%' . $filtros['busca'] . '%';
            }

            if ($filtros['status'] !== "") {
                $dql .= " AND a.ativo = :status ";
                $parametros['status'] = $filtros['status'];
            }

            if (count($order)) {
                $dql .= " ORDER BY a.{$order['field']} {$order['order']} ";
            }

            //Retorna os dados para a busca ou o total encontrado de acordo com os filtros
            if ($total) {
                $result = $this->getEntityManager()->createQuery($dql)->setParameters($parametros)->getResult();
                return $result[0]['total'];
                
            } else {
                
                //Cria e executa query
                $query = $this->getEntityManager()->createQuery($dql);
                
                if($limit){
                    $query->setMaxResults($limit);
                }
                
                if($offset){
                    $query->setFirstResult($offset);
                }
                
                $query->setParameters($parametros);
                return $query->getResult();
            }
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }

}
