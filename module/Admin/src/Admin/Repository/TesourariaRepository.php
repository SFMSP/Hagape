<?php

namespace Admin\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;

/**
 * Description of TesourariaRepository
 *
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class TesourariaRepository extends BaseRepository implements RepositoryInterface {

    /**
     * 
     * @param array $filtros
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function busca($filtros, $limit, $offset, $order = array(), $total = false) {
        try {
            //Monta a query dql buscando os dados ou buscando o total de acordo com os filtros
            if ($total) {
                $dql = "SELECT count(t) total FROM Admin\Entity\Tesouraria t WHERE t.excluido = 0 ";
            } else {
                $dql = "SELECT t FROM Admin\Entity\Tesouraria t WHERE t.excluido = 0 ";
            }
            $parametros = array();

            //Aplica os filtros
            if (!empty($filtros['busca'])) {
                $dql .= " AND LOWER(t.nome) LIKE :busca ";
                $parametros['busca'] = '%' . $filtros['busca'] . '%';
            }

            if ($filtros['status'] !== "") {
                $dql .= " AND t.ativo = :status ";
                $parametros['status'] = $filtros['status'];
            }



            if (count($order)) {
                $dql .= " ORDER BY t.{$order['field']} {$order['order']} ";
            }

            //Retorna os dados para a busca ou o total encontrado de acordo com os filtros
            if ($total) {
                $result = $this->getEntityManager()->createQuery($dql)->setParameters($parametros)->getResult();
                return $result[0]['total'];
            } else {

                //Cria e executa query
                $query = $this->getEntityManager()->createQuery($dql);
                $query->setMaxResults($limit);
                $query->setFirstResult($offset);
                $query->setParameters($parametros);
                return $query->getResult();
            }
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }

}
