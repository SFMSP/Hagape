<?php

namespace Admin\Service;

use Application\Service\BaseApplicationService;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Classe que contém as operações realizadas na entidade de usuários
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Cemiterio extends BaseApplicationService {

    public function save(array $data) {

        $data['cidade'] = $this->getEm()->getReference('Application\Entity\Cidade', $data['cidade']);
        $data['tipo'] = $this->getEm()->getReference('Admin\Entity\Tipo', $data['tipo']);

        if (isset($data['tipoSepultamento'])) {
            $tiposSepultamento = new ArrayCollection();

            foreach ($data['tipoSepultamento'] as $tipo) {
                $tiposSepultamento->add($this->getEm()->getReference('Admin\Entity\TipoSepultamento', $tipo));
            }

            $data['tipoSepultamento'] = $tiposSepultamento;
        }else{
            $data['tipoSepultamento'] = new ArrayCollection();
        }
        
        return parent::save($data);
    }

}
