<?php

namespace Admin\Service;

use Application\Service\BaseApplicationService;

/**
 * Classe que contém as operações realizadas na entidade de usuários
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Trafego extends BaseApplicationService {

    public function save(array $data) {

        $data['cidade'] = $this->getEm()->getReference('Application\Entity\Cidade', $data['cidade']);

        return parent::save($data);
    }

}
