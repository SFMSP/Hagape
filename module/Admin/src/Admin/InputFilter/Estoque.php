<?php
namespace Admin\InputFilter;

use Base\Validator\BaseValidator;

/**
 * Classe que faz as validações do formulário de estoque
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Estoque extends BaseValidator {

    /**
     * Monta os filtros e as validações
     */
    public function __construct() {
        //Monta as validações de campos obrigatórios padrão
        $obrigatoriosPadrao = array(
            "nome" => "Campo obrigatório",
            "endereco" => "Campo obrigatório",
            "numero" => "Campo obrigatório",
            "bairro" => "Campo obrigatório",
            "cep" => "Campo obrigatório",
            "estado" => "Campo obrigatório",
            "cidade" => "Campo obrigatório",
            "primeiroTelefone" => "Campo obrigatório",
            "tipo" => "Campo obrigatório",
        );

        $this->addEmptyValidators($obrigatoriosPadrao);

        $this->add(array(
            'name' => 'email',
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'), array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'EmailAddress',
                    'options' => array(
                        'useDomainCheck' => false,
                        'message' => 'Endereço de e-mail inválido'
                    )),
            )
        ));
    }

}
