<?php

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Tesouraria
 *
 * @ORM\Table(name="tb_tesouraria")
 * @ORM\Entity(repositoryClass="Admin\Repository\TesourariaRepository")
 */
class Tesouraria extends BaseEntity implements EntityInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_tesouraria", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=true)
     */
    private $dataCadastro;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome", type="string", length=255, nullable=true)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_endereco", type="string", length=255, nullable=true)
     */
    private $endereco;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_primeiro_telefone", type="string", length=50, nullable=true)
     */
    private $primeiroTelefone;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_segundo_telefone", type="string", length=50, nullable=true)
     */
    private $segundoTelefone;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_observacao", type="text", length=65535, nullable=true)
     */
    private $observacao;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_ativo", type="boolean", nullable=true)
     */
    private $ativo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_excluido", type="boolean", nullable=true)
     */
    private $excluido;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Application\Entity\Usuario", mappedBy="id")
     * @ORM\JoinTable(name="tb_tesouraria_usuario",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_tesouraria", referencedColumnName="id_tesouraria")
     *   }
     * )
     */
    private $usuarios;

    public function __construct() {
        $this->dataCadastro = new \DateTime('now');
        $this->excluido = false;
    }

    function getId() {
        return $this->id;
    }

    function getDataCadastro() {
        return $this->dataCadastro;
    }

    function getNome() {
        return $this->nome;
    }

    function getEndereco() {
        return $this->endereco;
    }

    function getPrimeiroTelefone() {
        return $this->primeiroTelefone;
    }

    function getSegundoTelefone() {
        return $this->segundoTelefone;
    }

    function getEmail() {
        return $this->email;
    }

    function getObservacao() {
        return $this->observacao;
    }

    function getAtivo() {
        return $this->ativo;
    }

    function getExcluido() {
        return $this->excluido;
    }

    function setId($id) {
        $this->id = $id;
        return $this;
    }

    function setDataCadastro(\DateTime $dataCadastro) {
        $this->dataCadastro = $dataCadastro;
        return $this;
    }

    function setNome($nome) {
        $this->nome = $nome;
        return $this;
    }

    function setEndereco($endereco) {
        $this->endereco = $endereco;
        return $this;
    }

    function setPrimeiroTelefone($primeiroTelefone) {
        $this->primeiroTelefone = $primeiroTelefone;
        return $this;
    }

    function setSegundoTelefone($segundoTelefone) {
        $this->segundoTelefone = $segundoTelefone;
        return $this;
    }

    function setEmail($email) {
        $this->email = $email;
        return $this;
    }

    function setObservacao($observacao) {
        $this->observacao = $observacao;
        return $this;
    }

    function setAtivo($ativo) {
        $this->ativo = $ativo;
        return $this;
    }

    function setExcluido($excluido) {
        $this->excluido = $excluido;
        return $this;
    }

    public function __toString() {
        return $this->nome;
    }

    public function getLabel() {
        return $this->__toString();
    }

    public function toArray() {
        return array(
            "id" => $this->id,
            "dataCadastro" => $this->dataCadastro,
            "nome" => $this->nome,
            "endereco" => $this->endereco,
            "primeiroTelefone" => $this->primeiroTelefone,
            "segundoTelefone" => $this->segundoTelefone,
            "email" => $this->email,
            "observacao" => $this->observacao,
            "ativo" => $this->ativo,
            "excluido" => $this->excluido
        );
    }

}
