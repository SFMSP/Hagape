<?php

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;
use Application\Entity\Cidade;

/**
 * Cemiterio
 *
 * @ORM\Table(name="tb_cemiterio", indexes={@ORM\Index(name="FK_cemiterio_cidade", columns={"id_cidade"}), @ORM\Index(name="FK_cemitero_tipo", columns={"id_tipo"})})
 * @ORM\Entity(repositoryClass="Admin\Repository\CemiterioRepository")
 */
class Cemiterio extends BaseEntity implements EntityInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_cemiterio", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=false)
     */
    private $dataCadastro;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome", type="string", length=255, nullable=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_endereco", type="string", length=255, nullable=false)
     */
    private $endereco;

    /**
     * @var integer
     *
     * @ORM\Column(name="txt_numero", type="string", nullable=false)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_bairro", type="string", length=100, nullable=false)
     */
    private $bairro;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_cep", type="string", length=20, nullable=false)
     */
    private $cep;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_primeiro_telefone", type="string", length=50, nullable=false)
     */
    private $primeiroTelefone;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_segundo_telefone", type="string", length=50, nullable=false)
     */
    private $segundoTelefone;

    /**
     * @var string
     *
     * @ORM\Column(name="categoria", type="string", nullable=false)
     */
    private $categoria;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_observacao", type="string", length=255, nullable=false)
     */
    private $observacao;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_ativo", type="boolean", nullable=false)
     */
    private $ativo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_excluido", type="boolean", nullable=false)
     */
    private $excluido;

    /**
     * @var Cidade
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Cidade")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cidade", referencedColumnName="id_cidade")
     * })
     */
    private $cidade;

    /**
     * @var Tipo
     *
     * @ORM\ManyToOne(targetEntity="Tipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipo", referencedColumnName="id_tipo")
     * })
     */
    private $tipo;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="TipoSepultamento", inversedBy="id")
     * @ORM\JoinTable(name="tb_tipo_sepultamento_cemiterio",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_cemiterio", referencedColumnName="id_cemiterio")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_tipo_sepultamento", referencedColumnName="id_tipo_sepultamento")
     *   }
     * )
     */
    private $tipoSepultamento;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Application\Entity\Usuario", mappedBy="id")
     * @ORM\JoinTable(name="tb_cemiterio_usuario",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_cemiterio", referencedColumnName="id_cemiterio")
     *   }
     * )
     */
    private $usuarios;

    /**
     * Constructor
     */
    public function __construct() {
        $this->dataCadastro = new \DateTime('now');
        $this->tipoSepultamento = new \Doctrine\Common\Collections\ArrayCollection();
        $this->excluido = false;
    }

    function getId() {
        return $this->id;
    }

    function getDataCadastro() {
        return $this->dataCadastro;
    }

    function getNome() {
        return $this->nome;
    }

    function getEndereco() {
        return $this->endereco;
    }

    function getNumero() {
        return $this->numero;
    }

    function getBairro() {
        return $this->bairro;
    }

    function getCep() {
        return $this->cep;
    }

    function getPrimeiroTelefone() {
        return $this->primeiroTelefone;
    }

    function getSegundoTelefone() {
        return $this->segundoTelefone;
    }

    function getCategoria() {
        return $this->categoria;
    }

    function getEmail() {
        return $this->email;
    }

    function getObservacao() {
        return $this->observacao;
    }

    function getAtivo() {
        return $this->ativo;
    }

    function getExcluido() {
        return $this->excluido;
    }

    function getCidade() {
        return $this->cidade;
    }

    function getTipo() {
        return $this->tipo;
    }

    function getTipoSepultamento() {
        return $this->tipoSepultamento;
    }

    function setId($id) {
        $this->id = $id;
        return $this;
    }

    function setDataCadastro(\DateTime $dataCadastro) {
        $this->dataCadastro = $dataCadastro;
        return $this;
    }

    function setNome($nome) {
        $this->nome = $nome;
        return $this;
    }

    function setEndereco($endereco) {
        $this->endereco = $endereco;
        return $this;
    }

    function setNumero($numero) {
        $this->numero = $numero;
        return $this;
    }

    function setBairro($bairro) {
        $this->bairro = $bairro;
        return $this;
    }

    function setCep($cep) {
        $this->cep = $cep;
        return $this;
    }

    function setPrimeiroTelefone($primeiroTelefone) {
        $this->primeiroTelefone = $primeiroTelefone;
        return $this;
    }

    function setSegundoTelefone($segundoTelefone) {
        $this->segundoTelefone = $segundoTelefone;
        return $this;
    }

    function setCategoria($categoria) {
        $this->categoria = $categoria;
        return $this;
    }

    function setEmail($email) {
        $this->email = $email;
        return $this;
    }

    function setObservacao($observacao) {
        $this->observacao = $observacao;
        return $this;
    }

    function setAtivo($ativo) {
        $this->ativo = $ativo;
        return $this;
    }

    function setExcluido($excluido) {
        $this->excluido = $excluido;
        return $this;
    }

    function setCidade(Cidade $cidade) {
        $this->cidade = $cidade;
        return $this;
    }

    function setTipo(Tipo $tipo) {
        $this->tipo = $tipo;
        return $this;
    }

    function setTipoSepultamento(\Doctrine\Common\Collections\Collection $tipoSepultamento) {
        $this->tipoSepultamento = $tipoSepultamento;
        return $this;
    }

    public function __toString() {
        return $this->nome;
    }

    public function getLabel() {
        return $this->__toString();
    }

    public function toArray() {
        $dados = array(
            "id" => $this->id,
            "dataCadastro" => $this->dataCadastro,
            "nome" => $this->nome,
            "endereco" => $this->endereco,
            "numero" => $this->numero,
            "bairro" => $this->bairro,
            "cep" => $this->cep,
            "primeiroTelefone" => $this->primeiroTelefone,
            "segundoTelefone" => $this->segundoTelefone,
            "categoria" => $this->categoria,
            "email" => $this->email,
            "observacao" => $this->observacao,
            "ativo" => $this->ativo,
            "excluido" => $this->excluido,
            "cidade" => $this->cidade->getId(),
            "estado" => $this->cidade->getUf()->getId(),
            "tipo" => $this->tipo
        );

        $dados['tipoSepultamento'] = array();

        foreach ($this->tipoSepultamento as $tipo) {

            $dados['tipoSepultamento'][$tipo->getId()] = $tipo->getId();
        }

        return $dados;
    }

}
