<?php
namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * TipoSepultamento
 *
 * @ORM\Table(name="tb_tipo_sepultamento")
 * @ORM\Entity(repositoryClass="Admin\Repository\TipoSepultamentoRepository")
 */
class TipoSepultamento extends BaseEntity implements EntityInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_tipo_sepultamento", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome", type="string", length=255, nullable=false)
     */
    private $nome;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Cemiterio", mappedBy="id")
     * @ORM\JoinTable(name="tb_tipo_sepultamento_cemiterio",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_cemiterio", referencedColumnName="id_cemiterio")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_tipo_sepultamento", referencedColumnName="id_tipo_sepultamento")
     *   }
     * )
     */
    private $cemiterios;

    /**
     * Constructor
     */
    public function __construct() {
        $this->cemiterios = new \Doctrine\Common\Collections\ArrayCollection();
    }

    function getId() {
        return $this->id;
    }

    function getNome() {
        return $this->nome;
    }

    function getCemiterios() {
        return $this->cemiterios;
    }

    function setId($id) {
        $this->id = $id;
        return $this;
    }

    function setNome($nome) {
        $this->nome = $nome;
        return $this;
    }

    function setCemiterios(\Doctrine\Common\Collections\Collection $cemiterios) {
        $this->cemiterios = $cemiterios;
        return $this;
    }

    public function __toString() {
        return $this->nome;
    }

    public function getLabel() {
        return $this->__toString();
    }

    public function toArray() {
        return array(
            "id" => $this->id,
            "nome" => $this->nome,
            "cemiterios" => $this->cemiterios
        );
    }

}
