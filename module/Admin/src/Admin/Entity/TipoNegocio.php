<?php

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;
/**
 * TipoNegocio
 *
 * @ORM\Table(name="tb_tipo_negocio")
 * @ORM\Entity(repositoryClass="Admin\Repository\TipoNegocioRepository")
 */
class TipoNegocio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_tipo_negocio", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_tipo", type="string", length=50, nullable=true)
     */
    private $tipo;
    function getId() {
        return $this->id;
    }

    function getTipo() {
        return $this->tipo;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

     public function __toString() {
        return $this->tipo;
    }

    public function getLabel() {
        return $this->__toString();
    }

    public function toArray() {
        return array(
            "id" => $this->id,
            "tipo" => $this->tipo,
          
        );
    }



}
