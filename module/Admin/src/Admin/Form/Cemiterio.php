<?php

namespace Admin\Form;

use Zend\Form\Form;
use Admin\InputFilter\Cemiterio as CemiterioFilter;

/**
 * Classe que abstrai o formulário de cemitério
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Cemiterio extends Form {

    /**
     * Monta o formulário de tesouraria
     */
    public function __construct(CemiterioFilter $inputFilter, $estados = array(), $cidades = array(), $tipos = array(), $tiposSepultamento = array()) {
        //Seta o nome do formulário
        parent::__construct('cemiterio');

        $this->setLabel('Cemitério/Crematório');
        $this->setAttribute('icon', 'icon-user');
        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'form-horizontal form-bordered form-label-stripped'));

        $this->setInputFilter($inputFilter);

        //Adiciona o campo que recebe o nome do usuário
        $this->add(array(
            'name' => 'nome',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'nome',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Nome*:',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));

        //Adiciona o campo que recebe o endereço
        $this->add(array(
            'name' => 'endereco',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'endereco',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Endereço*:',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));

        //Adiciona o campo que recebe o número
        $this->add(array(
            'name' => 'numero',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'numero',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Nº*:',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label numero'
                )
            ),
        ));

        //Adiciona o campo que recebe o bairro
        $this->add(array(
            'name' => 'bairro',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'bairro',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Bairro*: ',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));

        //Adiciona o campo que recebe o bairro
        $this->add(array(
            'name' => 'cep',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'cep',
                'class' => 'form-control cep'
            ),
            'options' => array(
                'label' => 'CEP*: ',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));

        //Adiciona o select de estado
        $this->add(array(
            'name' => 'estado',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'estado',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Estado*:',
                'value_options' => $estados,
                'disable_inarray_validator' => true,
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));

        //Adiciona o select de cidade
        $this->add(array(
            'name' => 'cidade',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'cidade',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Cidade*:',
                'value_options' => $cidades,
                'disable_inarray_validator' => true,
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));

        //Adiciona o select de tipo
        $this->add(array(
            'name' => 'tipo',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'tipo',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Tipo*:',
                'value_options' => $tipos,
                'disable_inarray_validator' => true,
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));


        //Adiciona campo que recebe tipos de sepultamento
        $this->add(array(
            'type' => 'Zend\Form\Element\MultiCheckbox',
            'name' => 'tipoSepultamento',
            'options' => array(
                'label' => 'Tipo de Sepultamento* :',
                'label_attributes' => array(
                    'style' => 'width: 100%'
                ),
                'value_options' => $tiposSepultamento,
                'disable_inarray_validator' => true
            )
        ));

        //Adiciona campo que recebe categorias
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'categoria',
            'options' => array(
                'label' => 'Categoria*:',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label',
                    'style' => 'width: 60px; padding-top: 0;'
                ),
                'disable_inarray_validator' => true,
                'value_options' => array(
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4'
                )
            ),
            'attributes' => array(
                'id' => 'categoria',
            //'value' => 1
            )
        ));

        //Adiciona o campo que recebe o primeiro telefone
        $this->add(array(
            'name' => 'primeiroTelefone',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'primeiroTelefone',
                'class' => 'form-control phone_with_ddd'
            ),
            'options' => array(
                'label' => 'Telefone*:',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label fone'
                )
            ),
        ));

        //Adiciona o campo que recebe o segundo telefone
        $this->add(array(
            'name' => 'segundoTelefone',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'segundoTelefone',
                'class' => 'form-control phone_with_ddd'
            ),
            'options' => array(
                'label' => 'Telefone 2:',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));

        //Adiciona o campo que recebe o login do usuário
        $this->add(array(
            'name' => 'email',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'email',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'E-mail: ',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));

        //Adiciona o campo que recebe as observações
        $this->add(array(
            'name' => 'observacao',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'id' => 'observacao',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Observações: ',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'ativo',
            'options' => array(
                'label' => 'Status*:',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label',
                    'style' => 'width: 100px; padding-top: 0;'
                ),
                'value_options' => array(
                    '1' => 'Ativo',
                    '0' => 'Inativo',
                ),
            ),
            'attributes' => array(
                'id' => 'status',
                'value' => 1
            )
        ));

        //Adiciona o campo hidden que leva o id
        $this->add(array(
            'name' => 'id',
            'type' => 'Zend\Form\Element\Hidden'
        ));

        //Monta o submit
        $this->add(array(
            'name' => 'enviar',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => 'ENVIAR',
                'class' => 'btn btn-primary submit-input-loading'
            )
        ));
    }

    public function isValid() {

        if ($this->get('tipo')->getValue() == 2) {
            $this->getInputFilter()->remove('tipoSepultamento');
        }

        $valid = parent::isValid();

        return $valid;
    }

}
