<?php

namespace Admin\Form;

use Zend\Form\Form;

/**
 * Classe que abstrai o formulário de agencia
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class DefaultFiltro extends Form {

    /**
     * Monta o formulário de tesouraria
     */
    public function __construct() {
        //Seta o nome do formulário
        parent::__construct('agencia');

        $this->setLabel('Agência');
        $this->setAttribute('icon', 'icon-user');
        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'form-horizontal form-bordered form-label-stripped'));

        $this->add(array(
            'name' => 'status',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'status',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Status',
                'value_options' => array(
                    '' => 'Todos',
                    '1' => 'Ativo',
                    '0' => 'Inativo',
                ),
                'disable_inarray_validator' => true,
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));
    }

}
