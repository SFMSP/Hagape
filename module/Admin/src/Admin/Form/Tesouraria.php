<?php

namespace Admin\Form;

use Zend\Form\Form;
use Admin\InputFilter\Tesouraria as TesourariaFilter;

/**
 * Classe que abstrai o formulário de tesouraria
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Tesouraria extends Form {

    /**
     * Monta o formulário de tesouraria
     */
    public function __construct(TesourariaFilter $inputFilter) {
        //Seta o nome do formulário
        parent::__construct('tesouraria');

        $this->setLabel('Tesouraria');
        $this->setAttribute('icon', 'icon-user');
        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'form-horizontal form-bordered form-label-stripped'));

        $this->setInputFilter($inputFilter);

        //Adiciona o campo que recebe o nome do usuário
        $this->add(array(
            'name' => 'nome',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'nome',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Nome *:',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));

        //Adiciona o campo que recebe o endereço
        $this->add(array(
            'name' => 'endereco',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'endereco',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Endereço *:',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));

        //Adiciona o campo que recebe o primeiro telefomne
        $this->add(array(
            'name' => 'primeiroTelefone',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'primeiroTelefone',
                'class' => 'form-control phone_with_ddd'
            ),
            'options' => array(
                'label' => 'Telefone *:',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));

        //Adiciona o campo que recebe o segundo telefone
        $this->add(array(
            'name' => 'segundoTelefone',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'segundoTelefone',
                'class' => 'form-control phone_with_ddd'
            ),
            'options' => array(
                'label' => 'Telefone 2:',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));


        //Adiciona o campo que recebe o login do usuário
        $this->add(array(
            'name' => 'email',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'email',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'E-mail:',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));

         //Adiciona o campo que recebe as observações
        $this->add(array(
            'name' => 'observacao',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'id' => 'observacao',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Observação:',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));

      $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'ativo',
            'options' => array(
                'label' => 'Status *:',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label',
                    'style' => 'width: 100px; padding-top: 0;'
                ),
                'value_options' => array(
                    '1' => 'Ativo',
                    '0' => 'Inativo',
                ),
            ),
            'attributes' => array(
                'id' => 'status',
                'value' => 1
            )
        ));

        //Adiciona o campo hidden que leva o id
        $this->add(array(
            'name' => 'id',
            'type' => 'Zend\Form\Element\Hidden'
        ));

        //Monta o submit
        $this->add(array(
            'name' => 'enviar',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => 'ENVIAR',
                'class' => 'btn btn-primary submit-input-loading'
            )
        ));
    }

}
