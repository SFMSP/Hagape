<?php

namespace Admin\Form;

use Zend\Form\Form;
use Admin\Entity\Estoque;

/**
 * Classe que abstrai o formulário de agencia
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class EstoqueFiltro extends Form {

    /**
     * Monta o formulário de tesouraria
     */
    public function __construct() {
        //Seta o nome do formulário
        parent::__construct('estoque');

        $this->setLabel('Estoque');
        $this->setAttribute('icon', 'icon-user');
        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'form-horizontal form-bordered form-label-stripped'));

        $this->add(array(
            'name' => 'status',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'status',
                'class' => 'form-control',
                'style' => 'float: left; width: 100px; margin-right: 10px; '
            ),
            'options' => array(
                'label' => 'Status',
                'value_options' => array(
                    '' => 'Status',
                    '1' => 'Ativo',
                    '0' => 'Inativo',
                ),
                'disable_inarray_validator' => true,
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));

        $this->add(array(
            'name' => 'tipo',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'tipo',
                'class' => 'form-control',
                'style' => 'float: left; width: 100px; '
            ),
            'options' => array(
                'label' => 'Tipo',
                'value_options' => array(
                    '' => 'Tipo',
                    Estoque::ESTOQUE_CENTRAL => 'Central',
                    Estoque::POLO => 'Polo',
                ),
                'disable_inarray_validator' => true,
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));
    }

}
