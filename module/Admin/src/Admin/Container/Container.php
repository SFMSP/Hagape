<?php

namespace Admin\Container;

use Base\Container\BaseContainer as BaseContainerInterface;

/**
 * Classe que armazena todas as dependências para serem chamados na classe Module
 *
 * @author Praxedes
 */
class Container implements BaseContainerInterface {

    /**
     * 
     * Retorna as instâncias dos forms
     * @return array
     */
    public function getForms() {
        return array(
            'Admin\Form\Agencia' => function($sm) {
                $inputFilter = new \Admin\InputFilter\Agencia();

                return new \Admin\Form\Agencia($inputFilter);
            },
            'Admin\Form\Cemiterio' => function($sm) {
                $estados = $sm->get('Application\Repository\UfRepository')->getArraySelect();
                $tipos = $sm->get('Admin\Repository\TipoRepository')->getArraySelect();
                $cidades = array('' => 'Escolha uma Cidade');
                //$tiposSepultamento = $sm->get('Admin\Repository\TipoSepultamentoRepository')->getArraySelect('', array(), false);
                
                $tiposSepultamento = $sm->get('Agencia\Repository\ServicoRepository')->getArraySelect('', array(), false);

                $inputFilter = new \Admin\InputFilter\Cemiterio();

                $form = new \Admin\Form\Cemiterio($inputFilter, $estados, $cidades, $tipos, $tiposSepultamento);
                $form->setData(array('estado' => 26));
                return $form;
            },
            'Admin\Form\Estoque' => function($sm) {
                $estados = $sm->get('Application\Repository\UfRepository')->getArraySelect();
                $estoqueRepository = $sm->get('Admin\Repository\EstoqueRepository');
                
                $inputFilter = new \Admin\InputFilter\Estoque();

                $form = new \Admin\Form\Estoque($inputFilter, $estados);
                $form->setEstoqueRepository($estoqueRepository);
                $form->setData(array('estado' => 26));
                return $form;
            },
            'Admin\Form\Tesouraria' => function($sm) {
                $inputFilter = new \Admin\InputFilter\Tesouraria();

                return new \Admin\Form\Tesouraria($inputFilter);
            },
            'Admin\Form\Trafego' => function($sm) {
                $estados = $sm->get('Application\Repository\UfRepository')->getArraySelect();
                $cidades = $sm->get('Application\Repository\CidadeRepository')->getArraySelect();

                $inputFilter = new \Admin\InputFilter\Trafego();

                $form = new \Admin\Form\Trafego($inputFilter, $estados, $cidades);
                $form->setData(array('estado' => 26));
                return $form;
            },
            'Admin\Form\DefaultFiltro' => function($sm) {
                return new \Admin\Form\DefaultFiltro();
            },
            'Admin\Form\EstoqueFiltro' => function($sm) {
                return new \Admin\Form\EstoqueFiltro();
            },
        );
    }

    /**
     * Método que configura as dependências
     * @return array
     */
    public function getAuthServices() {
        return array();
    }

    /**
     * 
     * Retorna os services com as instâncias dos repositories
     * @return array
     */
    public function getRepositories() {
        return array(
            'Admin\Repository\AgenciaRepository' => function($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Admin\Entity\Agencia');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Admin\Repository\CemiterioRepository' => function($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Admin\Entity\Cemiterio');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Admin\Repository\EstoqueRepository' => function($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Admin\Entity\Estoque');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Admin\Repository\TesourariaRepository' => function($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Admin\Entity\Tesouraria');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Admin\Repository\TipoRepository' => function($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Admin\Entity\Tipo');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Admin\Repository\TipoSepultamentoRepository' => function($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Admin\Entity\TipoSepultamento');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Agencia\Repository\ServicoRepository' => function($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\Servico');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },        
            'Admin\Repository\TrafegoRepository' => function($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Admin\Entity\Trafego');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            }
        );
    }

    /**
     * 
     * @return array
     */
    public function getFormsFilter() {
        return array();
    }

    /**
     * 
     * Retorna as instâncias de services
     * @return array
     */
    public function getServices() {
        return array(
            'Admin\Service\Agencia' => function($sm) {
                $service = new \Admin\Service\Agencia($sm->get('Doctrine\ORM\EntityManager'), new \Admin\Entity\Agencia(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                        ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                        ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());


                return $service;
            },
            'Admin\Service\Cemiterio' => function($sm) {
                $service = new \Admin\Service\Cemiterio($sm->get('Doctrine\ORM\EntityManager'), new \Admin\Entity\Cemiterio(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                        ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                        ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());


                return $service;
            },
            'Admin\Service\Estoque' => function($sm) {
                $service = new \Admin\Service\Estoque($sm->get('Doctrine\ORM\EntityManager'), new \Admin\Entity\Estoque(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                        ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                        ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());


                return $service;
            },
            'Admin\Service\Tesouraria' => function($sm) {
                $service = new \Admin\Service\Tesouraria($sm->get('Doctrine\ORM\EntityManager'), new \Admin\Entity\Tesouraria(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                        ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                        ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());


                return $service;
            },
            'Admin\Service\Trafego' => function($sm) {
                $service = new \Admin\Service\Trafego($sm->get('Doctrine\ORM\EntityManager'), new \Admin\Entity\Trafego(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                        ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                        ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());


                return $service;
            }
        );
    }

    /**
     * 
     * Retorna os serviços básicos
     * @return array
     */
    public function getBaseServices() {
        return array();
    }

    /**
     * Retorna os serviços do PHPOffice
     * @return array
     */
    public function getOfficeServices() {
        return array();
    }

    public function getMailConfig() {
        return array();
    }

}
