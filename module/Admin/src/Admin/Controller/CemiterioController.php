<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Application\Controller\CrudApplicationController;

/**
 * Classe controle de usuários
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class CemiterioController extends CrudApplicationController {

    /**
     *
     * @var string
     */
    protected $nameRepository = 'Admin\Repository\CemiterioRepository';

    /**
     *
     * @var string 
     */
    protected $nameService = 'Admin\Service\Cemiterio';

    /**
     *
     * @var string 
     */
    protected $nameForm = 'Admin\Form\Cemiterio';

    /**
     *
     * @var string 
     */
    protected $nameFormFilter = 'Admin\Form\DefaultFiltro';

    /**
     *
     * @var array 
     */
    protected $jsIndex = array('cemiterio/index.js');

    /**
     *
     * @var array 
     */
    protected $jsFormulario = array('cemiterio/formulario.js');

    /**
     *
     * @var string 
     */
    protected $controller = 'cemiterio';

    /**
     *
     * @var string 
     */
    protected $route = 'admin/default';

    /**
     *
     * @var string
     */
    protected $logicDelete = true;

    /**
     * Ação chamada pelo js para realizar a paginação
     * @return string
     */
    public function paginacaoAction() {
        //Armazena a resposta
        $response = $this->getResponse();

        //Armazena a requisição
        $request = $this->getRequest()->getPost();

        //Organiza os dados da busca
        $busca = array(
            "busca" => mb_strtolower($request['search']['value'], 'UTF-8'),
            "status" => $request['status']
        );

        //Cria a ordenação
        $order = array();

        if (isset($request['order'][0]['column'])) {
            switch ($request['order'][0]['column']) {
                case 1: $order = array('field' => 'c.nome', 'order' => $request['order'][0]['dir']);
                    break;
                case 2: $order = array('field' => 't.nome', 'order' => $request['order'][0]['dir']);
                    break;
                case 3: $order = array('field' => 'c.ativo', 'order' => $request['order'][0]['dir']);
                    break;
            }
        }

        //Busca os dados
        $registros = $this->getServiceLocator()->get($this->nameRepository)->busca($busca, $request['length'], $request['start'], $order);
        $registrosTotaisBusca = $this->getServiceLocator()->get($this->nameRepository)->busca($busca, null, null, array(), true);
        $registrosTotais = $this->getServiceLocator()->get($this->nameRepository)->countAll(true);
        $dados = array();

        //Armazena os dados retornados em um array
        foreach ($registros as $registro) {
            $valor = array();
            $valor[] = '<input type="checkbox" name="checkbox-list" class="checkboxes" value="' . $registro->getId() . '" />';

            if ($this->verifyPermission('cemiterio', 'editar')) {
                $valor[] = "<a href='" . $this->url()->fromRoute('admin/default', array('controller' => 'cemiterio', 'action' => 'formulario', 'id' => $registro->getId())) . "' title='Editar Item'>" . $registro->getNome() . "</a>";
            } else {
                $valor[] = $registro->getNome();
            }
            $valor[] = $registro->getTipo()->getNome();
            $valor[] = $registro->getAtivo() ? "Ativo" : "Inativo";


            $dados[] = $valor;
        }

        //Organiza o retorno
        $retorno['draw'] = $request['draw'];
        $retorno['recordsTotal'] = $registrosTotais;
        $retorno['recordsFiltered'] = $registrosTotaisBusca;
        $retorno['data'] = $dados;


        //Retorna a resposta
        $response->setContent(\Zend\Json\Json::encode($retorno));
        return $response;
    }

    /**
     * Método padrão de formulário
     * @return \Application\Controller\ViewModel
     */
    public function formularioAction() {

        //Instancia o formulário e a requisição
        $form = $this->getServiceLocator()->get($this->nameForm);
        $request = $this->getRequest();

        //Verifica se a requisição foi feita via post
        if ($request->isPost()) {

            //Busca os dados
            $dados = $request->getPost()->toArray();
            if ($this->params()->fromRoute('id')) {
                $dados['id'] = $this->params()->fromRoute('id');
            }

            if ($dados['estado']) {
                $form->get('cidade')->setValueOptions($this->getCidadesAction($dados['estado'], false));
            }

            $form->setData($dados);

            //Valida o formulário
            if ($form->isValid()) {
                $service = $this->getServiceLocator()->get($this->nameService);
                $success = $service->save($dados);
                $this->setConfirmMessages($success);

                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            } else {
                $this->flashMessenger()->addErrorMessage('Por favor, verifique os campos do formulário');
            }
        } elseif ($this->params()->fromRoute('id')) {

            //Busca os dados
            $dados = $this->getServiceLocator()->get($this->nameRepository)->find($this->params()->fromRoute('id'));
            if ($dados) {
                $form->get('cidade')->setValueOptions($this->getCidadesAction($dados->getCidade()->getUf()->getId(), false));
                $form->setData($dados->toArray());
            }
        }

        //Seta os scripts e retorna para view
        if (!empty($this->jsFormulario)) {
            $this->layout()->setVariable('scripts', $this->jsFormulario);
        }

        //Retorna os dados da view Model
        return new ViewModel(array('form' => $form));
    }

}
