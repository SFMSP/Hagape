<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Application\Controller\CrudApplicationController;

/**
 * Classe controle de usuários
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class TesourariaController extends CrudApplicationController {

    /**
     *
     * @var string
     */
    protected $nameRepository = 'Admin\Repository\TesourariaRepository';

    /**
     *
     * @var string 
     */
    protected $nameService = 'Admin\Service\Tesouraria';

    /**
     *
     * @var string 
     */
    protected $nameForm = 'Admin\Form\Tesouraria';

    /**
     *
     * @var array 
     */
    protected $jsIndex = array('tesouraria/index.js');

    /**
     *
     * @var string 
     */
    protected $nameFormFilter = 'Admin\Form\DefaultFiltro';

    /**
     *
     * @var string 
     */
    protected $controller = 'tesouraria';

    /**
     *
     * @var string 
     */
    protected $route = 'admin/default';

    /**
     *
     * @var string
     */
    protected $logicDelete = true;

    /**
     * Ação chamada pelo js para realizar a paginação
     * @return string
     */
    public function paginacaoAction() {
        //Armazena a resposta
        $response = $this->getResponse();

        //Armazena a requisição
        $request = $this->getRequest()->getPost();

        //Organiza os dados da busca
        $busca = array(
            "busca" => mb_strtolower($request['search']['value'], 'UTF-8'),
            "status" => $request['status']
        );

        //Cria a ordenação
        $order = array();

        if (isset($request['order'][0]['column'])) {
            switch ($request['order'][0]['column']) {
                case 1: $order = array('field' => 'nome', 'order' => $request['order'][0]['dir']);
                    break;
                case 2: $order = array('field' => 'ativo', 'order' => $request['order'][0]['dir']);
                    break;
            }
        }

        //Busca os dados
        $registros = $this->getServiceLocator()->get($this->nameRepository)->busca($busca, $request['length'], $request['start'], $order);
        $registrosTotaisBusca = $this->getServiceLocator()->get($this->nameRepository)->busca($busca, null, null, array(), true);
        $registrosTotais = $this->getServiceLocator()->get($this->nameRepository)->countAll(true);
        $dados = array();

        //Armazena os dados retornados em um array
        foreach ($registros as $registro) {
            $valor = array();
            $valor[] = '<input type="checkbox" name="checkbox-list" class="checkboxes" value="' . $registro->getId() . '" />';

            if ($this->verifyPermission('tesouraria', 'editar')) {
                $valor[] = "<a href='" . $this->url()->fromRoute('admin/default', array('controller' => 'tesouraria', 'action' => 'formulario', 'id' => $registro->getId())) . "' title='Editar Item'>" . $registro->getNome() . "</a>";
            } else {
                $valor[] = $registro->getNome();
            }

            $valor[] = $registro->getAtivo() ? "Ativo" : "Inativo";



            $dados[] = $valor;
        }

        //Organiza o retorno
        $retorno['draw'] = $request['draw'];
        $retorno['recordsTotal'] = $registrosTotais;
        $retorno['recordsFiltered'] = $registrosTotaisBusca;
        $retorno['data'] = $dados;


        //Retorna a resposta
        $response->setContent(\Zend\Json\Json::encode($retorno));
        return $response;
    }

    public function InboxAction() {

        return new ViewModel();
    }

}
