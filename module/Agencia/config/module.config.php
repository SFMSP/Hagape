<?php
namespace Agencia;

return array(
    'router' => array(
        'routes' => array(
            'agencia' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/agencia',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Agencia\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/[:controller[/:action][/:id]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '\d+'
                            ),
                            'defaults' => array(),
                        )
                    )
                )
            )
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Agencia\Controller\Index' => 'Agencia\Controller\IndexController',
            'Agencia\Controller\Agencia' => 'Agencia\Controller\AgenciaController',
            'Agencia\Controller\Contratacao' => 'Agencia\Controller\ContratacaoController',
            'Agencia\Controller\Contratante' => 'Agencia\Controller\ContratanteController',
            'Agencia\Controller\DadosObito' => 'Agencia\Controller\DadosObitoController',
            'Agencia\Controller\Declarante' => 'Agencia\Controller\DeclaranteController',
            'Agencia\Controller\Falecido' => 'Agencia\Controller\FalecidoController',
            'Agencia\Controller\DadosObitoVelorio' => 'Agencia\Controller\DadosObitoVelorioController',     
            'Agencia\Controller\DadosAmputacao' => 'Agencia\Controller\DadosAmputacaoController',
            'Agencia\Controller\Paciente' => 'Agencia\Controller\PacienteController',
            'Agencia\Controller\TransladoCorpo' => 'Agencia\Controller\TransladoCorpoController',
            'Agencia\Controller\NotaContratacao' => 'Agencia\Controller\NotaContratacaoController',
            'Agencia\Controller\ProdutoServico' => 'Agencia\Controller\ProdutoServicoController',
            'Agencia\Controller\Emissao' => 'Agencia\Controller\EmissaoController',
            'Agencia\Controller\Pagamento' => 'Agencia\Controller\PagamentoController',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Agencia' => __DIR__ . '/../view',
        )
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    ),
);
