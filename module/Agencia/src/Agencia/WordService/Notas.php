<?php
namespace Agencia\WordService;

use Base\WordService\BaseWordService;
use PhpOffice\PhpWord\TemplateProcessor;
use Admin\Entity\Agencia as AgenciaEntity;
use Agencia\Entity\Contratante as ContratanteEntity;
use Agencia\Entity\Localizacao as LocalizacaoEntity;
use Agencia\Entity\Falecido as FalecidoEntity;
use Agencia\Entity\DadosObito as DadosObitoEntity;
use Agencia\Entity\LocalFalecimento as LocalFalecimentoEntity;
use Agencia\Entity\DadosEmpresa as DadosEmpresaEntity;
use Agencia\Entity\DadosObitoVelorio as DadosObitoVelorioEntity;
use Agencia\Entity\Velorio as DadosVelorioEntity;
use Agencia\Entity\Paciente as PacienteEntity;
use Agencia\Entity\DadosAmputacao as DadosAmputacaoEntity;
use Agencia\Entity\Medico as MedicoEntity;
use Agencia\Entity\Sepultamento as SepultamentoEntity;
use Admin\Entity\Cemiterio as CemiterioEntity;
use Agencia\Entity\Cremacao as CremacaoEntity;
use Agencia\Entity\VendaProdutoServico as VendaProdutoServicoEntity;

use Base\Helper\DatetimeFormat;
/**
 * Classe que gera o relatório de venda
 *
 * @author Praxedes
 */
class Notas extends BaseWordService {

    /**
     *
     * @var TemplateProcessor 
     */
    protected $templateProcessor;

    /**
     * 
     * @param string $template
     * @return \Sistema\WordService\Vencimentos
     */
    public function setTemplate($template) {
        $this->templateProcessor = new TemplateProcessor($this->templateDir . $template);
        return $this;
    }

    /**
     * Gera a nota em docx
     * 
     * @param array $relatorio
     * @return type
     */
    public function gerarRelatorioNotas(
            DatetimeFormat $dateTimeFormat,
            $relatorio,
            $agencia, 
            $contratante,
            $delcarante,
            $localizacao,
            $falecido,
            $dadosObito,
            $localFalecimento,
            $dadosEmpresa,
            $dadosObitoVelorio,
            $dadosVelorio,
            $paciente,
            $dadosAmputacao,
            $medico,
            $sepultamento,
            $cemiterio,
            $cremacao,
            array $produtosVendidos,
            array $servicosVendidos,
            array $arrayChaveValorCategoria,
            array $arrayServicos,
            $vendaProduto,
            array $pagamentos
    ) {

        
        if($relatorio->getTipoContratacao()){
            $tipo_contratacao = $relatorio->getTipoContratacao()->getTipoContratacao();
        }else{
            $tipo_contratacao = "Registro de Obito";
        }
        
        //Seta a data de geração do relatório
        $this->templateProcessor->setValue('data_gerado'        , date('d/m/Y H:i'));
        $this->templateProcessor->setValue('n_nota'             , $relatorio->getNumeroNota());
        $this->templateProcessor->setValue('tel'                , $agencia->getPrimeiroTelefone());
        $this->templateProcessor->setValue('agencia'            , $agencia->getNome()." ");
        $this->templateProcessor->setValue('tipoContratacao'    , $tipo_contratacao);
        
        //Contratante OU declarante
        if($contratante){
            $this->templateProcessor->setValue('contratante'        , $contratante->getNomeContratante());
            $this->templateProcessor->setValue('cpf'                , $contratante->getCpf());
            $this->templateProcessor->setValue('rg'                 , $contratante->getRg());
            $this->templateProcessor->setValue('grau_parentesco'    , $contratante->getGrauParentesco());
            $this->templateProcessor->setValue('cel'                , $contratante->getTelefoneCelular());
            $this->templateProcessor->setValue('nome_mae'           , $contratante->getNomeMae());
        }else{
            $this->templateProcessor->setValue('contratante'        , $delcarante->getNome());
            $this->templateProcessor->setValue('cpf'                , $contratante->getCpf());
            $this->templateProcessor->setValue('rg'                 , $contratante->getRg());
            $this->templateProcessor->setValue('grau_parentesco'    , $contratante->getGrauParentesco());
            $this->templateProcessor->setValue('cel'                , $contratante->getTelefone());
            $this->templateProcessor->setValue('nome_mae'           , $contratante->getNomeMae());
        }
        //Dados Empresa
        $this->templateProcessor->setValue('empresa'                , $dadosEmpresa->getNomeEmpresarial() ? $dadosEmpresa->getNomeEmpresarial() : "");
        $this->templateProcessor->setValue('cnpj'                   , $dadosEmpresa->getCnpj() ? $dadosEmpresa->getCnpj() : "");
        //localizacao
        $endereco = "";
        if($localizacao) $endereco = $localizacao->getEndereco().",".$localizacao->getNumero()."-".$localizacao->getBairro()."-".$localizacao->getCidade();
        $this->templateProcessor->setValue('endereco'               , $endereco );
        $this->templateProcessor->setValue('cep'                    , $localizacao->getCep() ? $localizacao->getCep() : "");
        //Falecido
        $this->templateProcessor->setValue('falecido'               , $falecido->getNome() ? $falecido->getNome() : "");
        $this->templateProcessor->setValue('sexo'                   , $falecido->getSexo() ? $falecido->getSexo() : "");
        $this->templateProcessor->setValue('idade'                  , $falecido->getIdade() ? $falecido->getIdade() : "");
        //DadosObito
        $dateTimeFormat = $dadosObito->getDataFalecimento() ? $dadosObito->getDataFalecimento() : "";
        $this->templateProcessor->setValue('data_obito'             , $dateTimeFormat ? $dateTimeFormat->format("d/m/Y") : "");
        $this->templateProcessor->setValue('horario'                , $dadosObito->getHoraFalecimento() ? $dadosObito->getHoraFalecimento() : "");
        $this->templateProcessor->setValue('causa_morte'            , $dadosObito->getCausaMortis() ? $dadosObito->getCausaMortis() : "");    

        $this->templateProcessor->setValue('destino_falecimento'    , $dadosObito->getDestinoFinal() ? $dadosObito->getDestinoFinal() : "");
        $dateTimeFormat = $dadosObito->getDataCadastro() ? $dadosObito->getDataCadastro() : "";
        $this->templateProcessor->setValue('data'                   , $dateTimeFormat ? $dateTimeFormat->format("d/m/Y") : "");
        $this->templateProcessor->setValue('observacoes'            , $dadosObito->getObservacao() ? $dadosObito->getObservacao() : "");
        //LocalFalecimento
        $this->templateProcessor->setValue('local_falecimento'          , $localFalecimento->getLocal() ? $localFalecimento->getLocal() : "");    
        //DadosVelorio
        $this->templateProcessor->setValue('local_velorio'              , $dadosVelorio->getLocalVelorio() ? $dadosVelorio->getLocalVelorio() : "");    
        $dateTimeFormat = $dadosVelorio->getDataSaida() ? $dadosVelorio->getDataSaida() : "";
        $this->templateProcessor->setValue('data_saida'                 , $dateTimeFormat ? $dateTimeFormat->format("d/m/Y") : "");
        $this->templateProcessor->setValue('horario_saida'              , $dadosVelorio->getHoraSaida() ? $dadosVelorio->getHoraSaida() : "");
        //Paciente
        $this->templateProcessor->setValue('paciente'                   , $paciente->getNomePaciente() ? $paciente->getNomePaciente() : "");
        $this->templateProcessor->setValue('membro_amputado'            , $paciente->getMembroAmputado() ? $paciente->getMembroAmputado() : "");
        //$dadosAmputacao
        $this->templateProcessor->setValue('causa_amputacao'            , $dadosAmputacao->getCausaAmputacao() ? $dadosAmputacao->getCausaAmputacao() : "");
        $this->templateProcessor->setValue('local_amputacao'            , $dadosAmputacao->getLocalAmputacao() ? $dadosAmputacao->getLocalAmputacao() : "");
        $this->templateProcessor->setValue('medico'                     , $medico->getNomeMedico() ? $medico->getNomeMedico() : "");
        $this->templateProcessor->setValue('crm'                        , $medico->getCrm()        ? $medico->getCrm() : "");    
        //Sepultamento - Cemiterio
        $this->templateProcessor->setValue('sepultamento'               , $dadosVelorio->getLocalVelorio() ? $dadosVelorio->getLocalVelorio() : "");
        $dateTimeFormat = $sepultamento->getDataSepultamento() ? $sepultamento->getDataSepultamento() : "";
        $this->templateProcessor->setValue('dt_sepul'                   , $dateTimeFormat ? $dateTimeFormat->format("d/m/Y") : "");
        $this->templateProcessor->setValue('hsepul'                     , $sepultamento->getHoraSepultamento() ? $sepultamento->getHoraSepultamento() : "");
        $this->templateProcessor->setValue('destino_sepultamento'       , $cemiterio->getNome() ? $cemiterio->getNome() : "");
        //Crematorio
        $this->templateProcessor->setValue('crematorio'                 , $dadosVelorio->getLocalVelorio() ? $dadosVelorio->getLocalVelorio() : "");
        $this->templateProcessor->setValue('destino_crematorio'         , $cemiterio->getNome() ? $cemiterio->getNome() : "");
        $dateTimeFormat = $cremacao->getDataCremacao() ? $cremacao->getDataCremacao() : "";
        $this->templateProcessor->setValue('dt_cremacao'                , $dateTimeFormat ? $dateTimeFormat->format("d/m/Y") : "");
        $this->templateProcessor->setValue('h_cremacao'                 , $cremacao->getHoraCremacao() ? $cremacao->getHoraCremacao() : "");
        
        //Clona as linhas
        $this->templateProcessor->cloneRow('produto', count($produtosVendidos));
        
        //Cria o contador
        $contador = 1;
        
        //Produtos
        if($produtosVendidos){
            foreach ($produtosVendidos as $registro) {
                //Clona as linhas
                $this->templateProcessor->setValue('categoria#'.$contador, $registro->getProduto()->getCategoria()->getNome());
                $this->templateProcessor->setValue('produto#'.$contador, $registro->getProduto()->getNome());
                $this->templateProcessor->setValue('tamanho#'.$contador, $registro->getProduto()->getMedida()->getMedida());
                $this->templateProcessor->setValue('qtd#'.$contador, $registro->getQuantidade());
                $this->templateProcessor->setValue('valor#'.$contador, "R$ ".number_format($registro->getValorConjunto(), 2, ',', '.'));
                $contador++;
            }
        }
        
        //SERVIÇOS    
        $arrayGamba = array();
        if($arrayServicos){
            foreach ($arrayServicos as $key => $registros) {
                $nome_categoria = $arrayChaveValorCategoria[$key];
                $arrayGamba[] = array('nome' => $nome_categoria, 'valor' => '', 'tipo' => 'T');
                foreach ($registros as $registro) {
                    $nome_servico   = $registro->getServico()->getNome();
                    $valor_servico  = "R$ ".number_format($registro->getValorConjunto(), 2, ',', '.');
                    $arrayGamba[]   = array('nome' => $nome_servico, 'valor' => $valor_servico, 'tipo' => 'S');
                }
            }
        }
        
        $this->templateProcessor->cloneRow('tipo_servico', count($arrayGamba));      
        $contador = 1;
        if($arrayGamba){
            foreach ($arrayGamba as $gamba){
                $nome = $gamba["nome"];
                //Gamba maluca para colocar negrito
                if($gamba["tipo"] == "T") $nome = "<w:r><w:rPr><w:b/></w:rPr><w:t>". $gamba["nome"] ."</w:t></w:r>";
                $this->templateProcessor->setValue('tipo_servico#'.$contador,  $nome);
                $this->templateProcessor->setValue('valor_servico#'.$contador, $gamba["valor"]);
                $contador++;
            }
        }
        
        $this->templateProcessor->setValue('observacao_6'               , $vendaProduto->getObservacao() ? $vendaProduto->getObservacao() : "");
        $this->templateProcessor->setValue('valor_total'                , $vendaProduto->getValorTotal() ? "R$ ".number_format($vendaProduto->getValorTotal(), 2, ',', '.') : "");
        $this->templateProcessor->setValue('convenio'                   , $vendaProduto->getConvenio()->getNome() ? $vendaProduto->getConvenio()->getNome() : "");
        
        
        
        $contador = 1;
        $total_pago = "";
        $metodo = "";
        //PAGAMENTOS
        $this->templateProcessor->cloneRow('nome_pagamento', count($pagamentos));
        if($pagamentos){
            foreach ($pagamentos as $registro) {
                $total_pago += $registro->getValorPagamento();

                $metodo .= $registro->getTipoPagamento()->getTipoPagamento()." - ";
                $metodo .= $registro->getBandeira()->getBandeira()." - ";
                $metodo .= $registro->getParcelamento()." - ";
                $metodo .= $registro->getAutorizacao();

                $this->templateProcessor->setValue('nome_pagamento#'.$contador, $registro->getNome());
                $this->templateProcessor->setValue('cpf_pagamento#'.$contador, $registro->getNumeroDocumento());
                $this->templateProcessor->setValue('tel_pagamento#'.$contador, $registro->getTelefone());
                $this->templateProcessor->setValue('metodo_pagamento#'.$contador, $metodo );
                $this->templateProcessor->setValue('valor_pag#'.$contador, "R$ ".number_format($registro->getValorPagamento(), 2, ',', '.'));
                $contador++;
            }
        }
        $this->templateProcessor->setValue('total_pago', $total_pago ? "R$ ".number_format($total_pago, 2, ',', '.') : "");
        
        $nomeArquivo = "nota".date('Ymdhms').".docx";
        
        
        $this->templateProcessor->saveAs($this->tmpDir.$nomeArquivo);
        
        return $this->tmpDir.$nomeArquivo;
    }

}
