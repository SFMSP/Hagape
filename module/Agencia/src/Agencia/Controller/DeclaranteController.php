<?php

namespace Agencia\Controller;

use Application\Controller\CrudApplicationController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container as SessionConteiner;

class DeclaranteController extends CrudApplicationController
{
    /**
     * @var string
     */
    protected $nameForm = 'Agencia\Form\Declarante';
    /**   
     * @var string
     */
    protected $nameRepository = 'Agencia\Repository\DeclaranteRepository';
    /**
     * @var string
     */
    protected $nameService = 'Agencia\Service\Declarante';
    /**
     * @var string
     */
    protected $controller = 'declarante';
    /**
     * @var string
     */
    protected $route = 'agencia/default';
    /**
     * @var array
     */
    protected $jsIndex = array('declarante/index.js');
    /**
     * @var array
     */
    protected $jsFormulario = array('declarante/formulario.js');

    /**
     * @return ViewModel
     */
   

    /**
     * Método padrão de formulário
     * @return |Application\Controller\ViewModel
     */
    public function formularioAction()
    {
        //Instancia o formulário e a requisição
        $form = $this->getServiceLocator()->get($this->nameForm);
        $request = $this->getRequest();
       
        $estados = [];
      
        //Verificando de recebeu o dado da session
        $contratacao = new SessionConteiner('Contratacao');
      
        $abas = $this->getServiceLocator()->get('Agencia\Repository\ProcessoAbaRepository')->buscaAba($contratacao->tipoOperacao, $contratacao->tipoContratacao, $contratacao->tipoDestinoFinal);
      
        //Verifica se a requisição foi feita via post
        if ($request->isPost()) {
            //Busca os dados
            $dados = $request->getPost()->toArray();
            
            if ($this->params()->fromRoute('id')) {
                $dados['id'] = $this->params()->fromRoute('id');
            }
            
            $form->setData($dados);
            //Valida o formulário
           
            if ($form->isValid()) {
                $service = $this->getServiceLocator()->get($this->nameService);
              
                $success = $service->save($dados);
                $this->setConfirmMessages($success);

                return $this->redirect()->toRoute($this->route, array('controller' => 'declarante', 'action' => 'formulario'));
               // return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            } else {
                
           
                $this->flashMessenger()->addErrorMessage('Por favor, verifique os campos do formulário');
            }


        } elseif ($contratacao->id) {
            $contratacaoReference = $this->getEm()->getReference('Agencia\Entity\Contratacao', ['id' => $contratacao->id]);
            $dados = $this->getServiceLocator()->get($this->nameRepository)->findBy(['contratacao' => $contratacaoReference]);

            if ($dados) {
                $dadosLocalizacao = $this->getServiceLocator()->get('Agencia\Repository\LocalizacaoRepository')->findBy(['declarante' => $dados[0]]);
                $dadosEmpresa = $this->getServiceLocator()->get('Agencia\Repository\DadosEmpresaRepository')->findOneBy(['declarante' => $dados[0]]);
                $estadoCidade = $this->getServiceLocator()->get('Application\Repository\CidadeRepository')->findBy(['id' => $dadosLocalizacao[0]->getCidade()]);
                $cidades = $this->getServiceLocator()->get('Application\Repository\CidadeRepository')->getArraySelect("Escolha uma Cidade", [], true, " where t.uf = " . $estadoCidade[0]->getUf()->getId() . " ");

                $form->get('nome')->setValue($dados[0]->getNome());
                $form->get('telefoneResidencial')->setValue($dados[0]->getTelefone());
                $form->get('telefoneCelular')->setValue($dados[0]->getCelular());
                $form->get('email')->setValue($dados[0]->getEmail());
                $form->get('endereco')->setValue($dadosLocalizacao[0]->getEndereco());
                $form->get('bairro')->setValue($dadosLocalizacao[0]->getBairro());
                $form->get('numero')->setValue($dadosLocalizacao[0]->getNumero());
                $form->get('estado')->setValue($estadoCidade[0]->getId());
                $form->get('cidade')->setValueOptions($cidades);
                $form->get('cidade')->setValue($dadosLocalizacao[0]->getCidade());
                $form->get('estado')->setValue($estadoCidade[0]->getUf()->getId());
                $form->get('complemento')->setValue($dadosLocalizacao[0]->getComplemento());
                $form->get('cep')->setValue($dadosLocalizacao[0]->getCep());
                
                
                
               if ($dadosEmpresa) {
                    $form->get('empresa')->setValue($dadosEmpresa->getNomeEmpresarial());
                    $form->get('cnpj')->setValue($dadosEmpresa->getCnpj());
                    $form->get('enderecoEmpresa')->setValue($dadosEmpresa->getEndereco());
                    $form->get('telefoneEmpresa')->setValue($dadosEmpresa->getTelefone());
                    $form->get('representante')->setValue($dadosEmpresa->getRepresentante());
                    $form->get('empresaAcompanha')->setValue('S');
                }

                $form->setData($dados[0]->toArray());
            } else {
                $dados['id'] = '';
            }
          
        }

            if (!empty($this->jsFormulario)) {
                $this->layout()->setVariable('scripts', $this->jsFormulario);
            }
        return new ViewModel(array(
            'form' => $form, 
            'contratacao' => $contratacao, 
            'estados' => $estados,
            'aba' =>$abas,
             'controller'=> $this->controller
          
                ));
    }
    
     
    
    


}