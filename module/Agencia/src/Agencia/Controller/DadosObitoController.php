<?php

namespace Agencia\Controller;

use Application\Controller\CrudApplicationController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container as SessionConteiner;

class DadosObitoController extends CrudApplicationController
{
    /**
     * @var string
     */
    protected $nameForm = 'Agencia\Form\DadosObito';
    /**   
     * @var string
     */
    protected $nameRepository = 'Agencia\Repository\DadosObitoRepository';
    /**
     * @var string
     */
    protected $nameService = 'Agencia\Service\DadosObito';
    /**
     * @var string
     */
    protected $controller = 'dadosobito';
    /**
     * @var string
     */
    protected $route = 'agencia/default';
    /**
     * @var array
     */
    protected $jsIndex = array('dadosobito/index.js');
    /**
     * @var array
     */
    protected $jsFormulario = array('dadosobito/formulario.js');

    /**
     * @return ViewModel
     */
   

    /**
     * Método padrão de formulário
     * @return |Application\Controller\ViewModel
     */
    public function formularioAction()
    {
        //Instancia o formulário e a requisição
        $form = $this->getServiceLocator()->get($this->nameForm);
        $request = $this->getRequest();
       
        $estados = [];
        $velorio=[];
        $falecimento=[];
        $medicos=[];
         $cemiterios=[];
        $crematorios=[];
        //Verificando de recebeu o dado da session
        $contratacao = new SessionConteiner('Contratacao');
        $abas = $this->getServiceLocator()->get('Agencia\Repository\ProcessoAbaRepository')->buscaAba($contratacao->tipoOperacao, $contratacao->tipoContratacao, $contratacao->tipoDestinoFinal);
        //Verifica se a requisição foi feita via post
        if ($request->isPost()) {
            //Busca os dados
            $dados = $request->getPost()->toArray();
           
            if ($this->params()->fromRoute('id')) {
                $dados['id'] = $this->params()->fromRoute('id');
            }

            $form->setData($dados);
            //Valida o formulário
            if ($form->isValid()) {
                $service = $this->getServiceLocator()->get($this->nameService);
                
                $success = $service->save($dados);
                $this->setConfirmMessages($success);

                return $this->redirect()->toRoute($this->route, array('controller' => 'dadosobito', 'action' => 'formulario'));
               // return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            } else {
                 var_dump($form->getMessages());die;
                $this->flashMessenger()->addErrorMessage('Por favor, verifique os campos do formulário');
            }


        } elseif ($contratacao->id) {
          
            $velorio =      $this->getServiceLocator()->get('Agencia\Repository\VelorioRepository')->buscaArrayInputFilter();
            $falecimento =  $this->getServiceLocator()->get('Agencia\Repository\LocalFalecimentoRepository')->buscaArrayInputFilter();
            $medicos =      $this->getServiceLocator()->get('Agencia\Repository\MedicoRepository')->buscaArrayInputFilter();
            $cemiterios =   $this->getServiceLocator()->get('Admin\Repository\CemiterioRepository')->buscaArrayInputFilter(1);
            $crematorios =  $this->getServiceLocator()->get('Admin\Repository\CemiterioRepository')->buscaArrayInputFilter(2);
            
           
            $falecido = $this->getServiceLocator()->get('Agencia\Repository\FalecidoRepository')->findBy(['contratacao' => $contratacao->id]);
            $dados = $this->getServiceLocator()->get($this->nameRepository)->findBy(['falecido' => $falecido]);
            
            if ($dados) {
                $sepultamento = $this->getServiceLocator()->get('Agencia\Repository\SepultamentoRepository')->findBy(['dadosObito' => $dados[0]]);
                $cremacao = $this->getServiceLocator()->get('Agencia\Repository\CremacaoRepository')->findBy(['dadosObito' => $dados[0]]);
                //$medico = $this->getServiceLocator()->get('Agencia\Repository\MedicoRepository')->findBy(['dadosObito' => $dados[0]]);
                if($dados[0]->getLocalFalecimento()){
                    $form->get('localFalecimento')->setValue($dados[0]->getLocalFalecimento());
                    $form->get('enderecoFalecimento')->setValue($dados[0]->getLocalFalecimento()->getEndereco());
                }else{
                    $form->get('localFalecimento')->setValue($dados[0]->getNomeLocalFalecimento());
                    $form->get('enderecoFalecimento')->setValue($dados[0]->getEnderecoFalecimento());
                }
                
                $form->get('cartorio')->setValue($dados[0]->getCartorio());
                
                //$form->get('dataFalecimento')->setValue($dados[0]->getDataFalecimento());
                $form->get('hrFalecimento')->setValue($dados[0]->getHoraFalecimento());
                $form->get('causaMortis')->setValue($dados[0]->getCausaMortis());
                $form->get('proaim')->setValue($dados[0]->getProaim());
                $form->get('destinoFinal')->setValue($dados[0]->getDestinoFinal());
                 //print_r($dados[0]->getMedico()->getNomeMedico());die;
                //if($dados[0]->getMedico()){
                  //  print_r($dados[0]->getMedico());die;
                   //$form->get('medico1')->setValue($dados[0]->getNomeFalecido());
                //$form->get('medico2')->setValue($dados[0]->getNomeFalecido()); 
               // }
                if($sepultamento){
                   
                    $form->get('cemiterio')->setValue($sepultamento[0]->getCemiterio()->getNome());
                    $form->get('endereco')->setValue($sepultamento[0]->getCemiterio()->getEndereco());
                    $form->get('hrSepultamento')->setValue($sepultamento[0]->getHoraSepultamento());
                }
                if($cremacao){
                   
                    $form->get('cemiterio')->setValue($cremacao[0]->getCemiterio()->getNome());
                    $form->get('endereco')->setValue($cremacao[0]->getCemiterio()->getEndereco());
                    $form->get('hrCremacao')->setValue($cremacao[0]->getHoraCremacao());
                }
                
                
                //$form->get('medico1')->setValue($dados[0]->getNomeFalecido());
                //$form->get('medico2')->setValue($dados[0]->getNomeFalecido());
                //$form->get('crm1')->setValue($dados[0]->getNomeFalecido());
                //$form->get('crm2')->setValue($dados[0]->getNomeFalecido());
                //$form->get('seraVelado')->setValue($dados[0]->getSeraVelado());
                //$form->get('localVelorio')->setValue($dados[0]->getLocalVelorio());
                //$form->get('enderecoVelorio')->setValue($dados[0]->getEnderecoVelorio());
                //$form->get('dtSaida')->setValue($dados[0]->getDtSaida());
                //$form->get('hrSaida')->setValue($dados[0]->getHrSaida());
                //$form->get('estado')->setValue($dados[0]->getEstado());
               // $form->get('crematorio')->setValue($dados[0]->getCrematorio());
                //$form->get('endereco')->setValue($dados[0]->getCrematorio()->getEndereco());
                //$form->get('dtCremacao')->setValue($dados[0]->getDtCremacao());
                //$form->get('hrCremacao')->setValue($dados[0]->getHrCremacao());
                $form->get('observacao')->setValue($dados[0]->getObservacao());
                 
            } else {
                $dados['id'] = '';
            }
          
        }

            if (!empty($this->jsFormulario)) {
                $this->layout()->setVariable('scripts', $this->jsFormulario);
            }
        return new ViewModel(array(
            'form' => $form, 
            'contratacao' => $contratacao, 
            'estados' => $estados,
            'medicos'=>$medicos,
            'falecimento'=>$falecimento,
            'localVelorio'=>$velorio,
            'cemiterio'=>$cemiterios,
            'crematorio'=>$crematorios,
            'aba' =>$abas,
             'controller'=> $this->controller
                ));
    }
    
     
    
    


}