<?php

namespace Agencia\Controller;

use Application\Controller\CrudApplicationController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container as SessionConteiner;

class ProdutoServicoController extends CrudApplicationController
{
    /**
     * @var string
     */
    protected $nameForm = 'Agencia\Form\ProdutoServico';
    /**
     * @var string
     */
    protected $nameRepository = 'Agencia\Repository\ProdutoVendidoRepository';
    /**
     * @var string
     */
    protected $nameService = 'Agencia\Service\ProdutoServico';
    /**
     * @var string
     */
    protected $controller = 'produtoServico';
    /**
     * @var string
     */
    protected $route = 'agencia/default';
    /**
     * @var array
     */
    protected $jsIndex = array('produtoServico/index.js');
    /**
     * @var array
     */
    protected $jsFormulario = array('produtoServico/formulario.js');

    /**
     * @return ViewModel
     */
    public function indexAction()
    {
        return new ViewModel();
    }

    /**
     * Método padrão de formulário
     * @return |Application\Controller\ViewModel
     */
    public function formularioAction()
    {
        //Instancia o formulário e a requisição
        $form = $this->getServiceLocator()->get($this->nameForm);
        $request = $this->getRequest();
        //Verificando de recebeu o dado da session
        $contratacao = new SessionConteiner('Contratacao');
        $abas = $this->getServiceLocator()->get('Agencia\Repository\ProcessoAbaRepository')->buscaAba(1, $contratacao->tipoOperacao, $contratacao->tipoContratacao, $contratacao->tipoDestinoFinal);
        //Verifica se a requisição foi feita via post
        if ($request->isPost()) {
            //Busca os dados
            $dados = $request->getPost()->toArray();
            if ($this->params()->fromRoute('id')) {
                $dados['id'] = $this->params()->fromRoute('id');
            }

            $form->setData($dados);
            //Valida o formulário
            if ($form->isValid() && $contratacao->id) {
                $service = $this->getServiceLocator()->get($this->nameService);
                $success = $service->save($dados);
                $this->setConfirmMessages($success);

                return $this->redirect()->toRoute($this->route, array('controller' => 'escolhaProduto', 'action' => 'formulario'));

            } else {
                $this->flashMessenger()->addErrorMessage('Por favor, verifique os campos do formulário');
            }

        } elseif ($contratacao->id) {
            $contratacaoReference = $this->getEm()->getReference('Agencia\Entity\Contratacao', ['id' => $contratacao->id]);
            //$dados = $this->getServiceLocator()->get($this->nameRepository)->findBy(['contratacao' => $contratacaoReference]);
            $arCampos = $form->getInputFilter()->getInputs();
            $arConvenio = $this->getServiceLocator()->get('Agencia\Repository\ConvenioRepository')->getArraySelect('Selecione um Convênio');
            $form->get('convenioSelect')->setValueOptions($arConvenio);
            //Buscando urnas
            $arUrna = $this->getServiceLocator()->get('Estoque\Repository\ProdutoRepository')->buscaUrna();
            $arSelectUrna = [];
            foreach ($arUrna as $key => $values) {
                $arSelectUrna[$values['id']] = $values['nome'];
            }
            $arSelectUrna = ['' => ''] + $arSelectUrna;
            $form->get('urna')->setValueOptions($arSelectUrna);
            //Buscando revestimento
            $arRevestimento = $this->getServiceLocator()->get('Estoque\Repository\ProdutoRepository')->buscaProdutoPorCategoria(5);
            $arRevestimentoSelect = [];
            foreach ($arRevestimento as $key => $values) {
                $arRevestimentoSelect[$values['id']] = $values['nome'];
            }
            $arRevestimentoSelect = ['' => ''] + $arRevestimentoSelect;
            $form->get('revestimento')->setValueOptions($arRevestimentoSelect);
            //Enfeite
            $arEnfeite = $this->getServiceLocator()->get('Estoque\Repository\ProdutoRepository')->buscaCategoriasEnfeite(6);
            $arEnfeiteSelect = [];
            foreach ($arEnfeite as $key => $values) {
                $arEnfeiteSelect[$values['id']] = $values['nome'];
            }
            $arEnfeiteSelect = ['' => ''] + $arEnfeiteSelect;
            $form->get('enfeite')->setValueOptions($arEnfeiteSelect);
            //Veu
            $arVeu = $this->getServiceLocator()->get('Estoque\Repository\ProdutoRepository')->buscaProdutoPorCategoria(7);
            $arVeuSelect = [];
            foreach ($arVeu as $key => $values) {
                $arVeuSelect[$values['id']] = $values['nome'];
            }
            $arVeuSelect = ['' => ''] + $arVeuSelect;
            $form->get('veu')->setValueOptions($arVeuSelect);
            //iluminacao
            $arIluminacao = $this->getServiceLocator()->get('Estoque\Repository\ProdutoRepository')->buscaProdutoPorCategoria(8);
            $arIluminacaoSelect = [];
            foreach ($arIluminacao as $key => $values) {
                $arIluminacaoSelect[$values['id']] = $values['nome'];
            }
            $arIluminacaoSelect = ['' => ''] + $arIluminacaoSelect;
            $form->get('iluminacao')->setValueOptions($arIluminacaoSelect);
            //Tipo de sepultamento
            $arTipoSepultamento = $this->getServiceLocator()->get('Agencia\Repository\ServicoRepository')->buscaServicoCategoria(1);
            $arTipoSepultamentoSelect = [];
            foreach ($arTipoSepultamento as $key => $values) {
                $arTipoSepultamentoSelect[$values['id']] = $values['nome'];
            }
            $arTipoSepultamentoSelect = ['' => ''] + $arTipoSepultamentoSelect;
            $form->get('tipoSepultamento')->setValueOptions($arTipoSepultamentoSelect);
            //TRANSPORTE



            //TAXA

        }

        //Seta os scripts e retorna para view
        if ($this->verifyPermission('contratacao', 'incluir')) {
            if (!empty($this->jsFormulario)) {
                $this->layout()->setVariable('scripts', $this->jsFormulario);
            }
        } else {
            //Seta os scripts e retorna para view
            $this->layout()->setVariable('scripts', array('produtoServico/formularioVisualiza.js'));
        }

        //Retorna os dados da view Model
        return new ViewModel([
            'form' => $form,
            'contratacao' => $contratacao,
            'aba' => $abas,
            'controller' => $this->controller
        ]);
    }

    public
    function buscaDadosProdutoAction()
    {
        $produto = '';
        $request = $this->getRequest()->getPost()->toArray();
        if (isset($request['produto'])) {
            $produto = $this->getServiceLocator()->get('Estoque\Repository\ProdutoRepository')->buscaDadosProduto($request['produto']);
        }
        if (isset($request['categoria'])) {
            $produto = $this->getServiceLocator()->get('Estoque\Repository\ProdutoRepository')->buscaProdutoEnfeite($request['categoria']);
        }
        if ($produto) {
            die(\Zend\Json\Json::encode($produto));
        } else {
            die(\Zend\Json\Json::encode(array()));
        }
    }

    public
    function buscaDadosServicoAction()
    {
        $servico = '';
        $request = $this->getRequest()->getPost()->toArray();
        if (isset($request['servico']) && isset($request['categoria'])) {
            $servico = $this->getServiceLocator()->get('Agencia\Repository\ServicoRepository')->buscaServicoCategoria($request['categoria'], $request['servico']);
        }
        if (isset($request['servico'])) {
            $servico = $this->getServiceLocator()->get('Agencia\Repository\ServicoRepository')->buscaDadosServico($request['servico']);
        }
        if ($servico) {
            die(\Zend\Json\Json::encode($servico));
        } else {
            die(\Zend\Json\Json::encode(array()));
        }
    }


}//END OF CLASS