<?php

namespace Agencia\Controller;

use Application\Controller\CrudApplicationController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container as SessionConteiner;

class ContratanteController extends CrudApplicationController
{
    /**
     * @var string
     */
    protected $nameForm = 'Agencia\Form\Contratante';
    /**
     * @var string
     */
    protected $nameRepository = 'Agencia\Repository\ContratanteRepository';
    /**
     * @var string
     */
    protected $nameService = 'Agencia\Service\Contratante';
    /**
     * @var string
     */
    protected $controller = 'contratante';
    /**
     * @var string
     */
    protected $route = 'agencia/default';
    /**
     * @var array
     */
    protected $jsIndex = array('contratante/index.js');
    /**
     * @var array
     */
    protected $jsFormulario = array('contratante/formulario.js');

    /**
     * @return ViewModel
     */
    public function indexAction()
    {
        return new ViewModel();
    }

    /**
     * Método padrão de formulário
     * @return |Application\Controller\ViewModel
     */
    public function formularioAction()
    {
        //Instancia o formulário e a requisição
        $form = $this->getServiceLocator()->get($this->nameForm);
        $request = $this->getRequest();
        $estados = [];
        //Verificando de recebeu o dado da session
        $contratacao = new SessionConteiner('Contratacao');
       
        $abas = $this->getServiceLocator()->get('Agencia\Repository\ProcessoAbaRepository')->buscaAba($contratacao->tipoOperacao, $contratacao->tipoContratacao, $contratacao->tipoDestinoFinal);
        
        
        if ($request->isPost()) {
            //Busca os dados
            $dados = $request->getPost()->toArray();
            if ($this->params()->fromRoute('id')) {
                $dados['id'] = $this->params()->fromRoute('id');
            }

            $form->setData($dados);
            //Valida o formulário
            if ($form->isValid() && $contratacao->id) {
                $service = $this->getServiceLocator()->get($this->nameService);
                $success = $service->save($dados);
                $this->setConfirmMessages($success);

                //return $this->redirect()->toRoute($this->route, array('controller' => 'falecido', 'action' => 'formulario'));
                return $this->redirect()->toRoute($this->route, array('controller' => 'contratacao', 'action' => 'index'));

            } else {
                $this->flashMessenger()->addErrorMessage('Por favor, verifique os campos do formulário');
            }

        } elseif ($contratacao->id) {

            $contratacaoReference = $this->getEm()->getReference('Agencia\Entity\Contratacao', ['id' => $contratacao->id]);
            $dados = $this->getServiceLocator()->get($this->nameRepository)->findBy(['contratacao' => $contratacaoReference]);

            if ($dados) {
                $dadosLocalizacao = $this->getServiceLocator()->get('Agencia\Repository\LocalizacaoRepository')->findBy(['contratante' => $dados[0]]);
                $dadosEmpresa = $this->getServiceLocator()->get('Agencia\Repository\DadosEmpresaRepository')->findOneBy(['contratante' => $dados[0]]);
                $estadoCidade = $this->getServiceLocator()->get('Application\Repository\CidadeRepository')->findBy(['id' => $dadosLocalizacao[0]->getCidade()]);
                $cidades = $this->getServiceLocator()->get('Application\Repository\CidadeRepository')->getArraySelect("Escolha uma Cidade", [], true, " where t.uf = " . $estadoCidade[0]->getUf()->getId() . " ");

                $form->get('nome')->setValue($dados[0]->getNomeContratante());
                $form->get('endereco')->setValue($dadosLocalizacao[0]->getEndereco());
                $form->get('bairro')->setValue($dadosLocalizacao[0]->getBairro());
                $form->get('numero')->setValue($dadosLocalizacao[0]->getNumero());
                $form->get('estado')->setValue($estadoCidade[0]->getId());
                $form->get('cidade')->setValueOptions($cidades);
                $form->get('cidade')->setValue($dadosLocalizacao[0]->getCidade());
                $form->get('estado')->setValue($estadoCidade[0]->getUf()->getId());
                $form->get('complemento')->setValue($dadosLocalizacao[0]->getComplemento());
                $form->get('cep')->setValue($dadosLocalizacao[0]->getCep());
                if ($dadosEmpresa) {
                    $form->get('empresa')->setValue($dadosEmpresa->getNomeEmpresarial());
                    $form->get('cnpj')->setValue($dadosEmpresa->getCnpj());
                    $form->get('enderecoEmpresa')->setValue($dadosEmpresa->getEndereco());
                    $form->get('telefoneEmpresa')->setValue($dadosEmpresa->getTelefone());
                    $form->get('representante')->setValue($dadosEmpresa->getRepresentante());
                    $form->get('empresaAcompanha')->setValue('S');
                }

                $form->setData($dados[0]->toArray());
            } else {
                $dados['id'] = '';
            }
        }

        //Seta os scripts e retorna para view
        if ($this->verifyPermission('contratacao', 'incluir')) {
            if (!empty($this->jsFormulario)) {
                $this->layout()->setVariable('scripts', $this->jsFormulario);
            }
        } else {
            //Seta os scripts e retorna para view
            $this->layout()->setVariable('scripts', array('contratacao/formularioVisualiza.js'));
        }

        //Retorna os dados da view Model
        return new ViewModel([
            'form' => $form,
            'contratacao' => $contratacao,
            'estados' => $estados,
            'aba' =>$abas,
            'controller'=> $this->controller
                
        ]);
    }

}