<?php


namespace Agencia\Controller;

use Application\Controller\CrudApplicationController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container as SessionConteiner;

class FalecidoController extends CrudApplicationController
{
    /**
     * @var string
     */
    protected $nameForm = 'Agencia\Form\Falecido';
    /**
     * @var string
     */
    protected $nameRepository = 'Agencia\Repository\FalecidoRepository';
    /**
     * @var string
     */
    protected $nameService = 'Agencia\Service\Falecido';
    /**
     * @var string
     */
    protected $controller = 'falecido';
    /**
     * @var string
     */
    protected $route = 'agencia/default';
    /**
     * @var array
     */
    protected $jsIndex = array('falecido/index.js');
    /**
     * @var array
     */
    protected $jsFormulario = array('falecido/formulario.js');

    /**
     * @return ViewModel
     */
    public function indexAction()
    {
        return new ViewModel();
    }

    /**
     * Método padrão de formulário
     * @return |Application\Controller\ViewModel
     */
    public function formularioAction()
    {
        //Instancia o formulário e a requisição
        $form = $this->getServiceLocator()->get($this->nameForm);
        $request = $this->getRequest();
        $estados = [];
        $raca = [];
        $arFilhos = [];
        $arCasamentos = [];
        //Verificando de recebeu o dado da session
        $contratacao = new SessionConteiner('Contratacao');
        $abas = $this->getServiceLocator()->get('Agencia\Repository\ProcessoAbaRepository')->buscaAba($contratacao->tipoOperacao, $contratacao->tipoContratacao, $contratacao->tipoDestinoFinal);
        //Verifica se a requisição foi feita via post
        if ($request->isPost()) {
            //Busca os dados
            $dados = $request->getPost()->toArray();
            if ($this->params()->fromRoute('id')) {
                $dados['id'] = $this->params()->fromRoute('id');
            }

            $form->setData($dados);
            //Valida o formulário
            if ($form->isValid() && $contratacao->id) {
                $service = $this->getServiceLocator()->get($this->nameService);
                $success = $service->save($dados);
                $this->setConfirmMessages($success);

                //return $this->redirect()->toRoute($this->route, array('controller' => 'falecido', 'action' => 'formulario'));
                return $this->redirect()->toRoute($this->route, array('controller' => 'dadosObito', 'action' => 'formulario'));

            } else {
               
                $this->flashMessenger()->addErrorMessage('Por favor, verifique os campos do formulário');
            }

        } elseif ($contratacao->id) {
            $contratacaoReference = $this->getEm()->getReference('Agencia\Entity\Contratacao', ['id' => $contratacao->id]);
            $dados = $this->getServiceLocator()->get($this->nameRepository)->findOneBy(['contratacao' => $contratacaoReference]);
            if ($dados) {
                $dadosFilhos = $this->getServiceLocator()->get('Agencia\Repository\FilhoFalecidoRepository')->findBy(['falecido' => $dados]);
                $dadosLocalizacao = $this->getServiceLocator()->get('Agencia\Repository\LocalizacaoRepository')->findOneBy(['falecido' => $dados]);

                foreach ($dadosFilhos as $filho) {
                    $arAux = $filho->toArray();
                    unset($arAux['falecido']);
                    unset($arAux['dataCadastro']);
                    $arFilhos[] = $arAux;
                }
                $dadosCasamentos = $this->getServiceLocator()->get('Agencia\Repository\CasamentoRepository')->findBy(['falecido' => $dados]);
                foreach ($dadosCasamentos as $casamento) {
                    $arAux = $casamento->toArray();
                    unset($arAux['falecido']);
                    unset($arAux['dataCadastro']);
                    $arCasamentos[] = $arAux;
                }

                $dados = $dados->toArray();
                unset($dados['falecido']);

                $dados['estadoCivilMae'] = $dados['estadoCivilMae']->getId();
                $dados['estadoCivilPai'] = $dados['estadoCivilPai']->getId();
                $dados['deixaFilhos'] = $dados['deixaFilho'];
                $dados['raca'] = $dados['cor'];

                $form->get('numeroDocumento')->setValue($dados['rg']);
                $form->get('nascimentoObito')->setValue($dados['natMorto']);
                $form->get('estadoCivil')->setValue($dados['estadoCivil']);

                if ($dadosLocalizacao) {
                    $estadoCidade = $this->getServiceLocator()->get('Application\Repository\CidadeRepository')->findBy(['id' => $dadosLocalizacao->getCidade()]);
                    $cidades = $this->getServiceLocator()->get('Application\Repository\CidadeRepository')->getArraySelect("Escolha uma Cidade", [], true, " where t.uf = " . $estadoCidade[0]->getUf()->getId() . " ");
                    $form->get('cidade')->setValueOptions($cidades);
                    $form->get('cidade')->setValue($dadosLocalizacao->getCidade());
                    $form->get('estado')->setValue($estadoCidade[0]->getUf()->getId());
                    $dados['endereco'] = $dadosLocalizacao->getEndereco();
                    $dados['bairro'] = $dadosLocalizacao->getBairro();
                    $dados['numero'] = $dadosLocalizacao->getNumero();
                    $dados['complemento'] = $dadosLocalizacao->getComplemento();
                    $dados['cep'] = $dadosLocalizacao->getCep();
                }
                $form->setData($dados);
            }

        } else if ($this->params()->fromRoute('id')) {

            $form->get('nascimentoObito')->setValue('N');
        }

        //Seta os scripts e retorna para view
        if ($this->verifyPermission('contratacao', 'incluir')) {
            if (!empty($this->jsFormulario)) {
                $this->layout()->setVariable('scripts', $this->jsFormulario);
            }
        } else {
            //Seta os scripts e retorna para view
            $this->layout()->setVariable('scripts', array('falecido/formularioVisualiza.js'));
        }

        //Retorna os dados da view Model
        return new ViewModel([
            'form' => $form,
            'contratacao' => $contratacao,
            'estado' => $estados,
            'raca' => $raca,
            'filhos' => $arFilhos,
            'casamentos' => $arCasamentos,
            'aba' =>$abas,
             'controller'=> $this->controller

        ]);
    }


}