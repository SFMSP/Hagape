<?php

namespace Agencia\Controller;

use Application\Controller\CrudApplicationController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container as SessionConteiner;

class PacienteController extends CrudApplicationController
{
    /**
     * @var string
     */
    protected $nameForm = 'Agencia\Form\Paciente';
    /**
     * @var string
     */
    protected $nameRepository = 'Agencia\Repository\PacienteRepository';
    /**
     * @var string
     */
    protected $nameService = 'Agencia\Service\Paciente';
    /**
     * @var string
     */
    protected $controller = 'paciente';
    /**
     * @var string
     */
    protected $route = 'agencia/default';
    /**
     * @var array
     */
    protected $jsIndex = array('paciente/index.js');
    /**
     * @var array
     */
    protected $jsFormulario = array('paciente/formulario.js');

   
    /**
     * Método padrão de formulário
     * @return |Application\Controller\ViewModel
     */
    public function formularioAction()
    {
        //Instancia o formulário e a requisição
        $form = $this->getServiceLocator()->get($this->nameForm);
        $request = $this->getRequest();
        //Verificando de recebeu o dado da session
        $contratacao = new SessionConteiner('Contratacao');
        $abas = $this->getServiceLocator()->get('Agencia\Repository\ProcessoAbaRepository')->buscaAba(1, $contratacao->tipoOperacao, $contratacao->tipoContratacao, $contratacao->tipoDestinoFinal);
        //Verifica se a requisição foi feita via post
        if ($request->isPost()) {
            //Busca os dados
            $dados = $request->getPost()->toArray();
            if ($this->params()->fromRoute('id')) {
                $dados['id'] = $this->params()->fromRoute('id');
            }

            $form->setData($dados);
            //Valida o formulário
            if ($form->isValid() && $contratacao->id) {
                $service = $this->getServiceLocator()->get($this->nameService);
                $success = $service->save($dados);
                $this->setConfirmMessages($success);

                //return $this->redirect()->toRoute($this->route, array('controller' => 'falecido', 'action' => 'formulario'));
                return $this->redirect()->toRoute($this->route, array('controller' => 'paciente', 'action' => 'formulario'));

            } else {
                $this->flashMessenger()->addErrorMessage('Por favor, verifique os campos do formulário');
            }

        } elseif ($contratacao->id) {
            $contratacaoReference = $this->getEm()->getReference('Agencia\Entity\Contratacao', ['id' => $contratacao->id]);
            $dados = $this->getServiceLocator()->get($this->nameRepository)->findBy(['contratacao' => $contratacaoReference]);

            if ($dados) {
                
                 $form->get('pecasAnatomicas')->setValue($dados[0]->getPecaAnatomica());
                 $form->get('nome')->setValue($dados[0]->getNomePaciente());
                 $form->get('membroAmputado')->setValue($dados[0]->getMembroAmputado());
                 
             } else {
                $dados['id'] = '';
            }
            
        }

        //Seta os scripts e retorna para view
        if ($this->verifyPermission('contratacao', 'incluir')) {
            if (!empty($this->jsFormulario)) {
                $this->layout()->setVariable('scripts', $this->jsFormulario);
            }
        } else {
            //Seta os scripts e retorna para view
            $this->layout()->setVariable('scripts', array('contratacao/formularioVisualiza.js'));
        }

        //Retorna os dados da view Model
        return new ViewModel([
            'form' => $form,
            'contratacao' => $contratacao,
            'aba' =>$abas,
            'controller'=> $this->controller
            
        ]);
    }

}