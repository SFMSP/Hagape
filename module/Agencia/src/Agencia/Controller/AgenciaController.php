<?php


namespace Agencia\Controller;
use Application\Controller\CrudApplicationController;
use Zend\View\Model\ViewModel;

class AgenciaController extends CrudApplicationController
{
    public function indexAction()
    {
        return new ViewModel();
    }
}