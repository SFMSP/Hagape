<?php

namespace Agencia\Controller;

use Application\Controller\CrudApplicationController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container as SessionConteiner;

class DadosAmputacaoController extends CrudApplicationController
{
    /**
     * @var string
     */
    protected $nameForm = 'Agencia\Form\DadosAmputacao';
    /**   
     * @var string
     */
    protected $nameRepository = 'Agencia\Repository\DadosAmputacaoRepository';
    /**
     * @var string
     */
    protected $nameService = 'Agencia\Service\DadosAmputacao';
    /**
     * @var string
     */
    protected $controller = 'dadosAmputacao';
    /**
     * @var string
     */
    protected $route = 'agencia/default';
    /**
     * @var array
     */
    protected $jsIndex = array('dadosAmputacao/index.js');
    /**
     * @var array
     */
    protected $jsFormulario = array('dadosAmputacao/formulario.js');

    /**
     * @return ViewModel
     */
   

    /**
     * Método padrão de formulário
     * @return |Application\Controller\ViewModel
     */
    public function formularioAction()
    {
        //Instancia o formulário e a requisição
        $form = $this->getServiceLocator()->get($this->nameForm);
        $request = $this->getRequest();
       
        $estados = [];
        $velorio=[];
        $falecimento=[];
        $medicos=[];
        $cemiterios=[];
        $crematorios=[];
        //Verificando de recebeu o dado da session
        $contratacao = new SessionConteiner('Contratacao');
        $abas = $this->getServiceLocator()->get('Agencia\Repository\ProcessoAbaRepository')->buscaAba($contratacao->tipoOperacao, $contratacao->tipoContratacao, $contratacao->tipoDestinoFinal);

        //Verifica se a requisição foi feita via post
        if ($request->isPost()) {
            //Busca os dados
            $dados = $request->getPost()->toArray();
            
            if ($this->params()->fromRoute('id')) {
                $dados['id'] = $this->params()->fromRoute('id');
            }
            
            $form->setData($dados);
            //Valida o formulário
            if ($form->isValid()) {
                $service = $this->getServiceLocator()->get($this->nameService);
               
                $success = $service->save($dados);
                $this->setConfirmMessages($success);

                return $this->redirect()->toRoute($this->route, array('controller' => 'dadosobito', 'action' => 'formulario'));
               // return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            } else {
          
                $this->flashMessenger()->addErrorMessage('Por favor, verifique os campos do formulário');
            }


        } elseif ($contratacao->id) {
          
            $velorio =      $this->getServiceLocator()->get('Agencia\Repository\VelorioRepository')->buscaArrayInputFilter();
            $falecimento =  $this->getServiceLocator()->get('Agencia\Repository\LocalFalecimentoRepository')->buscaArrayInputFilter();
            $medicos =      $this->getServiceLocator()->get('Agencia\Repository\MedicoRepository')->buscaArrayInputFilter();
            $cemiterios =   $this->getServiceLocator()->get('Admin\Repository\CemiterioRepository')->buscaArrayInputFilter(1);
            $crematorios =  $this->getServiceLocator()->get('Admin\Repository\CemiterioRepository')->buscaArrayInputFilter(2);
            $paciente = $this->getServiceLocator()->get('Agencia\Repository\PacienteRepository')->findBy(['contratacao' => $contratacao->id]);
            $dados = $this->getServiceLocator()->get($this->nameRepository)->findBy(['paciente' => $paciente]);
            if ($dados) {
              
                 $form->get('causaAmputacao')->setValue($dados[0]->getCausaAmputacao());
                 $form->get('localAmputacao')->setValue($dados[0]->getLocalAmputacao());
                 $form->get('cemiterio')->setValue($dados[0]->getCemiterio());
                 $form->get('enderecoCemiterio')->setValue($dados[0]->getCemiterio()->getEndereco());
                 //$form->get('dtSepultamento')->setValue($dados[0]->getCemiterioDestino());
                 //$form->get('hrSepultamento')->setValue($dados[0]->getHoraCremacao());
                 $form->get('crematorio')->setValue($dados[0]->getCrematorio());
                 $form->get('enderecoCrematorio')->setValue($dados[0]->getCrematorio()->getEndereco());
                 //$form->get('dtCremacao')->setValue($dados[0]->getCemiterioDestino()->getEndereco());
                // $form->get('hrCremacao')->setValue($dados[0]->getCemiterioDestino()->getEndereco());
                 //$form->get('medico1')->setValue($dados[0]->getCemiterioDestino()->getEndereco());
                 //$form->get('crm1')->setValue($dados[0]->getCemiterioDestino()->getEndereco());
                 
                             
            } else {
                $dados['id'] = '';
            }
            
            
            
          
        }

            if (!empty($this->jsFormulario)) {
                $this->layout()->setVariable('scripts', $this->jsFormulario);
            }
        return new ViewModel(array(
            'form' => $form, 
            'contratacao' => $contratacao, 
            'estados' => $estados,
            'medicos'=>$medicos,
            'falecimento'=>$falecimento,
            'localVelorio'=>$velorio,
            'cemiterio'=>$cemiterios,
            'crematorio'=>$crematorios,
            'aba' =>$abas,
             'controller'=> $this->controller
                ));
    }
    
     
    
    


}