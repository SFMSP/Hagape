<?php

namespace Agencia\Controller;

use Application\Controller\CrudApplicationController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container as SessionConteiner;

class TransladoCorpoController extends CrudApplicationController
{
    /**
     * @var string
     */
    protected $nameForm = 'Agencia\Form\TransladoCorpo';
    /**   
     * @var string
     */
    protected $nameRepository = 'Agencia\Repository\TransladoCorpoRepository';
    /**
     * @var string
     */
    protected $nameService = 'Agencia\Service\TransladoCorpo';
    /**
     * @var string
     */
    protected $controller = 'transladoCorpo';
    /**
     * @var string
     */
    protected $route = 'agencia/default';
    /**
     * @var array
     */
    protected $jsIndex = array('transladoCorpo/index.js');
    /**
     * @var array
     */
    protected $jsFormulario = array('transladocorpo/formulario.js');

    /**
     * @return ViewModel
     */
   

    /**
     * Método padrão de formulário
     * @return |Application\Controller\ViewModel
     */
    public function formularioAction()
    {
        //Instancia o formulário e a requisição
        $form = $this->getServiceLocator()->get($this->nameForm);
        $request = $this->getRequest();
        $cemiterios=[];
        $crematorios=[];
        //Verificando de recebeu o dado da session
        $contratacao = new SessionConteiner('Contratacao');
        $abas = $this->getServiceLocator()->get('Agencia\Repository\ProcessoAbaRepository')->buscaAba($contratacao->tipoOperacao, $contratacao->tipoContratacao, $contratacao->tipoDestinoFinal);

        //Verifica se a requisição foi feita via post
        if ($request->isPost()) {
            //Busca os dados
            $dados = $request->getPost()->toArray();
            
            if ($this->params()->fromRoute('id')) {
                $dados['id'] = $this->params()->fromRoute('id');
            }
            
            $form->setData($dados);
            //Valida o formulário
            if ($form->isValid()) {
                $service = $this->getServiceLocator()->get($this->nameService);
               
                $success = $service->save($dados);
                $this->setConfirmMessages($success);

                return $this->redirect()->toRoute($this->route, array('controller' => 'contratacao', 'action' => 'index'));
               // return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            } else {
          
                $this->flashMessenger()->addErrorMessage('Por favor, verifique os campos do formulário');
            }


        } elseif ($contratacao->id) {
          
            $cemiterios =   $this->getServiceLocator()->get('Admin\Repository\CemiterioRepository')->buscaArrayInputFilter(1);
            $crematorios =  $this->getServiceLocator()->get('Admin\Repository\CemiterioRepository')->buscaArrayInputFilter(2);
            $contratacaoReference = $this->getEm()->getReference('Agencia\Entity\Contratacao', ['id' => $contratacao->id]);
            $dados = $this->getServiceLocator()->get($this->nameRepository)->findBy(['contratacao' => $contratacaoReference]);

            if ($dados) {
                
                 $form->get('dtSaida')->setValue($dados[0]->getDataSaida()->format('d/m/Y'));
                 $form->get('enderecoCemiterio')->setValue($dados[0]->getCemiterioSaida()->getEndereco());
                 $form->get('hrSepultamento')->setValue($dados[0]->getHoraCremacao());
                 $form->get('enderecoCemiterioDestino')->setValue($dados[0]->getCemiterioDestino()->getEndereco());
                 $form->setData($dados[0]->toArray());
            } else {
                $dados['id'] = '';
            }
        }

            if (!empty($this->jsFormulario)) {
                $this->layout()->setVariable('scripts', $this->jsFormulario);
            }
        return new ViewModel(array(
            'form' => $form, 
            'contratacao' => $contratacao,
            'cemiterio'=>$cemiterios,
            'crematorio'=>$crematorios,
            'aba' =>$abas,
            'controller'=> $this->controller
       
                ));
    }
    
     
    
    


}