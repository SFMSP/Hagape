<?php

namespace Agencia\Controller;

use Application\Controller\CrudApplicationController;
use Zend\View\Model\ViewModel;
use Base\Helper\DatetimeFormat;
use Base\Helper\FloatFormat;
use Zend\Session\Container as SessionConteiner;

class EmissaoController extends CrudApplicationController
{
    /**
     * @var string
     */
    protected $nameForm = 'Agencia\Form\Emissao';
    /**
     * @var string
     */
    protected $nameRepository = 'Agencia\Repository\EmissaoRepository';
    /**
     * @var string
     */
    protected $nameService = 'Agencia\Service\Emissao';
    /**
     * @var string
     */
    protected $controller = 'emissao';
    /**
     * @var string
     */
    protected $route = 'agencia/default';
    /**
     * @var array
     */
    protected $jsIndex = array('emissao/index.js');
    /**
     * @var array
     */
    protected $jsFormulario = array('emissao/formulario.js');

    /**
     * @return ViewModel
     */
    public function indexAction()
    {
        return new ViewModel();
    }

    /**
     * Método padrão de formulário
     * @return |Application\Controller\ViewModel
     */
    public function formularioAction()
    {
        //Instancia o formulário e a requisição
        $form = $this->getServiceLocator()->get($this->nameForm);
        $request = $this->getRequest();
        $estados = [];
        
        //Verificando de recebeu o dado da session
        $contratacao = new SessionConteiner('Contratacao');
        $abas = $this->getServiceLocator()->get('Agencia\Repository\ProcessoAbaRepository')->buscaAba(1, $contratacao->tipoOperacao, $contratacao->tipoContratacao, $contratacao->tipoDestinoFinal);
        
        //Verifica se a requisição foi feita via post
        if ($request->isPost()) {
//            primei   
        } elseif ($contratacao->id) {
//            edicao
        }

        //Seta os scripts e retorna para view
        if ($this->verifyPermission('contratacao', 'incluir')) {
            if (!empty($this->jsFormulario)) {
                $this->layout()->setVariable('scripts', $this->jsFormulario);
            }
        } else {
            //Seta os scripts e retorna para view
            $this->layout()->setVariable('scripts', array('contratacao/formularioVisualiza.js'));
        }

        //Retorna os dados da view Model
        return new ViewModel([
            'form' => $form,
            'contratacao' => $contratacao,
            'aba' =>$abas,
            'controller'=> $this->controller
        ]);
    }

    
    public function emitirNotaAction(){
        
        //Instancia o helper de formatação de data e o de valores float
        $dateTimeFormat = new DatetimeFormat();
        $floatFormat = new FloatFormat();

        //Realiza a busca 
        $id = $this->params()->fromRoute('id');
        $relatorio          = $this->getServiceLocator()->get('Agencia\Repository\ContratacaoRepository')->buscarDadosNotas($id);
        $agencia            = $this->getServiceLocator()->get('Admin\Repository\AgenciaRepository')->find($relatorio->getAgencia());
        $contratante        = $this->getServiceLocator()->get('Agencia\Repository\ContratanteRepository')->findOneBy(array('contratacao' => $relatorio->getId()));
        $declarante         = $this->getServiceLocator()->get('Agencia\Repository\DeclaranteRepository')->findOneBy(array('contratacao' => $relatorio->getId()));
        $localizacao        = $this->getServiceLocator()->get('Agencia\Repository\LocalizacaoRepository')->findOneBy(array('contratante' => $contratante->getId()));
        $falecido           = $this->getServiceLocator()->get('Agencia\Repository\FalecidoRepository')->findOneBy(array('contratacao' => $relatorio->getId()));
        $dadosObito         = $this->getServiceLocator()->get('Agencia\Repository\DadosObitoRepository')->findOneBy(array('falecido' => $falecido->getId()));
        $cartorio           = $dadosObito->getCartorio();
        $localFalecimento   = $this->getServiceLocator()->get('Agencia\Repository\LocalFalecimentoRepository')->findOneBy(array('cartorio' => $cartorio->getId()));
        $dadosEmpresa       = $this->getServiceLocator()->get('Agencia\Repository\DadosEmpresaRepository')->findOneBy(array('contratacao' => $relatorio->getId()));
        $dadosObitoVelorio  = $this->getServiceLocator()->get('Agencia\Repository\DadosObitoVelorioRepository')->findOneBy(array('contratacao' => $relatorio->getId()));
        $dadosVelorio       = $this->getServiceLocator()->get('Agencia\Repository\VelorioRepository')->findOneBy(array('contratacao' => $relatorio->getId()));
        $paciente           = $this->getServiceLocator()->get('Agencia\Repository\PacienteRepository')->findOneBy(array('contratacao' => $relatorio->getId()));
        $dadosAmputacao     = $this->getServiceLocator()->get('Agencia\Repository\DadosAmputacaoRepository')->findOneBy(array('paciente' => $paciente->getId()));
        $medico             = $dadosObito->getMedico(); 
        $medico             = $medico[0];
        $sepultamento       =  $this->getServiceLocator()->get('Agencia\Repository\SepultamentoRepository')->findOneBy(array('contratacao' => $relatorio->getId()));
        $cremacao           =  $this->getServiceLocator()->get('Agencia\Repository\CremacaoRepository')->findOneBy(array('contratacao' => $relatorio->getId()));
        $cemiterio          =  $this->getServiceLocator()->get('Admin\Repository\CemiterioRepository')->find($sepultamento->getCemiterio());
        
        //ITENS
        $vendaProduto       = $this->getServiceLocator()->get('Agencia\Repository\VendaProdutoServicoRepository')->findOneBy(array('contratacao' => $relatorio->getId()));
        $produtosVendidos   = $this->getServiceLocator()->get('Agencia\Repository\ProdutoVendidoRepository')->findBy(array('vendaProduto' => $vendaProduto->getId()));
        $servicosVendidos   = $this->getServiceLocator()->get('Agencia\Repository\ServicoVendidoRepository')->findBy(array('vendaProduto' => $vendaProduto->getId()));
        $pagamentos         = $this->getServiceLocator()->get('Agencia\Repository\PagamentoRepository')->findBy(array('contratacao' => $relatorio->getId()));
        
        $arrayChaveValorCategoria = array();
        $arrayServicos            = array();
        
        foreach ($servicosVendidos as $registro) {
            $arrayChaveValorCategoria[$registro->getServico()->getCategoria()->getId()] = $registro->getServico()->getCategoria()->getNome();
            $arrayServicos[$registro->getServico()->getCategoria()->getId()][] = $registro;
        }
        
        //Service para gerar WORD
        $word = $this->getServiceLocator()->get('Agencia\WordService\Notas');
        
        $nome_template = $this->getServiceLocator()->get('Agencia\Repository\ContratacaoRepository')->buscarTemplate($id);
        $nome_template  = $nome_template ? $nome_template[0]["txt_nome_template"] : "template_nota.docx";
        
//        echo $nome_template;
//        die();
//        
        $arquivo = $word->setTemplate($nome_template)->gerarRelatorioNotas(
            $dateTimeFormat,
            $relatorio,
            $agencia,
            $contratante,
            $declarante,
            $localizacao,
            $falecido,
            $dadosObito,
            $localFalecimento,
            $dadosEmpresa,
            $dadosObitoVelorio,
            $dadosVelorio,
            $paciente,
            $dadosAmputacao,
            $medico,
            $sepultamento,
            $cemiterio,
            $cremacao,
            $produtosVendidos,
            $servicosVendidos,
            $arrayChaveValorCategoria,
            $arrayServicos,
            $vendaProduto,
            $pagamentos
        );

        $sessionContainer = new \Zend\Session\Container();
        $sessionContainer->arquivo_salvo = $arquivo;

//        die(\Zend\Json\Json::encode(array('arquivo' => $arquivo)));
        
        // Configuramos os headers que serão enviados para o browser
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="' . basename($arquivo) . '"');
        header('Content-Type: application/octet-stream');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($arquivo));
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Expires: 0');

        // Envia o arquivo para o cliente
        readfile($arquivo);
        die();
        
        
    }
    
}