<?php

namespace Agencia\Controller;

use Application\Controller\CrudApplicationController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container as SessionConteiner;

class PagamentoController extends CrudApplicationController
{
    /**
     * @var string
     */
    protected $nameForm = 'Agencia\Form\Pagamento';
    /**   
     * @var string
     */
    protected $nameRepository = 'Agencia\Repository\PagamentoRepository';
    /**
     * @var string
     */
    protected $nameService = 'Agencia\Service\Pagamento';
    /**
     * @var string
     */
    protected $controller = 'pagamento';
    /**
     * @var string
     */
    protected $route = 'agencia/default';
    /**
     * @var array
     */
    protected $jsIndex = array('pagamento/index.js');
    /**
     * @var array
     */
    protected $jsFormulario = array('pagamento/formulario.js');

    /**
     * @return ViewModel
     */
   

    /**
     * Método padrão de formulário
     * @return |Application\Controller\ViewModel
     */
    public function formularioAction()
    {
        //Instancia o formulário e a requisição
        $form = $this->getServiceLocator()->get($this->nameForm);
        $request = $this->getRequest();
       
        $estados = [];
        $velorio=[];
        $falecimento=[];
        $medicos=[];
        $cemiterios=[];
        $crematorios=[];
        $pag=[];
        //Verificando de recebeu o dado da session
        $contratacao = new SessionConteiner('Contratacao');
        $abas = $this->getServiceLocator()->get('Agencia\Repository\ProcessoAbaRepository')->buscaAba($contratacao->tipoOperacao, $contratacao->tipoContratacao, $contratacao->tipoDestinoFinal);

        //Verifica se a requisição foi feita via post
        if ($request->isPost()) {
            //Busca os dados
            $dados = $request->getPost()->toArray();
            
            if ($this->params()->fromRoute('id')) {
                $dados['id'] = $this->params()->fromRoute('id');
            }
            
            $form->setData($dados);
            //Valida o formulário
            if ($form->isValid()) {
                $service = $this->getServiceLocator()->get($this->nameService);
             
                $success = $service->save($dados);
                $this->setConfirmMessages($success);

                return $this->redirect()->toRoute($this->route, array('controller' => 'pagamento', 'action' => 'formulario'));
               // return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            } else {
          
                $this->flashMessenger()->addErrorMessage('Por favor, verifique os campos do formulário');
            }


        } elseif ($contratacao->id) {
          
            $velorio =      $this->getServiceLocator()->get('Agencia\Repository\VelorioRepository')->buscaArrayInputFilter();
            $falecimento =  $this->getServiceLocator()->get('Agencia\Repository\LocalFalecimentoRepository')->buscaArrayInputFilter();
            $medicos =      $this->getServiceLocator()->get('Agencia\Repository\MedicoRepository')->buscaArrayInputFilter();
            $cemiterios =   $this->getServiceLocator()->get('Admin\Repository\CemiterioRepository')->buscaArrayInputFilter(1);
            $crematorios =  $this->getServiceLocator()->get('Admin\Repository\CemiterioRepository')->buscaArrayInputFilter(2);
            
            $contratacaoReference = $this->getEm()->getReference('Agencia\Entity\Contratacao', ['id' => $contratacao->id]);
            $dados = $this->getServiceLocator()->get($this->nameRepository)->findBy(['contratacao' => $contratacaoReference]);

            if ($dados) {
                
                $pag = array();
                for($i = 0; $i < count($dados); $i++){
                    $pag['tbEmitente'][$i] = $dados[$i]->getEmitente();
                    $pag['tbBandeira'][$i] = $dados[$i]->getBandeira();
                    $pag['tbDocumento'][$i] = $dados[$i]->getNumeroDocumento();
                    $pag['tbTipoPagamento'][$i] = $dados[$i]->getTipoPagamento();
                    $pag['tbPinpad'][$i] = $dados[$i]->getPinpad();
                    $pag['tbValor'][$i] = $dados[$i]->getValorPagamento();
                    $pag['tbNome'][$i] = $dados[$i]->getNome();
                    $pag['tbTelefone'][$i] = $dados[$i]->getTelefone();
                    $pag['tbParcelamento'][$i] = $dados[$i]->getParcelamento();
                    $pag['tbAutorizacao'][$i] = $dados[$i]->getAutorizacao();
                    $pag['tbEndereco'][$i] = $dados[$i]->getEndereco();
                    $pag['tbCvdoc'][$i] = $dados[$i]->getEndereco();
                       
                    
                   
                }
                
              
             } else {
                $dados['id'] = '';
            }
            
          
            }
            //$situacao = $this->getEm()->getReference('Agencia\Entity\Contratacao', ['id' => $contratacao->id])->getSituacao();
            //if($situacao==1){
              //  $this->layout()->setVariable('scripts', array('pagamento/formularioVisualiza.js')); 
            //}else{
                if (!empty($this->jsFormulario)) {

                 $this->layout()->setVariable('scripts', $this->jsFormulario);
                } 
           // }
            
        return new ViewModel(array(
            'form' => $form, 
            'contratacao' => $contratacao, 
            'estados' => $estados,
            'medicos'=>$medicos,
            'falecimento'=>$falecimento,
            'localVelorio'=>$velorio,
            'cemiterio'=>$cemiterios,
            'crematorio'=>$crematorios,
            'aba' =>$abas,
            'controller'=> $this->controller,
            'pagamentos'=>$pag,
                ));
    }
    
     
    
    


}