<?php


namespace Agencia\Controller;

use Application\Controller\CrudApplicationController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container as SessionConteiner;


/**
 * Class ContratacaoController
 * @package Agencia\Controller
 */
class ContratacaoController extends CrudApplicationController
{
    /**
     * @var string
     */
    protected $nameForm = 'Agencia\Form\Contratacao';
    /**
     * @var string
     */
    protected $nameRepository = 'Agencia\Repository\ContratacaoRepository';
    /**
     * @var string
     */
    protected $nameService = 'Agencia\Service\Contratacao';
    /**
     * @var string
     */
    protected $controller = 'contratacao';
    /**
     * @var string
     */
    protected $route = 'agencia/default';
    /**
     * @var array
     */
    protected $jsIndex = array('contratacao/index.js');
    /**
     * @var array
     */
    protected $jsFormulario = array('contratacao/formulario.js');
    /**
     * @var array
     */
    protected $nameFormFilter = 'Agencia\Form\AgenciaFiltro';

    /**
     * @return ViewModel
     */
   
    /**
     * Método padrão de formulário
     * @return |Application\Controller\ViewModel
     */
    public function formularioAction()
    {

        //Instancia o formulário e a requisição
        $form = $this->getServiceLocator()->get($this->nameForm);
        $request = $this->getRequest();
        $operacao = array();
        $destino = array();
       
        //Verifica se a requisição foi feita via post
        if ($request->isPost()) {
            //Busca os dados
            $dados = $request->getPost()->toArray();
            if ($this->params()->fromRoute('id')) {
                $dados['id'] = $this->params()->fromRoute('id');
            }

            $form->setData($dados);
            //Valida o formulário
            if ($form->isValid()) {
                $service = $this->getServiceLocator()->get($this->nameService);
                $service->save($dados);

                //Caso for registro de obito
                if($dados['operacao'] == '3') {
                    return $this->redirect()->toRoute($this->route, array('controller' => 'declarante', 'action' => 'formulario'));
                }else{
                    return $this->redirect()->toRoute($this->route, array('controller' => 'contratante', 'action' => 'formulario'));
                }

            } else {
                $this->flashMessenger()->addErrorMessage('Por favor, verifique os campos do formulário');
            }

        //Vem da URL ou Edição
        } elseif ($this->params()->fromRoute('id')) {
            
            //Busca dados Contratacao
            $dados = $this->getServiceLocator()->get($this->nameRepository)->find($this->params()->fromRoute('id'));
           
            //Carrega os dados pra dentro da session
            $this->loadSessionContainer('Contratacao', $dados);
            return $this->redirect()->toRoute($this->route, array('controller' => 'contratante', 'action' => 'formulario'));
        }

        //Seta os scripts e retorna para view
        if ($this->verifyPermission('contratacao', 'incluir')) {
            if (!empty($this->jsFormulario)) {
                $this->layout()->setVariable('scripts', $this->jsFormulario);
            }
        } else {
            //Seta os scripts e retorna para view
            $this->layout()->setVariable('scripts', array('contratacao/formularioVisualiza.js'));
        }

        //Retorna os dados da view Model
        return new ViewModel(array('form' => $form, 'operacao' => $operacao, 'destino' => $destino));
    }

    public function buscaTipoContratacaoAction()
    {
        $idTipoOperacao = $this->getRequest()->getPost('tipoOperacao');
        $arFiltro = [];
        if ($idTipoOperacao) {
            switch ($idTipoOperacao) {
                case 1:
                    $arFiltro = ['1', '2', '3', '4'];
                    break;
                case 2:
                    $arFiltro = ['5', '6', '7'];
                    break;
                default:
                    $arFiltro = [];
                    break;
            }
        }

        $arTipoContratacao = $this->getServiceLocator()->get('Agencia\Repository\TipoContratacaoRepository')->findBy(['id' => $arFiltro]);
        $array = [];
        if (is_array($arTipoContratacao)) {
            foreach ($arTipoContratacao as $key => $value) {
                $array[$key]['id'] = $value->getId();
                $array[$key]['nome'] = $value->getTipoContratacao();
            }
        }
        echo \Zend\Json\Json::encode($array);
        die();
    }

    public function loadSessionContainer($nome = '', $dados = '')
    {
        
       
        if ($nome && $dados) {
             
            $contratacao = new SessionConteiner($nome);
            
             if($dados->getTipoOperacao()->getId()==3){
                 
                 $contratacao->tipoContratacao ='';
                 $contratacao->destinoFinal='';
                 
             }else{
                 $contratacao->tipoContratacao = $dados->getTipoContratacao()->getId();
                 $contratacao->destinoFinal = $dados->getTipoDestinoFinal()->getId();
             }
             
            
            $contratacao->id = $dados->getId();
            $contratacao->numeroNota = $dados->getNumeroNota();
            $contratacao->usuario = $dados->getUsuario();
            $contratacao->agencia = $dados->getAgencia();
            $contratacao->totalContratacao = $dados->getTotalContratacao();
            $contratacao->contratacaoOrigem = $dados->getContratacaoOrigem();
            $contratacao->tipoOperacao = $dados->getTipoOperacao()->getId();
            
        }
    }

    public function paginacaoAction() {
        //Armazena a resposta
        $response = $this->getResponse();
        
        //Armazena a requisição
        $request = $this->getRequest()->getPost();
       
        //Organiza os dados da busca
        $busca = array(
            "busca" => mb_strtolower($request['search']['value'], 'UTF-8'),
            "agencia" => $request['agencia'],
            "filtroData" =>$request['filtroData']
                
           );
          
        //Cria a ordenação
        $order = array();

        if (isset($request['order'][0]['column'])) {
            switch ($request['order'][0]['column']) {
                case 1: $order = array('field' => 'numeroNota', 'order' => $request['order'][0]['dir']);
                    break;
                case 2: $order = array('field' => 'nomeContratante', 'order' => $request['order'][0]['dir']);
                    break;
                case 3: $order = array('field' => 'usuario', 'order' => $request['order'][0]['dir']);
                    break;
                case 4: $order = array('field' => 'dataCadastro', 'order' => $request['order'][0]['dir']);
                    break;
                case 5: $order = array('field' => 'agencia', 'order' => $request['order'][0]['dir']);
                    break;
                case 6: $order = array('field' => 'dataCadastro', 'order' => $request['order'][0]['dir']);
                    break;
                case 7: $order = array('field' => 'usuario', 'order' => $request['order'][0]['dir']);
                    break;
                case 8: $order = array('field' => 'totalContratacao', 'order' => $request['order'][0]['dir']);
                    break;
               
                
            }
        }

        $registros = $this->getServiceLocator()->get($this->nameRepository)->busca($busca, $request['length'], $request['start'], $order);
        $registrosTotaisBusca = $this->getServiceLocator()->get($this->nameRepository)->busca($busca, 0, 0, array(), true);
        $registrosTotais = $this->getServiceLocator()->get($this->nameRepository)->countAll();
        $dados = array();
       //Armazena os dados retornados em um array
        foreach ($registros as $registro) {
            
            $valor = array();
            $valor[] = '<input type="checkbox" name="checkbox-list" class="checkboxes" value="' . $registro['id'] . '" />';

            if ($this->verifyPermission('contratacao', 'editar')) {
                
                $arr = array(
                    'route'=>$this->route,
                    'controller'=>$this->controller,
                    'action'=>'formulario',
                    'id'=>$registro['id']
                 );
              
                
                $valor[] = "<a href='".$this->urls($arr)."' title='Editar Item'>" . $registro['numeroNota'] . "</a>";
                
               
            } else {
                $valor[] = $registro['numeroNota']; 
            }
            
            $dataCadastro =explode('-',substr($registro['dataCadastro'],0,10));
                       
            $valor[] = $registro['nomeContratante'];
            $valor[] = $registro['nome'];
            $valor[] = $dataCadastro[2].'/'.$dataCadastro[1].'/'.$dataCadastro[0];
            $valor[] = $registro['agencia'];
            switch ($registro['situacao']){
                case 1: $valor[] = "<a onclick='verConteudo(".$registro['id'].",this)'><button class='btn red-haze fa fa-plus-circle'></button>Concluído</a>";break; 
                case 2: $valor[] = "Em Andamento";break; 
                case 3: $valor[] = "Cancelada";break;
           }
          
           $valor[] = $registro['user'];
           $valor[] = 'R$ '. number_format($registro['totalContratacao'], 2, ',', '.');
           $dados[] = $valor;
            
            
            
        }

        //Organiza o retorno
        $retorno['draw'] = $request['draw'];
        $retorno['recordsTotal'] = $registrosTotais;
        $retorno['recordsFiltered'] = $registrosTotaisBusca;
        $retorno['data'] = $dados;
       

        //Retorna a resposta
        $response->setContent(\Zend\Json\Json::encode($retorno));
        return $response;
    }
    public function notasAction(){
        
        $response = $this->getResponse();
        $request = $this->getRequest()->getPost();
       
        $registros = $this->getServiceLocator()->get($this->nameRepository)->notasComplementares($request['nota']);
         $array = array();
        
            $k=0;
             foreach ($registros as $registro) {
                $array[$k]['txt_numero_nota'] = $registro['txt_numero_nota'];
                $dataCadastro =explode('-',substr($registro['dt_contratacao_complementar'],0,10));
                $array[$k]['data'] = $dataCadastro[2].'/'.$dataCadastro[1].'/'.$dataCadastro[0];
                $array[$k]['nu_preco_medio_atual'] = $registro['nu_preco_medio_atual'];
                 $k++;
            }
         $response->setContent(\Zend\Json\Json::encode($array));
        return $response;
    }
    
}