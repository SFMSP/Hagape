<?php
namespace Agencia\Service;

use Application\Service\BaseApplicationService;
use Zend\Session\Container as SessionConteiner;
use Agencia\Repository\FalecidoRepository;
use Agencia\Repository\LocalFalecimentoRepository;
use Agencia\Repository\MedicoRepository;
use Agencia\Repository\TransporteRepository;
use Agencia\Repository\CartorioRepository;
use Agencia\Repository\DadosObitoRepository;
use Agencia\Repository\ContratacaoRepository;
use Agencia\Service\Transporte;
use Agencia\Service\Cartorio;
use Agencia\Service\Medico;




class DadosObito extends BaseApplicationService
{
    
    private $cartorioService;
    private $localFalecimentoService;
    private $transporteService;
    private $cemiterioService;
    private $medicoService;
    private $falecidoRepository;
    private $localFalecimentoRepository;
    private $medicoRepository;
    private $transporteRepository;
    private $cartorioRepository;
    private $dadosObitoRepository;
    private $contratacaoRepository;
    private $sepultamentoService;
    private $velorioService;
    private $cremacaoService;
    /**
     * @param mixed $cremacaoService
     */
    public function setCremacaoService($cremacaoService)
    {
        $this->cremacaoService = $cremacaoService;
        return $this;
    }
    /**
     * @param mixed $velorioService
     */
    public function setVelorioService($velorioService)
    {
        $this->velorioService = $velorioService;
        return $this;
    }
    /**
     * 
     * @param DadosObitoRepository $dadosObitoRepository
     * @return \Agencia\Service\DadosObito
     */
    function setDadosObitoRepository(DadosObitoRepository $dadosObitoRepository) {
        $this->dadosObitoRepository = $dadosObitoRepository;
        return $this;
    }
    /**
     * 
     * @param CartorioRepository $cartorioRepository
     * @return \Agencia\Service\Cartorio
     */
    function setCartorioRepository(CartorioRepository $cartorioRepository) {
        $this->cartorioRepository = $cartorioRepository;
        return $this;
    }
    /**
     * 
     * @param TransporteRepository $transporteRepository
     * @return \Agencia\Service\Transporte
     */
    function setTransporteRepository(TransporteRepository $transporteRepository) {
        $this->transporteRepository = $transporteRepository;
        return $this;
    }
    /**
     * @param mixed $sepultamentoService
     */
    public function setSepultamentoService($sepultamentoService)
    {
        $this->sepultamentoService = $sepultamentoService;
        return $this;
    }
    /**
     * 
     * @param MedicoRepository $medicoRepository
     * @return \Agencia\Service\Medico
     */
    function setMedicoRepository(MedicoRepository $medicoRepository) {
        $this->medicoRepository = $medicoRepository;
        return $this;
    }
     /**
     * 
     * @param LocalFalecimentoRepository $localFalecimentoRepository
     * @return \Agencia\Service\LocalFalecimento
     */
    function setLocalFalecimentoRepository(LocalFalecimentoRepository $localFalecimentoRepository)
    {
        $this->localFalecimentoRepository = $localFalecimentoRepository;
        return $this;
    }
    /**
     * 
     * @param FalecidoRepository $falecidoRepository
     * @return \Agencia\Service\Falecido
     */
    function setFalecidoRepository(FalecidoRepository $falecidoRepository) {
        $this->falecidoRepository = $falecidoRepository;
        return $this;
    }
     
    /**
     * @param mixed $cartorioService
     */
    public function setCartorioService($cartorioService)
    {
        $this->cartorioService = $cartorioService;
        return $this;
    }
    /**
     * @param mixed $localFalecimentoService
     */
    public function setLocalFalecimentoService($localFalecimentoService)
    {
        $this->localFalecimentoService = $localFalecimentoService;
        return $this;
    }
    
     /**
     * @param mixed $transporteService
     */
    public function setTransporteService($transporteService)
    {
        $this->transporteService = $transporteService;
        return $this;
    }
    /**
     * @param mixed $cemiterioService
     */
    public function setCemiterioService($cemiterioService)
    {
        $this->cemiterioService = $cemiterioService;
        return $this;
    }
    /**
     * @param mixed $medicoService
     */
    public function setMedicoService($medicoService)
    {
        $this->medicoService = $medicoService;
        return $this;
    }
   /**
     * 
     * @param ContratacaoRepository $contratacaoRepository
     * @return \Agencia\Service\ContratacaoRepository
     */
    function setContratacaoRepository(ContratacaoRepository $contratacaoRepository) {
        $this->contratacaoRepository = $contratacaoRepository;
        return $this;
    }
    
    
    
    public function save(array $data)
    {
        $contratacao = new SessionConteiner('Contratacao');

        $data['dataCadastro'] = new \DateTime('now');
        $data['contratacao'] = $this->getEm()->getReference('Agencia\Entity\Contratacao', ['id' => $contratacao->id]);
        
        //$data['falecido'] = $this->falecidoRepository->findOneBy(array('contratacao' => $contratacao->id));
        //print_r($data['falecido']);die;
        
        //$data['contratacao'] = $this->getEm()->getReference('Agencia\Entity\Contratacao', $contratacao->id);
        //$data['falecido'] =  $falecido;
        
        
        try {
            
            $this->getEm()->beginTransaction();
           
             if (!empty($data['cartorioId'])){
                
                $cartorio =  $this->cartorioRepository->findOneBy(array('id' => $data['cartorioId']));

                 if($cartorio){
                    $data['cartorio'] = $cartorio;
                    
                 }
                
            }else if($data['cartorio']!==''){
             
                     $carto['id'] ='';
                     $carto['nome'] = $data['cartorio'];
                     $carto['dataCadastro'] = new \DateTime('now');
                     $cartorios = $this->cartorioService->save($carto);
                
                    if (!$cartorios) {
                                        $this->getEm()->rollback();
                                        return false;
                                    }
                    $data['cartorio'] = $this->cartorioRepository->findOneBy(array('id' => $cartorios->getId()));
                
            }
           
            if (!empty($data['localFalecimentoId'])){
                 
                $local = $this->getEm()->getReference('Agencia\Entity\LocalFalecimento', ['id' => $data['localFalecimentoId']]);

                 if($local){
                    $data['localFalecimento'] = $local;
                    
                 }
                
            }else if($data['localFalecimento']!==''){
                //print_r($data['localFalecimento']);die;
                $data['nomeLocalFalecimento'] = $data['localFalecimento'];
                $data['enderecoFalecimento'] = $data['enderecoFalecimento'];
                $data['localFalecimento']=null;
                $data['horaFalecimento']= $data['hrFalecimento'];
                
            }
            
           
           if ($data['dataFalecimento']) {
                $data['dataFalecimento'] = new \DateTime($this->getDateTimeFormat()->formatUs($data['dataFalecimento']));
            } else {
                $data['dataFalecimento'] = null;
            }
           
              
          
           /* insere na tabela transporte***/
           if(!empty($data['tipoTransporte'])){
              
                    $trans = array();
                    $trans['id'] ='';    
                    $trans['tipoTransporte']         = $data['tipoTransporte'];
                    $trans['tipoCarro']              = $data['tipoCarro'];

               if(!empty($data['remocao'])){
                   $trans['localRemocaoFalecimento'] ="S";
                   $trans['localRemocao']            = "";
                   $trans['endereco']                = "";

               }else{
                   $trans['localRemocaoFalecimento'] ="N";
                   $trans['localRemocao']            = $data['localRemocao'];
                   $trans['endereco']                = $data['enderecoRemocao'];
               }
                 
                $trans = $this->transporteService->save($trans);
                
                if (!$trans) {
                    $this->getEm()->rollback();
                    return false;
                }
               
           
                 $data['transporte'] = $this->transporteRepository->findOneBy(array('id' => $trans->getId()));
            }
           
             if($data['operacao3']==3){
                 
                 $data['seraVelado'] = 0;
                  
              }else{
                 if($data['seraVelado']){
              
                if($data['seraVelado']==0){
                        $data['corpoVelado'] = 'N';
                    } else if($data['seraVelado']==1){
                        $data['corpoVelado']='S';
                    }else if($data['seraVelado']==2){
                        $data['corpoVelado']='N';
                    }
                    }
              }
           
       
            $med = new \Doctrine\Common\Collections\ArrayCollection();
            
            
            if (!empty($data['crm1'])) {
                $crm = $this->medicoRepository->findOneBy(array('crm' => $data['crm1']));

                if ($crm) {
                    $medico = $this->getEm()->getReference('Agencia\Entity\Medico', $crm->getId());
                } else {
                    $medico = new \Agencia\Entity\Medico();
                    $medico->setNomeMedico($data['medico1']);
                    $medico->setCrm($data['crm1']);
                    $medico->setDataCadastro(new \DateTime('now'));
                    
                    $this->getEm()->persist($medico);
                    $this->getEm()->flush();
                 
                }
                
                if (!$medico) {
                    $this->getEm()->rollback();
                    return false;
                }
                
        
                
                $med->add($medico);
            }
            if (!empty($data['crm2'])) {
                $crm = $this->medicoRepository->findOneBy(array('crm' => $data['crm2']));

                if ($crm) {
                    $medico = $this->getEm()->getReference('Agencia\Entity\Medico', $crm->getId());
                } else {
                    $medico = new \Agencia\Entity\Medico();
                    $medico->setNomeMedico($data['medico2']);
                    $medico->setCrm($data['crm2']);
                    $medico->setDataCadastro(new \DateTime('now'));
                    
                    $this->getEm()->persist($medico);
                    $this->getEm()->flush();
                 
                }
                
                if (!$medico) {
                    $this->getEm()->rollback();
                    return false;
                }
                
        
                
                $med->add($medico);
            }
            
            $data['medico'] = $med; 
               
             
            $obito = parent::save($data);
            
             if (!$obito) {
                    $this->getEm()->rollback();
                    return false;
            }
              
            /*Sepultamento**/
          if($data['tipoDestinoFinal']==1){
              
                if($data['hiddenCemiterio']){
                   
                   $cemiterios = $this->getEm()->getReference('Admin\Entity\Cemiterio', ['id' => $data['hiddenCemiterio']]);

                 if($cemiterios){
                    $data['cemiterio'] = $cemiterios->getId();
                    $variavel  = $cemiterios;
                 }
                 
                   
               }else if($data['cemiterio']!==''){
                   
                        $cemit = new \Admin\Entity\Cemiterio();
                        $cemit->setNome($data['cemiterio']);
                        $cemit->setEndereco($data['endereco']);
                        $cemit->setTipo($this->getEm()->getReference('Admin\Entity\Tipo', 1));
                        $cemit->setDataCadastro(new \DateTime('now'));
                        $cemit->setNumero('');
                        $cemit->setBairro('');
                        $cemit->setCep('');
                        $cemit->setPrimeiroTelefone('');
                        $cemit->setSegundoTelefone('');
                        $cemit->setCategoria('');
                        $cemit->setEmail('');
                        $cemit->setObservacao('');
                        $cemit->setAtivo(1);
                        $this->getEm()->persist($cemit);
                        $this->getEm()->flush();
                        $variavel =$cemit;
                        
               }
                        $sepultamento = array();
                        $sepultamento['cemiterio'] =  $variavel;  
                        $sepultamento['id'] = '';
                        $sepultamento['contratacao'] = $this->getEm()->getReference('Agencia\Entity\Contratacao', $contratacao->id);
                        $sepultamento['dataCadastro'] = new \DateTime('now');
                        $sepultamento['dadosObito'] = $this->getEm()->getReference('Agencia\Entity\DadosObito', $obito->getId());
                        $sepultamento['dataSepultamento'] = new \DateTime($this->getDateTimeFormat()->formatUs($data['dtSepultamento']));
                        $sepultamento['horaSepultamento'] = $data['hrSepultamento'];
                        
                        $this->sepultamentoService->save($sepultamento);
            }
           /*Cremação**/
           if($data['tipoDestinoFinal']==2){
                
           if($data['hiddenCrematorio']){
                   
                   $crematorios = $this->getEm()->getReference('Admin\Entity\Cemiterio', ['id' => $data['hiddenCrematorio']]);

                 if($crematorios){
                    $data['cemiterio'] = $crematorios->getId();
                    $variavel = $crematorios;
                 }
                
                   
               }else if($data['crematorio']!==''){
                
                   //
                        $cremat = new \Admin\Entity\Cemiterio();
                        $cremat->setNome($data['crematorio']);
                        $cremat->setEndereco($data['endereco']);
                        $cremat->setTipo($this->getEm()->getReference('Admin\Entity\Tipo', 2));
                        $cremat->setDataCadastro(new \DateTime('now'));
                        $cremat->setNumero('');
                        $cremat->setBairro('');
                        $cremat->setCep('');
                        $cremat->setPrimeiroTelefone('');
                        $cremat->setSegundoTelefone('');
                        $cremat->setCategoria('');
                        $cremat->setEmail('');
                        $cremat->setObservacao('');
                        $cremat->setAtivo(1);
                        $this->getEm()->persist($cremat);
                        $this->getEm()->flush();
                        $variavel = $cremat;
                        
               }
                        $cremacao = array();
                        $cremacao['cemiterio'] = $variavel;
                        $cremacao['id'] = '';
                        $cremacao['contratacao'] = $this->getEm()->getReference('Agencia\Entity\Contratacao', $contratacao->id);
                        $cremacao['dataCadastro'] = new \DateTime('now');
                        $cremacao['dadosObito'] = $this->getEm()->getReference('Agencia\Entity\DadosObito', $obito->getId());
                        $cremacao['dataCremacao'] = new \DateTime($this->getDateTimeFormat()->formatUs($data['dtCremacao']));
                        $cremacao['horaCremacao'] = $data['hrCremacao'];
                        $cremacao['endereco'] = $data['endereco'];
                        $cremacao['crematorio'] = $data['crematorio'];
                        
                        $this->cremacaoService->save($cremacao);
               
               
            
            
            
           }
         
            if($data['corpoVelado']=='S'){
                 
                $velado = array();
                $velado['id'] = '';
                $velado['dadosObito'] = $this->getEm()->getReference('Agencia\Entity\DadosObito', $obito->getId());
                $velado['contratacao'] = $this->getEm()->getReference('Agencia\Entity\Contratacao', $contratacao->id);
                $velado['localVelorio'] = $data['localVelorio'];
                $velado['dataCadastro'] = new \DateTime('now');
                $velado['endereco'] = $data['enderecoVelorio'];
                $velado['dataSaida'] = new \DateTime($this->getDateTimeFormat()->formatUs($data['dtSaida']));
                $velado['horaSaida'] = $data['hrSaida'];    
                
                $velorios  = $this->velorioService->save($velado);
                 
                 
                  
            }
           
         
            $this->getEm()->commit();
            
                return true;
            
        } catch (\Exception $ex) {
            $this->log->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            $this->getEm()->rollback();
            return false;
        }
    }
}