<?php

namespace Agencia\Service;

use Application\Service\BaseApplicationService;
use Zend\Session\Container as SessionConteiner;
use Agencia\Repository\ContratanteRepository;


class TransladoCorpo extends BaseApplicationService
{
        private $cemiterioService;
        private $contratanteRepository;
    /**
     * @param mixed $cemiterioService
     */
    public function setCemiterioService($cemiterioService)
    {
        $this->cemiterioService = $cemiterioService;
        return $this;
    }
    /**
     * 
     * @param ContratanteRepository $contratanteRepository
     * @return \Agencia\Service\ContratanteRepository
     */
    function setContratanteRepository(ContratanteRepository $contratanteRepository) {
        $this->contratanteRepository = $contratanteRepository;
        return $this;
    }
    
    
    
    public function save(array $data)
    {
        $contratacao = new SessionConteiner('Contratacao');
        
      
        $data['dataCadastro'] = new \DateTime('now');
        $data['contratacao'] = $this->getEm()->getReference('Agencia\Entity\Contratacao', ['id' => $contratacao->id]);
        $contratante = $this->contratanteRepository->findOneBy(array('contratacao' => $contratacao->id));
        $data['contratante'] = $this->getEm()->getReference('Agencia\Entity\Contratante', $contratante->getId());
        try {
       $this->getEm()->beginTransaction();
      
                    /**Sepultamento**/
       if($data['tipoDestinoFinal']==1){
           
         
        if($data['hiddenCemiterio']){

               $cemiterioSaida = $this->getEm()->getReference('Admin\Entity\Cemiterio', ['id' => $data['hiddenCemiterio']]);

                 if($cemiterioSaida){
                    $data['cemiterioSaida'] = $cemiterioSaida;
                 }


           } else if($data['cemiterioSaida']!==''){
                        $cemit = new \Admin\Entity\Cemiterio();
                        $cemit->setNome($data['cemiterioSaida']);
                        $cemit->setEndereco($data['enderecoCemiterio']);
                        $cemit->setTipo($this->getEm()->getReference('Admin\Entity\Tipo', 1));
                        $cemit->setDataCadastro($data['dataCadastro']);
                        $cemit->setNumero('');
                        $cemit->setBairro('');
                        $cemit->setCep('');
                        $cemit->setPrimeiroTelefone('');
                        $cemit->setSegundoTelefone('');
                        $cemit->setCategoria('');
                        $cemit->setEmail('');
                        $cemit->setObservacao('');
                        $cemit->setAtivo(1);
                        $this->getEm()->persist($cemit);
                        $this->getEm()->flush();
               $data['cemiterioSaida'] =    $cemit;
           }    
           
           
           
        if($data['hiddenCemiterioDestino']){
        
            $cemiterioDestino = $this->getEm()->getReference('Admin\Entity\Cemiterio', ['id' => $data['hiddenCemiterioDestino']]);
            if($cemiterioDestino){
                   $data['cemiterioDestino'] = $cemiterioDestino;
            } 
            
          
        } else if($data['cemiterioDestino']!==''){
            $cemit = new \Admin\Entity\Cemiterio();
                    $cemit->setNome($data['cemiterioSaida']);
                    $cemit->setEndereco($data['enderecoCemiterio']);
                    $cemit->setTipo($this->getEm()->getReference('Admin\Entity\Tipo', 2));
                    $cemit->setDataCadastro($data['dataCadastro']);
                    $cemit->setNumero('');
                    $cemit->setBairro('');
                    $cemit->setCep('');
                    $cemit->setPrimeiroTelefone('');
                    $cemit->setSegundoTelefone('');
                    $cemit->setCategoria('');
                    $cemit->setEmail('');
                    $cemit->setObservacao('');
                    $cemit->setAtivo(1);
                    $this->getEm()->persist($cemit);
                    $this->getEm()->flush();
           $data['cemiterioDestino'] =    $cemit;
            
            
        }   
           
           
       }else if($data['tipoDestinoFinal']==2){
           
            if($data['hiddenCemiterio']){

               $cemiterioSaida = $this->getEm()->getReference('Admin\Entity\Cemiterio', ['id' => $data['hiddenCemiterio']]);

                 if($cemiterioSaida){
                    $data['cemiterioSaida'] = $cemiterioSaida;
                 }


           } else if($data['cemiterioSaida']!==''){
                        $cemit = new \Admin\Entity\Cemiterio();
                        $cemit->setNome($data['cemiterioSaida']);
                        $cemit->setEndereco($data['enderecoCemiterio']);
                        $cemit->setTipo($this->getEm()->getReference('Admin\Entity\Tipo', 1));
                        $cemit->setDataCadastro($data['dataCadastro']);
                        $cemit->setNumero('');
                        $cemit->setBairro('');
                        $cemit->setCep('');
                        $cemit->setPrimeiroTelefone('');
                        $cemit->setSegundoTelefone('');
                        $cemit->setCategoria('');
                        $cemit->setEmail('');
                        $cemit->setObservacao('');
                        $cemit->setAtivo(1);
                        $this->getEm()->persist($cemit);
                        $this->getEm()->flush();
               $data['cemiterioSaida'] =    $cemit;
           }
           
            if($data['hiddenCrematorioDestino']){
        $crematorioDestino = $this->getEm()->getReference('Admin\Entity\Cemiterio', ['id' => $data['hiddenCrematorioDestino']]);
       
            if($crematorioDestino){
                    $data['cemiterioDestino'] = $crematorioDestino;
            } 

       }else if($data['crematorioDestino']!==''){
           
            $cemit = new \Admin\Entity\Cemiterio();
                    $cemit->setNome($data['crematorioDestino']);
                    $cemit->setEndereco($data['enderecoCemiterio']);
                    $cemit->setTipo($this->getEm()->getReference('Admin\Entity\Tipo', 2));
                    $cemit->setDataCadastro($data['dataCadastro']);
                    $cemit->setNumero('');
                    $cemit->setBairro('');
                    $cemit->setCep('');
                    $cemit->setPrimeiroTelefone('');
                    $cemit->setSegundoTelefone('');
                    $cemit->setCategoria('');
                    $cemit->setEmail('');
                    $cemit->setObservacao('');
                    $cemit->setAtivo(1);
                    $this->getEm()->persist($cemit);
                    $this->getEm()->flush();
           $data['cemiterioDestino'] =    $cemit;
       }
           
           
       }
      
        
        
      
            
        
       $data['dataSaida'] =new \DateTime($this->getDateTimeFormat()->formatUs($data['dtSaida'])); 
       if($data['hrSepultamento']){
            $data['horaCremacao'] = $data['hrSepultamento'];       
       }
       
       if(isset($data['hrCremacao'])){
            $data['horaCremacao'] = $data['hrCremacao'];       
       }
       
          
            //Insere Contratante
            $translado = parent::save($data);
            if (!$translado) {
                $this->getEm()->rollback();
                return false;
            }

          
        

            $this->getEm()->commit();
            return true;

        } catch (\Exception $ex) {
            $this->log->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            $this->getEm()->rollback();
            return false;
        }
    }

}
