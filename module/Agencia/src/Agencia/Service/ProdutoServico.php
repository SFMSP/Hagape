<?php

namespace Agencia\Service;

use Application\Service\BaseApplicationService;
use Zend\Session\Container as SessionConteiner;

class ProdutoServico extends BaseApplicationService
{
    private $produtoVendidoService;
    private $servicoVendidoService;

    /**
     * @param mixed $produtoVendidoService
     */
    public function setProdutoVendidoService($produtoVendidoService)
    {
        $this->produtoVendidoService = $produtoVendidoService;
        return $this;
    }

    /**
     * @param mixed $servicoVendidoService
     */
    public function setServicoVendidoService($servicoVendidoService)
    {
        $this->servicoVendidoService = $servicoVendidoService;
        return $this;
    }


    public function save(array $data)
    {
        $contratacao = new SessionConteiner('Contratacao');

        if (isset($data['convenio'])) {
            $data['boolConvenio'] = 1;
        } else {
            $data['boolConvenio'] = 0;
        }
        if (isset($data['remocaoCorpoLocalFalecimento'])) {
            $data['removeCorpoLocalFal'] = 1;
            $data['localRemocaoCorpo'] = $data['localCorpoRemocao'];
            $data['endRemocaoCorpo'] = $data['enderecoCorpoRemocao'];
        } else {
            $data['removeCorpoLocalFal'] = 0;
            $data['localRemocaoCorpo'] = '';
            $data['endRemocaoCorpo'] = '';
        }
        if (isset($data['remocaoMembroLocalFalecimento'])) {
            $data['removeMembroLocalAmp'] = 1;
            $data['localRemocaoMembro'] = $data['localMembroRemocao'];
            $data['endRemocaoMembro'] = $data['enderecoMembroRemocao'];
        } else {
            $data['removeCorpoLocalFal'] = 0;
            $data['localRemocaoMembro'] = '';
            $data['endRemocaoMembro'] = '';
        }

        $data['valorTotal'] = preg_replace('/[.]/', '', $data['valorTotal']);
        $data['valorTotal'] = preg_replace('/[,]/', '.', $data['valorTotal']);
        $data['observacao'] = $data['observacoes'];
        if (isset($data['convenioSelect'])) {
            $data['convenio'] = $this->getEm()->getRepository('Agencia\Entity\Convenio')->findBy(array('id' => $data['convenioSelect']));
        } else {
            $data['convenio'] = null;
        }
        $data['contratacao'] = $this->getEm()->getRepository('Agencia\Entity\Contratacao')->findOneBy(array('id' => $contratacao->id));

        try {
            $this->getEm()->beginTransaction();
            //Insere a venda do Produto e Serviço
            $entityVendaProdutoServico = parent::save($data);
            if (!$entityVendaProdutoServico) {
                $this->getEm()->rollback();
                return false;
            }

            $arDadosVendaProduto = array();
            $arDadosVendaProduto['produto'] = $this->getEm()->getRepository('Estoque\Entity\Produto')->findOneBy(array('id' => $data['urna']));
            $arDadosVendaProduto['quantidade'] = $data["quantidadeUrna"];
            $arDadosVendaProduto['valorConjunto'] = preg_replace('/[.]/', '', $data['valorConjuntoUrna']);
            $arDadosVendaProduto['valorConjunto'] = preg_replace('/[,]/', '.', $arDadosVendaProduto['valorConjunto']);
            $arDadosVendaProduto['categoria'] = $arDadosVendaProduto['produto']->getCategoria();
            $arDadosVendaProduto['classificacaoVenda'] = $this->getEm()->getRepository('Agencia\Entity\ClassificacaoVenda')->findOneBy(array('produto' => $arDadosVendaProduto['produto']));
            $arDadosVendaProduto['vendaProduto'] = $entityVendaProdutoServico;

            //Insere a venda do Produto Vendido da Urna
            $entityProdutoVendido = $this->produtoVendidoService->save($arDadosVendaProduto);

            if (!$entityProdutoVendido) {
                $this->getEm()->rollback();
                return false;
            }


            $arDadosVendaProduto = [];
            $arDadosVendaProduto['produto'] = $this->getEm()->getRepository('Estoque\Entity\Produto')->findOneBy(array('id' => $data['revestimento']));
            $arDadosVendaProduto['quantidade'] = $data["quantidadeRevestimento"];
            $arDadosVendaProduto['valorConjunto'] = preg_replace('/[.]/', '', $data['valorConjuntoRevestimento']);
            $arDadosVendaProduto['valorConjunto'] = preg_replace('/[,]/', '.', $arDadosVendaProduto['valorConjunto']);
            $arDadosVendaProduto['categoria'] = $arDadosVendaProduto['produto']->getCategoria();
            $arDadosVendaProduto['classificacaoVenda'] = $this->getEm()->getRepository('Agencia\Entity\ClassificacaoVenda')->findOneBy(array('produto' => $arDadosVendaProduto['produto']));
            $arDadosVendaProduto['vendaProduto'] = $entityVendaProdutoServico;

            //Insere a venda do Produto Vendido do Revestimento
            $entityProdutoVendido = $this->produtoVendidoService->save($arDadosVendaProduto);

            echo '<pre>';
            var_dump($entityProdutoVendido);
            die('</pre>');
            if (!$entityProdutoVendido) {
                $this->getEm()->rollback();
                return false;
            }



            $this->getEm()->commit();
            return true;

        } catch (\Exception $ex) {
            $this->log->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            $this->getEm()->rollback();
            return false;
        }

    }

}