<?php
namespace Agencia\Service;

use Application\Service\BaseApplicationService;
use Zend\Session\Container as SessionConteiner;
use Agencia\Repository\MedicoRepository;
use Agencia\Repository\DadosAmputacaoRepository;
use Admin\Repository\CemiterioRepository;
use Agencia\Repository\PacienteRepository;
use Agencia\Service\Medico;
use Agencia\Service\Sepultamento;



class DadosAmputacao extends BaseApplicationService
{
    

    private $pacienteRepository;
    private $dadosAmputacaoRepository;
    private $medicoRepository;
    private $cremacaoService;
    private $sepultamentoService;
    private $cemiterioRepository;
            
/**
     * 
     * @param CemiterioRepository $cemiterioRepository
     * @return \Admin\Service\Cemiterio
     */
    function setCemiterioRepository(CemiterioRepository $cemiterioRepository) {
        $this->cemiterioRepository = $cemiterioRepository;
        return $this;
    }

    /**
     * 
     * @param DadosAmputacaoRepository $dadosAmputacaoRepository
     * @return \Agencia\Service\DadosAmputacao
     */
    function setDadosAmputacaoRepository(DadosAmputacaoRepository $dadosAmputacaoRepository) {
        $this->dadosAmputacaoRepository = $dadosAmputacaoRepository;
        return $this;
    }
    /**
     * 
     * @param PacienteRepository $pacienteRepository
     * @return \Agencia\Service\PacienteRepository
     */
    function setPacienteRepository(PacienteRepository $pacienteRepository) {
        $this->pacienteRepository = $pacienteRepository;
        return $this;
    }
    /**
     * 
     * @param MedicoRepository $medicoRepository
     * @return \Agencia\Service\MedicoRepository
     */
    function setMedicoRepository(MedicoRepository $medicoRepository) {
        $this->medicoRepository = $medicoRepository;
        return $this;
    }
    
    /**
     * @param mixed $cremacaoService
     */
    public function setCremacaoService($cremacaoService)
    {
        $this->cremacaoService = $cremacaoService;
        return $this;
    }
    /**
     * @param mixed $sepultamentoService
     */
    public function setSepultamentoService($sepultamentoService)
    {
        $this->sepultamentoService = $sepultamentoService;
        return $this;
    }
    
    public function save(array $data)
    {
        
       
        $contratacao = new SessionConteiner('Contratacao');

        $data['dataCadastro'] = new \DateTime('now');
        $data['paciente']   = $this->pacienteRepository->findOneBy(array('contratacao' => $contratacao->id));
       try {
            $this->getEm()->beginTransaction();
            
            $med = new \Doctrine\Common\Collections\ArrayCollection();
            
            
            if (!empty($data['crm1'])) {
                $crm = $this->medicoRepository->findOneBy(array('crm' => $data['crm1']));

                if ($crm) {
                    $medico = $this->getEm()->getReference('Agencia\Entity\Medico', $crm->getId());
                } else {
                    $medico = new \Agencia\Entity\Medico();
                    $medico->setNomeMedico($data['medico1']);
                    $medico->setCrm($data['crm1']);
                    $medico->setDataCadastro($data['dataCadastro']);
                    
                    $this->getEm()->persist($medico);
                    $this->getEm()->flush();
                 
                }
                
                if (!$medico) {
                    $this->getEm()->rollback();
                    return false;
                }
                
        
                
                $med->add($medico);
            }
           
            $data['medico'] = $med; 
            $amputacao = parent::save($data);
            
             if (!$amputacao) {
                    $this->getEm()->rollback();
                    return false;
            }
         
            
            
            
            
           /*Sepultamento**/
           if($data['tipoDestinoFinal']==1){
               
             
               if($data['hiddenCemiterio']){
                   
                   $cemiterios = $this->getEm()->getReference('Admin\Entity\Cemiterio', ['id' => $data['hiddenCemiterio']]);

                 if($cemiterios){
                    $data['cemiterio'] = $cemiterios->getId();
                    $variavel  = $cemiterios->getId();
                 }
                 
                   
               }else if($data['cemiterio']!==''){
                  
                        $cemit = new \Admin\Entity\Cemiterio();
                        $cemit->setNome($data['cemiterio']);
                        $cemit->setEndereco($data['enderecoCemiterio']);
                        $cemit->setTipo($this->getEm()->getReference('Admin\Entity\Tipo', 1));
                        $cemit->setDataCadastro($data['dataCadastro']);
                        $cemit->setNumero('');
                        $cemit->setBairro('');
                        $cemit->setCep('');
                        $cemit->setPrimeiroTelefone('');
                        $cemit->setSegundoTelefone('');
                        $cemit->setCategoria('');
                        $cemit->setEmail('');
                        $cemit->setObservacao('');
                        $cemit->setAtivo(1);
                        $this->getEm()->persist($cemit);
                        $this->getEm()->flush();
                        $variavel =$cemit->getId();
               }
                        $sepultamento = array();
                        $sepultamento['cemiterio'] =  $variavel;  
                        $sepultamento['id'] = '';
                        $sepultamento['contratacao'] = $this->getEm()->getReference('Agencia\Entity\Contratacao', $contratacao->id);
                        $sepultamento['dataCadastro'] = new \DateTime('now');
                        $sepultamento['dadosAmputacao'] = $this->getEm()->getReference('Agencia\Entity\DadosAmputacao', $amputacao->getId());
                        $sepultamento['dataSepultamento'] = new \DateTime($this->getDateTimeFormat()->formatUs($data['dtSepultamento']));
                        $sepultamento['horaSepultamento'] = $data['hrSepultamento'];
                        
                        $this->sepultamentoService->save($sepultamento);
              
               
               
           }
           /*Cremação**/
           if($data['tipoDestinoFinal']==2){
              
               
              
               if($data['hiddenCrematorio']){
                   
                   $crematorios = $this->getEm()->getReference('Admin\Entity\Cemiterio', ['id' => $data['hiddenCrematorio']]);

                 if($crematorios){
                    $data['cemiterio'] = $crematorios->getId();
                    $variavel = $crematorios->getId();
                 }
                
                   
               }else if($data['crematorio']!==''){
                
                   //
                        $cremat = new \Admin\Entity\Cemiterio();
                        $cremat->setNome($data['crematorio']);
                        $cremat->setEndereco($data['enderecoCrematorio']);
                        $cremat->setTipo($this->getEm()->getReference('Admin\Entity\Tipo', 2));
                        $cremat->setDataCadastro($data['dataCadastro']);
                        $cremat->setNumero('');
                        $cremat->setBairro('');
                        $cremat->setCep('');
                        $cremat->setPrimeiroTelefone('');
                        $cremat->setSegundoTelefone('');
                        $cremat->setCategoria('');
                        $cremat->setEmail('');
                        $cremat->setObservacao('');
                        $cremat->setAtivo(1);
                        $this->getEm()->persist($cremat);
                        $this->getEm()->flush();
                        $variavel = $cremat->getId();
                        
               }
                        $cremacao = array();
                        $cremacao['cemiterio'] = $variavel;
                        $cremacao['id'] = '';
                        $cremacao['contratacao'] = $this->getEm()->getReference('Agencia\Entity\Contratacao', $contratacao->id);
                        $cremacao['dataCadastro'] = new \DateTime('now');
                        $cremacao['dadosAmputacao'] = $this->getEm()->getReference('Agencia\Entity\DadosAmputacao', $amputacao->getId());
                        $cremacao['dataCremacao'] = new \DateTime($this->getDateTimeFormat()->formatUs($data['dtCremacao']));
                        $cremacao['horaCremacao'] = $data['hrCremacao'];
                        $cremacao['endereco'] = $data['enderecoCrematorio'];
                        $cremacao['crematorio'] = $data['crematorio'];
                        
                        $this->cremacaoService->save($cremacao);
               
               
               
               
           }
           
          
       
           
           $this->getEm()->commit();
            return true;
            
        } catch (\Exception $ex) {
            die($ex->getMessage());
            $this->log->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            $this->getEm()->rollback();
            return false;
        }
    }
}