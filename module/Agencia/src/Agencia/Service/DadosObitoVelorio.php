<?php
namespace Agencia\Service;

use Application\Service\BaseApplicationService;
use Zend\Session\Container as SessionConteiner;
use Agencia\Repository\LocalFalecimentoRepository;
use Agencia\Repository\MedicoRepository;
use Agencia\Repository\DadosObitoVelorioRepository;
use Agencia\Repository\ContratacaoRepository;
use Agencia\Repository\ContratanteRepository;
use Agencia\Service\Medico;



class DadosObitoVelorio extends BaseApplicationService
{
    

    private $localFalecimentoService;
    private $cemiterioService;
    private $medicoService;
    private $localFalecimentoRepository;
    private $medicoRepository;
    private $dadosObitoVelorioRepository;
    private $contratanteRepository;
    private $contratacaoRepository;
    private $sepultamentoService;
    private $velorioService;
    private $cremacaoService;
    
    /**
     * 
     * @param ContratacaoRepository $contratacaoRepository
     * @return \Agencia\Service\ContratacaoRepository
     */
    function setContratacaoRepository(ContratacaoRepository $contratacaoRepository) {
        $this->contratacaoRepository = $contratacaoRepository;
        return $this;
    }
    /**
     * 
     * @param ContratanteRepository $contratanteRepository
     * @return \Agencia\Service\ContratanteRepository
     */
    function setContratanteRepository(ContratanteRepository $contratanteRepository) {
        $this->contratanteRepository = $contratanteRepository;
        return $this;
    }
    /**
     * 
     * @param DadosObitoVelorioRepository $dadosObitoVelorioRepository
     * @return \Agencia\Service\DadosObitoVelorio
     */
    function setDadosObitoVelorioRepository(DadosObitoVelorioRepository $dadosObitoVelorioRepository) {
        $this->dadosObitoVelorioRepository = $dadosObitoVelorioRepository;
        return $this;
    }
    
 
    /**
     * 
     * @param MedicoRepository $medicoRepository
     * @return \Agencia\Service\Medico
     */
    function setMedicoRepository(MedicoRepository $medicoRepository) {
        $this->medicoRepository = $medicoRepository;
        return $this;
    }
     /**
     * 
     * @param LocalFalecimentoRepository $localFalecimentoRepository
     * @return \Agencia\Service\LocalFalecimento
     */
    function setLocalFalecimentoRepository(LocalFalecimentoRepository $localFalecimentoRepository)
    {
        $this->localFalecimentoRepository = $localFalecimentoRepository;
        return $this;
    }
   
    /**
     * @param mixed $cremacaoService
     */
    public function setCremacaoService($cremacaoService)
    {
        $this->cremacaoService = $cremacaoService;
        return $this;
    }
    /**
     * @param mixed $localFalecimentoService
     */
    public function setLocalFalecimentoService($localFalecimentoService)
    {
        $this->localFalecimentoService = $localFalecimentoService;
        return $this;
    }
    /**
     * @param mixed $sepultamentoService
     */
    public function setSepultamentoService($sepultamentoService)
    {
        $this->sepultamentoService = $sepultamentoService;
        return $this;
    }
    /**
     * @param mixed $velorioService
     */
    public function setVelorioService($velorioService)
    {
        $this->velorioService = $velorioService;
        return $this;
    }
    
    
    /**
     * @param mixed $cemiterioService
     */
    public function setCemiterioService($cemiterioService)
    {
        $this->cemiterioService = $cemiterioService;
        return $this;
    }
    /**
     * @param mixed $medicoService
     */
    public function setMedicoService($medicoService)
    {
        $this->medicoService = $medicoService;
        return $this;
    }
   
    
    public function save(array $data)
    {
        
        
  
        $contratacao = new SessionConteiner('Contratacao');

        $data['dataCadastro'] = new \DateTime('now');
        $contratante = $this->contratanteRepository->findOneBy(array('contratacao' => $contratacao->id));
        $data['contratante'] = $this->getEm()->getReference('Agencia\Entity\Contratante', $contratante->getId());
        $data['contratacao'] = $this->getEm()->getReference('Agencia\Entity\Contratacao', $contratacao->id);
        
       try {
            $this->getEm()->beginTransaction();
           
            if ($data['localFalecimentoId']){
                 
                $local = $this->getEm()->getReference('Agencia\Entity\LocalFalecimento', ['id' => $data['localFalecimentoId']]);

                 if($local){
                    $data['localFalecimento'] = $local;
                    
                 }
                
            }else if($data['localFalecimento']!==''){
                
                $data['ondeFaleceu'] = $data['localFalecimento'];
                $data['endereco'] = $data['enderecoFalecimento'];
                $data['localFalecimento']=null;
                
            }
            
            if(!empty($data['nome'])){
                
                 $data['nomeFalecido'] = $data['nome'];
                
            }
                       
            if($data['seraVelado']!= null){
                
                if($data['seraVelado']==0){
                    $data['corpoVelado'] = 'N';
                } else if($data['seraVelado']==1){
                    $data['corpoVelado']='S';
                }else if($data['seraVelado']==2){
                    $data['corpoVelado']='N';
                }
                            
            }
           
            if ($data['dataFalecimento']) {
                $data['dataFalecimento'] = new \DateTime($this->getDateTimeFormat()->formatUs($data['dataFalecimento']));
            } else {
                $data['dataFalecimento'] = null;
            }
            if(!empty($data['tamanho'])){
              $data['tamanhoCaixao']= $data['tamanho']; 
            }
            
            $med = new \Doctrine\Common\Collections\ArrayCollection();
            
            
            if (!empty($data['crm1'])) {
                $crm = $this->medicoRepository->findOneBy(array('crm' => $data['crm1']));

                if ($crm) {
                    $medico = $this->getEm()->getReference('Agencia\Entity\Medico', $crm->getId());
                } else {
                    $medico = new \Agencia\Entity\Medico();
                    $medico->setNomeMedico($data['medico1']);
                    $medico->setCrm($data['crm1']);
                    $medico->setDataCadastro($data['dataCadastro']);
                    
                    $this->getEm()->persist($medico);
                    $this->getEm()->flush();
                 
                }
                
                if (!$medico) {
                    $this->getEm()->rollback();
                    return false;
                }
                
        
                
                $med->add($medico);
            }
            if (!empty($data['crm2'])) {
                $crm = $this->medicoRepository->findOneBy(array('crm' => $data['crm2']));

                if ($crm) {
                    $medico = $this->getEm()->getReference('Agencia\Entity\Medico', $crm->getId());
                } else {
                    $medico = new \Agencia\Entity\Medico();
                    $medico->setNomeMedico($data['medico2']);
                    $medico->setCrm($data['crm2']);
                    $medico->setDataCadastro($data['dataCadastro']);
                    
                    $this->getEm()->persist($medico);
                    $this->getEm()->flush();
                 
                }
                
                if (!$medico) {
                    $this->getEm()->rollback();
                    return false;
                }
                
        
                
                $med->add($medico);
            }
            
            $data['medico'] = $med; 
            $obitoVelorio = parent::save($data);
            
             if (!$obitoVelorio) {
                    $this->getEm()->rollback();
                    return false;
            }
          
             /*Sepultamento**/
           if($data['tipoDestinoFinal']==1){
                
                if($data['hiddenCemiterio']){
                   
                   $cemiterios = $this->getEm()->getReference('Admin\Entity\Cemiterio', ['id' => $data['hiddenCemiterio']]);

                 if($cemiterios){
                    $data['cemiterio'] = $cemiterios->getId();
                    $variavel  = $cemiterios->getId();
                 }
                 
                   
               }else if($data['cemiterio']!==''){
                   $cemit = new \Admin\Entity\Cemiterio();
                        $cemit->setNome($data['cemiterio']);
                        $cemit->setEndereco($data['enderecoCemiterio']);
                        $cemit->setTipo($this->getEm()->getReference('Admin\Entity\Tipo', 1));
                        $cemit->setDataCadastro($data['dataCadastro']);
                        $cemit->setNumero('');
                        $cemit->setBairro('');
                        $cemit->setCep('');
                        $cemit->setPrimeiroTelefone('');
                        $cemit->setSegundoTelefone('');
                        $cemit->setCategoria('');
                        $cemit->setEmail('');
                        $cemit->setObservacao('');
                        $cemit->setAtivo(1);
                        $this->getEm()->persist($cemit);
                        $this->getEm()->flush();
                        $variavel =$cemit->getId();
               }
                        $sepultamento = array();
                        $sepultamento['cemiterio'] =  $variavel;  
                        $sepultamento['id'] = '';
                        $sepultamento['contratacao'] = $this->getEm()->getReference('Agencia\Entity\Contratacao', $contratacao->id);
                        $sepultamento['dataCadastro'] = new \DateTime('now');
                        $sepultamento['dadosObitoVelorio'] = $this->getEm()->getReference('Agencia\Entity\DadosObitoVelorio', $obitoVelorio->getId());
                        $sepultamento['dataSepultamento'] = new \DateTime($this->getDateTimeFormat()->formatUs($data['dtSepultamento']));
                        $sepultamento['horaSepultamento'] = $data['hrSepultamento'];
                        
                        $this->sepultamentoService->save($sepultamento);
           
               
               
               
            
            
            
           }
           /*Cremação**/
           if($data['tipoDestinoFinal']==2){
                
           if($data['hiddenCrematorio']){
                   
                   $crematorios = $this->getEm()->getReference('Admin\Entity\Cemiterio', ['id' => $data['hiddenCrematorio']]);

                 if($crematorios){
                    $data['cemiterio'] = $crematorios->getId();
                    $variavel = $crematorios->getId();
                 }
                
                   
               }else if($data['crematorio']!==''){
                
                   //
                        $cremat = new \Admin\Entity\Cemiterio();
                        $cremat->setNome($data['crematorio']);
                        $cremat->setEndereco($data['enderecoCrematorio']);
                        $cremat->setTipo($this->getEm()->getReference('Admin\Entity\Tipo', 2));
                        $cremat->setDataCadastro($data['dataCadastro']);
                        $cremat->setNumero('');
                        $cremat->setBairro('');
                        $cremat->setCep('');
                        $cremat->setPrimeiroTelefone('');
                        $cremat->setSegundoTelefone('');
                        $cremat->setCategoria('');
                        $cremat->setEmail('');
                        $cremat->setObservacao('');
                        $cremat->setAtivo(1);
                        $this->getEm()->persist($cremat);
                        $this->getEm()->flush();
                        $variavel = $cremat->getId();
                        
               }
                        $cremacao = array();
                        $cremacao['cemiterio'] = $variavel;
                        $cremacao['id'] = '';
                        $cremacao['contratacao'] = $this->getEm()->getReference('Agencia\Entity\Contratacao', $contratacao->id);
                        $cremacao['dataCadastro'] = new \DateTime('now');
                        $cremacao['dadosObitoVelorio'] = $this->getEm()->getReference('Agencia\Entity\DadosObitoVelorio', $obitoVelorio->getId());
                        $cremacao['dataCremacao'] = new \DateTime($this->getDateTimeFormat()->formatUs($data['dtCremacao']));
                        $cremacao['horaCremacao'] = $data['hrCremacao'];
                        $cremacao['endereco'] = $data['enderecoCrematorio'];
                        $cremacao['crematorio'] = $data['crematorio'];
                        
                        $this->cremacaoService->save($cremacao);
               
               
            
            
            
           }
           //print_r($data['corpoVelado']);die;
           
            if($data['corpoVelado']=='S'){
                 
                $velado = array();
                $velado['id'] = '';
                $velado['dadosObitoVelorio'] = $this->getEm()->getReference('Agencia\Entity\DadosObitoVelorio', $obitoVelorio->getId());
                $velado['contratacao'] = $this->getEm()->getReference('Agencia\Entity\Contratacao', $contratacao->id);
                $velado['localVelorio'] = $data['localVelorio'];
                $velado['dataCadastro'] = new \DateTime('now');
                $velado['endereco'] = $data['enderecoVelorio'];
                $velado['dataSaida'] = new \DateTime($this->getDateTimeFormat()->formatUs($data['dtSaida']));
                $velado['horaSaida'] = $data['hrSaida'];        
                $velorios  = $this->velorioService->save($velado);
                 
                if (!$velorios) {
                    $this->getEm()->rollback();
                    return false;
                }
                     
                  
            }
           
            $this->getEm()->commit();
            return true;
            
        } catch (\Exception $ex) {
            die($ex->getMessage());
            $this->log->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            $this->getEm()->rollback();
            return false;
        }
    }
}