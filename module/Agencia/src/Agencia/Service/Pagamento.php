<?php
namespace Agencia\Service;

use Application\Service\BaseApplicationService;
use Zend\Session\Container as SessionConteiner;
use Agencia\Repository\BandeiraRepository;
use Agencia\Repository\PagamentoRepository;

class Pagamento extends BaseApplicationService
{

    private $bandeiraRepository;
    private $contratacaoService;
    private $pagamentoRepository;
    /**
     * 
     * @param BandeiraRepository $bandeiraRepository
     * @return \Agencia\Service\Bandeira
     */
    function setBandeiraRepository(BandeiraRepository $bandeiraRepository) {
        $this->bandeiraRepository = $bandeiraRepository;
        return $this;
    }
    /**
     * 
     * @param PagamentoRepository $pagamentoRepository
     * @return \Agencia\Service\Pagamento
     */
    function setPagamentoRepository(PagamentoRepository $pagamentoRepository) {
        $this->pagamentoRepository = $pagamentoRepository;
        return $this;
    }
    /**
     * @param mixed $contratacaoService
     */
    public function setContratacaoService($contratacaoService)
    {
        $this->contratacaoService = $contratacaoService;
        return $this;
    }
    
    public function save(array $data)
    {
        $contratacao = new SessionConteiner('Contratacao');

        try {
            $this->getEm()->beginTransaction();
            
                for($i = 0; $i < count($data['emitente']); $i++)
                  {
                    $pag = array();
                    
     
                      $pag['contratacao']    = $this->getEm()->getReference('Agencia\Entity\Contratacao', ['id' => $contratacao->id]);
                      $pag['tipoPagamento']  = $this->getEm()->getReference('Agencia\Entity\TipoPagamento', ['id' => $data['tipoPagamento'][$i]]);
                      $pag['nome']           = $data['nome'][$i];
                      $pag['numeroDocumento']= $data['documento'][$i];
                      $pag['telefone']       = $data['telefoneResidencial'][$i];
                      $pag['nome']           = $data['nome'][$i];
                      $pag['endereco']       = $data['endereco'][$i];
                      $pag['pinpad']         = $data['pinpad'][$i];
                      $pag['cvdoc']          = $data['cvdoc'][$i];
                      $pag['autorizacao']    = $data['autorizacao'][$i];
                       if($data['bandeira'][$i]){
                            $pag['bandeira'] = $this->getEm()->getReference('Agencia\Entity\Bandeira', $data['bandeira'][$i]);
                      }
                      $pag['parcelamento']   = $data['parcelamento'][$i];
                      $pag['valorPagamento'] = $data['valor'][$i];
                      $pag['emitente']       = $this->getEm()->getReference('Agencia\Entity\Emitente', ['id' => $data['emitente'][$i]]);
                     
                      $pagamento = parent::save($pag);
                  
            
                        if (!$pagamento) {
                               $this->getEm()->rollback();
                               return false;
                       }

                  }
                //  print($pagamento->getValorPagamento());die;
                  
                  $contrata = array();
                  
                  $contrata['id']= $contratacao->id;
                  $contrata['totalContratacao']=$pagamento->getValorPagamento();
                  
                  $contr = $this->contratacaoService->update($contrata);
                
                    if (!$contr) {
                        $this->getEm()->rollback();
                        return false;
                    }
                      
            $this->getEm()->commit();
            return true;        
                  
                  
        } catch (\Exception $ex) {
            $this->log->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            $this->getEm()->rollback();
            return false;
        }
       
        
     
        
        
        
        
       
    }
}