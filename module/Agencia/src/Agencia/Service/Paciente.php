<?php

namespace Agencia\Service;

use Application\Service\BaseApplicationService;
use Zend\Session\Container as SessionConteiner;

class Paciente extends BaseApplicationService
{
   
    public function save(array $data)
    {
             try {
        $contratacao = new SessionConteiner('Contratacao');
        
        $data['contratacao'] = $this->getEm()->getReference('Agencia\Entity\Contratacao', ['id' => $contratacao->id]);
        $data['contratante'] = $this->getEm()->getRepository('Agencia\Entity\Contratante')->findOneBy(array('contratacao' =>  $contratacao->id));
        $data['dataCadastro'] = new \DateTime('now');
        
        $peca = $data['pecasAnatomicas'];
        unset($data['pecasAnatomicas']);
        if(isset($peca)){
             if(($peca==2) or ($peca==0)){        
                $data['nomePaciente'] = $data['nome'];
                $data['membroAmputado'] = $this->getEm()->getReference('Agencia\Entity\Membro', $data['membroAmputado'])->getMembro();
                $data['pecasAnatomicas'] = 0;
             }else{
                 $data['pecasAnatomicas'] = 1;
                }
        } else {
            $data['nomePaciente'] = $data['nome'];
            $data['membroAmputado'] = $this->getEm()->getReference('Agencia\Entity\Membro', $data['membroAmputado'])->getMembro();
            $data['pecasAnatomicas'] = 1;
        }
        
       
        
           
            parent::save($data);
            
            

        } catch (\Exception $ex) {
            $this->log->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            $this->getEm()->rollback();
            return false;
        }
    }

}
