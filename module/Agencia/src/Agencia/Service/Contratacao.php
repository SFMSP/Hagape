<?php


namespace Agencia\Service;

use Application\Service\BaseApplicationService;
use Zend\Session\Container as SessionConteiner;

class Contratacao extends BaseApplicationService
{
    private $localizacao;
    private $dadosEmpresa;
    private $contratacao;

    /**
     * @param mixed $localizacao
     */
    public function setLocalizacaoService($localizacao)
    {
        $this->localizacao = $localizacao;
        return $this;
    }

    /**
     * @param mixed $dadosEmpresa
     */
    public function setDadosEmpresaService($dadosEmpresa)
    {
        $this->dadosEmpresa = $dadosEmpresa;
        return $this;
    }

    public function setContratacao($contratacao)
    {
        $this->contratacao = $contratacao;
        return $this;
    }


    public function save(array $data)
    {
        $vlTotalContratacao = 0;
        if (array_key_exists('id', $data)) {

        } else {
            $data['id'] = '';
        }

        $data['dataCadastro'] = new \DateTime('now');
        $data['numeroNota'] = $this->dadosUser['filtrosSelecionados']['agencia'];
        $data['numeroNota'] .= '-';
        $data['numeroNota'] .= $this->getEm()->getRepository('Agencia\Entity\Contratacao')->recuperaNumeroSequencialNota($data['dataCadastro']->format('Y'));
        $data['numeroNota'] .= "/" . $data['dataCadastro']->format('Y');
        $data['usuario'] = $this->dadosUser['idUsuario'];
        $data['agencia'] = $this->dadosUser['filtrosSelecionados']['agencia'];
        $data['totalContratacao'] = $vlTotalContratacao;
        $data['contratacaoOrigem'] = $this->dadosUser['filtrosSelecionados']['agencia'];
        $data['tipoOperacao'] = $this->getEm()->getRepository('Agencia\Entity\TipoOperacao')->findOneBy(['id' => $data['operacao']]);
        
        if (array_key_exists('destino', $data) && isset($data['destino'])) {
              
            $data['tipoDestinoFinal'] = $this->getEm()->getReference('Agencia\Entity\TipoDestinoFinal', ['id' => $data['destino']]);
        } else {
           
            $data['tipoContratacao'];
        }
        if (array_key_exists('tipoContratacao', $data) && $data['tipoContratacao'] != '') {
            
            $data['tipoContratacao'] = $this->getEm()->getReference('Agencia\Entity\TipoContratacao', ['id' => $data['tipoContratacao']]);
        } else {
            
            $data['tipoContratacao'] = null;
        }

        $data['situacao'] = \Agencia\Entity\Contratacao::STATUS_PENDENTE;
       

        try {
            $this->getEm()->beginTransaction();
            //Insere Contratacao
            //var_dump($data['situacao']);die;
            $entity = parent::save($data);
            if (!$entity) {
                $this->getEm()->rollback();
                return false;
            } else {
                $contratacao = new SessionConteiner('Contratacao');
                if ($contratacao->id) {
                    $contratacao->getManager()->getStorage()->clear('Contratacao');
                }
                $contratacao->id = $entity->getId();
                $contratacao->numeroNota = $data['numeroNota'];
                $contratacao->usuario = $data['usuario'];
                $contratacao->agencia = $data['agencia'];
                $contratacao->totalContratacao = $data['totalContratacao'];
                $contratacao->contratacaoOrigem = $data['contratacaoOrigem'];
                $contratacao->tipoOperacao = $data['tipoOperacao']->getId();
              
                
                if ($data['tipoContratacao'] != null) {
                    $contratacao->tipoContratacao = $data['tipoContratacao']->getId();
                }
                if ($data['tipoDestinoFinal'] != null) {
                    $contratacao->tipoDestinoFinal = $data['tipoDestinoFinal']->getId();
                }
                //print_r($data['situacao']);die;
                $contratacao->situacao = $data['situacao'];

                $this->getEm()->commit();
                return true;
            }
        } catch (\Exception $ex) {
            $this->log->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            $this->getEm()->rollback();
            return false;
        }
    }

    public function delete($id)
    {
//        $dadosEmpresa = $this->em->getRepository('Agencia\Entity\DadosEmpresa')->findOneBy(['contratante' => $id]);
//        if ($dadosEmpresa) {
//            $dadosEmpresaEntity = $this->dadosEmpresa->delete($dadosEmpresa->getId());
//            if ($dadosEmpresaEntity) {
//                return false;
//            }
//        }
    }

}