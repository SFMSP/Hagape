<?php

namespace Agencia\Service;

use Application\Service\BaseApplicationService;
use Zend\Session\Container as SessionConteiner;

class Falecido extends BaseApplicationService
{
    private $filhoFalecidoService;
    private $certidaoNascimentoService;
    private $casamentoService;
    private $nascimentoObitoService;
    private $localizacaoService;

    /**
     * @param mixed $localizacaoService
     */
    public function setLocalizacaoService($localizacaoService)
    {
        $this->localizacaoService = $localizacaoService;
    }


    /**
     * @param mixed $nascimentoService
     */
    public function setNascimentoObitoService($nascimentoObitoService)
    {
        $this->nascimentoObitoService = $nascimentoObitoService;
        return $this;
    }

    /**
     * @param mixed $filhoFalecidoService
     */
    public function setFilhoFalecidoService($filhoFalecidoService)
    {
        $this->filhoFalecidoService = $filhoFalecidoService;
        return $this;
    }

    /**
     * @param mixed $certidaoNascimentoService
     */
    public function setCertidaoNascimentoService($certidaoNascimentoService)
    {
        $this->certidaoNascimentoService = $certidaoNascimentoService;
        return $this;
    }

    /**
     * @param mixed $casamentoService
     */
    public function setCasamentoService($casamentoService)
    {
        $this->casamentoService = $casamentoService;
        return $this;
    }

    public function save(array $data)
    {
        $contratacao = new SessionConteiner('Contratacao');

        $data['cor'] = $this->getEm()->getRepository('Agencia\Entity\Etnia')->findOneBy(array('id' => $data['raca']))->getId();
        $data['natMorto'] = ($data['nascimentoObito'] == 'N') ? 0 : 1;
        $data['rg'] = $data['numeroDocumento'];
        $data['dataNascimento'] = new \DateTime($this->getDateTimeFormat()->formatUs($data['dataNascimento']));
        $data['estadoCivil'] = $this->getEm()->getRepository('Agencia\Entity\EstadoCivil')->findOneBy(array('id' => $data['estadoCivil']))->getId();
        $data['bensInventario'] = $data['bensInventariar'];
        $data['deixaFilho'] = $data['deixaFilhos'];
        $data['dataCadastro'] = new \DateTime('now');
        $data['estadoCivilMae'] = $this->getEm()->getRepository('Agencia\Entity\EstadoCivil')->findOneBy(array('id' => $data['estadoCivilMae']));;
        $data['contratacao'] = $this->getEm()->getRepository('Agencia\Entity\Contratacao')->findOneBy(array('id' => $contratacao->id));
        $data['contratante'] = $this->getEm()->getRepository('Agencia\Entity\Contratante')->findOneBy(array('contratacao' => $data['contratacao']));
        $data['declarante'] = $this->getEm()->getRepository('Agencia\Entity\Declarante')->findOneBy(array('contratacao' => $data['contratacao']));
        $data['estadoCivilPai'] = $this->getEm()->getRepository('Agencia\Entity\EstadoCivil')->findOneBy(array('id' => $data['estadoCivilPai']));;

        try {
            $this->getEm()->beginTransaction();
            //Salvando dados do Falecido
            $entityFalecido = parent::save($data);
            if (!$entityFalecido) {
                $this->getEm()->rollback();
                return false;
            }

            $localizacaoReference = $this->getEm()->getRepository('Agencia\Entity\Localizacao')->findOneBy(array('falecido' => $entityFalecido));
            if ($localizacaoReference) {
                $arLocalizacao['id'] = $localizacaoReference->getId();
            }

            $arLocalizacao['endereco'] = $data['endereco'];
            $arLocalizacao['bairro'] = $data['bairro'];
            $arLocalizacao['numero'] = $data['numero'];
            $arLocalizacao['cidade'] = $data['cidade'];
            $arLocalizacao['complemento'] = $data['complemento'];
            $arLocalizacao['cep'] = $data['cep'];
            $arLocalizacao['dataCadastro'] = new \DateTime('now');
            $arLocalizacao['declarante'] = null;
            $arLocalizacao['falecido'] = $this->getEm()->getRepository('Agencia\Entity\Falecido')->findOneBy(array('id' => $entityFalecido->getId()));

            $entityLocalizacao = $this->localizacaoService->save($arLocalizacao);
            if (!$entityLocalizacao) {
                $this->getEm()->rollback();
                return false;
            }

            if ($data['estadoCivil'] == 1) {
                $certidaoReference = $this->getEm()->getRepository('Agencia\Entity\CertidaoNascimento')->findOneBy(array('falecido' => $entityFalecido));
                if ($certidaoReference) {
                    $arDadosCertidaoNascimento['id'] = $certidaoReference->getId();
                }
                $arDadosCertidaoNascimento['cartorio'] = $data['cartorio'];
                $arDadosCertidaoNascimento['cidade'] = $data['cidadeCartorio'];
                $arDadosCertidaoNascimento['livro'] = $data['livro'];
                $arDadosCertidaoNascimento['folha'] = $data['folha'];
                $arDadosCertidaoNascimento['numero'] = $data['numeroLivro'];
                $arDadosCertidaoNascimento['dataCadastro'] = new \DateTime('now');
                $arDadosCertidaoNascimento['falecido'] = $this->getEm()->getRepository('Agencia\Entity\Falecido')->findOneBy(array('id' => $entityFalecido->getId()));
                //Salvando dados de Solteiro Certidao de nascimento
                $entityCertidaNascimento = $this->certidaoNascimentoService->save($arDadosCertidaoNascimento);
                if (!$entityCertidaNascimento) {
                    $this->getEm()->rollback();
                    return false;
                }
            } elseif ($data['estadoCivil'] == 2) {
                $casamentoReference = $this->getEm()->getRepository('Agencia\Entity\Casamento')->findBy(array('falecido' => $entityFalecido));
                //Deletando os casamentos
                if ($casamentoReference) {
                    foreach ($casamentoReference as $casamentoDelete) {
                        $entityCasamento = $this->casamentoService->delete($casamentoDelete->getId());
                        if (!$entityCasamento) {
                            $this->getEm()->rollback();
                            return false;
                        }
                    }
                }
                foreach ($data['nomeConjuges'] as $key => $conjuge) {
                    if ($conjuge) {
                        $arDadosCasamentos['dataCadastro'] = new \DateTime('now');
                        $arDadosCasamentos['nomeConjuge'] = $conjuge;
                        $arDadosCasamentos['dataCasamento'] = new \DateTime($this->getDateTimeFormat()->formatUs($data['dataCasamentos'][$key]));
                        $arDadosCasamentos['cartorio'] = $data['cartorioCasamentos'][$key];
                        $arDadosCasamentos['cidade'] = $data['cidadeCasamentos'][$key];
                        $arDadosCasamentos['ufCidade'] = $data['ufCidades'][$key];
                        $arDadosCasamentos['livro'] = $data['livroCasamentos'][$key];
                        $arDadosCasamentos['folha'] = $data['folhaCasamentos'][$key];
                        $arDadosCasamentos['numero'] = $data['numeroLivroCasamentos'][$key];
                        $arDadosCasamentos['falecido'] = $this->getEm()->getRepository('Agencia\Entity\Falecido')->findOneBy(array('id' => $entityFalecido->getId()));
                        //Salvando dados de Casamento
                        $entityCasamento = $this->casamentoService->save($arDadosCasamentos);
                        if (!$entityCasamento) {
                            $this->getEm()->rollback();
                            return false;
                        }
                    }
                }
            }

            if ($data['deixaFilho']) {
                $filhoReference = $this->getEm()->getRepository('Agencia\Entity\FilhoFalecido')->findBy(array('falecido' => $entityFalecido));
                //Deletando os Filhos
                if ($filhoReference) {
                    foreach ($filhoReference as $filhoDelete) {
                        $entityFilhoFalecido = $this->filhoFalecidoService->delete($filhoDelete->getId());
                        if (!$entityFilhoFalecido) {
                            $this->getEm()->rollback();
                            return false;
                        }
                    }
                }

                foreach ($data['nomeFilhos'] as $key => $filhos) {
                    if ($filhos) {
                        $arDadosFilhos['nomeFilho'] = $filhos;
                        $arDadosFilhos['idade'] = $data['maoiridadeFilhos'][$key];
                        $arDadosFilhos['observacao'] = $data['observacaoFilhos'][$key];
                        $arDadosFilhos['dataCadastro'] = new \DateTime('now');
                        $arDadosFilhos['falecido'] = $this->getEm()->getRepository('Agencia\Entity\Falecido')->findOneBy(array('id' => $entityFalecido->getId()));
                        //Salvando dados de Casamento
                        $entityFilhoFalecido = $this->filhoFalecidoService->save($arDadosFilhos);
                        if (!$entityFilhoFalecido) {
                            $this->getEm()->rollback();
                            return false;
                        }
                    }
                }
            }

            if ($data['nascimentoObito'] == 'S') {
                $nascimentoObitoReference = $this->getEm()->getRepository('Agencia\Entity\Casamento')->findOneBy(array('falecido' => $entityFalecido));
                if ($nascimentoObitoReference) {
                    $arNascimentoObito['id'] = $nascimentoObitoReference->getId();
                }
                $arNascimentoObito['dataCadastro'] = new \DateTime('now');
                $arNascimentoObito['local'] = $data['localNascimento'];
                $arNascimentoObito['dataObito'] = new \DateTime($this->getDateTimeFormat()->formatUs($data['dataNascimentoNascObito']));
                $arNascimentoObito['horario'] = $data['horaNascimento'];
                $arNascimentoObito['avoMPaterno'] = $data['avoPaterno'];
                $arNascimentoObito['avoMMaterno'] = $data['avoMaterno'];
                $arNascimentoObito['avoFPaterno'] = $data['avoPaterna'];
                $arNascimentoObito['avoFMaterno'] = $data['avoMaterna'];
                $arNascimentoObito['gestacao'] = $data['gestacao'];
                $arNascimentoObito['gravidez'] = $data['gravidez'];
                $arNascimentoObito['falecido'] = $this->getEm()->getRepository('Agencia\Entity\Falecido')->findOneBy(array('id' => $entityFalecido->getId()));
                //Salvando dados de Nascimento seguido de Obito
                $entityNascimentoObito = $this->nascimentoObitoService->save($arNascimentoObito);
                if (!$entityNascimentoObito) {
                    $this->getEm()->rollback();
                    return false;
                }
            }

            $this->getEm()->commit();
            return true;

        } catch (\Exception $ex) {
            $this->log->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            $this->getEm()->rollback();
            return false;
        }
    }
}