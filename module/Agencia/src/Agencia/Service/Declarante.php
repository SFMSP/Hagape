<?php

namespace Agencia\Service;

use Application\Service\BaseApplicationService;
use Zend\Session\Container as SessionConteiner;

class Declarante extends BaseApplicationService
{
    private $localizacaoService;
    private $dadosEmpresaService;

    /**
     * @param mixed $dadosEmpresaService
     */
    public function setDadosEmpresaService($dadosEmpresaService)
    {
        $this->dadosEmpresaService = $dadosEmpresaService;
        return $this;
    }

    public function setLocalizacaoService($localizacaoService)
    {
        $this->localizacaoService = $localizacaoService;
        return $this;
    }

    public function save(array $data)
    {
        
      
        $contratacao = new SessionConteiner('Contratacao');

        $data['dataCadastro'] = new \DateTime('now');
        $data['contratacao'] = $this->getEm()->getReference('Agencia\Entity\Contratacao', ['id' => $contratacao->id]);
        $data['nome'] = $data['nome'];
        $data['email'] = $data['email'];

        try {
            $this->getEm()->beginTransaction();
            //Insere Contratante
            $entityDeclarante = parent::save($data);
            if (!$entityDeclarante) {
                $this->getEm()->rollback();
                return false;
            }

            $localizacaoReference = $this->getEm()->getRepository('Agencia\Entity\Localizacao')->findBy(array('declarante' => $entityDeclarante));
            if ($localizacaoReference) {
                $arLocalizacao['id'] = $localizacaoReference[0]->getId();
            }

            $arLocalizacao['endereco'] = $data['endereco'];
            $arLocalizacao['bairro'] = $data['bairro'];
            $arLocalizacao['numero'] = $data['numero'];
            $arLocalizacao['cidade'] = $data['cidade'];
            $arLocalizacao['complemento'] = $data['complemento'];
            $arLocalizacao['cep'] = $data['cep'];
            $arLocalizacao['dataCadastro'] = new \DateTime('now');
            $arLocalizacao['declarante'] = $this->getEm()->getReference('Agencia\Entity\Declarante', ['id' => $entityDeclarante->getId()]);
            $arLocalizacao['contratante'] = null;

            $entityLocalizacao = $this->localizacaoService->save($arLocalizacao);
            if (!$entityLocalizacao) {
                $this->getEm()->rollback();
                return false;
            }

            $dadosEmpresaReference = $this->getEm()->getRepository('Agencia\Entity\DadosEmpresa')->findBy(array('declarante' => $entityDeclarante));
            if ($dadosEmpresaReference) {
                $arDadosEmpresa['id'] = $dadosEmpresaReference[0]->getId();
            }

            $arDadosEmpresa['nomeEmpresarial'] = $data['empresa'];
            $arDadosEmpresa['cnpj'] = $data['cnpj'];
            $arDadosEmpresa['dataCadastro'] = new \DateTime('now');
            $arDadosEmpresa['telefone'] = $data['telefoneEmpresa'];
            $arDadosEmpresa['contratacao'] = $this->getEm()->getReference('Agencia\Entity\Contratacao', ['id' => $contratacao->id]);
            $arDadosEmpresa['placaVeiculo'] = $data['placaVeiculo'];
            $arDadosEmpresa['nomeMotorista']= $data['nomeMotorista'];
            $arDadosEmpresa['rgMotorista']= $data['rgMotorista'];

            $entityDadosEmpresa = $this->dadosEmpresaService->save($arDadosEmpresa);
            if (!$entityDadosEmpresa) {
                $this->getEm()->rollback();
                return false;
            }


            $this->getEm()->commit();
            return true;

        } catch (\Exception $ex) {
            $this->log->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            $this->getEm()->rollback();
            return false;
        }
    }

}
