<?php

namespace Agencia\Service;

use Application\Service\BaseApplicationService;
use Zend\Session\Container as SessionConteiner;

class NotaContratacao extends BaseApplicationService
{
    private $localizacaoService;
    private $dadosEmpresaService;

    /**
     * @param mixed $dadosEmpresaService
     */
    public function setDadosEmpresaService($dadosEmpresaService)
    {
        $this->dadosEmpresaService = $dadosEmpresaService;
        return $this;
    }

    public function setLocalizacaoService($localizacaoService)
    {
        $this->localizacaoService = $localizacaoService;
        return $this;
    }

    public function save(array $data)
    {
        $contratacao = new SessionConteiner('Contratacao');

        $data['dataCadastro'] = new \DateTime('now');
        $data['contratacao'] = $this->getEm()->getReference('Agencia\Entity\Contratacao', ['id' => $contratacao->id]);
        $data['nomeContratante'] = $data['nome'];

        try {
            $this->getEm()->beginTransaction();
            //Insere Contratante
            $entityContratante = parent::save($data);
            if (!$entityContratante) {
                $this->getEm()->rollback();
                return false;
            }

            $localizacaoReference = $this->getEm()->getRepository('Agencia\Entity\Localizacao')->findBy(array('contratante' => $entityContratante));
            if($localizacaoReference){
                $arLocalizacao['id'] = $localizacaoReference[0]->getId();
            }

            $arLocalizacao['endereco'] = $data['endereco'];
            $arLocalizacao['bairro'] = $data['bairro'];
            $arLocalizacao['numero'] = $data['numero'];
            $arLocalizacao['cidade'] = $data['cidade'];
            $arLocalizacao['complemento'] = $data['complemento'];
            $arLocalizacao['cep'] = $data['cep'];
            $arLocalizacao['dataCadastro'] = new \DateTime('now');
            $arLocalizacao['declarante'] = null;
            $arLocalizacao['contratante'] = $this->getEm()->getReference('Agencia\Entity\Contratante', ['id' => $entityContratante->getId()]);

            $entityLocalizacao = $this->localizacaoService->save($arLocalizacao);
            if (!$entityLocalizacao) {
                $this->getEm()->rollback();
                return false;
            }

            $dadosEmpresaReference = $this->getEm()->getRepository('Agencia\Entity\DadosEmpresa')->findBy(array('contratante' => $entityContratante));
            if ($dadosEmpresaReference) {
                $arDadosEmpresa['id'] = $dadosEmpresaReference[0]->getId();
            }
            if ($data['empresaAcompanha'] == 'S') {
                $arDadosEmpresa['nomeEmpresarial'] = $data['empresa'];
                $arDadosEmpresa['cnpj'] = $data['cnpj'];
                $arDadosEmpresa['dataCadastro'] = new \DateTime('now');
                $arDadosEmpresa['endereco'] = $data['enderecoEmpresa'];
                $arDadosEmpresa['telefone'] = $data['telefoneEmpresa'];
                $arDadosEmpresa['representante'] = $data['representante'];
                $arDadosEmpresa['contratante'] = $this->getEm()->getReference('Agencia\Entity\Contratante', ['id' => $entityContratante->getId()]);
                $arDadosEmpresa['contratacao'] = $this->getEm()->getReference('Agencia\Entity\Contratacao', ['id' => $contratacao->id]);
                $entityDadosEmpresa = $this->dadosEmpresaService->save($arDadosEmpresa);
                if (!$entityDadosEmpresa) {
                    $this->getEm()->rollback();
                    return false;
                }
            }else{
                $entityDadosEmpresa = $this->dadosEmpresaService->delete($arDadosEmpresa);
                if (!$entityDadosEmpresa) {
                    $this->getEm()->rollback();
                    return false;
                }
            }

            $this->getEm()->commit();
            return true;

        } catch (\Exception $ex) {
            $this->log->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            $this->getEm()->rollback();
            return false;
        }
    }

}
