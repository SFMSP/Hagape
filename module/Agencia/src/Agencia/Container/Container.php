<?php

namespace Agencia\Container;

use Zend\Session\Container as SessionContainer;
use Base\Container\BaseContainer as BaseContainerInterface;

/**
 * Classe que armazena todas as dependências para serem chamados na classe Module
 *
 * @author Praxedes
 */
class Container implements BaseContainerInterface
{

    /**
     *
     * Retorna as instâncias dos repositórios
     * @return array
     */
    public function getForms()
    {
        return array(
            'Agencia\Form\Contratacao' => function ($sm) {
                $t = new \Agencia\Entity\Contratacao();
                $inputFilter = new \Agencia\InputFilter\Contratacao();
                $operacao = $sm->get('Agencia\Repository\TipoOperacaoRepository')->getArraySelect('Selecione', array(), true, '');
                $destino = $sm->get('Agencia\Repository\TipoDestinoFinalRepository')->getArraySelect('Selecione', array(), true, '');
                $form = new \Agencia\Form\Contratacao($inputFilter, $operacao, $destino);
                $form->setContratacaoRepository($sm->get('Agencia\Repository\ContratacaoRepository'));

                return $form;
            },
            'Agencia\Form\Contratante' => function ($sm) {
                $t = new \Agencia\Entity\Contratante();
                $estados = $sm->get('Application\Repository\UfRepository')->getArraySelect('Selecione um Estado');
                $processoRepository = $sm->get('Agencia\Repository\ProcessoRepository');
                $contratacao = new SessionContainer('Contratacao');
                $arCampos = $sm->get('Agencia\Repository\ProcessoCampoRepository')->recuperaObrigatoriedadeCamposProcesso(2, $contratacao->tipoOperacao, $contratacao->tipoContratacao, $contratacao->tipoDestinoFinal);
                $inputFilter = new \Agencia\InputFilter\Contratante($arCampos);
                $form = new \Agencia\Form\Contratante($inputFilter, $contratacao, $estados, $processoRepository,$arCampos);
                return $form;
            },
            'Agencia\Form\AgenciaFiltro' => function ($sm) {
                $agencia = $sm->get('Admin\Repository\AgenciaRepository');
                return new \Agencia\Form\AgenciaFiltro($agencia->getArraySelect("Agência"));
            },
            'Agencia\Form\DadosObito' => function ($sm) {
                $t = new \Agencia\Entity\DadosObito();
                $inputFilter = new \Agencia\InputFilter\DadosObito();
                $estados = $sm->get('Application\Repository\UfRepository')->getArraySelect();
                $medicos = $sm->get('Agencia\Repository\MedicoRepository')->getArraySelect();
                $transporte = $sm->get('Agencia\Repository\ServicoRepository')->getArraySelect('Selecione um Transporte', [], true, 'WHERE t.categoria = 2');
                $carro = $sm->get('Agencia\Repository\ServicoRepository')->getArraySelect('Selecione um Transporte', [], true, 'WHERE t.categoria = 3');
                $processoRepository = $sm->get('Agencia\Repository\ProcessoRepository');
               
                $contratacao = new SessionContainer('Contratacao');
                $arCampos = $sm->get('Agencia\Repository\ProcessoCampoRepository')->recuperaObrigatoriedadeCamposProcesso(4, $contratacao->tipoOperacao, $contratacao->tipoContratacao, $contratacao->tipoDestinoFinal);
              // print_r($arCampos);die;
                $inputFilter = new \Agencia\InputFilter\DadosObito($arCampos);
                $form = new \Agencia\Form\DadosObito($inputFilter, $contratacao, $estados, $processoRepository, $medicos,$arCampos,$transporte,$carro);
                return $form;
            },
            'Agencia\Form\Falecido' => function ($sm) {
                $t = new \Agencia\Entity\Falecido();
                $processoRepository = $sm->get('Agencia\Repository\ProcessoRepository');
                $estados = $sm->get('Application\Repository\UfRepository')->getArraySelect();
                $raca = $sm->get('Agencia\Repository\EtniaRepository')->getArraySelect('Selecione uma Etnia', [], true, '');
                $contratacao = new SessionContainer('Contratacao');
                $arCampos = $sm->get('Agencia\Repository\ProcessoCampoRepository')->recuperaObrigatoriedadeCamposProcesso(3, $contratacao->tipoOperacao, $contratacao->tipoContratacao, $contratacao->tipoDestinoFinal);
                $inputFilter = new \Agencia\InputFilter\Falecido($arCampos);
                $form = new \Agencia\Form\Falecido($inputFilter, $contratacao,$processoRepository,$arCampos,$raca,$estados);
                return $form;
            },
            'Agencia\Form\Declarante' => function ($sm) {
                $t = new \Agencia\Entity\Declarante();
                $estados = $sm->get('Application\Repository\UfRepository')->getArraySelect('Selecione um Estado');
                $processoRepository = $sm->get('Agencia\Repository\ProcessoRepository');
                $contratacao = new SessionContainer('Contratacao');
                $arCampos = $sm->get('Agencia\Repository\ProcessoCampoRepository')->recuperaObrigatoriedadeCamposProcesso(1, $contratacao->tipoOperacao, '', '');
                
                $inputFilter = new \Agencia\InputFilter\Declarante($arCampos);
                $cidades = $sm->get('Application\Repository\CidadeRepository')->getArraySelect();
                $form = new \Agencia\Form\Declarante($inputFilter, $contratacao, $estados, $processoRepository,$arCampos,$cidades);
                return $form;
            },
            'Agencia\Form\ProdutoServico' => function ($sm) {
                $t = new \Agencia\Entity\ProdutoVendido();
                $processoRepository = $sm->get('Agencia\Repository\ProcessoRepository');
                $contratacao = new SessionContainer('Contratacao');
                $arCampos = $sm->get('Agencia\Repository\ProcessoCampoRepository')->recuperaObrigatoriedadeCamposProcesso(8, $contratacao->tipoOperacao, $contratacao->tipoContratacao, $contratacao->tipoDestinoFinal);
                $inputFilter = new \Agencia\InputFilter\ProdutoServico($arCampos);
                $form = new \Agencia\Form\ProdutoServico($inputFilter, $contratacao, $processoRepository,$arCampos);
                return $form;
            },
            'Agencia\Form\DadosObitoVelorio' => function ($sm) {
                $t = new \Agencia\Entity\DadosObitoVelorio();
                $inputFilter = new \Agencia\InputFilter\DadosObitoVelorio();
                $estados = $sm->get('Application\Repository\UfRepository')->getArraySelect();
                $medicos = $sm->get('Agencia\Repository\MedicoRepository')->getArraySelect();
                $processoRepository = $sm->get('Agencia\Repository\ProcessoRepository');            
                $contratacao = new SessionContainer('Contratacao');
                $arCampos = $sm->get('Agencia\Repository\ProcessoCampoRepository')->recuperaObrigatoriedadeCamposProcesso(11, $contratacao->tipoOperacao, $contratacao->tipoContratacao, $contratacao->tipoDestinoFinal);
                $inputFilter = new \Agencia\InputFilter\DadosObitoVelorio($arCampos);
                $form = new \Agencia\Form\DadosObitoVelorio($inputFilter, $contratacao, $estados, $processoRepository, $medicos,$arCampos);
                return $form;
            },
            'Agencia\Form\DadosAmputacao' => function ($sm) {
                $t = new \Agencia\Entity\DadosAmputacao();
                $inputFilter = new \Agencia\InputFilter\DadosAmputacao();
                $estados = $sm->get('Application\Repository\UfRepository')->getArraySelect();
                $medicos = $sm->get('Agencia\Repository\MedicoRepository')->getArraySelect();
                $processoRepository = $sm->get('Agencia\Repository\ProcessoRepository');            
                $contratacao = new SessionContainer('Contratacao');
                $arCampos = $sm->get('Agencia\Repository\ProcessoCampoRepository')->recuperaObrigatoriedadeCamposProcesso(6, $contratacao->tipoOperacao, $contratacao->tipoContratacao, $contratacao->tipoDestinoFinal);
                $inputFilter = new \Agencia\InputFilter\DadosAmputacao($arCampos);
                $form = new \Agencia\Form\DadosAmputacao($inputFilter, $contratacao, $estados, $processoRepository, $medicos,$arCampos);
                return $form;
            },
            'Agencia\Form\NotaContratacao' => function ($sm) {
                $t = new \Agencia\Entity\NotaContratacao();
                $estados = $sm->get('Application\Repository\UfRepository')->getArraySelect('Selecione um Estado');
                $processoRepository = $sm->get('Agencia\Repository\ProcessoRepository');
                $contratacao = new SessionContainer('Contratacao');
                $arCampos = $sm->get('Agencia\Repository\ProcessoCampoRepository')->recuperaObrigatoriedadeCamposProcesso(2, $contratacao->tipoOperacao, $contratacao->tipoContratacao, $contratacao->tipoDestinoFinal);
                $inputFilter = new \Agencia\InputFilter\NotaContratacao($arCampos);
                $form = new \Agencia\Form\NotaContratacao($inputFilter, $contratacao, $estados, $processoRepository,$arCampos);
                return $form;
            },
            'Agencia\Form\Paciente' => function ($sm) {
                $t = new \Agencia\Entity\Paciente();
                $contratacao = new SessionContainer('Contratacao');
                $membro = $sm->get('Agencia\Repository\MembroRepository')->getArraySelect('Selecione um Membro');
                $arCampos = $sm->get('Agencia\Repository\ProcessoCampoRepository')->recuperaObrigatoriedadeCamposProcesso(5, $contratacao->tipoOperacao, $contratacao->tipoContratacao, $contratacao->tipoDestinoFinal);
                $inputFilter = new \Agencia\InputFilter\Paciente($arCampos);
                $form = new \Agencia\Form\Paciente($inputFilter, $contratacao,$arCampos,$membro);
                return $form;
            },
            'Agencia\Form\TransladoCorpo' => function ($sm) {
                $t = new \Agencia\Entity\TransladoCorpo();
                $contratacao = new SessionContainer('Contratacao');
                $arCampos = $sm->get('Agencia\Repository\ProcessoCampoRepository')->recuperaObrigatoriedadeCamposProcesso(7, $contratacao->tipoOperacao, $contratacao->tipoContratacao, $contratacao->tipoDestinoFinal);
                $inputFilter = new \Agencia\InputFilter\TransladoCorpo($arCampos);
                $form = new \Agencia\Form\TransladoCorpo($inputFilter, $contratacao,$arCampos);
                return $form;
            },
            'Agencia\Form\Emissao' => function ($sm) {
//                 $t = new \Agencia\Entity\Emissao();
//                $estados = $sm->get('Application\Repository\UfRepository')->getArraySelect('Selecione um Estado');
                $processoRepository = $sm->get('Agencia\Repository\ProcessoRepository');
//                $contratacao = new SessionContainer('Contratacao');
//                $arCampos = $sm->get('Agencia\Repository\ProcessoCampoRepository')->recuperaObrigatoriedadeCamposProcesso(2, $contratacao->tipoOperacao, $contratacao->tipoContratacao, $contratacao->tipoDestinoFinal);
//                $inputFilter = new \Agencia\InputFilter\Emissao($arCampos);
                $form = new \Agencia\Form\Emissao($processoRepository);
                return $form;
            },
            'Agencia\Form\Pagamento' => function ($sm) {
                $t = new \Agencia\Entity\Pagamento();
                $contratacao = new SessionContainer('Contratacao');
                $arCampos = $sm->get('Agencia\Repository\ProcessoCampoRepository')->recuperaObrigatoriedadeCamposProcesso(9, $contratacao->tipoOperacao, $contratacao->tipoContratacao, $contratacao->tipoDestinoFinal);
                $inputFilter = new \Agencia\InputFilter\Pagamento($arCampos);
                $tipoPagamento = $sm->get('Agencia\Repository\TipoPagamentoRepository')->getArraySelect('Selecione um Tipo de Pagamento');
                $bandeira = $sm->get('Agencia\Repository\BandeiraRepository')->getArraySelect('Selecione uma Bandeira');
                $emitente = $sm->get('Agencia\Repository\EmitenteRepository')->getArraySelect('Selecione um Emitente');
                $form = new \Agencia\Form\Pagamento($inputFilter, $contratacao,$arCampos,$tipoPagamento,$bandeira,$emitente);
                return $form;
            },        
        );
    }

    /**
     *
     * Retorna os services com as instâncias dos repositories
     * @return array
     */
    public function getRepositories()
    {
        return array(
            'Admin\Repository\AgenciaRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Admin\Entity\Agencia');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Agencia\Repository\ContratacaoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\Contratacao');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Agencia\Repository\ServicoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\Servico');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Agencia\Repository\TipoOperacaoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\TipoOperacao');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Agencia\Repository\TipoDestinoFinalRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\TipoDestinoFinal');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Agencia\Repository\TipoContratacaoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\TipoContratacao');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Agencia\Repository\ContratanteRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\Contratante');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Agencia\Repository\ProcessoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\Processo');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Agencia\Repository\DadosObitoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\DadosObito');
                
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Agencia\Repository\ProcessoCampoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\ProcessoCampo');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Agencia\Repository\MedicoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\Medico');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Agencia\Repository\LocalFalecimentoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\LocalFalecimento');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Agencia\Repository\CartorioRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\Cartorio');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Agencia\Repository\VelorioRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\Velorio');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Agencia\Repository\DadosEmpresaRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\DadosEmpresa');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Agencia\Repository\LocalizacaoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\Localizacao');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Agencia\Repository\FalecidoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\Falecido');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Agencia\Repository\DeclaranteRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\Declarante');
                $repository->setLogger($sm->get('Base\Log\Log'));
                return $repository;
            },

            'Agencia\Repository\TransporteRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\Transporte');
                $repository->setLogger($sm->get('Base\Log\Log'));
                return $repository;
            },

            'Agencia\Repository\ProdutoVendidoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\ProdutoVendido');
                $repository->setLogger($sm->get('Base\Log\Log'));
                return $repository;
            },
            'Agencia\Repository\DadosObitoVelorioRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\DadosObitoVelorio');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Agencia\Repository\DadosAmputacaoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\DadosAmputacao');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },        
            'Agencia\Repository\NotaContratacaoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\NotaContratacao');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },       
            'Agencia\Repository\PacienteRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\Paciente');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Agencia\Repository\TransladoCorpoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\TransladoCorpo');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Agencia\Repository\FilhoFalecidoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\FilhoFalecido');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Agencia\Repository\CasamentoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\Casamento');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Agencia\Repository\EtniaRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\Etnia');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Agencia\Repository\MembroRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\Membro');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Agencia\Repository\ProcessoAbaRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\ProcessoAba');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },        
//            'Agencia\Repository\EmissaoRepository' => function ($sm) {
//                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\Emissao');
//                $repository->setLogger($sm->get('Base\Log\Log'));
//
//                return $repository;
//            }
            'Agencia\Repository\ConvenioRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\Convenio');
                $repository->setLogger($sm->get('Base\Log\Log'));
                return $repository;
            },
            'Agencia\Repository\PagamentoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\Pagamento');
                $repository->setLogger($sm->get('Base\Log\Log'));
                return $repository;
            },        
            'Agencia\Repository\SepultamentoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\Sepultamento');
                $repository->setLogger($sm->get('Base\Log\Log'));
                return $repository;
            },        
            'Agencia\Repository\CremacaoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\Cremacao');
                $repository->setLogger($sm->get('Base\Log\Log'));
                return $repository;
            },        
            'Agencia\Repository\VendaProdutoServicoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\VendaProdutoServico');
                $repository->setLogger($sm->get('Base\Log\Log'));
                return $repository;
            },
            'Agencia\Repository\TipoPagamentoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\TipoPagamento');
                $repository->setLogger($sm->get('Base\Log\Log'));
                return $repository;
            },        

                    
            'Agencia\Repository\ServicoVendidoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\ServicoVendido');
                $repository->setLogger($sm->get('Base\Log\Log'));
                return $repository;
            },        
            'Agencia\Repository\EmitenteRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\Emitente');
                $repository->setLogger($sm->get('Base\Log\Log'));
                return $repository;
            },        
            'Agencia\Repository\BandeiraRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Agencia\Entity\Bandeira');
                $repository->setLogger($sm->get('Base\Log\Log'));
                return $repository;
            },            

        );
    }

    /**
     *
     * @return array
     */
    public function getFormsFilter()
    {
        return array();
    }

    /**
     *
     * Retorna as instâncias de services
     * @return array
     */
    public function getServices()
    {
        return array(
            'Admin\Service\Agencia' => function ($sm) {
                $service = new \Admin\Service\Agencia($sm->get('Doctrine\ORM\EntityManager'), new \Admin\Entity\Agencia(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());

                return $service;
            },
            'Agencia\Service\Contratacao' => function ($sm) {
                $service = new \Agencia\Service\Contratacao($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\Contratacao(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat())
                    ->setLocalizacaoService($sm->get('Agencia\Service\Localizacao'))
                    ->setDadosEmpresaService($sm->get('Agencia\Service\DadosEmpresa'))
                    ->setContratacao(new SessionContainer('Contratacao'));

                return $service;
            },
            'Agencia\Service\Servico' => function ($sm) {
                $service = new \Agencia\Service\Servico($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\Servico(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());

                return $service;
            },
            'Agencia\Service\Contratante' => function ($sm) {
                $service = new \Agencia\Service\Contratante($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\Contratante(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat())
                    ->setLocalizacaoService($sm->get('Agencia\Service\Localizacao'))
                    ->setDadosEmpresaService($sm->get('Agencia\Service\DadosEmpresa'));

                return $service;
            },
            'Agencia\Service\Localizacao' => function ($sm) {
                $service = new \Agencia\Service\Localizacao($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\Localizacao(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());

                return $service;
            },
            'Agencia\Service\DadosEmpresa' => function ($sm) {
                $service = new \Agencia\Service\DadosEmpresa($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\DadosEmpresa(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());

                return $service;
            },
            'Agencia\Service\DadosObito' => function ($sm) {
                $service = new \Agencia\Service\DadosObito($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\DadosObito(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat())
                    ->setLocalFalecimentoService($sm->get('Agencia\Service\LocalFalecimento'))
                    ->setCartorioService($sm->get('Agencia\Service\Cartorio'))
                    ->setTransporteService($sm->get('Agencia\Service\Transporte')) 
                    ->setTransporteRepository($sm->get('Agencia\Repository\TransporteRepository')) 
                    ->setCemiterioService($sm->get('Admin\Service\Cemiterio'))
                    ->setMedicoService($sm->get('Agencia\Service\Medico'))
                    ->setMedicoRepository($sm->get('Agencia\Repository\MedicoRepository'))
                    ->setFalecidoRepository($sm->get('Agencia\Repository\FalecidoRepository'))
                    ->setLocalFalecimentoRepository($sm->get('Agencia\Repository\LocalFalecimentoRepository'))
                    ->setCartorioRepository($sm->get('Agencia\Repository\CartorioRepository'))  
                    ->setDadosObitoRepository($sm->get('Agencia\Repository\DadosObitoRepository'))    
                    ->setContratacaoRepository($sm->get('Agencia\Repository\ContratacaoRepository')) 
                    ->setSepultamentoService($sm->get('Agencia\Service\Sepultamento'))
                    ->setVelorioService($sm->get('Agencia\Service\Velorio'))    
                    ->setCremacaoService($sm->get('Agencia\Service\Cremacao'))      
                    ;

                return $service;
            },
            'Agencia\Service\LocalFalecimento' => function ($sm) {
                $service = new \Agencia\Service\LocalFalecimento($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\LocalFalecimento(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());

                return $service;
            },
            'Agencia\Service\Falecido' => function ($sm) {
                $service = new \Agencia\Service\Falecido($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\Falecido(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat())
                    ->setFilhoFalecidoService($sm->get('Agencia\Service\FilhoFalecido'))
                    ->setCertidaoNascimentoService($sm->get('Agencia\Service\CertidaoNascimento'))
                    ->setCasamentoService($sm->get('Agencia\Service\Casamento'))
                    ->setNascimentoObitoService($sm->get('Agencia\Service\NascimentoObito'))
                    ->setLocalizacaoService($sm->get('Agencia\Service\Localizacao'));

                return $service;
            },
            'Agencia\Service\LocalFaleciemnto' => function ($sm) {
                $service = new \Agencia\Service\LocalFaleciemnto($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\LocalFaleciemnto(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());

                return $service;
            }, 
            'Agencia\Service\Cartorio' => function ($sm) {
                $service = new \Agencia\Service\Cartorio($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\Cartorio(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());

                return $service;
            },        
            'Agencia\Service\Transporte' => function ($sm) {
                $service = new \Agencia\Service\Transporte($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\Transporte(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());

                return $service;
            }, 
            'Admin\Service\Cemiterio' => function ($sm) {
                $service = new \Admin\Service\Cemiterio($sm->get('Doctrine\ORM\EntityManager'), new \Admin\Entity\Cemiterio(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());

                return $service;
            }, 
            'Agencia\Service\Medico' => function ($sm) {
                $service = new \Agencia\Service\Medico($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\Medico(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());

                return $service;
            },
            'Agencia\Service\Declarante' => function ($sm) {
                $service = new \Agencia\Service\Declarante($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\Declarante(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat())
                    ->setLocalizacaoService($sm->get('Agencia\Service\Localizacao'))
                    ->setDadosEmpresaService($sm->get('Agencia\Service\DadosEmpresa'))
                    //->setContratacaoRepository($sm->get('Agencia\Repository\ContratacaoRepository')) 
                        
                        ;

                return $service;
            },
            'Agencia\Service\ProdutoServico' => function ($sm) {
                $service = new \Agencia\Service\ProdutoServico($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\VendaProdutoServico(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat())
                    ->setProdutoVendidoService($sm->get('Agencia\Service\ProdutoVendido'))
                    ->setServicoVendidoService($sm->get('Agencia\Service\ServicoVendido'));
                return $service;
            },
            'Agencia\Service\ProdutoVendido' => function ($sm) {
                $service = new \Agencia\Service\ProdutoVendido($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\ProdutoVendido(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());
                return $service;
            },
            'Agencia\Service\ServicoVendido' => function ($sm) {
                $service = new \Agencia\Service\ServicoVendido($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\ServicoVendido(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());
                return $service;
            },
            'Agencia\Service\DadosObitoVelorio' => function ($sm) {
                $service = new \Agencia\Service\DadosObitoVelorio($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\DadosObitoVelorio(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat())
                    ->setLocalFalecimentoService($sm->get('Agencia\Service\LocalFalecimento'))
                    ->setCemiterioService($sm->get('Admin\Service\Cemiterio'))
                    ->setMedicoService($sm->get('Agencia\Service\Medico'))
                    ->setMedicoRepository($sm->get('Agencia\Repository\MedicoRepository'))
                    ->setLocalFalecimentoRepository($sm->get('Agencia\Repository\LocalFalecimentoRepository'))
                    ->setDadosObitoVelorioRepository($sm->get('Agencia\Repository\DadosObitoVelorioRepository'))    
                    ->setContratanteRepository($sm->get('Agencia\Repository\ContratanteRepository'))    
                    ->setContratacaoRepository($sm->get('Agencia\Repository\ContratacaoRepository')) 
                    ->setSepultamentoService($sm->get('Agencia\Service\Sepultamento'))
                    ->setVelorioService($sm->get('Agencia\Service\Velorio'))    
                    ->setCremacaoService($sm->get('Agencia\Service\Cremacao'))      
                    ;

                return $service;
            },
            'Agencia\Service\DadosAmputacao' => function ($sm) {
                $service = new \Agencia\Service\DadosAmputacao($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\DadosAmputacao(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat())
                    ->setMedicoRepository($sm->get('Agencia\Repository\MedicoRepository')) 
                    ->setDadosAmputacaoRepository($sm->get('Agencia\Repository\DadosAmputacaoRepository')) 
                    ->setPacienteRepository($sm->get('Agencia\Repository\PacienteRepository'))     
                    ->setCremacaoService($sm->get('Agencia\Service\Cremacao'))
                    ->setSepultamentoService($sm->get('Agencia\Service\Sepultamento'))
                    ->setCemiterioRepository($sm->get('Admin\Repository\CemiterioRepository'))     
                       
                     ;

                return $service;
            },
            'Agencia\Service\NotaContratacao' => function ($sm) {
                $service = new \Agencia\Service\Contratante($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\NotaContratacao(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat())
                    ->setLocalizacaoService($sm->get('Agencia\Service\Localizacao'))
                    ->setDadosEmpresaService($sm->get('Agencia\Service\DadosEmpresa'));

                return $service;
            },
            'Agencia\Service\Paciente' => function ($sm) {
                $service = new \Agencia\Service\Paciente($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\Paciente(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat())
                    
                    ;

                return $service;
            },
            'Agencia\Service\TransladoCorpo' => function ($sm) {
                $service = new \Agencia\Service\TransladoCorpo($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\TransladoCorpo(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat())
                    ->setCemiterioService($sm->get('Admin\Service\Cemiterio'))
                    ->setContratanteRepository($sm->get('Agencia\Repository\ContratanteRepository'))    
       ;

                return $service;
            },
            'Agencia\Service\Sepultamento' => function ($sm) {
                $service = new \Agencia\Service\Sepultamento($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\Sepultamento(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat())
                   ;
                return $service;
            },
            'Agencia\Service\Velorio' => function ($sm) {
                $service = new \Agencia\Service\Velorio($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\Velorio(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat())
                   ;
                return $service;
            },

            'Agencia\Service\Cremacao' => function ($sm) {
                $service = new \Agencia\Service\Cremacao($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\Cremacao(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat())
                   ;

                return $service;
            },        

            'Agencia\Service\FilhoFalecido' => function ($sm) {
                $service = new \Agencia\Service\FilhoFalecido($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\FilhoFalecido(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());
                return $service;
            },
            'Agencia\Service\CertidaoNascimento' => function ($sm) {
                $service = new \Agencia\Service\CertidaoNascimento($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\CertidaoNascimento(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());
                return $service;
            },
            'Agencia\Service\Casamento' => function ($sm) {
                $service = new \Agencia\Service\Casamento($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\Casamento(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());
                return $service;
            },
            'Agencia\Service\NascimentoObito' => function ($sm) {
                $service = new \Agencia\Service\NascimentoObito($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\NascimentoObito(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());
                return $service;
            },
            'Agencia\Service\Emissao' => function ($sm) {
                $service = new \Agencia\Service\Emissao($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\Emissao(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat())
                    ->setLocalizacaoService($sm->get('Agencia\Service\Localizacao'))
                    ->setDadosEmpresaService($sm->get('Agencia\Service\DadosEmpresa'));

                return $service;
            },
            'Agencia\Service\Pagamento' => function ($sm) {
                $service = new \Agencia\Service\Pagamento($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\Pagamento(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat())
                    ->setBandeiraRepository($sm->get('Agencia\Repository\BandeiraRepository'))    
                    ->setPagamentoRepository($sm->get('Agencia\Repository\PagamentoRepository')) 
                    ->setContratacaoService($sm->get('Agencia\Service\Contratacao'))        
                  ;

                return $service;
            },
            'Agencia\Service\Bandeira' => function ($sm) {
                $service = new \Agencia\Service\Bandeira($sm->get('Doctrine\ORM\EntityManager'), new \Agencia\Entity\Bandeira(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat())
              
                  ;

                return $service;
            },          


        );
    }

    /**
     * Retorna os serviços do PHPOffice
     * @return array
     */
    public function getOfficeServices()
    {
        return array(
            'Agencia\WordService\Notas' => function($sm) {
                $config = $sm->get('BaseConfig');
                return new \Agencia\WordService\Notas(new \PhpOffice\PhpWord\PhpWord(), $config['tmp'], $config['uploads'], $config['templates']);
            },
        );
    }

    public function getMailConfig()
    {
        return array();
    }

    public function getAuthServices()
    {
        return array();
    }

    public function getBaseServices()
    {
        return array();
    }

}
