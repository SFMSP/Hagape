<?php
namespace Agencia\Form;
use Zend\Form\Form;

/**
 * Classe que abstrai o filtro na listagem de pedidos
 *
 * @autor Evandro Melos
 */
class AgenciaFiltro extends Form
{
    /**
     * Monta o formulário de filtro de Agencia
     * @param array
     */
    public function __construct(array $agencia) {
        //Seta o nome do formulário
        parent::__construct('contratacao');

        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'stdform stdform2'));

        //Adiciona o select de situacao
        $this->add(array(
            'name' => 'agencia',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'agencia'
            ),
            'options' => array(
                'label' => 'Agência: ',
                'value_options' => $agencia,
                'disable_inarray_validator' => true
            ),
        ));
    }
}