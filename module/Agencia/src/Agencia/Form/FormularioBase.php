<?php


namespace Agencia\Form;

use Zend\Form\Form;
use Agencia\InputFilter\Contratante as FormularioBaseFilter;

//Formulario base para as abas

class FormularioBase extends Form
{
    var $arSimNao = [
        0 => 'Não',
        1 => 'Sim'
    ];

    public function retornaCamposAbaContratante($estados)
    {
        return [
            $this->getCampoNomeCompleto(),
            $this->getCampoGrauParentesco(),
            $this->getCampoTelefoneResidencial(),
            $this->getCampoCpf(),
            $this->getCampoRG(),
            $this->getCampoCel(),
            $this->getCampoEmail(),
            $this->getCampoProfissao(),
            $this->getCampoNomeMae(),
            $this->getCampoEndereco(),
            $this->getCampoNumero(),
            $this->getCampoComplemento(),
            $this->getCampoCEP(),
            $this->getCampoBairro(),
            $this->getCampoCidade(),
            $this->getCampoEstado($estados),
            $this->setCampoCheckbox('empresaAcompanha', 'Empresa Acompanha ?'),
            $this->getCampoEmpresa(),
            $this->getCampoEnderecoEmpresa(),
            $this->getCampoRepresentante(),
            $this->getCampoTelefoneEmpresa(),
            $this->getCampoCNPJ()
        ];
    }

    public function retornaCamposAbaDadosObito($estado, $transporte, $carro)

    {


        return [
            $this->setCampoText('localFalecimento', 'Local Falecimento:'),
            $this->setCampoText('cartorio', 'Cartório:'),
            $this->setCampoData('dataFalecimento', 'Data de Falecimento:'),
            $this->setCampoText('causaMortis', 'Causa Mortis:'),
            $this->setCampoSelect('destinoFinal', 'Destino Final:', array('', 'Sepultamento', 'Cremação')),
            $this->setCampoText('proaim', 'PROAIM:'),
            $this->setCampoText('hrFalecimento', 'Hr. de Falecimento:'),
            $this->setCampoText('proaim', 'PROAIM:'),
            $this->setCampoText('enderecoFalecimento', 'Endereço:'),
            $this->setCampoSelect('seraVelado', 'Será Velado?', array('', 'Sim', 'Não')),
            $this->setCampoSelect('estado', 'Estado:', $estado),
            $this->setCampoText('localVelorio', 'Local Velorio:'),
            $this->setCampoData('dtSaida', 'Data de Saída:'),
            $this->setCampoText('hrSaida', 'Hr. de Saída:'),
            $this->setCampoText('enderecoVelorio', 'Endereço:'),
            $this->setCampoText('medico1', 'Médico 1:'),
            $this->setCampoText('medico2', 'Médico 2:'),
            $this->setCampoText('crm1', 'CRM 1:'),
            $this->setCampoText('crm2', 'CRM 2:'),
            $this->setCampoText('crematorio', 'Crematório:'),
            $this->setCampoText('endereco', 'Endereço:'),
            $this->setCampoData('dtCremacao', 'Data de Cremação:'),
            $this->setCampoText('hrCremacao', 'Hr. de Cremação:'),
            $this->setCampoText('cemiterio', 'Cemitério:'),
            $this->setCampoText('endereco', 'Endereço:'),
            $this->setCampoSelect('tipoTransporte', 'Tipo de Transporte:', $transporte),
            $this->setCampoData('dtSepultamento', 'Data de Sepultamento:'),
            $this->setCampoText('hrSepultamento', 'Hr. de Sepultamento:'),
            $this->setCampoText('enderecoRemocao', 'Endereço:'),
            $this->setCampoText('localRemocao', 'Local de Remoção:'),
            $this->setCampoText('remocao', 'Local de remoção é o mesmo de falecimento?'),
            $this->setCampoSelect('tipoCarro', 'Tipo de Carro:', $carro),

        ];
    }

    public function retornaCamposAbaDeclarante($estados)
    {
        return [
            
            $this->setCampoText('nome', 'Nome Completo:'),
            $this->setCampoText('grauParentesco', 'Grau de Parentesco:'),
            $this->setCampoText('telefoneResidencial', 'Telefone Residencial:','phone_with_ddd'),
            $this->setCampoText('cpf', 'CPF:','cpf'),
            $this->setCampoText('rg', 'RG:'),
            $this->setCampoText('telefoneCelular', 'Telefone Celular:','phone_with_ddd'),
            $this->setCampoText('telefoneResidencial', 'Telefone Residencial:'),
            $this->setCampoText('profissao', 'Profissão:'),
            $this->setCampoText('nomeMae', 'Nome da Mãe:'),
            $this->setCampoText('endereco', 'Endereço:'),
            $this->setCampoText('numero', 'Numero:'),
            $this->setCampoText('complemento', 'Complemento:'),
            $this->setCampoText('cep', 'CEP:','cep'),
            $this->setCampoText('bairro', 'Bairro:'),
            $this->setCampoSelect('estado', 'Estado:', $estados),
            $this->setCampoText('empresa', 'Empresa:'),
            $this->setCampoText('telefoneEmpresa', 'Telefone da Empresa:','phone_with_ddd'),
            $this->setCampoText('cnpj', 'CNPJ:','cnpj'),
           // $this->setCampoCheckbox('empresaAcompanha', 'Empresa Acompanha ?'),
            $this->setCampoText('placaVeiculo', 'Placa do Veiculo:'),
            $this->setCampoText('nomeMotorista', 'Nome do Motorista:'),
            $this->setCampoText('rgMotorista', 'RG do Motorista:'),
     
        ];
    }

    public function retornaCamposAbaProdutoServico(
        $arUrnas = [],
        $arRevestimento = [],
        $arEnfeite = [],
        $arVeu = [],
        $arIluminacao = [],
        $arConvenios = []
    )
    {
        return [
            $this->setCampoRadio('convenio', 'Convênio:', $this->arSimNao, ''),
            $this->setCampoSelect(
                'convenioSelect',
                ' ',
                $arConvenios
            ),
            $this->setCampoRadio(
                'remocaoCorpoLocalFalecimento',
                'Remoção do corpo no local do falecimento?',
                $this->arSimNao
            ),
            $this->setCampoText('localCorpoRemocao', 'Local de Remoção:', ''),
            $this->setCampoText('enderecoCorpoRemocao', 'Endereço:', ''),
            $this->setCampoRadio(
                'remocaoMembroLocalFalecimento',
                'Remoção do membro no local do falecimento?',
                $this->arSimNao
            ),
            $this->setCampoText('localMembroRemocao', 'Local de Remoção:', ''),
            $this->setCampoText('enderecoMembroRemocao', 'Endereço:', ''),
            //-------------------------Field Produtos-----------------------------
            $this->setCampoSelect('urna', 'Urna:', $arUrnas),
            $this->setCampoNumber('quantidadeUrna', ' ',1,10,1),
            $this->setCampoText('unidadeMedidaUrna', ' '),
            $this->setCampoText('valorConjuntoUrna', ' ','money'),
            $this->setCampoText('disponivelUrna', ' '),
            //Revestimento
            $this->setCampoSelect('revestimento', 'Revestimento:', $arRevestimento),
            $this->setCampoNumber('quantidadeRevestimento', ' ',1,10,1),
            $this->setCampoText('unidadeMedidaRevestimento', ' '),
            $this->setCampoText('valorConjuntoRevestimento', ' ','money'),
            $this->setCampoText('disponivelRevestimento', ''),
            //Enfeite
            $this->setCampoSelect('enfeite', 'Enfeite:', $arEnfeite),
            //Veu
            $this->setCampoSelect('veu', 'Véu:', $arVeu),
            $this->setCampoNumber('quantidadeVeu', ' ',1,10,1),
            $this->setCampoText('unidadeMedidaVeu', ' '),
            $this->setCampoText('valorConjuntoVeu', ' ','money'),
            $this->setCampoText('disponivelVeu', ''),
            //Iluminacao
            $this->setCampoSelect('iluminacao', 'Iluminação:', $arIluminacao),
            $this->setCampoNumber('quantidadeIluminacao', ' ',1,10,1),
            $this->setCampoText('unidadeMedidaIluminacao', ' '),
            $this->setCampoText('valorConjuntoIluminacao', ' ','money'),
            $this->setCampoText('disponivelIluminacao', ''),
            //-------------------------Field Taxa e Servicos-----------------------------
            $this->setCampoSelect('tipoSepultamento', 'Tipo de Sepultamento:', $arIluminacao),
            $this->setCampoText('quantidadeSepultamento', ' '),
            $this->setCampoText('valorConjuntoSepultamento', ' ','money'),
            //Transporte
            $this->setCampoCheckbox('transporteCarreto', 'Carro CARRETO'),
            $this->setCampoNumber('quantidadeTransporteCarreto', ' ',1,10,1),
            $this->setCampoText('valorConjuntoTransporteCarreto', ' ','money'),
            $this->setCampoCheckbox('transporteEnterro', 'Carro ENTERRO'),
            $this->setCampoNumber('quantidadeTransporteEnterro', ' ',1,10,1),
            $this->setCampoText('valorConjuntoTransporteEnterro', ' ','money'),
            $this->setCampoCheckbox('transporteRemocao', 'Carro REMOÇÃO'),
            $this->setCampoNumber('quantidadeTransporteRemocao', ' ',1,10,1),
            $this->setCampoText('valorConjuntoTransporteRemocao', ' ','money'),
            $this->setCampoCheckbox('transporteCremacao', 'Carro CREMAÇÃO'),
            $this->setCampoNumber('quantidadeTransporteCremacao', ' ',1,10,1),
            $this->setCampoText('valorConjuntoTransporteCremacao', ' ','money'),
            $this->setCampoCheckbox('transporteViagem', 'Carro VIAGEM'),
            $this->setCampoNumber('quantidadeTransporteViagem', ' ',1,10,1),
            $this->setCampoText('valorConjuntoTransporteViagem', ' ','money'),
            //Taxas
            $this->setCampoCheckbox('taxaSepultamento', 'Sepultamento'),
            $this->setCampoCheckbox('taxaEssaParamento', 'ESSA/Paramento'),
            $this->setCampoCheckbox('taxaVelorio', 'Velório'),
            $this->setCampoCheckbox('taxaCremacao', 'Cremação'),
            $this->setCampoCheckbox('taxaCondolencia', 'Mesa de Condolência'),
            $this->setCampoCheckbox('taxaViagem', 'Viagem'),
            $this->setCampoCheckbox('taxaCamaraFria', 'Câmara Fria'),
            $this->setCampoNumber('quantidadeTaxaCamaraFria', ' ',1,10,1),
            $this->setCampoText('valorConjuntoTaxaCamaraFria', ' ','money'),
            $this->setCampoCheckbox('taxaOutros', 'Outros'),
            $this->setCampoRadio('creditarDebitar', 'Outros', ['creditar' => '(+)Creditar', 'debitar' => '(-)Debitar'], ''),
            $this->setCampoText('valorConjuntoOutros', ' ','money'),
            //VALOR TOTAL
            $this->setCampoText('valorTotal', 'Valor Total:','money'),
            //Observações
            $this->setCampoTextArea('observacoes', 'Observações:')

        ];
    }

    public function retornaCamposAbaFalecido($raca = [], $estados = [])
    {
        $arEstadoCivil = [
            '' => 'Selecione',
            '1' => 'Solteiro(a)',
            '2' => 'Casado(a)'
        ];
        $arEstadoCivilPais = [
            '' => 'Selecione',
            '1' => 'Solteiro(a)',
            '2' => 'Casado(a)',
            '3' => 'Falecido(a)'
        ];
        return [
            $this->setCampoSelect('nascimentoObito', 'Nasc. e Óbito:', $this->arSimNao),
            $this->setCampoText('nome', 'Nome Completo:'),
            $this->setCampoText('naturalidade', 'Naturalidade:'),
            $this->getCampoSexo(),
            $this->getCampoProfissao(),
            $this->setCampoSelect('raca', 'Etnia/Cor:', $raca),
            $this->setCampoSelect('documento', 'Documento:', ['rg' => 'RG']),
            $this->setCampoText('numeroDocumento', 'Nº:'),
            $this->getCampoCpf(),
            $this->setCampoData('dataNascimento', 'Dt. Nascimento'),
            $this->setCampoIdade('idade', 'Idade:'),
            $this->setCampoSelect('estadoCivil', 'Estado Civil:', $arEstadoCivil),
            $this->getCampoEndereco(),
            $this->getCampoBairro(),
            $this->getCampoNumero(),
            $this->getCampoCidade(),
            $this->getCampoComplemento(),
            $this->getCampoEstado($estados),
            $this->getCampoCEP(),
            $this->setCampoSelect('marcaPasso', 'Marca Passo:', $this->arSimNao),
            $this->setCampoSelect('eleitor', 'Eleitor:', $this->arSimNao),
            $this->setCampoSelect('reservista', 'Reservista:', $this->arSimNao),
            $this->setCampoText('numeroBeneficio', 'Nº Beneficio:'),
            $this->setCampoSelect('inss', 'INSS:', $this->arSimNao),
            $this->setCampoSelect('bensInventariar', 'Bens à inventariar:', $this->arSimNao),
            $this->setCampoSelect('deixaTestamento', 'Deixa Testamento:', $this->arSimNao),
            $this->setCampoText('nomeMae', 'Nome Mãe:'),
            $this->setCampoSelect('estadoCivilMae', 'Estado Civil:', $arEstadoCivilPais),
            $this->setCampoIdade('idadeMae', 'Idade:'),
            $this->setCampoText('naturalidadeMae', 'Naturalidade:'),
            $this->setCampoText('profissaoMae', 'Profissão:'),
            $this->setCampoText('nomePai', 'Nome Pai:'),
            $this->setCampoSelect('estadoCivilPai', 'Estado Civil:', $arEstadoCivilPais),
            $this->setCampoIdade('idadePai', 'Idade:'),
            $this->setCampoText('naturalidadePai', 'Naturalidade:'),
            $this->setCampoText('profissaoPai', 'Profissão:'),
            $this->setCampoSelect('deixaFilhos', 'Deixa filhos:', $this->arSimNao),
            $this->setCampoText('cartorio', 'Cartório:'),
            $this->setCampoText('cidadeCartorio', 'Cidade:'),
            $this->setCampoSelect('estadoCartorio', 'Estado:', $estados),
            $this->setCampoText('livro', 'Livro:'),
            $this->setCampoText('folha', 'Folha:'),
            $this->setCampoText('numeroLivro', 'Numero:'),
            /*Dados para NASCIMENTO E OBITO*/
            $this->setCampoText('localNascimento', 'Local:'),
            $this->setCampoData('dataNascimentoNascObito', 'Data:'),
            $this->setCampoText('horaNascimento', 'Hora:', 'hora'),
            $this->setCampoText('avoPaterno', 'Avô Paterno:'),
            $this->setCampoText('avoMaterno', 'Avô Materno:'),
            $this->setCampoText('avoMaterna', 'Avó Materna:'),
            $this->setCampoText('avoPaterna', 'Avó Paterna:'),
            $this->setCampoText('nomeCompletoTestemunha1', 'Nome Completo:'),
            $this->setCampoSelect('estadoCivilTestemunha1', 'Estado Civil:', $arEstadoCivil),
            $this->setCampoText('nacionalidadeTestemunha1', 'Nacionalidade:'),
            $this->setCampoText('profissaoTestemunha1', 'Profissão:'),
            $this->setCampoText('enderecoTestemunha1', 'Endereço:'),
            $this->setCampoText('bairroTestemunha1', 'Bairro:'),
            $this->setCampoText('nomeCompletoTestemunha2', 'Nome Completo:'),
            $this->setCampoSelect('estadoCivilTestemunha2', 'Estado Civil:', $arEstadoCivil),
            $this->setCampoText('nacionalidadeTestemunha2', 'Nacionalidade:'),
            $this->setCampoText('profissaoTestemunha2', 'Profissão:'),
            $this->setCampoText('enderecoTestemunha2', 'Endereço:'),
            $this->setCampoText('bairroTestemunha2', 'Bairro:'),
            $this->setCampoText('gestacao', 'Gestação(semanas):'),
            $this->setCampoText('gravidez', 'Gravidez:'),
        ];
    }

    public function retornaFormDadosPessoais()
    {
        return [
            $this->getCampoNomeCompleto(),
            $this->getCampoGrauParentesco(),
            $this->getCampoTelefoneResidencial(),
            $this->getCampoCpf(),
            $this->getCampoRG(),
            $this->getCampoCel(),
            $this->getCampoEmail(),
            $this->getCampoProfissao(),
            $this->getCampoNomeMae(),
        ];
    }

    public function retornaFormLocalizacao($estados)
    {
        return [
            $this->getCampoEndereco(),
            $this->getCampoNumero(),
            $this->getCampoComplemento(),
            $this->getCampoCEP(),
            $this->getCampoBairro(),
            $this->getCampoCidade(),
            $this->getCampoEstado($estados)
        ];

    }

    public function retornaFormEmpresa()
    {
        return [
            $this->setCampoCheckbox('empresaAcompanha', 'Empresa Acompanha ?'),
            $this->getCampoEmpresa(),
            $this->getCampoEnderecoEmpresa(),
            $this->getCampoRepresentante(),
            $this->getCampoTelefoneEmpresa(),
            $this->getCampoCNPJ()
        ];

    }

    public function retornaCamposAbaDadosObitoVelorio($estado)

    {


        return [
            $this->setCampoText('nome', 'Nome Completo:'),
            $this->setCampoText('localFalecimento', 'Local Falecimento:'),
            $this->setCampoData('dataFalecimento', 'Data de Falecimento:'),
            $this->setCampoText('causaMortis', 'Causa Mortis:'),
            $this->setCampoSelect('destinoFinal', 'Destino Final:', array('', 'Sepultamento', 'Cremação')),
            $this->setCampoText('hrFalecimento', 'Hr. de Falecimento:'),
            $this->setCampoText('enderecoFalecimento', 'Endereço:'),
            $this->setCampoSelect('seraVelado', 'Será Velado?', array('', 'Sim', 'Não')),
            $this->setCampoText('localVelorio', 'Local Velorio:'),
            $this->setCampoData('dtSaida', 'Data de Saída:'),
            $this->setCampoText('hrSaida', 'Hr. de Saída:'),
            $this->setCampoText('enderecoVelorio', 'Endereço:'),
            $this->setCampoText('medico1', 'Médico 1:'),
            $this->setCampoText('medico2', 'Médico 2:'),
            $this->setCampoText('crm1', 'CRM 1:'),
            $this->setCampoText('crm2', 'CRM 2:'),
            $this->setCampoText('crematorio', 'Crematório:'),
            $this->setCampoText('enderecoCrematorio', 'Endereço:'),
            $this->setCampoData('dtCremacao', 'Data de Cremação:'),
            $this->setCampoText('hrCremacao', 'Hr. de Cremação:'),
            $this->setCampoText('cemiterio', 'Cemitério:'),
            $this->setCampoText('enderecoCemiterio', 'Endereço:'),
            $this->setCampoData('dtSepultamento', 'Data de Sepultamento:'),
            $this->setCampoText('hrSepultamento', 'Hr. de Sepultamento:'),
            $this->setCampoText('tamanho', 'Tamanho:'),


        ];
    }

    public function retornaCamposAbaAmputacao($estado)

    {


        return [
            $this->setCampoText('causaAmputacao', 'Causa da Amputação:'),
            $this->setCampoText('localAmputacao', 'Local da Amputação:'),

            $this->setCampoText('medico1', 'Médico 1:'),
            $this->setCampoText('medico2', 'Médico 2:'),
            $this->setCampoText('crm1', 'CRM 1:'),
            $this->setCampoText('crm2', 'CRM 2:'),
            $this->setCampoText('crematorio', 'Crematório:'),
            $this->setCampoText('enderecoCrematorio', 'Endereço:'),
            $this->setCampoData('dtCremacao', 'Data de Cremação:'),
            $this->setCampoText('hrCremacao', 'Hr. de Cremação:'),
            $this->setCampoText('cemiterio', 'Cemitério:'),
            $this->setCampoText('enderecoCemiterio', 'Endereço:'),
            $this->setCampoData('dtSepultamento', 'Data de Sepultamento:'),
            $this->setCampoText('hrSepultamento', 'Hr. de Sepultamento:'),


        ];
    }

    public function retornaCamposAbaPaciente($membro)
    {
        return [
            $this->setCampoText('nome', 'Nome Completo:'),
            $this->setCampoSelect('membroAmputado', 'Membro Amputatado:', $membro),
            $this->setCampoSelect('pecasAnatomicas', 'Peças Anatômicas:', array('', 'Sim', 'Não')),

        ];
    }
    public function retornaCamposAbaTranslado(){
        return[

    $this->setCampoText('nomeFalecido','Nome do Falecido:'),
    $this->setCampoText('numeroProcesso','Numéro do Processo'),
    $this->setCampoText('cemiterioSaida','Cemitério de Saída'),
    $this->setCampoText('hrCremacao','Hr. Cremação:'),
    $this->setCampoData('dtSaida','Dt. Saída:'),
    $this->setCampoText('hrSepultamento','Hr. Sepultamento:'),
    $this->setCampoText('cemiterioDestino','Cemitério de Destino:'),
    $this->setCampoText('crematorioDestino','Cremátorio de Destino:'),
    $this->setCampoText('enderecoCemiterio', 'Endereço:'),            
    $this->setCampoText('enderecoCrematorio', 'Endereço:'),   
    $this->setCampoText('enderecoCemiterioDestino', 'Endereço:'),            
    $this->setCampoText('enderecoCrematorioDestino', 'Endereço:'),            
        ];
    }
    public function retornaCamposAbaPagamento($tipoPagamento,$bandeira,$emitente){
        return[

    $this->setCampoText('nome','Nome:'),
    $this->setCampoText('documento','Documento:'),
    $this->setCampoText('telefoneResidencial','Telefone:'),
    $this->setCampoText('endereco','Endereço:'),
    $this->setCampoText('pinpad','PinPad:'),
    $this->setCampoText('autorizacao','Autorização:'),
    $this->setCampoText('cvdoc', 'N° CV/DOC:'),            
    $this->setCampoText('valor', 'Valor:'),   
    $this->setCampoSelect('emitente', 'Emitente:',$emitente),  
    $this->setCampoSelect('bandeira','Bandeira:',$bandeira),
    $this->setCampoSelect('tipoPagamento', 'Tipo Pagamento:',$tipoPagamento),  
    $this->setCampoSelect('parcelamento','Parcelamento:',array('','1x','2x','3x')),
        ];
    }
    



    
    

    public function getCampoNomeCompleto()
    {
        //Adiciona o campo nome
        return
            array(
                'name' => 'nome',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'nome',
                    'class' => 'form-control'
                ),
                'options' => array(
                    'label' => 'Nome Completo:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

    public function getCampoCpf()
    {
        //Adiciona o campo cpf
        return
            array(
                'name' => 'cpf',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'cpf',
                    'class' => 'form-control cpf',
                    'maxlength' => '14'
                ),
                'options' => array(
                    'label' => 'CPF:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

    public function getCampoRG()
    {
        //Adiciona o campo rg
        return
            array(
                'name' => 'rg',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'rg',
                    'class' => 'form-control',
                    'maxlength' => '10'

                ),
                'options' => array(
                    'label' => 'RG:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

    public function getCampoProfissao()
    {
        //Adiciona o campo profissao
        return
            array(
                'name' => 'profissao',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'profissao',
                    'class' => 'form-control'
                ),
                'options' => array(
                    'label' => 'Profissão:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

    public function getCampoGrauParentesco()
    {
        //Adiciona o campo grau parentesco
        return
            array(
                'name' => 'grauParentesco',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'grauParentesco',
                    'class' => 'form-control'
                ),
                'options' => array(
                    'label' => 'Grau de Parentesco:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }


    public function getCampoTelefoneResidencial()
    {
        //Telefone Residencial
        return
            array(
                'name' => 'telefoneResidencial',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'telefoneResidencial',
                    'class' => 'form-control phone_with_ddd'
                ),
                'options' => array(
                    'label' => 'Tel. Residencial:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

    public function getCampoCel()
    {
        //Telefone Cel
        return
            array(
                'name' => 'telefoneCelular',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'telefoneCelular',
                    'class' => 'form-control phone_with_ddd'
                ),
                'options' => array(
                    'label' => 'Tel. Celular:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

    public function getCampoEmail()
    {
        //Email
        return
            array(
                'name' => 'email',
                'type' => 'Zend\Form\Element\Email',
                'attributes' => array(
                    'id' => 'email',
                    'class' => 'form-control email',
                    'Placeholder' => '@email.com.'
                ),
                'options' => array(
                    'label' => 'E-mail*: ',
                    'label_attributes' => array(
                        'class' => 'control-label'
                    )
                ),
            );
    }

    public function getCampoNomeMae()
    {
        //Nome Mãe
        return
            array(
                'name' => 'nomeMae',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'nomeMae',
                    'class' => 'form-control'
                ),
                'options' => array(
                    'label' => 'Nome da Mãe:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

    public function getCampoEndereco()
    {
        //Endereco
        return array(
            'name' => 'endereco',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'endereco',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Endereço:',
                'label_attributes' => array(
                    'class' => 'control-label',
                )
            ),
        );
    }

    public function getCampoNumero()
    {
        //Numero
        return
            array(
                'name' => 'numero',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'numero',
                    'class' => 'form-control'
                ),
                'options' => array(
                    'label' => 'Numero:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

    public function getCampoComplemento()
    {
        //Complemento
        return
            array(
                'name' => 'complemento',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'complemento',
                    'class' => 'form-control'
                ),
                'options' => array(
                    'label' => 'Complemento:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

    public function getCampoCEP()
    {
        //CEP
        return
            array(
                'name' => 'cep',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'cep',
                    'class' => 'form-control cep'
                ),
                'options' => array(
                    'label' => 'CEP:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

    public function getCampoBairro()
    {
        //Bairro
        return
            array(
                'name' => 'bairro',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'bairro',
                    'class' => 'form-control'
                ),
                'options' => array(
                    'label' => 'Bairro:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

    public function getCampoCidade()
    {
        //Estado
        return
            array(
                'name' => 'cidade',
                'type' => 'Zend\Form\Element\Select',
                'attributes' => array(
                    'id' => 'cidade',
                    'class' => 'form-control'
                ),
                'options' => array(
                    'label' => 'Cidade:',
                    'value_options' => ['' => 'Escolha uma cidade'],
                    'disable_inarray_validator' => true,
                    'label_attributes' => array(
                        'class' => 'control-label'
                    )
                ),
            );
    }

    public function getCampoEstado($estados)
    {
        //Estado
        return
            array(
                'name' => 'estado',
                'type' => 'Zend\Form\Element\Select',
                'attributes' => array(
                    'id' => 'estado',
                    'class' => 'form-control',
                    'onChange' => 'mudarEstado(jQuery(this).val())',
                ),
                'options' => array(
                    'label' => 'Estado:',
                    'value_options' => $estados,
                    'disable_inarray_validator' => true,
                    'label_attributes' => array(
                        'class' => 'control-label'
                    )
                ),
            );
    }

    public function getCampoEmpresa()
    {
        //Empresa
        return
            array(
                'name' => 'empresa',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'empresa',
                    'class' => 'form-control'
                ),
                'options' => array(
                    'label' => 'Nome Empresarial:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

    public function getCampoCNPJ()
    {
        //CNPJ
        return
            array(
                'name' => 'cnpj',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'cnpj',
                    'class' => 'form-control cnpj',
                    'maxlength' => '17'
                ),
                'options' => array(
                    'label' => 'CNPJ:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

    public function getCampoLocalFalecimento()
    {
        //Local do Falecimento
        return
            array(
                'name' => 'localFalecimento',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'localFalecimento',
                    'class' => 'form-control localFalecimento'
                ),
                'options' => array(
                    'label' => 'Local Falecimento:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );

    }

    public function getCampoCartorio()
    {
        //Local do Falecimento
        return
            array(
                'name' => 'cartorio',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'cartorio',
                    'class' => 'form-control cartorio'
                ),
                'options' => array(
                    'label' => 'Cartório:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );

    }

    public function getCampoDataFalecimento()
    {
        //Local do Falecimento
        return
            array(
                'name' => 'dataFalecimento',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'dataFalecimento',
                    'class' => 'form-control datepicker',
                    'value' => date('d/m/Y')
                ),
                'options' => array(
                    'label' => 'Data de Falecimento:',
                    'label_attributes' => array(
                        'class' => 'control-label ',
                    )
                ),
            );

    }

    public function getCampoHrFalecimento()
    {
        //Local do Falecimento
        return
            array(
                'name' => 'hrFalecimento',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'hrFalecimento',
                    'class' => 'form-control hrFalecimento'
                ),
                'options' => array(
                    'label' => 'Hr. de Falecimento:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );

    }

    public function getCampoCausaMortis()
    {
        //Local do Falecimento
        return
            array(
                'name' => 'causaMortis',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'causaMortis',
                    'class' => 'form-control causaMortis'
                ),
                'options' => array(
                    'label' => 'Causa da Mortis:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );

    }

    public function getCampoDestinoFinal()
    {
        //Local do Falecimento

        return
            array(
                'name' => 'destinoFinal',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'destinoFinal',
                    'class' => 'form-control destinoFinal'
                ),
                'options' => array(
                    'label' => 'Destino Final:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );

    }

    public function getCampoProaim()
    {
        //Local do Falecimento

        return
            array(
                'name' => 'proaim',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'proaim',
                    'class' => 'form-control proaim'
                ),
                'options' => array(
                    'label' => 'PROAIM:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );

    }

    public function getCampoMedico1()
    {
        //Local do Falecimento

        return
            array(
                'name' => 'medico1',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'medico1',
                    'class' => 'form-control medico1'
                ),
                'options' => array(
                    'label' => 'Médico 1:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );

    }

    public function getCampoMedico2()
    {
        //Local do Falecimento

        return
            array(
                'name' => 'medico2',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'medico2',
                    'class' => 'form-control medico2'
                ),
                'options' => array(
                    'label' => 'Médico 2:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );

    }

    public function getCampoCrm1()
    {
        //Local do Falecimento

        return
            array(
                'name' => 'crm1',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'crm1',
                    'class' => 'form-control crm1'
                ),
                'options' => array(
                    'label' => 'CRM 1:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );

    }

    public function getCampoCrm2()
    {
        //Local do Falecimento

        return
            array(
                'name' => 'crm2',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'crm2',
                    'class' => 'form-control crm2'
                ),
                'options' => array(
                    'label' => 'CRM 2:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );

    }

    public function getCampoCemiterio($cemiterio)
    {

        //Cemiterio
        return
            array(
                'name' => 'cemiterio',
                'type' => 'Zend\Form\Element\Select',
                'attributes' => array(
                    'id' => 'cemiterio',
                    'class' => 'form-control'
                ),
                'options' => array(
                    'label' => 'Cemitério:',
                    'value_options' => $cemiterio,
                    'disable_inarray_validator' => true,
                    'label_attributes' => array(
                        'class' => 'control-label'
                    )
                ),
            );


    }

    public function getCampoDataSepultamento()
    {
        return
            array(
                'name' => 'dtSepultamento',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'dtSepultamento',
                    'class' => 'form-control datepicker',
                    'value' => date('d/m/Y')
                ),
                'options' => array(
                    'label' => 'Data de Sepultamento:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

    public function getCampoHrSepultamento()
    {
        return
            array(
                'name' => 'hrSepultamento',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'hrSepultamento',
                    'class' => 'form-control hrSepultamento'
                ),
                'options' => array(
                    'label' => 'Hr. de Sepultamento:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

    public function getCampoCrematorio($crematorio)
    {
        //Crematorios
        return
            array(
                'name' => 'crematorio',
                'type' => 'Zend\Form\Element\Select',
                'attributes' => array(
                    'id' => 'crematorio',
                    'class' => 'form-control'
                ),
                'options' => array(
                    'label' => 'Crematório:',
                    'value_options' => $crematorio,
                    'disable_inarray_validator' => true,
                    'label_attributes' => array(
                        'class' => 'control-label'
                    )
                ),
            );


    }

    public function getCampoDataCremacao()
    {
        return
            array(
                'name' => 'dtCremacao',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'hrSepultamento',
                    'class' => 'form-control datepicker',
                    'value' => date('d/m/Y')
                ),
                'options' => array(
                    'label' => 'Data de Cremação:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

    public function getCampoHrCremacao()
    {
        return
            array(
                'name' => 'hrCremacao',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'hrCremacao',
                    'class' => 'form-control hrCremacao'
                ),
                'options' => array(
                    'label' => 'Hr. de Cremação:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

    public function getCampoVelado()
    {
        return
            array(
                'name' => 'seraVelado',
                'type' => 'Zend\Form\Element\Select',
                'attributes' => array(
                    'id' => 'seraVelado',
                    'class' => 'form-control'
                ),
                'options' => array(
                    'label' => 'Será Velado:',
                    'value_options' => array('', 'Sim', 'Não'),
                    'disable_inarray_validator' => true
                ),
            );
    }

    public function getCampoTipoTransporte()
    {
        return
            array(
                'name' => 'tipoTransporte',
                'type' => 'Zend\Form\Element\Select',
                'attributes' => array(
                    'id' => 'tipoTransporte',
                    'class' => 'form-control'
                ),
                'options' => array(
                    'label' => 'Tipo de Transporte:',
                    'value_options' => array('Aéreo', 'Terreste'),
                    'disable_inarray_validator' => true
                ),
            );
    }

    public function getCampoLocalVelorio()
    {
        return
            array(
                'name' => 'localVelorio',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'localVelorio',
                    'class' => 'form-control localVelorio'
                ),
                'options' => array(
                    'label' => 'Local Velório:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

    public function getCampoDataSaida()
    {
        return
            array(
                'name' => 'dtSaida',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'dtSaida',
                    'class' => 'form-control datepicker',
                    'value' => date('d/m/Y')
                ),
                'options' => array(
                    'label' => 'Data de Saída:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

    public function getCampoHrSaida()
    {
        return
            array(
                'name' => 'hrSaida',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'hrSaida',
                    'class' => 'form-control hrSaida'
                ),
                'options' => array(
                    'label' => 'Hr. de Saída:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

    public function getCampoObservacao()
    {
        return
            array(
                'name' => 'observacao',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'observacao',
                    'class' => 'form-control observacao'
                ),
                'options' => array(
                    'label' => 'Observação:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

    public function getEnderecoFalecimento()
    {
        return
            array(
                'name' => 'enderecoFalecimento',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'enderecoFalecimento',
                    'class' => 'form-control enderecoFalecimento'
                ),
                'options' => array(
                    'label' => 'Endereço:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

    public function getCampoEnderecoVelorio()
    {
        return
            array(
                'name' => 'enderecoVelorio',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'enderecoVelorio',
                    'class' => 'form-control enderecoVelorio'
                ),
                'options' => array(
                    'label' => 'Endereço:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

    public function getCampoRemocao()
    {
        return
            array(
                'name' => 'remocao',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'remocao',
                    'class' => 'form-control remocao'
                ),
                'options' => array(
                    'label' => 'Local de remoção é o mesmo de falecimento?',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

    public function setCampoText($name, $label, $class = '')
    {
        return
            array(
                'name' => $name,
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => $name,
                    'class' => "form-control " . $class . " "
                ),
                'options' => array(
                    'label' => $label,
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

    public function setCampoSelect($name, $label, $select)
    {
        return
            array(
                'name' => $name,
                'type' => 'Zend\Form\Element\Select',
                'attributes' => array(
                    'id' => $name,
                    'class' => 'form-control'
                ),
                'options' => array(
                    'label' => $label,
                    'value_options' => $select,
                    'disable_inarray_validator' => true
                ),
            );
    }

    public function setCampoData($name, $label)
    {
        return
            array(
                'name' => $name,
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => $name,
                    'class' => 'form-control datepicker',
                    'value' => date('d/m/Y')
                ),
                'options' => array(
                    'label' => $label,
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

    public function setCampoTextArea($name, $label)
    {
        return
            array(
                'name' => $name,
                'type' => 'Zend\Form\Element\Textarea',
                'attributes' => array(
                    'id' => $name,
                    'class' => 'form-control'
                ),
                'options' => array(
                    'label' => $label,
                    'label_attributes' => array(
                        'class' => 'control-label'
                    )
                ),
            );
    }

    public function setCampoCheckbox($name, $label)
    {
        return array(
            'name' => $name,
            'type' => 'Zend\Form\Element\Checkbox',
            'attributes' => array(
                'id' => $name,
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => $label,
                'use_hidden_element' => true,
                'checked_value' => 'S',
                'unchecked_value' => 'N',
                'label_attributes' => array(
                    'class' => 'control-label'
                )
            ),
        );
    }

    public function getCampoEnderecoEmpresa()
    {
        //Endereco
        return array(
            'name' => 'enderecoEmpresa',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'enderecoEmpresa',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Endereço:',
                'label_attributes' => array(
                    'class' => 'control-label',
                )
            ),
        );

    }

    public function getCampoRepresentante()
    {
        //Endereco
        return array(
            'name' => 'representante',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'representante',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Representante:',
                'label_attributes' => array(
                    'class' => 'control-label',
                )
            ),
        );
    }

    public function getCampoTelefoneEmpresa()
    {
        //Telefone Empresa
        return
            array(
                'name' => 'telefoneEmpresa',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'telefoneEmpresa',
                    'class' => 'form-control phone_with_ddd'
                ),
                'options' => array(
                    'label' => 'Telefone:',
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );

    }

    public function getCampoSexo()
    {
        return
            array(
                'name' => 'sexo',
                'type' => 'Zend\Form\Element\Select',
                'attributes' => array(
                    'id' => 'sexo',
                    'class' => 'form-control'
                ),
                'options' => array(
                    'label' => 'Sexo:',
                    'value_options' => [
                        '' => 'Selecione o Sexo',
                        'M' => 'Masculino',
                        'F' => 'Feminino'
                    ],
                    'disable_inarray_validator' => true
                ),
            );

    }

    public function setCampoIdade($name = '', $label = '', $class = '')
    {
        //Idade
        return
            array(
                'name' => $name,
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'id' => 'telefoneEmpresa',
                    'class' => "form-control " . $class . " ",
                    'maxlength' => '2'
                ),
                'options' => array(
                    'label' => $label,
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

    public function setCampoRadio(
        $name = '',
        $label = '',
        $value = [],
        $class = ''
    )
    {
        //Adiciona campo que recebe categorias
        return
            array(
                'type' => 'Zend\Form\Element\Radio',
                'name' => $name,
                'attributes' => array(
                    'id' => $name,
                ),
                'options' => array(
                    'label' => $label,
                    'label_attributes' => array(
                        'class' => 'control-label ' . $class . ''
                    ),
                    'disable_inarray_validator' => true,
                    'value_options' => $value
                )
            );
    }

    public function setCampoNumber(
        $name = ''
        , $label = ''
        , $min = 1
        , $max = 100
        , $step = 1
    )
    {
        //Number
        return
            array(
                'name' => $name,
                'type' => 'Zend\Form\Element\Number',
                'attributes' => array(
                    'id' => $name,
                    'class' => 'form-control',
                    'min' => $min,
                    'max' => $max,
                    'step' => $step

                ),
                'options' => array(
                    'label' => $label,
                    'label_attributes' => array(
                        'class' => 'control-label',
                    )
                ),
            );
    }

}//END OF CLASS