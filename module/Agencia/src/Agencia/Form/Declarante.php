<?php

namespace Agencia\Form;

use Zend\Form\Form;
use Agencia\InputFilter\Declarante as DeclaranteFilter;
use Zend\Session\Container as SessionConteiner;
use Agencia\Form\FormularioBase;

/**
 * Class Declarante
 * @package Agencia\Form
 */
class Declarante extends Form
{
    /**
     * @var
     */
    private $processoRepository;

    /**
     * @var
     */
    private $estados;
    /**
     * @var
     */
    private $cidades;

    /**
     * @return mixed
     */
    public function getEstados()
    {
        return $this->estados;
    }

    /**
     * @param mixed $estados
     */
    public function setEstados($estados)
    {
        $this->estados = $estados;
    }
     /**
     * @return mixed
     */
    public function getCidades()
    {
        return $this->cidades;
    }

    /**
     * @param mixed $cidades
     */
    public function setCidades($cidades)
    {
        $this->cidades = $cidades;
    }

    /**
     * @return mixed
     */
    public function getProcessoRepository()
    {
        return $this->processoRepository;
    }

    /**
     * @param mixed $processoRepository
     */
    public function setProcessoRepository($processoRepository)
    {
        $this->processoRepository = $processoRepository;
        return $this;
    }

    /**
     * Declarante constructor.
     * @param DeclaranteFilter $inputFilter
     * @param SessionConteiner $contratacao
     * @param array $estados
     */
    public function __construct(DeclaranteFilter $inputFilter, SessionConteiner $contratacao, $estados = [], $processoRepository = '', $arCamposProcesso = [],$cidades=[])
    {
        //Seta o nome do formulário
        parent::__construct('declarante');

        $this->setLabel('Declarante');
        $this->setAttribute('icon', 'icon-user');
        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'horizontal-form'));
        $this->setInputFilter($inputFilter);
        $this->setEstados($estados);
        $this->setCidades($cidades);

        if ($contratacao->id) {
            $this->retornaCamposAbaDeclarante($arCamposProcesso, $estados);
            //Submit
            
          $this->add(array(
            'name' => 'cidade',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'cidade',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Cidade*:',
                'value_options' =>$cidades,
                'disable_inarray_validator' => true,
                'label_attributes' => array(
                    'class' => 'control-label'
                )
            ),
        ));
             $this->add(array(
            'name' => 'email',
            'type' =>'Zend\Form\Element\Email',
            'attributes' => array(
                'id' => 'email',
                'class' => 'form-control email',
                'Placeholder'=>'@email.com.'
            ),
            'options' => array(
                'label' => 'E-mail*: ',
                'label_attributes' => array(
                    'class' => 'control-label'
                )
            ),
        ));
            $this->add(array(
                'name' => 'enviar',
                'type' => 'Zend\Form\Element\Submit',
                'attributes' => array(
                    'value' => 'Avançar',
                    'class' => 'btn btn-primary submit-input-loading',
                    'id' => 'enviar'
                )
            ));

            //Adiciona o campo hidden que leva o id
            $this->add(array(
                'name' => 'id',
                'type' => 'Zend\Form\Element\Hidden'
            ));


        }

    }//End __construct

    public function retornaCamposAbaDeclarante($arCamposProcesso = [], $estados = ['' => 'Escolha um Estado'])
    {
        $formularioBase = new FormularioBase();
        foreach ($formularioBase->retornaCamposAbaDeclarante($estados) as $camposAba) {
            foreach ($arCamposProcesso as $value) {
                if ($camposAba['name'] == $value['campoVariavel']) {
                    $this->add($camposAba);
                }
            }
        }
    }
public function isValid() {
    
         if($this->get('estado')){
            
             $this->getInputFilter()->remove('estado');
        }
        if($this->get('cidade')){
            
            $this->getInputFilter()->remove('cidade');
        }
       if($this->get('email')){
            
            $this->getInputFilter()->remove('email');
        }
        if($this->get('profissao')){
            
            $this->getInputFilter()->remove('profissao');
        }
        if($this->get('nomeMae')){
            
            $this->getInputFilter()->remove('nomeMae');
        }
        
         if($this->get('endereco')){
            
            $this->getInputFilter()->remove('endereco');
        }
         if($this->get('bairro')){
            
            $this->getInputFilter()->remove('bairro');
        }
         if($this->get('numero')){
            
            $this->getInputFilter()->remove('numero');
        }
         if($this->get('complemento')){
            
            $this->getInputFilter()->remove('complemento');
        }
         if($this->get('cep')){
            
            $this->getInputFilter()->remove('cep');
        }
         if($this->get('empresa')){
            
            $this->getInputFilter()->remove('empresa');
        }
        
         if($this->get('telefoneCelular')){
            
            $this->getInputFilter()->remove('telefoneCelular');
        }
        if($this->get('rg')){
            
            $this->getInputFilter()->remove('rg');
        }
        if($this->get('telefoneResidencial')){
            
            $this->getInputFilter()->remove('telefoneResidencial');
        }
        if($this->get('cpf')){
            
            $this->getInputFilter()->remove('cpf');
        }
        if($this->get('grauParentesco')){
            
            $this->getInputFilter()->remove('grauParentesco');
        }
        if($this->get('nome')){
            
            $this->getInputFilter()->remove('nome');
        }
        if($this->get('placaVeiculo')){
            
            $this->getInputFilter()->remove('placaVeiculo');
        }
        if($this->get('rgMotorista')){
            
            $this->getInputFilter()->remove('rgMotorista');
        }
        if($this->get('nomeMotorista')){
            
            $this->getInputFilter()->remove('nomeMotorista');
        }
        if($this->get('telefoneEmpresa')){
            
            $this->getInputFilter()->remove('telefoneEmpresa');
        }
        if($this->get('cnpj')){
            
            $this->getInputFilter()->remove('cnpj');
        }
       
        
        
        return parent::isValid();
    }
}//END OF CLASS