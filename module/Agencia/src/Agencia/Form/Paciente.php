<?php

namespace Agencia\Form;

use Zend\Form\Form;
use Agencia\InputFilter\Paciente as PacienteFilter;
use Zend\Session\Container as SessionConteiner;
use Agencia\Form\FormularioBase;

/**
 * Class Paciente
 * @package Agencia\Form
 */
class Paciente extends Form
{
   
    /**
     * Paciente constructor.
     * @param PacienteFilter $inputFilter
     * @param SessionConteiner $contratacao
     * @param array $estados
     */
    public function __construct(PacienteFilter $inputFilter, SessionConteiner $contratacao,$arCamposProcesso = [],$membro)
    {
        //Seta o nome do formulário
        parent::__construct('Paciente');

        $this->setLabel('Contratacao');
        $this->setAttribute('icon', 'icon-user');
        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'horizontal-form'));
        $this->setInputFilter($inputFilter);


        if ($contratacao->id) {
            $this->retornaCamposAba($arCamposProcesso,$membro);
            //Submit
            $this->add(array(
                'name' => 'enviar',
                'type' => 'Zend\Form\Element\Submit',
                'attributes' => array(
                    'value' => 'Avançar',
                    'class' => 'btn btn-primary submit-input-loading',
                    'id' => 'enviar'
                )
            ));

            //Adiciona o campo hidden que leva o id
            $this->add(array(
                'name' => 'id',
                'type' => 'Zend\Form\Element\Hidden'
            ));


        }

    }//End __construct

    public function retornaCamposAba($arCamposProcesso = [],$membro)
    {
        $formularioBase = new FormularioBase();
        foreach ($formularioBase->retornaCamposAbaPaciente($membro) as $camposAba) {
            foreach ($arCamposProcesso as $value) {
                if ($camposAba['name'] == 'representante'){
                    $this->add($camposAba);
                }
                if ($camposAba['name'] == $value['campoVariavel']) {
                    $this->add($camposAba);
                }
            }
        }
    }

public function isValid() {
    
        if($this->get('membroAmputado')){
            
             $this->getInputFilter()->remove('membroAmputado');
        }
        if($this->get('pecasAnatomicas')){
            
             $this->getInputFilter()->remove('pecasAnatomicas');
        }
        
        
        return parent::isValid();
    }

}//END F CLASS


