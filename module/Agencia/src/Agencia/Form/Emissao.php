<?php

namespace Agencia\Form;

use Zend\Form\Form;
use Agencia\InputFilter\Emissao as EmissaoFilter;
use Zend\Session\Container as SessionConteiner;
use Agencia\Form\FormularioBase;

/**
 * Class Contratante
 * @package Agencia\Form
 */
class Emissao extends Form
{
    
  /**
     * Contratante constructor.
     * @param ContratanteFilter $inputFilter
     * @param SessionConteiner $contratacao
     * @param array $estados
     */
    public function __construct()
    {
        //Seta o nome do formulário
        parent::__construct('emissao');

        $this->setLabel('Emissao');
        $this->setAttribute('icon', 'icon-user');
        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'horizontal-form'));


            
            //Submit
            $this->add(array(
                'name' => 'download',
                'type' => 'Zend\Form\Element\Submit',
                'attributes' => array(
                    'value' => 'Download',
                    'class' => 'btn btn-primary submit-input-loading',
                    'url' => 'agencia/emissao/emitirnota',
                    'id' => 'download'
                )
            ));

            //Adiciona o campo hidden que leva o id
            $this->add(array(
                'name' => 'id',
                'type' => 'Zend\Form\Element\Hidden'
            ));


    }//End __construct


}//END OF CLASS