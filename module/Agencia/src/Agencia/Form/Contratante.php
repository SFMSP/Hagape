<?php

namespace Agencia\Form;

use Zend\Form\Form;
use Agencia\InputFilter\Contratante as ContratanteFilter;
use Zend\Session\Container as SessionConteiner;
use Agencia\Form\FormularioBase;

/**
 * Class Contratante
 * @package Agencia\Form
 */
class Contratante extends Form
{
    /**
     * @var
     */
    private $processoRepository;

    /**
     * @var
     */
    private $estados;

    /**
     * @return mixed
     */
    public function getEstados()
    {
        return $this->estados;
    }

    /**
     * @param mixed $estados
     */
    public function setEstados($estados)
    {
        $this->estados = $estados;
    }

    /**
     * @return mixed
     */
    public function getProcessoRepository()
    {
        return $this->processoRepository;
    }

    /**
     * @param mixed $processoRepository
     */
    public function setProcessoRepository($processoRepository)
    {
        $this->processoRepository = $processoRepository;
        return $this;
    }

    /**
     * Contratante constructor.
     * @param ContratanteFilter $inputFilter
     * @param SessionConteiner $contratacao
     * @param array $estados
     */
    public function __construct(ContratanteFilter $inputFilter, SessionConteiner $contratacao, $estados = [], $processoRepository = '', $arCamposProcesso = [])
    {
        //Seta o nome do formulário
        parent::__construct('contratante');

        $this->setLabel('Contratacao');
        $this->setAttribute('icon', 'icon-user');
        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'horizontal-form'));
        $this->setInputFilter($inputFilter);
        $this->setEstados($estados);

        if ($contratacao->id) {
            $this->retornaCamposAbaContratante($arCamposProcesso, $estados);
            //Submit
            $this->add(array(
                'name' => 'enviar',
                'type' => 'Zend\Form\Element\Submit',
                'attributes' => array(
                    'value' => 'Avançar',
                    'class' => 'btn btn-primary submit-input-loading',
                    'id' => 'enviar'
                )
            ));

            //Adiciona o campo hidden que leva o id
            $this->add(array(
                'name' => 'id',
                'type' => 'Zend\Form\Element\Hidden'
            ));


        }

    }//End __construct

    public function retornaCamposAbaContratante($arCamposProcesso = [], $estados = ['' => 'Escolha um Estado'])
    {
        $formularioBase = new FormularioBase();
        foreach ($formularioBase->retornaCamposAbaContratante($estados) as $camposAba) {
            foreach ($arCamposProcesso as $value) {
                if ($camposAba['name'] == 'representante'){
                    $this->add($camposAba);
                }
                if ($camposAba['name'] == $value['campoVariavel']) {
                    $this->add($camposAba);
                }
            }
        }
    }

}//END OF CLASS