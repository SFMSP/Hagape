<?php


namespace Agencia\Form;

use Zend\Form\Form;
use Zend\Session\Container as SessionConteiner;
use Agencia\InputFilter\DadosObito as DadosObitoFilter;
use Agencia\Form\FormularioBase;

class DadosObito extends Form
{
     /**
     * @var
     */
    private $processoRepository;

    /**
     * @var
     */
    private $estados;
    private $transporte;
    private $carro;

    /**
     * @return mixed
     */
    public function getEstados()
    {
        return $this->estados;
    }
     /**
     * @return mixed
     */
    public function getTransporte()
    {
        return $this->transporte;
    }
    public function getCarro()
    {
        return $this->carro;
    }
    /**
     * @var
     */
   
    private $medicos;
    public function getMedicos()
    {
        return $this->medicos;
    }
    /**
     * @param mixed $medicos
     */
    public function setMedicos($medicos)
    {
        $this->medicos = $medicos;
    }
    /**
     * @param mixed $estados
     */
    public function setEstados($estados)
    {
        $this->estados = $estados;
    }
     /**
     * @param mixed $transporte
     */
    public function SetTransporte($transporte)
    {
        $this->transporte = $transporte;
    }
     /**
     * @param mixed $carro
     */
    public function setCarro($carro)
    {
        $this->carro = $carro;
    }

    /**
     * @return mixed
     */
    public function getProcessoRepository()
    {
        return $this->processoRepository;
    }

    /**
     * @param mixed $processoRepository
     */
    public function setProcessoRepository($processoRepository)
    {
        $this->processoRepository = $processoRepository;
        return $this;
    }

    /**
     * Contratante constructor.
     * @param DadosObitoFilter $inputFilter
     * @param SessionConteiner $contratacao
     * @param array $estados
     * @param array $transporte
     */
    public function __construct(DadosObitoFilter $inputFilter, SessionConteiner $contratacao, $estados = [], $processoRepository = '',$medicos=[],$arCamposProcesso = [],$transporte=[],$carro=[])
    {
        //Seta o nome do formulário
        parent::__construct('dadosobito');

        $this->setLabel('Dados Obito');
        $this->setAttribute('icon', 'icon-user');
        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'horizontal-form'));
        $this->setInputFilter($inputFilter);
        $this->setEstados($estados);
        $this->setTransporte($transporte);
        $this->setCarro($carro);
       
        $this->setMedicos($medicos);
        
        if ($contratacao->id) {
            $this->retornaCamposAbaDados($arCamposProcesso, $estados,$transporte,$carro);
            //Submit
            $this->add(array(
                'name' => 'enviar',
                'type' => 'Zend\Form\Element\Submit',
                'attributes' => array(
                    'value' => 'Avançar',
                    'class' => 'btn btn-primary submit-input-loading',
                    'id' => 'enviar'
                )
            ));

            //Adiciona o campo hidden que leva o id
            $this->add(array(
                'name' => 'id',
                'type' => 'Zend\Form\Element\Hidden'
            ));
           
             $this->add(array(
            'name' => 'localFalecimentoId',
            'type' => 'Zend\Form\Element\Hidden',
            'attributes' => array(
                'id' => 'localFalecimentoId',
                'class' => 'form-control'
            ),
           
        ));


        }
           
        $this->add(array(
            'name' => 'observacao',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'id' => 'observacao',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Observação:',
                'label_attributes' => array(
                    'class' => 'control-label'
                )
            ),
        ));
        
    }//End __construct

   public function retornaCamposAbaDados($arCamposProcesso = [], $estados = ['' => 'Escolha um Estado'],$transporte = ['' => 'Escolha um Transporte'],$carro=[''=>'Escolha um Carro'])
    {
       //print_r($arCamposProcesso);die;
        $formularioBase = new FormularioBase();
        foreach ($formularioBase->retornaCamposAbaDadosObito($estados,$transporte,$carro) as $camposAba) {
          
            foreach ($arCamposProcesso as $value) {
                if ($camposAba['name'] == 'representante'){
                    $this->add($camposAba);
                }
                if ($camposAba['name'] == $value['campoVariavel']) {
                    $this->add($camposAba);
                }
            }
        }
    }
    public function isValid() {
       $valid = parent::isValid();
        
        if(($this->get('localFalecimentoId')->getValue() == null) && ($this->get('enderecoFalecimento')->getValue() == '')){
            $this->get('enderecoFalecimento')->setMessages(array('Informe o Endereço de Falecimento!'));
            $valid = false;
        }
        
        
        if($this->get('destinoFinal')){
            
            $this->getInputFilter()->remove('destinoFinal');
             $valid = true;
        }
        if($this->get('tipoTransporte')){
            
            $this->getInputFilter()->remove('tipoTransporte');
             $valid = true;
        }
        if($this->get('tipoCarro')){
            
            $this->getInputFilter()->remove('tipoCarro');
             $valid = true;
        }
        if($this->get('seraVelado')){
            
            $this->getInputFilter()->remove('seraVelado');
             $valid = true;
        }
        if($this->get('estado')){
            
             $this->getInputFilter()->remove('estado');
              $valid = true;
        }
        
        
        return $valid;
    }
}