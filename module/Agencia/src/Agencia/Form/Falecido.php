<?php

namespace Agencia\Form;

use Zend\Form\Form;
use Agencia\InputFilter\Falecido as FalecidoFilter;
use Zend\Session\Container as SessionConteiner;
use Agencia\Form\FormularioBase;

class Falecido extends Form
{
    /**
     * @var
     */
    private $processoRepository;

    /**
     * @param mixed $processoRepository
     */
    public function setProcessoRepository($processoRepository)
    {
        $this->processoRepository = $processoRepository;
        return $this;
    }

    /**
     * Contratante constructor.
     * @param FalecidoFilter $inputFilter
     * @param SessionConteiner $contratacao
     * @param array $estados
     */
    public function __construct(
        FalecidoFilter $inputFilter
        , SessionConteiner $contratacao
        , $processoRepository = ''
        , $arCamposProcesso = []
        , $raca = []
        , $estados = []
    )
    {
        //Seta o nome do formulário
        parent::__construct('falecido');

        $this->setLabel('Falecido');
        $this->setAttribute('icon', 'icon-user');
        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'horizontal-form'));
        $this->setInputFilter($inputFilter);

        if ($contratacao->id) {
            $this->retornaCamposAbaFalecido($arCamposProcesso, $raca, $estados);
            //Submit
            $this->add(array(
                'name' => 'enviar',
                'type' => 'Zend\Form\Element\Submit',
                'attributes' => array(
                    'value' => 'Avançar',
                    'class' => 'btn btn-primary submit-input-loading',
                    'id' => 'enviar'
                )
            ));

            //Adiciona o campo hidden que leva o id
            $this->add(array(
                'name' => 'id',
                'type' => 'Zend\Form\Element\Hidden'
            ));
        }

    }//End __construct

    public function retornaCamposAbaFalecido($arCamposProcesso = [], $raca = [], $estados = [])
    {
        $formularioBase = new FormularioBase();
        foreach ($formularioBase->retornaCamposAbaFalecido($raca, $estados) as $camposAba) {
            $this->add($camposAba);
//            foreach ($arCamposProcesso as $value) {
//                if ($camposAba['name'] == $value['campoVariavel']) {
//                }
//            }
        }
    }

    public function isValid()
    {
        $valid = true;
        if ($this->get('estadoCivil')->getValue() == 2) {
            $this->getInputFilter()->remove('cartorio');
            $this->getInputFilter()->remove('cidadeCartorio');
            $this->getInputFilter()->remove('estadoCartorio');
            $this->getInputFilter()->remove('livro');
            $this->getInputFilter()->remove('folha');
            $this->getInputFilter()->remove('numeroLivro');
            $this->getInputFilter()->remove('cidade');
           
            
            
            
            
        }
            $this->getInputFilter()->remove('estadoCivilTestemunha2');
            $this->getInputFilter()->remove('estadoCivilTestemunha1');
        if ($this->get('sexo')->getValue() == "F") {
            $this->getInputFilter()->remove('reservista');
        }
        if ($this->get('nascimentoObito')->getValue() == "N") {
            $this->getInputFilter()->remove('localNascimento');
            $this->getInputFilter()->remove('dataNascimentoNascObito');
            $this->getInputFilter()->remove('horaNascimento');
            $this->getInputFilter()->remove('avoPaterno');
            $this->getInputFilter()->remove('avoMaterno');
            $this->getInputFilter()->remove('avoMaterna');
            $this->getInputFilter()->remove('avoPaterna');
            $this->getInputFilter()->remove('nomeCompletoTestemunha1');
            $this->getInputFilter()->remove('estadoCivilTestemunha1');
            $this->getInputFilter()->remove('nacionalidadeTestemunha1');
            $this->getInputFilter()->remove('profissaoTestemunha1');
            $this->getInputFilter()->remove('enderecoTestemunha1');
            $this->getInputFilter()->remove('bairroTestemunha1');
            $this->getInputFilter()->remove('nomeCompletoTestemunha2');
            $this->getInputFilter()->remove('estadoCivilTestemunha2');
            $this->getInputFilter()->remove('nacionalidadeTestemunha2');
            $this->getInputFilter()->remove('profissaoTestemunha2');
            $this->getInputFilter()->remove('enderecoTestemunha2');
            $this->getInputFilter()->remove('bairroTestemunha2');
            $this->getInputFilter()->remove('gestacao');
            $this->getInputFilter()->remove('gravidez');
        }

        $valid = parent::isValid();

        return $valid;
    }

}