<?php

namespace Agencia\Form;

use Zend\Form\Form;
use Agencia\InputFilter\ProdutoServico as ProdutoServicoFilter;
use Zend\Session\Container as SessionConteiner;
use Agencia\Form\FormularioBase;

class ProdutoServico extends Form
{
    /**
     * Contratante constructor.
     * @param ProdutoServicoFilter $inputFilter
     * @param SessionConteiner $contratacao
     * @param array $estados
     */
    public function __construct(
        ProdutoServicoFilter $inputFilter,
        SessionConteiner $contratacao,
        $processoRepository = '',
        $arCamposProcesso = []
    )
    {
        //Seta o nome do formulário
        parent::__construct('produtoServico');

        $this->setLabel('Escolha de Produto');
        $this->setAttribute('icon', 'icon-user');
        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'horizontal-form'));
        $this->setInputFilter($inputFilter);

        if ($contratacao->id) {
            $this->retornaCamposAbaProdutoServico($arCamposProcesso);
            //Submit
            $this->add(array(
                'name' => 'enviar',
                'type' => 'Zend\Form\Element\Submit',
                'attributes' => array(
                    'value' => 'Avançar',
                    'class' => 'btn btn-primary submit-input-loading',
                    'id' => 'enviar'
                )
            ));

            //Adiciona o campo hidden que leva o fator Robin Hood para Produtos
            $this->add(array(
                'name' => 'hdnRobinHoodProduto',
                'type' => 'Zend\Form\Element\Hidden'
            ));

            //Adiciona o campo hidden que leva o fator Robin Hood para servicos
            $this->add(array(
                'name' => 'hdnRobinHoodServico',
                'type' => 'Zend\Form\Element\Hidden'
            ));

            //Adiciona o campo hidden que leva o id
            $this->add(array(
                'name' => 'id',
                'type' => 'Zend\Form\Element\Hidden'
            ));


        }

    }//End __construct

    public function retornaCamposAbaProdutoServico($arCamposProcesso = [])
    {
        $formularioBase = new FormularioBase();
        foreach ($formularioBase->retornaCamposAbaProdutoServico() as $camposAba) {
            $this->add($camposAba);
        }
    }

    public function isValid()
    {
        $isValid = true;

        if (!$this->get('convenio')->getValue()){
            $this->getInputFilter()->remove('convenio');
            $this->getInputFilter()->remove('convenioSelect');
        }
        if (!$this->get('remocaoCorpoLocalFalecimento')->getValue()){
            $this->getInputFilter()->remove('remocaoCorpoLocalFalecimento');
            $this->getInputFilter()->remove('localCorpoRemocao');
            $this->getInputFilter()->remove('enderecoCorpoRemocao');
        }
        if (!$this->get('remocaoMembroLocalFalecimento')->getValue()){
            $this->getInputFilter()->remove('remocaoMembroLocalFalecimento');
            $this->getInputFilter()->remove('localMembroRemocao');
            $this->getInputFilter()->remove('enderecoMembroRemocao');
        }
        if ($this->get('valorConjuntoSepultamento')->getValue()){
            $this->getInputFilter()->remove('quantidadeSepultamento');
        }
        if (!$this->get('revestimento')->getValue()){
            $this->getInputFilter()->remove('revestimento');
            $this->getInputFilter()->remove('quantidadeRevestimento');
        }
        if (!$this->get('enfeite')->getValue()){
            $this->getInputFilter()->remove('enfeite');
        }
        if (!$this->get('veu')->getValue()){
            $this->getInputFilter()->remove('veu');
            $this->getInputFilter()->remove('quantidadeVeu');
        }
        if (!$this->get('iluminacao')->getValue()){
            $this->getInputFilter()->remove('iluminacao');
            $this->getInputFilter()->remove('quantidadeIluminacao');
        }
        if($this->get('transporteCarreto')->getValue() == 'N'){
            $this->getInputFilter()->remove('quantidadeTransporteCarreto');
        }
        if($this->get('transporteEnterro')->getValue() == 'N'){
            $this->getInputFilter()->remove('quantidadeTransporteEnterro');
        }
        if($this->get('transporteRemocao')->getValue() == 'N'){
            $this->getInputFilter()->remove('quantidadeTransporteRemocao');
        }
        if($this->get('transporteCremacao')->getValue() == 'N'){
            $this->getInputFilter()->remove('quantidadeTransporteCremacao');
        }
        if($this->get('transporteViagem')->getValue() == 'N'){
            $this->getInputFilter()->remove('quantidadeTransporteViagem');
        }
        if($this->get('taxaCamaraFria')->getValue() == 'N'){
            $this->getInputFilter()->remove('quantidadeTaxaCamaraFria');
        }
        if($this->get('taxaCamaraFria')->getValue() == 'N'){
            $this->getInputFilter()->remove('quantidadeTaxaCamaraFria');
        }

        $isValid = parent::isValid();

        return $isValid;
    }


}//END OF CLASS