<?php


namespace Agencia\Form;

use Zend\Form\Form;
use Zend\Session\Container as SessionConteiner;
use Agencia\InputFilter\DadosAmputacao as DadosAmputacaoFilter;
use Agencia\Form\FormularioBase;

class DadosAmputacao extends Form
{
     /**
     * @var
     */
    private $processoRepository;

    /**
     * @var
     */
    private $estados;
    /**
     * @return mixed
     */
    public function getEstados()
    {
        return $this->estados;
    }
     /**
     * @return mixed
     */

    /**
     * @var
     */
   
    private $medicos;
    public function getMedicos()
    {
        return $this->medicos;
    }
    /**
     * @param mixed $medicos
     */
    public function setMedicos($medicos)
    {
        $this->medicos = $medicos;
    }
    /**
     * @param mixed $estados
     */
    public function setEstados($estados)
    {
        $this->estados = $estados;
    }

    /**
     * @return mixed
     */
    public function getProcessoRepository()
    {
        return $this->processoRepository;
    }

    /**
     * @param mixed $processoRepository
     */
    public function setProcessoRepository($processoRepository)
    {
        $this->processoRepository = $processoRepository;
        return $this;
    }

    /**
     * Contratante constructor.
     * @param DadosAmputacaoFilter $inputFilter
     * @param SessionConteiner $contratacao
     */
    public function __construct(DadosAmputacaoFilter $inputFilter, SessionConteiner $contratacao, $estados = [], $processoRepository = '',$medicos=[],$arCamposProcesso = [])
    {
        //Seta o nome do formulário
        parent::__construct('dadosamputacao');

        $this->setLabel('Dados Amputacao');
        $this->setAttribute('icon', 'icon-user');
        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'horizontal-form'));
        $this->setInputFilter($inputFilter);
        $this->setEstados($estados);
        $this->setMedicos($medicos);
        
        if ($contratacao->id) {
            $this->retornaCamposAbaDados($arCamposProcesso, $estados);
            //Submit
            $this->add(array(
                'name' => 'enviar',
                'type' => 'Zend\Form\Element\Submit',
                'attributes' => array(
                    'value' => 'Avançar',
                    'class' => 'btn btn-primary submit-input-loading',
                    'id' => 'enviar'
                )
            ));

            //Adiciona o campo hidden que leva o id
            $this->add(array(
                'name' => 'id',
                'type' => 'Zend\Form\Element\Hidden'
            ));


        }
           
        $this->add(array(
            'name' => 'observacao',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'id' => 'observacao',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Observação:',
                'label_attributes' => array(
                    'class' => 'control-label'
                )
            ),
        ));
        
    }//End __construct

   public function retornaCamposAbaDados($arCamposProcesso = [], $estados = ['' => 'Escolha um Estado'])
    {
       //print_r($arCamposProcesso);die;
        $formularioBase = new FormularioBase();
        foreach ($formularioBase->retornaCamposAbaAmputacao($estados) as $camposAba) {
        
            foreach ($arCamposProcesso as $value) {
                if ($camposAba['name'] == 'representante'){
                    $this->add($camposAba);
                }
                if ($camposAba['name'] == $value['campoVariavel']) {
                    $this->add($camposAba);
                }
            }
        }
    }
    
    public function isValid() {
      /*  if($this->get('destinoFinal')){
            
             $this->getInputFilter()->remove('destinoFinal');
        }
        if($this->get('seraVelado')){
            
             $this->getInputFilter()->remove('seraVelado');
        }
        */
        
        return parent::isValid();
    }
}