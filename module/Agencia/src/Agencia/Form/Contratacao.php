<?php


namespace Agencia\Form;

use Zend\Form\Form;
use Agencia\InputFilter\Contratacao as ContratacaoFilter;

class Contratacao extends Form
{
    private $contratacaoRepository;

    /**
     * @return mixed
     */
    public function getContratacaoRepository()
    {
        return $this->contratacaoRepository;
    }

    /**
     * @param mixed $contratacaoRepository
     */
    public function setContratacaoRepository($contratacaoRepository)
    {
        $this->contratacaoRepository = $contratacaoRepository;
    }

    /**
     * Monta o formulário
     */
    public function __construct(ContratacaoFilter $inputFilter, $operacao = array(), $destino = array())
    {
        //Seta o nome do formulário
        parent::__construct('contratacao');

        $this->setLabel('Contratacao');
        $this->setAttribute('icon', 'icon-user');
        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'horizontal-form'));

        $this->setInputFilter($inputFilter);

        //Adiciona o select de fornecedor
        $this->add(array(
            'name' => 'operacao',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'operacao',
                'class' => 'form-control',
                'onChange' => 'buscaTipoContratacao(jQuery(this).val())'
            ),
            'options' => array(
                'label' => 'Tipo de Operação',
                'value_options' => $operacao,
                'disable_inarray_validator' => true,
                'label_attributes' => array(
                    'class' => 'control-label'
                )
            ),
        ));

        //Adiciona o select de fornecedor
        $this->add(array(
            'name' => 'tipoContratacao',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'tipoContratacao',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Tipo de Contratação',
                'value_options' => ['' => 'Selecione'],
                'disable_inarray_validator' => true,
                'label_attributes' => array(
                    'class' => 'control-label'
                )
            ),
        ));

        //Adiciona o select de fornecedor
        $this->add(array(
            'name' => 'destino',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'destino',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Destino Final',
                'value_options' => $destino,
                'disable_inarray_validator' => true,
                'label_attributes' => array(
                    'class' => 'control-label'
                )
            ),
        ));

        //Monta o submit
        $this->add(array(
            'name' => 'enviar',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => 'OK',
                'class' => 'btn btn-primary submit-input-loading',
                'id' => 'enviar'
            )
        ));

        //Adiciona o campo hidden que leva o id
        $this->add(array(
            'name' => 'id',
            'type' => 'Zend\Form\Element\Hidden'
        ));


    }//End __construct

    public function isValid()
    {
        if ($this->get('operacao')->getValue() == 2) {
            $this->getInputFilter()->remove('destino');
        }
        if ($this->get('operacao')->getValue() == 3) {
            $this->getInputFilter()->remove('tipoContratacao');
            $this->getInputFilter()->remove('destino');
        }

        $valid = parent::isValid();

        return $valid;
    }

}