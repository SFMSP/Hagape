<?php

namespace Agencia\Form;

use Zend\Form\Form;
use Agencia\InputFilter\TransladoCorpo as TransladoCorpoFilter;
use Zend\Session\Container as SessionConteiner;
use Agencia\Form\FormularioBase;

/**
 * Class TransladoCorpo
 * @package Agencia\Form
 */
class TransladoCorpo extends Form
{
    
    /**
     * TransladoCorpo constructor.
     * @param TransladoCorpoFilter $inputFilter
     * @param SessionConteiner $contratacao
     * @param array $estados
     */
    public function __construct(TransladoCorpoFilter $inputFilter, SessionConteiner $contratacao,  $arCamposProcesso = [])
    {
        //Seta o nome do formulário
        parent::__construct('transladoCorpo');

        $this->setLabel('Contratacao');
        $this->setAttribute('icon', 'icon-user');
        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'horizontal-form'));
        $this->setInputFilter($inputFilter);
       

        if ($contratacao->id) {
            $this->retornaCamposAbaTranslado($arCamposProcesso);
            //Submit
            $this->add(array(
                'name' => 'enviar',
                'type' => 'Zend\Form\Element\Submit',
                'attributes' => array(
                    'value' => 'Avançar',
                    'class' => 'btn btn-primary submit-input-loading',
                    'id' => 'enviar'
                )
            ));

            //Adiciona o campo hidden que leva o id
            $this->add(array(
                'name' => 'id',
                'type' => 'Zend\Form\Element\Hidden'
            ));


        }

    }//End __construct

    public function retornaCamposAbaTranslado($arCamposProcesso = [])
    {
        $formularioBase = new FormularioBase();
        foreach ($formularioBase->retornaCamposAbaTranslado() as $camposAba) {
            foreach ($arCamposProcesso as $value) {
                if ($camposAba['name'] == 'representante'){
                    $this->add($camposAba);
                }
                if ($camposAba['name'] == $value['campoVariavel']) {
                    $this->add($camposAba);
                }
            }
        }
    }

}//END OF CLASS