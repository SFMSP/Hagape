<?php


namespace Agencia\Form;

use Zend\Form\Form;
use Zend\Session\Container as SessionConteiner;
use Agencia\InputFilter\Pagamento as PagamentoFilter;
use Agencia\Form\FormularioBase;

class Pagamento extends Form
{
    private $tipoPagamento;
    private $bandeira;
    private $emitente;
    
     /**
     * @return mixed
     */
    public function getTipoPagamento()
    {
        return $this->tipoPagamento;
    }

    /**
     * @param mixed $tipoPagamento
     */
    public function setTipoPagamento($tipoPagamento)
    {
        $this->tipoPagamento = $tipoPagamento;
    }
     /**
     * @return mixed
     */
    public function getBandeira()
    {
        return $this->bandeira;
    }

    /**
     * @param mixed $bandeira
     */
    public function setBandeira($bandeira)
    {
        $this->bandeira = $bandeira;
    }
     /**
     * @return mixed
     */
    public function getEmitente()
    {
        return $this->emitente;
    }

    /**
     * @param mixed $emitente
     */
    public function setEmitente($emitente)
    {
        $this->emitente = $emitente;
    }
    
    
    
    
    
    /**
     * Contratante constructor.
     * @param PagamentoFilter $inputFilter
     * @param SessionConteiner $contratacao
     * @param array $tipoPagamento
     * @param array $bandeira
     * @param array $emitente
     */
    public function __construct(PagamentoFilter $inputFilter, SessionConteiner $contratacao,$arCamposProcesso = [],$tipoPagamento= [],$bandeira= [],$emitente= [])
    {
        //Seta o nome do formulário
        parent::__construct('pagamento');

        $this->setLabel('Pagamento');
        $this->setAttribute('icon', 'icon-user');
        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'horizontal-form'));
        $this->setInputFilter($inputFilter);
        
        $this->setTipoPagamento($tipoPagamento);
        $this->setBandeira($bandeira);
        $this->setEmitente($emitente);
        
        
        if ($contratacao->id) {
           
            $this->retornaCamposAbaDados($arCamposProcesso,$tipoPagamento,$bandeira,$emitente);
            //Submit
            $this->add(array(
                'name' => 'enviar',
                'type' => 'Zend\Form\Element\Submit',
                'attributes' => array(
                    'value' => 'Avançar',
                    'class' => 'btn btn-primary submit-input-loading',
                    'id' => 'enviar'
                )
            ));
          

            //Adiciona o campo hidden que leva o id
            $this->add(array(
                'name' => 'id',
                'type' => 'Zend\Form\Element\Hidden'
            ));
            $this->add(array(
                'name' => 'contratante',
                'type' => 'Zend\Form\Element\Hidden',
                'attributes' => array(
                    'id' => 'contratante',
                    'class' => 'form-control datepicker',
                    'value' => $contratacao->id,
                ),
            ));
            
            


        }
           
        $this->add(array(
            'name' => 'observacao',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'id' => 'observacao',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Observação:',
                'label_attributes' => array(
                    'class' => 'control-label'
                )
            ),
        ));
        
    }//End __construct

   public function retornaCamposAbaDados($arCamposProcesso = [],$tipoPagamento= [],$bandeira= [],$emitente= [])
    {
      
        $formularioBase = new FormularioBase();
        foreach ($formularioBase->retornaCamposAbaPagamento($tipoPagamento,$bandeira,$emitente) as $camposAba) {
        
            foreach ($arCamposProcesso as $value) {
                if ($camposAba['name'] == 'representante'){
                    $this->add($camposAba);
                }
                if ($camposAba['name'] == $value['campoVariavel']) {
                    $this->add($camposAba);
                }
            }
        }
    }
    
    public function isValid() {
      /*  if($this->get('destinoFinal')){
            
             $this->getInputFilter()->remove('destinoFinal');
        }
        if($this->get('seraVelado')){
            
             $this->getInputFilter()->remove('seraVelado');
        }
        */
        
        return parent::isValid();
    }
}