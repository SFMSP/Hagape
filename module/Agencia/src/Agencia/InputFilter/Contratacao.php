<?php


namespace Agencia\InputFilter;

use Base\Validator\BaseValidator;

class Contratacao extends BaseValidator
{
    /**
     * Monta os filtros e as validações
     */
    public function __construct()
    {
        //Monta as validações de campos obrigatórios padrão
        $obrigatoriosPadrao = array(
            "operacao" => "Campo obrigatório",
            "tipoContratacao" => "Campo obrigatório",
            "destino" => "Campo obrigatório"
        );

        $this->addEmptyValidators($obrigatoriosPadrao);
    }
}