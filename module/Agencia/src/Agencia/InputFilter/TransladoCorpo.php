<?php


namespace Agencia\InputFilter;

use Base\Validator\BaseValidator;

class TransladoCorpo extends BaseValidator
{
    /**
     * Monta os filtros e as validações
     */
    public function __construct($campos = [])
    {
        $obrigatoriosPadrao = [];
        foreach ($campos as $campo) {
            if ($campo['campoVariavel'] != '' && $campo['obrigatorio'] == 1) {
                $obrigatoriosPadrao[$campo['campoVariavel']] = "Campo obrigatório";
            }
        }

        $this->addEmptyValidators($obrigatoriosPadrao);
    }
}