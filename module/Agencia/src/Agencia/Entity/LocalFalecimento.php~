<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;
/**
 * LocalFalecimento
 *
 * @ORM\Table(name="tb_local_falecimento", indexes={@ORM\Index(name="fk_tb_local_falecimento_tb_cartorio2_idx", columns={"id_cartorio"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\LocalFalecimentoRepository")
 */
class LocalFalecimento extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_local_falecimento", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_local", type="string", length=255, nullable=true)
     */
    private $local;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_endereco", type="string", length=255, nullable=true)
     */
    private $endereco;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_telefone", type="string", length=15, nullable=true)
     */
    private $telefone;

    /**
     * @var integer
     *
     * @ORM\Column(name="tipo", type="integer", nullable=true)
     */
    private $tipo;

    /**
     * @var \Agencia\Entity\Cartorio
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Cartorio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cartorio", referencedColumnName="id_cartorio")
     * })
     */
    private $cartorio;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLocal()
    {
        return $this->local;
    }

    /**
     * @param string $local
     */
    public function setLocal($local)
    {
        $this->local = $local;
    }

    /**
     * @return string
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param string $endereco
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    }

    /**
     * @return string
     */
    public function getTelefone()
    {
        return $this->telefone;
    }

    /**
     * @param string $telefone
     */
    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;
    }

    /**
     * @return int
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param int $tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    /**
     * @return Cartorio
     */
    public function getCartorio()
    {
        return $this->cartorio;
    }

    /**
     * @param Cartorio $cartorio
     */
    public function setCartorio($cartorio)
    {
        $this->cartorio = $cartorio;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'local' => $this->local,
            'endereco' => $this->endereco,
            'telefone' => $this->telefone,
            'tipo' => $this->tipo,
            'cartorio' => $this->cartorio
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->local;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
