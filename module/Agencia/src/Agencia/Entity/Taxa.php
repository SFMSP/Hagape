<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Taxa
 *
 * @ORM\Table(name="tb_taxa", indexes={@ORM\Index(name="fk_tb_taxa_tb_contratacao1_idx", columns={"id_contratacao"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\TaxaRepository")
 */
class Taxa extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_taxa", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cremacao", type="boolean", nullable=true)
     */
    private $cremacao;

    /**
     * @var boolean
     *
     * @ORM\Column(name="sepultamento", type="boolean", nullable=true)
     */
    private $sepultamento;

    /**
     * @var boolean
     *
     * @ORM\Column(name="camara_fria", type="boolean", nullable=true)
     */
    private $camaraFria;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantidade", type="integer", nullable=true)
     */
    private $quantidade;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_conjunto", type="decimal", precision=7, scale=2, nullable=true)
     */
    private $valorConjunto;

    /**
     * @var boolean
     *
     * @ORM\Column(name="outros", type="boolean", nullable=true)
     */
    private $outros;

    /**
     * @var boolean
     *
     * @ORM\Column(name="creditar", type="boolean", nullable=true)
     */
    private $creditar;

    /**
     * @var boolean
     *
     * @ORM\Column(name="debitar", type="boolean", nullable=true)
     */
    private $debitar;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_total", type="decimal", precision=7, scale=2, nullable=true)
     */
    private $valorTotal;

    /**
     * @var string
     *
     * @ORM\Column(name="observacao", type="text", nullable=true)
     */
    private $observacao;

    /**
     * @var \Agencia\Entity\Contratacao
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Contratacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contratacao", referencedColumnName="id_contratacao")
     * })
     */
    private $contratacao;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return boolean
     */
    public function isCremacao()
    {
        return $this->cremacao;
    }

    /**
     * @param boolean $cremacao
     */
    public function setCremacao($cremacao)
    {
        $this->cremacao = $cremacao;
    }

    /**
     * @return boolean
     */
    public function isSepultamento()
    {
        return $this->sepultamento;
    }

    /**
     * @param boolean $sepultamento
     */
    public function setSepultamento($sepultamento)
    {
        $this->sepultamento = $sepultamento;
    }

    /**
     * @return boolean
     */
    public function isCamaraFria()
    {
        return $this->camaraFria;
    }

    /**
     * @param boolean $camaraFria
     */
    public function setCamaraFria($camaraFria)
    {
        $this->camaraFria = $camaraFria;
    }

    /**
     * @return int
     */
    public function getQuantidade()
    {
        return $this->quantidade;
    }

    /**
     * @param int $quantidade
     */
    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;
    }

    /**
     * @return string
     */
    public function getValorConjunto()
    {
        return $this->valorConjunto;
    }

    /**
     * @param string $valorConjunto
     */
    public function setValorConjunto($valorConjunto)
    {
        $this->valorConjunto = $valorConjunto;
    }

    /**
     * @return boolean
     */
    public function isOutros()
    {
        return $this->outros;
    }

    /**
     * @param boolean $outros
     */
    public function setOutros($outros)
    {
        $this->outros = $outros;
    }

    /**
     * @return boolean
     */
    public function isCreditar()
    {
        return $this->creditar;
    }

    /**
     * @param boolean $creditar
     */
    public function setCreditar($creditar)
    {
        $this->creditar = $creditar;
    }

    /**
     * @return boolean
     */
    public function isDebitar()
    {
        return $this->debitar;
    }

    /**
     * @param boolean $debitar
     */
    public function setDebitar($debitar)
    {
        $this->debitar = $debitar;
    }

    /**
     * @return string
     */
    public function getValorTotal()
    {
        return $this->valorTotal;
    }

    /**
     * @param string $valorTotal
     */
    public function setValorTotal($valorTotal)
    {
        $this->valorTotal = $valorTotal;
    }

    /**
     * @return string
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * @param string $observacao
     */
    public function setObservacao($observacao)
    {
        $this->observacao = $observacao;
    }

    /**
     * @return Contratacao
     */
    public function getContratacao()
    {
        return $this->contratacao;
    }

    /**
     * @param Contratacao $contratacao
     */
    public function setContratacao($contratacao)
    {
        $this->contratacao = $contratacao;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'cremacao' => $this->cremacao,
            'sepultamento' => $this->sepultamento,
            'camaraFria' => $this->camaraFria,
            'quantidade' => $this->quantidade,
            'valorConjunto' => $this->valorConjunto,
            'outros' => $this->outros,
            'creditar' => $this->creditar,
            'debitar' => $this->debitar,
            'valorTotal' => $this->valorTotal,
            'observacao' => $this->observacao,
            'contratacao' => $this->contratacao
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->observacao;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }



    /**
     * Get cremacao
     *
     * @return boolean 
     */
    public function getCremacao()
    {
        return $this->cremacao;
    }

    /**
     * Get sepultamento
     *
     * @return boolean 
     */
    public function getSepultamento()
    {
        return $this->sepultamento;
    }

    /**
     * Get camaraFria
     *
     * @return boolean 
     */
    public function getCamaraFria()
    {
        return $this->camaraFria;
    }

    /**
     * Get outros
     *
     * @return boolean 
     */
    public function getOutros()
    {
        return $this->outros;
    }

    /**
     * Get creditar
     *
     * @return boolean 
     */
    public function getCreditar()
    {
        return $this->creditar;
    }

    /**
     * Get debitar
     *
     * @return boolean 
     */
    public function getDebitar()
    {
        return $this->debitar;
    }
}
