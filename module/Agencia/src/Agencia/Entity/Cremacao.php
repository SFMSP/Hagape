<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Cremacao
 *
 * @ORM\Table(name="tb_cremacao", indexes={@ORM\Index(name="fk_tb_cremacao_tb_venda_idx", columns={"id_contratacao"}), @ORM\Index(name="fk_tb_cremacao_tb_dados_obito1_idx", columns={"id_dados_obito"}), @ORM\Index(name="fk_tb_cremacao_tb_dados_obito_velorio1_idx", columns={"id_dados_obito_velorio"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\CremacaoRepository")
 */
class Cremacao extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_cremacao", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_crematorio", type="string", length=255, nullable=true)
     */
    private $crematorio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cremacao", type="date", nullable=true)
     */
    private $dataCremacao;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_hora_cremacao", type="string", length=45, nullable=true)
     */
    private $horaCremacao;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_municipio", type="string", length=255, nullable=true)
     */
    private $municipio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="date", nullable=true)
     */
    private $dataCadastro;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_endereco", type="string", length=255, nullable=true)
     */
    private $endereco;

    /**
     * @var \Admin\Entity\Cemiterio
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Cemiterio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cemiterio", referencedColumnName="id_cemiterio")
     * })
     */
    private $cemiterio;

    /**
     * @var \Agencia\Entity\Contratacao
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Contratacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contratacao", referencedColumnName="id_contratacao")
     * })
     */
    private $contratacao;

    /**
     * @var \Agencia\Entity\DadosObito
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\DadosObito")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_dados_obito", referencedColumnName="id_dados_obito")
     * })
     */
    private $dadosObito;

    /**
     * @var \Agencia\Entity\DadosObitoVelorio
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\DadosObitoVelorio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_dados_obito_velorio", referencedColumnName="id_dados_obito_velorio")
     * })
     */
    private $dadosObitoVelorio;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCrematorio()
    {
        return $this->crematorio;
    }

    /**
     * @param string $crematorio
     */
    public function setCrematorio($crematorio)
    {
        $this->crematorio = $crematorio;
    }

    /**
     * @return \DateTime
     */
    public function getDataCremacao()
    {
        return $this->dataCremacao;
    }

    /**
     * @param \DateTime $dataCremacao
     */
    public function setDataCremacao($dataCremacao)
    {
        $this->dataCremacao = $dataCremacao;
    }

    /**
     * @return string
     */
    public function getHoraCremacao()
    {
        return $this->horaCremacao;
    }

    /**
     * @param string $horaCremacao
     */
    public function setHoraCremacao($horaCremacao)
    {
        $this->horaCremacao = $horaCremacao;
    }

    /**
     * @return string
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }

    /**
     * @param string $municipio
     */
    public function setMunicipio($municipio)
    {
        $this->municipio = $municipio;
    }

    /**
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param \DateTime $dataCadastro
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
    }

    /**
     * @return string
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param string $endereco
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    }

       
    function getCemiterio() {
        
        return $this->cemiterio;
    }

    function setIdCemiterio(\Admin\Entity\Cemiterio $cemiterio) {
        $this->cemiterio = $cemiterio;
    }
    
    
    

    /**
     * @return Contratacao
     */
    public function getContratacao()
    {
        return $this->contratacao;
    }

    /**
     * @param Contratacao $contratacao
     */
    public function setContratacao($contratacao)
    {
        $this->contratacao = $contratacao;
    }

    /**
     * @return DadosObito
     */
    public function getDadosObito()
    {
        return $this->dadosObito;
    }

    /**
     * @param DadosObito $dadosObito
     */
    public function setDadosObito($dadosObito)
    {
        $this->dadosObito = $dadosObito;
    }

    /**
     * @return DadosObitoVelorio
     */
    public function getDadosObitoVelorio()
    {
        return $this->dadosObitoVelorio;
    }

    /**
     * @param DadosObitoVelorio $dadosObitoVelorio
     */
    public function setDadosObitoVelorio($dadosObitoVelorio)
    {
        $this->dadosObitoVelorio = $dadosObitoVelorio;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'crematorio' => $this->crematorio,
            'dataCremacao' => $this->dataCremacao->format('d/m/Y'),
            'horaCremacao' => $this->horaCremacao,
            'municipio' => $this->municipio,
            'dataCadastro' => $this->dataCadastro->format('d/m/Y'),
            'endereco' => $this->endereco,
            'cemiterio' => $this->cemiterio,
            'contratacao' => $this->contratacao,
            'dadosObito' => $this->dadosObito,
            'dadosObitoVelorio' => $this->dadosObitoVelorio,
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->crematorio;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
