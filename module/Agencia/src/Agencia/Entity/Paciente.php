<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Paciente
 *
 * @ORM\Table(name="tb_paciente", indexes={@ORM\Index(name="fk_tb_paciente_tb_venda1_idx", columns={"id_contratacao"}), @ORM\Index(name="fk_tb_paciente_tb_contratante1_idx", columns={"id_contratante"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\PacienteRepository")
 */
class Paciente extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_paciente", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome_paciente", type="string", length=255, nullable=true)
     */
    private $nomePaciente;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_membro_amputado", type="string", length=255, nullable=true)
     */
    private $membroAmputado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=true)
     */
    private $dataCadastro;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_peca_anatomica", type="boolean", nullable=true)
     */
    private $pecaAnatomica;

    /**
     * @var \Agencia\Entity\Contratacao
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Contratacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contratacao", referencedColumnName="id_contratacao")
     * })
     */
    private $contratacao;

    /**
     * @var \Agencia\Entity\Contratante
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Contratante")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contratante", referencedColumnName="id_contratante")
     * })
     */
    private $contratante;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNomePaciente()
    {
        return $this->nomePaciente;
    }

    /**
     * @param string $nomePaciente
     */
    public function setNomePaciente($nomePaciente)
    {
        $this->nomePaciente = $nomePaciente;
    }

    /**
     * @return string
     */
    public function getMembroAmputado()
    {
        return $this->membroAmputado;
    }

    /**
     * @param string $membroAmputado
     */
    public function setMembroAmputado($membroAmputado)
    {
        $this->membroAmputado = $membroAmputado;
    }

    /**
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param \DateTime $dataCadastro
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
    }

    /**
     * @return boolean
     */
    public function isPecaAnatomica()
    {
        return $this->pecaAnatomica;
    }

    /**
     * @param boolean $pecaAnatomica
     */
    public function setPecaAnatomica($pecaAnatomica)
    {
        $this->pecaAnatomica = $pecaAnatomica;
    }

    /**
     * @return Contratacao
     */
    public function getContratacao()
    {
        return $this->contratacao;
    }

    /**
     * @param Contratacao $contratacao
     */
    public function setContratacao($contratacao)
    {
        $this->contratacao = $contratacao;
    }

    /**
     * @return Contratante
     */
    public function getContratante()
    {
        return $this->contratante;
    }

    /**
     * @param Contratante $contratante
     */
    public function setContratante($contratante)
    {
        $this->contratante = $contratante;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'nomePaciente' => $this->nomePaciente,
            'membroAmputado' => $this->membroAmputado,
            'dataCadastro' => $this->dataCadastro->format('d/m/Y'),
            'pecaAnatomica' => $this->pecaAnatomica,
            'contratacao' => $this->contratacao,
            'contratante' => $this->contratante
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->nomePaciente;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }



    /**
     * Get pecaAnatomica
     *
     * @return boolean 
     */
    public function getPecaAnatomica()
    {
        return $this->pecaAnatomica;
    }
}
