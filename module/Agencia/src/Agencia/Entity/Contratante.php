<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;
/**
 * Contratante
 *
 * @ORM\Table(name="tb_contratante", indexes={@ORM\Index(name="fk_tb_contratante_tb_venda1_idx", columns={"id_contratacao"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\ContratanteRepository")
 */
class Contratante extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_contratante", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome_contratante", type="string", length=255, nullable=true)
     */
    private $nomeContratante;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_cpf", type="string", length=15, nullable=true)
     */
    private $cpf;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_rg", type="string", length=15, nullable=true)
     */
    private $rg;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_profissao", type="string", length=150, nullable=true)
     */
    private $profissao;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_grau_parentesco", type="string", length=150, nullable=true)
     */
    private $grauParentesco;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_telefone_residencial", type="string", length=50, nullable=true)
     */
    private $telefoneResidencial;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_telefone_celular", type="string", length=50, nullable=true)
     */
    private $telefoneCelular;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome_mae", type="string", length=255, nullable=true)
     */
    private $nomeMae;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=true)
     */
    private $dataCadastro;

    /**
     * @var \Agencia\Entity\Contratacao
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Contratacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contratacao", referencedColumnName="id_contratacao")
     * })
     */
    private $contratacao;
    
     /**
     * @var \Agencia\Entity\DadosObitoVelorio
     *
     * @ORM\OneToMany(targetEntity="Agencia\Entity\DadosObitoVelorio", mappedBy="contratante")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contratante", referencedColumnName="id_contratante")
     * })
     */
    private $dadosObitoVelorio;
    
   
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNomeContratante()
    {
        return $this->nomeContratante;
    }

    /**
     * @param string $nomeContratante
     */
    public function setNomeContratante($nomeContratante)
    {
        $this->nomeContratante = $nomeContratante;
    }

    /**
     * @return string
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * @param string $cpf
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
    }

    /**
     * @return string
     */
    public function getRg()
    {
        return $this->rg;
    }

    /**
     * @param string $rg
     */
    public function setRg($rg)
    {
        $this->rg = $rg;
    }

    /**
     * @return string
     */
    public function getProfissao()
    {
        return $this->profissao;
    }

    /**
     * @param string $profissao
     */
    public function setProfissao($profissao)
    {
        $this->profissao = $profissao;
    }

    /**
     * @return string
     */
    public function getGrauParentesco()
    {
        return $this->grauParentesco;
    }

    /**
     * @param string $grauParentesco
     */
    public function setGrauParentesco($grauParentesco)
    {
        $this->grauParentesco = $grauParentesco;
    }

    /**
     * @return string
     */
    public function getTelefoneResidencial()
    {
        return $this->telefoneResidencial;
    }

    /**
     * @param string $telefoneResidencial
     */
    public function setTelefoneResidencial($telefoneResidencial)
    {
        $this->telefoneResidencial = $telefoneResidencial;
    }

    /**
     * @return string
     */
    public function getTelefoneCelular()
    {
        return $this->telefoneCelular;
    }

    /**
     * @param string $telefoneCelular
     */
    public function setTelefoneCelular($telefoneCelular)
    {
        $this->telefoneCelular = $telefoneCelular;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getNomeMae()
    {
        return $this->nomeMae;
    }

    /**
     * @param string $nomeMae
     */
    public function setNomeMae($nomeMae)
    {
        $this->nomeMae = $nomeMae;
    }

    /**
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param \DateTime $dataCadastro
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
    }

    /**
     * @return Contratacao
     */
    public function getContratacao()
    {
        return $this->contratacao;
    }

    /**
     * @param Contratacao $contratacao
     */
    public function setContratacao($contratacao)
    {
        $this->contratacao = $contratacao;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'nomeContratante' => $this->nomeContratante,
            'cpf' => $this->cpf,
            'rg' => $this->rg,
            'profissao' => $this->profissao,
            'grauParentesco' => $this->grauParentesco,
            'telefoneResidencial' => $this->telefoneResidencial,
            'telefoneCelular' => $this->telefoneCelular,
            'email' => $this->email,
            'nomeMae' => $this->nomeMae,
            'dataCadastro' => $this->dataCadastro->format('d/m/Y'),
            'contratacao' => $this->contratacao,
           
        ];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->nomeContratante;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
