<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Sepultamento
 *
 * @ORM\Table(name="tb_sepultamento", indexes={@ORM\Index(name="fk_tb_sepultamento_tb_dados_obito1_idx", columns={"id_dados_obito"}), @ORM\Index(name="fk_tb_sepultamento_tb_venda1_idx", columns={"id_contratacao"}), @ORM\Index(name="fk_tb_sepultamento_tb_dados_amaputacao1_idx", columns={"id_dados_amaputacao"}), @ORM\Index(name="fk_tb_sepultamento_tb_dados_obito_velorio1_idx", columns={"id_dados_obito_velorio"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\SepultamentoRepository")
 */
class Sepultamento extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_sepultamento", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

     /**
     * @var \Admin\Entity\Cemiterio
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Cemiterio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cemiterio", referencedColumnName="id_cemiterio")
     * })
     */
    private $cemiterio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_sepultamento", type="date", nullable=true)
     */
    private $dataSepultamento;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_hora_sepultamento", type="string", length=45, nullable=true)
     */
    private $horaSepultamento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=true)
     */
    private $dataCadastro;

    /**
     * @var \Agencia\Entity\DadosObito
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\DadosObito")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_dados_obito", referencedColumnName="id_dados_obito")
     * })
     */
    private $dadosObito;

    /**
     * @var \Agencia\Entity\Contratacao
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Contratacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contratacao", referencedColumnName="id_contratacao")
     * })
     */
    private $contratacao;

    /**
     * @var \Agencia\Entity\DadosAmputacao
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\DadosAmputacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_dados_amaputacao", referencedColumnName="id_dados_amaputacao")
     * })
     */
    private $dadosAmputacao;

    /**
     * @var \Agencia\Entity\DadosObitoVelorio
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\DadosObitoVelorio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_dados_obito_velorio", referencedColumnName="id_dados_obito_velorio")
     * })
     */
    private $dadosObitoVelorio;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

   
    function getCemiterio() {
        return $this->cemiterio;
    }

    function setCemiterio(\Admin\Entity\Cemiterio $cemiterio) {
        $this->cemiterio = $cemiterio;
    }

    /**
     * @return \DateTime
     */
    public function getDataSepultamento()
    {
        return $this->dataSepultamento;
    }

    /**
     * @param \DateTime $dataSepultamento
     */
    public function setDataSepultamento($dataSepultamento)
    {
        $this->dataSepultamento = $dataSepultamento;
    }

    /**
     * @return string
     */
    public function getHoraSepultamento()
    {
        return $this->horaSepultamento;
    }

    /**
     * @param string $horaSepultamento
     */
    public function setHoraSepultamento($horaSepultamento)
    {
        $this->horaSepultamento = $horaSepultamento;
    }

    /**
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param \DateTime $dataCadastro
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
    }

    /**
     * @return DadosObito
     */
    public function getDadosObito()
    {
        return $this->dadosObito;
    }

    /**
     * @param DadosObito $dadosObito
     */
    public function setDadosObito($dadosObito)
    {
        $this->dadosObito = $dadosObito;
    }

    /**
     * @return Contratacao
     */
    public function getContratacao()
    {
        return $this->contratacao;
    }

    /**
     * @param Contratacao $contratacao
     */
    public function setContratacao($contratacao)
    {
        $this->contratacao = $contratacao;
    }

    /**
     * @return DadosAmputacao
     */
    public function getDadosAmputacao()
    {
        return $this->dadosAmputacao;
    }

    /**
     * @param DadosAmputacao $dadosAmputacao
     */
    public function setDadosAmputacao($dadosAmputacao)
    {
        $this->dadosAmputacao = $dadosAmputacao;
    }

    /**
     * @return DadosObitoVelorio
     */
    public function getDadosObitoVelorio()
    {
        return $this->dadosObitoVelorio;
    }

    /**
     * @param DadosObitoVelorio $dadosObitoVelorio
     */
    public function setDadosObitoVelorio($dadosObitoVelorio)
    {
        $this->dadosObitoVelorio = $dadosObitoVelorio;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'cemiterio' => $this->cemiterio,
            'dataSepultamento' => $this->dataSepultamento->format('d/m/Y'),
            'horaSepultamento' => $this->horaSepultamento,
            'dataCadastro' => $this->dataCadastro->format('d/m/Y'),
            'dadosObito' => $this->dadosObito,
            'contratacao' => $this->contratacao,
            'dadosAmputacao' => $this->dadosAmputacao,
            'dadosObitoVelorio' => $this->dadosObitoVelorio
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return (string)$this->cemiterio;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
