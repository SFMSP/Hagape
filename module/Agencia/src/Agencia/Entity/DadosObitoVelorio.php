<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * DadosObitoVelorio
 *
 * @ORM\Table(name="tb_dados_obito_velorio", indexes={@ORM\Index(name="fk_tb_dados_obito_velorio_tb_venda1_idx", columns={"id_contratacao"}), @ORM\Index(name="fk_tb_dados_obito_velorio_tb_local_falecimento1_idx", columns={"id_local_falecimento"}), @ORM\Index(name="fk_tb_dados_obito_velorio_tb_contratante1_idx", columns={"id_contratante"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\DadosObitoVelorioRepository")
 */
class DadosObitoVelorio extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_dados_obito_velorio", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=true)
     */
    private $dataCadastro;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome_falecido", type="string", length=255, nullable=true)
     */
    private $nomeFalecido;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_endereco", type="string", length=255, nullable=true)
     */
    private $endereco;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_falecimento", type="date", nullable=true)
     */
    private $dataFalecimento;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_cidade_procedencia", type="string", length=255, nullable=true)
     */
    private $cidadeProcedencia;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_causa_mortis", type="string", length=255, nullable=true)
     */
    private $causaMortis;

    /**
     * @var integer
     *
     * @ORM\Column(name="tamanho_caixao", type="integer", nullable=true)
     */
    private $tamanhoCaixao;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_corpo_velado", type="boolean", nullable=true)
     */
    private $corpoVelado;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_observacao", type="text", nullable=true)
     */
    private $observacao;

    /**
     * @var \Agencia\Entity\Contratacao
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Contratacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contratacao", referencedColumnName="id_contratacao")
     * })
     */
    private $contratacao;

    /**
     * @var \Agencia\Entity\LocalFalecimento
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\LocalFalecimento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_local_falecimento", referencedColumnName="id_local_falecimento")
     * })
     */
    private $localFalecimento;

    /**
     * @var \Agencia\Entity\Contratante
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Contratante", inversedBy="dadosObitoVelorio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contratante", referencedColumnName="id_contratante")
     * })
     */
    private $contratante;

    
    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Agencia\Entity\Medico", inversedBy="dadosObitoVelorio")
     * @ORM\JoinTable(name="tb_dados_obito_velorio_tb_medico",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_dados_obito_velorio", referencedColumnName="id_dados_obito_velorio")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_medico", referencedColumnName="id_medico")
     *   }
     * )
     */
    
    private $medico;
    /**
     * @var string
     *
     * @ORM\Column(name="txt_local_falecimento", type="string", length=255, nullable=true)
     */
    private $ondeFaleceu;
    function getOndeFaleceu() {
        return $this->ondeFaleceu;
    }

    function setOndeFaleceu($ondeFaleceu) {
        $this->ondeFaleceu = $ondeFaleceu;
    }

        /**
     * Constructor
     */
    public function __construct()
    {
        $this->medico = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param \DateTime $dataCadastro
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
    }

    /**
     * @return string
     */
    public function getNomeFalecido()
    {
        return $this->nomeFalecido;
    }

    /**
     * @param string $nomeFalecido
     */
    public function setNomeFalecido($nomeFalecido)
    {
        $this->nomeFalecido = $nomeFalecido;
    }

    /**
     * @return string
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param string $endereco
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    }

    /**
     * @return \DateTime
     */
    public function getDataFalecimento()
    {
        return $this->dataFalecimento;
    }

    /**
     * @param \DateTime $dataFalecimento
     */
    public function setDataFalecimento($dataFalecimento)
    {
        $this->dataFalecimento = $dataFalecimento;
    }

    /**
     * @return string
     */
    public function getCidadeProcedencia()
    {
        return $this->cidadeProcedencia;
    }

    /**
     * @param string $cidadeProcedencia
     */
    public function setCidadeProcedencia($cidadeProcedencia)
    {
        $this->cidadeProcedencia = $cidadeProcedencia;
    }

    /**
     * @return string
     */
    public function getCausaMortis()
    {
        return $this->causaMortis;
    }

    /**
     * @param string $causaMortis
     */
    public function setCausaMortis($causaMortis)
    {
        $this->causaMortis = $causaMortis;
    }

    /**
     * @return int
     */
    public function getTamanhoCaixao()
    {
        return $this->tamanhoCaixao;
    }

    /**
     * @param int $tamanhoCaixao
     */
    public function setTamanhoCaixao($tamanhoCaixao)
    {
        $this->tamanhoCaixao = $tamanhoCaixao;
    }

    /**
     * @return boolean
     */
    public function isCorpoVelado()
    {
        return $this->corpoVelado;
    }

    /**
     * @param boolean $corpoVelado
     */
    public function setCorpoVelado($corpoVelado)
    {
        $this->corpoVelado = $corpoVelado;
    }

    /**
     * @return string
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * @param string $observacao
     */
    public function setObservacao($observacao)
    {
        $this->observacao = $observacao;
    }

    /**
     * @return Contratacao
     */
    public function getContratacao()
    {
        return $this->contratacao;
    }

    /**
     * @param Contratacao $contratacao
     */
    public function setContratacao($contratacao)
    {
        $this->contratacao = $contratacao;
    }

    /**
     * @return LocalFalecimento
     */
    public function getLocalFalecimento()
    {
        return $this->localFalecimento;
    }

    /**
     * @param LocalFalecimento $localFalecimento
     */
    public function setLocalFalecimento($localFalecimento)
    {
        $this->localFalecimento = $localFalecimento;
    }

    /**
     * @return Contratante
     */
    public function getContratante()
    {
        return $this->contratante;
    }

    /**
     * @param Contratante $contratante
     */
    public function setContratante($contratante)
    {
        $this->contratante = $contratante;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMedico()
    {
        return $this->medico;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $medico
     */
    public function setMedico($medico)
    {
        $this->medico = $medico;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'dataCadastro' => $this->dataCadastro->format('d/m/Y'),
            'nomeFalecido' => $this->nomeFalecido,
            'endereco' => $this->endereco,
            'dataFalecimento' => $this->dataFalecimento->format('d/m/Y'),
            'cidadeProcedencia' => $this->cidadeProcedencia,
            'causaMortis' => $this->causaMortis,
            'tamanhoCaixao' => $this->tamanhoCaixao,
            'corpoVelado' => $this->corpoVelado,
            'observacao' => $this->observacao,
            'contratacao' => $this->contratacao,
            'localFalecimento' => $this->localFalecimento,
            'contratante' => $this->contratante,
            'medico' => $this->medico,
            'ondeFaleceu' => $this->ondeFaleceu
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->nomeFalecido;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }



    /**
     * Get corpoVelado
     *
     * @return boolean 
     */
    public function getCorpoVelado()
    {
        return $this->corpoVelado;
    }

    /**
     * Add medico
     *
     * @param \Agencia\Entity\Medico $medico
     * @return DadosObitoVelorio
     */
    public function addMedico(\Agencia\Entity\Medico $medico)
    {
        $this->medico[] = $medico;

        return $this;
    }

    /**
     * Remove medico
     *
     * @param \Agencia\Entity\Medico $medico
     */
    public function removeMedico(\Agencia\Entity\Medico $medico)
    {
        $this->medico->removeElement($medico);
    }
}
