<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * TipoDestinoFinal
 *
 * @ORM\Table(name="tb_tipo_destino_final")
 * @ORM\Entity(repositoryClass="Agencia\Repository\TipoDestinoFinalRepository")
 */
class TipoDestinoFinal extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_tipo_destino_final", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_destino_final", type="string", length=255, nullable=true)
     */
    private $destinoFinal;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDestinoFinal()
    {
        return $this->destinoFinal;
    }

    /**
     * @param string $destinoFinal
     */
    public function setDestinoFinal($destinoFinal)
    {
        $this->destinoFinal = $destinoFinal;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'destinoFinal' => $this->destinoFinal
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->destinoFinal;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
