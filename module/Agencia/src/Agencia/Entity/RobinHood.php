<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * RobinHood
 *
 * @ORM\Table(name="tb_robin_hood", indexes={@ORM\Index(name="fk_tb_robin_hood_tb_servico1_idx", columns={"id_servico"}), @ORM\Index(name="fk_tb_robin_hood_tb_classificacao_venda1_idx", columns={"id_classificacao_venda"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\RobinHoodRepository")
 */
class RobinHood extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_robin_hood", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fator", type="string", length=150, nullable=true)
     */
    private $fator;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_categoria", type="integer", nullable=true)
     */
    private $categoria;

    /**
     * @var \Agencia\Entity\Servico
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Servico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_servico", referencedColumnName="id_servico")
     * })
     */
    private $servico;

    /**
     * @var \Agencia\Entity\ClassificacaoVenda
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\ClassificacaoVenda")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_classificacao_venda", referencedColumnName="id_classificacao_venda")
     * })
     */
    private $classificacaoVenda;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getFator()
    {
        return $this->fator;
    }

    /**
     * @param string $fator
     */
    public function setFator($fator)
    {
        $this->fator = $fator;
    }

    /**
     * @return int
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param int $categoria
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }

    /**
     * @return Servico
     */
    public function getServico()
    {
        return $this->servico;
    }

    /**
     * @param Servico $servico
     */
    public function setServico($servico)
    {
        $this->servico = $servico;
    }

    /**
     * @return ClassificacaoVenda
     */
    public function getClassificacaoVenda()
    {
        return $this->classificacaoVenda;
    }

    /**
     * @param ClassificacaoVenda $classificacaoVenda
     */
    public function setClassificacaoVenda($classificacaoVenda)
    {
        $this->classificacaoVenda = $classificacaoVenda;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'fator' => $this->fator,
            'categoria' => $this->categoria,
            'servico' => $this->servico,
            'classificacaoVenda' => $this->classificacaoVenda
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->fator;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
