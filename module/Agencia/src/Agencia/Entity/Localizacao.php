<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Localizacao
 *
 * @ORM\Table(name="tb_localizacao", indexes={@ORM\Index(name="fk_tb_localizacao_tb_declarante1_idx", columns={"id_declarante"}), @ORM\Index(name="fk_tb_localizacao_tb_falecido1_idx", columns={"id_falecido"}), @ORM\Index(name="fk_tb_localizacao_tb_contratante1_idx", columns={"id_contratante"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\LocalizacaoRepository")
 */
class Localizacao extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_localizacao", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_endereco", type="string", length=255, nullable=true)
     */
    private $endereco;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_bairro", type="string", length=255, nullable=true)
     */
    private $bairro;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_numero", type="string", length=45, nullable=true)
     */
    private $numero;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_cidade", type="integer", nullable=true)
     */
    private $cidade;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_complemento", type="string", length=45, nullable=true)
     */
    private $complemento;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_cep", type="string", length=20, nullable=true)
     */
    private $cep;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=true)
     */
    private $dataCadastro;

    /**
     * @var \Agencia\Entity\Declarante
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Declarante")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_declarante", referencedColumnName="id_declarante")
     * })
     */
    private $declarante;

    /**
     * @var \Agencia\Entity\Falecido
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Falecido")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_falecido", referencedColumnName="id_falecido")
     * })
     */
    private $falecido;

    /**
     * @var \Agencia\Entity\Contratante
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Contratante")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contratante", referencedColumnName="id_contratante")
     * })
     */
    private $contratante;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param string $endereco
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    }

    /**
     * @return string
     */
    public function getBairro()
    {
        return $this->bairro;
    }

    /**
     * @param string $bairro
     */
    public function setBairro($bairro)
    {
        $this->bairro = $bairro;
    }

    /**
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param string $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    /**
     * @return int
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * @param int $cidade
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;
    }

    /**
     * @return string
     */
    public function getComplemento()
    {
        return $this->complemento;
    }

    /**
     * @param string $complemento
     */
    public function setComplemento($complemento)
    {
        $this->complemento = $complemento;
    }

    /**
     * @return string
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * @param string $cep
     */
    public function setCep($cep)
    {
        $this->cep = $cep;
    }

    /**
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param \DateTime $dataCadastro
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
    }

    /**
     * @return Declarante
     */
    public function getDeclarante()
    {
        return $this->declarante;
    }

    /**
     * @param Declarante $declarante
     */
    public function setDeclarante($declarante)
    {
        $this->declarante = $declarante;
    }

    /**
     * @return Falecido
     */
    public function getFalecido()
    {
        return $this->falecido;
    }

    /**
     * @param Falecido $falecido
     */
    public function setFalecido($falecido)
    {
        $this->falecido = $falecido;
    }

    /**
     * @return Contratante
     */
    public function getContratante()
    {
        return $this->contratante;
    }

    /**
     * @param Contratante $contratante
     */
    public function setContratante($contratante)
    {
        $this->contratante = $contratante;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'endereco' => $this->endereco,
            'bairro' => $this->bairro,
            'numero' => $this->numero,
            'cidade' => $this->cidade,
            'complemento' => $this->complemento,
            'cep' => $this->cep,
            'dataCadastro' => $this->dataCadastro->format('d/m/Y'),
            'declarante' => $this->declarante,
            'falecido' => $this->falecido,
            'contratante' => $this->contratante
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->endereco;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
