<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Emitente
 *
 * @ORM\Table(name="tb_emitente")
 * @ORM\Entity(repositoryClass="Agencia\Repository\EmitenteRepository")
 */
class Emitente extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_emitente", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_emitente", type="string", length=150, nullable=false)
     */
    private $emitente;
    function getId() {
        return $this->id;
    }

    function getEmitente() {
        return $this->emitente;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEmitente($emitente) {
        $this->emitente = $emitente;
    }
 /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'emitente'=> $this->bandeira
        
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->emitente;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }



}
