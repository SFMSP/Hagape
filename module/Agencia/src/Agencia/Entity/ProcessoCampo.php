<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * ProcessoCampo
 *
 * @ORM\Table(name="tb_processo_campo", indexes={@ORM\Index(name="fk_tb_processo_campo_tb_processo1_idx", columns={"id_processo"}), @ORM\Index(name="fk_tb_processo_campo_tb_campo1_idx", columns={"id_campo"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\ProcessoCampoRepository")
 */
class ProcessoCampo extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_processo_campo", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="campo_opcao", type="string", length=150, nullable=true)
     */
    private $campoOpcao;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_obrigatorio", type="boolean", nullable=true)
     */
    private $obrigatorio;

    /**
     * @var \Agencia\Entity\Processo
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Processo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_processo", referencedColumnName="id_processo")
     * })
     */
    private $processo;

    /**
     * @var \Agencia\Entity\Campo
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Campo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_campo", referencedColumnName="id_campo")
     * })
     */
    private $campo;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCampoOpcao()
    {
        return $this->campoOpcao;
    }

    /**
     * @param string $campoOpcao
     */
    public function setCampoOpcao($campoOpcao)
    {
        $this->campoOpcao = $campoOpcao;
    }

    /**
     * @return boolean
     */
    public function isObrigatorio()
    {
        return $this->obrigatorio;
    }

    /**
     * @param boolean $obrigatorio
     */
    public function setObrigatorio($obrigatorio)
    {
        $this->obrigatorio = $obrigatorio;
    }

    /**
     * @return Processo
     */
    public function getProcesso()
    {
        return $this->processo;
    }

    /**
     * @param Processo $processo
     */
    public function setProcesso($processo)
    {
        $this->processo = $processo;
    }

    /**
     * @return Campo
     */
    public function getCampo()
    {
        return $this->campo;
    }

    /**
     * @param Campo $campo
     */
    public function setCampo($campo)
    {
        $this->campo = $campo;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'campoOpcao' => $this->campoOpcao,
            'obrigatorio' => $this->obrigatorio,
            'processo' => $this->processo,
            'campo' => $this->campo
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->campoOpcao;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }



    /**
     * Get obrigatorio
     *
     * @return boolean 
     */
    public function getObrigatorio()
    {
        return $this->obrigatorio;
    }
}
