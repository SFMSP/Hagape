<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * TipoPagamento
 *
 * @ORM\Table(name="tb_tipo_pagamento")
 * @ORM\Entity(repositoryClass="Agencia\Repository\TipoPagamentoRepository")
 */
class TipoPagamento extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_tipo_pagamento", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_tipo_pagamento", type="string", length=150, nullable=false)
     */
    private $tipoPagamento;
    
    function getId() {
        return $this->id;
    }

    function getTipoPagamento() {
        return $this->tipoPagamento;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTipoPagamento($tipoPagamento) {
        $this->tipoPagamento = $tipoPagamento;
    }

    
    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'tipoPagamento'=> $this->tipoPagamento
        
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->tipoPagamento;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }

    
    
    
    


}
