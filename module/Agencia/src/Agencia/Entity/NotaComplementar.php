<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * NotaComplementar
 *
 * @ORM\Table(name="tb_nota_complementar", indexes={@ORM\Index(name="IDX_7C35E196FA198E41", columns={"id_contratacao"}), @ORM\Index(name="IDX_7C35E196DFD5DABC", columns={"id_servico"}), @ORM\Index(name="FK_tb_nota_complementar_tb_produto", columns={"id_produto"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\NotaComplementarRepository")
 */
class NotaComplementar extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_nota_complementar", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

   
     /**
     * @var \Estoque\Entity\Produto
     *
     * @ORM\ManyToOne(targetEntity="Estoque\Entity\Produto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_produto", referencedColumnName="id_produto")
     * })
     */
    private $produto;

    /**
     * @var \Agencia\Entity\Contratacao
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Contratacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contratacao", referencedColumnName="id_contratacao")
     * })
     */
    private $contratacao;

    /**
     * @var \Agencia\Entity\Servico
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Servico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_servico", referencedColumnName="id_servico")
     * })
     */
    private $servico;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_contratacao_complementar", type="datetime", nullable=true)
     */
    private $dtComplementar;
    
    
    function getDtComplementar() {
        return $this->dtComplementar;
    }

    function setDtComplementar(\DateTime $dtComplementar) {
        $this->dtComplementar = $dtComplementar;
    }

        /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    function getProduto() {
        return $this->produto;
    }

    function setProduto(\Estoque\Entity\Produto $produto) {
        $this->produto = $produto;
    }

    
    /**
     * @return Contratacao
     */
    public function getContratacao()
    {
        return $this->contratacao;
    }

    /**
     * @param Contratacao $contratacao
     */
    public function setContratacao($contratacao)
    {
        $this->contratacao = $contratacao;
    }

    /**
     * @return TbServico
     */
    public function getServico()
    {
        return $this->servico;
    }

    /**
     * @param TbServico $servico
     */
    public function setServico($servico)
    {
        $this->servico = $servico;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'produto' => $this->produto,
            'contratacao' => $this->contratacao,
            'servico' => $this->servico
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return (string)$this->produto;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
