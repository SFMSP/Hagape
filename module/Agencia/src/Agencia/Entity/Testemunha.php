<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Testemunha
 *
 * @ORM\Table(name="tb_testemunha", indexes={@ORM\Index(name="fk_tb_testemunha_tb_falecido1_idx", columns={"id_falecido"}), @ORM\Index(name="fk_tb_testemunha_tb_estado_civil1_idx", columns={"id_estado_civil"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\TestemunhaRepository")
 */
class Testemunha extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_testemunha", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome_testemunha", type="string", length=255, nullable=true)
     */
    private $nomeTestemunha;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_naturalidade", type="string", length=255, nullable=true)
     */
    private $naturalidade;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_profissao", type="string", length=255, nullable=true)
     */
    private $profissao;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_endereco", type="string", length=255, nullable=true)
     */
    private $endereco;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_bairro", type="string", length=255, nullable=true)
     */
    private $bairro;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=true)
     */
    private $dataCadastro;

    /**
     * @var \Agencia\Entity\Falecido
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Falecido")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_falecido", referencedColumnName="id_falecido")
     * })
     */
    private $falecido;

    /**
     * @var \Agencia\Entity\EstadoCivil
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\EstadoCivil")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estado_civil", referencedColumnName="id_estado_civil")
     * })
     */
    private $estadoCivil;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Agencia\Entity\Rogo", inversedBy="testemunha")
     * @ORM\JoinTable(name="tb_testemunha_tb_rogo",
     *   joinColumns={
     *     @ORM\JoinColumn(name="tb_testemunha_id_testemunha", referencedColumnName="id_testemunha")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="tb_rogo_id_rogo", referencedColumnName="id_rogo")
     *   }
     * )
     */
    private $rogo;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->rogo = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNomeTestemunha()
    {
        return $this->nomeTestemunha;
    }

    /**
     * @param string $nomeTestemunha
     */
    public function setNomeTestemunha($nomeTestemunha)
    {
        $this->nomeTestemunha = $nomeTestemunha;
    }

    /**
     * @return string
     */
    public function getNaturalidade()
    {
        return $this->naturalidade;
    }

    /**
     * @param string $naturalidade
     */
    public function setNaturalidade($naturalidade)
    {
        $this->naturalidade = $naturalidade;
    }

    /**
     * @return string
     */
    public function getProfissao()
    {
        return $this->profissao;
    }

    /**
     * @param string $profissao
     */
    public function setProfissao($profissao)
    {
        $this->profissao = $profissao;
    }

    /**
     * @return string
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param string $endereco
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    }

    /**
     * @return string
     */
    public function getBairro()
    {
        return $this->bairro;
    }

    /**
     * @param string $bairro
     */
    public function setBairro($bairro)
    {
        $this->bairro = $bairro;
    }

    /**
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param \DateTime $dataCadastro
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
    }

    /**
     * @return Falecido
     */
    public function getFalecido()
    {
        return $this->falecido;
    }

    /**
     * @param Falecido $falecido
     */
    public function setFalecido($falecido)
    {
        $this->falecido = $falecido;
    }

    /**
     * @return EstadoCivil
     */
    public function getEstadoCivil()
    {
        return $this->estadoCivil;
    }

    /**
     * @param EstadoCivil $estadoCivil
     */
    public function setEstadoCivil($estadoCivil)
    {
        $this->estadoCivil = $estadoCivil;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRogo()
    {
        return $this->rogo;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $rogo
     */
    public function setRogo($rogo)
    {
        $this->rogo = $rogo;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'nomeTestemunha' => $this->nomeTestemunha,
            'naturalidade' => $this->naturalidade,
            'profissao' => $this->profissao,
            'endereco' => $this->endereco,
            'bairro' => $this->bairro,
            'dataCadastro' => $this->dataCadastro->format('d/m/Y'),
            'falecido' => $this->falecido,
            'estadoCivil' => $this->estadoCivil,
            'rogo' => $this->rogo
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->nomeTestemunha;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


    /**
     * Add rogo
     *
     * @param \Agencia\Entity\Rogo $rogo
     * @return Testemunha
     */
    public function addRogo(\Agencia\Entity\Rogo $rogo)
    {
        $this->rogo[] = $rogo;

        return $this;
    }

    /**
     * Remove rogo
     *
     * @param \Agencia\Entity\Rogo $rogo
     */
    public function removeRogo(\Agencia\Entity\Rogo $rogo)
    {
        $this->rogo->removeElement($rogo);
    }
}
