<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * DadosAmputacao
 *
 * @ORM\Table(name="tb_dados_amputacao", indexes={@ORM\Index(name="fk_tb_dados_amaputacao_tb_paciente1_idx", columns={"id_paciente"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\DadosAmputacaoRepository")
 */
class DadosAmputacao extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_dados_amaputacao", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_causa_amputacao", type="string", length=255, nullable=true)
     */
    private $causaAmputacao;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_local_amputacao", type="string", length=255, nullable=true)
     */
    private $localAmputacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=true)
     */
    private $dataCadastro;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_municipio", type="integer", nullable=true)
     */
    private $municipio;

    /**
     * @var \Agencia\Entity\Paciente
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Paciente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_paciente", referencedColumnName="id_paciente")
     * })
     */
    private $paciente;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Agencia\Entity\Medico", inversedBy="idDadosAmaputacao")
     * @ORM\JoinTable(name="tb_dados_amputacao_tb_medico",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_dados_amaputacao", referencedColumnName="id_dados_amaputacao")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_medico", referencedColumnName="id_medico")
     *   }
     * )
     */
    private $medico;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->medico = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCausaAmputacao()
    {
        return $this->causaAmputacao;
    }

    /**
     * @param string $causaAmputacao
     */
    public function setCausaAmputacao($causaAmputacao)
    {
        $this->causaAmputacao = $causaAmputacao;
    }

    /**
     * @return string
     */
    public function getLocalAmputacao()
    {
        return $this->localAmputacao;
    }

    /**
     * @param string $localAmputacao
     */
    public function setLocalAmputacao($localAmputacao)
    {
        $this->localAmputacao = $localAmputacao;
    }

    /**
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param \DateTime $dataCadastro
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
    }

    /**
     * @return int
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }

    /**
     * @param int $municipio
     */
    public function setMunicipio($municipio)
    {
        $this->municipio = $municipio;
    }

    /**
     * @return Paciente
     */
    public function getPaciente()
    {
        return $this->paciente;
    }

    /**
     * @param Paciente $paciente
     */
    public function setPaciente($paciente)
    {
        $this->paciente = $paciente;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMedico()
    {
        return $this->medico;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $medico
     */
    public function setMedico($medico)
    {
        $this->medico = $medico;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'causaAmputacao' => $this->causaAmputacao,
            'localAmputacao' => $this->localAmputacao,
            'dataCadastro' => $this->dataCadastro->format('d/m/y'),
            'municipio' => $this->municipio,
            'paciente' => $this->paciente,
            'medico' => $this->medico
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->causaAmputacao;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


    /**
     * Add medico
     *
     * @param \Agencia\Entity\Medico $medico
     * @return DadosAmputacao
     */
    public function addMedico(\Agencia\Entity\Medico $medico)
    {
        $this->medico[] = $medico;

        return $this;
    }

    /**
     * Remove medico
     *
     * @param \Agencia\Entity\Medico $medico
     */
    public function removeMedico(\Agencia\Entity\Medico $medico)
    {
        $this->medico->removeElement($medico);
    }
}
