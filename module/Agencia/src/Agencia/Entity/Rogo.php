<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Rogo
 *
 * @ORM\Table(name="tb_rogo")
 * @ORM\Entity(repositoryClass="Agencia\Repository\RogoRepository")
 */
class Rogo extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_rogo", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_numero_declaracao", type="string", length=150, nullable=true)
     */
    private $numeroDeclaracao;

    /**
     * @var string
     *
     * @ORM\Column(name="tb_rogocol", type="string", length=45, nullable=true)
     */
    private $rogocol;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Agencia\Entity\Testemunha", mappedBy="rogo")
     */
    private $testemunha;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNumeroDeclaracao()
    {
        return $this->numeroDeclaracao;
    }

    /**
     * @param string $numeroDeclaracao
     */
    public function setNumeroDeclaracao($numeroDeclaracao)
    {
        $this->numeroDeclaracao = $numeroDeclaracao;
    }

    /**
     * @return string
     */
    public function getRogocol()
    {
        return $this->rogocol;
    }

    /**
     * @param string $rogocol
     */
    public function setRogocol($rogocol)
    {
        $this->rogocol = $rogocol;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTestemunha()
    {
        return $this->testemunha;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $testemunha
     */
    public function setTestemunha($testemunha)
    {
        $this->testemunha = $testemunha;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'numeroDeclaracao' => $this->numeroDeclaracao,
            'rogocol' => $this->rogocol,
            'testemunha' => $this->testemunha
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->numeroDeclaracao;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->testemunha = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add testemunha
     *
     * @param \Agencia\Entity\Testemunha $testemunha
     * @return Rogo
     */
    public function addTestemunha(\Agencia\Entity\Testemunha $testemunha)
    {
        $this->testemunha[] = $testemunha;

        return $this;
    }

    /**
     * Remove testemunha
     *
     * @param \Agencia\Entity\Testemunha $testemunha
     */
    public function removeTestemunha(\Agencia\Entity\Testemunha $testemunha)
    {
        $this->testemunha->removeElement($testemunha);
    }
}
