<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * ServicoVendido
 *
 * @ORM\Table(name="tb_servico_vendido", indexes={@ORM\Index(name="fk_tb_servico_vendido_tb_classificacao_venda1_idx", columns={"id_classificacao_venda"}), @ORM\Index(name="fk_tb_servico_vendido_tb_venda_produto1_idx", columns={"id_venda_produto"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\ServicoVendidoRepository")
 */
class ServicoVendido extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_servico_vendido", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_conjunto", type="string", length=150, nullable=true)
     */
    private $valorConjunto;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantidade", type="integer", nullable=true)
     */
    private $quantidade;

     /**
     * @var \Estoque\Entity\CategoriaProduto
     *
     * @ORM\ManyToOne(targetEntity="Estoque\Entity\CategoriaProduto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_categoria", referencedColumnName="id_categoria")
     * })
     */
    private $categoria;

    /**
     * @var \Agencia\Entity\Servico
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Servico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_servico", referencedColumnName="id_servico")
     * })
     */
    private $servico;

    /**
     * @var boolean
     *
     * @ORM\Column(name="outros", type="boolean", nullable=true)
     */
    private $outros;

    /**
     * @var boolean
     *
     * @ORM\Column(name="debitar", type="boolean", nullable=true)
     */
    private $debitar;

    /**
     * @var boolean
     *
     * @ORM\Column(name="creditar", type="boolean", nullable=true)
     */
    private $creditar;

    /**
     * @var \Agencia\Entity\ClassificacaoVenda
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\ClassificacaoVenda")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_classificacao_venda", referencedColumnName="id_classificacao_venda")
     * })
     */
    private $classificacaoVenda;

    /**
     * @var \Agencia\Entity\VendaProdutoServico
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\VendaProdutoServico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_venda_produto", referencedColumnName="id_venda_produto")
     * })
     */
    private $vendaProduto;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getValorConjunto()
    {
        return $this->valorConjunto;
    }

    /**
     * @param string $valorConjunto
     */
    public function setValorConjunto($valorConjunto)
    {
        $this->valorConjunto = $valorConjunto;
    }

    /**
     * @return int
     */
    public function getQuantidade()
    {
        return $this->quantidade;
    }

    /**
     * @param int $quantidade
     */
    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;
    }

    /**
     * @return int
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param int $categoria
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }

    /**
     * @return int
     */
    public function getServico()
    {
        return $this->servico;
    }

    /**
     * @param int $servico
     */
    public function setServico($servico)
    {
        $this->servico = $servico;
    }

    /**
     * @return bool
     */
    public function isOutros()
    {
        return $this->outros;
    }

    /**
     * @param bool $outros
     */
    public function setOutros($outros)
    {
        $this->outros = $outros;
    }

    /**
     * @return bool
     */
    public function isDebitar()
    {
        return $this->debitar;
    }

    /**
     * @param bool $debitar
     */
    public function setDebitar($debitar)
    {
        $this->debitar = $debitar;
    }

    /**
     * @return bool
     */
    public function isCreditar()
    {
        return $this->creditar;
    }

    /**
     * @param bool $creditar
     */
    public function setCreditar($creditar)
    {
        $this->creditar = $creditar;
    }

    /**
     * @return ClassificacaoVenda
     */
    public function getClassificacaoVenda()
    {
        return $this->classificacaoVenda;
    }

    /**
     * @param ClassificacaoVenda $classificacaoVenda
     */
    public function setClassificacaoVenda($classificacaoVenda)
    {
        $this->classificacaoVenda = $classificacaoVenda;
    }

    /**
     * @return VendaProdutoServico
     */
    public function getVendaProduto()
    {
        return $this->vendaProduto;
    }

    /**
     * @param VendaProdutoServico $vendaProduto
     */
    public function setVendaProduto($vendaProduto)
    {
        $this->vendaProduto = $vendaProduto;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'valorConjunto' => $this->valorConjunto,
            'quantidade' => $this->quantidade,
            'categoria' => $this->categoria,
            'servico' => $this->servico,
            'outros' => $this->outros,
            'debitar' => $this->debitar,
            'creditar' => $this->creditar,
            'classificacaoVenda' => $this->classificacaoVenda,
            'vendaProduto' => $this->vendaProduto,
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return (string)$this->valorConjunto;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
