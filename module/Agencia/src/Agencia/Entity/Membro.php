<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;
/**
 * Membro
 *
 * @ORM\Table(name="tb_membro")
 * @ORM\Entity(repositoryClass="Agencia\Repository\MembroRepository")
 */
class Membro
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_membro", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_membro", type="string", length=50, nullable=true)
     */
    private $membro;
    function getId() {
        return $this->id;
    }

    function getMembro() {
        return $this->membro;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setMembro($membro) {
        $this->membro = $membro;
    }

 public function toArray()
    {
        return[
            'id' => $this->id,
            'membro' => $this->membro
          
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->membro;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
