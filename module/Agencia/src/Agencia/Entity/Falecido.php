<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Falecido
 *
 * @ORM\Table(name="tb_falecido", indexes={@ORM\Index(name="fk_tb_falecido_tb_venda1_idx", columns={"id_contratacao"}), @ORM\Index(name="fk_tb_falecido_tb_contratante1_idx", columns={"id_contratante"}), @ORM\Index(name="fk_tb_falecido_tb_declarante1_idx", columns={"id_declarante"}), @ORM\Index(name="fk_tb_falecido_tb_estado_civil1_idx", columns={"id_estado_civil_pai"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\FalecidoRepository")
 */
class Falecido extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_falecido", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_nat_morto", type="boolean", nullable=true)
     */
    private $natMorto;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome", type="string", length=255, nullable=true)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_naturalidade", type="string", length=255, nullable=true)
     */
    private $naturalidade;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_sexo", type="string", length=1, nullable=true)
     */
    private $sexo;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_profissao", type="string", length=255, nullable=true)
     */
    private $profissao;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_cor", type="integer", nullable=true)
     */
    private $cor;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_rg", type="string", length=15, nullable=true)
     */
    private $rg;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_cpf", type="string", length=15, nullable=true)
     */
    private $cpf;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_nascimento", type="date", nullable=true)
     */
    private $dataNascimento;

    /**
     * @var integer
     *
     * @ORM\Column(name="idade", type="integer", nullable=true)
     */
    private $idade;

    /**
     * @var string
     *
     * @ORM\Column(name="estado_civil", type="string", length=1, nullable=true)
     */
    private $estadoCivil;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_marca_passo", type="boolean", nullable=true)
     */
    private $marcaPasso;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_reservista", type="boolean", nullable=true)
     */
    private $reservista;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_eleitor", type="boolean", nullable=true)
     */
    private $eleitor;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_inss", type="boolean", nullable=true)
     */
    private $inss;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_numero_beneficio", type="string", length=150, nullable=true)
     */
    private $numeroBeneficio;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_bens_inventario", type="boolean", nullable=true)
     */
    private $bensInventario;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_deixa_testamento", type="boolean", nullable=true)
     */
    private $deixaTestamento;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome_pai", type="string", length=255, nullable=true)
     */
    private $nomePai;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome_mae", type="string", length=255, nullable=true)
     */
    private $nomeMae;

    /**
     * @var integer
     *
     * @ORM\Column(name="idade_pai", type="integer", nullable=true)
     */
    private $idadePai;

    /**
     * @var integer
     *
     * @ORM\Column(name="idade_mae", type="integer", nullable=true)
     */
    private $idadeMae;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_naturalidade_pai", type="string", length=150, nullable=true)
     */
    private $naturalidadePai;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_naturalidade_mae", type="string", length=150, nullable=true)
     */
    private $naturalidadeMae;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_profissao_mae", type="string", length=150, nullable=true)
     */
    private $profissaoMae;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_profissao_pai", type="string", length=150, nullable=true)
     */
    private $profissaoPai;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_deixa_filho", type="boolean", nullable=true)
     */
    private $deixaFilho;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=true)
     */
    private $dataCadastro;

    /**
     * @var \Agencia\Entity\EstadoCivil
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\EstadoCivil")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estado_civil_mae", referencedColumnName="id_estado_civil")
     * })
     */
    private $estadoCivilMae;

    /**
     * @var \Agencia\Entity\Contratacao
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Contratacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contratacao", referencedColumnName="id_contratacao")
     * })
     */
    private $contratacao;

    /**
     * @var \Agencia\Entity\Contratante
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Contratante")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contratante", referencedColumnName="id_contratante")
     * })
     */
    private $contratante;

    /**
     * @var \Agencia\Entity\Declarante
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Declarante")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_declarante", referencedColumnName="id_declarante")
     * })
     */
    private $declarante;

    /**
     * @var \Agencia\Entity\EstadoCivil
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\EstadoCivil")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estado_civil_pai", referencedColumnName="id_estado_civil")
     * })
     */
    private $estadoCivilPai;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return boolean
     */
    public function isNatMorto()
    {
        return $this->natMorto;
    }

    /**
     * @param boolean $natMorto
     */
    public function setNatMorto($natMorto)
    {
        $this->natMorto = $natMorto;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return string
     */
    public function getNaturalidade()
    {
        return $this->naturalidade;
    }

    /**
     * @param string $naturalidade
     */
    public function setNaturalidade($naturalidade)
    {
        $this->naturalidade = $naturalidade;
    }

    /**
     * @return string
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * @param string $sexo
     */
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;
    }

    /**
     * @return string
     */
    public function getProfissao()
    {
        return $this->profissao;
    }

    /**
     * @param string $profissao
     */
    public function setProfissao($profissao)
    {
        $this->profissao = $profissao;
    }

    /**
     * @return int
     */
    public function getCor()
    {
        return $this->cor;
    }

    /**
     * @param int $cor
     */
    public function setCor($cor)
    {
        $this->cor = $cor;
    }

    /**
     * @return string
     */
    public function getRg()
    {
        return $this->rg;
    }

    /**
     * @param string $rg
     */
    public function setRg($rg)
    {
        $this->rg = $rg;
    }

    /**
     * @return string
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * @param string $cpf
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
    }

    /**
     * @return \DateTime
     */
    public function getDataNascimento()
    {
        return $this->dataNascimento;
    }

    /**
     * @param \DateTime $dataNascimento
     */
    public function setDataNascimento($dataNascimento)
    {
        $this->dataNascimento = $dataNascimento;
    }

    /**
     * @return int
     */
    public function getIdade()
    {
        return $this->idade;
    }

    /**
     * @param int $idade
     */
    public function setIdade($idade)
    {
        $this->idade = $idade;
    }

    /**
     * @return string
     */
    public function getEstadoCivil()
    {
        return $this->estadoCivil;
    }

    /**
     * @param string $estadoCivil
     */
    public function setEstadoCivil($estadoCivil)
    {
        $this->estadoCivil = $estadoCivil;
    }

    /**
     * @return boolean
     */
    public function isMarcaPasso()
    {
        return $this->marcaPasso;
    }

    /**
     * @param boolean $marcaPasso
     */
    public function setMarcaPasso($marcaPasso)
    {
        $this->marcaPasso = $marcaPasso;
    }

    /**
     * @return boolean
     */
    public function isReservista()
    {
        return $this->reservista;
    }

    /**
     * @param boolean $reservista
     */
    public function setReservista($reservista)
    {
        $this->reservista = $reservista;
    }

    /**
     * @return boolean
     */
    public function isEleitor()
    {
        return $this->eleitor;
    }

    /**
     * @param boolean $eleitor
     */
    public function setEleitor($eleitor)
    {
        $this->eleitor = $eleitor;
    }

    /**
     * @return boolean
     */
    public function isInss()
    {
        return $this->inss;
    }

    /**
     * @param boolean $inss
     */
    public function setInss($inss)
    {
        $this->inss = $inss;
    }

    /**
     * @return string
     */
    public function getNumeroBeneficio()
    {
        return $this->numeroBeneficio;
    }

    /**
     * @param string $numeroBeneficio
     */
    public function setNumeroBeneficio($numeroBeneficio)
    {
        $this->numeroBeneficio = $numeroBeneficio;
    }

    /**
     * @return boolean
     */
    public function isBensInventario()
    {
        return $this->bensInventario;
    }

    /**
     * @param boolean $bensInventario
     */
    public function setBensInventario($bensInventario)
    {
        $this->bensInventario = $bensInventario;
    }

    /**
     * @return boolean
     */
    public function isDeixaTestamento()
    {
        return $this->deixaTestamento;
    }

    /**
     * @param boolean $deixaTestamento
     */
    public function setDeixaTestamento($deixaTestamento)
    {
        $this->deixaTestamento = $deixaTestamento;
    }

    /**
     * @return string
     */
    public function getNomePai()
    {
        return $this->nomePai;
    }

    /**
     * @param string $nomePai
     */
    public function setNomePai($nomePai)
    {
        $this->nomePai = $nomePai;
    }

    /**
     * @return string
     */
    public function getNomeMae()
    {
        return $this->nomeMae;
    }

    /**
     * @param string $nomeMae
     */
    public function setNomeMae($nomeMae)
    {
        $this->nomeMae = $nomeMae;
    }

    /**
     * @return int
     */
    public function getIdadePai()
    {
        return $this->idadePai;
    }

    /**
     * @param int $idadePai
     */
    public function setIdadePai($idadePai)
    {
        $this->idadePai = $idadePai;
    }

    /**
     * @return int
     */
    public function getIdadeMae()
    {
        return $this->idadeMae;
    }

    /**
     * @param int $idadeMae
     */
    public function setIdadeMae($idadeMae)
    {
        $this->idadeMae = $idadeMae;
    }

    /**
     * @return string
     */
    public function getNaturalidadePai()
    {
        return $this->naturalidadePai;
    }

    /**
     * @param string $naturalidadePai
     */
    public function setNaturalidadePai($naturalidadePai)
    {
        $this->naturalidadePai = $naturalidadePai;
    }

    /**
     * @return string
     */
    public function getNaturalidadeMae()
    {
        return $this->naturalidadeMae;
    }

    /**
     * @param string $naturalidadeMae
     */
    public function setNaturalidadeMae($naturalidadeMae)
    {
        $this->naturalidadeMae = $naturalidadeMae;
    }

    /**
     * @return string
     */
    public function getProfissaoMae()
    {
        return $this->profissaoMae;
    }

    /**
     * @param string $profissaoMae
     */
    public function setProfissaoMae($profissaoMae)
    {
        $this->profissaoMae = $profissaoMae;
    }

    /**
     * @return string
     */
    public function getProfissaoPai()
    {
        return $this->profissaoPai;
    }

    /**
     * @param string $profissaoPai
     */
    public function setProfissaoPai($profissaoPai)
    {
        $this->profissaoPai = $profissaoPai;
    }

    /**
     * @return boolean
     */
    public function isDeixaFilho()
    {
        return $this->deixaFilho;
    }

    /**
     * @param boolean $deixaFilho
     */
    public function setDeixaFilho($deixaFilho)
    {
        $this->deixaFilho = $deixaFilho;
    }

    /**
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param \DateTime $dataCadastro
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
    }

    /**
     * @return EstadoCivil
     */
    public function getEstadoCivilMae()
    {
        return $this->estadoCivilMae;
    }

    /**
     * @param EstadoCivil $estadoCivilMae
     */
    public function setEstadoCivilMae($estadoCivilMae)
    {
        $this->estadoCivilMae = $estadoCivilMae;
    }
    /**
     * @return Contratacao
     */
    public function getContratacao()
    {
        return $this->contratacao;
    }

    /**
     * @param Contratacao $contratacao
     */
    public function setContratacao($contratacao)
    {
        $this->contratacao = $contratacao;
    }

    /**
     * @return Contratante
     */
    public function getContratante()
    {
        return $this->contratante;
    }

    /**
     * @param Contratante $contratante
     */
    public function setContratante($contratante)
    {
        $this->contratante = $contratante;
    }

    /**
     * @return Declarante
     */
    public function getDeclarante()
    {
        return $this->declarante;
    }

    /**
     * @param Declarante $declarante
     */
    public function setDeclarante($declarante)
    {
        $this->declarante = $declarante;
    }

    /**
     * @return EstadoCivil
     */
    public function getEstadoCivilPai()
    {
        return $this->estadoCivilPai;
    }

    /**
     * @param EstadoCivil $estadoCivilPai
     */
    public function setEstadoCivilPai($estadoCivilPai)
    {
        $this->estadoCivilPai = $estadoCivilPai;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'natMorto' => $this->natMorto,
            'nome' => $this->nome,
            'naturalidade' => $this->naturalidade,
            'sexo' => $this->sexo,
            'profissao' => $this->profissao,
            'cor' => $this->cor,
            'rg' => $this->rg,
            'cpf' => $this->cpf,
            'dataNascimento' => $this->dataNascimento->format('d/m/Y'),
            'idade' => $this->idade,
            'estadoCivil' => $this->estadoCivil,
            'marcaPasso' => $this->marcaPasso,
            'reservista' => $this->reservista,
            'eleitor' => $this->eleitor,
            'inss' => $this->inss,
            'numeroBeneficio' => $this->numeroBeneficio,
            'bensInventario' => $this->bensInventario,
            'deixaTestamento' => $this->deixaTestamento,
            'nomePai' => $this->nomePai,
            'nomeMae' => $this->nomeMae,
            'idadePai' => $this->idadePai,
            'idadeMae' => $this->idadeMae,
            'naturalidadePai' => $this->naturalidadePai,
            'naturalidadeMae' => $this->naturalidadeMae,
            'profissaoMae' => $this->profissaoMae,
            'profissaoPai' => $this->profissaoPai,
            'deixaFilho' => $this->deixaFilho,
            'dataCadastro' => $this->dataCadastro->format('d/m/Y'),
            'estadoCivilMae' => $this->estadoCivilMae,
            'contratacao' => $this->contratacao,
            'contratante' => $this->contratante,
            'declarante' => $this->declarante,
            'estadoCivilPai' => $this->estadoCivilPai
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->nome;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }



    /**
     * Get natMorto
     *
     * @return boolean 
     */
    public function getNatMorto()
    {
        return $this->natMorto;
    }

    /**
     * Get marcaPasso
     *
     * @return boolean 
     */
    public function getMarcaPasso()
    {
        return $this->marcaPasso;
    }

    /**
     * Get reservista
     *
     * @return boolean 
     */
    public function getReservista()
    {
        return $this->reservista;
    }

    /**
     * Get eleitor
     *
     * @return boolean 
     */
    public function getEleitor()
    {
        return $this->eleitor;
    }

    /**
     * Get inss
     *
     * @return boolean 
     */
    public function getInss()
    {
        return $this->inss;
    }

    /**
     * Get bensInventario
     *
     * @return boolean 
     */
    public function getBensInventario()
    {
        return $this->bensInventario;
    }

    /**
     * Get deixaTestamento
     *
     * @return boolean 
     */
    public function getDeixaTestamento()
    {
        return $this->deixaTestamento;
    }

    /**
     * Get deixaFilho
     *
     * @return boolean 
     */
    public function getDeixaFilho()
    {
        return $this->deixaFilho;
    }
}
