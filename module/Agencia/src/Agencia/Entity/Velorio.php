<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Velorio
 *
 * @ORM\Table(name="tb_velorio", indexes={@ORM\Index(name="fk_tb_velorio_tb_dados_obito_velorio1_idx", columns={"id_dados_obito_velorio"}), @ORM\Index(name="fk_tb_velorio_tb_venda1_idx", columns={"id_contratacao"}), @ORM\Index(name="fk_tb_velorio_tb_dados_obito1_idx", columns={"id_dados_obito"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\VelorioRepository")
 */
class Velorio extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_velorio", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_endereco", type="string", length=255, nullable=true)
     */
    private $endereco;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_local_velorio", type="string", length=255, nullable=true)
     */
    private $localVelorio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_saida", type="date", nullable=true)
     */
    private $dataSaida;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_hora_saida", type="string", length=45, nullable=true)
     */
    private $horaSaida;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=true)
     */
    private $dataCadastro;

    /**
     * @var \Agencia\Entity\DadosObitoVelorio
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\DadosObitoVelorio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_dados_obito_velorio", referencedColumnName="id_dados_obito_velorio")
     * })
     */
    private $dadosObitoVelorio;

    /**
     * @var \Agencia\Entity\Contratacao
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Contratacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contratacao", referencedColumnName="id_contratacao")
     * })
     */
    private $contratacao;

    /**
     * @var \Agencia\Entity\DadosObito
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\DadosObito")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_dados_obito", referencedColumnName="id_dados_obito")
     * })
     */
    private $dadosObito;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param string $endereco
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    }

    /**
     * @return string
     */
    public function getLocalVelorio()
    {
        return $this->localVelorio;
    }

    /**
     * @param string $localVelorio
     */
    public function setLocalVelorio($localVelorio)
    {
        $this->localVelorio = $localVelorio;
    }

    /**
     * @return \DateTime
     */
    public function getDataSaida()
    {
        return $this->dataSaida;
    }

    /**
     * @param \DateTime $dataSaida
     */
    public function setDataSaida($dataSaida)
    {
        $this->dataSaida = $dataSaida;
    }

    /**
     * @return string
     */
    public function getHoraSaida()
    {
        return $this->horaSaida;
    }

    /**
     * @param string $horaSaida
     */
    public function setHoraSaida($horaSaida)
    {
        $this->horaSaida = $horaSaida;
    }

    /**
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param \DateTime $dataCadastro
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
    }

    /**
     * @return DadosObitoVelorio
     */
    public function getDadosObitoVelorio()
    {
        return $this->dadosObitoVelorio;
    }

    /**
     * @param DadosObitoVelorio $dadosObitoVelorio
     */
    public function setDadosObitoVelorio($dadosObitoVelorio)
    {
        $this->dadosObitoVelorio = $dadosObitoVelorio;
    }

    /**
     * @return Contratacao
     */
    public function getContratacao()
    {
        return $this->contratacao;
    }

    /**
     * @param Contratacao $contratacao
     */
    public function setContratacao($contratacao)
    {
        $this->contratacao = $contratacao;
    }

    /**
     * @return DadosObito
     */
    public function getDadosObito()
    {
        return $this->dadosObito;
    }

    /**
     * @param DadosObito $dadosObito
     */
    public function setDadosObito($dadosObito)
    {
        $this->dadosObito = $dadosObito;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'endereco' => $this->endereco,
            'localVelorio' => $this->localVelorio,
            'dataSaida' => $this->dataSaida->format('d/m/Y'),
            'horaSaida' => $this->horaSaida,
            'dataCadastro' => $this->dataCadastro->format('d/m/Y'),
            'dadosObitoVelorio' => $this->dadosObitoVelorio,
            'contratacao' => $this->contratacao,
            'dadosObito' => $this->dadosObito
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->localVelorio;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
