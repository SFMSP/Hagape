<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * ServicoVendido
 *
 * @ORM\Table(name="tb_servico_vendido", indexes={@ORM\Index(name="fk_tb_servico_vendido_tb_classificacao_venda1_idx", columns={"id_classificacao_venda"}), @ORM\Index(name="fk_tb_servico_vendido_tb_contratacao1_idx", columns={"id_contratacao"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\ServicoVendidoRepository")
 */
class ServicoVendido extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_servico_vendido", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_conjunto", type="string", length=150, nullable=true)
     */
    private $valorConjunto;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantidade", type="integer", nullable=true)
     */
    private $quantidade;

    /**
     * @var \Agencia\Entity\ClassificacaoVenda
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\ClassificacaoVenda")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_classificacao_venda", referencedColumnName="id_classificacao_venda")
     * })
     */
    private $classificacaoVenda;

    /**
     * @var \Agencia\Entity\Contratacao
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Contratacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contratacao", referencedColumnName="id_contratacao")
     * })
     */
    private $contratacao;








    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getValorConjunto()
    {
        return $this->valorConjunto;
    }

    /**
     * @param string $valorConjunto
     */
    public function setValorConjunto($valorConjunto)
    {
        $this->valorConjunto = $valorConjunto;
    }

    /**
     * @return int
     */
    public function getQuantidade()
    {
        return $this->quantidade;
    }

    /**
     * @param int $quantidade
     */
    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;
    }

    /**
     * @return ClassificacaoVenda
     */
    public function getClassificacaoVenda()
    {
        return $this->classificacaoVenda;
    }

    /**
     * @param ClassificacaoVenda $classificacaoVenda
     */
    public function setClassificacaoVenda($classificacaoVenda)
    {
        $this->classificacaoVenda = $classificacaoVenda;
    }

    /**
     * @return Contratacao
     */
    public function getContratacao()
    {
        return $this->contratacao;
    }

    /**
     * @param Contratacao $contratacao
     */
    public function setContratacao($contratacao)
    {
        $this->contratacao = $contratacao;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'valorConjunto' => $this->valorConjunto,
            'quantidade' => $this->quantidade,
            'classificacaoVenda' => $this->classificacaoVenda,
            'contratacao' => $this->contratacao
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return (string)$this->id;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }
}
