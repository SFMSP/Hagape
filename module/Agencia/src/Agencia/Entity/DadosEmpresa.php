<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;
/**
 * DadosEmpresa
 *
 * @ORM\Table(name="tb_dados_empresa", indexes={@ORM\Index(name="fk_tb_dados_empresa_tb_contratante1_idx", columns={"id_contratante"}), @ORM\Index(name="fk_tb_dados_empresa_tb_venda1_idx", columns={"id_contratacao"}), @ORM\Index(name="FK_tb_dados_empresa_tb_declarante", columns={"id_declarante"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\DadosEmpresaRepository")
 */
class DadosEmpresa extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_dados_empresa", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome_empresarial", type="string", length=255, nullable=true)
     */
    private $nomeEmpresarial;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_cnpj", type="string", length=20, nullable=true)
     */
    private $cnpj;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=true)
     */
    private $dataCadastro;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_endereco", type="string", length=250, nullable=true)
     */
    private $endereco;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_telefone", type="string", length=15, nullable=true)
     */
    private $telefone;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_representante", type="string", length=150, nullable=true)
     */
    private $representante;

    /**
     * @var \Agencia\Entity\Contratante
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Contratante")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contratante", referencedColumnName="id_contratante")
     * })
     */
    private $contratante;

    /**
     * @var \Agencia\Entity\Contratacao
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Contratacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contratacao", referencedColumnName="id_contratacao")
     * })
     */
    private $contratacao;
    
    /**
     * @var \Agencia\Entity\Declarante
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Declarante")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_declarante", referencedColumnName="id_declarante")
     * })
     */
    private $declarante;
    
    function getDeclarante(){
        return $this->declarante;
    }

    function setDeclarante(\Agencia\Entity\Declarante $declarante) {
        $this->declarante = $declarante;
    }

        /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNomeEmpresarial()
    {
        return $this->nomeEmpresarial;
    }

    /**
     * @param string $nomeEmpresarial
     */
    public function setNomeEmpresarial($nomeEmpresarial)
    {
        $this->nomeEmpresarial = $nomeEmpresarial;
    }

    /**
     * @return string
     */
    public function getCnpj()
    {
        return $this->cnpj;
    }

    /**
     * @param string $cnpj
     */
    public function setCnpj($cnpj)
    {
        $this->cnpj = $cnpj;
    }

    /**
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param \DateTime $dataCadastro
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
    }

    /**
     * @return string
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param string $endereco
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    }

    /**
     * @return string
     */
    public function getTelefone()
    {
        return $this->telefone;
    }

    /**
     * @param string $telefone
     */
    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;
    }

    /**
     * @return string
     */
    public function getRepresentante()
    {
        return $this->representante;
    }

    /**
     * @param string $representante
     */
    public function setRepresentante($representante)
    {
        $this->representante = $representante;
    }

    /**
     * @return Contratante
     */
    public function getContratante()
    {
        return $this->contratante;
    }

    /**
     * @param Contratante $contratante
     */
    public function setContratante($contratante)
    {
        $this->contratante = $contratante;
    }

    /**
     * @return Contratacao
     */
    public function getContratacao()
    {
        return $this->contratacao;
    }

    /**
     * @param Contratacao $contratacao
     */
    public function setContratacao($contratacao)
    {
        $this->contratacao = $contratacao;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'nomeEmpresarial' => $this->nomeEmpresarial,
            'cnpj' => $this->cnpj,
            'dataCadastro' => $this->dataCadastro,
            'endereco' => $this->endereco,
            'telefone' => $this->telefone,
            'representante' => $this->representante,
            'contratante' => $this->contratante,
            'contratacao' => $this->contratacao,
            'declarante'=>$this->declarante
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->nomeEmpresarial;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
