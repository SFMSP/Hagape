<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;
/**
 * Casamento
 *
 * @ORM\Table(name="tb_casamento", indexes={@ORM\Index(name="fk_tb_casamento_tb_falecido1_idx", columns={"id_falecido"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\CasamentoRepository")
 */
class Casamento extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_casamento", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=true)
     */
    private $dataCadastro;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome_conjuge", type="string", length=255, nullable=true)
     */
    private $nomeConjuge;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_casamento", type="date", nullable=true)
     */
    private $dataCasamento;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_cartorio", type="string", length=255, nullable=true)
     */
    private $cartorio;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_cidade", type="integer", nullable=true)
     */
    private $cidade;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_livro", type="string", length=150, nullable=true)
     */
    private $livro;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_folha", type="string", length=150, nullable=true)
     */
    private $folha;

    /**
     * @var integer
     *
     * @ORM\Column(name="numero", type="integer", nullable=true)
     */
    private $numero;

    /**
     * @var \Agencia\Entity\Falecido
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Falecido")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_falecido", referencedColumnName="id_falecido")
     * })
     */
    private $falecido;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param \DateTime $dataCadastro
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
    }

    /**
     * @return string
     */
    public function getNomeConjuge()
    {
        return $this->nomeConjuge;
    }

    /**
     * @param string $nomeConjuge
     */
    public function setNomeConjuge($nomeConjuge)
    {
        $this->nomeConjuge = $nomeConjuge;
    }

    /**
     * @return \DateTime
     */
    public function getDataCasamento()
    {
        return $this->dataCasamento;
    }

    /**
     * @param \DateTime $dataCasamento
     */
    public function setDataCasamento($dataCasamento)
    {
        $this->dataCasamento = $dataCasamento;
    }

    /**
     * @return string
     */
    public function getCartorio()
    {
        return $this->cartorio;
    }

    /**
     * @param string $cartorio
     */
    public function setCartorio($cartorio)
    {
        $this->cartorio = $cartorio;
    }

    /**
     * @return int
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * @param int $cidade
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;
    }

    /**
     * @return string
     */
    public function getLivro()
    {
        return $this->livro;
    }

    /**
     * @param string $livro
     */
    public function setLivro($livro)
    {
        $this->livro = $livro;
    }

    /**
     * @return string
     */
    public function getFolha()
    {
        return $this->folha;
    }

    /**
     * @param string $folha
     */
    public function setFolha($folha)
    {
        $this->folha = $folha;
    }

    /**
     * @return int
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param int $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    /**
     * @return Falecido
     */
    public function getFalecido()
    {
        return $this->falecido;
    }

    /**
     * @param Falecido $falecido
     */
    public function setFalecido($falecido)
    {
        $this->falecido = $falecido;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'dataCadastro' => $this->dataCadastro->format('d/m/Y'),
            'nomeConjuge' => $this->nomeConjuge,
            'dataCasamento' => $this->dataCasamento->format('d/m/Y'),
            'cartorio' => $this->cartorio,
            'cidade' => $this->cidade,
            'livro' => $this->livro,
            'folha' => $this->folha,
            'numero' => $this->numero,
            'falecido' => $this->falecido
        ];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->nomeConjuge;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
