<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;


/**
 * Pagamento
 *
 * @ORM\Table(name="tb_pagamento", indexes={@ORM\Index(name="IDX_C428D160FA198E41", columns={"id_contratacao"}), @ORM\Index(name="FK_tb_pagamento_tb_tipo_pagamento", columns={"tipo_pagamento"}), @ORM\Index(name="FK_tb_pagamento_tb_bandeira", columns={"bandeira"}), @ORM\Index(name="FK_tb_pagamento_tb_emitente", columns={"id_emitente"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\PagamentoRepository")
 */
class Pagamento extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_pagamento", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="parcelamento", type="integer", nullable=true)
     */
    private $parcelamento;

    /**
     * @var string
     *
     * @ORM\Column(name="pinpad", type="string", length=150, nullable=true)
     */
    private $pinpad;

    /**
     * @var string
     *
     * @ORM\Column(name="autorizacao", type="string", length=150, nullable=true)
     */
    private $autorizacao;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_pagamento", type="string", length=150, nullable=true)
     */
    private $valorPagamento;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=250, nullable=true)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="numero_documento", type="string", length=150, nullable=true)
     */
    private $numeroDocumento;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_telefone", type="string", length=14, nullable=true)
     */
    private $telefone;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_endereco", type="string", length=250, nullable=true)
     */
    private $endereco;

    /**
     * @var \Agencia\Entity\Emitente
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Emitente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_emitente", referencedColumnName="id_emitente")
     * })
     */
    private $emitente;

    /**
     * @var \Agencia\Entity\Contratacao
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Contratacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contratacao", referencedColumnName="id_contratacao")
     * })
     */
    private $contratacao;

    /**
     * @var \Agencia\Entity\Bandeira
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Bandeira")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="bandeira", referencedColumnName="id_bandeira")
     * })
     */
    private $bandeira;

    /**
     * @var \Agencia\Entity\TipoPagamento
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\TipoPagamento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_pagamento", referencedColumnName="id_tipo_pagamento")
     * })
     */
    private $tipoPagamento;
   /**
     * @var string
     *
     * @ORM\Column(name="txt_cvdoc", type="string", length=50, nullable=true)
     */
    private $cvdoc;
    
    function getCvdoc() {
        return $this->cvdoc;
    }

    function setCvdoc($cvdoc) {
        $this->cvdoc = $cvdoc;
    }

        
    
    function getId() {
        return $this->id;
    }

    function getParcelamento() {
        return $this->parcelamento;
    }

    function getPinpad() {
        return $this->pinpad;
    }

    function getAutorizacao() {
        return $this->autorizacao;
    }

    function getValorPagamento() {
        return $this->valorPagamento;
    }

    function getNome() {
        return $this->nome;
    }

    function getNumeroDocumento() {
        return $this->numeroDocumento;
    }

    function getTelefone() {
        return $this->telefone;
    }

    function getEndereco() {
        return $this->endereco;
    }

    function getEmitente() {
        return $this->emitente;
    }

    function getContratacao() {
        return $this->contratacao;
    }

    function getBandeira() {
        return $this->bandeira;
    }

    function getTipoPagamento() {
        return $this->tipoPagamento;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setParcelamento($parcelamento) {
        $this->parcelamento = $parcelamento;
    }

    function setPinpad($pinpad) {
        $this->pinpad = $pinpad;
    }

    function setAutorizacao($autorizacao) {
        $this->autorizacao = $autorizacao;
    }

    function setValorPagamento($valorPagamento) {
        $this->valorPagamento = $valorPagamento;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setNumeroDocumento($numeroDocumento) {
        $this->numeroDocumento = $numeroDocumento;
    }

    function setTelefone($telefone) {
        $this->telefone = $telefone;
    }

    function setEndereco($endereco) {
        $this->endereco = $endereco;
    }

    function setEmitente(\Agencia\Entity\Emitente $emitente) {
        $this->emitente = $emitente;
    }

    function setContratacao(\Agencia\Entity\Contratacao $contratacao) {
        $this->contratacao = $contratacao;
    }

    function setBandeira(\Agencia\Entity\Bandeira $bandeira) {
        
        
        $this->bandeira = $bandeira;
    }

    function setTipoPagamento(\Agencia\Entity\TipoPagamento $tipoPagamento) {
        $this->tipoPagamento = $tipoPagamento;
    }


    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'tipoPagamento' => $this->tipoPagamento,
            'bandeira' => $this->bandeira,
            'parcelamento' => $this->parcelamento,
            'pinpad' => $this->pinpad,
            'autorizacao' => $this->autorizacao,
            'valorPagamento' => $this->valorPagamento,
            'nome' => $this->nome,
            'numeroDocumento' => $this->numeroDocumento,
            'telefone' => $this->telefone,
            'endereco' => $this->endereco,
            'contratacao' => $this->contratacao,
            'emitente' => $this->emitente,
            'cvdoc'=> $this->cvdoc
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->nome;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }



}
