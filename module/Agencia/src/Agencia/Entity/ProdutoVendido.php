<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * ProdutoVendido
 *
 * @ORM\Table(name="tb_produto_vendido", indexes={@ORM\Index(name="fk_tb_produto_vendido_tb_produto1_idx", columns={"id_produto"}),@ORM\Index(name="fk_tb_produto_vendido_tb_classificacao_venda1_idx", columns={"id_classificacao_venda"}), @ORM\Index(name="fk_tb_produto_vendido_tb_venda_produto1_idx", columns={"id_venda_produto"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\ProdutoVendidoRepository")
 */
class ProdutoVendido extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_produto_vendido", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

     /**
     * @var \Estoque\Entity\Produto
     *
     * @ORM\ManyToOne(targetEntity="Estoque\Entity\Produto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_produto", referencedColumnName="id_produto")
     * })
     */
    private $produto;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantidade", type="integer", nullable=true)
     */
    private $quantidade;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_conjunto", type="string", length=150, nullable=true)
     */
    private $valorConjunto;

    
     /**
     * @var \Estoque\Entity\CategoriaProduto
     *
     * @ORM\ManyToOne(targetEntity="Estoque\Entity\CategoriaProduto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_categoria", referencedColumnName="id_categoria")
     * })
     */
    private $categoria;

    /**
     * @var \Agencia\Entity\ClassificacaoVenda
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\ClassificacaoVenda")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_classificacao_venda", referencedColumnName="id_classificacao_venda")
     * })
     */
    private $classificacaoVenda;

    /**
     * @var \Agencia\Entity\VendaProdutoServico
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\VendaProdutoServico",cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_venda_produto", referencedColumnName="id_venda_produto")
     * })
     */
    private $vendaProduto;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getProduto()
    {
        return $this->produto;
    }

    /**
     * @param int $produto
     */
    public function setProduto($produto)
    {
        $this->produto = $produto;
    }

    /**
     * @return int
     */
    public function getQuantidade()
    {
        return $this->quantidade;
    }

    /**
     * @param int $quantidade
     */
    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;
    }

    /**
     * @return string
     */
    public function getValorConjunto()
    {
        return $this->valorConjunto;
    }

    /**
     * @param string $valorConjunto
     */
    public function setValorConjunto($valorConjunto)
    {
        $this->valorConjunto = $valorConjunto;
    }

    /**
     * @return int
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param int $categoria
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }

    /**
     * @return ClassificacaoVenda
     */
    public function getClassificacaoVenda()
    {
        return $this->classificacaoVenda;
    }

    /**
     * @param ClassificacaoVenda $classificacaoVenda
     */
    public function setClassificacaoVenda($classificacaoVenda)
    {
        $this->classificacaoVenda = $classificacaoVenda;
    }

    /**
     * @return VendaProdutoServico
     */
    public function getVendaProduto()
    {
        return $this->vendaProduto;
    }

    /**
     * @param VendaProdutoServico $vendaProduto
     */
    public function setVendaProduto($vendaProduto)
    {
        $this->vendaProduto = $vendaProduto;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'produto' => $this->produto,
            'quantidade' => $this->quantidade,
            'valorConjunto' => $this->valorConjunto,
            'categoria' => $this->categoria,
            'classificacaoVenda' => $this->classificacaoVenda,
            'vendaProduto' => $this->vendaProduto,
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->produto;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
