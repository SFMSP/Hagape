<?php
namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Bandeira
 *
 * @ORM\Table(name="tb_bandeira")
 * @ORM\Entity(repositoryClass="Agencia\Repository\BandeiraRepository")
 */
class Bandeira extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_bandeira", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_bandeira", type="string", length=150, nullable=false)
     */
    private $bandeira;
    
    function getId() {
        return $this->id;
    }

    function getBandeira() {
        return $this->bandeira;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setBandeira($bandeira) {
        $this->bandeira = $bandeira;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'bandeira'=> $this->bandeira
        
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->bandeira;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
