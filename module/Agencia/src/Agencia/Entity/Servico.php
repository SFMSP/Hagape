<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Servico
 *
 * @ORM\Table(name="tb_servico")
 * @ORM\Entity(repositoryClass="Agencia\Repository\ServicoRepository")
 */
class Servico extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_servico", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_cod_sof", type="string", length=150, nullable=true)
     */
    private $codSof;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome", type="string", length=150, nullable=true)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="valor", type="string", length=150, nullable=true)
     */
    private $valor;

    /**
     * @var boolean
     *
     * @ORM\Column(name="situacao", type="boolean", nullable=true)
     */
    private $situacao;

    /**
     * @var string
     *
     * @ORM\Column(name="preco_venda", type="decimal", precision=7, scale=2, nullable=true)
     */
    private $precoVenda;
    /**
     * @var \Estoque\Entity\CategoriaProduto
     *
     * @ORM\ManyToOne(targetEntity="Estoque\Entity\CategoriaProduto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_categoria", referencedColumnName="id_categoria")
     * })
     */
    private $categoria;
    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCodSof()
    {
        return $this->codSof;
    }

    /**
     * @param string $codSof
     */
    public function setCodSof($codSof)
    {
        $this->codSof = $codSof;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return string
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * @param string $valor
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    }

    function getCategoria() {
        return $this->categoria;
    }

    function setCategoria(\Estoque\Entity\CategoriaProduto $categoria) {
        $this->categoria = $categoria;
    }

        /**
     * @return boolean
     */
    public function isSituacao()
    {
        return $this->situacao;
    }

    /**
     * @param boolean $situacao
     */
    public function setSituacao($situacao)
    {
        $this->situacao = $situacao;
    }

    /**
     * @return string
     */
    public function getPrecoVenda()
    {
        return $this->precoVenda;
    }

    /**
     * @param string $precoVenda
     */
    public function setPrecoVenda($precoVenda)
    {
        $this->precoVenda = $precoVenda;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'codSof' => $this->codSof,
            'nome' => $this->nome,
            'valor' => $this->valor,
            'categoria' => $this->categoria,
            'situacao' => $this->situacao,
            'precoVenda' => $this->precoVenda
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->nome;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }



    /**
     * Get situacao
     *
     * @return boolean 
     */
    public function getSituacao()
    {
        return $this->situacao;
    }
}
