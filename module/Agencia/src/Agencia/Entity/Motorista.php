<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Motorista
 *
 * @ORM\Table(name="tb_motorista", indexes={@ORM\Index(name="fk_tb_motorista_tb_dados_empresa1_idx", columns={"id_dados_empresa"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\MotoristaRepository")
 */
class Motorista extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_motorista", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome_motorista", type="string", length=255, nullable=true)
     */
    private $nomeMotorista;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_rg", type="string", length=45, nullable=true)
     */
    private $rg;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_placa_veiculo", type="string", length=45, nullable=true)
     */
    private $placaVeiculo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=true)
     */
    private $dataCadastro;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_telefone", type="string", length=15, nullable=true)
     */
    private $telefone;

    /**
     * @var \Agencia\Entity\DadosEmpresa
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\DadosEmpresa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_dados_empresa", referencedColumnName="id_dados_empresa")
     * })
     */
    private $dadosEmpresa;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNomeMotorista()
    {
        return $this->nomeMotorista;
    }

    /**
     * @param string $nomeMotorista
     */
    public function setNomeMotorista($nomeMotorista)
    {
        $this->nomeMotorista = $nomeMotorista;
    }

    /**
     * @return string
     */
    public function getRg()
    {
        return $this->rg;
    }

    /**
     * @param string $rg
     */
    public function setRg($rg)
    {
        $this->rg = $rg;
    }

    /**
     * @return string
     */
    public function getPlacaVeiculo()
    {
        return $this->placaVeiculo;
    }

    /**
     * @param string $placaVeiculo
     */
    public function setPlacaVeiculo($placaVeiculo)
    {
        $this->placaVeiculo = $placaVeiculo;
    }

    /**
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param \DateTime $dataCadastro
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
    }

    /**
     * @return string
     */
    public function getTelefone()
    {
        return $this->telefone;
    }

    /**
     * @param string $telefone
     */
    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;
    }

    /**
     * @return DadosEmpresa
     */
    public function getDadosEmpresa()
    {
        return $this->dadosEmpresa;
    }

    /**
     * @param DadosEmpresa $dadosEmpresa
     */
    public function setDadosEmpresa($dadosEmpresa)
    {
        $this->dadosEmpresa = $dadosEmpresa;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'nomeMotorista' => $this->nomeMotorista,
            'rg' => $this->rg,
            'placaVeiculo' => $this->placaVeiculo,
            'dataCadastro' => $this->dataCadastro,
            'telefone' => $this->telefone,
            'dadosEmpresa' => $this->dadosEmpresa
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->nomeMotorista;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
