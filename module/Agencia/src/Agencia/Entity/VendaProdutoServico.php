<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * VendaProdutoServico
 *
 * @ORM\Table(name="tb_venda_produto_servico", indexes={@ORM\Index(name="fk_tb_venda_produto_tb_convenio1_idx", columns={"id_convenio"}), @ORM\Index(name="fk_tb_venda_produto_tb_contratacao1_idx", columns={"id_contratacao"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\VendaProdutoServicoRepository")
 */
class VendaProdutoServico extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_venda_produto", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_convenio", type="boolean", nullable=true)
     */
    private $boolConvenio;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_remove_corpo_local_fal", type="boolean", nullable=true)
     */
    private $removeCorpoLocalFal;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_local_remocao_corpo", type="string", length=255, nullable=true)
     */
    private $localRemocaoCorpo;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_end_remocao_corpo", type="string", length=255, nullable=true)
     */
    private $endRemocaoCorpo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_remove_membro_local_amp", type="boolean", nullable=true)
     */
    private $removeMembroLocalAmp;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_local_remocao_membro", type="string", length=255, nullable=true)
     */
    private $localRemocaoMembro;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_end_remocao_membro", type="string", length=255, nullable=true)
     */
    private $endRemocaoMembro;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_total", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $valorTotal;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_observacao", type="text", nullable=true)
     */
    private $observacao;

    /**
     * @var \Agencia\Entity\Convenio
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Convenio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_convenio", referencedColumnName="id_convenio")
     * })
     */
    private $convenio;

    /**
     * @var \Agencia\Entity\Contratacao
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Contratacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contratacao", referencedColumnName="id_contratacao")
     * })
     */
    private $contratacao;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return bool
     */
    public function isBoolConvenio()
    {
        return $this->boolConvenio;
    }

    /**
     * @param bool $boolConvenio
     */
    public function setBoolConvenio($boolConvenio)
    {
        $this->boolConvenio = $boolConvenio;
    }

    /**
     * @return bool
     */
    public function isRemoveCorpoLocalFal()
    {
        return $this->removeCorpoLocalFal;
    }

    /**
     * @param bool $removeCorpoLocalFal
     */
    public function setRemoveCorpoLocalFal($removeCorpoLocalFal)
    {
        $this->removeCorpoLocalFal = $removeCorpoLocalFal;
    }

    /**
     * @return string
     */
    public function getLocalRemocaoCorpo()
    {
        return $this->localRemocaoCorpo;
    }

    /**
     * @param string $localRemocaoCorpo
     */
    public function setLocalRemocaoCorpo($localRemocaoCorpo)
    {
        $this->localRemocaoCorpo = $localRemocaoCorpo;
    }

    /**
     * @return string
     */
    public function getEndRemocaoCorpo()
    {
        return $this->endRemocaoCorpo;
    }

    /**
     * @param string $endRemocaoCorpo
     */
    public function setEndRemocaoCorpo($endRemocaoCorpo)
    {
        $this->endRemocaoCorpo = $endRemocaoCorpo;
    }

    /**
     * @return bool
     */
    public function isRemoveMembroLocalAmp()
    {
        return $this->removeMembroLocalAmp;
    }

    /**
     * @param bool $removeMembroLocalAmp
     */
    public function setRemoveMembroLocalAmp($removeMembroLocalAmp)
    {
        $this->removeMembroLocalAmp = $removeMembroLocalAmp;
    }

    /**
     * @return string
     */
    public function getLocalRemocaoMembro()
    {
        return $this->localRemocaoMembro;
    }

    /**
     * @param string $localRemocaoMembro
     */
    public function setLocalRemocaoMembro($localRemocaoMembro)
    {
        $this->localRemocaoMembro = $localRemocaoMembro;
    }

    /**
     * @return string
     */
    public function getEndRemocaoMembro()
    {
        return $this->endRemocaoMembro;
    }

    /**
     * @param string $endRemocaoMembro
     */
    public function setEndRemocaoMembro($endRemocaoMembro)
    {
        $this->endRemocaoMembro = $endRemocaoMembro;
    }

    /**
     * @return string
     */
    public function getValorTotal()
    {
        return $this->valorTotal;
    }

    /**
     * @param string $valorTotal
     */
    public function setValorTotal($valorTotal)
    {
        $this->valorTotal = $valorTotal;
    }

    /**
     * @return string
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * @param string $observacao
     */
    public function setObservacao($observacao)
    {
        $this->observacao = $observacao;
    }

    /**
     * @return Convenio
     */
    public function getConvenio()
    {
        return $this->convenio;
    }

    /**
     * @param Convenio $convenio
     */
    public function setConvenio($convenio)
    {
        $this->convenio = $convenio;
    }

    /**
     * @return Contratacao
     */
    public function getContratacao()
    {
        return $this->contratacao;
    }

    /**
     * @param Contratacao $contratacao
     */
    public function setContratacao($contratacao)
    {
        $this->contratacao = $contratacao;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'boolConvenio' => $this->boolConvenio,
            'removeCorpoLocalFal' => $this->removeCorpoLocalFal,
            'localRemocaoCorpo' => $this->localRemocaoCorpo,
            'endRemocaoCorpo' => $this->endRemocaoCorpo,
            'removeMembroLocalAmp' => $this->removeMembroLocalAmp,
            'localRemocaoMembro' => $this->localRemocaoMembro,
            'endRemocaoMembro' => $this->endRemocaoMembro,
            'valorTotal' => $this->valorTotal,
            'observacao' => $this->observacao,
            'convenio' => $this->convenio,
            'contratacao' => $this->contratacao
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->observacao;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
