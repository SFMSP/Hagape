<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;
/**
 * Cemiterio
 *
 * @ORM\Table(name="tb_cemiterio", indexes={@ORM\Index(name="IDX_1AACBFF64CAF86B1", columns={"id_cidade"}), @ORM\Index(name="IDX_1AACBFF6FB0D0145", columns={"id_tipo"})})
 * @ORM\Entity
 */
class Cemiterio extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_cemiterio", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=false)
     */
    private $dataCadastro;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome", type="string", length=255, nullable=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_endereco", type="string", length=255, nullable=false)
     */
    private $endereco;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_numero", type="string", length=255, nullable=false)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_bairro", type="string", length=100, nullable=false)
     */
    private $bairro;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_cep", type="string", length=20, nullable=false)
     */
    private $cep;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_primeiro_telefone", type="string", length=50, nullable=false)
     */
    private $primeiroTelefone;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_segundo_telefone", type="string", length=50, nullable=false)
     */
    private $segundoTelefone;

    /**
     * @var string
     *
     * @ORM\Column(name="categoria", type="string", length=255, nullable=false)
     */
    private $categoria;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_observacao", type="string", length=255, nullable=false)
     */
    private $observacao;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_ativo", type="boolean", nullable=false)
     */
    private $ativo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_excluido", type="boolean", nullable=false)
     */
    private $excluido;

    /**
     * @var \Agencia\Entity\Tipo
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Tipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipo", referencedColumnName="id_tipo")
     * })
     */
    private $tipo;

    /**
     * @var \Agencia\Entity\Cidade
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Cidade")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cidade", referencedColumnName="id_cidade")
     * })
     */
    private $cidade;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Agencia\Entity\Usuario", mappedBy="cemiterio")
     */
    private $usuario;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Agencia\Entity\TipoSepultamento", inversedBy="cemiterio")
     * @ORM\JoinTable(name="tb_tipo_sepultamento_cemiterio",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_cemiterio", referencedColumnName="id_cemiterio")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_tipo_sepultamento", referencedColumnName="id_tipo_sepultamento")
     *   }
     * )
     */
    private $tipoSepultamento;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->usuario = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tipoSepultamento = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param \DateTime $dataCadastro
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return string
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param string $endereco
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    }

    /**
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param string $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    /**
     * @return string
     */
    public function getBairro()
    {
        return $this->bairro;
    }

    /**
     * @param string $bairro
     */
    public function setBairro($bairro)
    {
        $this->bairro = $bairro;
    }

    /**
     * @return string
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * @param string $cep
     */
    public function setCep($cep)
    {
        $this->cep = $cep;
    }

    /**
     * @return string
     */
    public function getPrimeiroTelefone()
    {
        return $this->primeiroTelefone;
    }

    /**
     * @param string $primeiroTelefone
     */
    public function setPrimeiroTelefone($primeiroTelefone)
    {
        $this->primeiroTelefone = $primeiroTelefone;
    }

    /**
     * @return string
     */
    public function getSegundoTelefone()
    {
        return $this->segundoTelefone;
    }

    /**
     * @param string $segundoTelefone
     */
    public function setSegundoTelefone($segundoTelefone)
    {
        $this->segundoTelefone = $segundoTelefone;
    }

    /**
     * @return string
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param string $categoria
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * @param string $observacao
     */
    public function setObservacao($observacao)
    {
        $this->observacao = $observacao;
    }

    /**
     * @return boolean
     */
    public function isAtivo()
    {
        return $this->ativo;
    }

    /**
     * @param boolean $ativo
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
    }

    /**
     * @return boolean
     */
    public function isExcluido()
    {
        return $this->excluido;
    }

    /**
     * @param boolean $excluido
     */
    public function setExcluido($excluido)
    {
        $this->excluido = $excluido;
    }

    /**
     * @return Tipo
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param Tipo $tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    /**
     * @return Cidade
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * @param Cidade $cidade
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTipoSepultamento()
    {
        return $this->tipoSepultamento;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $tipoSepultamento
     */
    public function setTipoSepultamento($tipoSepultamento)
    {
        $this->tipoSepultamento = $tipoSepultamento;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        // TODO: Implement toArray() method.
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        // TODO: Implement __toString() method.
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        // TODO: Implement getLabel() method.
    }

}
