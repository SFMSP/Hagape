<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * NascimentoObito
 *
 * @ORM\Table(name="tb_nascimento_obito", indexes={@ORM\Index(name="fk_tb_nascimento_obito_tb_falecido1_idx", columns={"id_falecido"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\NascimentoObitoRepository")
 */
class NascimentoObito extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_nascimento_obito", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=true)
     */
    private $dataCadastro;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_local", type="string", length=255, nullable=true)
     */
    private $local;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_obito", type="date", nullable=true)
     */
    private $dataObito;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_horario", type="string", length=45, nullable=true)
     */
    private $horario;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_avo_m_paterno", type="string", length=255, nullable=true)
     */
    private $avoMPaterno;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_avo_m_materno", type="string", length=255, nullable=true)
     */
    private $avoMMaterno;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_avo_f_paterno", type="string", length=255, nullable=true)
     */
    private $avoFPaterno;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_avo_f_materno", type="string", length=255, nullable=true)
     */
    private $avoFMaterno;

    /**
     * @var integer
     *
     * @ORM\Column(name="gestacao", type="integer", nullable=true)
     */
    private $gestacao;

    /**
     * @var string
     *
     * @ORM\Column(name="gravidez", type="string", length=150, nullable=true)
     */
    private $gravidez;

    /**
     * @var \Agencia\Entity\Falecido
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Falecido")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_falecido", referencedColumnName="id_falecido")
     * })
     */
    private $falecido;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param \DateTime $dataCadastro
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
    }

    /**
     * @return string
     */
    public function getLocal()
    {
        return $this->local;
    }

    /**
     * @param string $local
     */
    public function setLocal($local)
    {
        $this->local = $local;
    }

    /**
     * @return \DateTime
     */
    public function getDataObito()
    {
        return $this->dataObito;
    }

    /**
     * @param \DateTime $dataObito
     */
    public function setDataObito($dataObito)
    {
        $this->dataObito = $dataObito;
    }

    /**
     * @return string
     */
    public function getHorario()
    {
        return $this->horario;
    }

    /**
     * @param string $horario
     */
    public function setHorario($horario)
    {
        $this->horario = $horario;
    }

    /**
     * @return string
     */
    public function getAvoMPaterno()
    {
        return $this->avoMPaterno;
    }

    /**
     * @param string $avoMPaterno
     */
    public function setAvoMPaterno($avoMPaterno)
    {
        $this->avoMPaterno = $avoMPaterno;
    }

    /**
     * @return string
     */
    public function getAvoMMaterno()
    {
        return $this->avoMMaterno;
    }

    /**
     * @param string $avoMMaterno
     */
    public function setAvoMMaterno($avoMMaterno)
    {
        $this->avoMMaterno = $avoMMaterno;
    }

    /**
     * @return string
     */
    public function getAvoFPaterno()
    {
        return $this->avoFPaterno;
    }

    /**
     * @param string $avoFPaterno
     */
    public function setAvoFPaterno($avoFPaterno)
    {
        $this->avoFPaterno = $avoFPaterno;
    }

    /**
     * @return string
     */
    public function getAvoFMaterno()
    {
        return $this->avoFMaterno;
    }

    /**
     * @param string $avoFMaterno
     */
    public function setAvoFMaterno($avoFMaterno)
    {
        $this->avoFMaterno = $avoFMaterno;
    }

    /**
     * @return int
     */
    public function getGestacao()
    {
        return $this->gestacao;
    }

    /**
     * @param int $gestacao
     */
    public function setGestacao($gestacao)
    {
        $this->gestacao = $gestacao;
    }

    /**
     * @return string
     */
    public function getGravidez()
    {
        return $this->gravidez;
    }

    /**
     * @param string $gravidez
     */
    public function setGravidez($gravidez)
    {
        $this->gravidez = $gravidez;
    }

    /**
     * @return Falecido
     */
    public function getFalecido()
    {
        return $this->falecido;
    }

    /**
     * @param Falecido $falecido
     */
    public function setFalecido($falecido)
    {
        $this->falecido = $falecido;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'dataCadastro' => $this->dataCadastro->format('d/m/Y'),
            'local' => $this->local,
            'dataObito' => $this->dataObito->format('d/m/Y'),
            'horario' => $this->horario,
            'avoMPaterno' => $this->avoMPaterno,
            'avoMMaterno' => $this->avoMMaterno,
            'avoFPaterno' => $this->avoFPaterno,
            'avoFMaterno' => $this->avoFMaterno,
            'gestacao' => $this->gestacao,
            'gravidez' => $this->gravidez,
            'falecido' => $this->falecido
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->local;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
