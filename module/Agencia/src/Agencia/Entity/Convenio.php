<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;
/**
 * Convenio
 *
 * @ORM\Table(name="tb_convenio", indexes={@ORM\Index(name="fk_tb_convenio_tb_classificacao_venda1_idx", columns={"id_classificacao_venda"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\ConvenioRepository")
 */
class Convenio extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_convenio", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome", type="string", length=150, nullable=true)
     */
    private $nome;

    /**
     * @var integer
     *
     * @ORM\Column(name="tipo", type="integer", nullable=true)
     */
    private $tipo;

    /**
     * @var string
     *
     * @ORM\Column(name="valor", type="string", length=45, nullable=true)
     */
    private $valor;

    /**
     * @var \Agencia\Entity\ClassificacaoVenda
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\ClassificacaoVenda")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_classificacao_venda", referencedColumnName="id_classificacao_venda")
     * })
     */
    private $classificacaoVenda;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return int
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param int $tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    /**
     * @return string
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * @param string $valor
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    }

    /**
     * @return ClassificacaoVenda
     */
    public function getClassificacaoVenda()
    {
        return $this->classificacaoVenda;
    }

    /**
     * @param ClassificacaoVenda $classificacaoVenda
     */
    public function setClassificacaoVenda($classificacaoVenda)
    {
        $this->classificacaoVenda = $classificacaoVenda;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'nome' => $this->nome,
            'tipo' => $this->tipo,
            'valor' => $this->valor,
            'classificacaoVenda' => $this->classificacaoVenda,
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->nome;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
