<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProcessoAba
 *
 * @ORM\Table(name="tb_processo_aba")
 * @ORM\Entity(repositoryClass="Agencia\Repository\ProcessoAbaRepository")
 */
class ProcessoAba
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_processo_aba", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_aba", type="integer", nullable=false)
     */
    private $aba;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_processo", type="integer", nullable=true)
     */
    private $processo;

    /**
     * @var string
     *
     * @ORM\Column(name="campo_opcao", type="string", length=150, nullable=true)
     */
    private $campoOpcao;

    /**
     * @var integer
     *
     * @ORM\Column(name="bool_declaracao_obito", type="integer", nullable=true)
     */
    private $boolDeclaracaoObito;
    function getId() {
        return $this->id;
    }

    function getAba() {
        return $this->aba;
    }

    function getProcesso() {
        return $this->processo;
    }

    function getCampoOpcao() {
        return $this->campoOpcao;
    }

    function getBoolDeclaracaoObito() {
        return $this->boolDeclaracaoObito;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setAba($aba) {
        $this->aba = $aba;
    }

    function setProcesso($processo) {
        $this->processo = $processo;
    }

    function setCampoOpcao($campoOpcao) {
        $this->campoOpcao = $campoOpcao;
    }

    function setBoolDeclaracaoObito($boolDeclaracaoObito) {
        $this->boolDeclaracaoObito = $boolDeclaracaoObito;
    }
    public function toArray()
    {
        return [
            'id' => $this->id,
            'aba' => $this->aba,
            'processo' => $this->processo,
            'campoOpcao' => $this->campoOpcao,
            'boolDeclaracaoObito' => $this->boolDeclaracaoObito
        
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->processo;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
