<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * TransladoCorpo
 *
 * @ORM\Table(name="tb_translado_corpo", indexes={@ORM\Index(name="fk_tb_translado_corpo_tb_venda1_idx", columns={"id_contratacao"}), @ORM\Index(name="fk_tb_translado_corpo_tb_contratante1_idx", columns={"id_contratante"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\TransladoCorpoRepository")
 */
class TransladoCorpo extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_translado_corpo", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome_falecido", type="string", length=255, nullable=true)
     */
    private $nomeFalecido;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_numero_processo", type="string", length=45, nullable=true)
     */
    private $numeroProcesso;

     /**
     * @var \Admin\Entity\Cemiterio
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Cemiterio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cemiterio_saida", referencedColumnName="id_cemiterio")
     * })
     */
    private $cemiterioSaida;

   /**
     * @var \Admin\Entity\Cemiterio
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Cemiterio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cemiterio_destino", referencedColumnName="id_cemiterio")
     * })
     */
    private $cemiterioDestino;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_saida", type="date", nullable=true)
     */
    private $dataSaida;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=true)
     */
    private $dataCadastro;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_hora_cremacao", type="string", length=45, nullable=true)
     */
    private $horaCremacao;

    /**
     * @var \Agencia\Entity\Contratacao
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Contratacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contratacao", referencedColumnName="id_contratacao")
     * })
     */
    private $contratacao;

    /**
     * @var \Agencia\Entity\Contratante
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Contratante")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contratante", referencedColumnName="id_contratante")
     * })
     */
    private $contratante;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNomeFalecido()
    {
        return $this->nomeFalecido;
    }

    /**
     * @param string $nomeFalecido
     */
    public function setNomeFalecido($nomeFalecido)
    {
        $this->nomeFalecido = $nomeFalecido;
    }

    /**
     * @return string
     */
    public function getNumeroProcesso()
    {
        return $this->numeroProcesso;
    }

    /**
     * @param string $numeroProcesso
     */
    public function setNumeroProcesso($numeroProcesso)
    {
        $this->numeroProcesso = $numeroProcesso;
    }

    /**
     * @return int
     */
    public function getCemiterioSaida()
    {
        return $this->cemiterioSaida;
    }

    /**
     * @param int $cemiterioSaida
     */
    function setCemiterioSaida(\Admin\Entity\Cemiterio $cemiterioSaida) {
        $this->cemiterioSaida = $cemiterioSaida;
    }

    /**
     * @return int
     */
    public function getCemiterioDestino()
    {
        return $this->cemiterioDestino;
    }
    /**
     * @param int $cemiterioDestino
     */
     function setCemiterioDestino(\Admin\Entity\Cemiterio $cemiterioDestino) {
        $this->cemiterioDestino = $cemiterioDestino;
    }
    /**
     * @return \DateTime
     */
    public function getDataSaida()
    {
        return $this->dataSaida;
    }

    /**
     * @param \DateTime $dataSaida
     */
    public function setDataSaida($dataSaida)
    {
        $this->dataSaida = $dataSaida;
    }

    /**
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param \DateTime $dataCadastro
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
    }

    /**
     * @return string
     */
    public function getHoraCremacao()
    {
        return $this->horaCremacao;
    }

    /**
     * @param string $horaCremacao
     */
    public function setHoraCremacao($horaCremacao)
    {
        $this->horaCremacao = $horaCremacao;
    }

    /**
     * @return Contratacao
     */
    public function getContratacao()
    {
        return $this->contratacao;
    }

    /**
     * @param Contratacao $contratacao
     */
    public function setContratacao($contratacao)
    {
        $this->contratacao = $contratacao;
    }

    /**
     * @return Contratante
     */
    public function getContratante()
    {
        return $this->contratante;
    }

    /**
     * @param Contratante $contratante
     */
    public function setContratante($contratante)
    {
        $this->contratante = $contratante;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'nomeFalecido' => $this->nomeFalecido,
            'numeroProcesso' => $this->numeroProcesso,
            'cemiterioSaida' => $this->cemiterioSaida,
            'cemiterioDestino' => $this->cemiterioDestino,
            'dataSaida' => $this->dataSaida->format('d/m/Y'),
            'dataCadastro' => $this->dataCadastro->format('d/m/Y'),
            'horaCremacao' => $this->horaCremacao,
            'contratacao' => $this->contratacao,
            'contratante' => $this->contratante
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->nomeFalecido;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
