<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * FilhoFalecido
 *
 * @ORM\Table(name="tb_filho_falecido", indexes={@ORM\Index(name="fk_tb_filho_falecido_tb_falecido1_idx", columns={"id_falecido"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\FilhoFalecidoRepository")
 */
class FilhoFalecido extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_filho_falecido", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome_filho", type="string", length=255, nullable=true)
     */
    private $nomeFilho;

    /**
     * @var integer
     *
     * @ORM\Column(name="idade", type="integer", nullable=true)
     */
    private $idade;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_observacao", type="text", nullable=true)
     */
    private $observacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=true)
     */
    private $dataCadastro;

    /**
     * @var \Agencia\Entity\Falecido
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Falecido")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_falecido", referencedColumnName="id_falecido")
     * })
     */
    private $falecido;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNomeFilho()
    {
        return $this->nomeFilho;
    }

    /**
     * @param string $nomeFilho
     */
    public function setNomeFilho($nomeFilho)
    {
        $this->nomeFilho = $nomeFilho;
    }

    /**
     * @return int
     */
    public function getIdade()
    {
        return $this->idade;
    }

    /**
     * @param int $idade
     */
    public function setIdade($idade)
    {
        $this->idade = $idade;
    }

    /**
     * @return string
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * @param string $observacao
     */
    public function setObservacao($observacao)
    {
        $this->observacao = $observacao;
    }

    /**
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param \DateTime $dataCadastro
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
    }

    /**
     * @return Falecido
     */
    public function getFalecido()
    {
        return $this->falecido;
    }

    /**
     * @param Falecido $falecido
     */
    public function setFalecido($falecido)
    {
        $this->falecido = $falecido;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'nomeFilho' => $this->nomeFilho,
            'idade' => $this->idade,
            'observacao' => $this->observacao,
            'dataCadastro' => $this->dataCadastro->format('d/m/Y'),
            'falecido' => $this->falecido
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->nomeFilho;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
