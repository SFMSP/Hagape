<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * ClassificacaoVenda
 *
 * @ORM\Table(name="tb_classificacao_venda", indexes={@ORM\Index(name="fk_id_produto_idx", columns={"id_produto"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\ClassificacaoVendaRepository")
 */
class ClassificacaoVenda extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_classificacao_venda", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome", type="string", length=150, nullable=true)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="preco", type="string", length=150, nullable=true)
     */
    private $preco;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_tamanho", type="integer", nullable=true)
     */
    private $tamanho;

    /**
     * @var boolean
     *
     * @ORM\Column(name="situacao", type="boolean", nullable=true)
     */
    private $situacao;

    /**
     * @var \Estoque\Entity\Produto
     *
     * @ORM\ManyToOne(targetEntity="Estoque\Entity\Produto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_produto", referencedColumnName="id_produto")
     * })
     */
    private $produto;

    /**
     * @return \Estoque\Entity\Produto
     */
    public function getProduto()
    {
        return $this->produto;
    }

    /**
     * @param \Estoque\Entity\Produto $produto
     */
    public function setProduto($produto)
    {
        $this->produto = $produto;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return string
     */
    public function getPreco()
    {
        return $this->preco;
    }

    /**
     * @param string $preco
     */
    public function setPreco($preco)
    {
        $this->preco = $preco;
    }

    /**
     * @return int
     */
    public function getTamanho()
    {
        return $this->tamanho;
    }

    /**
     * @param int $tamanho
     */
    public function setTamanho($tamanho)
    {
        $this->tamanho = $tamanho;
    }

    /**
     * @return boolean
     */
    public function isSituacao()
    {
        return $this->situacao;
    }

    /**
     * @param boolean $situacao
     */
    public function setSituacao($situacao)
    {
        $this->situacao = $situacao;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'nome' => $this->nome,
            'preco' => $this->preco,
            'tamanho' => $this->tamanho,
            'situacao' => $this->situacao,
            'produto' => $this->produto,
        ];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->nome;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->__toString();
    }


    /**
     * Get situacao
     *
     * @return boolean
     */
    public function getSituacao()
    {
        return $this->situacao;
    }
}
