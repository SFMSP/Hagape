<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * DadosObito
 *
 * @ORM\Table(name="tb_dados_obito", indexes={@ORM\Index(name="fk_tb_dados_obito_tb_cartorio1_idx", columns={"id_cartorio"}), @ORM\Index(name="fk_tb_dados_obito_tb_local_falecimento1_idx", columns={"id_local_falecimento"}), @ORM\Index(name="fk_tb_dados_obito_tb_falecido1_idx", columns={"id_falecido"}), @ORM\Index(name="fk_tb_dados_obito_tb_transporte1_idx", columns={"id_transporte"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\DadosObitoRepository")
 */
class DadosObito extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_dados_obito", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=true)
     */
    private $dataCadastro;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_local_falecimento", type="string", length=255, nullable=true)
     */
    private $nomeLocalFalecimento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_falecimento", type="date", nullable=true)
     */
    private $dataFalecimento;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_hora_falecimento", type="string", length=45, nullable=true)
     */
    private $horaFalecimento;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_causa_mortis", type="string", length=255, nullable=true)
     */
    private $causaMortis;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_proaim", type="string", length=150, nullable=true)
     */
    private $proaim;

    /**
     * @var string
     *
     * @ORM\Column(name="destino_final", type="string", length=1, nullable=true)
     */
    private $destinoFinal;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_observacao", type="text", nullable=true)
     */
    private $observacao;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_sera_velado", type="boolean", nullable=true)
     */
    private $seraVelado;

    /**
     * @var \Agencia\Entity\Cartorio
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Cartorio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cartorio", referencedColumnName="id_cartorio")
     * })
     */
    private $cartorio;

    /**
     * @var \Agencia\Entity\LocalFalecimento
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\LocalFalecimento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_local_falecimento", referencedColumnName="id_local_falecimento")
     * })
     */
    private $localFalecimento;

    
    /**
     * @var \Agencia\Entity\Falecido
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Falecido")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_falecido", referencedColumnName="id_falecido")
     * })
     */
    
    
    private $falecido;


    /**
     * @var \Agencia\Entity\Transporte
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\Transporte",cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_transporte", referencedColumnName="id_transporte")
     * })
     */
    private $transporte;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Agencia\Entity\Medico", inversedBy="dadosObito")
     * @ORM\JoinTable(name="tb_dados_obito_tb_medico",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_dados_obito", referencedColumnName="id_dados_obito")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_medico", referencedColumnName="id_medico")
     *   }
     * )
     */
    private $medico;
    /**
     * @var string
     *
     * @ORM\Column(name="txt_endereco", type="text", nullable=true)
     */
    private $enderecoFalecimento;
    
    function getEnderecoFalecimento() {
        return $this->enderecoFalecimento;
    }

    function setEnderecoFalecimento($enderecoFalecimento) {
        $this->enderecoFalecimento = $enderecoFalecimento;
    }

        /**
     * Constructor
     */
    public function __construct()
    {
        $this->medico = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param \DateTime $dataCadastro
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
    }

    /**
     * @return string
     */
    public function getNomeLocalFalecimento()
    {
        return $this->nomeLocalFalecimento;
    }

    /**
     * @param string $nomeLocalFalecimento
     */
    public function setNomeLocalFalecimento($nomeLocalFalecimento)
    {
        $this->nomeLocalFalecimento = $nomeLocalFalecimento;
    }

    /**
     * @return \DateTime
     */
    public function getDataFalecimento()
    {
        return $this->dataFalecimento;
    }

    /**
     * @param \DateTime $dataFalecimento
     */
    public function setDataFalecimento($dataFalecimento)
    {
        $this->dataFalecimento = $dataFalecimento;
    }

    /**
     * @return string
     */
    public function getHoraFalecimento()
    {
        return $this->horaFalecimento;
    }

    /**
     * @param string $horaFalecimento
     */
    public function setHoraFalecimento($horaFalecimento)
    {
        $this->horaFalecimento = $horaFalecimento;
    }

    /**
     * @return string
     */
    public function getCausaMortis()
    {
        return $this->causaMortis;
    }

    /**
     * @param string $causaMortis
     */
    public function setCausaMortis($causaMortis)
    {
        $this->causaMortis = $causaMortis;
    }

    /**
     * @return string
     */
    public function getProaim()
    {
        return $this->proaim;
    }

    /**
     * @param string $proaim
     */
    public function setProaim($proaim)
    {
        $this->proaim = $proaim;
    }

    /**
     * @return string
     */
    public function getDestinoFinal()
    {
        return $this->destinoFinal;
    }

    /**
     * @param string $destinoFinal
     */
    public function setDestinoFinal($destinoFinal)
    {
        $this->destinoFinal = $destinoFinal;
    }

    /**
     * @return string
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * @param string $observacao
     */
    public function setObservacao($observacao)
    {
        $this->observacao = $observacao;
    }

    /**
     * @return boolean
     */
    public function isSeraVelado()
    {
        return $this->seraVelado;
    }

    /**
     * @param boolean $seraVelado
     */
    public function setSeraVelado($seraVelado)
    {
        $this->seraVelado = $seraVelado;
    }

    /**
     * @return Cartorio
     */
    public function getCartorio()
    {
        return $this->cartorio;
    }

    /**
     * @param Cartorio $cartorio
     */
    public function setCartorio($cartorio)
    {
        $this->cartorio = $cartorio;
    }

    /**
     * @return LocalFalecimento
     */
    public function getLocalFalecimento()
    {
        return $this->localFalecimento;
    }

    /**
     * @param LocalFalecimento $localFalecimento
     */
    public function setLocalFalecimento($localFalecimento)
    {
        $this->localFalecimento = $localFalecimento;
    }


    /**
     * @return Transporte
     */
    public function getTransporte()
    {
        return $this->transporte;
    }

    /**
     * @param Transporte $transporte
     */
    public function setTransporte($transporte)
    {
        $this->transporte = $transporte;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMedico()
    {
        return $this->medico;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $medico
     */
    public function setMedico($medico)
    {
        $this->medico = $medico;
    }

    /**
     * @return mixed
     */
    function getFalecido() {
        return $this->falecido;
    }

    function setFalecido(\Agencia\Entity\Falecido $falecido) {
        $this->falecido = $falecido;
    }

        public function toArray()
    {
        return [
            'id' => $this->id,
            'dataCadastro' => $this->dataCadastro->format('d/m/Y'),
            'nomeLocalFalecimento' => $this->nomeLocalFalecimento,
            'dataFalecimento' => $this->dataFalecimento->format('d/m/Y'),
            'horaFalecimento' => $this->horaFalecimento,
            'causaMortis' => $this->causaMortis,
            'proaim' => $this->proaim,
            'destinoFinal' => $this->destinoFinal,
            'observacao' => $this->observacao,
            'seraVelado' => $this->seraVelado,
            'cartorio' => $this->cartorio,
            'localFalecimento' => $this->localFalecimento,
            'falecido' => $this->falecido,
            'transporte' => $this->transporte,
            'medico' => $this->medico,
            'enderecoFalecimento'=> $this->enderecoFalecimento
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->nomeLocalFalecimento;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


    /**
     * Get seraVelado
     *
     * @return boolean 
     */
    public function getSeraVelado()
    {
        return $this->seraVelado;
    }

    /**
     * Add medico
     *
     * @param \Agencia\Entity\Medico $medico
     * @return DadosObito
     */
    public function addMedico(\Agencia\Entity\Medico $medico)
    {
        $this->medico[] = $medico;

        return $this;
    }

    /**
     * Remove medico
     *
     * @param \Agencia\Entity\Medico $medico
     */
    public function removeMedico(\Agencia\Entity\Medico $medico)
    {
        $this->medico->removeElement($medico);
    }
}
