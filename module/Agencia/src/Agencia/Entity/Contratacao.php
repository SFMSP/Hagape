<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Contratacao
 *
 * @ORM\Table(name="tb_contratacao", indexes={@ORM\Index(name="fk_tb_venda_tb_tipo_operacao1_idx", columns={"id_tipo_operacao"}), @ORM\Index(name="fk_tb_venda_tb_destino_venda1_idx", columns={"id_tipo_destino_final"}), @ORM\Index(name="fk_tb_venda_tb_tipo_contratacao1_idx", columns={"id_tipo_contratacao"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\ContratacaoRepository")
 */
class Contratacao extends BaseEntity implements EntityInterface
{
    /**
     * Lista de situacao da contratação
     */
    const STATUS_CONCLUIDO = 1;
    const STATUS_PENDENTE = 2;
    const STATUS_CANCELADO = 3;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_contratacao", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=true)
     */
    private $dataCadastro;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_numero_nota", type="string", length=250, nullable=true)
     */
    private $numeroNota;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_usuario", type="integer", nullable=true)
     */
    private $usuario;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_agencia", type="integer", nullable=true)
     */
    private $agencia;

    /**
     * @var string
     *
     * @ORM\Column(name="vl_total_contratacao", type="string", length=45, nullable=true)
     */
    private $totalContratacao;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_contratacao_origem", type="integer", nullable=true)
     */
    private $contratacaoOrigem;

    /**
     * @var \Agencia\Entity\TipoOperacao
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\TipoOperacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipo_operacao", referencedColumnName="id_tipo_operacao")
     * })
     */
    private $tipoOperacao;

    /**
     * @var \Agencia\Entity\TipoDestinoFinal
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\TipoDestinoFinal")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipo_destino_final", referencedColumnName="id_tipo_destino_final")
     * })
     */
    private $tipoDestinoFinal;

    /**
     * @var \Agencia\Entity\TipoContratacao
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\TipoContratacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipo_contratacao", referencedColumnName="id_tipo_contratacao")
     * })
     */
    private $tipoContratacao;
  /**
     * @var integer
     *
     * @ORM\Column(name="situacao", type="integer", nullable=true)
     */
    private $situacao;
    
    function getSituacao() {
        return $this->situacao;
    }

    function setSituacao($situacao) {
        $this->situacao = $situacao;
    }
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param \DateTime $dataCadastro
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
    }

    /**
     * @return string
     */
    public function getNumeroNota()
    {
        return $this->numeroNota;
    }

    /**
     * @param string $numeroNota
     */
    public function setNumeroNota($numeroNota)
    {
        $this->numeroNota = $numeroNota;
    }

    /**
     * @return int
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param int $usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * @return int
     */
    public function getAgencia()
    {
        return $this->agencia;
    }

    /**
     * @param int $agencia
     */
    public function setAgencia($agencia)
    {
        $this->agencia = $agencia;
    }

    /**
     * @return string
     */
    public function getTotalContratacao()
    {
        return $this->totalContratacao;
    }

    /**
     * @param string $totalContratacao
     */
    public function setTotalContratacao($totalContratacao)
    {
        $this->totalContratacao = $totalContratacao;
    }

    /**
     * @return int
     */
    public function getContratacaoOrigem()
    {
        return $this->contratacaoOrigem;
    }

    /**
     * @param int $contratacaoOrigem
     */
    public function setContratacaoOrigem($contratacaoOrigem)
    {
        $this->contratacaoOrigem = $contratacaoOrigem;
    }

    /**
     * @return TipoOperacao
     */
    public function getTipoOperacao()
    {
        return $this->tipoOperacao;
    }

    /**
     * @param TipoOperacao $tipoOperacao
     */
    public function setTipoOperacao($tipoOperacao)
    {
        $this->tipoOperacao = $tipoOperacao;
    }

    /**
     * @return TipoDestinoFinal
     */
    public function getTipoDestinoFinal()
    {
        return $this->tipoDestinoFinal;
    }

    /**
     * @param TipoDestinoFinal $destinoFinal
     */
    public function setTipoDestinoFinal($tipoDestinoFinal)
    {
        $this->tipoDestinoFinal = $tipoDestinoFinal;
    }

    /**
     * @return TipoContratacao
     */
    public function getTipoContratacao()
    {
        return $this->tipoContratacao;
    }

    /**
     * @param TipoContratacao $tipoContratacao
     */
    public function setTipoContratacao($tipoContratacao)
    {
        $this->tipoContratacao = $tipoContratacao;
    }

    public function toArray()
    {
        return [
        
            'id' => $this->id,
            'dataCadastro' => $this->dataCadastro->format('d/m/Y'),
            'numeroNota' => $this->numeroNota,
            'usuario' => $this->usuario,
            'agencia' => $this->agencia,
            'totalContratacao' => $this->totalContratacao,
            'contratacaoOrigem' => $this->contratacaoOrigem,
            'tipoOperacao' => $this->tipoOperacao,
            'tipoDestinoFinal' => $this->tipoDestinoFinal,
            'tipoContratacao' => $this->tipoContratacao,
            'situacao'=> $this->situacao,
        ];
    }

    public function __toString()
    {
        return $this->numeroNota;
    }

    public function getLabel()
    {
        return $this->__toString();
    }


}
