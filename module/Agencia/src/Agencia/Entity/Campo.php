<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;
/**
 * Campo
 *
 * @ORM\Table(name="tb_campo")
 * @ORM\Entity(repositoryClass="Agencia\Repository\CampoRepository")
 */
class Campo extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_campo", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_campo", type="string", length=150, nullable=true)
     */
    private $campo;

    /**
     * @var string
     *
     * @ORM\Column(name="campo_variavel", type="string", length=150, nullable=true)
     */
    private $campoVariavel;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCampo()
    {
        return $this->campo;
    }

    /**
     * @param string $campo
     */
    public function setCampo($campo)
    {
        $this->campo = $campo;
    }

    /**
     * @return string
     */
    public function getCampoVariavel()
    {
        return $this->campoVariavel;
    }

    /**
     * @param string $campoVariavel
     */
    public function setCampoVariavel($campoVariavel)
    {
        $this->campoVariavel = $campoVariavel;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'campo' => $this->campo,
            'campoVariavel' => $this->campoVariavel
        ];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->campo;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
