<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * TipoContratacao
 *
 * @ORM\Table(name="tb_tipo_contratacao")
 * @ORM\Entity(repositoryClass="Agencia\Repository\TipoContratacaoRepository")
 */
class TipoContratacao extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_tipo_contratacao", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_tipo_contratacao", type="string", length=150, nullable=true)
     */
    private $tipoContratacao;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTipoContratacao()
    {
        return $this->tipoContratacao;
    }

    /**
     * @param string $tipoContratacao
     */
    public function setTipoContratacao($tipoContratacao)
    {
        $this->tipoContratacao = $tipoContratacao;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'tipoContratacao' => $this->tipoContratacao
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return (string)$this->id;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
