<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * TipoOperacao
 *
 * @ORM\Table(name="tb_tipo_operacao")
 * @ORM\Entity(repositoryClass="Agencia\Repository\TipoOperacaoRepository")
 */
class TipoOperacao extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_tipo_operacao", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_tipo_operacao", type="string", length=150, nullable=true)
     */
    private $tipoOperacao;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTipoOperacao()
    {
        return $this->tipoOperacao;
    }

    /**
     * @param string $tipoOperacao
     */
    public function setTipoOperacao($tipoOperacao)
    {
        $this->tipoOperacao = $tipoOperacao;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'tipoOperacao' => $this->tipoOperacao
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->tipoOperacao;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
