<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Transporte
 *
 * @ORM\Table(name="tb_transporte")
 * @ORM\Entity(repositoryClass="Agencia\Repository\TransporteRepository")
 */
class Transporte extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_transporte", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="tipo_transporte", type="boolean", nullable=true)
     */
    private $tipoTransporte;

    /**
     * @var boolean
     *
     * @ORM\Column(name="tipo_carro", type="boolean", nullable=true)
     */
    private $tipoCarro;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_local_remocao_falecimento", type="string", length=1, nullable=true)
     */
    private $localRemocaoFalecimento;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_local_remocao", type="string", length=255, nullable=true)
     */
    private $localRemocao;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_endereco", type="string", length=255, nullable=true)
     */
    private $endereco;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return boolean
     */
    public function isTipoTransporte()
    {
        return $this->tipoTransporte;
    }

    /**
     * @param boolean $tipoTransporte
     */
    public function setTipoTransporte($tipoTransporte)
    {
        $this->tipoTransporte = $tipoTransporte;
    }

    /**
     * @return boolean
     */
    public function isTipoCarro()
    {
        return $this->tipoCarro;
    }

    /**
     * @param boolean $tipoCarro
     */
    public function setTipoCarro($tipoCarro)
    {
        $this->tipoCarro = $tipoCarro;
    }

    /**
     * @return string
     */
    public function getLocalRemocaoFalecimento()
    {
        return $this->localRemocaoFalecimento;
    }

    /**
     * @param string $localRemocaoFalecimento
     */
    public function setLocalRemocaoFalecimento($localRemocaoFalecimento)
    {
        $this->localRemocaoFalecimento = $localRemocaoFalecimento;
    }

    /**
     * @return string
     */
    public function getLocalRemocao()
    {
        return $this->localRemocao;
    }

    /**
     * @param string $localRemocao
     */
    public function setLocalRemocao($localRemocao)
    {
        $this->localRemocao = $localRemocao;
    }

    /**
     * @return string
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param string $endereco
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'tipoTransporte' => $this->tipoTransporte,
            'tipoCarro' => $this->tipoCarro,
            'localRemocaoFalecimento' => $this->localRemocaoFalecimento,
            'localRemocao' => $this->localRemocao,
            'endereco' => $this->endereco
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return (string) $this->tipoTransporte;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }



    /**
     * Get tipoTransporte
     *
     * @return boolean 
     */
    public function getTipoTransporte()
    {
        return $this->tipoTransporte;
    }

    /**
     * Get tipoCarro
     *
     * @return boolean 
     */
    public function getTipoCarro()
    {
        return $this->tipoCarro;
    }
}
