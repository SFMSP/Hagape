<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;
/**
 * Medico
 *
 * @ORM\Table(name="tb_medico")
 * @ORM\Entity(repositoryClass="Agencia\Repository\MedicoRepository")
 */
class Medico extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_medico", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome_medico", type="string", length=255, nullable=true)
     */
    private $nomeMedico;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=true)
     */
    private $dataCadastro;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_crm", type="string", length=45, nullable=true)
     */
    private $crm;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Agencia\Entity\DadosAmputacao", mappedBy="medico")
     */
    private $dadosAmaputacao;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Agencia\Entity\DadosObito", mappedBy="medico")
     */
    private $dadosObito;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Agencia\Entity\DadosObitoVelorio", mappedBy="medico")
     */
    private $dadosObitoVelorio;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dadosAmaputacao = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dadosObito = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dadosObitoVelorio = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNomeMedico()
    {
        return $this->nomeMedico;
    }

    /**
     * @param string $nomeMedico
     */
    public function setNomeMedico($nomeMedico)
    {
        $this->nomeMedico = $nomeMedico;
    }

    /**
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * @param \DateTime $dataCadastro
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
    }

    /**
     * @return string
     */
    public function getCrm()
    {
        return $this->crm;
    }

    /**
     * @param string $crm
     */
    public function setCrm($crm)
    {
        $this->crm = $crm;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDadosAmaputacao()
    {
        return $this->dadosAmaputacao;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $dadosAmaputacao
     */
    public function setDadosAmaputacao($dadosAmaputacao)
    {
        $this->dadosAmaputacao = $dadosAmaputacao;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDadosObito()
    {
        return $this->dadosObito;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $dadosObito
     */
    public function setDadosObito($dadosObito)
    {
        $this->dadosObito = $dadosObito;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDadosObitoVelorio()
    {
        return $this->dadosObitoVelorio;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $dadosObitoVelorio
     */
    public function setDadosObitoVelorio($dadosObitoVelorio)
    {
        $this->dadosObitoVelorio = $dadosObitoVelorio;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return[
            'id' => $this->id,
            'nomeMedico' => $this->nomeMedico,
            'dataCadastro' => $this->dataCadastro,
            'crm' => $this->crm,
            'dadosAmaputacao' => $this->dadosAmaputacao,
            'dadosObito' => $this->dadosObito,
            'dadosObitoVelorio' => $this->dadosObitoVelorio
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->nomeMedico;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


    /**
     * Add dadosAmaputacao
     *
     * @param \Agencia\Entity\DadosAmputacao $dadosAmaputacao
     * @return Medico
     */
    public function addDadosAmaputacao(\Agencia\Entity\DadosAmputacao $dadosAmaputacao)
    {
        $this->dadosAmaputacao[] = $dadosAmaputacao;

        return $this;
    }

    /**
     * Remove dadosAmaputacao
     *
     * @param \Agencia\Entity\DadosAmputacao $dadosAmaputacao
     */
    public function removeDadosAmaputacao(\Agencia\Entity\DadosAmputacao $dadosAmaputacao)
    {
        $this->dadosAmaputacao->removeElement($dadosAmaputacao);
    }

    /**
     * Add dadosObito
     *
     * @param \Agencia\Entity\DadosObito $dadosObito
     * @return Medico
     */
    public function addDadosObito(\Agencia\Entity\DadosObito $dadosObito)
    {
        $this->dadosObito[] = $dadosObito;

        return $this;
    }

    /**
     * Remove dadosObito
     *
     * @param \Agencia\Entity\DadosObito $dadosObito
     */
    public function removeDadosObito(\Agencia\Entity\DadosObito $dadosObito)
    {
        $this->dadosObito->removeElement($dadosObito);
    }

    /**
     * Add dadosObitoVelorio
     *
     * @param \Agencia\Entity\DadosObitoVelorio $dadosObitoVelorio
     * @return Medico
     */
    public function addDadosObitoVelorio(\Agencia\Entity\DadosObitoVelorio $dadosObitoVelorio)
    {
        $this->dadosObitoVelorio[] = $dadosObitoVelorio;

        return $this;
    }

    /**
     * Remove dadosObitoVelorio
     *
     * @param \Agencia\Entity\DadosObitoVelorio $dadosObitoVelorio
     */
    public function removeDadosObitoVelorio(\Agencia\Entity\DadosObitoVelorio $dadosObitoVelorio)
    {
        $this->dadosObitoVelorio->removeElement($dadosObitoVelorio);
    }
}
