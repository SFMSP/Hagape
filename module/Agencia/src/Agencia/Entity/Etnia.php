<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Etnia
 *
 * @ORM\Table(name="tb_etnia")
 * @ORM\Entity(repositoryClass="Agencia\Repository\EtniaRepository")
 */
class Etnia extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_etnia", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_etnia", type="string", length=45, nullable=false)
     */
    private $etnia;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEtnia()
    {
        return $this->etnia;
    }

    /**
     * @param string $etnia
     */
    public function setEtnia($etnia)
    {
        $this->etnia = $etnia;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'etnia' => $this->etnia
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->etnia;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
