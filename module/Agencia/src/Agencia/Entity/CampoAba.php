<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;
/**
 * CampoAba
 *
 * @ORM\Table(name="tb_campo_tb_aba", indexes={@ORM\Index(name="fk_tb_campo_has_tb_aba_tb_aba1_idx", columns={"id_aba"}), @ORM\Index(name="fk_tb_campo_has_tb_aba_tb_campo1_idx", columns={"id_campo"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\CampoAbaRepository")
 */
class CampoAba extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_aba_campo", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var \Agencia\Entity\Campo
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Agencia\Entity\Campo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_campo", referencedColumnName="id_campo")
     * })
     */
    private $campo;

    /**
     * @var \Agencia\Entity\Aba
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Agencia\Entity\Aba")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_aba", referencedColumnName="id_aba")
     * })
     */
    private $aba;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Campo
     */
    public function getCampo()
    {
        return $this->campo;
    }

    /**
     * @param Campo $campo
     */
    public function setCampo($campo)
    {
        $this->campo = $campo;
    }

    /**
     * @return Aba
     */
    public function getAba()
    {
        return $this->aba;
    }

    /**
     * @param Aba $aba
     */
    public function setAba($aba)
    {
        $this->aba = $aba;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return[
            'id' => $this->id,
            'campo' => $this->campo,
            'aba' => $this->aba
        ];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->id;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
