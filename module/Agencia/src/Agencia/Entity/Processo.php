<?php

namespace Agencia\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Processo
 *
 * @ORM\Table(name="tb_processo", indexes={@ORM\Index(name="fk_tb_processo_tb_tipo_operacao1_idx", columns={"id_tipo_operacao"}), @ORM\Index(name="fk_tb_processo_tb_destino_final1_idx", columns={"id_tipo_destino_final"})})
 * @ORM\Entity(repositoryClass="Agencia\Repository\ProcessoRepository")
 */
class Processo extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_processo", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_processo", type="string", length=150, nullable=true)
     */
    private $processo;
    
    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome_template", type="string", length=150, nullable=true)
     */
    private $template;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_tipo_contratacao", type="integer", nullable=true)
     */
    private $tipoContratacao;

    /**
     * @var \Agencia\Entity\TipoOperacao
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\TipoOperacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipo_operacao", referencedColumnName="id_tipo_operacao")
     * })
     */
    private $tipoOperacao;

    /**
     * @var \Agencia\Entity\TipoDestinoFinal
     *
     * @ORM\ManyToOne(targetEntity="Agencia\Entity\TipoDestinoFinal")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipo_destino_final", referencedColumnName="id_tipo_destino_final")
     * })
     */
    private $tipoDestinoFinal;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getProcesso()
    {
        return $this->processo;
    }

    /**
     * @param string $processo
     */
    public function setProcesso($processo)
    {
        $this->processo = $processo;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param string $processo
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }
    
    /**
     * @return int
     */
    public function getTipoContratacao()
    {
        return $this->tipoContratacao;
    }

    /**
     * @param int $tipoContratacao
     */
    public function setTipoContratacao($tipoContratacao)
    {
        $this->tipoContratacao = $tipoContratacao;
    }

    /**
     * @return TipoOperacao
     */
    public function getTipoOperacao()
    {
        return $this->tipoOperacao;
    }

    /**
     * @param TipoOperacao $tipoOperacao
     */
    public function setTipoOperacao($tipoOperacao)
    {
        $this->tipoOperacao = $tipoOperacao;
    }

    /**
     * @return TbTipoDestinoFinal
     */
    public function getTipoDestinoFinal()
    {
        return $this->tipoDestinoFinal;
    }

    /**
     * @param TbTipoDestinoFinal $tipoDestinoFinal
     */
    public function setTipoDestinoFinal($tipoDestinoFinal)
    {
        $this->tipoDestinoFinal = $tipoDestinoFinal;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'processo' => $this->processo,
            'tipoContratacao' => $this->tipoContratacao,
            'tipoOperacao' => $this->tipoOperacao,
            'tipoDestinoFinal' => $this->tipoDestinoFinal
        ];
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->processo;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->__toString();
    }


}
