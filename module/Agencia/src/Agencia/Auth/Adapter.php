<?php

namespace Agencia\Auth;

use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;

/**
 * Classe responsável pela autenticação de usuários
 *
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Adapter implements AdapterInterface {

    /**
     *
     * @var  \Application\Repository\UsuarioRepository
     */
    protected $usuarioReposity;

    /**
     *
     * @var \Application\Repository\PrivilegioRepository
     */
    protected $privilegioRepository;

    /**
     *
     * @var \Application\Service\LogLogin
     */
    protected $serviceLogLogin;

    /**
     *
     * @var string 
     */
    protected $username;

    /**
     *
     * @var string
     */
    protected $password;

    /**
     * 
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(\Application\Repository\UsuarioRepository $usuarioRepository, \Application\Repository\PrivilegioRepository $privilegioRepository, \Application\Service\LogLogin $logLogin) {
        $this->usuarioReposity = $usuarioRepository;
        $this->privilegioRepository = $privilegioRepository;
        $this->serviceLogLogin = $logLogin;
    }

    /**
     * Método que seta o usuário
     * @param string $username
     */
    public function setUsername($username) {
        $this->username = $username;
        return $this;
    }

    /**
     * Método que seta a senha
     * @param string $password
     */
    public function setPassword($password) {
        $this->password = $password;
        return $this;
    }

    /**
     * Método que realiza a autenticação
     * @return \Zend\Authentication\Result
     */
    public function authenticate() {
        //Instancia o repository
        $repository = $this->usuarioReposity;
        $auth = $repository->autenticar($this->username, $this->password);

        //Realiza a autenticação
        if ($auth) {
            //Registra o log de login
            $this->serviceLogLogin->save(array('ip' => $_SERVER['REMOTE_ADDR'], 'usuario' => $auth));

            //Busca os privilégios
            $privilegios = $this->getPrivilegios($auth->getPerfil());

            //Busca os filtros que o usuário possui
            $filtros = $this->privilegioRepository->getFiltrosPerfil($auth->getPerfil()->getId());

            $arrFiltros = array();

            foreach ($filtros as $filt) {
                $arrFiltros[] = $filt['filtro'];
            }



            //Monta o array com os dados do usuário
            $dadosUser = array(
                'idUsuario' => $auth->getId(),
                'usuarioNome' => $auth->getNome(),
                'usuarioEmail' => $auth->getEmail(),
                'login' => $auth->getLogin(),
                'avatar' => $auth->getAvatar(),
                'perfil' => $auth->getPerfil()->getNome(),
                'trocaSenha' => $auth->getTrocaSenha(),
                'privilegios' => $privilegios,
                'possuiFiltros' => $arrFiltros ? true : false,
                'filtros' => $arrFiltros,
                'filtrosSelecionados' => array(),
            );


            //Preenche a sessão com os ids dos filtros que o usuário possuí permissão
            $dadosUser['agencias'] = array();
            $dadosUser['cemiterios'] = array();
            $dadosUser['estoques'] = array();
            $dadosUser['trafegos'] = array();
            $dadosUser['tesourarias'] = array();
            
            foreach ($auth->getAgencia() as $key => $agencia) {
                $dadosUser['agencias'][$key] = $agencia->getId();
            }

            foreach ($auth->getCemiterio() as $key => $cemiterio) {
                $dadosUser['cemiterios'][$key] = $cemiterio->getId();
            }

            foreach ($auth->getEstoque() as $key => $estoque) {
                $dadosUser['estoques'][$key] = $estoque->getId();
            }

            foreach ($auth->getTrafego() as $key => $trafego) {
                $dadosUser['trafegos'][$key] = $trafego->getId();
            }

            foreach ($auth->getTesouraria() as $key => $tesouraria) {
                $dadosUser['tesourarias'][$key] = $tesouraria->getId();
            }

            return new Result(Result::SUCCESS, $dadosUser, array('OK'));
        } else {
            return new Result(Result::FAILURE_CREDENTIAL_INVALID, null, array());
        }
    }

    /**
     * 
     * @param \Application\Entity\Perfil $perfil
     */
    public function getPrivilegios(\Application\Entity\Perfil $perfil) {
        //Cria o array qeu receberá os privilegios gerais e do perfil
        $privilegios = array();
        $privilegiosUser = array();

        //Monta o array de permissões do usuário
        foreach ($perfil->getPrivilegios() as $privilegio) {
            $privilegiosUser[$privilegio->getFuncionalidade()->getController()][$privilegio->getAcao()->getAction()] = true;
        }

        //Monta o array com todos os privilégios
        foreach ($this->privilegioRepository->findAll() as $privilegio) {
            if (isset($privilegiosUser[$privilegio->getFuncionalidade()->getController()][$privilegio->getAcao()->getAction()])) {
                $privilegios[$privilegio->getFuncionalidade()->getController()][$privilegio->getAcao()->getAction()] = true;
            } else {
                $privilegios[$privilegio->getFuncionalidade()->getController()][$privilegio->getAcao()->getAction()] = false;
            }
        }

        //Retorna a relação de privilégios
        return $privilegios;
    }

}
