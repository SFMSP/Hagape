<?php

namespace Agencia\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;

class ServicoRepository extends BaseRepository implements RepositoryInterface
{
    public function buscaServicoCategoria($idCategoria = '', $idServico = '')
    {
        if ($idCategoria) {
            $stSql = " 
                      SELECT 
                            tb_servico.id_servico AS id
                            ,tb_servico.txt_nome AS nome
                            ,tb_servico.valor AS valor                        
						    ,COALESCE(tb_robin_hood.fator,1) AS fator
                      FROM tb_servico                    
					  INNER JOIN tb_robin_hood
					    ON tb_robin_hood.id_servico = tb_servico.id_servico                  
                      WHERE tb_servico.id_categoria = " . $idCategoria . "
            ";
            if ($idServico) {
                $stSql .= "\n AND tb_servico.id_servico = " . $idServico . " ";
            }
            try {
                $connection = $this->getEntityManager()->getConnection();
                if ($connection) {
                    $stmt = $connection->query($stSql);
                    //Percorre e organiza os resultados
                    return $stmt->fetchAll();
                }

            } catch (\Exception $ex) {
                $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            }
        } else {
            return [];
        }
    }

    public function buscaDadosServico($idServico = '')
    {
        if ($idServico) {
            $stSql = " 
                      SELECT 
                            tb_servico.txt_nome AS nome
                            ,tb_servico.valor AS valor                        
						    ,COALESCE(tb_robin_hood.fator,1) AS fator
                      FROM tb_servico                    
					  INNER JOIN tb_robin_hood
					    ON tb_robin_hood.id_servico = tb_servico.id_servico                  
                      WHERE tb_servico.id_servico = " . $idServico . "
            ";
            try {
                $connection = $this->getEntityManager()->getConnection();
                if ($connection) {
                    $stmt = $connection->query($stSql);
                    //Percorre e organiza os resultados
                    return $stmt->fetchAll();
                }

            } catch (\Exception $ex) {
                $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            }
        } else {
            return [];
        }
    }

}//END OF CLASS
