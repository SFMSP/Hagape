<?php

namespace Agencia\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;
class VelorioRepository extends BaseRepository implements RepositoryInterface
{
    public function busca($filtros, $limit, $offset, $order = array(), $total = false) {
         try {
            $parametros = array();
           
           
            if (!empty($filtros['busca'])) {
                
                $dql = "SELECT m FROM Agencia\Entity\Velorio m  WHERE 1 = 1";
                
                $dql .= " AND (LOWER(m.localVelorio) LIKE :busca ) ";
                      
                $parametros['busca'] = '%' . $filtros['busca'] . '%';
            }
           
            if (count($order)) {
                $dql .= " ORDER BY m.{$order['field']} {$order['order']} ";
            }
            
        
            //Retorna os dados para a busca ou o total encontrado de acordo com os filtros
            if ($total) {
                $result = $this->getEntityManager()->createQuery($dql)->setParameters($parametros)->getResult();
                return $result[0]['total'];
            } else {
                //Cria e executa query
                $query = $this->getEntityManager()->createQuery($dql);
                $query->setMaxResults($limit);
                $query->setFirstResult($offset);
                $query->setParameters($parametros);
                return $query->getResult();
            }
                
          
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }
    public function buscaArrayInputFilter() {
         try {
            
             $dql = "SELECT txt_local_velorio FROM tb_velorio   WHERE 1 = 1";

            $connection = $this->getEntityManager()->getConnection();

            if ($connection) {
            
             $stmt = $connection->query($dql);
                
            $local ="";
            foreach ($stmt->fetchAll() as $localvelorio) {
                
               $local.="'".$localvelorio['txt_local_velorio']."',";
                
            }
            $local = substr($local,0,-1);
            
                return "[".$local."]";
            }

        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }
}
