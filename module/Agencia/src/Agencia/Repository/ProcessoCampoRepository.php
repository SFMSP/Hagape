<?php

namespace Agencia\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;

class ProcessoCampoRepository extends BaseRepository implements RepositoryInterface
{
    public function recuperaObrigatoriedadeCamposProcesso($idAba, $tipoOperacao, $tipoContratacao, $tipoDestinoFinal)
    {
        $stSql = " SELECT DISTINCT 
                        tb_campo.txt_campo AS nomeCampo
                        ,tb_campo.campo_variavel AS campoVariavel
                        ,tb_processo_campo.bool_obrigatorio AS obrigatorio
                    FROM sfmsp.tb_campo_tb_aba 
                    JOIN tb_campo
                      ON tb_campo.id_campo = tb_campo_tb_aba .id_campo
                    JOIN tb_processo_campo
                      ON tb_processo_campo.id_campo = tb_campo.id_campo
                    JOIN tb_processo
                      ON tb_processo.id_processo = tb_processo_campo.id_processo
                    WHERE id_aba = " . $idAba . "                    
                    AND tb_processo.id_tipo_operacao = ". $tipoOperacao;
        
         //Caso for Registro de Obito
        if ($tipoContratacao) {
            $stSql .= " AND tb_processo.id_tipo_contratacao = " . $tipoContratacao;
        }

        //Caso for Registro de Obito
        if ($tipoDestinoFinal) {
            $stSql .= " AND tb_processo.id_tipo_destino_final = " . $tipoDestinoFinal;
        }

        $stSql .= " ORDER BY tb_campo.id_campo";
       //print_r($stSql);die;
        try {
            $connection = $this->getEntityManager()->getConnection();
            if ($connection) {
                $stmt = $connection->query($stSql);
                if ($stmt) {
                    return $stmt->fetchAll();
                }
            }
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }

}
