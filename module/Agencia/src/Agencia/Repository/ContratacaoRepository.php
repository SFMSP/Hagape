<?php

namespace Agencia\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;

class ContratacaoRepository extends BaseRepository implements RepositoryInterface
{
    public function recuperaNumeroSequencialNota($ano = '')
    {

        $stSql = "SELECT LPAD(count(id_contratacao)+1,5,'0') AS proximoCodigo
                    FROM tb_contratacao
                    WHERE 1=1
                    ";
        if ($ano) {
            $stSql .= "AND DATE_FORMAT(dt_cadastro,'y') = '" . $ano . "' ";
        }

        try {
            $connection = $this->getEntityManager()->getConnection();
            if ($connection) {
                $stmt = $connection->query($stSql);
                if ($stmt) {
                    $proximoCodigo = $stmt->fetch();
                    $proximoCodigo = $proximoCodigo['proximoCodigo'];
                    return $proximoCodigo;
                }
            }
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }

    public function busca($filtros, $limit, $offset, $order = array(), $total = false)
    {
        try {

            if ($total) {
                $dql = "SELECT count(*) as total FROM tb_contratacao WHERE 1 = 1 ";

            } else {

                $dql = "SELECT 
                        tb_contratacao.id_contratacao AS id,
                        tb_contratacao.txt_numero_nota AS numeroNota,
                        tb_contratante.txt_nome_contratante AS nomeContratante,
                        tb_falecido.txt_nome AS nome,
                        tb_contratacao.dt_cadastro AS dataCadastro,
                        tb_agencia.txt_nome AS agencia,
                        tb_usuario.txt_nome AS user,
                        tb_contratacao.vl_total_contratacao AS totalContratacao,
                        tb_contratacao.situacao
                         FROM tb_contratacao 
                         LEFT JOIN tb_contratante ON tb_contratante.id_contratacao = tb_contratacao.id_contratacao 
                         LEFT JOIN tb_falecido ON tb_contratante.id_contratacao =tb_contratacao.id_contratacao
                         LEFT JOIN tb_agencia ON tb_contratacao.id_agencia = tb_agencia.id_agencia
                         LEFT JOIN tb_usuario ON tb_contratacao.id_usuario = tb_usuario.id_usuario WHERE 1 = 1 ";

            }


            if (!empty($filtros['agencia'])) {
                $dql .= " AND tb_agencia.id_agencia = " . $filtros['agencia'];

            }
            if (!empty($filtros['filtroData'])) {

                switch ($filtros['filtroData']) {
                    case 'dia':
                        $dql .= " AND tb_contratacao.dt_cadastro like '" . date("Y-m-d") . "%'";
                        break;
                    case 'semana':
                        $dql .= " AND tb_contratacao.dt_cadastro BETWEEN CURRENT_DATE()-7 AND CURRENT_DATE()";
                        break;
                    case 'mes':
                        $dql .= " AND tb_contratacao.dt_cadastro BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()";
                        break;
                    case 'todos':
                        $dql .= "";
                        break;
                }


            }

            $connection = $this->getEntityManager()->getConnection();
            
            if ($connection) {
                $stmt = $connection->query($dql);
                return $stmt->fetchAll();
            }

        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }

    public function notasComplementares($nota)
    {
        try {


            $dql = "SELECT tb_contratacao.txt_numero_nota,
                    tb_nota_complementar.dt_contratacao_complementar,
                    tb_produto.nu_preco_medio_atual
                    FROM tb_nota_complementar 
                    LEFT JOIN tb_contratacao ON tb_contratacao.id_contratacao = tb_nota_complementar.id_contratacao
                    LEFT JOIN tb_produto ON tb_produto.id_produto = tb_nota_complementar.id_produto
                    WHERE tb_nota_complementar.id_contratacao=" . $nota;


            $connection = $this->getEntityManager()->getConnection();

            if ($connection) {
                $stmt = $connection->query($dql);
                return $stmt->fetchAll();
            }

        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }
    
    public function buscarTemplate($id){
        try {

//            $dql = "SELECT p.txt_nome_template
//                    FROM tb_contratacao c                         
//                    INNER JOIN tb_processo p on (
//                    p.id_tipo_operacao = c.id_tipo_operacao AND
//                    (p.id_tipo_destino_final = c.id_tipo_destino_final OR p.id_tipo_contratacao IS NULL) AND
//                    (p.id_tipo_contratacao = c.id_tipo_contratacao OR p.id_tipo_contratacao IS NULL))
//                    WHERE c.id_contratacao =" . $id;
            
            $dql = "
                SELECT p.txt_nome_template 
                FROM tb_contratacao c 
                INNER JOIN tb_processo p 
                WHERE c.id_contratacao = ".$id." AND
                p.id_tipo_operacao = c.id_tipo_operacao AND
                (p.id_tipo_destino_final = c.id_tipo_destino_final or c.id_tipo_destino_final IS NULL ) AND
                (p.id_tipo_contratacao = c.id_tipo_contratacao or c.id_tipo_contratacao IS NULL )";
            $connection = $this->getEntityManager()->getConnection();

            if ($connection) {
                $stmt = $connection->query($dql);
                return $stmt->fetchAll();
            }

        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }
        
    public function buscarDadosNotas($id){
        //Cria a query
        $qb = $this->createQueryBuilder('v');
        $qb->where('v.id = :id')
                    ->setParameter('id', $id);
//        $qb->andWhere('v.numeroNota = :numeroNota')
//                    ->setParameter('numeroNota', "9-00001/2016");

        $result = $qb->getQuery()->getResult();
        if($result[0]){
            $result = $result[0];
        }
        
        return $result;
    }

}