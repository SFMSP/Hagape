<?php

namespace Agencia\Repository;
use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;
class MedicoRepository extends BaseRepository implements RepositoryInterface
{
    
    
    public function busca($filtros, $limit, $offset, $order = array(), $total = false) {
        try {
            $parametros = array();
           
           
                $dql = "SELECT m FROM Agencia\Entity\Medico m  WHERE 1 = 1";
           
            $parametros = array();

            //Aplica os filtros
            if (!empty($filtros['busca'])) {
                $dql .= " AND (LOWER(m.nomeMedico) = :busca)";
                $parametros['busca'] = $filtros['busca'];
            }
            
                //Cria e executa query
                $query = $this->getEntityManager()->createQuery($dql);
                $query->setParameters($parametros);
                return $query->getResult();
           
                
          
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }
    public function buscaArrayInputFilter() {
         try {
            
             $dql = "SELECT txt_nome_medico
                    FROM tb_medico
                    WHERE 1 = 1";


            $connection = $this->getEntityManager()->getConnection();

            if ($connection) {
            
                $stmt = $connection->query($dql);
                
            $med ="";
            foreach ($stmt->fetchAll() as $medico) {
                
               $med.="'".$medico['txt_nome_medico']."',";
                
            }
            $med = substr($med,0,-1);
            
                return "[".$med."]";
            }

        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }
}
