<?php

namespace Agencia\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;
class LocalFalecimentoRepository extends BaseRepository implements RepositoryInterface
{
    public function busca($filtros, $limit, $offset, $order = array(), $total = false) {
         try {
            $parametros = array();
           
           
            if (!empty($filtros['busca'])) {
                
                $dql = "SELECT m FROM Agencia\Entity\LocalFalecimento m  WHERE 1 = 1";
                
                $dql .= " AND (LOWER(m.local) LIKE :busca ) ";
                      
                $parametros['busca'] = '%' . $filtros['busca'] . '%';
            }
           
            if (count($order)) {
                $dql .= " ORDER BY m.{$order['field']} {$order['order']} ";
            }
            
        
            //Retorna os dados para a busca ou o total encontrado de acordo com os filtros
            if ($total) {
                $result = $this->getEntityManager()->createQuery($dql)->setParameters($parametros)->getResult();
                return $result[0]['total'];
            } else {
                //Cria e executa query
                $query = $this->getEntityManager()->createQuery($dql);
                $query->setMaxResults($limit);
                $query->setFirstResult($offset);
                $query->setParameters($parametros);
                return $query->getResult();
            }
                
          
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }
    public function buscaArrayInputFilter() {
         try {
            
             $dql = "SELECT txt_local FROM tb_local_falecimento   WHERE 1 = 1";

            $connection = $this->getEntityManager()->getConnection();

            if ($connection) {
            
             $stmt = $connection->query($dql);
                
            $local ="";
            foreach ($stmt->fetchAll() as $localFalecimento) {
                
               $local.="'".$localFalecimento['txt_local']."',";
                
            }
            $local = substr($local,0,-1);
            
                return "[".$local."]";
            }

        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }
    public function buscaLocal($filtro){
        try {
            
             $dql = "SELECT txt_local
                       FROM tb_local_falecimento
                      WHERE txt_local like'%".$filtro."'";

            
            $connection = $this->getEntityManager()->getConnection();

            if ($connection) {
            
                $stmt = $connection->query($dql);
                
            $res ="";
            foreach ($stmt->fetchAll() as $var) {
                
               $res.="'".$var['txt_nome']."',";
                
            }
            $res = substr($res,0,-1);
            
                return "[".$res."]";
            }

        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }
}
