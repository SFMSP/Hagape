<?php

namespace Estoque;

return array(
    'router' => array(
        'routes' => array(
            'estoque' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/estoque',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Estoque\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/[:controller[/:action][/:id]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '\d+'
                            ),
                            'defaults' => array(),
                        )
                    )
                )
            )
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Estoque\Controller\Index' => 'Estoque\Controller\IndexController',
            'Estoque\Controller\Fornecedor' => 'Estoque\Controller\FornecedorController',
            'Estoque\Controller\Contrato' => 'Estoque\Controller\ContratoController',
            'Estoque\Controller\Pedido' => 'Estoque\Controller\PedidoController',
            'Estoque\Controller\Descarte' => 'Estoque\Controller\DescarteController',
            'Estoque\Controller\Recebimento' => 'Estoque\Controller\RecebimentoController',
            'Estoque\Controller\Transferencia' => 'Estoque\Controller\TransferenciaController',
            'Estoque\Controller\FechamentoPeriodo' => 'Estoque\Controller\FechamentoPeriodoController',
            'Estoque\Controller\RecebimentoTransferencia' => 'Estoque\Controller\RecebimentoTransferenciaController',
            'Estoque\Controller\RecebimentoDescarte' => 'Estoque\Controller\RecebimentoDescarteController'
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Estoque' => __DIR__ . '/../view',
        )
    ),
    'pedido' => array(
        'atestado' => array(
            'titulo' => 'Serviço Funerário do Município de São Paulo',
            'atestante' => 'Jorge Soares',
            'cargo' => 'Chefe E.P.R.',
        )
    ),
    'date_month' => array(
        1 => 'Janeiro',
        2 => 'Fevereiro',
        3 => 'Março',
        4 => 'Abril',
        5 => 'Maio',
        6 => 'Junho',
        7 => 'Julho',
        8 => 'Agosto',
        9 => 'Setembro',
        10 => 'Outubro',
        11 => 'Novembro',
        12 => 'Dezembro'
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
                'home' => array(
                    'options' => array(
                        'route' => 'home',
                        'defaults' => array(
                            '__NAMESPACE__' => 'Application\Controller',
                            'controller' => 'Index',
                            'action' => 'index'
                        )
                    ),
                ),
                'fechamentoPeriodo' => array(
                    'options' => array(
                        'route' => 'fechamentoPeriodo',
                        'defaults' => array(
                            'controller' => 'Estoque\Controller\FechamentoPeriodo',
                            'action' => 'fechamentoPeriodo'
                        )
                    )
                )
            ),
        ),
    )
);
