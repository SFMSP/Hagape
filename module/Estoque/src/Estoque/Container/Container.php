<?php

namespace Estoque\Container;

use Zend\Session\SessionManager;
use Base\Container\BaseContainer as BaseContainerInterface;

/**
 * Classe que armazena todas as dependências para serem chamados na classe Module
 *
 * @author Praxedes
 */
class Container implements BaseContainerInterface
{

    /**
     *
     * Retorna as instâncias dos repositórios
     * @return array
     */
    public function getForms()
    {
        return array(
            'Estoque\Form\Contrato' => function ($sm) {
                $t = new \Estoque\Entity\Contrato();
                $inputFilter = new \Estoque\InputFilter\Contrato();
                $form = new \Estoque\Form\Contrato($inputFilter);
                $form->setContratoRepository($sm->get('Estoque\Repository\ContratoRepository'));
                return $form;
            },
            'Estoque\Form\Fornecedor' => function ($sm) {
                $estados = $sm->get('Application\Repository\UfRepository')->getArraySelect();
                $cidades = $sm->get('Application\Repository\CidadeRepository')->getArraySelect();
                             
                //$cidades = array('' => 'Escolha uma Cidade');
                $t = new \Estoque\Entity\Fornecedor();
                $inputFilter = new \Estoque\InputFilter\Fornecedor();
                $form = new \Estoque\Form\Fornecedor($inputFilter, $estados, $cidades);
                $form->setFornecedorRepository($sm->get('Estoque\Repository\FornecedorRepository'));
               // $form->setData(array('estado' => 26));
                             
               // return new \Estoque\Form\Fornecedor($inputFilter);
                 return $form;
            },
            'Estoque\Form\Pedido' => function ($sm) {
                $fornecedores = $sm->get('Estoque\Repository\FornecedorRepository')->getArraySelect('Selecione um Fornecedor',array('0'=>'nome','1'=>'asc'));
                $contratos = array('' => 'Selecione um Contrato');
                $empenhos = array('' => 'Selecione um Empenho');
                $produtos = $sm->get('Estoque\Repository\ProdutoRepository')->getArraySelect('Selecione um Item');
                $form = new \Estoque\Form\Pedido(new \Estoque\InputFilter\Pedido(), $fornecedores, $contratos, $empenhos, $produtos);
                $form->get('numeroPedido')->setValue($sm->get('Estoque\Repository\PedidoRepository')->getNumeroPedidoDisponivel());
                
                return $form;
            },
            'Estoque\Form\PedidoFiltro' => function ($sm) {
                $situacaoPedido = $sm->get('Estoque\Repository\SituacaoPedidoRepository');
                return new \Estoque\Form\PedidoFiltro($situacaoPedido->getArraySelect("Situação"));
            },
            'Estoque\Form\Recebimento' => function ($sm) {
                $form =  new \Estoque\Form\Recebimento(new \Estoque\InputFilter\Recebimento());
                $form->setItemPedidoRepository($sm->get('Estoque\Repository\ItemPedidoRepository'));
                                
                return $form;
            },
            'Estoque\Form\Transferencia' => function ($sm) {
                $dadosUser = $sm->get('DadosUser');
                $estoques = $sm->get('Admin\Repository\EstoqueRepository')->getArraySelect("Selecione um Estoque");
                $produtos = $sm->get('Estoque\Repository\EstoqueProdutoRepository')->getProdutosEstoque($dadosUser['filtrosSelecionados']['estoque']['id']);
                $produtos = $sm->get('Estoque\Repository\ProdutoRepository')->arrayObjectToSelect($produtos, "Selecione um Produto");
                unset($estoques[$dadosUser['filtrosSelecionados']['estoque']['id']]);
                
                $form = new \Estoque\Form\Transferencia(new \Estoque\InputFilter\Transferencia(), $estoques, $produtos);
                $form->setEstoqueProdutoRepository($sm->get('Estoque\Repository\EstoqueProdutoRepository'))
                     ->setProdutoRepository($sm->get('Estoque\Repository\ProdutoRepository'))
                     ->setDadosUser($sm->get('DadosUser'))
                     ->setItemTransferenciaRepository($sm->get('Estoque\Repository\ItemTransferenciaRepository'))
                     ->get('numeroTransferencia')->setValue($sm->get('Estoque\Repository\PedidoTransferenciaRepository')->getNumeroPedidoTransferenciaDisponivel());
                
                return $form;
            },
            'Estoque\Form\Descarte' => function ($sm) {
                $dadosUser = $sm->get('DadosUser');
                $estoques = $sm->get('Admin\Repository\EstoqueRepository')->getArraySelect("Selecione um Estoque");
                $produtos = $sm->get('Estoque\Repository\EstoqueProdutoRepository')->getProdutosEstoque($dadosUser['filtrosSelecionados']['estoque']['id']);
                $produtos = $sm->get('Estoque\Repository\ProdutoRepository')->arrayObjectToSelect($produtos, "Selecione um Produto");
                unset($estoques[$dadosUser['filtrosSelecionados']['estoque']['id']]);
                
                $form = new \Estoque\Form\Descarte(new \Estoque\InputFilter\Descarte(), $estoques, $produtos);
                $form->setEstoqueProdutoRepository($sm->get('Estoque\Repository\EstoqueProdutoRepository'))
                     ->setProdutoRepository($sm->get('Estoque\Repository\ProdutoRepository'))
                     ->setDadosUser($sm->get('DadosUser'))
                     ->setItemDescarteRepository($sm->get('Estoque\Repository\ItemDescarteRepository'))
                     ->get('numeroDescarte')->setValue($sm->get('Estoque\Repository\SolicitacaoDescarteRepository')->getNumeroDescarteDisponivel());
                
                return $form;
            },
            'Estoque\Form\TransferenciaFiltro' => function ($sm) {
                $situacaoTransferencia = $sm->get('Estoque\Repository\SituacaoTransferenciaRepository');
                return new \Estoque\Form\TransferenciaFiltro($situacaoTransferencia->getArraySelect("Situação"));
            },
            'Estoque\Form\DescarteFiltro' => function ($sm) {
                $situacaoDescarte = $sm->get('Estoque\Repository\SituacaoDescarteRepository');
                return new \Estoque\Form\DescarteFiltro($situacaoDescarte->getArraySelect("Situação"));
            },
             'Estoque\Form\RecebimentoTransferencia' => function ($sm) {
                $form =  new \Estoque\Form\RecebimentoTransferencia(new \Estoque\InputFilter\RecebimentoTransferencia());
                $form->setItemTransferenciaRepository($sm->get('Estoque\Repository\ItemTransferenciaRepository'));
                
                return $form;
            },
            'Estoque\Form\Estorno' => function ($sm) {
                return new \Estoque\Form\Estorno();
            },
            'Estoque\Form\EstornoDescarte' => function ($sm) {
                return new \Estoque\Form\EstornoDescarte();
            },
            'Estoque\Form\RecebimentoDescarte' => function ($sm){
                $form = new \Estoque\Form\RecebimentoDescarte(new \Estoque\InputFilter\RecebimentoDescarte());
                $form->setItemDescarteRepository($sm->get('Estoque\Repository\ItemDescarteRepository'));
                
                return $form;
            }
        );
    }

    /**
     *
     * Retorna os services com as instâncias dos repositories
     * @return array
     */
    public function getRepositories()
    {
        return array(
            'Estoque\Repository\ContratoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Estoque\Entity\Contrato');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Estoque\Repository\FornecedorRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Estoque\Entity\Fornecedor');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Estoque\Repository\PedidoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Estoque\Entity\Pedido');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Estoque\Repository\EmpenhoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Estoque\Entity\Empenho');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Estoque\Repository\ProdutoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Estoque\Entity\Produto');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Estoque\Repository\EstoqueProdutoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Estoque\Entity\EstoqueProduto');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Estoque\Repository\RecebimentoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Estoque\Entity\Recebimento');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Estoque\Repository\ItemRecebidoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Estoque\Entity\ItemRecebido');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Estoque\Repository\ItemPedidoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Estoque\Entity\ItemPedido');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },          
            'Estoque\Repository\EstornoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Estoque\Entity\Estorno');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Estoque\Repository\UnidadeMedidaRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Estoque\Entity\UnidadeMedida');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },        
            'Estoque\Repository\ItemTransferenciaRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Estoque\Entity\ItemTransferencia');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Estoque\Repository\PedidoTransferenciaRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Estoque\Entity\PedidoTransferencia');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Estoque\Repository\RecebimentoTransferenciaRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Estoque\Entity\RecebimentoTransferencia');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Estoque\Repository\SituacaoTransferenciaRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Estoque\Entity\SituacaoTransferencia');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Estoque\Repository\SituacaoPedidoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Estoque\Entity\SituacaoPedido');
                $repository->setLogger($sm->get('Base\Log\Log'));
                return $repository;
            },
            'Estoque\Repository\FechamentoPeriodoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Estoque\Entity\FechamentoPeriodo');
                $repository->setLogger($sm->get('Base\Log\Log'));
                return $repository;
            }, 
            'Estoque\Repository\ItemDescarteRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Estoque\Entity\ItemDescarte');
                $repository->setLogger($sm->get('Base\Log\Log'));
                return $repository;
            },
            'Estoque\Repository\ItemRecebidoDescarteRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Estoque\Entity\ItemRecebidoDescarte');
                $repository->setLogger($sm->get('Base\Log\Log'));
                return $repository;
            },
            'Estoque\Repository\RecebimentoDescarteRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Estoque\Entity\RecebimentoDescarte');
                $repository->setLogger($sm->get('Base\Log\Log'));
                return $repository;
            },
            'Estoque\Repository\SolicitacaoDescarteRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Estoque\Entity\SolicitacaoDescarte');
                $repository->setLogger($sm->get('Base\Log\Log'));
                return $repository;
            },
            'Estoque\Repository\SituacaoDescarteRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Estoque\Entity\SituacaoDescarte');
                $repository->setLogger($sm->get('Base\Log\Log'));
                return $repository;
            },
            'Estoque\Repository\EstornoDescarteRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Estoque\Entity\EstornoDescarte');
                $repository->setLogger($sm->get('Base\Log\Log'));
                return $repository;
            },
        );
    }

    /**
     *
     * @return array
     */
    public function getFormsFilter()
    {
        return array();
    }

    /**
     *
     * Retorna as instâncias de services
     * @return array
     */
    public function getServices()
    {
        return array(
            'Estoque\Service\Contrato' => function ($sm) {
                $service = new \Estoque\Service\Contrato($sm->get('Doctrine\ORM\EntityManager'), new \Estoque\Entity\Contrato(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat())
                    ->setEmpenhoService($sm->get('Estoque\Service\Empenho'));
                return $service;
            },
            'Estoque\Service\Fornecedor' => function ($sm) {
                $service = new \Estoque\Service\Fornecedor($sm->get('Doctrine\ORM\EntityManager'), new \Estoque\Entity\Fornecedor(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat())
                    ->setContratoService($sm->get('Estoque\Service\Contrato'));
                
                return $service;
            },
                    
            'Estoque\Service\Pedido' => function ($sm) {
                $service = new \Estoque\Service\Pedido($sm->get('Doctrine\ORM\EntityManager'), new \Estoque\Entity\Pedido(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat())
                    ->setEstoqueRepository($sm->get('Admin\Repository\EstoqueRepository'))
                    ->setItemPedidoService($sm->get('Estoque\Service\ItemPedido'))
                    ->setPedidoRepository($sm->get('Estoque\Repository\PedidoRepository'));
                    
                return $service;
            },
            'Estoque\Service\ItemPedido' => function ($sm) {
                $service = new \Estoque\Service\ItemPedido($sm->get('Doctrine\ORM\EntityManager'), new \Estoque\Entity\ItemPedido(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());
                return $service;
            },        
            'Estoque\Service\Empenho' => function ($sm) {
                $service = new \Estoque\Service\Empenho($sm->get('Doctrine\ORM\EntityManager'), new \Estoque\Entity\Empenho(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());
                return $service;
            },
            'Estoque\Service\Produto' => function ($sm) {
                $service = new \Estoque\Service\Produto($sm->get('Doctrine\ORM\EntityManager'), new \Estoque\Entity\Produto(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());
                return $service;
            },
            'Estoque\Service\EstoqueProduto' => function ($sm) {
                $service = new \Estoque\Service\EstoqueProduto($sm->get('Doctrine\ORM\EntityManager'), new \Estoque\Entity\EstoqueProduto(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat())
                    ->setEstoqueProdutoRepository($sm->get('Estoque\Repository\EstoqueProdutoRepository'))
                    ->setProdutoService($sm->get('Estoque\Service\Produto'));
                return $service;
            },
            'Estoque\Service\Recebimento' => function ($sm) {
                $service = new \Estoque\Service\Recebimento($sm->get('Doctrine\ORM\EntityManager'), new \Estoque\Entity\Recebimento(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setStringFormat(new \Base\Helper\StringFormat())
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat())
                    ->setItemRecebidoService($sm->get('Estoque\Service\ItemRecebido')) 
                    ->setMovimentacaoService($sm->get('Estoque\Service\Movimentacao')) 
                    ->setEstoqueProdutoService($sm->get('Estoque\Service\EstoqueProduto')) 
                    ->setPedidoService($sm->get('Estoque\Service\Pedido')) 
                    ->setEstoqueRepository($sm->get('Admin\Repository\EstoqueRepository'));
                
                return $service;
            },
            'Estoque\Service\Descarte' => function ($sm) {
                $service = new \Estoque\Service\Descarte($sm->get('Doctrine\ORM\EntityManager'), new \Estoque\Entity\SolicitacaoDescarte(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setStringFormat(new \Base\Helper\StringFormat())
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat())
                    ->setEstoqueProdutoService($sm->get('Estoque\Service\EstoqueProduto'))    
                    ->setEstoqueRepository($sm->get('Admin\Repository\EstoqueRepository'))
                    ->setMovimentacaoService($sm->get('Estoque\Service\Movimentacao')) 
                    ->setSolicitacaoDescarteRepository($sm->get('Estoque\Repository\SolicitacaoDescarteRepository'))
                    ->setItemDescarteService($sm->get('Estoque\Service\ItemDescarte'))    
                    ->setItemDescarteRepository($sm->get('Estoque\Repository\ItemDescarteRepository'));   
                
                return $service;
            },
            'Estoque\Service\ItemRecebido' => function ($sm) {
                $service = new \Estoque\Service\ItemRecebido($sm->get('Doctrine\ORM\EntityManager'), new \Estoque\Entity\ItemRecebido(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());
                return $service;
            },
            'Estoque\Service\Movimentacao' => function ($sm) {
                $service = new \Estoque\Service\Movimentacao($sm->get('Doctrine\ORM\EntityManager'), new \Estoque\Entity\Movimentacao(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());
                return $service;
            },
            'Estoque\Service\Estorno' => function ($sm) {
                $service = new \Estoque\Service\Estorno($sm->get('Doctrine\ORM\EntityManager'), new \Estoque\Entity\Estorno(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setEstoqueProdutoService($sm->get('Estoque\Service\EstoqueProduto'))
                    ->setMovimentacaoService($sm->get('Estoque\Service\Movimentacao')) 
                    ->setPedidoTransferenciaService($sm->get('Estoque\Service\PedidoTransferencia'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());
                return $service;
            },
            'Estoque\Service\ItemRecebidoTransferencia' => function ($sm) {
                $service = new \Estoque\Service\ItemRecebidoTransferencia($sm->get('Doctrine\ORM\EntityManager'), new \Estoque\Entity\ItemRecebidoTransferencia(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());
                return $service;
            },
            'Estoque\Service\ItemTransferencia' => function ($sm) {
                $service = new \Estoque\Service\ItemTransferencia($sm->get('Doctrine\ORM\EntityManager'), new \Estoque\Entity\ItemTransferencia(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());
                return $service;
            },
            'Estoque\Service\PedidoTransferencia' => function ($sm) {
                $service = new \Estoque\Service\PedidoTransferencia($sm->get('Doctrine\ORM\EntityManager'), new \Estoque\Entity\PedidoTransferencia(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setPedidoTransferenciaRepository($sm->get('Estoque\Repository\PedidoTransferenciaRepository'))
                    ->setItemTransferenciaService($sm->get('Estoque\Service\ItemTransferencia'))
                    ->setItemTransferenciaRepository($sm->get('Estoque\Repository\ItemTransferenciaRepository'))
                    ->setEstoqueProdutoService($sm->get('Estoque\Service\EstoqueProduto'))
                    ->setMovimentacaoService($sm->get('Estoque\Service\Movimentacao')) 
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());
                return $service;
            }, 
            'Estoque\Service\RecebimentoTransferencia' => function ($sm) {
                $service = new \Estoque\Service\RecebimentoTransferencia($sm->get('Doctrine\ORM\EntityManager'), new \Estoque\Entity\RecebimentoTransferencia(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat())
                    ->setItemRecebidoTransferenciaService($sm->get('Estoque\Service\ItemRecebidoTransferencia'))
                    ->setMovimentacaoService($sm->get('Estoque\Service\Movimentacao'))
                    ->setEstoqueProdutoService($sm->get('Estoque\Service\EstoqueProduto'));
                
                return $service;
            },
            'Estoque\Service\SituacaoPedido' => function ($sm) {
                $service = new \Estoque\Service\SituacaoPedido($sm->get('Doctrine\ORM\EntityManager'), new \Estoque\Entity\SituacaoPedido(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());
                return $service;
            },
            'Estoque\Service\FechamentoPeriodo' => function ($sm) {
                $service = new \Estoque\Service\FechamentoPeriodo($sm->get('Doctrine\ORM\EntityManager'), new \Estoque\Entity\FechamentoPeriodo(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat())
                    ->setAlteracaoCustoService($sm->get('Estoque\Service\AlteracaoCusto'))
                    ->setProdutoService($sm->get('Estoque\Service\Produto'))
                    ->setMovimentacaoService($sm->get('Estoque\Service\Movimentacao'))
                    ->setFechamentoPeriodoRepository($sm->get('Estoque\Repository\FechamentoPeriodoRepository'));

                return $service;
            },
            'Estoque\Service\AlteracaoCusto' => function ($sm) {
                $service = new \Estoque\Service\AlteracaoCusto($sm->get('Doctrine\ORM\EntityManager'), new \Estoque\Entity\AlteracaoCusto(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());

                return $service;
            },
            'Estoque\Service\ItemDescarte' => function ($sm) {
                $service = new \Estoque\Service\ItemDescarte($sm->get('Doctrine\ORM\EntityManager'), new \Estoque\Entity\ItemDescarte(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());
                
                return $service;
            },
            'Estoque\Service\ItemRecebidoDescarte' => function ($sm) {
                $service = new \Estoque\Service\ItemRecebidoDescarte($sm->get('Doctrine\ORM\EntityManager'), new \Estoque\Entity\ItemRecebidoDescarte(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());
                
                return $service;
            },
            'Estoque\Service\RecebimentoDescarte' => function ($sm) {
                $service = new \Estoque\Service\RecebimentoDescarte($sm->get('Doctrine\ORM\EntityManager'), new \Estoque\Entity\RecebimentoDescarte(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat())
                    ->setItemRecebidoDescarteService($sm->get('Estoque\Service\ItemRecebidoDescarte'))    
                    ->setMovimentacaoService($sm->get('Estoque\Service\Movimentacao'))    
                    ->setEstoqueProdutoService($sm->get('Estoque\Service\EstoqueProduto'));    
                
                return $service;
            },
            'Estoque\Service\EstornoDescarte' => function ($sm) {
                $service = new \Estoque\Service\EstornoDescarte($sm->get('Doctrine\ORM\EntityManager'), new \Estoque\Entity\EstornoDescarte(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setEstoqueProdutoService($sm->get('Estoque\Service\EstoqueProduto'))
                    ->setMovimentacaoService($sm->get('Estoque\Service\Movimentacao')) 
                    ->setSolicitacaoDescarteService($sm->get('Estoque\Service\Descarte'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());
                return $service;
            },
        );
    }

    /**
     * Retorna os serviços do PHPOffice
     * @return array
     */
    public function getOfficeServices()
    {
        return array();
    }

    public function getMailConfig()
    {
        return array();
    }

    public function getAuthServices()
    {
        return array();
    }

    public function getBaseServices()
    {
        return array();
    }

}
