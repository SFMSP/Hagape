<?php

namespace Estoque\Service;

use Application\Service\BaseApplicationService;
use Estoque\Service\Movimentacao as MovimentacaoService;
use Estoque\Service\EstoqueProduto as EstoqueProdutoService;
use Estoque\Service\Descarte as SolicitacaoDescarteService;
use Estoque\Entity\Movimentacao as MovimentacaoEntity;

/**
 * Classe que contém as operações realizadas na entidade de item de estorno de descarte
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class EstornoDescarte extends BaseApplicationService {

    /**
     *
     * @var MovimentacaoService 
     */
    protected $movimentacaoService;

    /**
     *
     * @var EstoqueProdutoService 
     */
    protected $estoqueProdutoService;
    
    /**
     *
     * @var SolicitacaoDescarteService 
     */
    protected $solicitacaoDescarteService;

    /**
     * 
     * @param MovimentacaoService $movimentacaoService
     * @return \Estoque\Service\Estorno
     */
    public function setMovimentacaoService(MovimentacaoService $movimentacaoService) {
        $this->movimentacaoService = $movimentacaoService;
        return $this;
    }

    /**
     * 
     * @param EstoqueProdutoService $estoqueProdutoService
     * @return \Estoque\Service\Estorno
     */
    public function setEstoqueProdutoService(EstoqueProdutoService $estoqueProdutoService) {
        $this->estoqueProdutoService = $estoqueProdutoService;
        return $this;
    }
    
    /**
     * 
     * @param SolicitacaoDescarteService $solicitacaoDescarteService
     * @return \Estoque\Service\Estorno
     */
    public function setSolicitacaoDescarteService(SolicitacaoDescarteService $solicitacaoDescarteService) {
        $this->solicitacaoDescarteService = $solicitacaoDescarteService;
        return $this;
    }

    
    public function save(array $data) {

        $this->getEm()->beginTransaction();

        $data['ids'] = explode(',', $data['ids']);

        foreach ($data['ids'] as $item) {

            $itemDescarte = $this->getEm()->getReference('Estoque\Entity\ItemDescarte', $item);
            $qtdEstornada = $itemDescarte->getItemRecebido() ? $itemDescarte->getQtd() - $itemDescarte->getItemRecebido()->getQtd() : $itemDescarte->getQtd();

            if ($qtdEstornada > 0) {
                $dadosEstorno['operacao'] = 1;
                $dadosEstorno['qtd'] = $qtdEstornada;
                $dadosEstorno['dataEstorno'] = new \DateTime($this->dateTimeFormat->formatUs($data['dataEstorno']));
                $dadosEstorno['motivo'] = $data['motivo'];
                $dadosEstorno['estoqueOrigem'] = $this->getEm()->getReference('Admin\Entity\Estoque', $itemDescarte->getSolicitacaoDescarte()->getEstoqueOrigem()->getId());
                $dadosEstorno['produto'] = $this->getEm()->getReference('Estoque\Entity\Produto', $itemDescarte->getProduto()->getId());
                $dadosEstorno['itemDescarte'] = $this->getEm()->getReference('Estoque\Entity\ItemDescarte', $itemDescarte->getId());

                if (!parent::save($dadosEstorno)) {
                    $this->getEm()->rollback();
                    return false;
                }

                if (!$this->estoqueProdutoService->adicionarEstoque($dadosEstorno['produto'], $dadosEstorno['estoqueOrigem'], $dadosEstorno['qtd'], '+', false)) {
                    $this->getEm()->rollback();
                    return false;
                }

                $dadosMovimentacao['qtd'] = $dadosEstorno['qtd'];
                $dadosMovimentacao['valorTotal'] =  $qtdEstornada * $itemDescarte->getCustoMedio();
                $dadosMovimentacao['dataMovimentacao'] = $dadosEstorno['dataEstorno'];
                $dadosMovimentacao['tipoMovimentacao'] = MovimentacaoEntity::TIPO_MOVIMENTACAO_ENTRADA;
                $dadosMovimentacao['acaoMovimentacao'] = MovimentacaoEntity::ACAO_MOVIMENTACAO_ESTORNO;
                $dadosMovimentacao['estoque'] = $itemDescarte->getSolicitacaoDescarte()->getEstoqueOrigem()->getId();
                $dadosMovimentacao['produto'] = $itemDescarte->getProduto()->getId();
                $dadosMovimentacao['usuario'] = $this->dadosUser['idUsuario'];
                $dadosMovimentacao['solicitacaoDescarte'] = $itemDescarte->getSolicitacaoDescarte()->getId() ;
                
                if (!$this->movimentacaoService->save($dadosMovimentacao)) {
                    $this->getEm()->rollback();
                    return false;
                }
            }
        }
        
        if($data['todosDescartes'] == 1){
            $this->solicitacaoDescarteService->update(array('id' => $data['idSolicitacaoDescarte'], 'situacaoDescarte' => $this->getEm()->getReference('Estoque\Entity\SituacaoDescarte', \Estoque\Entity\SolicitacaoDescarte::STATUS_DESCARTADO)));
        
        }

        $this->getEm()->commit();
        return true;
    }

}
