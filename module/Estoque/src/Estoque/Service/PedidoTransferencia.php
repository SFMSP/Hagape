<?php

namespace Estoque\Service;

use Application\Service\BaseApplicationService;
use Estoque\Entity\PedidoTransferencia as PedidoTransferenciaEntity;
use Estoque\Repository\PedidoTransferenciaRepository;
use Estoque\Repository\ItemTransferenciaRepository;
use Estoque\Service\ItemTransferencia as ItemTransferenciaService;
use Estoque\Service\EstoqueProduto as EstoqueProdutoService;
use Estoque\Service\Movimentacao as MovimentacaoService;
use Estoque\Entity\Movimentacao as MovimentacaoEntity;

/**
 * Classe que contém as operações realizadas na entidade de item de pedidoTransferencia
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class PedidoTransferencia extends BaseApplicationService {

    /**
     *
     * @var PedidoTransferenciaRepository 
     */
    protected $pedidoTransferenciaRepository;

    /**
     *
     * @var ItemTransferenciaService 
     */
    protected $itemTransferenciaService;

    /**
     *
     * @var ItemTransferenciaRepository 
     */
    protected $itemTransferenciaRepository;

    /**
     *
     * @var EstoqueProdutoService 
     */
    protected $estoqueProdutoService;

    /**
     *
     * @var MovimentacaoService 
     */
    protected $movimentacaoService;

    /**
     * 
     * @param PedidoTransferenciaRepository $pedidoTransferenciaRepository
     * @return \Estoque\Service\PedidoTransferencia
     */
    public function setPedidoTransferenciaRepository(PedidoTransferenciaRepository $pedidoTransferenciaRepository) {
        $this->pedidoTransferenciaRepository = $pedidoTransferenciaRepository;
        return $this;
    }

    /**
     * 
     * @param ItemTransferenciaService $itemTransferenciaService
     * @return \Estoque\Service\PedidoTransferencia
     */
    public function setItemTransferenciaService(ItemTransferenciaService $itemTransferenciaService) {
        $this->itemTransferenciaService = $itemTransferenciaService;
        return $this;
    }

    /**
     * 
     * @param ItemTransferenciaRepository $itemTransferenciaRepository
     * @return \Estoque\Service\PedidoTransferencia
     */
    public function setItemTransferenciaRepository(ItemTransferenciaRepository $itemTransferenciaRepository) {
        $this->itemTransferenciaRepository = $itemTransferenciaRepository;
        return $this;
    }

    /**
     * 
     * @param EstoqueProdutoService $estoqueProdutoService
     * @return \Estoque\Service\PedidoTransferencia
     */
    public function setEstoqueProdutoService(EstoqueProdutoService $estoqueProdutoService) {
        $this->estoqueProdutoService = $estoqueProdutoService;
        return $this;
    }

    /**
     * 
     * @param MovimentacaoService $movimentacaoService
     * @return \Estoque\Service\PedidoTransferencia
     */
    public function setMovimentacaoService(MovimentacaoService $movimentacaoService) {
        $this->movimentacaoService = $movimentacaoService;
        return $this;
    }

    public function save(array $data) {

        try {
            //Em caso de edição, verifica se o registro de fato pode ser alterado, de acordo com o seu status
            if (!empty($data['id'])) {

                $pedidoTransferencia = $this->getEm()->find('Estoque\Entity\PedidoTransferencia', $data['id']);

                if (($data['acao'] == 'C' || $data['acao'] == 'CE') && $pedidoTransferencia->getSituacaoTransferencia()->getId() != PedidoTransferenciaEntity::STATUS_CADASTRADO) {
                    return false;
                }
            }


            //Formata os dados para serem salvos
            $data['dataTransferencia'] = new \DateTime($this->dateTimeFormat->formatUs($data['dataTransferencia']));
            $data['estoqueDestino'] = $this->getEm()->getReference("Admin\Entity\Estoque", $data['estoqueDestino']);
            $data['estoqueOrigem'] = $this->getEm()->getReference("Admin\Entity\Estoque", $this->dadosUser['filtrosSelecionados']['estoque']['id']);
            $data['usuario'] = $this->getEm()->getReference('Application\Entity\Usuario', $this->dadosUser['idUsuario']);

            if ($data['acao'] == 'CE') {
                $data['situacaoTransferencia'] = $this->getEm()->getReference("Estoque\Entity\SituacaoTransferencia", PedidoTransferenciaEntity::STATUS_EM_TRANSITO);
            } else {
                $data['situacaoTransferencia'] = $this->getEm()->getReference("Estoque\Entity\SituacaoTransferencia", PedidoTransferenciaEntity::STATUS_CADASTRADO);
            }

            if (!empty($data['dataEntrega'])) {
                $data['dataEntrega'] = new \DateTime($this->dateTimeFormat->formatUs($data['dataEntrega']));
            } else {
                $data['dataEntrega'] = null;
            }

            if (empty($data['id'])) {
                $data['numeroTransferencia'] = $this->pedidoTransferenciaRepository->getNumeroPedidoTransferenciaDisponivel();
            }

            $this->getEm()->beginTransaction();

            $pedidoTransferencia = parent::save($data);

            //Deleta os itens do pedido caso existam ids para deletar
            if (!empty($data['idItensExcluidos'])) {
                $itensExcluidos = substr($data['idItensExcluidos'], 0, -1);
                $itensExcluidos = explode(',', $itensExcluidos);
                $itensExcluidos = $this->itemTransferenciaRepository->findIn($itensExcluidos);

                foreach ($itensExcluidos as $itemExcluido) {
                    $deleteItem = $this->itemTransferenciaService->delete($itemExcluido->getId());
                    $adicionaEstoque = $this->estoqueProdutoService->adicionarEstoque($itemExcluido->getProduto()->getId(), $itemExcluido->getPedidoTransferencia()->getEstoqueOrigem()->getId(), $itemExcluido->getQtd(), '+', false);

                    if (!$deleteItem || !$adicionaEstoque) {
                        $this->getEm()->rollback();
                        return false;
                    }

                    $dadosMovimentacao['qtd'] = $itemExcluido->getQtd();
                    $dadosMovimentacao['valorTotal'] = $itemExcluido->getValorTotal();
                    $dadosMovimentacao['dataMovimentacao'] = new \DateTime('now');
                    $dadosMovimentacao['tipoMovimentacao'] = MovimentacaoEntity::TIPO_MOVIMENTACAO_ENTRADA;
                    $dadosMovimentacao['acaoMovimentacao'] = MovimentacaoEntity::ACAO_MOVIMENTACAO_TRANSFERENCIA;
                    $dadosMovimentacao['estoque'] = $itemExcluido->getPedidoTransferencia()->getEstoqueOrigem()->getId();
                    $dadosMovimentacao['produto'] = $itemExcluido->getProduto()->getId();
                    $dadosMovimentacao['usuario'] = $this->dadosUser['idUsuario'];
                    $dadosMovimentacao['pedidoTransferencia'] = $itemExcluido->getPedidoTransferencia()->getId();

                    if (!$this->movimentacaoService->save($dadosMovimentacao)) {
                        $this->getEm()->rollback();
                        return false;
                    }
                }
            }

            $itensTranferencia = $this->itemTransferenciaRepository->findBy(array('pedidoTransferencia' => $pedidoTransferencia->getId()));
            $itensTrans = array();

            if ($itensTranferencia) {
                foreach ($itensTranferencia as $itemTrans) {
                    $itensTrans[$itemTrans->getProduto()->getId()] = $itemTrans->getId();
                }
            }

            if ($pedidoTransferencia) {

                foreach ($data['id-produto'] as $key => $item) {
                    if (!isset($itensTrans[$item])) {
                        $itemTransferencia['qtd'] = $data['qtd-produto'][$key];
                        $itemTransferencia['custoMedio'] = $data['preco-medio'][$key];
                        $itemTransferencia['valorTotal'] = $data['valor-total'][$key];
                        $itemTransferencia['pedidoTransferencia'] = $this->getEm()->getReference('Estoque\Entity\PedidoTransferencia', $pedidoTransferencia->getId());
                        $itemTransferencia['produto'] = $this->getEm()->getReference('Estoque\Entity\Produto', $data['id-produto'][$key]);

                        if (!$this->itemTransferenciaService->save($itemTransferencia)) {
                            $this->getEm()->rollback();
                            return false;
                        }

                        if (!$this->estoqueProdutoService->adicionarEstoque($data['id-produto'][$key], $pedidoTransferencia->getEstoqueOrigem()->getId(), $data['qtd-produto'][$key], '-', false)) {
                            $this->getEm()->rollback();
                            return false;
                        }
                        $dadosMovimentacao = array();
                        $dadosMovimentacao['qtd'] = $data['qtd-produto'][$key];
                        $dadosMovimentacao['valorTotal'] = $data['valor-total'][$key];
                        $dadosMovimentacao['dataMovimentacao'] = new \DateTime('now');
                        $dadosMovimentacao['tipoMovimentacao'] = MovimentacaoEntity::TIPO_MOVIMENTACAO_SAIDA;
                        $dadosMovimentacao['acaoMovimentacao'] = MovimentacaoEntity::ACAO_MOVIMENTACAO_TRANSFERENCIA;
                        $dadosMovimentacao['estoque'] = $this->dadosUser['filtrosSelecionados']['estoque']['id'];
                        $dadosMovimentacao['produto'] = $data['id-produto'][$key];
                        $dadosMovimentacao['usuario'] = $this->dadosUser['idUsuario'];
                        $dadosMovimentacao['pedidoTransferencia'] = $pedidoTransferencia->getId();

                        if (!$this->movimentacaoService->save($dadosMovimentacao)) {
                            $this->getEm()->rollback();
                            return false;
                        }
                    }
                }
            }


            $this->getEm()->commit();

            return true;
        } catch (\Exception $ex) {
            $this->log->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            $this->getEm()->rollback();
            return false;
        }
    }

    /**
     * Método que muda o status do pedido para cancelado
     * 
     * @param integer $id
     * @return boolean
     */
    public function cancelar($id) {

        $this->getEm()->beginTransaction();

        //Busca os dados do pedido
        $pedido = $this->getEm()->find('Estoque\Entity\PedidoTransferencia', $id);

        //Verifica se é possível realizar o cancelamento
        if ($pedido->getSituacaoTransferencia()->getId() == PedidoTransferenciaEntity::STATUS_CADASTRADO) {
            $update = $this->update(array('id' => $pedido->getId(), 'situacaoTransferencia' => $this->getEm()->getReference('Estoque\Entity\SituacaoTransferencia', PedidoTransferenciaEntity::STATUS_CANCELADO)));

            if ($update) {

                $pedido = $this->getEm()->find('Estoque\Entity\PedidoTransferencia', $id);

                foreach ($pedido->getItensTransferencia() as $item) {
                    if (!$this->estoqueProdutoService->adicionarEstoque($item->getProduto()->getId(), $pedido->getEstoqueOrigem()->getId(), $item->getQtd(), '+', false)) {
                        $this->getEm()->rollback();
                        return false;
                    }
                }

                //Remove todas as movimentacoes pertencentes ao pedido de transferencia
                $delete = $this->getEm()->createQueryBuilder()->delete('Estoque\Entity\Movimentacao', 'm')->where('m.pedidoTransferencia = :id ')->setParameter('id', $id)->getQuery()->execute();

                if ($delete) {
                    $this->getEm()->commit();
                    return true;
                } else {
                    $this->getEm()->rollback();
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Método que deleta pedidos em massa
     * 
     * @param type $ids
     * @return boolean
     */
    public function deleteAll($ids) {
        try {
            //Busca todos os pedidos requisitados para exclusão
            $pedidosTransferencia = $this->pedidoTransferenciaRepository->findIn($ids);

            //Inicia a transação
            $this->getEm()->beginTransaction();

            //Percorre os pedidos e verifica se eles podem ser excluídos
            foreach ($pedidosTransferencia as $pedido) {
                if ($pedido->getSituacaoTransferencia()->getId() == PedidoTransferenciaEntity::STATUS_CADASTRADO) {
                    foreach ($pedido->getItensTransferencia() as $item) {
                        $this->getEm()->remove($this->getEm()->getReference('Estoque\Entity\ItemTransferencia', $item->getId()));
                        $this->getEm()->flush();
                        $this->getEm()->clear();

                        if (!$this->estoqueProdutoService->adicionarEstoque($item->getProduto()->getId(), $pedido->getEstoqueOrigem()->getId(), $item->getQtd(), '+', false)) {
                            $this->getEm()->rollback();
                            return false;
                        }
                    }

                    //Remove todas as movimentacoes pertencentes ao pedido de transferencia
                    $deleteMovimentacao = $this->getEm()->createQueryBuilder()->delete('Estoque\Entity\Movimentacao', 'm')->where('m.pedidoTransferencia = :id ')->setParameter('id', $pedido->getId())->getQuery()->execute();

                    if (!$deleteMovimentacao) {
                        $this->getEm()->rollback();
                        return false;
                    }

                    $delete = parent::delete($pedido->getId());

                    if (!$delete) {
                        $this->getEm()->rollback();
                        return false;
                    }
                } else {
                    $this->getEm()->rollback();
                    return false;
                }
            }

            //Commita a transação
            $this->getEm()->commit();
            return true;
        } catch (\Exception $ex) {
            $this->log->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            $this->getEm()->rollback();
            return false;
        }
    }

}
