<?php

namespace Estoque\Service;

use Application\Service\BaseApplicationService;
use Estoque\Entity\EstoqueProduto as EstoqueProdutoEntity;

/**
 * Classe que contém as operações realizadas na entidade de pedido
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class EstoqueProduto extends BaseApplicationService {

    /**
     *
     * @var \Estoque\Repository\EstoqueProdutoRepository 
     */
    private $estoqueProdutoRepository;

    /**
     *
     * @var \Estoque\Service\Produto
     */
    private $produtoService;

    /**
     * 
     * @param \Estoque\Repository\EstoqueProdutoRepository $estoqueProdutoRepository
     * @return \Estoque\Service\EstoqueProduto
     */
    public function setEstoqueProdutoRepository(\Estoque\Repository\EstoqueProdutoRepository $estoqueProdutoRepository) {
        $this->estoqueProdutoRepository = $estoqueProdutoRepository;
        return $this;
    }

    /**
     * 
     * @param \Estoque\Service\Produto $produtoService
     * @return \Estoque\Service\EstoqueProduto
     */
    function setProdutoService(\Estoque\Service\Produto $produtoService) {
        $this->produtoService = $produtoService;
        return $this;
    }

    public function save(array $data) {

        $data['produto'] = $this->em->getReference('Estoque\Entity\Produto', $data['produto']);
        $data['estoque'] = $this->em->getReference('Admin\Entity\Estoque', $data['estoque']);
        $data['estoqueOrigem'] = $this->em->getReference('Admin\Entity\Estoque', $data['estoqueOrigem']);

        return parent::save($data);
    }

    public function adicionarEstoque($produto, $estoque, $qtd, $operacao, $alterarQtdProduto = true) {

        if ($operacao != '+' && $operacao != '-') {
            return false;
        }

        $estoqueProduto = $this->estoqueProdutoRepository->findOneBy(array('produto' => $produto, 'estoque' => $estoque));

        if (!$estoqueProduto) {
            $produtoEntity = $this->getEm()->find('Estoque\Entity\Produto', $produto);

            if ($produtoEntity) {
                $data['produto'] = $produto;
                $data['estoque'] = $estoque;
                $data['estoqueOrigem'] = $estoque;
                $data['qtd'] = 0;

                $estoqueProduto = $this->save($data);
            } else {
                return false;
            }
        }

        if ($estoqueProduto) {

            if ($operacao == '+') {
                $qtdNova = $estoqueProduto->getQtd() + $qtd;
            } else if ($operacao == '-') {
                $qtdNova = $estoqueProduto->getQtd() - $qtd;
            }

            if ($qtdNova < 0) {
                return false;
            }

            $this->getEm()->beginTransaction();
            $save = parent::save(array('id' => $estoqueProduto->getId(), 'qtd' => $qtdNova));

            if ($save) {

                if ($alterarQtdProduto) {
                    $save = $this->produtoService->alterarEstoque($produto, $qtd, $operacao);

                    if ($save) {
                        $this->getEm()->commit();
                        return true;
                    } else {
                        return false;
                        $this->getEm()->rollback();
                    }
                } else {
                    $this->getEm()->commit();
                    return true;
                }
            } else {
                $this->getEm()->rollback();
                return false;
            }
        } else {
            return false;
        }
    }

}
