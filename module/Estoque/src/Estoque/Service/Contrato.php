<?php
namespace Estoque\Service;

use Application\Service\BaseApplicationService;

/**
 * Classe que contém as operações realizadas na entidade de Contrato
 *
 * @autor Evandro Melos <evandro.noguez@jointecnologia.com.br>
 */
class Contrato extends BaseApplicationService
{
    private $empenhoService;

    /**
     * @param mixed $empenhoService
     */
    public function setEmpenhoService($empenhoService)
    {
        $this->empenhoService = $empenhoService;
        return $this;
    }

    public function save(array $data)
    {
        try {
            //Abre transacao
            $this->getEm()->beginTransaction();

            $data['fornecedor'] = $this->getEm()->getReference('Estoque\Entity\Fornecedor', $data['fornecedor']);
            $data['dataVencimento'] = new \DateTime($this->getDateTimeFormat()->formatUs($data['hiddenDataVencimento']));
            $data['valor'] = preg_replace('/[.]/', '', $data['valor']);
            $data['valor'] = preg_replace('/[,]/', '.', $data['valor']);

            //Insere Contrato
            $entity = parent::save($data);

            if (!$entity) {
                $this->getEm()->rollback();
                return false;
            }

            if ($data['idEmpenhosExcluidos']) {
                $data['idEmpenhosExcluidos'] = substr($data['idEmpenhosExcluidos'], 1, strlen($data['idEmpenhosExcluidos']));
                $arEmpenhoExcluir = explode(';', $data['idEmpenhosExcluidos']);
                foreach ($arEmpenhoExcluir as $value) {
                    //Deleta empenhos de acordo com o contrato
                    $empenhoEntity = $this->empenhoService->delete($value);
                    if (!$empenhoEntity) {
                        $this->getEm()->rollback();
                        return false;
                    }
                }
            }

            foreach ($data['empenho'] as $key => $value) {
                $dadosEmpenho['id'] = $data['empenho'][$key];
                $dadosEmpenho['numEmpenho'] = $data['numEmpenhos'][$key];
                $dadosEmpenho['valor'] = $data['valorEmpenhos'][$key];
                $dadosEmpenho['dataEmpenho'] = new \DateTime($this->getDateTimeFormat()->formatUs($data['dataEmpenhos'][$key]));
                $dadosEmpenho['provisorio'] = ($data['provisorios'][$key]=='true')?1:0;
                $dadosEmpenho['observacao'] = $data['observacoes'][$key];
                $dadosEmpenho['ativo'] = $data['ativo'];
                $dadosEmpenho['contrato'] = $this->getEm()->getReference('Estoque\Entity\Contrato', $entity->getId());
                //Insere empenho de acordo com o contrato
                $empenhoEntity = $this->empenhoService->save($dadosEmpenho);
                if (!$empenhoEntity) {
                    $this->getEm()->rollback();
                    return false;
                }
            }

            $this->getEm()->commit();
            return true;
        } catch (\Exception $ex) {
            $this->log->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            $this->getEm()->rollback();
            return false;
        }

    }

    public function mudarStatus($ids, $status)
    {
        $pedido = $this->em->getRepository('Estoque\Entity\Pedido')->findBy(array('contrato' => $ids));
        //Case haja vinculo do contrato com o pedido não poderá ser desativado
        if (empty($pedido)) {
            return parent::mudarStatus($ids, $status);
        } else {
            return FALSE;
        }

    }

    public function delete($id)
    {
        $pedido = $this->em->getRepository('Estoque\Entity\Pedido')->findBy(array('contrato' => $id));
        if (empty($pedido)) {
            $empenho = $this->em->getRepository('Estoque\Entity\Empenho')->findBy(array('contrato' => $id));
            foreach ($empenho as $value) {
                $empenhoEntity = $this->empenhoService->delete($value->getId());
                if ($empenhoEntity) {
                    return parent::delete($id);
                } else {
                    return FALSE;
                }
            }
        } else {
            return FALSE;
        }
    }

}//Fim da classe