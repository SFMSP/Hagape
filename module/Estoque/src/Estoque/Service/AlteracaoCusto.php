<?php

namespace Estoque\Service;
use Application\Service\BaseApplicationService;
/**
 * Classe que contém as operações realizadas na entidade de AlteracaoCusto
 *
 * @autor Evandro Melos <evandro.noguez@jointecnologia.com.br>
 */
class AlteracaoCusto extends BaseApplicationService
{
    private $fechamentoPeriodoService;

    /**
     * @param mixed $fechamentoPeriodoService
     */
    public function setFechamentoPeriodoService($fechamentoPeriodoService)
    {
        $this->fechamentoPeriodoService = $fechamentoPeriodoService;
        return $this;
    }
}