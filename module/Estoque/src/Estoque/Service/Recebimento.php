<?php

namespace Estoque\Service;

use Application\Service\BaseApplicationService;
use Estoque\Service\ItemRecebido as ItemRecebidoService;
use Estoque\Service\EstoqueProduto as EstoqueProdutoService;
use Estoque\Service\Movimentacao as MovimentacaoService;
use Estoque\Service\Pedido as PedidoService;

use Estoque\Entity\Movimentacao as MovimentacaoEntity;
use Estoque\Entity\Pedido as PedidoEntity;
use Estoque\Entity\SituacaoPedido as SituacaoPedidoEntity;
use Admin\Repository\EstoqueRepository;


/**
 * Description of Recebimento
 *
 * @author Eduardo Praxedes Heinske (eduardo.praxedes@jointecnologia.com.br)
 */
class Recebimento extends BaseApplicationService {

    /**
     *
     * @var ItemRecebidoService
     */
    private $itemRecebidoService;

    /**
     *
     * @var MovimentacaoService
     */
    private $movimentacaoService;

    /**
     *
     * @var EstoqueProdutoService 
     */
    private $estoqueProdutoService;

    /**
     *
     * @var PedidoService
     */
    private $pedidoService;

    /**
     *
     * @var EstoqueRepository 
     */
    private $estoqueRepository;

    /**
     * 
     * @param ItemRecebidoService $itemRecebidoService
     * @return \Estoque\Service\Recebimento
     */
    public function setItemRecebidoService(ItemRecebidoService $itemRecebidoService) {
        $this->itemRecebidoService = $itemRecebidoService;
        return $this;
    }

    /**
     * 
     * @param MovimentacaoService $movimentacaoService
     * @return \Estoque\Service\Recebimento
     */
    public function setMovimentacaoService(MovimentacaoService $movimentacaoService) {
        $this->movimentacaoService = $movimentacaoService;
        return $this;
    }

    /**
     * 
     * @param EstoqueProdutoService $estoqueProdutoService
     * @return \Estoque\Service\Recebimento
     */
    public function setEstoqueProdutoService(EstoqueProdutoService $estoqueProdutoService) {
        $this->estoqueProdutoService = $estoqueProdutoService;
        return $this;
    }

    /**
     * 
     * @param PedidoService $pedidoService
     * @return \Estoque\Service\Recebimento
     */
    public function setPedidoService(PedidoService $pedidoService) {
        $this->pedidoService = $pedidoService;
        return $this;
    }

    /**
     * 
     * @param EstoqueRepository $estoqueRepository
     * @return \Estoque\Service\Recebimento
     */
    public function setEstoqueRepository(EstoqueRepository $estoqueRepository) {
        $this->estoqueRepository = $estoqueRepository;
        return $this;
    }
  

    /**
     * Método que realiza o salvamento de um recebimento, registra suas movimentações e atualiza o estoque
     * 
     * @param array $data
     * @return boolean
     */
    public function save(array $data) {

        try {
            //Inicia a transação
            $this->getEm()->beginTransaction();

            //Formata a data de recebimento
            $data['dataRecebimento'] = new \DateTime($this->dateTimeFormat->formatUs($data['dataRecebimento']));

            //Armazena o nome do arquivo da nota fiscal
            $data['notaFiscal'] = $data['notaFiscal'] ? $this->getStringFormat()->getNameFilePath($data['notaFiscal']['tmp_name']) : $data['hdnNotaFiscal'];

            //Atribui o pedido
            $data['pedido'] = $this->getEm()->getReference('Estoque\Entity\Pedido', $data['pedido']);

            //Atribui o usuário e o valor total do recebimento (valor total inicialmente como 0)
            $data['usuario'] = $this->getEm()->getReference('Application\Entity\Usuario', $this->dadosUser['idUsuario']);
            $data['valorTotal'] = 0;
            //Salva o recebimento
            $recebimento = parent::save($data);

            //Cria a variável qur armazenará o valor total
            $valorTotal = 0;

            if ($recebimento) {

                //Salva os itens recebidos
                foreach ($data['itemRecebido'] as $key => $item) {

                    //Verifica se de fato ouve recebimento de um item
                    if ($data['qtdRecebida'][$key] > 0) {

                        $dados['qtdRecebida'] = $data['qtdRecebida'][$key];
                        $dados['itemPedido'] = $this->getEm()->getReference('Estoque\Entity\ItemPedido', $data['itemRecebido'][$key]);
                        $dados['recebimento'] = $this->getEm()->getReference('Estoque\Entity\Recebimento', $recebimento->getId());

                        //Registra o item recebida
                        $itemRecebido = $this->itemRecebidoService->save($dados);

                        if (!$itemRecebido) {
                            $this->getEm()->rollback();
                            return false;
                        }

                        //Busca o id do estoque central e do produto 
                        $estoqueDestino = $itemRecebido->getItemPedido()->getPedido()->getEstoqueDestino();
                        $produto = $itemRecebido->getItemPedido()->getProduto()->getId();



                        //Adiciona no estoque
                        if (!$this->estoqueProdutoService->adicionarEstoque($produto, $estoqueDestino->getId(), $dados['qtdRecebida'], '+')) {
                            $this->getEm()->rollback();
                            return false;
                        }

                        //Registra uma movimentação
                        $dadosMovimentacao['qtd'] = $dados['qtdRecebida'];
                        $dadosMovimentacao['valorTotal'] = $dados['qtdRecebida'] * $itemRecebido->getItemPedido()->getPrecoUnitario();
                        $dadosMovimentacao['dataMovimentacao'] = $data['dataRecebimento'];
                        $dadosMovimentacao['tipoMovimentacao'] = MovimentacaoEntity::TIPO_MOVIMENTACAO_ENTRADA;
                        $dadosMovimentacao['acaoMovimentacao'] = MovimentacaoEntity::ACAO_MOVIMENTACAO_COMPRA;
                        $dadosMovimentacao['estoque'] = $estoqueDestino->getId();
                        $dadosMovimentacao['produto'] = $itemRecebido->getItemPedido()->getProduto()->getId();
                        $dadosMovimentacao['usuario'] = $this->dadosUser['idUsuario'];
                        $dadosMovimentacao['recebimento'] = $recebimento->getId();

                        if (!$this->movimentacaoService->save($dadosMovimentacao)) {
                            $this->getEm()->rollback();
                            return false;
                        }

                        $valorTotal += $dadosMovimentacao['valorTotal'];
                    }
                }

                //Altera o status do pedido
                $pedido = $this->getEm()->find('Estoque\Entity\Pedido', $itemRecebido->getItemPedido()->getPedido()->getId());

                if ($pedido->getTotalAReceber() == $pedido->getTotalRecebido()) {
                    $pedido->setSituacao($this->getEm()->getReference('Estoque\Entity\SituacaoPedido', PedidoEntity::STATUS_CONCLUIDO));
                } else {
                    $pedido->setSituacao($this->getEm()->getReference('Estoque\Entity\SituacaoPedido', PedidoEntity::STATUS_RECEBIDO_INCOMPLETO));
                }

                //Persiste a alteração no status do pedido
                $this->getEm()->persist($pedido);
                $this->getEm()->flush();
                $this->getEm()->clear();

                //Realiza alteração no campo valorTotal do recebimento
                $recebimento = $this->getEm()->find('Estoque\Entity\Recebimento', $recebimento->getId());
                $recebimento->setValorTotal($valorTotal);
                $this->getEm()->persist($recebimento);
                $this->getEm()->flush();

                //Transfere o arquivo da nota fiscal para a pasta permanente caso o arquivo tenha sido selecionado
                if ($data['notaFiscal']) {
                    copy('./tmp/' . $data['notaFiscal'], './public/upload/notaFiscal/' . $data['notaFiscal']);
                    unlink('./tmp/' . $data['notaFiscal']);
                }

                $this->getEm()->commit();

                return $recebimento;
            } else {
                $this->getEm()->rollback();
                return false;
            }
        } catch (\Exception $e) {
            $this->getEm()->rollback();
            $this->log->log(\Zend\Log\Logger::ERR, $e->getMessage());
            return false;
        }
    }
    private function verificaFechamento($pedido, $redirect = false){
        
        $recebimento = $this->getServiceLocator()->get($this->nameRepository)->findBy(array('pedido' => $pedido));
        $fechamento = $this->getServiceLocator()->get('Estoque\Repository\FechamentoPeriodoRepository')->findAll();
   
        foreach($fechamento as $var){
            
      
            foreach($recebimento as $recebimentos){


                if($var->getDataPeriodoInicial()->format('m/Y') == $recebimentos->getDataRecebimento()->format('m/Y') ){
                   
                    return $redirect ? $this->redirect()->toRoute('home/default', array('controller' => 'home', 'action' => 'index')) : false;
                    
                }
            }
        
        }
        
        
        
        return true;
    }
    /**
     * Método que deleta recebimentos em massa
     * 
     * @param array $ids
     * @return boolean
     */
    public function deleteAll($ids) {

        //Inicia uma transação
        $this->getEm()->beginTransaction();

        try {

            //Desfaz todas as relações dos recebimentos 
            foreach ($ids as $id) {

                //Busca os dados do recebimento
                $recebimento = $this->getEm()->find('Estoque\Entity\Recebimento', $id);

                //Desconta de todos os estoques todos os itens do recebimento
                foreach ($recebimento->getItensRecebidos() as $itemRecebido) {

                    if (!$this->estoqueProdutoService->adicionarEstoque($itemRecebido->getItemPedido()->getProduto()->getId(), $itemRecebido->getItemPedido()->getPedido()->getEstoqueDestino()->getId(), $itemRecebido->getQtdRecebida(), '-')) {
                        $this->getEm()->rollback();
                        return false;
                    }

                    $this->getEm()->createQueryBuilder()->delete('Estoque\Entity\ItemRecebido', 'i')->where('i.id = :id')->setParameter('id', $itemRecebido->getId())->getQuery()->execute();
                }

                //Deleta todos as movimentações do recebimento
                $this->getEm()->createQueryBuilder()->delete('Estoque\Entity\Movimentacao', 'r')->where('r.recebimento = :id')->setParameter('id', $id)->getQuery()->execute();
            }

            $delete = parent::deleteAll($ids);

            if ($delete) {
                $this->getEm()->commit();
                return true;
            } else {
                $this->getEm()->rollback();
                return false;
            }
        } catch (\Exception $e) {
            $this->getEm()->rollback();
            $this->log->log(\Zend\Log\Logger::ERR, $e->getMessage());
            return false;
        }
    }

}
