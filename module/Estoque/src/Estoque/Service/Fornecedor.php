<?php
namespace Estoque\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Application\Service\BaseApplicationService;

   
/**
 * Classe que contÃ©m as operaÃ§Ãµes realizadas na entidade de usuÃ¡rios
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Fornecedor extends BaseApplicationService {
    
    private $contratoService;
    
    function setContratoService($contratoService) {
        $this->contratoService = $contratoService;
        return $this;
    }

    public function save(array $data) {
      
        $cnpj = $data['cnpj'];
        $cnpj = str_replace('.', '', $cnpj); 
        $cnpj = str_replace('/', '', $cnpj); 
        $data['cnpj'] = str_replace('-', '', $cnpj); 
        //print_r($data['cnpj']);die;
        return parent::save($data);
        
    }
    public function delete($id) {
          
        $contrato = $this->em->getRepository('Estoque\Entity\Contrato')->findBy(array('fornecedor' => $id));
     
         if (empty($contrato)) {
               
             return parent::delete($id);
             
         }else{
               
            return FALSE;
              
         }
        
        
    }
    public function mudarStatus($ids, $status) {
        
        $contrato = $this->em->getRepository('Estoque\Entity\Contrato')->findBy(array('fornecedor' => $ids));
       
         if (empty($contrato)) {
            
             return parent::mudarStatus($ids, $status);
             
         }else{
           
             return FALSE;
         }
        
        
    }
  
    
  
}