<?php

namespace Estoque\Service;

use Application\Service\BaseApplicationService;
use Admin\Repository\EstoqueRepository;
use Estoque\Repository\PedidoRepository;
use Estoque\Service\ItemPedido as ItemPedidoService;
use Estoque\Entity\Pedido as PedidoEntity;

/**
 * Classe que contém as operações realizadas na entidade de pedido
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Pedido extends BaseApplicationService {

    /**
     *
     * @var EstoqueRepository
     */
    private $estoqueRepository;

    /**
     *
     * @var ItemPedidoService 
     */
    private $itemPedidoService;

    /**
     *
     * @var PedidoRepository
     */
    private $pedidoRepository;

    /**
     * 
     * @param EstoqueRepository $estoqueRepository
     * @return \Estoque\Service\Pedido
     */
    public function setEstoqueRepository(EstoqueRepository $estoqueRepository) {
        $this->estoqueRepository = $estoqueRepository;
        return $this;
    }

    /**
     * 
     * @param ItemPedidoService $itemPedidoService
     * @return \Estoque\Service\Pedido
     */
    public function setItemPedidoService(ItemPedidoService $itemPedidoService) {
        $this->itemPedidoService = $itemPedidoService;
        return $this;
    }

    /**
     * 
     * @param PedidoRepository $pedidoRepository
     * @return \Estoque\Service\Pedido
     */
    function setPedidoRepository(PedidoRepository $pedidoRepository) {
        $this->pedidoRepository = $pedidoRepository;
        return $this;
    }

    public function save(array $data) {

        //Valida se a operação selecionada pode ser realizada
        if (!empty($data['id'])) {
            $pedidoAtual = $this->getEm()->find('Estoque\Entity\Pedido', $data['id']);

            if (($data['acao'] == 'C' || $data['acao'] == 'CE') && $pedidoAtual->getSituacao()->getId() !== PedidoEntity::STATUS_CADASTRADO) {
                return false;
            }

        }


        try {
            $this->getEm()->beginTransaction();

            $data['fornecedor'] = $this->getEm()->getReference('Estoque\Entity\Fornecedor', $data['fornecedor']);
            $data['contrato'] = $this->getEm()->getReference('Estoque\Entity\Contrato', $data['contrato']);
            $data['empenho'] = $this->getEm()->getReference('Estoque\Entity\Empenho', $data['empenho']);
            
            if(empty($data['id'])){
                $data['numeroPedido'] = $this->pedidoRepository->getNumeroPedidoDisponivel();
            }
            
            if ($data['dataSolicitacao']) {
                $data['dataSolicitacao'] = new \DateTime($this->getDateTimeFormat()->formatUs($data['dataSolicitacao']));
            } else {
                $data['dataSolicitacao'] = null;
            }

            if ($data['dataEntrega']) {
                $data['dataEntrega'] = new \DateTime($this->getDateTimeFormat()->formatUs($data['dataEntrega']));
            } else {
                $data['dataEntrega'] = null;
            }

            $data['estoqueOrigem'] = $this->estoqueRepository->getEstoqueCentral();
            $data['estoqueDestino'] = $this->estoqueRepository->getEstoqueCentral();

            switch ($data['acao']) {
                case 'C': $data['situacao'] = $this->getEm()->getReference('Estoque\Entity\SituacaoPedido', PedidoEntity::STATUS_CADASTRADO);
                    break;
                case 'CE': $data['situacao'] = $this->getEm()->getReference('Estoque\Entity\SituacaoPedido', PedidoEntity::STATUS_SOLICITADO);
                    break;
            }


            //Salva o pedido
            $pedido = parent::save($data);

            if ($pedido) {

                //Deleta os itens do pedido caso existam ids para deletar
                if (!empty($data['idItensExcluidos'])) {
                    $itensExcluidos = substr($data['idItensExcluidos'], 0, -1);
                    $itensExcluidos = explode(',', $itensExcluidos);

                    if (!$this->itemPedidoService->deleteAll($itensExcluidos)) {
                        $this->getEm()->rollback();
                        return false;
                    }
                }

                //Salva todos os itens do pedido
                foreach ($data['id-item-pedido'] as $key => $item) {
                    if ($item == 0) {
                        $dadosItem['produto'] = $this->getEm()->getReference('Estoque\Entity\Produto', $data['id-produto'][$key]);
                        $dadosItem['qtd'] = $data['qtd-produto'][$key];
                        $dadosItem['precoUnitario'] = $data['preco-unitario-produto'][$key];
                        $dadosItem['pedido'] = $this->getEm()->getReference('Estoque\Entity\Pedido', $pedido->getId());

                        if (!$this->itemPedidoService->save($dadosItem)) {
                            $this->getEm()->rollback();
                            return false;
                        }
                    }
                }
            } else {
                $this->getEm()->rollback();
            }
            //Commita a transação
            $this->getEm()->commit();

            return true;
        } catch (\Exception $ex) {
            $this->log->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            $this->getEm()->rollback();
            return false;
        }
    }

    /**
     * Método que muda o status do pedido para cancelado
     * 
     * @param integer $id
     * @return boolean
     */
    public function cancelar($id) {
        //Busca os dados do pedido
        $pedido = $this->getEm()->find('Estoque\Entity\Pedido', $id);
        
        //Verifica se é possível realizar o cancelamento
        if ($pedido->getSituacao()->getId() == PedidoEntity::STATUS_SOLICITADO) {
            return $this->update(array('id' => $pedido->getId(), 'situacao' => $this->getEm()->getReference('Estoque\Entity\SituacaoPedido', PedidoEntity::STATUS_CANCELADO)));
        } else {
            return false;
        }
    }

    /**
     * Método que deleta pedidos em massa
     * 
     * @param type $ids
     * @return boolean
     */
    public function deleteAll($ids) {
        try {
            //Busca todos os pedidos requisitados para exclusão
            $pedidos = $this->pedidoRepository->findIn($ids);

            //Inicia a transação
            $this->getEm()->beginTransaction();

            //Percorre os pedidos e verifica se eles podem ser excluídos
            foreach ($pedidos as $pedido) {
                if ($pedido->getSituacao()->getId() == PedidoEntity::STATUS_CADASTRADO) {
                    foreach($pedido->getItensPedido() as $item){
                        $this->getEm()->remove($item);
                        $this->getEm()->flush();
                        $this->getEm()->clear();
                    }
                    $delete = parent::delete($pedido->getId());

                    if (!$delete) {
                        $this->getEm()->rollback();
                        return false;
                    }
                } else {
                    $this->getEm()->rollback();
                    return false;
                }
            }

            //Commita a transação
            $this->getEm()->commit();
            return true;
        } catch (\Exception $ex) {
            $this->log->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            $this->getEm()->rollback();
            return false;
        }
    }

}
