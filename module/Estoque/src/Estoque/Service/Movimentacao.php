<?php

namespace Estoque\Service;

use Application\Service\BaseApplicationService;

/**
 * Description of Movimentacao
 *
 * @author Praxedes
 */
class Movimentacao extends BaseApplicationService {

    public function save(array $data) {

        $data['tipoMovimentacao'] = $this->em->getReference('Estoque\Entity\TipoMovimentacao', $data['tipoMovimentacao']);
        $data['acaoMovimentacao'] = $this->em->getReference('Estoque\Entity\AcaoMovimentacao', $data['acaoMovimentacao']);
        $data['usuario'] = $this->em->getReference('Application\Entity\Usuario', $data['usuario']);
        $data['estoque'] = $this->em->getReference('Admin\Entity\Estoque', $data['estoque']);
        $data['produto'] = $this->em->getReference('Estoque\Entity\Produto', $data['produto']);

        if (isset($data['recebimento']) && $data['recebimento']) {
            $data['recebimento'] = $this->em->getReference('Estoque\Entity\Recebimento', $data['recebimento']);
        }

        if (isset($data['pedidoTransferencia']) && $data['pedidoTransferencia']) {
            $data['pedidoTransferencia'] = $this->em->getReference('Estoque\Entity\PedidoTransferencia', $data['pedidoTransferencia']);
        }

        if (isset($data['recebimentoTransferencia']) && $data['recebimentoTransferencia']) {
            $data['recebimentoTransferencia'] = $this->em->getReference('Estoque\Entity\RecebimentoTransferencia', $data['recebimentoTransferencia']);
        }

        if (isset($data['descarte']) && $data['descarte']) {
            $data['descarte'] = $this->em->getReference('Estoque\Entity\SolicitacaoDescarte', $data['descarte']);
        }


        return parent::save($data);
    }

    public function saveAlteracaoFechamentoPeriodo(array $data) {
        return parent::save($data);
    }

}
