<?php

namespace Estoque\Service;

use Application\Service\BaseApplicationService;

/**
 * Classe que contém as operações realizadas na entidade de produto
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Produto extends BaseApplicationService {

    public function alterarEstoque($produto, $qtd, $operacao) {

        if ($operacao != '+' && $operacao != '-') {
            return false;
        }

        $produto = $this->getEm()->find('Estoque\Entity\Produto', $produto);

        if ($produto) {

            if ($operacao == '+') {
                $qtd = $produto->getQtdAtual() + $qtd;
            } else if ($operacao == '-') {
                $qtd = $produto->getQtdAtual() - $qtd;
            }
            return parent::save(array('id' => $produto->getId(), 'qtdAtual' => $qtd));
        } else {
            return false;
        }
    }

}
