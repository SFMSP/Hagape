<?php

namespace Estoque\Service;

use Application\Service\BaseApplicationService;
use Estoque\Service\ItemRecebidoDescarte as ItemRecebidoDescarteService;
use Estoque\Service\EstoqueProduto as EstoqueProdutoService;
use Estoque\Service\Movimentacao as MovimentacaoService;
use Estoque\Entity\Movimentacao as MovimentacaoEntity;
use Estoque\Entity\SolicitacaoDescarte as SolicitacaoDescarteEntity;

/**
 * Classe que contém as operações realizadas na entidade de item de RecebimentoDescarte
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class RecebimentoDescarte extends BaseApplicationService {

    /**
     *
     * @var ItemRecebidoDescarteService 
     */
    protected $itemRecebidoDescarteService;

    /**
     *
     * @var MovimentacaoService
     */
    private $movimentacaoService;

    /**
     *
     * @var EstoqueProdutoService 
     */
    private $estoqueProdutoService;

    /**
     * 
     * @param ItemRecebidoDescarteService $itemRecebidoDescarteService
     * @return \Estoque\Service\RecebimentoDescarte
     */
    public function setItemRecebidoDescarteService(ItemRecebidoDescarteService $itemRecebidoDescarteService) {
        $this->itemRecebidoDescarteService = $itemRecebidoDescarteService;
        return $this;
    }

    /**
     * 
     * @param MovimentacaoService $movimentacaoService
     * @return \Estoque\Service\RecebimentoDescarte
     */
    public function setMovimentacaoService(MovimentacaoService $movimentacaoService) {
        $this->movimentacaoService = $movimentacaoService;
        return $this;
    }

    /**
     * 
     * @param EstoqueProdutoService $estoqueProdutoService
     * @return \Estoque\Service\RecebimentoDescarte
     */
    public function setEstoqueProdutoService(EstoqueProdutoService $estoqueProdutoService) {
        $this->estoqueProdutoService = $estoqueProdutoService;
        return $this;
    }

    public function save(array $data) {

        $this->getEm()->beginTransaction();

        $data['dataRecebimentoDescarte'] = new \DateTime($this->dateTimeFormat->formatUs($data['dataRecebimento']));
        $data['solicitacaoDescarte'] = $this->getEm()->getReference('Estoque\Entity\SolicitacaoDescarte', $data['solicitacaoDescarte']);

        $recebimentoDescarte = parent::save($data);

        if ($recebimentoDescarte) {

            foreach ($data['itemRecebido'] as $key => $item) {

                if ($data['qtdRecebida'][$key] > 0) {
                    $dadosItemRecebido['qtd'] = $data['qtdRecebida'][$key];
                    $dadosItemRecebido['itemDescarte'] = $this->getEm()->getReference('Estoque\Entity\ItemDescarte', $data['itemRecebido'][$key]);
                    $dadosItemRecebido['recebimentoDescarte'] = $this->getEm()->getReference('Estoque\Entity\RecebimentoDescarte', $recebimentoDescarte->getId());

                    $itemRecebidoDescarte = $this->itemRecebidoDescarteService->save($dadosItemRecebido);

                    if (!$itemRecebidoDescarte) {
                        $this->getEm()->rollback();
                        return false;
                    }

                    $estoqueDestino = $recebimentoDescarte->getSolicitacaoDescarte()->getEstoqueDestino()->getId();
                    $produto = $itemRecebidoDescarte->getItemDescarte()->getProduto()->getId();
    
                    //Adiciona no estoque
                    if (!$this->estoqueProdutoService->adicionarEstoque($produto, $estoqueDestino, $dadosItemRecebido['qtd'], '+', false)) {
                        $this->getEm()->rollback();
                        return false;
                    }

                    //Registra uma movimentação
                    $dadosMovimentacao['qtd'] = $dadosItemRecebido['qtd'];
                    $dadosMovimentacao['valorTotal'] = $dadosItemRecebido['qtd'] * $itemRecebidoDescarte->getItemDescarte()->getCustoMedio();
                    $dadosMovimentacao['dataMovimentacao'] = $data['dataRecebimentoDescarte'];
                    $dadosMovimentacao['tipoMovimentacao'] = MovimentacaoEntity::TIPO_MOVIMENTACAO_ENTRADA;
                    $dadosMovimentacao['acaoMovimentacao'] = MovimentacaoEntity::ACAO_MOVIMENTACAO_DESCARTE;
                    $dadosMovimentacao['estoque'] = $estoqueDestino;
                    $dadosMovimentacao['produto'] = $produto;
                    $dadosMovimentacao['usuario'] = $this->dadosUser['idUsuario'];
                    $dadosMovimentacao['recebimentoDescarte'] = $recebimentoDescarte->getId();

                    if (!$this->movimentacaoService->save($dadosMovimentacao)) {
                        $this->getEm()->rollback();
                        return false;
                    }
                }
            }

            //Altera o status da solicitação de descarte
            $solicitacaoDescarte = $this->getEm()->find('Estoque\Entity\SolicitacaoDescarte', $recebimentoDescarte->getSolicitacaoDescarte()->getId());

            if ($solicitacaoDescarte->getTotalAReceber() == ($solicitacaoDescarte->getTotalRecebido() + $solicitacaoDescarte->getTotalEstornado()) ) {
                $solicitacaoDescarte->setSituacaoDescarte($this->getEm()->getReference('Estoque\Entity\SituacaoDescarte', SolicitacaoDescarteEntity::STATUS_DESCARTADO));
            } else {
                $solicitacaoDescarte->setSituacaoDescarte($this->getEm()->getReference('Estoque\Entity\SituacaoDescarte', SolicitacaoDescarteEntity::STATUS_PENDENTE));
            }

            $this->getEm()->persist($solicitacaoDescarte);
            $this->getEm()->flush();

            $this->getEm()->commit();
            return true;
        } else {
            return false;
        }
    }

}
