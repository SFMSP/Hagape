<?php

namespace Estoque\Service;
use Application\Service\BaseApplicationService;
use Estoque\Entity\Movimentacao;

/**
 * Classe que contém as operações realizadas na entidade de FechamentoPeriodo
 *
 * @autor Evandro Melos <evandro.noguez@jointecnologia.com.br>
 */
class FechamentoPeriodo extends BaseApplicationService
{
    /**
     * @var
     */
    private $alteracaoCustoService;
    /**
     * @var
     */
    private $produtoService;
    /**
     * @var
     */
    private $fechamentoPeriodoRepository;
    /**
     * @var
     */
    private $movimentacaoService;

    /**
     * @param mixed $movimentacaoService
     */
    public function setMovimentacaoService($movimentacaoService)
    {
        $this->movimentacaoService = $movimentacaoService;
        return $this;
    }
    /**
     * @param mixed $fechamentoPeriodoRepository
     */
    public function setFechamentoPeriodoRepository($fechamentoPeriodoRepository)
    {
        $this->fechamentoPeriodoRepository = $fechamentoPeriodoRepository;
        return $this;
    }
    /**
     * @param mixed $produtoService
     */
    public function setProdutoService($produtoService)
    {
        $this->produtoService = $produtoService;
        return $this;
    }

    /**
     * @param mixed $alteracaoCustoService
     */
    public function setAlteracaoCustoService($alteracaoCustoService)
    {
        $this->alteracaoCustoService = $alteracaoCustoService;
        return $this;
    }

    /**
     * @param array $pedidosRecebidos
     */
    public function processaFechamentoPeriodo($pedidosRecebidos = array(),\DateTime $dataExecucao)
    {
        $nuPrecoMedioAnterior = 0;
        $produto = '';
        $stObservacao = '';
        $dtExecucaoFechamento = $dataExecucao->format('d/m/y');

        try{
            //Abre transacao
            $this->getEm()->beginTransaction();

            $dadosFechamento['id'] = '';
            $dadosFechamento['dataPeriodoInicial'] = new \DateTime($pedidosRecebidos[0]['data_inicial_periodo']);
            $dadosFechamento['dataPeriodoFinal'] = new \DateTime($pedidosRecebidos[0]['data_final_periodo']);
            $dadosFechamento['dataExecucao'] = $dataExecucao;

            $fechamentoEntity = parent::save($dadosFechamento);

            if (!$fechamentoEntity){
                $this->getEm()->rollback();
                return false;
            }

            foreach ($pedidosRecebidos as $key => $value) {
                $produto = $this->getEm()
                    ->getReference('Estoque\Entity\Produto', ['id' => $value['produto']]);

                $nuPrecoMedioAnterior = $produto->getPrecoMedioAtual();
                $stObservacao = "Produto " . $produto->getNome() . " na data " . $dtExecucaoFechamento;
                $stObservacao .= " passou de R$ " . number_format($nuPrecoMedioAnterior, 2, ',', '.') . "";
                $stObservacao .= " para R$ " . number_format($value['valor_preco_medio_atual'], 2, ',', '.') . "";

                $arDadosAlteracaoCusto['id'] = '';
                $arDadosAlteracaoCusto['produto'] = $produto;
                $arDadosAlteracaoCusto['fechamento'] = $this->getEm()->getReference('Estoque\Entity\FechamentoPeriodo', ['id' => $fechamentoEntity->getId()]);
                $arDadosAlteracaoCusto['numCustoMedioAnterior'] = $nuPrecoMedioAnterior;
                $arDadosAlteracaoCusto['numCustoMedioAtual'] = $value['valor_preco_medio_atual'];
                $arDadosAlteracaoCusto['observacao'] = $stObservacao;

                $alteracaoEntity = $this->alteracaoCustoService->save($arDadosAlteracaoCusto);

                if (!$alteracaoEntity){
                    $this->getEm()->rollback();
                    return false;
                }

                //Alterando valor de custo medio atual com o novo valor
                $produto->setPrecoMedioAtual($value['valor_preco_medio_atual']);
                $produto->setMedida($this->getEm()->getReference('Estoque\Entity\UnidadeMedida', ['id' => $produto->getMedida()->getId()]));
                $produto->setCategoria($this->getEm()->getReference('Estoque\Entity\CategoriaProduto', ['id' => $produto->getCategoria()->getId()]));
                $produtoEntity = $this->produtoService->save($produto->toArray());

                if (!$produtoEntity){
                    $this->getEm()->rollback();
                    return false;
                }

                //Busca as movimentações do periodo do dia 1º até o dia que foi executada a rotina
                $arMovimentacoes = $this->fechamentoPeriodoRepository
                    ->buscaMovimentacaoSaidaInicioPeriodo($dataExecucao,$produtoEntity->getId());

                if($arMovimentacoes){
                    foreach ($arMovimentacoes as $chave => $movimentacao){
                        $movimentacaoEntityReference = $this->getEm()->getReference('Estoque\Entity\Movimentacao', ['id' => $movimentacao['id']]);
                        //Atualiza valor de saida com o novo custo medio
                        $movimentacaoEntityReference->setValorTotal($movimentacao['nu_qtd'] * $value['valor_preco_medio_atual']);
                        $movimentacaoEntity = $this->movimentacaoService
                            ->saveAlteracaoFechamentoPeriodo($movimentacaoEntityReference->toArray());

                        if (!$movimentacaoEntity){
                            $this->getEm()->rollback();
                            return false;
                        }
                    }
                }
            }

            $this->getEm()->commit();
            return true;
        } catch (\Exception $ex) {
            $this->log->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            $this->getEm()->rollback();
            return false;
        }

    }

}