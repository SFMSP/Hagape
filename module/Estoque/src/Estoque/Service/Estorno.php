<?php

namespace Estoque\Service;

use Application\Service\BaseApplicationService;
use Estoque\Service\Movimentacao as MovimentacaoService;
use Estoque\Service\EstoqueProduto as EstoqueProdutoService;
use Estoque\Service\PedidoTransferencia as PedidoTransferenciaService;
use Estoque\Entity\Movimentacao as MovimentacaoEntity;

/**
 * Classe que contém as operações realizadas na entidade de item de estorno
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Estorno extends BaseApplicationService {

    /**
     *
     * @var MovimentacaoService 
     */
    protected $movimentacaoService;

    /**
     *
     * @var EstoqueProdutoService 
     */
    protected $estoqueProdutoService;
    
    /**
     *
     * @var PedidoTransferenciaService 
     */
    protected $pedidoTransferenciaService;

    /**
     * 
     * @param MovimentacaoService $movimentacaoService
     * @return \Estoque\Service\Estorno
     */
    public function setMovimentacaoService(MovimentacaoService $movimentacaoService) {
        $this->movimentacaoService = $movimentacaoService;
        return $this;
    }

    /**
     * 
     * @param EstoqueProdutoService $estoqueProdutoService
     * @return \Estoque\Service\Estorno
     */
    public function setEstoqueProdutoService(EstoqueProdutoService $estoqueProdutoService) {
        $this->estoqueProdutoService = $estoqueProdutoService;
        return $this;
    }
    
    /**
     * 
     * @param PedidoTransferenciaService $pedidoTransferenciaService
     * @return \Estoque\Service\Estorno
     */
    public function setPedidoTransferenciaService(PedidoTransferenciaService $pedidoTransferenciaService) {
        $this->pedidoTransferenciaService = $pedidoTransferenciaService;
        return $this;
    }

    
    public function save(array $data) {

        $this->getEm()->beginTransaction();

        $data['ids'] = explode(',', $data['ids']);

        foreach ($data['ids'] as $item) {

            $itemTransferencia = $this->getEm()->getReference('Estoque\Entity\ItemTransferencia', $item);
            $qtdEstornada = $itemTransferencia->getItemRecebido() ? $itemTransferencia->getQtd() - $itemTransferencia->getItemRecebido()->getQtd() : $itemTransferencia->getQtd();

            if ($qtdEstornada > 0) {
                $dadosEstorno['operacao'] = 1;
                $dadosEstorno['qtd'] = $qtdEstornada;
                $dadosEstorno['dataEstorno'] = new \DateTime($this->dateTimeFormat->formatUs($data['dataEstorno']));
                $dadosEstorno['motivo'] = $data['motivo'];
                $dadosEstorno['estoqueOrigem'] = $this->getEm()->getReference('Admin\Entity\Estoque', $itemTransferencia->getPedidoTransferencia()->getEstoqueOrigem()->getId());
                $dadosEstorno['produto'] = $this->getEm()->getReference('Estoque\Entity\Produto', $itemTransferencia->getProduto()->getId());
                $dadosEstorno['itemTransferencia'] = $this->getEm()->getReference('Estoque\Entity\ItemTransferencia', $itemTransferencia->getId());

                if (!parent::save($dadosEstorno)) {
                    $this->getEm()->rollback();
                    return false;
                }

                if (!$this->estoqueProdutoService->adicionarEstoque($dadosEstorno['produto'], $dadosEstorno['estoqueOrigem'], $dadosEstorno['qtd'], '+', false)) {
                    $this->getEm()->rollback();
                    return false;
                }

                $dadosMovimentacao['qtd'] = $dadosEstorno['qtd'];
                $dadosMovimentacao['valorTotal'] =  $qtdEstornada * $itemTransferencia->getCustoMedio();
                $dadosMovimentacao['dataMovimentacao'] = $dadosEstorno['dataEstorno'];
                $dadosMovimentacao['tipoMovimentacao'] = MovimentacaoEntity::TIPO_MOVIMENTACAO_ENTRADA;
                $dadosMovimentacao['acaoMovimentacao'] = MovimentacaoEntity::ACAO_MOVIMENTACAO_ESTORNO;
                $dadosMovimentacao['estoque'] = $itemTransferencia->getPedidoTransferencia()->getEstoqueOrigem()->getId();
                $dadosMovimentacao['produto'] = $itemTransferencia->getProduto()->getId();
                $dadosMovimentacao['usuario'] = $this->dadosUser['idUsuario'];
                $dadosMovimentacao['recebimentoTransferencia'] = $itemTransferencia->getItemRecebido()->getRecebimentoTransferencia()->getId() ;
                
                if (!$this->movimentacaoService->save($dadosMovimentacao)) {
                    $this->getEm()->rollback();
                    return false;
                }
            }
        }
        
        if($data['todasTransferencias'] == 1){
            $this->pedidoTransferenciaService->update(array('id' => $data['idPedidoTransferencia'], 'situacaoTransferencia' => $this->getEm()->getReference('Estoque\Entity\SituacaoTransferencia', \Estoque\Entity\PedidoTransferencia::STATUS_ENCERRADO)));
        
        }

        $this->getEm()->commit();
        return true;
    }

}
