<?php

namespace Estoque\Service;

use Application\Service\BaseApplicationService;
use Estoque\Service\ItemRecebidoTransferencia as ItemRecebidoTransferenciaService;
use Estoque\Service\EstoqueProduto as EstoqueProdutoService;
use Estoque\Service\Movimentacao as MovimentacaoService;
use Estoque\Entity\Movimentacao as MovimentacaoEntity;
use Estoque\Entity\PedidoTransferencia as PedidoTransferenciaEntity;

/**
 * Classe que contém as operações realizadas na entidade de item de recebimentoTransferencia
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class RecebimentoTransferencia extends BaseApplicationService {

    /**
     *
     * @var ItemRecebidoTransferenciaService 
     */
    protected $itemRecebidoTransferenciaService;

    /**
     *
     * @var MovimentacaoService
     */
    private $movimentacaoService;

    /**
     *
     * @var EstoqueProdutoService 
     */
    private $estoqueProdutoService;

    /**
     * 
     * @param ItemRecebidoTransferenciaService $itemRecebidoTransferenciaService
     * @return \Estoque\Service\RecebimentoTransferencia
     */
    public function setItemRecebidoTransferenciaService(ItemRecebidoTransferenciaService $itemRecebidoTransferenciaService) {
        $this->itemRecebidoTransferenciaService = $itemRecebidoTransferenciaService;
        return $this;
    }

    /**
     * 
     * @param MovimentacaoService $movimentacaoService
     * @return \Estoque\Service\RecebimentoTransferencia
     */
    public function setMovimentacaoService(MovimentacaoService $movimentacaoService) {
        $this->movimentacaoService = $movimentacaoService;
        return $this;
    }

    /**
     * 
     * @param EstoqueProdutoService $estoqueProdutoService
     * @return \Estoque\Service\RecebimentoTransferencia
     */
    public function setEstoqueProdutoService(EstoqueProdutoService $estoqueProdutoService) {
        $this->estoqueProdutoService = $estoqueProdutoService;
        return $this;
    }

    public function save(array $data) {

        $this->getEm()->beginTransaction();

        $data['dataRecebimentoTransferencia'] = new \DateTime($this->dateTimeFormat->formatUs($data['dataRecebimento']));
        $data['pedidoTransferencia'] = $this->getEm()->getReference('Estoque\Entity\PedidoTransferencia', $data['pedidoTransferencia']);

        $recebimentoTransferencia = parent::save($data);

        if ($recebimentoTransferencia) {

            foreach ($data['itemRecebido'] as $key => $item) {

                if ($data['qtdRecebida'][$key] > 0) {
                    $dadosItemRecebido['qtd'] = $data['qtdRecebida'][$key];
                    $dadosItemRecebido['itemTransferencia'] = $this->getEm()->getReference('Estoque\Entity\ItemTransferencia', $data['itemRecebido'][$key]);
                    $dadosItemRecebido['recebimentoTransferencia'] = $this->getEm()->getReference('Estoque\Entity\RecebimentoTransferencia', $recebimentoTransferencia->getId());

                    $itemRecebidoTransferencia = $this->itemRecebidoTransferenciaService->save($dadosItemRecebido);

                    if (!$itemRecebidoTransferencia) {
                        $this->getEm()->rollback();
                        return false;
                    }

                    $estoqueDestino = $recebimentoTransferencia->getPedidoTransferencia()->getEstoqueDestino()->getId();
                    $produto = $itemRecebidoTransferencia->getItemTransferencia()->getProduto()->getId();
    
                    //Adiciona no estoque
                    if (!$this->estoqueProdutoService->adicionarEstoque($produto, $estoqueDestino, $dadosItemRecebido['qtd'], '+', false)) {
                        $this->getEm()->rollback();
                        return false;
                    }

                    //Registra uma movimentação
                    $dadosMovimentacao['qtd'] = $dadosItemRecebido['qtd'];
                    $dadosMovimentacao['valorTotal'] = $dadosItemRecebido['qtd'] * $itemRecebidoTransferencia->getItemTransferencia()->getCustoMedio();
                    $dadosMovimentacao['dataMovimentacao'] = $data['dataRecebimentoTransferencia'];
                    $dadosMovimentacao['tipoMovimentacao'] = MovimentacaoEntity::TIPO_MOVIMENTACAO_ENTRADA;
                    $dadosMovimentacao['acaoMovimentacao'] = MovimentacaoEntity::ACAO_MOVIMENTACAO_TRANSFERENCIA;
                    $dadosMovimentacao['estoque'] = $estoqueDestino;
                    $dadosMovimentacao['produto'] = $itemRecebidoTransferencia->getItemTransferencia()->getProduto()->getId();
                    $dadosMovimentacao['usuario'] = $this->dadosUser['idUsuario'];
                    $dadosMovimentacao['recebimentoTransferencia'] = $recebimentoTransferencia->getId();

                    if (!$this->movimentacaoService->save($dadosMovimentacao)) {
                        $this->getEm()->rollback();
                        return false;
                    }
                }
            }

            //Altera o status do pedido de transferência
            $pedidoTransferencia = $this->getEm()->find('Estoque\Entity\PedidoTransferencia', $recebimentoTransferencia->getPedidoTransferencia()->getId());

            if ($pedidoTransferencia->getTotalAReceber() == $pedidoTransferencia->getTotalRecebido()) {
                $pedidoTransferencia->setSituacaoTransferencia($this->getEm()->getReference('Estoque\Entity\SituacaoTransferencia', PedidoTransferenciaEntity::STATUS_ENCERRADO));
            } else {
                $pedidoTransferencia->setSituacaoTransferencia($this->getEm()->getReference('Estoque\Entity\SituacaoTransferencia', PedidoTransferenciaEntity::STATUS_RECEBIDO_INCOMPLETO));
            }

            $this->getEm()->persist($pedidoTransferencia);
            $this->getEm()->flush();

            $this->getEm()->commit();
            return true;
        } else {
            return false;
        }
    }

}
