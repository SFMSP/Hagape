<?php

namespace Estoque\Service;

use Application\Service\BaseApplicationService;
use Estoque\Entity\SolicitacaoDescarte as SolicitacaoDescarteEntity;
use Estoque\Repository\SolicitacaoDescarteRepository;
use Estoque\Repository\ItemDescarteRepository;
use Admin\Repository\EstoqueRepository;
use Estoque\Service\ItemDescarte as ItemDescarteService;
use Estoque\Service\EstoqueProduto as EstoqueProdutoService;
use Estoque\Service\Movimentacao as MovimentacaoService;
use Estoque\Entity\Movimentacao as MovimentacaoEntity;

/**
 * Classe que contém as operações realizadas na entidade de item de descarte
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Descarte extends BaseApplicationService {

    /**
     *
     * @var SolicitacaoDescarteRepository 
     */
    protected $solicitacaoDescarteRepository;

    /**
     *
     * @var ItemDescarteService 
     */
    protected $itemDescarteService;

    /**
     *
     * @var ItemDescarteRepository 
     */
    protected $itemDescarteRepository;

    /**
     *
     * @var EstoqueRepository 
     */
    protected $estoqueRepository;

    /**
     *
     * @var EstoqueProdutoService 
     */
    protected $estoqueProdutoService;

    /**
     *
     * @var MovimentacaoService 
     */
    protected $movimentacaoService;

    /**
     * 
     * @param SolicitacaoDescarteRepository $solicitacaoDescarteRepository
     * @return \Estoque\Service\SolicitacaoDescarte
     */
    public function setSolicitacaoDescarteRepository(SolicitacaoDescarteRepository $solicitacaoDescarteRepository) {
        $this->solicitacaoDescarteRepository = $solicitacaoDescarteRepository;
        return $this;
    }

    /**
     * 
     * @param ItemDescarteService $itemDescarteService
     * @return \Estoque\Service\SolicitacaoDescarte
     */
    public function setItemDescarteService(ItemDescarteService $itemDescarteService) {
        $this->itemDescarteService = $itemDescarteService;
        return $this;
    }

    /**
     * 
     * @param ItemDescarteRepository $itemDescarteRepository
     * @return \Estoque\Service\SolicitacaoDescarte
     */
    public function setItemDescarteRepository(ItemDescarteRepository $itemDescarteRepository) {
        $this->itemDescarteRepository = $itemDescarteRepository;
        return $this;
    }

    /**
     * 
     * @param EstoqueRepository $estoqueRepository
     * @return \Estoque\Service\Descarte
     */
    public function setEstoqueRepository(EstoqueRepository $estoqueRepository) {
        $this->estoqueRepository = $estoqueRepository;
        return $this;
    }

    /**
     * 
     * @param EstoqueProdutoService $estoqueProdutoService
     * @return \Estoque\Service\SolicitacaoDescarte
     */
    public function setEstoqueProdutoService(EstoqueProdutoService $estoqueProdutoService) {
        $this->estoqueProdutoService = $estoqueProdutoService;
        return $this;
    }

    /**
     * 
     * @param MovimentacaoService $movimentacaoService
     * @return \Estoque\Service\SolicitacaoDescarte
     */
    public function setMovimentacaoService(MovimentacaoService $movimentacaoService) {
        $this->movimentacaoService = $movimentacaoService;
        return $this;
    }

    public function save(array $data) {

        try {
            //Em caso de edição, verifica se o registro de fato pode ser alterado, de acordo com o seu status
            if (!empty($data['id'])) {

                $solicitacaoDescarte = $this->getEm()->find('Estoque\Entity\SolicitacaoDescarte', $data['id']);

                if (($data['acao'] == 'C' || $data['acao'] == 'CE') && $solicitacaoDescarte->getSituacaoDescarte()->getId() != SolicitacaoDescarteEntity::STATUS_CADASTRADO) {
                    return false;
                }
            }

            
            //Formata os dados para serem salvos
            $data['dataDescarte'] = new \DateTime($this->dateTimeFormat->formatUs($data['dataDescarte']));
            $data['estoqueOrigem'] = $this->getEm()->getReference("Admin\Entity\Estoque", $this->dadosUser['filtrosSelecionados']['estoque']['id']);
            $data['estoqueDestino'] = $this->getEm()->getReference("Admin\Entity\Estoque", $this->estoqueRepository->getEstoqueCentral()->getId());
            $data['usuario'] = $this->getEm()->getReference('Application\Entity\Usuario', $this->dadosUser['idUsuario']);

            if ($data['acao'] == 'CE') {
                $data['situacaoDescarte'] = $this->getEm()->getReference("Estoque\Entity\SituacaoDescarte", SolicitacaoDescarteEntity::STATUS_SOLICITADO);
            } else {
                $data['situacaoDescarte'] = $this->getEm()->getReference("Estoque\Entity\SituacaoDescarte", SolicitacaoDescarteEntity::STATUS_CADASTRADO);
            }

            if (empty($data['id'])) {
                $data['numeroDescarte'] = $this->solicitacaoDescarteRepository->getNumeroDescarteDisponivel();
            }

            $this->getEm()->beginTransaction();

            $solicitacaoDescarte = parent::save($data);
            
            //Deleta os itens do pedido caso existam ids para deletar
            if (!empty($data['idItensExcluidos']) && !empty($data['id'])) {
                $itensExcluidos = substr($data['idItensExcluidos'], 0, -1);
                $itensExcluidos = explode(',', $itensExcluidos);
                $itensExcluidos = $this->itemDescarteRepository->findIn($itensExcluidos);

                foreach ($itensExcluidos as $itemExcluido) {
                    $deleteItem = $this->itemDescarteService->delete($itemExcluido->getId());
                    $adicionaEstoque = $this->estoqueProdutoService->adicionarEstoque($itemExcluido->getProduto()->getId(), $itemExcluido->getSolicitacaoDescarte()->getEstoqueOrigem()->getId(), $itemExcluido->getQtd(), '+', false);

                    if (!$deleteItem || !$adicionaEstoque) {
                        $this->getEm()->rollback();
                        return false;
                    }

                    $dadosMovimentacao['qtd'] = $itemExcluido->getQtd();
                    $dadosMovimentacao['valorTotal'] = $itemExcluido->getValorTotal();
                    $dadosMovimentacao['dataMovimentacao'] = new \DateTime('now');
                    $dadosMovimentacao['tipoMovimentacao'] = MovimentacaoEntity::TIPO_MOVIMENTACAO_ENTRADA;
                    $dadosMovimentacao['acaoMovimentacao'] = MovimentacaoEntity::ACAO_MOVIMENTACAO_DESCARTE;
                    $dadosMovimentacao['estoque'] = $itemExcluido->getSolicitacaoDescarte()->getEstoqueOrigem()->getId();
                    $dadosMovimentacao['produto'] = $itemExcluido->getProduto()->getId();
                    $dadosMovimentacao['usuario'] = $this->dadosUser['idUsuario'];
                    $dadosMovimentacao['descarte'] = $itemExcluido->getSolicitacaoDescarte()->getId();

                    if (!$this->movimentacaoService->save($dadosMovimentacao)) {
                        $this->getEm()->rollback();
                        return false;
                    }
                }
            }

            $itensDescarte = $this->itemDescarteRepository->findBy(array('solicitacaoDescarte' => $solicitacaoDescarte->getId()));
            $itensDesc = array();

            if ($itensDescarte) {
                foreach ($itensDescarte as $itemDesc) {
                    $itensDesc[$itemDesc->getProduto()->getId()] = $itemDesc->getId();
                }
            }

            if ($solicitacaoDescarte) {

                foreach ($data['id-produto'] as $key => $item) {
                    if (!isset($itensDesc[$item])) {
                        $itemDescarte['qtd'] = $data['qtd-produto'][$key];
                        $itemDescarte['custoMedio'] = $data['preco-medio'][$key];
                        $itemDescarte['valorTotal'] = $data['valor-total'][$key];
                        $itemDescarte['solicitacaoDescarte'] = $this->getEm()->getReference('Estoque\Entity\SolicitacaoDescarte', $solicitacaoDescarte->getId());
                        $itemDescarte['produto'] = $this->getEm()->getReference('Estoque\Entity\Produto', $data['id-produto'][$key]);

                        if (!$this->itemDescarteService->save($itemDescarte)) {
                            $this->getEm()->rollback();
                            return false;
                        }

                        if (!$this->estoqueProdutoService->adicionarEstoque($data['id-produto'][$key], $solicitacaoDescarte->getEstoqueOrigem()->getId(), $data['qtd-produto'][$key], '-', false)) {
                            $this->getEm()->rollback();
                            return false;
                        }
                        $dadosMovimentacao = array();
                        $dadosMovimentacao['qtd'] = $data['qtd-produto'][$key];
                        $dadosMovimentacao['valorTotal'] = $data['valor-total'][$key];
                        $dadosMovimentacao['dataMovimentacao'] = new \DateTime('now');
                        $dadosMovimentacao['tipoMovimentacao'] = MovimentacaoEntity::TIPO_MOVIMENTACAO_SAIDA;
                        $dadosMovimentacao['acaoMovimentacao'] = MovimentacaoEntity::ACAO_MOVIMENTACAO_DESCARTE;
                        $dadosMovimentacao['estoque'] = $this->dadosUser['filtrosSelecionados']['estoque']['id'];
                        $dadosMovimentacao['produto'] = $data['id-produto'][$key];
                        $dadosMovimentacao['usuario'] = $this->dadosUser['idUsuario'];
                        $dadosMovimentacao['descarte'] = $solicitacaoDescarte->getId();

                        if (!$this->movimentacaoService->save($dadosMovimentacao)) {
                            $this->getEm()->rollback();
                            return false;
                        }
                    }
                }
            }


            $this->getEm()->commit();

            return true;
        } catch (\Exception $ex) {
            $this->log->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            $this->getEm()->rollback();
            return false;
        }
    }

    /**
     * Método que muda o status da solicitação para cancelado
     * 
     * @param integer $id
     * @return boolean
     */
    public function cancelar($id) {

        $this->getEm()->beginTransaction();

        //Busca os dados da solicitação
        $solicitacao = $this->getEm()->find('Estoque\Entity\SolicitacaoDescarte', $id);

        //Verifica se é possível realizar o cancelamento
        if ($solicitacao->getSituacaoDescarte()->getId() == SolicitacaoDescarteEntity::STATUS_CADASTRADO) {
            $update = $this->update(array('id' => $solicitacao->getId(), 'situacaoDescarte' => $this->getEm()->getReference('Estoque\Entity\SituacaoDescarte', SolicitacaoDescarteEntity::STATUS_CANCELADO)));

            if ($update) {

                $solicitacao = $this->getEm()->find('Estoque\Entity\SolicitacaoDescarte', $id);

                foreach ($solicitacao->getItensDescarte() as $item) {
                    if (!$this->estoqueProdutoService->adicionarEstoque($item->getProduto()->getId(), $solicitacao->getEstoqueOrigem()->getId(), $item->getQtd(), '+', false)) {
                        $this->getEm()->rollback();
                        return false;
                    }
                }

                //Remove todas as movimentacoes pertencentes ao pedido de transferencia
                $delete = $this->getEm()->createQueryBuilder()->delete('Estoque\Entity\Movimentacao', 'm')->where('m.descarte = :id ')->setParameter('id', $id)->getQuery()->execute();

                if ($delete) {
                    $this->getEm()->commit();
                    return true;
                } else {
                    $this->getEm()->rollback();
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Método que deleta as solicitações de descarte em massa
     * 
     * @param type $ids
     * @return boolean
     */
    public function deleteAll($ids) {
        try {
            //Busca todas as solicitações requisitadas para exclusão
            $solicitacoesDescarte = $this->solicitacaoDescarteRepository->findIn($ids);

            //Inicia a transação
            $this->getEm()->beginTransaction();

            //Percorre oas solicitações e verifica se eles podem ser excluídos
            foreach ($solicitacoesDescarte as $solicitacao) {
                if ($solicitacao->getSituacaoDescarte()->getId() == SolicitacaoDescarteEntity::STATUS_CADASTRADO) {
                    foreach ($solicitacao->getItensDescarte() as $item) {
                        $this->getEm()->remove($this->getEm()->getReference('Estoque\Entity\ItemDescarte', $item->getId()));
                        $this->getEm()->flush();
                        $this->getEm()->clear();

                        if (!$this->estoqueProdutoService->adicionarEstoque($item->getProduto()->getId(), $solicitacao->getEstoqueOrigem()->getId(), $item->getQtd(), '+', false)) {
                            $this->getEm()->rollback();
                            return false;
                        }
                    }

                    //Remove todas as movimentacoes pertencentes ao pedido de transferencia
                    $deleteMovimentacao = $this->getEm()->createQueryBuilder()->delete('Estoque\Entity\Movimentacao', 'm')->where('m.descarte = :id ')->setParameter('id', $solicitacao->getId())->getQuery()->execute();

                    if (!$deleteMovimentacao) {
                        $this->getEm()->rollback();
                        return false;
                    }

                    $delete = parent::delete($solicitacao->getId());

                    if (!$delete) {
                        $this->getEm()->rollback();
                        return false;
                    }
                } else {
                    $this->getEm()->rollback();
                    return false;
                }
            }

            //Commita a transação
            $this->getEm()->commit();
            return true;
        } catch (\Exception $ex) {
            $this->log->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            $this->getEm()->rollback();
            return false;
        }
    }

}
