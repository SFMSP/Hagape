<?php

namespace Estoque\Controller;

use Zend\View\Model\ViewModel;
use Zend\Session\Container as SessionConteiner;
use Application\Controller\CrudApplicationController;
use Estoque\Entity\PedidoTransferencia as PedidoTransferenciaEntity;


class RecebimentoTransferenciaController extends CrudApplicationController {

    protected $nameForm = 'Estoque\Form\RecebimentoTransferencia';
    protected $nameRepository = 'Estoque\Repository\PedidoTransferenciaRepository';
    protected $nameService = 'Estoque\Service\RecebimentoTransferencia';
    protected $nameFormFilter = 'Estoque\Form\TransferenciaFiltro';
    protected $controller = 'recebimentoTransferencia';
    protected $route = 'estoque/default';
    protected $jsFormulario = array('recebimento-transferencia/formulario.js');
    protected $jsIndex = array('recebimento-transferencia/index.js');

    /**
     * Método chamado via ajax para listagem com datatables
     * 
     * @return type
     */
    public function paginacaoAction() {

        //Armazena a resposta
        $response = $this->getResponse();

        //Armazena a requisição
        $request = $this->getRequest()->getPost();

        //Busca os estoques do usuário na sessão 
        $dadosUser = $this->getDadosUser();
        $filtrosUser = $this->getFiltrosSelecionadosUser();

        //Organiza os dados da busca
        $busca = array(
            "busca" => mb_strtolower($request['search']['value'], 'UTF-8'),
            "estoques" => $dadosUser['estoques'],
            "situacao" => $request['situacao']
        );

        //Cria a ordenação
        $order = array();

        if (isset($request['order'][0]['column'])) {
            switch ($request['order'][0]['column']) {
                case 1: $order = array('field' => 'pt.numeroTransferencia', 'order' => $request['order'][0]['dir']);
                    break;
                case 2: $order = array('field' => 'u.nome', 'order' => $request['order'][0]['dir']);
                    break;
                case 3: $order = array('field' => 'pt.dataTransferencia', 'order' => $request['order'][0]['dir']);
                    break;
                case 4: $order = array('field' => 'rt.dataRecebimentoTransferencia', 'order' => $request['order'][0]['dir']);
                    break;
            }
        }

        //Busca os dados
        $registros = $this->getServiceLocator()->get($this->nameRepository)->buscaRecebimentos($busca, $request['length'], $request['start'], $order);
        $registrosTotaisBusca = $this->getServiceLocator()->get($this->nameRepository)->buscaRecebimentos($busca, 0, 0, array(), true);
        $registrosTotais = $this->getServiceLocator()->get($this->nameRepository)->countAll();
        $dados = array();
   
        //Armazena os dados retornados em um array
        foreach ($registros as $registro) {
            $valor = array();
            $valor[] = '<input type="checkbox" name="checkbox-list" class="checkboxes" value="' . $registro->getId() . '" />';

            
            if ($this->verifyPermission('recebimentoTransferencia', 'incluir')  
                    && $registro->getSituacaoTransferencia()->getId() == PedidoTransferenciaEntity::STATUS_EM_TRANSITO 
                    && $registro->getEstoqueDestino()->getId() == $filtrosUser['estoque']['id']) {

                $arr = array(
                    'route' => $this->route,
                    'controller' => $this->controller,
                    'action' => 'formulario',
                    'id' => $registro->getId()
                );

                $title = 'Incluir Recebimento';
            } else {
                $arr = array(
                    'route' => $this->route,
                    'controller' => $this->controller,
                    'action' => 'visualizar',
                    'id' => $registro->getId()
                );

                $title = 'Visualizar Item';
            }

            $valor[] = "<a href='" . $this->urls($arr) . "' title='" . $title . "'>" . $registro->getNumeroTransferencia() . "</a>";

            $valor[] = $registro->getUsuario()->getNome();
            $valor[] = "R$ " . number_format($registro->getValorTotalPedidoTransferencia(), 2, ',', '.');
            
            $classCSS = $this->getLabelSituacao($registro->getSituacaoTransferencia()->getNome());
            
          
            $valor[] = $registro->getSituacaoTransferencia() ? "<span class='label label-sm ".$classCSS."'>".$registro->getSituacaoTransferencia()->getNome()."</span>" : "";
              

            
            $valor[] = $registro->getDataTransferencia()->format('d/m/Y');
            $valor[] = $registro->getRecebimentoTransferencia() ? $registro->getRecebimentoTransferencia()->getDataRecebimentoTransferencia()->format('d/m/Y') : "Recebimento não Registrado";
            $valor[] = "";
            $dados[] = $valor;
        }

        //Organiza o retorno
        $retorno['draw'] = $request['draw'];
        $retorno['recordsTotal'] = $registrosTotais;
        $retorno['recordsFiltered'] = $registrosTotaisBusca;
        $retorno['data'] = $dados;


        //Retorna a resposta
        $response->setContent(\Zend\Json\Json::encode($retorno));
        return $response;
    }

    /**
     * Método padrão de formulário
     * @return \Application\Controller\ViewModel
     */
    public function formularioAction() {

        //Verifica a permissão de acesso ao formulário
        if ($this->params()->fromRoute('id')) {
            $this->validarPermissaoFormulario($this->params()->fromRoute('id'));
        } else {
            $this->redirect()->toRoute('home/default', array('controller' => 'home', 'action' => 'index'));
        }

        //Obtem a lista de itens do pedido
        $itensTransferencia = $this->getServiceLocator()->get('Estoque\Repository\ItemTransferenciaRepository')->findBy(array('pedidoTransferencia' => $this->params()->fromRoute('id')));

        //Instancia o formulário e a requisição
        $form = $this->getServiceLocator()->get($this->nameForm);
        $request = $this->getRequest();
        $qtdRecebida = array();
        $pedidoTransferencia = $this->params()->fromRoute('id');

        //Verifica se a requisição foi feita via post
        if ($request->isPost()) {

            //Manda os dados para o formulário
            $dados = $request->getPost()->toArray();
            $form->setData($dados);

            //Valida o formulário
            if ($form->valid($dados['qtdRecebida'], $dados['itemRecebido'])) {
                $dados['pedidoTransferencia'] = $pedidoTransferencia;
                $service = $this->getServiceLocator()->get($this->nameService);
                $success = $service->save($dados);

                $this->setConfirmMessages($success);

                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            } else {
                $qtdRecebida = $dados['qtdRecebida'];
                $this->flashMessenger()->addErrorMessage('Por favor, verifique os campos do formulário');
            }
        }else if ($this->params()->fromRoute('id')) {
            //Busca os dados do Recebimento da Transferencia usando o ID do Pedido de Transferencia
            $dados = $this->getServiceLocator()
                ->get('Estoque\Repository\RecebimentoTransferenciaRepository')
                ->findBy(["pedidoTransferencia" => $this->params()->fromRoute('id')]);
            if($dados){
                $form->get('dataRecebimento')->setValue($dados[0]->getDataRecebimentoTransferencia()->format('d/m/Y'));
                $form->get('nomeEntregador')->setValue($dados[0]->getNomeEntregador());
                $form->get('cpfRg')->setValue($dados[0]->getCpfRg());
                $form->get('observacao')->setValue($dados[0]->getObservacao());
            }
        }

        //Seta os scripts e retorna para view
        if (!empty($this->jsFormulario)) {
            $this->layout()->setVariable('scripts', $this->jsFormulario);
        }

        if (!empty($this->cssFormulario)) {
            $this->layout()->setVariable('styles', $this->cssFormulario);
        }


        //Retorna os dados da view Model
        return new ViewModel(array('form' => $form, 'itensTransferencia' => $itensTransferencia, 'qtdRecebida' => $qtdRecebida, 'pedidoTransferencia' => $pedidoTransferencia));
    }

    public function visualizarAction() {
        return $this->formularioAction();
    }

    /**
     * Método que valida se o usuário possuí permissão no formulário, levando em consideração o id do pedido de transferência
     * 
     * @return boolean
     */
    private function validarPermissaoFormulario($pedidoTransferencia) {
        $filtrosUser = $this->getFiltrosSelecionadosUser();
        $pedidoTransferencia = $this->getServiceLocator()->get('Estoque\Repository\PedidoTransferenciaRepository')->find($pedidoTransferencia);
        
        if ($pedidoTransferencia
                && $pedidoTransferencia->getEstoqueDestino()->getId() != $filtrosUser['estoque']['id'] 
                && $this->params()->fromRoute('action') == 'formulario') {
            
            return $this->redirect()->toRoute('home/default', array('controller' => 'home', 'action' => 'index'));
        } else {
            return true;
        }
    }

}
