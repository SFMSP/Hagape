<?php

namespace Estoque\Controller;

use Zend\View\Model\ViewModel;
use Zend\Session\Container as SessionContainer;
use Application\Controller\CrudApplicationController;
use Estoque\Entity\PedidoTransferencia;

class TransferenciaController extends CrudApplicationController {

    protected $nameForm = 'Estoque\Form\Transferencia';
    protected $nameRepository = 'Estoque\Repository\PedidoTransferenciaRepository';
    protected $nameService = 'Estoque\Service\PedidoTransferencia';
    protected $nameFormFilter = 'Estoque\Form\TransferenciaFiltro';
    protected $controller = 'transferencia';
    protected $route = 'estoque/default';
    protected $jsFormulario = array('transferencia/formulario.js');
    protected $jsIndex = array('transferencia/index.js');

    public function indexAction() {

        //Seta os scripts e retorna para view
        if (!empty($this->jsIndex)) {
            $this->layout()->setVariable('scripts', $this->jsIndex);
        }

        if (!empty($this->cssIndex)) {
            $this->layout()->setVariable('styles', $this->cssIndex);
        }

        //Busca mensagens a serem exibidas na tela
        $messages = $this->flashMessenger()->setNamespace('Application')->getMessages();

        //Busca o formulário do filtro
        $formFilter = !empty($this->nameFormFilter) ? $this->getServiceLocator()->get($this->nameFormFilter) : null;

        //Retorna os dados para view
        return new ViewModel(array('messages' => $messages, 'formFilter' => $formFilter));
    }

    public function paginacaoAction() {


        //Armazena a resposta
        $response = $this->getResponse();

        //Armazena a requisição
        $request = $this->getRequest()->getPost();

        //Busca os filtros do usuário
        $dadosUser = $this->getDadosUser();

        $filtrosSelecionados = $this->getFiltrosSelecionadosUser();

        //Organiza os dados da busca
        $busca = array(
            "estoques" => $dadosUser['estoques'],
            "busca" => mb_strtolower($request['search']['value'], 'UTF-8'),
            "situacao" => $request['situacao']
        );

        //Cria a ordenação
        $order = array();

        if (isset($request['order'][0]['column'])) {
            switch ($request['order'][0]['column']) {
                case 1: $order = array('field' => 'pt.numeroTransferencia', 'order' => $request['order'][0]['dir']);
                    break;
                case 2: $order = array('field' => 'pt.dataTransferencia', 'order' => $request['order'][0]['dir']);
                    break;
                case 3: $order = array('field' => 'e.nome', 'order' => $request['order'][0]['dir']);
                    break;
                case 4: $order = array('field' => 's.nome', 'order' => $request['order'][0]['dir']);
                    break;
            }
        }

        //Busca os dados
        $registros = $this->getServiceLocator()->get($this->nameRepository)->busca($busca, $request['length'], $request['start'], $order);
        $registrosTotaisBusca = $this->getServiceLocator()->get($this->nameRepository)->busca($busca, 0, 0, array(), true);
        $registrosTotais = $this->getServiceLocator()->get($this->nameRepository)->countAll();
        $dados = array();

        //Armazena os dados retornados em um array
        foreach ($registros as $registro) {
            $valor = array();
            $valor[] = '<input type="checkbox" name="checkbox-list" class="checkboxes" value="' . $registro->getId() . '" />';

            if ($this->verifyPermission('transferencia', 'editar') && $registro->getEstoqueOrigem()->getId() == $filtrosSelecionados['estoque']['id']) {

                $arr = array(
                    'route' => $this->route,
                    'controller' => $this->controller,
                    'action' => 'formulario',
                    'id' => $registro->getId()
                );

                $title = 'Editar Item';
            } else {
                $arr = array(
                    'route' => $this->route,
                    'controller' => $this->controller,
                    'action' => 'visualizar',
                    'id' => $registro->getId()
                );

                $title = 'Visualizar Item';
            }

            $links = "";

            if ($this->verifyPermission('transferencia', 'cancelarTransferencia') && $registro->getEstoqueOrigem()->getId() == $filtrosSelecionados['estoque']['id']) {
                $links .= "<a href='javascript:cancelarTransferencia(" . $registro->getId() . ");' class='fa fa-ban'><div class='tool-tip slideIn top'>Cancelar</div></a> ";
            }

            if ($this->verifyPermission('transferencia', 'estornarTransferencia') && $registro->getEstoqueOrigem()->getId() == $filtrosSelecionados['estoque']['id'] && ($registro->getSituacaoTransferencia()->getId() == PedidoTransferencia::STATUS_RECEBIDO_INCOMPLETO || $registro->getSituacaoTransferencia()->getId() == PedidoTransferencia::STATUS_EM_TRANSITO)) {
                $links .= "&nbsp;&nbsp;&nbsp;&nbsp;<a  href='" . $this->urls(array('route' => $this->route, 'controller' => $this->controller, 'action' => 'estorno', 'id' => $registro->getId())) . "' class='glyphicon glyphicon-repeat colorbox'><div class='tool-tip slideIn top'>Estornar</div></a> ";
            }

            $valor[] = "<a href='" . $this->urls($arr) . "' title='" . $title . "'>" . $registro->getNumeroTransferencia() . "</a>";
            $valor[] = $registro->getDataTransferencia() ? $registro->getDataTransferencia()->format('d/m/Y') : '';
            $valor[] = $registro->getEstoqueOrigem()->getNome();
            $valor[] = $registro->getEstoqueDestino()->getNome();
            
            $classCSS = $this->getLabelSituacao($registro->getSituacaoTransferencia()->getNome());
            
            $valor[] = $registro->getSituacaoTransferencia() ? "<span class='label label-sm ".$classCSS."'>".$registro->getSituacaoTransferencia()->getNome()."</span>" : "";
            $valor[] = "R$ " . number_format($registro->getValorTotalPedidoTransferencia(), 2, ',', '.');
            $valor[] = $links;
            $dados[] = $valor;
        }

        //Organiza o retorno
        $retorno['draw'] = $request['draw'];
        $retorno['recordsTotal'] = $registrosTotais;
        $retorno['recordsFiltered'] = $registrosTotaisBusca;
        $retorno['data'] = $dados;


        //Retorna a resposta
        $response->setContent(\Zend\Json\Json::encode($retorno));
        return $response;
    }

    /**
     * Método padrão de formulário
     * @return \Application\Controller\ViewModel
     */
    public function formularioAction() {
        //Habilita o formulário por padrão
        $enabledForm = true;

        //Valida a permissão na edição, verificando se o usuário está setado como estoque central
        if ($this->params()->fromRoute('id')) {
            $enabledForm = $this->validarPermissaoFormulario($this->params()->fromRoute('id'));
        }

        //Instancia o formulário e a requisição
        $form = $this->getServiceLocator()->get($this->nameForm);
        $request = $this->getRequest();
        $itensPedido = array();

        //Verifica se a requisição foi feita via post
        if ($request->isPost()) {

            //Busca os dados
            $dados = $request->getPost()->toArray();

            if ($this->params()->fromRoute('id')) {
                $dados['id'] = $this->params()->fromRoute('id');
            }

            $form->setData($dados);

            //Prepara as variáveis referentes aos itens do pedido para validação
            $itensProduto = isset($dados['id-produto']) ? $dados['id-produto'] : array();
            $itensQuantidade = isset($dados['qtd-produto']) ? $dados['qtd-produto'] : array();
            $valorTotal = isset($dados['valor-total']) ? $dados['valor-total'] : array();
            $precoMedio = isset($dados['preco-medio']) ? $dados['preco-medio'] : array();
            $itensTransferencia = isset($dados['id-item-transferencia']) ? $dados['id-item-transferencia'] : array();

            //Valida o formulário
            if ($form->valid($itensProduto, $itensQuantidade, $valorTotal, $precoMedio, $itensTransferencia)) {
                $service = $this->getServiceLocator()->get($this->nameService);
                $success = $service->save($dados);
                $this->setConfirmMessages($success);

                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            } else {

                //Cria um array com todos os itens do pedido para serem reenseridos no form
                $itensPedido = $this->recomporItensPedidoForm($dados);

                //Retira os produtos que já haviam sido selecionados caso eles existam
                if (isset($dados['id-produto'])) {
                    $form->retirarOpcoesProdutos($dados['id-produto']);
                }

                $this->flashMessenger()->addErrorMessage('Por favor, verifique os campos do formulário');
            }
        } elseif ($this->params()->fromRoute('id')) {

            //Busca os dados
            $dados = $this->getServiceLocator()->get($this->nameRepository)->find($this->params()->fromRoute('id'));

            if ($dados) {

                //Popula os itens do pedido
                $itensPedido = $this->recomporItensPedidoEntity($dados);

                //Retira das opções os produtos selecionados
                $form->retirarOpcoesProdutos($dados->getIdProdutosPedido());

                //Passa os dados para o form
                $form->setData($dados->toArray());

                //Verifica se o formulário deve ficar habilitado para a edição
                $enabledForm = $dados->getSituacaoTransferencia()->getId() == PedidoTransferencia::STATUS_CADASTRADO ? true : false;
            }
        }

        //Seta os scripts e retorna para view
        if (!empty($this->jsFormulario)) {
            $this->layout()->setVariable('scripts', $this->jsFormulario);
        }

        if (!empty($this->cssFormulario)) {
            $this->layout()->setVariable('styles', $this->cssFormulario);
        }

        return new ViewModel(array('form' => $form, 'itensPedido' => $itensPedido, 'enabledForm' => $enabledForm));
    }

    /**
     * Ação de Visualizar
     * @return type
     */
    public function visualizarAction() {
        if (!$this->params()->fromRoute('id')) {
            return $this->redirect()->toRoute('home/default', array('controller' => 'home', 'action' => 'index'));
        }

        return $this->formularioAction();
    }

    /**
     * Ação de Cancelar
     * 
     * @return boolean
     */
    public function cancelarTransferenciaAction() {
        $post = $this->getRequest()->getPost()->toArray();

        if (!$this->validarPermissaoFormulario($post['id'])) {
            return false;
        }

        if ($this->getServiceLocator()->get($this->nameService)->cancelar($post['id'])) {
            die(\Zend\Json\Json::encode(array('success' => 1)));
        } else {
            die(\Zend\Json\Json::encode(array('success' => 0)));
        }
    }

    /**
     * Ação de Deletar
     * 
     * @return boolean
     */
    public function deletarAction() {
        $dados = $this->getRequest()->getPost();

        foreach ($dados['ids'] as $id) {
            if (!$this->validarPermissaoFormulario($id)) {
                return false;
            }
        }

        parent::deletarAction();
    }

    public function estornoAction() {

        $form = $this->getServiceLocator()->get('Estoque\Form\Estorno');
        $request = $this->getRequest();
        $pedidoTransferencia = $this->getServiceLocator()->get('Estoque\Repository\PedidoTransferenciaRepository')->find($this->params()->fromRoute('id'));
        $estornos = $this->getServiceLocator()->get('Estoque\Repository\EstornoRepository')->getEstornosPedidoTransferencia($this->params()->fromRoute('id'));
        
        if ($request->isPost()) {
            if ($this->getServiceLocator()->get('Estoque\Service\Estorno')->save($request->getPost()->toArray())) {
                $this->setConfirmMessages(true);
                die(\Zend\Json\Json::encode(array('success' => 1 )));
            } else {
                $this->flashMessenger()->addErrorMessage('Erro ao realizar estorno!');
                die(\Zend\Json\Json::encode(array('success' => 0 )));
            }
        }

        $viewModel = new ViewModel(array('form' => $form, 'pedidoTransferencia' => $pedidoTransferencia, 'id' => $this->params()->fromRoute('id'), 'estornos' => $estornos ));
        $viewModel->setTerminal(true);

        return $viewModel;
    }

    /**
     * Método chamado via ajax que retorna a quantidade de produtos disponíveis em um determinado estoque
     */
    public function getQuantidadeDisponivelAction() {
        $request = $this->getRequest()->getPost()->toArray();
        $filtrosUser = $this->getFiltrosSelecionadosUser();
        $quantidade = $this->getServiceLocator()->get('Estoque\Repository\EstoqueProdutoRepository')->getQuantidadeDisponivelEstoque($request['produto'], $filtrosUser['estoque']['id']);

        if ($request['pedidoTransferencia']) {
            $itemTransferencia = $this->getServiceLocator()->get('Estoque\Repository\ItemTransferenciaRepository')->findOneBy(array('pedidoTransferencia' => $request['pedidoTransferencia'], 'produto' => $request['produto']));
            $quantidade += $itemTransferencia ? $itemTransferencia->getQtd() : 0;
        }

        die(\Zend\Json\Json::encode(array('quantidade' => $quantidade)));
    }

    private function validarPermissaoCentral($redirect = false) {

        //Valida se o usuário logado possuí o estoque central selecionado por padrão
        $dadosUser = $this->getDadosUser();
        $central = $dadosUser['filtrosSelecionados']['estoque']['central'];

        if (($this->params()->fromRoute('action') == 'formulario' || $this->params()->fromRoute('action') == 'deletar' || $this->params()->fromRoute('action') == 'index' || $this->params()->fromRoute('action') == 'paginacao' || $this->params()->fromRoute('action') == 'cancelarPedido') && !$central) {
            return $redirect ? $this->redirect()->toRoute('home/default', array('controller' => 'home', 'action' => 'index')) : false;
        } else {
            return true;
        }
    }

    /**
     * Método que valida se o usuário possuí permissão no formulário, levando em consideração o id do pedido de transferência
     * 
     * @return boolean
     */
    private function validarPermissaoFormulario($pedidoTransferencia) {
        $filtrosSelecionados = $this->getFiltrosSelecionadosUser();
        $pedidoTransferencia = $this->getServiceLocator()->get($this->nameRepository)->findOneBy(array('id' => $pedidoTransferencia, 'estoqueOrigem' => $filtrosSelecionados['estoque']['id']));

        if (!$pedidoTransferencia) {
            if ($this->params()->fromRoute('action') == 'visualizar') {
                return false;
            } else {
                return $this->redirect()->toRoute('home/default', array('controller' => 'home', 'action' => 'index'));
            }
        } else {
            return true;
        }
    }

    /**
     * Organiza os itens para serem recompostos no form 
     * 
     * @param array $dados
     * @return array
     */
    private function recomporItensPedidoForm($dados) {

        //Cria o array que armazenará os itens de forma organizada para form
        $itens = array();

        if (isset($dados['id-produto'])) {
            foreach ($dados['id-produto'] as $key => $item) {
                $itens[$key]['id-item-transferencia'] = $dados['id-item-transferencia'][$key];
                $itens[$key]['id-produto'] = $dados['id-produto'][$key];
                $itens[$key]['codigo-produto'] = $dados['codigo-produto'][$key];
                $itens[$key]['nome-produto'] = $dados['nome-produto'][$key];
                $itens[$key]['preco-medio'] = $dados['preco-medio'][$key];
                $itens[$key]['qtd-produto'] = $dados['qtd-produto'][$key];
                $itens[$key]['valor-total'] = $dados['valor-total'][$key];
            }
        }

        return $itens;
    }

    /**
     * Organiza os itens para serem recompostos no form 
     * 
     * @param PedidoTransferencia $pedidoTransferencia
     * @return array
     */
    private function recomporItensPedidoEntity(PedidoTransferencia $pedidoTransferencia) {

        //Cria o array que armazenará os itens de forma organizada para form
        $itens = array();

        foreach ($pedidoTransferencia->getItensTransferencia() as $key => $item) {
            $itens[$key]['id-item-transferencia'] = $item->getId();
            $itens[$key]['id-produto'] = $item->getProduto()->getId();
            $itens[$key]['codigo-produto'] = $item->getProduto()->getCodigo();
            $itens[$key]['nome-produto'] = $item->getProduto()->getNome();
            $itens[$key]['preco-medio'] = $item->getCustoMedio();
            $itens[$key]['qtd-produto'] = $item->getQtd();
            $itens[$key]['valor-total'] = $item->getValorTotal();
        }

        return $itens;
    }

}
