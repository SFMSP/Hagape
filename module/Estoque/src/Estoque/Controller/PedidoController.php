<?php

namespace Estoque\Controller;

use Zend\View\Model\ViewModel;
use Zend\Session\Container as SessionContainer;
use Application\Controller\CrudApplicationController;
use Estoque\Entity\Pedido;

class PedidoController extends CrudApplicationController {

    protected $nameForm = 'Estoque\Form\Pedido';
    protected $nameRepository = 'Estoque\Repository\PedidoRepository';
    protected $nameService = 'Estoque\Service\Pedido';
    protected $nameFormFilter = 'Estoque\Form\PedidoFiltro';
    protected $controller = 'pedido';
    protected $route = 'estoque/default';
    protected $jsFormulario = array('pedido/formulario.js');
    protected $jsIndex = array('pedido/index.js');

    public function indexAction() {
        //Verifica se o usuário logado é do estoque central
        $central = $this->validarPermissaoCentral();

        //Seta os scripts e retorna para view
        if (!empty($this->jsIndex)) {
            $this->layout()->setVariable('scripts', $this->jsIndex);
        }

        if (!empty($this->cssIndex)) {
            $this->layout()->setVariable('styles', $this->cssIndex);
        }

        //Busca mensagens a serem exibidas na tela
        $messages = $this->flashMessenger()->setNamespace('Application')->getMessages();

        //Busca o formulário do filtro
        $formFilter = !empty($this->nameFormFilter) ? $this->getServiceLocator()->get($this->nameFormFilter) : null;

        //Retorna os dados para view
        return new ViewModel(array('messages' => $messages, 'formFilter' => $formFilter, 'central' => $central));
    }

    public function paginacaoAction() {

        //Verifica se o usuário logado é do estoque central
        $central = $this->validarPermissaoCentral();

        //Armazena a resposta
        $response = $this->getResponse();

        //Armazena a requisição
        $request = $this->getRequest()->getPost();

        //Organiza os dados da busca
        $busca = array(
            "busca" => mb_strtolower($request['search']['value'], 'UTF-8'),
            "situacao" => $request['situacao']
        );

        //Cria a ordenação
        $order = array();

        if (isset($request['order'][0]['column'])) {
            switch ($request['order'][0]['column']) {
                case 1: $order = array('field' => 'p.numeroPedido', 'order' => $request['order'][0]['dir']);
                    break;
                case 2: $order = array('field' => 'e.numEmpenho', 'order' => $request['order'][0]['dir']);
                    break;
                case 3: $order = array('field' => 'p.dataSolicitacao', 'order' => $request['order'][0]['dir']);
                    break;
                case 4: $order = array('field' => 'f.nome', 'order' => $request['order'][0]['dir']);
                    break;
                case 5: $order = array('field' => 's.nome', 'order' => $request['order'][0]['dir']);
                    break;
            }
        }

        //Busca os dados
        $registros = $this->getServiceLocator()->get($this->nameRepository)->busca($busca, $request['length'], $request['start'], $order);
        $registrosTotaisBusca = $this->getServiceLocator()->get($this->nameRepository)->busca($busca, 0, 0, array(), true);
        $registrosTotais = $this->getServiceLocator()->get($this->nameRepository)->countAll();
        $dados = array();

        //Armazena os dados retornados em um array
        foreach ($registros as $registro) {
            $valor = array();
            $valor[] = '<input type="checkbox" name="checkbox-list" class="checkboxes" value="' . $registro->getId() . '" />';

            if ($this->verifyPermission('pedido', 'editar') && $central) {

                $arr = array(
                    'route' => $this->route,
                    'controller' => $this->controller,
                    'action' => 'formulario',
                    'id' => $registro->getId()
                );

                $title = 'Editar Item';
            } else {
                $arr = array(
                    'route' => $this->route,
                    'controller' => $this->controller,
                    'action' => 'visualizar',
                    'id' => $registro->getId()
                );

                $title = 'Visualizar Item';
            }

            $valor[] = "<a href='" . $this->urls($arr) . "' title='" . $title . "'>" . $registro->getNumeroPedido() . "</a>";
            $valor[] = $registro->getEmpenho()->getNumEmpenho();
            $valor[] = $registro->getDataSolicitacao() ? $registro->getDataSolicitacao()->format('d/m/Y') : '';
            $valor[] = $registro->getFornecedor()->getNome();
            $classCSS = $this->getLabelSituacao($registro->getSituacao()->getNome());
            $valor[] = $registro->getSituacao() ? "<span class='label label-sm ".$classCSS."'>".$registro->getSituacao()->getNome()."</span>" : "";
            $valor[] = "R$ " . number_format($registro->getValorTotalPedido(), 2, ',', '.');
            $valor[] = $this->verifyPermission('pedido', 'cancelarPedido') && $central && ($registro->getSituacao()->getid() == \Estoque\Entity\Pedido::STATUS_SOLICITADO) ? "<a href='javascript:showModalCancelarPedido(" . $registro->getId() . ");' class='fa fa-ban' ><div class='tool-tip slideIn top'>Cancelar</div></a> " : "";
            $dados[] = $valor;
        }

        //Organiza o retorno
        $retorno['draw'] = $request['draw'];
        $retorno['recordsTotal'] = $registrosTotais;
        $retorno['recordsFiltered'] = $registrosTotaisBusca;
        $retorno['data'] = $dados;


        //Retorna a resposta
        $response->setContent(\Zend\Json\Json::encode($retorno));
        return $response;
    }

    /**
     * Método padrão de formulário
     * @return \Application\Controller\ViewModel
     */
    public function formularioAction() {

        //Valida a permissão na inclusão/edição, verificando se o usuário está setado como estoque central
        $this->validarPermissaoFormulario();

        //Valida se o usuário é do estoque central, par poder acessar a página
        $this->validarPermissaoCentral(true);

        //Instancia o formulário e a requisição
        $form = $this->getServiceLocator()->get($this->nameForm);
        $request = $this->getRequest();
        $pedido = null;
        $itensPedido = array();
        $enabledForm = true;

        //Verifica se a requisição foi feita via post
        if ($request->isPost()) {

            //Busca os dados
            $dados = $request->getPost()->toArray();

            if ($this->params()->fromRoute('id')) {
                $dados['id'] = $this->params()->fromRoute('id');
            }

            //Injeta os valores no formulário
            if ($dados['fornecedor']) {
                $contratos = $this->getContratosAction($dados['fornecedor'], false, 'Selecione um Contrato');
                $form->get('contrato')->setValueOptions($contratos);
            }

            if ($dados['contrato']) {
                $empenhos = $this->getEmpenhosAction($dados['contrato'], false, 'Selecione um Empenho');
                $form->get('empenho')->setValueOptions($empenhos);
            }

            $form->setData($dados);

            //Prepara as variáveis referentes aos itens do pedido para validação
            $itensQuantidade = isset($dados['qtd-produto']) ? $dados['qtd-produto'] : array();
            $itensPrecoUnitario = isset($dados['preco-unitario-produto']) ? $dados['preco-unitario-produto'] : array();
            $valorEmpenho = !empty($dados['empenho']) ? $this->getEmpenhoDisponivelAction($dados['empenho'], false, $dados['id'] ? $dados['id'] : false) : 0;

            //Valida o formulário
            if ($form->valid($itensQuantidade, $itensPrecoUnitario, $valorEmpenho)) {
                $service = $this->getServiceLocator()->get($this->nameService);
                $success = $service->save($dados);
                $this->setConfirmMessages($success);

                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            } else {

                //Cria um array com todos os itens do pedido para serem reenseridos no form
                $itensPedido = $this->recomporItensPedidoForm($dados);

                //Retira os produtos que já haviam sido selecionados caso eles existam
                if (isset($dados['id-produto'])) {
                    $form->retirarOpcoesProdutos($dados['id-produto']);
                }

                $this->flashMessenger()->addErrorMessage('Por favor, verifique os campos do formulário');
            }
        } elseif ($this->params()->fromRoute('id')) {

            //Busca os dados
            $dados = $this->getServiceLocator()->get($this->nameRepository)->find($this->params()->fromRoute('id'));

            if ($dados) {

                //Repopula os campos de contrato e empenho
                $form->get('contrato')->setValueOptions($this->getContratosAction($dados->getContrato()->getFornecedor()->getId(), false, 'Selecione um Contrato'));
                $form->get('empenho')->setValueOptions($this->getEmpenhosAction($dados->getEmpenho()->getContrato()->getId(), false, 'Selecione um Empenho'));

                //Popula o campo referente ao valor de empenho disponível
                $form->get('valor-empenho-disponivel')->setValue($this->getEmpenhoDisponivelAction($dados->getEmpenho()->getId(), false));

                //Popula os itens do pedido
                $itensPedido = $this->recomporItensPedidoEntity($dados);

                //Retira das opções os produtos selecionados
                $form->retirarOpcoesProdutos($dados->getIdProdutosPedido());

                $classCSS = $this->getLabelSituacao($dados->getSituacao()->getNome());
              
                $dados->getSituacao()->setNome("<span class='label label-sm ".$classCSS."'>".$dados->getSituacao()->getNome()."</span>");
                
              
             //Passa os dados para o form
                $form->setData($dados->toArray());
                //Verifica se o formulário deve ficar habilitado para a edição
                $enabledForm = $dados->getSituacao()->getId() == Pedido::STATUS_CADASTRADO ? true : false;

                //Armazena o objeto pedido na variável que será enviada para view
                $pedido = $dados;
            }
        }
        if ($this->verifyPermission('pedido', 'incluir')) {
            //Seta os scripts e retorna para view
        if (!empty($this->jsFormulario)) {
            $this->layout()->setVariable('scripts', $this->jsFormulario);
        }else{
            //Seta os scripts e retorna para view
             $this->layout()->setVariable('scripts', array('contrato/formularioVisualiza.js'));
            }
}
        

        if (!empty($this->cssFormulario)) {
            $this->layout()->setVariable('styles', $this->cssFormulario);
        }

        //Retorna os dados da view Model
        return new ViewModel(array('form' => $form, 'itensPedido' => $itensPedido, 'enabledForm' => $enabledForm, 'pedido' => $pedido));
    }

    public function visualizarAction() {
        return $this->formularioAction();
    }

    public function cancelarPedidoAction() {

        if (!$this->validarPermissaoCentral()) {
            return false;
        }
        
        $post = $this->getRequest()->getPost()->toArray();

        if ($this->getServiceLocator()->get($this->nameService)->cancelar($post['id'])) {
            die(\Zend\Json\Json::encode(array('success' => 1)));
        } else {
            die(\Zend\Json\Json::encode(array('success' => 0)));
        }
    }

    public function deletarAction() {
        if (!$this->validarPermissaoCentral()) {
            return false;
        }

        parent::deletarAction();
    }

    /**
     * Redireciona o usuário para a página de recebimentos
     * 
     * @return type
     */
    public function redirecionarRecebimentoAction() {

        //Cria uma sessão passando o id do pedido
        $container = new SessionContainer('PedidoRecebimento');
        $container->pedido = $this->params()->fromRoute('id');

        //Redireciona para a aba de recebimentos
        return $this->redirect()->toRoute('estoque/default', array('controller' => 'recebimento', 'action' => 'index'));
    }

    private function validarPermissaoCentral($redirect = false) {

        //Valida se o usuário logado possuí o estoque central selecionado por padrão
        $dadosUser = $this->getDadosUser();
        $central = $dadosUser['filtrosSelecionados']['estoque']['central'];

        if (($this->params()->fromRoute('action') == 'formulario' || $this->params()->fromRoute('action') == 'deletar' || $this->params()->fromRoute('action') == 'index' || $this->params()->fromRoute('action') == 'paginacao' || $this->params()->fromRoute('action') == 'cancelarPedido') && !$central) {
            return $redirect ? $this->redirect()->toRoute('home/default', array('controller' => 'home', 'action' => 'index')) : false;
        } else {
            return true;
        }
    }

    /**
     * Método que valida se o usuário possuí permissão no formulário, levando em consideração o status do pedido
     * s
     * @param type $situacaoPedido
     * @return boolean
     */
    private function validarPermissaoFormulario() {
        return true;
    }

    /**
     * Organiza os itens para serem recompostos no form 
     * @param array $dados
     * @return array
     */
    private function recomporItensPedidoForm($dados) {

        //Cria o array que armazenará os itens de forma organizada para form
        $itens = array();

        if (isset($dados['id-produto'])) {
            foreach ($dados['id-produto'] as $key => $item) {
                $itens[$key]['id-item-pedido'] = $dados['id-item-pedido'][$key];
                $itens[$key]['id-produto'] = $dados['id-produto'][$key];
                $itens[$key]['codigo-produto'] = $dados['codigo-produto'][$key];
                $itens[$key]['nome-produto'] = $dados['nome-produto'][$key];
                $itens[$key]['preco-unitario-produto'] = $dados['preco-unitario-produto'][$key];
                $itens[$key]['qtd-produto'] = $dados['qtd-produto'][$key];
                $itens[$key]['total-valor-produto'] = $dados['total-valor-produto'][$key];
            }
        }

        return $itens;
    }

    /**
     * Organiza os itens para serem recompostos no form 
     * @param Pedido $pedido
     * @return array
     */
    private function recomporItensPedidoEntity(Pedido $pedido) {

        //Cria o array que armazenará os itens de forma organizada para form
        $itens = array();

        foreach ($pedido->getItensPedido() as $key => $item) {
            $itens[$key]['id-item-pedido'] = $item->getId();
            $itens[$key]['id-produto'] = $item->getProduto()->getId();
            $itens[$key]['codigo-produto'] = $item->getProduto()->getCodigo();
            $itens[$key]['nome-produto'] = $item->getProduto()->getNome();
            $itens[$key]['preco-unitario-produto'] = $item->getPrecoUnitario();
            $itens[$key]['qtd-produto'] = $item->getQtd();
            $itens[$key]['total-valor-produto'] = $item->getQtd() * $item->getPrecoUnitario();
        }

        return $itens;
    }
    
    
    public function exportarDocAction(){

        
        
        $entity = $this->getEm()->find('\Estoque\Entity\Recebimento', $this->params()->fromRoute('id'));
        
              
       
        $config = $this->getServiceLocator()->get('config')['pedido']['atestado'];
        
        
        
         $phpWord = new \PhpOffice\PhpWord\PhpWord();

        

        $phpWord->addParagraphStyle('left', array('align' => 'right'));
        $phpWord->addParagraphStyle('center', array('align' => 'center'));
        
        $phpWord->addFontStyle('font-title', array('bold' => true, 'size' => '12'));
        $phpWord->addFontStyle('font-date', array('bold' => false, 'size' => '12', 'aling' => 'right'));
          
        
        $section = $phpWord->addSection();
        //----------------------------------------------------------------------
        $textBox = $section->addTextBox(array(
            'borderSize' => 1, 
            'align'       => 'center',
            'borderColor' => '000000', 
            'width' => 600, 
            'height' => 120
        ));

        $textBox->addImage(APPLICATION_PATH.'/public/assets/layouts/layout4/img/sfms-logo-small-docx.jpg', array(

        'width'            => 100,
        'height'           => 100,
        'positioning'      => \PhpOffice\PhpWord\Style\Image::POSITION_ABSOLUTE,
        'posHorizontal'    => \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_RIGHT,
        'posHorizontalRel' => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_PAGE,
        'posVerticalRel'   => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_PAGE,
        'marginLeft'       => 110,
        'marginTop'        => 100,
      
        ));
        $textBox->addText(mb_strtoupper($config['titulo']), null, 'center');
        $textBox->addText('Departamento de Produção', null, 'center');
        $textBox->addText('Divisão Industrial', null, 'center');
        $textBox->addLine(array('height' => 30));
        $textBox->addText('Ordem de Fornecimento', null, 'center');
        
        
        $section->addLine(array('height' => 30));
        
        $textBox = $section->addTextBox(array(
            'borderSize' => 1, 
            'borderColor' => '000000', 
            'width' => 600, 
            'height' => 100
        ));
        
        $textBox->addText('O.F. Nº 73/2016');
        $textBox->addText('PED. Nº 03/2016');
        $textBox->addText('N.E. Nº 624/2016');
        $textBox->addText('PROCESSO: 2016-0.114.381-4');      
        $textBox->addText('EMPRESA- aMANTYKIR IND. E COM. DE EMBALAGENS EIRELLI');
        
        
        //----------------------------------------------------------------------
        
        $section->addLine(array('height' => 50));
        
        //$pStyle = array();
        $hStyle = array('borderSize' => 1, 'borderColor' => '000000');
        $rFStyle = array('borderRightSize' => 1, 'borderLeftSize' => 1, 'borderRightColor' => '000000', 'borderLeftColor' => '000000');
        $rOStyle = array('align' => 'center', 'borderRightSize' => 1, 'borderRightColor' => '000000');
        
        $width = 1500;
        
        $table = $section->addTable(array('cellMargin' => 0));
        
        $row = $table->addRow(350);
        $row->addCell($width, $hStyle)->addText('DATA', null, 'center');
        $row->addCell($width, $hStyle)->addText('ITEM', null, 'center');
        $row->addCell($width, $hStyle)->addText('QTDE.', null, 'center');
        $row->addCell($width, $hStyle)->addText('DESCRIÇÃO', null, 'center');
        $row->addCell($width, $hStyle)->addText('P.UNIT.', null, 'center');
        $row->addCell($width, $hStyle)->addText('P.TOTAL', null, 'center');
        
        foreach(array(1, 2, 3) as $row) {
        
            $row = $table->addRow(350);
            $row->addCell($width, $rFStyle)->addText('adfasd', null, 'center');
            $row->addCell($width, $rOStyle)->addText('afsdfasd', null, 'center');
            $row->addCell($width, $rOStyle)->addText('asdfasd', null, 'center');
            $row->addCell($width, $rOStyle)->addText('asdfasd', null, 'center');
            $row->addCell($width, $rOStyle)->addText('asdfas', null, 'center');
            $row->addCell($width, $rOStyle)->addText('afsads', null, 'center');
        }
        
        
        $row = $table->addRow(500);
        
        // 
        
        $cell = $row->addCell(9000, array('vMerge' => 'restart', 'borderSize' => 1, 'borderColor' => '000000', 'gridSpan' => 6)); 
        $cell->addLine();
        $cell->addText('LOCAL DE ENGREGA: Rua da coroa, 1.554, Vila Guilherme - S.P.');
        $cell->addLine();
        $cell->addText('DATA PREVISTA: Conforme cronograma acima ou de acordo');
        $cell->addLine();
        
        //----------------------------------------------------------------------
        $section->addLine(array('height' => 1000));
        
        $table = $section->addTable(array('cellMargin' => 0));
       
        $row = $table->addRow(350);
        
        $cell = $row->addCell(4500, array('borderColor' => '000000', 'borderLeftSize' => 1, 'borderTopSize' => 1, 'borderBottomSize' => 1));
        $cell->addLine();
        $cell->addText('RESP. P/ SOLICITAÇÃO:');
        $cell->addLine();
        $cell->addText('ASS. E CARIMBO:');
        $cell->addLine();
        $cell->addText('DATA:');
        $cell->addLine();
        
        $cell = $row->addCell(4500, array('borderColor' => '000000', 'borderRightSize' => 1, 'borderTopSize' => 1, 'borderBottomSize' => 1));
        $cell->addLine();
        $cell->addText('RESPONSAVEL P/RECEBIMENTO DO FAX.DA ORD. DE FORNEC.');
        $cell->addLine();
        $cell->addText('NOME:');
        $cell->addLine();
        $cell->addText('FUNÇÃO:');
        $cell->addLine();
        $cell->addText('DATA:  /   /  ');
        $cell->addLine();
          
        //----------------------------------------------------------------------
        // Enviando para download
        //----------------------------------------------------------------------
        header("Content-Type:application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        header('Content-Disposition: attachment; filename=atestado-de-recebimento.docx');
        
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save("php://output");
        
        exit(0);
    }
}
