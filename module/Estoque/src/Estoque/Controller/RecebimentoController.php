<?php

namespace Estoque\Controller;

use Zend\View\Model\ViewModel;
use Zend\Session\Container as SessionConteiner;
use Application\Controller\CrudApplicationController;
use Estoque\Entity\Pedido;
use Estoque\Entity\FechamentoPeriodo;
//use DOMPDFModule\View\Model\PdfModel;

class RecebimentoController extends CrudApplicationController {

    protected $nameForm = 'Estoque\Form\Recebimento';
    protected $nameRepository = 'Estoque\Repository\RecebimentoRepository';
    protected $nameService = 'Estoque\Service\Recebimento';
    protected $controller = 'recebimento';
    protected $route = 'estoque/default';
    protected $jsFormulario = array('recebimento/formulario.js');
    protected $jsIndex = array('recebimento/index.js');
 

    /**
     * 
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction() {

        //Verifica e obtem o pedido do recebimento caso ele exista
        $pedido = $this->getPedidoRecebimento();
        
        //Verifica a permissão para executar ações
        $permissaoAcoes = $this->validarPermissaoRecebimento($pedido);

        //Verifica se um recebimento acabou de ser gerado
        $atestado = new SessionConteiner('RecebimentoSalvo');
        $recebimentoSalvo = $atestado->recebimento ? $atestado->recebimento : "";
        $atestado->getManager()->getStorage()->clear('RecebimentoSalvo');

        //$recebiment = $this->getServiceLocator()->get($this->nameRepository)->findBy(array('id' => $pedido));
        $pedidoRecebimento = $this->getServiceLocator()->get('Estoque\Repository\PedidoRepository')->findBy(array('id' => $pedido));
      
        if (!empty($this->jsIndex)) {
            $this->layout()->setVariable('scripts', $this->jsIndex);
        }

        if (!empty($this->cssIndex)) {
            $this->layout()->setVariable('styles', $this->cssIndex);
        }
        
        //Busca mensagens a serem exibidas na tela
        $messages = $this->flashMessenger()->setNamespace('Application')->getMessages();

        //Retorna os dados para view
        return new ViewModel(array('messages' => $messages, 'pedido' => $pedido, 'recebimentoSalvo' => $recebimentoSalvo, 'permissaoAcoes' => $permissaoAcoes,'recebimento'=>$pedidoRecebimento));
    }

    /**
     * Método padrão de formulário
     * @return \Application\Controller\ViewModel
     */
    public function formularioAction() {

        //Verifica e obtem o pedido do recebimento caso ele exista
        $pedido = $this->getPedidoRecebimento();

        //Valida se o usuário ter permissão para acessar o formulário (somente pode acessar aquele que estiver logado como estoque central e o pedido não estiver em cadastro ou cancelado)
        $this->validarPermissaoRecebimento($pedido, true);

        //Obtem a lista de itens do pedido
        $itensPedido = $this->getServiceLocator()->get('Estoque\Repository\ItemPedidoRepository')->findBy(array('pedido' => $pedido));

        //Instancia o formulário e a requisição
        $form = $this->getServiceLocator()->get($this->nameForm);
        $request = $this->getRequest();

        $notaFiscal = null;
        $qtdRecebida = array();

        //Verifica se a requisição foi feita via post
        if ($request->isPost()) {

            //Faz o merge entre todos os dados vindos via POST e FILE
            $dados = array_merge_recursive(
                    $request->getPost()->toArray(), $request->getFiles()->toArray()
            );
            $dados['pedido'] = $pedido;
            $form->setData($dados);

            //Valida o formulário
            if ($form->valid($dados['qtdRecebida'], $dados['itemRecebido'])) {
                $upload = $form->getData();
                $dados['notaFiscal'] = isset($upload['notaFiscal']) ? $upload['notaFiscal'] : array();
                $service = $this->getServiceLocator()->get($this->nameService);
                        
                $fechamento = $this->getServiceLocator()->get('Estoque\Repository\FechamentoPeriodoRepository')->findAll();
   
                foreach($fechamento as $var){
                    
                   if($var->getDataPeriodoInicial()->format('m/Y') == substr($dados['dataRecebimento'],3)){
                        $this->flashMessenger()->addErrorMessage('Não é possível mais lançar recebimentos para este periodo, pois o mesmo já se encontra fechado');

                        return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
                   }
                }
                
                
                $success = $service->save($dados);

                if ($success) {
                    $atestado = new SessionConteiner('RecebimentoSalvo');
                    $atestado->recebimento = $success->getId();
                }

                $this->setConfirmMessages($success);

                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));

            } else {
                //Confirma o upload do arquivo para a pasta temporária caso não haja erro de validação
                $notaFiscalErros = $form->get('notaFiscal')->getMessages();

                if (empty($notaFiscalErros)) {
                    $upload = $form->getData();
                    $notaFiscal = isset($upload['notaFiscal']) ? $upload['notaFiscal'] : array('tmp_name' => $form->get('hdnNotaFiscal')->getValue(), 'name' => $dados['hdnNomeNota'], 'error' => 0);
                    $form->get('hdnNotaFiscal')->setValue($this->getStringFormat()->getNameFilePath($notaFiscal['tmp_name']));
                }

                $qtdRecebida = $dados['qtdRecebida'];

                $this->flashMessenger()->addErrorMessage('Por favor, verifique os campos do formulário');
            }
        } elseif ($this->params()->fromRoute('id')) {
            //Busca os dados
            $dados = $this->getServiceLocator()->get($this->nameRepository)->find($this->params()->fromRoute('id'));
            if($dados){
                $form->get('dataRecebimento')->setValue($dados->getDataRecebimento()->format('d/m/Y'));
                $form->get('responsavelEntrega')->setValue($dados->getResponsavelEntrega());
                $form->get('cpfRg')->setValue($dados->getCpfRg());
                $form->get('observacao')->setValue($dados->getObservacao());
                $form->get('numeroNota')->setValue($dados->getNumeroNota());
            }
        }
        //Seta os scripts e retorna para view
        if (!empty($this->jsFormulario)) {
            $this->layout()->setVariable('scripts', $this->jsFormulario);
        }

        if (!empty($this->cssFormulario)) {
            $this->layout()->setVariable('styles', $this->cssFormulario);
        }

        //Retorna os dados da view Model
        return new ViewModel(array('form' => $form, 'pedido' => $pedido, 'itensPedido' => $itensPedido, 'notaFiscal' => $notaFiscal, 'qtdRecebida' => $qtdRecebida));
    }

    /**
     * Método chamado via ajax para listagem com datatables
     * 
     * @return type
     */
    public function paginacaoAction() {

        //Verifica e obtem o pedido do recebimento caso ele exista
        $pedido = $this->getPedidoRecebimento();

        //Armazena a resposta
        $response = $this->getResponse();

        //Armazena a requisição
        $request = $this->getRequest()->getPost();

        //Organiza os dados da busca
        $busca = array(
            "busca" => mb_strtolower($request['search']['value'], 'UTF-8'),
            "pedido" => $pedido
        );

        //Cria a ordenação
        $order = array();

        if (isset($request['order'][0]['column'])) {
            switch ($request['order'][0]['column']) {
                case 1: $order = array('field' => 'r.numeroNota', 'order' => $request['order'][0]['dir']);
                    break;
                case 2: $order = array('field' => 'r.dataRecebimento', 'order' => $request['order'][0]['dir']);
                    break;
                case 3: $order = array('field' => 'u.nome', 'order' => $request['order'][0]['dir']);
                    break;
                case 4: $order = array('field' => 'r.valorTotal', 'order' => $request['order'][0]['dir']);
                    break;
            }
        }
        
        
        //Busca os dados
        $registros = $this->getServiceLocator()->get($this->nameRepository)->busca($busca, $request['length'], $request['start'], $order);
        $registrosTotaisBusca = $this->getServiceLocator()->get($this->nameRepository)->busca($busca, 0, 0, array(), true);
        $registrosTotais = $this->getServiceLocator()->get($this->nameRepository)->countAll();
        $dados = array();

        //Armazena os dados retornados em um array
        foreach ($registros as $registro) {
            $arr = array(
                'route' => $this->route,
                'controller' => $this->controller,
                'action' => 'visualizar',
                'id' => $registro->getId()
            );

            $valor = array();
            $valor[] = '<input type="checkbox" name="checkbox-list" class="checkboxes" value="' . $registro->getId() . '" />';

            $valor[] = "<a href='" . $this->urls($arr) . "' title='Visualizar'>".$registro->getNumeroNota()."</a>";
            $valor[] = $registro->getDataRecebimento()->format('d/m/Y');
            $valor[] = $registro->getUsuario()->getNome();
            $valor[] = "R$ " . number_format($registro->getValorTotal(), 2, ',', '.');
            $dados[] = $valor;
        }

        //Organiza o retorno
        $retorno['draw'] = $request['draw'];
        $retorno['recordsTotal'] = $registrosTotais;
        $retorno['recordsFiltered'] = $registrosTotaisBusca;
        $retorno['data'] = $dados;


        //Retorna a resposta
        $response->setContent(\Zend\Json\Json::encode($retorno));
        return $response;
    }

    public function gerarAtestadoAction() {
//        $this->layout(false);
//
//        // Instantiate new PDF Model
//        $pdf = new PdfModel();
//
//        // set filename
//        $pdf->setOption('filename', 'atestado_recebimento.pdf');
//
//        // Defaults to "8x11"
//        $pdf->setOption('paperSize', 'a4');
//
//        // paper orientation
//        $pdf->setOption('paperOrientation', 'portrait');
//
//        $pdf->setVariables(array(
//            'var1' => 'teste'
//        ));
//
//        return $pdf;
    }

    /**
     * Ação padrão de deletar, apenas com a adição das verificações de permissões específicas da funcionalidade de recebimento
     */
    public function deletarAction() {
        //Verifica e obtem o pedido do recebimento caso ele exista
        $pedido = $this->getPedidoRecebimento();
        
        //Valida se o usuário ter permissão para acessar o formulário (somente pode acessar aquele que estiver logado como estoque central e o pedido não estiver em cadastro ou cancelado)
        $this->validarPermissaoRecebimento($pedido, true);
        $dadosF = $this->getRequest()->getPost();
       
        $fechamento = $this->getServiceLocator()->get('Estoque\Repository\FechamentoPeriodoRepository')->findAll();
        
        $recebimento = $this->getServiceLocator()->get($this->nameRepository)->findBy(array('id' =>$dadosF['ids'][0]));
        
        foreach($recebimento as $re){
            
            $num = count($fechamento);

            if($num > 0) {

              for( $i = 0; $i<$num; $i++ ) {

                 
                  $fe = $fechamento[$i];

                  $dtRec  = $re->getDataRecebimento()->format('m/Y'); 
                  $dtFe   = $fe->getDataPeriodoInicial()->format('m/Y');

              }
               if($dtRec == $dtFe){
                      echo \Zend\Json\Json::encode(array('success' => 0, 'msg' => 'Não foi possível completar a ação! Não é possível excluir recebimentos de períodos fechados.'));

                        die();
                  }else{
                       parent::deletarAction();
                  }
            }else{
                 parent::deletarAction();
            }
        }
              
             // print_r($dtFe);die;  
	
              
       
        
       
    }

    /**
     * Método que valida se o usuário possuí permissão no formulário, levando em consideração o status do pedido
     * 
     * @param integer $pedido
     * @return boolean
     */
    private function validarPermissaoRecebimento($pedido, $redirect = false) {

        //Valida se o usuário logado possuí o estoque central selecionado por padrão
        $dadosUser = $this->getDadosUser();
        $central = $dadosUser['filtrosSelecionados']['estoque']['central'];

        if (($this->params()->fromRoute('action') == 'formulario' || $this->params()->fromRoute('action') == 'deletar' || $this->params()->fromRoute('action') == 'index') && !$central) {
            return $redirect ? $this->redirect()->toRoute('home/default', array('controller' => 'home', 'action' => 'index')) : false;
        }

        //Busca os dados do pedido
        $pedido = $this->getServiceLocator()->get('Estoque\Repository\PedidoRepository')->find($pedido);

        //Valida a situação do pedido
        if ($pedido->getSituacao()->getId() == Pedido::STATUS_CADASTRADO || $pedido->getSituacao()->getId() == Pedido::STATUS_CANCELADO) {
            return $redirect ? $this->redirect()->toRoute('home/default', array('controller' => 'home', 'action' => 'index')) : false;
        }

        return true;
    }
   

    private function getPedidoRecebimento() {
        //Verifica a existência da sessão contendo o id do pedido
        $container = new SessionConteiner('PedidoRecebimento');

        if (!$container->pedido) {
            $this->redirect()->toRoute('home');
        }

        return $container->pedido;
    }

    public function visualizarAction() {
        return $this->formularioAction($this->params());
    }
    
    public function exportarDocAction(){
            
        //----------------------------------------------------------------------
        // Carregando dados
        //----------------------------------------------------------------------
        $entity = $this->getEm()->find('\Estoque\Entity\Recebimento', $this->params()->fromRoute('id'));
        
        $valSerPago = $entity->getValorTotal();
        $valEmpenho = $entity->getPedido()->getEmpenho()->getValor();
        
        $valSaldoEmpenho = 0.00;
        $strFornecedor = $entity->getPedido()->getFornecedor()->getNome();
        $strNumeroNota = $entity->getNumeroNota();
        $numEmpenho = $entity->getPedido()->getEmpenho()->getNumEmpenho();
        
        
        foreach($entity->getPedido()->getEmpenho()->getPedidos() as $pedido) {
            
            foreach($pedido->getItensPedido() as $item) {
                
                $valSaldoEmpenho += ($item->getQtd() * $item->getPrecoUnitario());
            }
        }

        $valSaldoEmpenho = ($valEmpenho - $valSaldoEmpenho);
        
        $month = $this->getServiceLocator()->get('config')['date_month'];
        $date = $entity->getDataRecebimento();
        
        $text = sprintf('São Paulo, %s de %s de %s.',
           $date->format('d'),
           $month[$date->format('m')],
           $date->format('y')
        );
              
        //----------------------------------------------------------------------
        // Montando o word
        //----------------------------------------------------------------------
        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        $section = $phpWord->addSection();

        $config = $this->getServiceLocator()->get('config')['pedido']['atestado'];
        
        $phpWord->addParagraphStyle('left', array('align' => 'right'));
        $phpWord->addParagraphStyle('center', array('align' => 'center'));
        
        $phpWord->addFontStyle('font-title', array('bold' => true, 'size' => '12'));
        $phpWord->addFontStyle('font-date', array('bold' => false, 'size' => '12', 'aling' => 'right'));
                        
        $section->addImage(APPLICATION_PATH.'/public/assets/layouts/layout4/img/sfms-logo-docx.jpg', array('width' => 200, 'height' => 73));
        
        $section->addText(mb_strtoupper($config['titulo']), 'font-title', 'left');
        $section->addLine();
        $section->addText('TID Nº ________', 'font-title', 'left');
        $section->addLine(array('height' => 30));
        $section->addText('São Paulo, 08 de Setembro de 2016.', 'font-date', 'left');
        $section->addLine(array('height' => 30));
        $section->addText('ATESTADO', 'font-title', 'center');
        $section->addLine(array('height' => 30));
        $section->addText('À', 'font-title');
        $section->addText('Divisão Industrial', 'font-title');
        $section->addText('Sra. Diretora', 'font-title');
        $section->addLine(array('height' => 30));
            
        $textrun = $section->createTextRun();
        
        $textrun->addText('Atesto para os devidos fins, que nesta data recebemos da empresa ', array('size' => '11'));
        $textrun->addText($strFornecedor, array('bold' => true, 'size' => '11'));
        $textrun->addText('. - ME., O(s) material(ais) ', array('size' => '11'));
        $textrun->addText('constante(s) na Nota Fical(is) discriminada e foi(ram) entregue a contento', array('size' => '11'));
        
        $section->addLine(array('height' => 30));
        
        $width = 2000;
        $pStyle = array('align' => 'center');
        
        $table = $section->addTable(array('borderSize' => 1, 'borderColor' => '000000'));
        $row = $table->addRow(350);
        $row->addCell($width)->addText('Empenho', null, $pStyle);
        $row->addCell($width)->addText('Nota Fiscal', null, $pStyle);
        $row->addCell($width)->addText('Valor Empenho', null, $pStyle);
        $row->addCell($width)->addText('Valor a ser pago', null, $pStyle);
        $row->addCell($width)->addText('Saldo de Empenho', null, $pStyle);
        
        $row = $table->addRow(350);
        $row->addCell($width)->addText($numEmpenho, null, $pStyle);
        $row->addCell($width)->addText($strNumeroNota, null, $pStyle);
        $row->addCell($width)->addText(number_format($valEmpenho, 2, ',', '.'), null, $pStyle);
        $row->addCell($width)->addText(number_format($valSerPago, 2, ',', '.'), null, $pStyle);
        $row->addCell($width)->addText(number_format($valSaldoEmpenho, 2, ',', '.'), null, $pStyle);
        
        $section->addLine(array('height' => 30));
        $section->addText('Atenciosamente,', null, 'center');
        $section->addLine(array('height' => 50));
        $section->addText(str_repeat('-', '50'), null, 'center');
        $section->addText(mb_strtoupper($config['atestante']), 'font-title', 'center');
        $section->addText($config['cargo'], null, 'center');
          
        //----------------------------------------------------------------------
        // Enviando para download
        //----------------------------------------------------------------------
        header("Content-Type:application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        header('Content-Disposition: attachment; filename=atestado-de-recebimento.docx');
        
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save("php://output");
        
        exit(0);
    }
}
