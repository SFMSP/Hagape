<?php

namespace Estoque\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Controller\CrudApplicationController;

class FornecedorController extends CrudApplicationController {

    
    protected $nameForm = 'Estoque\Form\Fornecedor';
    protected $nameRepository = 'Estoque\Repository\FornecedorRepository';
    protected $nameService = 'Estoque\Service\Fornecedor';
    protected $controller = 'fornecedor';
    protected $route = 'estoque/default'; 
    protected $jsIndex = array('fornecedor/index.js');
    protected $jsFormulario = array('fornecedor/formulario.js');
   

    public function paginacaoAction() {
      //Armazena a resposta
        $response = $this->getResponse();
        
    //Armazena a requisição
        $request = $this->getRequest()->getPost();

        //Organiza os dados da busca
        $busca = array(
            "busca" => mb_strtolower($request['search']['value'], 'UTF-8')
           );
       
        //Cria a ordenação
        $order = array();

        if (isset($request['order'][0]['column'])) {
            switch ($request['order'][0]['column']) {
                case 1: $order = array('field' => 'nome', 'order' => $request['order'][0]['dir']);
                    break;
                case 2: $order = array('field' => 'cnpj', 'order' => $request['order'][0]['dir']);
                    break;
                case 3: $order = array('field' => 'telefone1', 'order' => $request['order'][0]['dir']);
                    break;
                case 4: $order = array('field' => 'ativo', 'order' => $request['order'][0]['dir']);
                    break;
            }
        }

        $registros = $this->getServiceLocator()->get($this->nameRepository)->buscaFornecedor($busca, $request['length'], $request['start'], $order);
        $registrosTotaisBusca = $this->getServiceLocator()->get($this->nameRepository)->buscaFornecedor($busca, 0, 0, array(), true);
        $registrosTotais = $this->getServiceLocator()->get($this->nameRepository)->countAll();
        $dados = array();
       //Armazena os dados retornados em um array
        foreach ($registros as $registro) {
            $valor = array();
            $valor[] = '<input type="checkbox" name="checkbox-list" class="checkboxes" value="' . $registro->getId() . '" />';

            if ($this->verifyPermission('fornecedor', 'editar')) {
                
                $arr = array(
                    'route'=>$this->route,
                    'controller'=>$this->controller,
                    'action'=>'formulario',
                    'id'=>$registro->getId()
                 );
              
                
                $valor[] = "<a href='".$this->urls($arr)."' title='Editar Item'>" . $registro->getNome() . "</a>";
                
               
            } else {
                $valor[] = $registro->getNome();
            }

            $valor[] = $registro->getCnpj();
            $valor[] = $registro->getTelefone1();
            $valor[] = $registro->getAtivo() ? "Ativo" : "Inativo";
            $dados[] = $valor;
        }

        //Organiza o retorno
        $retorno['draw'] = $request['draw'];
        $retorno['recordsTotal'] = $registrosTotais;
        $retorno['recordsFiltered'] = $registrosTotaisBusca;
        $retorno['data'] = $dados;


        //Retorna a resposta
        $response->setContent(\Zend\Json\Json::encode($retorno));
        return $response;
    }
    public function formularioAction() {
        //Instancia o formulário e a requisição
        $form = $this->getServiceLocator()->get($this->nameForm);
       
        $request = $this->getRequest();

        //Verifica se a requisição foi feita via post
        if ($request->isPost()) {
            
            //Busca os dados
            $dados = $request->getPost()->toArray();
            if ($this->params()->fromRoute('id')) {
                $dados['id'] = $this->params()->fromRoute('id');
            }

            $form->setData($dados);

            //Valida o formulário
            if ($form->isValid()) {
                              
                $service = $this->getServiceLocator()->get($this->nameService);
                $success = $service->save($dados);
                $this->setConfirmMessages($success);
                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));             
               
                
            } else {
                $this->flashMessenger()->addErrorMessage('Por favor, verifique os campos do formulário');
            }
            
            
            
        } elseif ($this->params()->fromRoute('id')) {
           
            //Busca os dados
            $dados = $this->getServiceLocator()->get($this->nameRepository)->find($this->params()->fromRoute('id'));
          
            if ($dados) {
                $dados = $dados->toArray();
                
                $form->setData($dados);

                $retorno = $this->getServiceLocator()->get('Application\Repository\UfRepository')->findAll();
                foreach ($retorno as $value) {
                    $estado[$value->getId()] = $value->getNome();
                }
                natsort($estado);
                $estado = ['' => 'Selecione o Estado'] + $estado;
                $form->get('estado')->setValueOptions($estado);
                $cidade = $this->getServiceLocator()->get('Application\Repository\CidadeRepository')->findBy(array('id' => $dados['cidade']));
                
                foreach ($cidade as $value) {
                    
                   $est = $this->getServiceLocator()->get('Application\Repository\UfRepository')->findBy(array('id' => $value->getUf()));
                   
                  
                   
                     foreach ($est as $value) {
                         
                         $arr[$value->getId()] = $value->getNome();
                         
                       $form->get('estado')->setValue($value->getId());
                     }
              
                   
                }
             
                
            }
        }
            
        
       // $this->verifyPermission($controller, $action)
            if ($this->verifyPermission('fornecedor', 'incluir')) {
                    if (!empty($this->jsFormulario)) {
                       $this->layout()->setVariable('scripts', $this->jsFormulario);
                    }
            }else{
            //Seta os scripts e retorna para view
             $this->layout()->setVariable('scripts', array('fornecedor/formularioVisualiza.js'));
            }
        
            
        //Retorna os dados da view Model
        return new ViewModel(array('form' => $form));
    }
    
   
    
}