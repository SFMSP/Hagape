<?php

namespace Estoque\Controller;

use Base\Helper\DatetimeFormat;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Controller\CrudApplicationController;

/**
 * Class ContratoController
 * @package Estoque\Controller
 */
class ContratoController extends CrudApplicationController
{
    protected $nameForm = 'Estoque\Form\Contrato';
    protected $nameRepository = 'Estoque\Repository\ContratoRepository';
    protected $nameService = 'Estoque\Service\Contrato';
    protected $controller = 'contrato';
    protected $route = 'estoque/default';
    protected $jsIndex = array('contrato/index.js');
    protected $jsFormulario = array('contrato/formulario.js');

    /**
     * Método padrão de formulário
     * @return |Application\Controller\ViewModel
     */
    public function formularioAction()
    {
        //Instancia o formulário e a requisição
        $form = $this->getServiceLocator()->get($this->nameForm);
        $request = $this->getRequest();
        $empenhos = array();

        //Verifica se a requisição foi feita via post
        if ($request->isPost()) {
            //Busca os dados
            $dados = $request->getPost()->toArray();
            if ($this->params()->fromRoute('id')) {
                $dados['id'] = $this->params()->fromRoute('id');
                //Busca Empenhos relacionados com o contrato para carregar a lista de empenhos que existe
                $empenhos = $this->getServiceLocator()->get('Estoque\Repository\EmpenhoRepository')->getEmpenhosPorContrato($this->params()->fromRoute('id'));
            }

            if ($dados['fornecedor']) {
                $retorno = $this->getServiceLocator()->get('Estoque\Repository\FornecedorRepository')->findAll();
                foreach ($retorno as $value) {
                    if($value->getAtivo()) {
                        $fornecedores[$value->getId()] = $value->getNome()." (".$value->getCnpj().")";
                    }
                }
                natsort($fornecedores);
                $fornecedores = ['' => 'Selecione o Fornecedor'] + $fornecedores;
                $form->get('fornecedor')->setValueOptions($fornecedores);
                $form->get('fornecedor')->setValue($dados['fornecedor'][0]);
            }

            $arEmpenhos = isset($dados['empenho']) ? $dados['empenho'] : [];

            $dados['provisorio'] = '';

            $form->setData($dados);
            //Valida o formulário
            if ($form->isValid($arEmpenhos)) {
                $service = $this->getServiceLocator()->get($this->nameService);

                $success = $service->save($dados);
                $this->setConfirmMessages($success);

                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            } else {
                $empenhos = [];
                foreach ($dados['empenho'] as $key => $value) {
                    $empenhos[$key]['empenho'] = $value;
                    $empenhos[$key]['numEmpenho'] = $dados['numEmpenhos'][$key];
                    $empenhos[$key]['dataEmpenho'] = $dados['dataEmpenhos'][$key];
                    $empenhos[$key]['hiddenValorEmpenho'] = $dados['valorEmpenhos'][$key];
                    $empenhos[$key]['valorEmpenho'] = number_format($dados['valorEmpenhos'][$key], 2, ',', '.');
                    $empenhos[$key]['observacao'] = $dados['observacoes'][$key];
                    $empenhos[$key]['provisorios'] = ($dados['provisorios'][$key]) ? 'true' : 'false';
                    //Valida vinculo com pedido
                    $empenho = $this->getServiceLocator()->get('Estoque\Repository\EmpenhoRepository')->getEmpenhosPorContrato($value);
                    if ($empenho) {
                        $arPedido = $this->getServiceLocator()->get('Estoque\Repository\PedidoRepository')->findBy(['empenho' => $value]);
                        //Caso haja vinculo com pedido o empenho não pode ser deletado
                        if ($arPedido) {
                            $empenhos[$key]['boolDeletavel'] = false;
                        }
                    }else{
                        $empenhos[$key]['boolDeletavel'] = true;
                    }
                }

                $form->get('hiddenValorTotalEmpenhado')
                    ->setAttributes([
                        'id' => 'hiddenValorTotalEmpenhado',
                        'value' => $dados['hiddenValorTotalEmpenhado']
                    ]);

                //Carregar dados do fornecedor para o formulario
                $retorno = $this->getServiceLocator()->get('Estoque\Repository\FornecedorRepository')->findAll();
                foreach ($retorno as $value) {
                    if($value->getAtivo()) {
                        $fornecedores[$value->getId()] = $value->getNome()." (".$value->getCnpj().")";
                    }
                }
                natsort($fornecedores);
                $fornecedores = ['' => 'Selecione o Fornecedor'] + $fornecedores;
                $form->get('fornecedor')->setValueOptions($fornecedores);
                if (isset($dados['fornecedor'][0])) {
                    $form->get('fornecedor')->setValue($dados['fornecedor'][0]);
                }

                $this->flashMessenger()->addErrorMessage('Por favor, verifique os campos do formulário');
            }

            //Vem da URL ou Edição
        } elseif ($this->params()->fromRoute('id')) {
            //Busca dados Contrato
            $dados = $this->getServiceLocator()->get($this->nameRepository)->find($this->params()->fromRoute('id'));
            //Busca Empenhos relacionados com o contrato para carregar a lista de empenhos que existe
            $empenhos = $this->getServiceLocator()->get('Estoque\Repository\EmpenhoRepository')->getEmpenhosPorContrato($this->params()->fromRoute('id'));
            //Atribuindo o valor total deos empenhos na label
            $valorTotalEmpenhos = 0.00;
            foreach ($empenhos as $key => $value) {
                $valorTotalEmpenhos += $value['valorEmpenho'];
                $arPedido = $this->getServiceLocator()->get('Estoque\Repository\PedidoRepository')->findBy(['empenho' => $value['empenho']]);
                //Caso haja vinculo com pedido o empenho não pode ser deletado
                $empenhos[$key]['boolDeletavel'] = true;
                if ($arPedido) {
                    $empenhos[$key]['boolDeletavel'] = false;
                }
            }
            $form->get('hiddenValorTotalEmpenhado')
                ->setAttributes([
                    'id' => 'hiddenValorTotalEmpenhado',
                    'value' => number_format($valorTotalEmpenhos, 2, '.', ',')
                ]);

            if ($dados) {
                $dados = $dados->toArray();
                $dados['valor'] = number_format($dados['valor'], 2, ',', '.');
                $dados['hiddenValorDisponivel'] = $dados['valor'] - $valorTotalEmpenhos;
                $dados['hiddenDataVencimento'] = $dados['dataVencimento'];
                $dados['hiddenNumContrato'] = $dados['numContrato'];
                $form->setData($dados);

                $retorno = $this->getServiceLocator()->get('Estoque\Repository\FornecedorRepository')->findAll();
                foreach ($retorno as $value) {
                    if($value->getAtivo()) {
                        $fornecedores[$value->getId()] = $value->getNome()." (".$value->getCnpj().")";
                    }
                }
                natsort($fornecedores);
                $fornecedores = ['' => 'Selecione o Fornecedor'] + $fornecedores;
                $form->get('fornecedor')->setValueOptions($fornecedores);
                $form->get('fornecedor')->setValue($dados['fornecedor']->getId());
            }
        } else {
            $dados = $this->getServiceLocator()->get('Estoque\Repository\FornecedorRepository')->findAll();
            foreach ($dados as $value) {
                if($value->getAtivo()){
                    $fornecedores[$value->getId()] = $value->getNome()." (".$value->getCnpj().")";
                }
            }
            natsort($fornecedores);
            $fornecedores = ['' => 'Selecione o Fornecedor'] + $fornecedores;
            $form->get('fornecedor')->setValueOptions($fornecedores);
        }

        //Seta os scripts e retorna para view
            if ($this->verifyPermission('contrato', 'incluir')) {
                    if (!empty($this->jsFormulario)) {
                       $this->layout()->setVariable('scripts', $this->jsFormulario);
                    }
            }else{
            //Seta os scripts e retorna para view
             $this->layout()->setVariable('scripts', array('contrato/formularioVisualiza.js'));
            }
        
        //Retorna os dados da view Model
        return new ViewModel(array('form' => $form, 'empenhos' => $empenhos));
    }

    /**
     * Retorna todos os fornecedores
     * @param boolean $fornecedor
     * @param boolean $json
     * @return mixed
     */
    public function getFornecedoresAction($fornecedor = false, $json = true)
    {
        if (!$fornecedor) {
            //Busca os dados da requisição
            $request = $this->getRequest()->getPost()->toArray();
            $fornecedor = $request['fornecedor'];
        }

        //Busca os fornecedores
        $fornecedores = $this->getServiceLocator()->get('Estoque\Repository\FornecedorRepository')->findBy(array('id' => $fornecedor));

        if ($json) {
            foreach ($fornecedores as $k => $value) {
                if($value->getAtivo()) {
                    $array[$k]['id'] = $value->getId();
                    $array[$k]['nome'] = $value->getNome();
                }
            }
        } else {
            foreach ($fornecedores as $value) {
                if($value->getAtivo()) {
                    $fornecedores[$value->getId()] = $value->getNome()." (".$value->getCnpj().")";
                }
            }
        }

        if ($json) {
            echo Json::encode($array);
            die();
        } else {
            return $array;
        }
    }

    public function paginacaoAction()
    {
        //Armazena a resposta
        $response = $this->getResponse();

        //Armazena a requisição
        $request = $this->getRequest()->getPost();

        //Organiza os dados da busca
        $busca = array(
            "busca" => mb_strtolower($request['search']['value'], 'UTF-8')
        );

        //Cria a ordenação
        $order = array();

        if (isset($request['order'][0]['column'])) {
            switch ($request['order'][0]['column']) {
                case 1:
                    $order = array('field' => 'c.numContrato', 'order' => $request['order'][0]['dir']);
                    break;
                case 2:
                    $order = array('field' => 'f.nome', 'order' => $request['order'][0]['dir']);
                    break;
                case 3:
                    $order = array('field' => 'c.numProcessoLicitatorio', 'order' => $request['order'][0]['dir']);
                    break;
                case 4:
                    $order = array('field' => 'valor', 'order' => $request['order'][0]['dir']);
                    break;
                case 5:
                    $order = array('field' => 'disponivel', 'order' => $request['order'][0]['dir']);
                    break;
            }
        }

        //Busca os dados
        $registros = $this->getServiceLocator()->get($this->nameRepository)->buscaContratos($busca, $request['length'], $request['start'], $order, false);
        $registrosTotaisBusca = $this->getServiceLocator()->get($this->nameRepository)->buscaContratos($busca, 0, 0, array(), true);
        $registrosTotais = $this->getServiceLocator()->get($this->nameRepository)->countAll();
        $dados = array();

        //Armazena os dados retornados em um array
        foreach ($registros as $registro) {
            $valor = array();
            $valor[] = "<input type='checkbox' name='checkbox-list' class='checkboxes' value='" . $registro['id'] . "' />";
            $dadosUser = $this->getFiltrosSelecionadosUser();

            if ($this->verifyPermission('contrato', 'editar')) {

                $arr = array(
                    'route' => $this->route,
                    'controller' => $this->controller,
                    'action' => 'formulario',
                    'id' => $registro['id']
                );

                $title = 'Editar Contrato';
                $valor[] = "<div class='text-center'><a href='" . $this->urls($arr) . "' title='" . $title . "'>" . $registro['numContrato'] . "</a></div>";
            } else {
                $valor[] = "<div class='text-center'>" . $registro['numContrato'] . "</div>";
            }

            $ativo = $registro['ativo'] ? 'Ativo' : 'Inativo';
            
            $registro['cnpj'] = substr($registro['cnpj'], 0, 2) . '.' . substr($registro['cnpj'], 2, 3) . 
                '.' . substr($registro['cnpj'], 5, 3) . '/' . 
                substr($registro['cnpj'], 8, 4) . '-' . substr($registro['cnpj'], 12, 2);
        
            $nomeFornecedorCnpj = $registro['nome'] ." (".$registro['cnpj'].")";
            $valor[] = "<div class='text-left'>" . $nomeFornecedorCnpj ."</div>";
            $valor[] = "<div class='text-center'>" . $registro['numProcessoLicitatorio'] . "</div>";
            $valor[] = "<div class='text-right'>R$: " . number_format($registro['disponivel'], 2, ',', '.') . "</div>";
            $valor[] = "<div class='text-right'>R$: " . number_format($registro['valor'], 2, ',', '.') . "</div>";
            $valor[] = "<div class='text-center'>" . $ativo . "</div>";
            $dados[] = $valor;
        }

        //Organiza o retorno
        $retorno['draw'] = $request['draw'];
        $retorno['recordsTotal'] = $registrosTotais;
        $retorno['recordsFiltered'] = $registrosTotaisBusca;
        $retorno['data'] = $dados;

        //Retorna a resposta
        $response->setContent(\Zend\Json\Json::encode($retorno));
        return $response;
    }


}