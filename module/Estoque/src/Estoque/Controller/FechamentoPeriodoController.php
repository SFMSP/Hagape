<?php

namespace Estoque\Controller;

use Application\Controller\CrudApplicationController;
use Zend\Authentication\Storage\Session;
use Estoque\Service\FechamentoPeriodo;
use Zend\Log\Logger;
use Zend\Authentication\AuthenticationService;
class FechamentoPeriodoController extends CrudApplicationController
{
    protected $nameService = 'Estoque\Service\FechamentoPeriodo';

    public function fechamentoPeriodoAction()
    {
        $dtNow = new \DateTime('now');
        $fechamento = false;
        #Busca os pedidos recebidos no periodo para atualizacao nas tabelas
        $pedidosRecebidos = $this->getServiceLocator()
            ->get('Estoque\Repository\FechamentoPeriodoRepository')
            ->buscaPedidosRecebidosFechamentoPeriodo($dtNow);

        //Instancia o adapter de autenticação
        $adapter = $this->getServiceLocator()->get('Application\Auth\Adapter');
        $adapter->setUsername('teste')->setPassword('123456');
        //Cria a session storage
        $storage = new Session('Application', null, $this->getServiceLocator()->get('SessionManager'));
        //Instancia o serviço de autenticação
        $autenticationService = new AuthenticationService();
        $autenticationService->setStorage($storage)->setAdapter($adapter);
        //Realiza a autenticação
        $auth = $autenticationService->authenticate();
        $identity = $auth->getIdentity();
        $storage->write($identity);

        if (count($pedidosRecebidos)) {

            $logger = new Logger();
            $writer = new \Zend\Log\Writer\Stream('logs/log.txt');
            $logger->addWriter($writer);

            $fechamentoService = new FechamentoPeriodo($this->getEm(),new \Estoque\Entity\FechamentoPeriodo(),$logger);
            $fechamentoService->setDadosUser($this->getDadosUser())
                ->setLogAlteracao($this->getServiceLocator()->get('Application\Service\LogAlteracao'))
                ->setDateTimeFormat(new \Base\Helper\DatetimeFormat())
                ->setAlteracaoCustoService($this->getServiceLocator()->get('Estoque\Service\AlteracaoCusto'))
                ->setProdutoService($this->getServiceLocator()->get('Estoque\Service\Produto'))
                ->setMovimentacaoService($this->getServiceLocator()->get('Estoque\Service\Movimentacao'))
                ->setFechamentoPeriodoRepository($this->getServiceLocator()->get('Estoque\Repository\FechamentoPeriodoRepository'));

            $fechamento = $fechamentoService->processaFechamentoPeriodo($pedidosRecebidos, $dtNow);

        }

        if ($fechamento) {
            $stRetorno  = "\n";
            $stRetorno .= "\n";
            $stRetorno .= "-----------------ROTINA FECHAMENTO DO PERIODO--------------------------";
            $stRetorno .= "\n";
            $stRetorno .= "Fechamento do periodo e atualização de dados realizado com sucesso!";
            $stRetorno .= "\n";
            $stRetorno .= "-----------------------------------------------------------------------";
            $stRetorno .= "\n";
            $stRetorno .= "\n";
            $stRetorno .= "\n";
            return $stRetorno ;
        } else {
            $stRetorno  = "\n";
            $stRetorno .= "\n";
            $stRetorno .= "-----------------ROTINA FECHAMENTO DO PERIODO--------------------------";
            $stRetorno .= "\n";
            $stRetorno .= "Ocorreu um erro, o fechamento do periodo não foi realizado, verifique o arquivo de log!";
            $stRetorno .= "\n";
            $stRetorno .= "-----------------------------------------------------------------------";
            $stRetorno .= "\n";
            $stRetorno .= "\n";
            $stRetorno .= "\n";
            return $stRetorno;
        }

    }

}