<?php

namespace Estoque\Controller;

use Zend\View\Model\ViewModel;
use Application\Controller\CrudApplicationController;
use Estoque\Entity\SolicitacaoDescarte as SolicitacaoDescarteEntity;


class RecebimentoDescarteController extends CrudApplicationController {

    protected $nameForm = 'Estoque\Form\RecebimentoDescarte';
    protected $nameRepository = 'Estoque\Repository\SolicitacaoDescarteRepository';
    protected $nameService = 'Estoque\Service\RecebimentoDescarte';
    protected $nameFormFilter = 'Estoque\Form\DescarteFiltro';
    protected $controller = 'recebimentoDescarte';
    protected $route = 'estoque/default';
    protected $jsFormulario = array('recebimento-descarte/formulario.js');
    protected $jsIndex = array('recebimento-descarte/index.js');

    /**
     * Método chamado via ajax para listagem com datatables
     * 
     * @return type
     */
    public function paginacaoAction() {

        //Armazena a resposta
        $response = $this->getResponse();

        //Armazena a requisição
        $request = $this->getRequest()->getPost();

        //Busca os estoques do usuário na sessão 
        $dadosUser = $this->getDadosUser();
        $filtrosUser = $this->getFiltrosSelecionadosUser();

        //Organiza os dados da busca
        $busca = array(
            "busca" => mb_strtolower($request['search']['value'], 'UTF-8'),
            "estoques" => $dadosUser['estoques'],
            "situacao" => $request['situacao']
        );

        //Cria a ordenação
        $order = array();

        if (isset($request['order'][0]['column'])) {
            switch ($request['order'][0]['column']) {
                case 1: $order = array('field' => 'sd.numeroDescarte', 'order' => $request['order'][0]['dir']);
                    break;
                case 2: $order = array('field' => 'u.nome', 'order' => $request['order'][0]['dir']);
                    break;
                case 3: $order = array('field' => 's.nome', 'order' => $request['order'][0]['dir']);
                    break;
                case 4: $order = array('field' => 'sd.dataDescarte', 'order' => $request['order'][0]['dir']);
                    break;
                case 4: $order = array('field' => 'rd.dataRecebimentoDescarte', 'order' => $request['order'][0]['dir']);
                    break;
            }
        }

        //Busca os dados
        $registros = $this->getServiceLocator()->get($this->nameRepository)->buscaRecebimentos($busca, $request['length'], $request['start'], $order);
        $registrosTotaisBusca = $this->getServiceLocator()->get($this->nameRepository)->buscaRecebimentos($busca, 0, 0, array(), true);
        $registrosTotais = $this->getServiceLocator()->get($this->nameRepository)->countAll();
        $dados = array();
   
        //Armazena os dados retornados em um array
        foreach ($registros as $registro) {
            $valor = array();
            $valor[] = '<input type="checkbox" name="checkbox-list" class="checkboxes" value="' . $registro->getId() . '" />';

            
            if ($this->verifyPermission('recebimentoDescarte', 'incluir')  
                    && $registro->getSituacaoDescarte()->getId() == SolicitacaoDescarteEntity::STATUS_SOLICITADO 
                    && $registro->getEstoqueDestino()->getId() == $filtrosUser['estoque']['id']) {

                $arr = array(
                    'route' => $this->route,
                    'controller' => $this->controller,
                    'action' => 'formulario',
                    'id' => $registro->getId()
                );

                $title = 'Incluir Recebimento';
            } else {
                $arr = array(
                    'route' => $this->route,
                    'controller' => $this->controller,
                    'action' => 'visualizar',
                    'id' => $registro->getId()
                );

                $title = 'Visualizar Item';
            }

            $valor[] = "<a href='" . $this->urls($arr) . "' title='" . $title . "'>" . $registro->getNumeroDescarte() . "</a>";
            $valor[] = $registro->getUsuario()->getNome();
            $valor[] = "R$ " . number_format($registro->getValorTotalSolicitacaoDescarte(), 2, ',', '.');
            
            $classCSS = $this->getLabelSituacao($registro->getSituacaoDescarte()->getNome());
            
            $valor[] = $registro->getSituacaoDescarte() ? "<span class='label label-sm ".$classCSS."'>".$registro->getSituacaoDescarte()->getNome()."</span>" : "";
              
            $valor[] = $registro->getDataDescarte()->format('d/m/Y');
            $valor[] = $registro->getRecebimentoDescarte() ? $registro->getRecebimentoDescarte()->getDataRecebimentoDescarte()->format('d/m/Y') : "Recebimento não Registrado";
            $valor[] = "";
            $dados[] = $valor;
        }

        //Organiza o retorno
        $retorno['draw'] = $request['draw'];
        $retorno['recordsTotal'] = $registrosTotais;
        $retorno['recordsFiltered'] = $registrosTotaisBusca;
        $retorno['data'] = $dados;


        //Retorna a resposta
        $response->setContent(\Zend\Json\Json::encode($retorno));
        return $response;
    }

    /**
     * Método padrão de formulário
     * @return \Application\Controller\ViewModel
     */
    public function formularioAction() {

        //Verifica a permissão de acesso ao formulário
        if ($this->params()->fromRoute('id')) {
            $this->validarPermissaoFormulario($this->params()->fromRoute('id'));
        } else {
            $this->redirect()->toRoute('home/default', array('controller' => 'home', 'action' => 'index'));
        }

        //Obtem a lista de itens de descarte
        $itensDescarte = $this->getServiceLocator()->get('Estoque\Repository\ItemDescarteRepository')->findBy(array('solicitacaoDescarte' => $this->params()->fromRoute('id')));

        //Instancia o formulário e a requisição
        $form = $this->getServiceLocator()->get($this->nameForm);
        $request = $this->getRequest();
        $qtdRecebida = array();
        $solicitacaoDescarte = $this->params()->fromRoute('id');

        //Verifica se a requisição foi feita via post
        if ($request->isPost()) {

            //Manda os dados para o formulário
            $dados = $request->getPost()->toArray();
            $form->setData($dados);

            //Valida o formulário
            if ($form->valid($dados['qtdRecebida'], $dados['itemRecebido'])) {
                $dados['solicitacaoDescarte'] = $solicitacaoDescarte;
                $service = $this->getServiceLocator()->get($this->nameService);
                $success = $service->save($dados);

                $this->setConfirmMessages($success);

                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            } else {
                $qtdRecebida = $dados['qtdRecebida'];
                $this->flashMessenger()->addErrorMessage('Por favor, verifique os campos do formulário');
            }
            
        }else if ($this->params()->fromRoute('id')) {
            
            //Busca os dados do Recebimento da Descarte usando o ID do solicitação de descarte
            $dados = $this->getServiceLocator()->get('Estoque\Repository\RecebimentoDescarteRepository')->findBy(["solicitacaoDescarte" => $this->params()->fromRoute('id')]);

        }

        //Seta os scripts e retorna para view
        if (!empty($this->jsFormulario)) {
            $this->layout()->setVariable('scripts', $this->jsFormulario);
        }

        if (!empty($this->cssFormulario)) {
            $this->layout()->setVariable('styles', $this->cssFormulario);
        }


        
        //Retorna os dados da view Model
        return new ViewModel(array('form' => $form, 'itensDescarte' => $itensDescarte, 'qtdRecebida' => $qtdRecebida, 'solicitacaoDescarte' => $solicitacaoDescarte));
    }

    public function visualizarAction() {
        return $this->formularioAction();
    }

    /**
     * Método que valida se o usuário possuí permissão no formulário, levando em consideração o id da solicitação de descarte
     * 
     * @param integer $solicitacaoDescarte
     * @return boolean
     */
    private function validarPermissaoFormulario($solicitacaoDescarte) {
        $filtrosUser = $this->getFiltrosSelecionadosUser();
        $solicitacaoDescarte = $this->getServiceLocator()->get('Estoque\Repository\SolicitacaoDescarteRepository')->find($solicitacaoDescarte);
        
        if ($solicitacaoDescarte
                && $solicitacaoDescarte->getEstoqueDestino()->getId() != $filtrosUser['estoque']['id'] 
                && $this->params()->fromRoute('action') == 'formulario') {
            
            return $this->redirect()->toRoute('home/default', array('controller' => 'home', 'action' => 'index'));
        } else {
            return true;
        }
    }

}
