<?php

namespace Estoque\Controller;

use Zend\View\Model\ViewModel;
use Application\Controller\CrudApplicationController;
use Estoque\Entity\SolicitacaoDescarte;

class DescarteController extends CrudApplicationController {
    

    protected $nameForm = 'Estoque\Form\Descarte';
    protected $nameRepository = 'Estoque\Repository\SolicitacaoDescarteRepository';
    protected $nameService = 'Estoque\Service\Descarte';
    protected $nameFormFilter = 'Estoque\Form\TransferenciaFiltro';
    protected $controller = 'descarte';
    protected $route = 'estoque/default';
    protected $jsFormulario = array('descarte/formulario.js');
    protected $jsIndex = array('descarte/index.js');

    public function indexAction() {

        //Seta os scripts e retorna para view
        if (!empty($this->jsIndex)) {
            $this->layout()->setVariable('scripts', $this->jsIndex);
        }

        if (!empty($this->cssIndex)) {
            $this->layout()->setVariable('styles', $this->cssIndex);
        }

        //Busca mensagens a serem exibidas na tela
        $messages = $this->flashMessenger()->setNamespace('Application')->getMessages();

        //Busca o formulário do filtro
        $formFilter = !empty($this->nameFormFilter) ? $this->getServiceLocator()->get($this->nameFormFilter) : null;

        //Retorna os dados para view
        return new ViewModel(array('messages' => $messages, 'formFilter' => $formFilter));
    }

    public function paginacaoAction() {
        //Armazena a resposta
        $response = $this->getResponse();

        //Armazena a requisição
        $request = $this->getRequest()->getPost();

        //Busca os filtros do usuário
        $dadosUser = $this->getDadosUser();

        $filtrosSelecionados = $this->getFiltrosSelecionadosUser();

        //Organiza os dados da busca
        $busca = array(
            "estoques" => $dadosUser['estoques'],
            "busca" => mb_strtolower($request['search']['value'], 'UTF-8'),
            "situacao" => $request['situacao']
        );

        //Cria a ordenação
        $order = array();

        if (isset($request['order'][0]['column'])) {
            switch ($request['order'][0]['column']) {
                case 1: $order = array('field' => 'sd.numeroTransferencia', 'order' => $request['order'][0]['dir']);
                    break;
                case 2: $order = array('field' => 'sd.dataDescarte', 'order' => $request['order'][0]['dir']);
                    break;
                case 3: $order = array('field' => 'e.nome', 'order' => $request['order'][0]['dir']);
                    break;
                case 4: $order = array('field' => 's.nome', 'order' => $request['order'][0]['dir']);
                    break;
            }
        }

        //Busca os dados
        $registros = $this->getServiceLocator()->get($this->nameRepository)->busca($busca, $request['length'], $request['start'], $order);
        $registrosTotaisBusca = $this->getServiceLocator()->get($this->nameRepository)->busca($busca, 0, 0, array(), true);
        $registrosTotais = $this->getServiceLocator()->get($this->nameRepository)->countAll();
        $dados = array();

        //Armazena os dados retornados em um array
        foreach ($registros as $registro) {
            $valor = array();
            $valor[] = '<input type="checkbox" name="checkbox-list" class="checkboxes" value="' . $registro->getId() . '" />';

            if ($this->verifyPermission('descarte', 'editar') && $registro->getEstoqueOrigem()->getId() == $filtrosSelecionados['estoque']['id']) {

                $arr = array(
                    'route' => $this->route,
                    'controller' => $this->controller,
                    'action' => 'formulario',
                    'id' => $registro->getId()
                );

                $title = 'Editar Item';
            } else {
                $arr = array(
                    'route' => $this->route,
                    'controller' => $this->controller,
                    'action' => 'visualizar',
                    'id' => $registro->getId()
                );

                $title = 'Visualizar Item';
            }

            $links = "";

            if ($this->verifyPermission('descarte', 'cancelarDescarte') && $registro->getEstoqueOrigem()->getId() == $filtrosSelecionados['estoque']['id']) {
                $links .= "<a href='javascript:cancelarDescarte(" . $registro->getId() . ");' class='fa fa-ban'></a> ";
            }

            if ($this->verifyPermission('descarte', 'estornarDescarte') && $registro->getEstoqueOrigem()->getId() == $filtrosSelecionados['estoque']['id'] && ($registro->getSituacaoDescarte()->getId() == SolicitacaoDescarte::STATUS_PENDENTE || $registro->getSituacaoDescarte()->getId() == SolicitacaoDescarte::STATUS_SOLICITADO)) {
                $links .= "&nbsp;&nbsp;&nbsp;&nbsp;<a  href='" . $this->urls(array('route' => $this->route, 'controller' => $this->controller, 'action' => 'estorno', 'id' => $registro->getId())) . "' class='fa fa-exclamation colorbox'></a> ";
            }

            $valor[] = "<a href='" . $this->urls($arr) . "' title='" . $title . "'>" . $registro->getNumeroDescarte() . "</a>";
            $valor[] = $registro->getDataDescarte() ? $registro->getDataDescarte()->format('d/m/Y') : '';
            $valor[] = $registro->getEstoqueOrigem()->getNome();

            $classCSS = $this->getLabelSituacao($registro->getSituacaoDescarte()->getNome());

            $valor[] = $registro->getSituacaoDescarte() ? "<span class='label label-sm " . $classCSS . "'>" . $registro->getSituacaoDescarte()->getNome() . "</span>" : "";
            $valor[] = $links;
            $dados[] = $valor;
        }

        //Organiza o retorno
        $retorno['draw'] = $request['draw'];
        $retorno['recordsTotal'] = $registrosTotais;
        $retorno['recordsFiltered'] = $registrosTotaisBusca;
        $retorno['data'] = $dados;


        //Retorna a resposta
        $response->setContent(\Zend\Json\Json::encode($retorno));
        return $response;
    }

    /**
     * Método padrão de formulário
     * @return \Application\Controller\ViewModel
     */
    public function formularioAction() {
        
        //Habilita o formulário por padrão
        $enabledForm = true;

        //Valida a permissão na edição 
        if ($this->params()->fromRoute('id')) {
            $enabledForm = $this->validarPermissaoFormulario($this->params()->fromRoute('id'));
        }

        //Instancia o formulário e a requisição
        $form = $this->getServiceLocator()->get($this->nameForm);
        $request = $this->getRequest();
        $itensDescarte = array();

        //Verifica se a requisição foi feita via post
        if ($request->isPost()) {

            //Busca os dados
            $dados = $request->getPost()->toArray();

            if ($this->params()->fromRoute('id')) {
                $dados['id'] = $this->params()->fromRoute('id');
            }

            $form->setData($dados);

            //Prepara as variáveis referentes aos itens do pedido para validação
            $itensProduto = isset($dados['id-produto']) ? $dados['id-produto'] : array();
            $itensQuantidade = isset($dados['qtd-produto']) ? $dados['qtd-produto'] : array();
            $valorTotal = isset($dados['valor-total']) ? $dados['valor-total'] : array();
            $precoMedio = isset($dados['preco-medio']) ? $dados['preco-medio'] : array();
            $itensDescarte = isset($dados['id-item-descarte']) ? $dados['id-item-descarte'] : array();

            //Valida o formulário
            if ($form->valid($itensProduto, $itensQuantidade, $valorTotal, $precoMedio, $itensDescarte)) {
                $service = $this->getServiceLocator()->get($this->nameService);
                $success = $service->save($dados);
                $this->setConfirmMessages($success);

                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            } else {

                //Cria um array com todos os itens do descarte para serem reenseridos no form
                $itensDescarte = $this->recomporItensDescarteForm($dados);

                //Retira os produtos que já haviam sido selecionados caso eles existam
                if (isset($dados['id-produto'])) {
                    $form->retirarOpcoesProdutos($dados['id-produto']);
                }

                $this->flashMessenger()->addErrorMessage('Por favor, verifique os campos do formulário');
            }
        } elseif ($this->params()->fromRoute('id')) {

            //Busca os dados
            $dados = $this->getServiceLocator()->get($this->nameRepository)->find($this->params()->fromRoute('id'));

            if ($dados) {

                //Popula os itens do pedido
                $itensDescarte = $this->recomporItensDescarteEntity($dados);

                //Retira das opções os produtos selecionados
                $form->retirarOpcoesProdutos($dados->getIdProdutosDescarte());

                //Passa os dados para o form
                $form->setData($dados->toArray());

                //Verifica se o formulário deve ficar habilitado para a edição
                $enabledForm = $dados->getSituacaoDescarte()->getId() == SolicitacaoDescarte::STATUS_CADASTRADO ? true : false;
            }
        }

        //Seta os scripts e retorna para view
        if (!empty($this->jsFormulario)) {
            $this->layout()->setVariable('scripts', $this->jsFormulario);
        }

        if (!empty($this->cssFormulario)) {
            $this->layout()->setVariable('styles', $this->cssFormulario);
        }

        return new ViewModel(array('form' => $form, 'itensDescarte' => $itensDescarte, 'enabledForm' => $enabledForm));
    }

    /**
     * Ação de Visualizar
     * @return type
     */
    public function visualizarAction() {
        if (!$this->params()->fromRoute('id')) {
            return $this->redirect()->toRoute('home/default', array('controller' => 'home', 'action' => 'index'));
        }

        return $this->formularioAction();
    }

    /**
     * Ação de Cancelar
     * 
     * @return boolean
     */
    public function cancelarDescarteAction() {
        $post = $this->getRequest()->getPost()->toArray();

        if (!$this->validarPermissaoFormulario($post['id'])) {
            return false;
        }

        if ($this->getServiceLocator()->get($this->nameService)->cancelar($post['id'])) {
            die(\Zend\Json\Json::encode(array('success' => 1)));
        } else {
            die(\Zend\Json\Json::encode(array('success' => 0)));
        }
    }

    /**
     * Ação de Deletar
     * 
     * @return boolean
     */
    public function deletarAction() {
        $dados = $this->getRequest()->getPost();

        foreach ($dados['ids'] as $id) {
            if (!$this->validarPermissaoFormulario($id)) {
                return false;
            }
        }

        parent::deletarAction();
    }

    public function estornoAction() {

        $form = $this->getServiceLocator()->get('Estoque\Form\EstornoDescarte');
        $request = $this->getRequest();
        $solicitacaoDescarte = $this->getServiceLocator()->get('Estoque\Repository\SolicitacaoDescarteRepository')->find($this->params()->fromRoute('id'));
        $estornos = $this->getServiceLocator()->get('Estoque\Repository\EstornoDescarteRepository')->getEstornosSolicitacaoDescarte($this->params()->fromRoute('id'));

        if ($request->isPost()) {
            if ($this->getServiceLocator()->get('Estoque\Service\EstornoDescarte')->save($request->getPost()->toArray())) {
                $this->setConfirmMessages(true);
                die(\Zend\Json\Json::encode(array('success' => 1)));
            } else {
                $this->flashMessenger()->addErrorMessage('Erro ao realizar estorno!');
                die(\Zend\Json\Json::encode(array('success' => 0)));
            }
        }

        $viewModel = new ViewModel(array('form' => $form, 'solicitacaoDescarte' => $solicitacaoDescarte, 'id' => $this->params()->fromRoute('id'), 'estornos' => $estornos));
        $viewModel->setTerminal(true);

        return $viewModel;
    }

    /**
     * Método chamado via ajax que retorna a quantidade de produtos disponíveis em um determinado estoque
     */
    public function getQuantidadeDisponivelAction() {
        $request = $this->getRequest()->getPost()->toArray();
        $filtrosUser = $this->getFiltrosSelecionadosUser();
        $quantidade = $this->getServiceLocator()->get('Estoque\Repository\EstoqueProdutoRepository')->getQuantidadeDisponivelEstoque($request['produto'], $filtrosUser['estoque']['id']);

        if ($request['solicitacaoDescarte']) {
            $itemDescarte = $this->getServiceLocator()->get('Estoque\Repository\ItemDescarteRepository')->findOneBy(array('solicitacaoDescarte' => $request['solicitacaoDescarte'], 'produto' => $request['produto']));
            $quantidade += $itemDescarte ? $itemDescarte->getQtd() : 0;
        }

        die(\Zend\Json\Json::encode(array('quantidade' => $quantidade)));
    }

    private function validarPermissaoCentral($redirect = false) {

        //Valida se o usuário logado possuí o estoque central selecionado por padrão
        $dadosUser = $this->getDadosUser();
        $central = $dadosUser['filtrosSelecionados']['estoque']['central'];

        if (($this->params()->fromRoute('action') == 'formulario' || $this->params()->fromRoute('action') == 'deletar' || $this->params()->fromRoute('action') == 'index' || $this->params()->fromRoute('action') == 'paginacao' || $this->params()->fromRoute('action') == 'cancelarPedido') && !$central) {
            return $redirect ? $this->redirect()->toRoute('home/default', array('controller' => 'home', 'action' => 'index')) : false;
        } else {
            return true;
        }
    }

    /**
     * Método que valida se o usuário possuí permissão no formulário, levando em consideração o id da solicitação
     * 
     * @return boolean
     */
    private function validarPermissaoFormulario($solicitacaoDescarte) {
        $filtrosSelecionados = $this->getFiltrosSelecionadosUser();
        $solicitacaoDescarte = $this->getServiceLocator()->get($this->nameRepository)->findOneBy(array('id' => $solicitacaoDescarte, 'estoqueOrigem' => $filtrosSelecionados['estoque']['id']));

        if (!$solicitacaoDescarte) {
            if ($this->params()->fromRoute('action') == 'visualizar') {
                return false;
            } else {
                return $this->redirect()->toRoute('home/default', array('controller' => 'home', 'action' => 'index'));
            }
        } else {
            return true;
        }
    }

    /**
     * Organiza os itens para serem recompostos no form 
     * 
     * @param array $dados
     * @return array
     */
    private function recomporItensDescarteForm($dados) {

        //Cria o array que armazenará os itens de forma organizada para form
        $itens = array();

        if (isset($dados['id-produto'])) {
            foreach ($dados['id-produto'] as $key => $item) {
                $itens[$key]['id-item-descarte'] = $dados['id-item-descarte'][$key];
                $itens[$key]['id-produto'] = $dados['id-produto'][$key];
                $itens[$key]['codigo-produto'] = $dados['codigo-produto'][$key];
                $itens[$key]['nome-produto'] = $dados['nome-produto'][$key];
                $itens[$key]['preco-medio'] = $dados['preco-medio'][$key];
                $itens[$key]['qtd-produto'] = $dados['qtd-produto'][$key];
                $itens[$key]['valor-total'] = $dados['valor-total'][$key];
            }
        }

        return $itens;
    }

    /**
     * Organiza os itens para serem recompostos no form 
     * 
     * @param SolicitacaoDescarte $solicitacaoDescarte
     * @return array
     */
    private function recomporItensDescarteEntity(SolicitacaoDescarte $solicitacaoDescarte) {

        //Cria o array que armazenará os itens de forma organizada para form
        $itens = array();

        foreach ($solicitacaoDescarte->getItensDescarte() as $key => $item) {
            $itens[$key]['id-item-descarte'] = $item->getId();
            $itens[$key]['id-produto'] = $item->getProduto()->getId();
            $itens[$key]['codigo-produto'] = $item->getProduto()->getCodigo();
            $itens[$key]['nome-produto'] = $item->getProduto()->getNome();
            $itens[$key]['preco-medio'] = $item->getCustoMedio();
            $itens[$key]['qtd-produto'] = $item->getQtd();
            $itens[$key]['valor-total'] = $item->getValorTotal();
        }

        return $itens;
    }

}
