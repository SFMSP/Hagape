<?php


namespace Estoque\Form;
use Zend\Form\Form;

/**
 * Classe que abstrai o filtro na listagem de pedidos
 *
 * @autor Evandro Melos
 */
class PedidoFiltro extends Form
{
    /**
     * Monta o formulário de filtro de pedidos
     * @param array
     */
    public function __construct(array $situacao) {
        //Seta o nome do formulário
        parent::__construct('pedido');

        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'stdform stdform2'));

        //Adiciona o select de situacao
        $this->add(array(
            'name' => 'situacao',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'situacao'
            ),
            'options' => array(
                'label' => 'Situacao: ',
                'value_options' => $situacao,
                'disable_inarray_validator' => true
            ),
        ));
    }
}