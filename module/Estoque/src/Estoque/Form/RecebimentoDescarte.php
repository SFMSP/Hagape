<?php

namespace Estoque\Form;

use Zend\Form\Form;
use Estoque\InputFilter\RecebimentoDescarte as RecebimentoDescarteFilter;
use Estoque\Repository\ItemDescarteRepository;

/**
 * Classe que abstrai o formulário de Recebimento de Recebimento de Descarte
 *
 * @autor Eduardo Praxedes Heinske <eduardo.praxedes@jointecnolonogia.com>
 */
class RecebimentoDescarte extends Form {

    /**
     *
     * @var ItemDescarteRepository 
     */
    protected $itemDescarteRepository;

    /**
     * 
     * @param ItemDescarteRepository $itemDescarteRepository
     * @return \Estoque\Form\RecebimentoDescarte
     */
    public function setItemDescarteRepository(ItemDescarteRepository $itemDescarteRepository) {
        $this->itemDescarteRepository = $itemDescarteRepository;
        return $this;
    }

    /**
     * Monta o formulário de Recebimento de Descarte
     */
    public function __construct(RecebimentoDescarteFilter $inputFilter) {
        //Seta o nome do formulário
        parent::__construct('recebimentoDescarte');

        $this->setLabel('Recebimento de Descarte');
        $this->setAttribute('enctype', 'multipart/form-data');
        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'horizontal-form'));
        $this->setInputFilter($inputFilter);

        //Adiciona o campo que recebe a data de recebimento
        $this->add(array(
            'name' => 'dataRecebimento',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'dataRecebimento',
                'class' => 'form-control datepicker'
            ),
            'options' => array(
                'label' => 'Data do Descarte:'
            ),
        ));

   

        //Adiciona o campo que recebe as observações
        $this->add(array(
            'name' => 'observacao',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'id' => 'observacao',
                'class' => 'form-control',
                'rows' => "5",
                'style' => "overflow:auto; resize:none;"
            ),
            'options' => array(
                'label' => 'Observação:',
                'label_attributes' => array(
                    'class' => 'control-label',
                )
            ),
        ));

        //Monta o campo hidden que receberá erros genéricos de validação do formulário de recebiementos que não se encaixarem em nenhum dos campos do form
        $this->add(array(
            'name' => 'hdnErrosRecebimento',
            'type' => 'Zend\Form\Element\Hidden'
                )
        );

        //Monta o campo de upload da nota
        $this->add(array(
            'name' => 'id',
            'type' => 'Zend\Form\Element\Hidden'
        ));

        //Monta o submit
        $this->add(array(
            'name' => 'enviar',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => 'ENVIAR',
                'class' => 'btn btn-primary submit-input-loading'
            )
        ));
    }

    public function valid($qtdRecebida = array(), $itemRecebido = array()) {
        $valid = parent::isValid();
        $qtdTotalRecebida = 0;
        $boolObservacaoObrigatorio = false;

        foreach ($qtdRecebida as $key => $qtd) {
            if ($qtd == 0 && !$boolObservacaoObrigatorio){
                $boolObservacaoObrigatorio = true;
            }
            $qtdTotalRecebida += $qtd;
        }

        if ($qtdTotalRecebida == 0) {
            $this->get('hdnErrosRecebimento')->setMessages(array('Informe o recebimento de pelo menos um item'));
            $valid = false;
        }

        if( $boolObservacaoObrigatorio ){
            if ( $this->get('observacao')->getValue() == '' ){
                $this->get('observacao')->setLabel('Observação*:');
                $this->get('observacao')->setMessages(array('Campo Observação Obrigarório devido a ausência de recebimento de todos os itens'));
                $valid = false;
            }
        }

        foreach ($itemRecebido as $key => $item) {
            $itemTransDescarte = $this->itemDescarteRepository->find($item);

            if ($qtdRecebida[$key] > $itemTransDescarte->getQtd()) {
                $this->get('hdnErrosRecebimento')->setMessages(array('A quantidade recebida de um item não pode ser maior do que a quantidade descartada.'));

                $valid = false;
            }
        }

        return $valid;
    }

}
