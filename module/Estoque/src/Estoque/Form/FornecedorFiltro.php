<?php
namespace Estoque\Form;

use Zend\Form\Form;

/**
 * Classe que abstrai o filtro na listagem de usuários
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class FornecedorFiltro extends Form {


    /**
     * Monta o formulário de filtro de vendas
     * @param array 
     */
    public function __construct(array $fornecedor) {
        //Seta o nome do formulário
        parent::__construct('fornecedor');

        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'stdform stdform2'));

        //Adiciona o select de clientes
        $this->add(array(
            'name' => 'nome',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'nome'
            ),
            'options' => array(
                'label' => 'nome: ',
                'value_options' => $fornecedor,
                'disable_inarray_validator' => true,
            ),
        ));
    }

}
