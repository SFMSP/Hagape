<?php

namespace Estoque\Form;

use Zend\Form\Form;

/**
 * Classe que abstrai o formulário de Recebimento de Estorno de descarte
 *
 * @autor Eduardo Praxedes Heinske <eduardo.praxedes@jointecnolonogia.com>
 */
class EstornoDescarte extends Form {

    /**
     * Monta o formulário de Estorno
     */
    public function __construct() {
        //Seta o nome do formulário
        parent::__construct('estornoDescarte');

        $this->setLabel('Estorno');
        $this->setAttribute('enctype', 'multipart/form-data');
        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'horizontal-form'));

        //Adiciona o campo que recebe a data do estorno
        $this->add(array(
            'name' => 'dataEstorno',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'dataEstorno',
                'class' => 'form-control datepicker'
            ),
            'options' => array(
                'label' => 'Data do Estorno:'
            ),
        ));

        //Adiciona o campo que recebe as observações
        $this->add(array(
            'name' => 'motivo',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'id' => 'motivo',
                'class' => 'form-control',
                'rows' => "5",
                'style' => "overflow:auto; resize:none;"
            ),
            'options' => array(
                'label' => 'Motivo: ',
                'label_attributes' => array(
                    'class' => 'control-label',
                )
            ),
        ));

        //Monta o campo de upload da nota
        $this->add(array(
            'name' => 'id',
            'type' => 'Zend\Form\Element\Hidden'
        ));

        //Monta o submit
        $this->add(array(
            'name' => 'enviar',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => 'ENVIAR',
                'class' => 'btn btn-primary submit-input-loading'
            )
        ));
    }

}
