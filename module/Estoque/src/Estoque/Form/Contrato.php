<?php

namespace Estoque\Form;

use Zend\Form\Form;
use Estoque\InputFilter\Contrato as ContratoFilter;

/**
 * Classe que abstrai o formulário de Contrato
 *
 * @autor Evandro Noguêz Melos <evandro.noguez@jointecnolonogia.com>
 */
class Contrato extends Form
{
    private $contratoRepository;

    /**
     * @return mixed
     */
    public function getContratoRepository()
    {
        return $this->contratoRepository;
    }

    /**
     * @param mixed $contratoService
     */
    public function setContratoRepository($contratoRepository)
    {
        $this->contratoRepository = $contratoRepository;
        return $this;
    }

    /**
     * Monta o formulário de Contrato
     */
    public function __construct(ContratoFilter $inputFilter, $fornecedor = array() )
    {
        //Seta o nome do formulário
        parent::__construct('contrato');

        $this->setLabel('Contrato');
        $this->setAttribute('icon', 'icon-user');
        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'horizontal-form'));

        $this->setInputFilter($inputFilter);

        /**
         * Componentes do Formulario de CONTRATO
         */

        //Adiciona o select de fornecedor
        $this->add(array(
            'name' => 'fornecedor',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'fornecedor',
                'class' => 'form-control autocomplete-select2'
            ),
            'options' => array(
                'label' => 'Fornecedor*:',
                'value_options' => $fornecedor,
                'disable_inarray_validator' => true,
                'label_attributes' => array(
                    'class' => 'control-label'
                )
            ),
        ));

        //Adiciona o campo que recebe o numero do contrato
        $this->add(array(
            'name' => 'numContrato',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'numContrato',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Nº contrato*:',
                'label_attributes' => array(
                    'class' => 'control-label',
                )
            ),
        ));

        //Adiciona o campo que recebe o nome do usuário
        $this->add(array(
            'name' => 'numProcessoLicitatorio',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'numProcessoLicitatorio',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Nº Proc. Licitatório *:',
                'label_attributes' => array(
                    'class' => 'control-label'
                )
            ),
        ));

        //Adiciona o campo que recebe o valor
        $this->add(array(
            'name' => 'valor',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'valor',
                'class' => 'form-control money col-md-1',
                'placeholder' => '0,00'
            ),
            'options' => array(
                'label' => 'Valor*:',
                'label_attributes' => array(
                    'class' => 'control-label'
                )
            ),
        ));

        //Adiciona o campo que recebe a data, já que é criado na view
        //Adicionado para carater de validação do campo
        $this->add(array(
            'name' => 'dataVencimento',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'dataVencimento',
                'class' => 'form-control datepicker',
                'value'=> date('d/m/Y')
            ),
            'options' => array(
                'label' => 'Data Vencimento*:',
                'label_attributes' => array(
                    'class' => 'control-label'
                )
            ),
        ));

        //Radio de Status
        $this->add(array(
            'name' => 'ativo',
            'type' => 'Zend\Form\Element\Radio',
            'attributes' => array(
                'id' => 'status',
                'value' => 1,
            ),
            'options' => array(
                'label' => 'Status *:',
                'label_attributes' => array(
                    'class' => 'control-label',
                ),
                'value_options' => array(
                    '1' => 'Ativo',
                    '0' => 'Inativo',
                ),
            )
        ));

        /**
         * Componentes do Formulario de EMPENHO
         */

        //Adiciona o campo que recebe o numero do contrato
        $this->add(array(
            'name' => 'numEmpenho',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'numEmpenho',
                'class' => 'form-control',
                'onchange' => 'validaNumeroEmpenho()'
            ),
            'options' => array(
                'label' => 'Nº do Empenho*:',
                'label_attributes' => array(
                    'class' => 'control-label',
                )
            ),
        ));

        //Adiciona o campo que recebe o numero do contrato
        $this->add(array(
            'name' => 'provisorio',
            'type' => 'Zend\Form\Element\CheckBox',
            'attributes' => array(
                'id' => 'provisorio',
                'class' => 'form-control',
                'value' => 'false'
            ),
            'options' => array(
                'label' => 'Provisório',
                'label_attributes' => array(
                    'class' => 'control-label',
                )
            ),
        ));

        //Adiciona o campo que recebe o numero do contrato
        $this->add(array(
            'name' => 'observacao',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'id' => 'observacao',
                'class' => 'form-control',
                'rows' => "5",
                'style' => "overflow:auto; resize:none;"
            ),
            'options' => array(
                'label' => 'Observação:',
                'label_attributes' => array(
                    'class' => 'control-label',
                )
            ),
        ));

        //Adiciona o campo que recebe o valor
        $this->add(array(
            'name' => 'valorEmpenho',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'valorEmpenho',
                'class' => 'form-control money col-md-1',
                'placeholder' => '0,00'
            ),
            'options' => array(
                'label' => 'Valor*:',
                'label_attributes' => array(
                    'class' => 'control-label'
                )
            ),
        ));

        //Adiciona o campo que recebe a data, já que é criado na view
        //Adicionado para carater de validação do campo
        $this->add(array(
            'name' => 'dataEmpenho',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'dataEmpenho',
                'class' => 'form-control datepicker',
                 'value'=> date('d/m/Y')
            ),
            'options' => array(
                'label' => 'Data:',
                'label_attributes' => array(
                    'class' => 'control-label'
                )
            ),
        ));


        //Adiciona os campos HIdden para pegar o valor dos datapicker
        $this->add(array(
            'name' => 'hiddenDataEmpenho',
            'type' => 'Zend\Form\Element\Hidden',
            'attributes' => array(
                'id' => 'hiddenDataEmpenho'
            ),
        ));

        $this->add(array(
            'name' => 'hiddenDataVencimento',
            'type' => 'Zend\Form\Element\Hidden',
            'attributes' => array(
                'id' => 'hiddenDataVencimento'
            ),
        ));

        $this->add(array(
            'name' => 'hiddenValorContrato',
            'type' => 'Zend\Form\Element\Hidden',
            'attributes' => array(
                'id' => 'hiddenValorContrato',
                'value' => 0.00
            ),
        ));

        $this->add(array(
            'name' => 'hiddenValorDisponivel',
            'type' => 'Zend\Form\Element\Hidden',
            'attributes' => array(
                'id' => 'hiddenValorDisponivel',
                'value' => 0.00
            ),
        ));

        $this->add(array(
            'name' => 'hiddenValorTotalEmpenhado',
            'type' => 'Zend\Form\Element\Hidden',
            'attributes' => array(
                'id' => 'hiddenValorTotalEmpenhado',
                'value' => 0.00
            ),
        ));

        //Monta o submit
        $this->add(array(
            'name' => 'enviar',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => 'Salvar',
                'class' => 'btn btn-primary submit-input-loading',
                'id' => 'enviar'
            )
        ));

        //Adiciona o campo hidden que leva o id
        $this->add(array(
            'name' => 'id',
            'type' => 'Zend\Form\Element\Hidden'
        ));

        //Adiciona o campo hidden que leva o id
        $this->add(array(
            'name' => 'hiddenErroListaEmpenho',
            'type' => 'Zend\Form\Element\Hidden',
            'options' => array(
                'label_attributes' => array(
                    'class' => 'control-label'
                )
            ),
        ));

        //Adiciona o campo hidden que leva os ids dos itens excluídos
        $this->add(array(
            'name' => 'idEmpenhosExcluidos',
            'type' => 'Zend\Form\Element\Hidden',
            'attributes' => array(
                'id' => 'idEmpenhosExcluidos'
            )
        ));

        //Adiciona o campo hidden que leva os ids dos itens excluídos
        $this->add(array(
            'name' => 'hiddenNumContrato',
            'type' => 'Zend\Form\Element\Hidden',
            'attributes' => array(
                'id' => 'hiddenNumContrato'
            )
        ));
    }

    public function isValid(Array $empenhos = [])
    {
        $this->getInputFilter()->remove('provisorio');
        $valid = parent::isValid();

        if (empty($empenhos)) {
            $this->get('hiddenErroListaEmpenho')->setMessages(array('Adicione um registro na lista de Empenho'));
            $valid = false;
        }
        if ($this->get('hiddenDataVencimento')->getValue() == "") {
            $this->get('dataVencimento')->setMessages(array('Campo obrigatório'));
            $valid = false;
        }

        //Caso o valor do contrato seja alterado e seja colocado um valor menor do que já foi empenhado
        $valorContrato = $this->get('hiddenValorContrato')->getValue();
        $valorDisponivelEmpenho = $this->get('hiddenValorTotalEmpenhado')->getValue();
        $valorInsuficiente = $valorContrato - $valorDisponivelEmpenho;
        if ($valorInsuficiente < 0){
            $this->get('valor')->setMessages(array('Valor do contrato não pode ser menor que o valor já empenhado.'));
            $valid = false;
        }

        //Caso haja contrato com o mesmo numero já cadastrado
        $numContrato = $this->get('numContrato')->getValue();
        $numContratoAnterior = $this->get('hiddenNumContrato')->getValue();
        //Compara se é o mesmo contrato e está sendo alterado outros dados
        if ($numContrato != $numContratoAnterior){
            $contrato = $this->contratoRepository->findBy(['numContrato'=>$numContrato]);
            if($contrato){
                $this->get('numContrato')->setMessages(array('Numero de contrato já cadastrado.'));
                $valid = false;
            }
        }

        return $valid;
    }
}//End of Class