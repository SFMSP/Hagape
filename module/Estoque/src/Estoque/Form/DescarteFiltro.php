<?php


namespace Estoque\Form;
use Zend\Form\Form;

/**
 * Classe que abstrai o filtro na listagem de Transferencia
 *
 * @autor Evandro Melos
 */
class DescarteFiltro extends Form
{
    /**
     * Monta o formulário de filtro de transferencia
     * @param array
     */
    public function __construct(array $situacao) {
        //Seta o nome do formulário
        parent::__construct('descarteFiltro');

        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'stdform stdform2'));

        //Adiciona o select de situacao
        $this->add(array(
            'name' => 'situacao',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'situacao'
            ),
            'options' => array(
                'label' => 'Situacao: ',
                'value_options' => $situacao,
                'disable_inarray_validator' => true
            ),
        ));
    }
}