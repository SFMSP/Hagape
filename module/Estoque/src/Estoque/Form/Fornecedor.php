<?php

namespace Estoque\Form;

use Zend\Form\Form;
use Estoque\InputFilter\Fornecedor as FornecedorFilter;
use Application\Entity\Cidade;
/**
 * Classe que abstrai o formulário de fornecedor
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Fornecedor extends Form {
    /**
     *
     * @var \Estoque\Repository\FornecedorRepository
     */
    private $fornecedorRepository;

    /**
     * 
     * @param \Estoque\Repository\FornecedorRepository $fornecedorRepository
     */
    function setFornecedorRepository(\Estoque\Repository\FornecedorRepository $fornecedorRepository) {
        $this->fornecedorRepository = $fornecedorRepository;
    }

    /**
     * Monta o formulário de fornecedor
     */
    public function __construct(FornecedorFilter $inputFilter, $estados = array(), $cidades = array())
{
        //Seta o nome do formulário
        parent::__construct('fornecedor');

        $this->setLabel('Fornecedor');
        $this->setAttribute('icon', 'icon-user');
        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'form-horizontal form-bordered form-label-stripped'));

        $this->setInputFilter($inputFilter);
        


        //Adiciona o campo que recebe o nome do fornecedor
        $this->add(array(
            'name' => 'nome',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'nome',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Nome*:',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));
        
        
        # Adiciona o campo CNPJ
        $this->add(array(
            'name' => 'cnpj',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'cnpj',
                'class' => 'form-control cnpj',
                 'maxlength'=>'18'
              
            ),
            'options' => array(
                'label' => 'CNPJ*:',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));
      
         # Adiciona o campo Endereço
        $this->add(array(
            'name' => 'endereco',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'endereco',
                'class' => 'form-control endereco',
                 'Placeholder'=>'Av/Rua'
            ),
            'options' => array(
                'label' => 'Endereço*:',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));

        //Adiciona o campo que recebe o número do endereço informado
        $this->add(array(
            'name' => 'numero',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'numero',
                'class' => 'form-control numero',
                 'Placeholder'=>'N°/Complemento'
            ),
            'options' => array(
                'label' => 'N°*: ',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));
        //Adiciona o campo que recebe o bairro do endereço informado
        $this->add(array(
            'name' => 'bairro',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'bairro',
                'class' => 'form-control bairro'
            ),
            'options' => array(
                'label' => 'Bairro°*: ',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));
           //Adiciona o campo que recebe o bairro do endereço informado
        $this->add(array(
            'name' => 'cep',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'cep',
                'class' => 'form-control cep'
            ),
            'options' => array(
                'label' => 'CEP°*: ',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));
       
       //Adiciona o select de estado
        $this->add(array(
            'name' => 'estado',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'estado',
                'class' => 'form-control',
                'Placeholder'=>'N°/Complemento'
            ),
            'options' => array(
                'label' => 'Estado*:',
                'value_options' => $estados,
                'disable_inarray_validator' => true,
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));
        
        //Adiciona o select de cidade
        $this->add(array(
            'name' => 'cidade',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'cidade',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Cidade*:',
                'value_options' =>$cidades,
                'disable_inarray_validator' => true,
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));
        
        
        //Adiciona o campo que recebe o primeiro numero de telefone
        $this->add(array(
            'name' => 'telefone1',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'telefone1',
                'class' => 'form-control telefone1 phone_with_ddd'
            ),
            'options' => array(
                'label' => 'Telefone*: ',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));
        //Adiciona o campo que recebe o segundo numero de telefone
        $this->add(array(
            'name' => 'telefone2',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'telefone2',
                'class' => 'form-control telefone2 phone_with_ddd'
            ),
            'options' => array(
                'label' => 'Telefone 2: ',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));
         //Adiciona o campo que recebe o e-mail
        $this->add(array(
            'name' => 'email',
            'type' =>'Zend\Form\Element\Email',
            'attributes' => array(
                'id' => 'email',
                'class' => 'form-control email',
                'Placeholder'=>'@email.com.'
            ),
            'options' => array(
                'label' => 'E-mail*: ',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));
         //Adiciona o campo que recebe o e-mail
        $this->add(array(
            'name' => 'observacao',
            'type' =>'Zend\Form\Element\Textarea',
            'attributes' => array(
                'id' => 'observacao',
                'class' => 'form-control observacao',
                'Placeholder'=>''
            ),
            'options' => array(
                'label' => 'Observação: ',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));
        
        //Adiciona os campos do tipo radio para opção de ativo e inativo
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'ativo',
            'options' => array(
                'label' => 'Status*:',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label',
                    'style' => 'width: 100px; padding-top: 0;'
                ),
                'value_options' => array(
                    '1' => 'Ativo',
                    '0' => 'Inativo',
                ),
            ),
            'attributes' => array(
                'id' => 'status',
                'value' => 1
            )
        ));

        //Adiciona o campo hidden que leva o id
        $this->add(array(
            'name' => 'id',
            'type' => 'Zend\Form\Element\Hidden'
        ));

        //Monta o submit
        $this->add(array(
            'name' => 'enviar',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => 'ENVIAR',
                'class' => 'btn btn-primary submit-input-loading'
            )
        ));
        
        
    }

    
    public function isValid() {
        $valid = parent::isValid();
        
        $this->get('email')->setMessages(array('Campo obrigatório!'));
        
        
        /*Verifica se é cnpj valido*/
        if(!empty($this->valida_cnpj($this->get('cnpj')->getValue()))){
        //É valido
            $filtros = array('cnpj'=>$this->get('cnpj')->getValue(),'nome'=>$this->get('nome')->getValue(),'id'=>$this->get('id')->getValue());
            //Verifica se é uma edição ou cadastro novo
            if(!empty($this->get('id')->getValue())){
               ///é edição
                $fornec = $this->fornecedorRepository->buscaFornecedorCnpj($filtros);
               
                if(!empty($fornec)){
                    
                    foreach ($fornec as $fornece) {
                       $fornece->getId();
                       $fornece->getCnpj();
                    }
                    
                    if(($fornece->getId()) != ($this->get('id')->getValue())){
                        $this->get('cnpj')->setMessages(array('Esse CNPJ já existe cadastrado em outro Fornecedor!'));
                        $valid = false;

                    }else{

                         $valid = true; 

                    }
                }  

            }else{
                  ///Cadastro novo
                
               // 
                 if(!empty($this->fornecedorRepository->buscaFornecedorCnpj($filtros))){
                $this->get('cnpj')->setMessages(array('Esse CNPJ já existe cadastrado!'));

                $valid = false;
                }
                
                 if(!empty($this->fornecedorRepository->buscaFornecedorExistente($filtros))){

                    $this->get('nome')->setMessages(array('Esse Nome já existe cadastrado!'));

                    $valid = false;
                }


                }


            return $valid;
        }else{
            
            $this->get('cnpj')->setMessages(array('CNPJ Inválido!'));
            $valid = false;
            
        }
       
         
}



/**
         * Valida CNPJ
         *
         * @param string $cnpj 
         * @return bool true para CNPJ correto
         *
         */
        function valida_cnpj ( $cnpj ) {
            // Deixa o CNPJ com apenas números
            $cnpj = preg_replace( '/[^0-9]/', '', $cnpj );
           
            // Garante que o CNPJ é uma string
            $cnpj = (string)$cnpj;

            // O valor original
            $cnpj_original = $cnpj;

            // Captura os primeiros 12 números do CNPJ
            $primeiros_numeros_cnpj = substr( $cnpj, 0, 12 );

            /**
             * Multiplicação do CNPJ
             *
             * @param string $cnpj Os digitos do CNPJ
             * @param int $posicoes A posição que vai iniciar a regressão
             * @return int O
             *
             */
            if ( ! function_exists('multiplica_cnpj') ) {
                function multiplica_cnpj( $cnpj, $posicao = 5 ) {
                    // Variável para o cálculo
                    $calculo = 0;

                    // Laço para percorrer os item do cnpj
                    for ( $i = 0; $i < strlen( $cnpj ); $i++ ) {
                        // Cálculo mais posição do CNPJ * a posição
                        $calculo = $calculo + ( $cnpj[$i] * $posicao );

                        // Decrementa a posição a cada volta do laço
                        $posicao--;

                        // Se a posição for menor que 2, ela se torna 9
                        if ( $posicao < 2 ) {
                            $posicao = 9;
                        }
                    }
                    // Retorna o cálculo
                    return $calculo;
                }
            }

            // Faz o primeiro cálculo
            $primeiro_calculo = multiplica_cnpj( $primeiros_numeros_cnpj );

            // Se o resto da divisão entre o primeiro cálculo e 11 for menor que 2, o primeiro
            // Dígito é zero (0), caso contrário é 11 - o resto da divisão entre o cálculo e 11
            $primeiro_digito = ( $primeiro_calculo % 11 ) < 2 ? 0 :  11 - ( $primeiro_calculo % 11 );

            // Concatena o primeiro dígito nos 12 primeiros números do CNPJ
            // Agora temos 13 números aqui
            $primeiros_numeros_cnpj .= $primeiro_digito;

            // O segundo cálculo é a mesma coisa do primeiro, porém, começa na posição 6
            $segundo_calculo = multiplica_cnpj( $primeiros_numeros_cnpj, 6 );
            $segundo_digito = ( $segundo_calculo % 11 ) < 2 ? 0 :  11 - ( $segundo_calculo % 11 );

            // Concatena o segundo dígito ao CNPJ
            $cnpj = $primeiros_numeros_cnpj . $segundo_digito;

            // Verifica se o CNPJ gerado é idêntico ao enviado
            if ( $cnpj === $cnpj_original ) {
                return true;
            }
        }



}