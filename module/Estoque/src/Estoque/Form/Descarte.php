<?php

namespace Estoque\Form;

use Zend\Form\Form;
use Estoque\InputFilter\Descarte as DescarteFilter;
use Estoque\Repository\EstoqueProdutoRepository;
use Estoque\Repository\ProdutoRepository;
use Estoque\Repository\ItemDescarteRepository;

/**
 * Classe que abstrai o formulário de transferencia
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Descarte extends Form {

    /**
     *
     * @var EstoqueProdutoRepository
     */
    protected $estoqueProdutoRepository;

    /**
     *
     * @var ProdutoRepository
     */
    protected $produtoRepository;

    /**
     *
     * @var array 
     */
    protected $dadosUser;

    /**
     *
     * @var ItemDescarteRepository 
     */
    protected $itemDescarteRepository;

    /**
     * 
     * @param EstoqueProdutoRepository $estoqueProdutoRepository
     * @return \Estoque\Form\Transferencia
     */
    public function setEstoqueProdutoRepository(EstoqueProdutoRepository $estoqueProdutoRepository) {
        $this->estoqueProdutoRepository = $estoqueProdutoRepository;
        return $this;
    }

    /**
     * 
     * @param ProdutoRepository $produtoRepository
     * @return \Estoque\Form\Transferencia
     */
    public function setProdutoRepository(ProdutoRepository $produtoRepository) {
        $this->produtoRepository = $produtoRepository;
        return $this;
    }

    /**
     * 
     * @param array $dadosUser
     * @return \Estoque\Form\Transferencia
     */
    public function setDadosUser(array $dadosUser) {
        $this->dadosUser = $dadosUser;
        return $this;
    }

    /**
     * 
     * @param ItemDescarteRepository $itemDescarteRepository
     * @return \Estoque\Form\Transferencia
     */
    public function setItemDescarteRepository(ItemDescarteRepository $itemDescarteRepository) {
        $this->itemDescarteRepository = $itemDescarteRepository;
        return $this;
    }

    /**
     * Monta o formulário de fornecedor
     */
    public function __construct(DescarteFilter $inputFilter, $estoques = array(), $produtos = array()) {
        //Seta o nome do formulário
        parent::__construct('descarte');

        $this->setLabel('Descarte');
        $this->setAttribute('icon', 'icon-user');
        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'horizontal-form'));
        $this->setInputFilter($inputFilter);


        //Adiciona o campo que recebe o número da transferência
        $this->add(array(
            'name' => 'numeroDescarte',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'numeroDescarte',
                'class' => 'form-control',
                'disabled' => true
            ),
            'options' => array(
                'label' => 'Nº da Transferência:'
            ),
        ));

        //Adiciona o campo que recebe a data do descarte
        $this->add(array(
            'name' => 'dataDescarte',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'dataDescarte',
                'class' => 'form-control datepicker'
            ),
            'options' => array(
                'label' => 'Data do Descarte*:'
            ),
        ));

        //Adiciona o campo que recebe a data da entrega
        $this->add(array(
            'name' => 'dataEntrega',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'dataEntrega',
                'class' => 'form-control datepicker'
            ),
            'options' => array(
                'label' => 'Data de Entrega:'
            ),
        ));

        //Adiciona o campo que recebe os produtos
        $this->add(array(
            'name' => 'produto',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'produto',
                'class' => 'form-control autocomplete-select'
            ),
            'options' => array(
                'label' => 'Item*:',
                'value_options' => $produtos,
                'disable_inarray_validator' => true
            ),
        ));

        //Campo que recebe a quantidade disponível do produto selecionado
        $this->add(array(
            'name' => 'quantidadeDisponivel',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'quantidadeDisponivel',
                'class' => 'form-control number-meddium',
                'disabled' => true
            ),
            'options' => array(
                'label' => 'Quantidade Disponível:'
            ),
        ));

        //Campo que recebe a quantidade
        $this->add(array(
            'name' => 'quantidade',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'quantidade',
                'class' => 'form-control number-meddium'
            ),
            'options' => array(
                'label' => 'Quantidade*:'
            ),
        ));

        //Campo que recebe o valor total resultado da multiplicação da quantidade com o preço médio
        $this->add(array(
            'name' => 'valorTotal',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'valorTotal',
                'class' => 'form-control money',
                'disabled' => true
            ),
            'options' => array(
                'label' => 'Valor Total:'
            ),
        ));

        //Adiciona o campo hidden que leva os ids dos itens excluídos
        $this->add(array(
            'name' => 'idItensExcluidos',
            'type' => 'Zend\Form\Element\Hidden',
            'attributes' => array(
                'id' => 'idItensExcluidos'
            )
        ));

        //Adiciona o campo hidden que leva o id
        $this->add(array(
            'name' => 'id',
            'type' => 'Zend\Form\Element\Hidden',
            'attributes' => array(
                'id' => 'id'
            )
        ));

        //Adiciona o campo hidden que leva o id
        $this->add(array(
            'name' => 'hiddenValorTotalProdutos',
            'type' => 'Zend\Form\Element\Hidden',
            'attributes' => array(
                'id' => 'hiddenValorTotalProdutos',
                'value' => 0.00
            )
        ));
    }

    /**
     * Realiza as validações padrões mais as validações referentes a itens de produto
     * 
     * @param array $itensQuantidade
     * @param array $itensPrecoUnitario
     * @param type $valorEmpenho
     * @return boolean
     */
    public function valid(array $itensProdutos, array $itensQuantidade, array $valorTotal, array $precoMedio, array $itensTransferencia) {
        $this->getInputFilter()->remove('produto');

        //Faz as validações principais
        $valid = parent::isValid();

        //Verifica se ao menos um produto foi selecionado para transferência
        if ($itensProdutos) {

            //Verifica se a quantidade informada no hidden de cada produto é de fato igual a quantidade disponível
            foreach ($itensQuantidade as $key => $item) {

                //Busca a quantidade disponível em estoque para o produto e o seu preço médio
                $estoqueDisponivel = $this->estoqueProdutoRepository->getQuantidadeDisponivelEstoque($itensProdutos[$key], $this->dadosUser['filtrosSelecionados']['estoque']['id']);
                $qtdUtilizada = 0;

                if ($itensTransferencia[$key]) {
                    $itemDescarte = $this->itemDescarteRepository->find($itensTransferencia[$key]);
                    $qtdUtilizada = $itemDescarte ? $itemDescarte->getQtd() : 0;
                }

                $produto = $this->produtoRepository->find($itensProdutos[$key]);

                //Valida se a quantidade informada é menor que a disponível em estoque
                if (($estoqueDisponivel + $qtdUtilizada) < $itensQuantidade[$key]) {
                    $valid = false;
                    $this->get('produto')->setMessages(array('A quantidade escolhida para os produtos deve ser menor ou igual a quantidade disponível.'));
                }

                //Valida se o preço médio do produto multiplicado pela quantidade é de fato o valor total informado nos campos hidden
                if (($produto->getPrecoMedioAtual() * $itensQuantidade[$key]) != $valorTotal[$key]) {
                    $valid = false;
                    $this->get('produto')->setMessages(array('O preço médio multiplicado pelo total informado é diferente do valor total apresentado.'));
                }

                //Valida se o preço médio informado no hidden é de fato igual ao preço médio do produto
                if ($produto->getPrecoMedioAtual() != $precoMedio[$key]) {
                    $valid = false;
                    $this->get('produto')->setMessages(array('O preço médio apresentado é diferente do preço médio do produto'));
                }
            }
        } else {
            $valid = false;
            $this->get('produto')->setMessages(array('Selecione pelo menos um item para o pedido de transferência'));
        }

        //Retorna o resultado da validação
        return $valid;
    }

    /**
     * Retira opções do select de produtos
     * 
     * @param type $produtos
     */
    public function retirarOpcoesProdutos($produtos) {
        $optionsProdutos = $this->get('produto')->getOption('value_options');

        foreach ($produtos as $id) {
            unset($optionsProdutos[$id]);
        }

        $this->get('produto')->setValueOptions($optionsProdutos);
    }

}
