<?php

namespace Estoque\Form;

use Zend\Form\Form;
use Estoque\InputFilter\Recebimento as RecebimentoFilter;
use Estoque\Repository\ItemPedidoRepository;

/**
 * Classe que abstrai o formulário de Recebimento
 *
 * @autor Evandro Noguêz Melos <evandro.noguez@jointecnolonogia.com>
 */
class Recebimento extends Form {

    /**
     *
     * @var  ItemPedidoRepository
     */
    protected $itemPedidoRepository;

    /**
     * 
     * @param ItemPedidoRepository $itemPedidoRepository
     * @return \Estoque\Form\Recebimento
     */
    public function setItemPedidoRepository(ItemPedidoRepository $itemPedidoRepository) {
        $this->itemPedidoRepository = $itemPedidoRepository;
        return $this;
    }

    /**
     * Monta o formulário de Contrato
     */
    public function __construct(RecebimentoFilter $inputFilter) {
        //Seta o nome do formulário
        parent::__construct('recebimento');

        $this->setLabel('Recebimento');
        $this->setAttribute('enctype', 'multipart/form-data');
        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'horizontal-form'));
        $this->setInputFilter($inputFilter);

        //Adiciona o campo que recebe a data de recebimento
        $this->add(array(
            'name' => 'dataRecebimento',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'dataRecebimento',
                'class' => 'form-control datepicker',
                 'value'=> date('d/m/Y')
            ),
            'options' => array(
                'label' => 'Data de Recebimento*:'
            ),
        ));

        //Adiciona o campo que recebe a data de recebimento
        $this->add(array(
            'name' => 'responsavelEntrega',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'responsavelEntrega',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Responsável da Entrega:'
            ),
        ));

        //Adiciona o campo que recebe o cpf/rg
        $this->add(array(
            'name' => 'cpfRg',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'cpfRg',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'CPF/RG:'
            ),
        ));

        //Adiciona o campo que recebe o número da nota
        $this->add(array(
            'name' => 'numeroNota',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'numeroNota',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Nº da Nota*:'
            ),
        ));

        //Monta o campo de upload da nota
        $this->add(array(
            'name' => 'notaFiscal',
            'type' => 'Zend\Form\Element\File',
            'options' => array(
                'label' => 'Nota Fiscal:',
                'label_attributes' => array(
                    'class' => 'control-label',
                )
            ),
        ));

        //Monta o campo hidden de upload a nota fiscal
        $this->add(array(
            'name' => 'hdnNotaFiscal',
            'type' => 'Zend\Form\Element\Hidden',
            'attributes' => array(
                'id' => 'hdnNotaFiscal'
            )
                )
        );

        //Adiciona o campo que recebe as observações
        $this->add(array(
            'name' => 'observacao',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'id' => 'observacao',
                'class' => 'form-control',
                'rows' => "5",
                'style' => "overflow:auto; resize:none;"
            ),
            'options' => array(
                'label' => 'Observação:',
                'label_attributes' => array(
                    'class' => 'control-label',
                )
            ),
        ));

        //Monta o campo hidden que receberá erros genéricos de validação do formulário de recebiementos que não se encaixarem em nenhum dos campos do form
        $this->add(array(
            'name' => 'hdnErrosRecebimento',
            'type' => 'Zend\Form\Element\Hidden'
                )
        );

        //Monta o campo de upload da nota
        $this->add(array(
            'name' => 'id',
            'type' => 'Zend\Form\Element\Hidden'
        ));

        //Monta o submit
        $this->add(array(
            'name' => 'enviar',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => 'ENVIAR',
                'class' => 'btn btn-primary submit-input-loading',
                'id' => 'enviar'
            )
        ));
    }

    public function valid($qtdRecebida = array(), $itemRecebido = array()) {

        //Valida se a nota fiscal já foi informada em outro submit
        if (!empty($this->get('hdnNotaFiscal')->getValue()) && file_exists("./tmp/".$this->get('hdnNotaFiscal')->getValue())) {
            $this->getInputFilter()->remove('notaFiscal');
        }

        $valid = parent::isValid();

        $qtdTotalRecebida = 0;
        $boolObservacaoObrigatorio = false;

        foreach ($qtdRecebida as $key => $qtd) {
            if ($qtd == 0 && !$boolObservacaoObrigatorio){
                $boolObservacaoObrigatorio = true;
            }
            $qtdTotalRecebida += $qtd;
        }

        if ($qtdTotalRecebida == 0) {
            $this->get('hdnErrosRecebimento')->setMessages(array('Informe o recebimento de pelo menos um item'));
            $valid = false;
        }

        if( $boolObservacaoObrigatorio ){
            if ( $this->get('observacao')->getValue() == '' ){
                $this->get('observacao')->setLabel('Observação*:');
                $this->get('observacao')->setMessages(array('Campo Observação Obrigarório devido a ausência de recebimento de todos os itens'));
                $valid = false;
            }
        }

        foreach ($itemRecebido as $key => $item) {
            $itemPedido = $this->itemPedidoRepository->find($item);

            if ($qtdRecebida[$key] > ($itemPedido->getQtd() - $itemPedido->getQuantidadeRecebida())) {
                $this->get('hdnErrosRecebimento')->setMessages(array('A quantidade recebida de um item não pode ser maior que a diferença do total com a quantidade já recebida.'));

                $valid = false;
            }
        }

        return $valid;
    }

}
