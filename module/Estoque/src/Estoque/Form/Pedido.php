<?php

namespace Estoque\Form;

use Zend\Form\Form;
use Estoque\InputFilter\Pedido as PedidoFilter;

/**
 * Classe que abstrai o formulário de fornecedor
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Pedido extends Form {

    /**
     * Monta o formulário de fornecedor
     */
    public function __construct(PedidoFilter $inputFilter, $fornecedores = array(), $contratos = array(), array $empenhos = array(), $produtos = array()) {
        //Seta o nome do formulário
        parent::__construct('pedido');

        $this->setLabel('Pedido');
        $this->setAttribute('icon', 'icon-user');
        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'horizontal-form'));

        $this->setInputFilter($inputFilter);


        //Adiciona o campo que recebe o número do pedido
        $this->add(array(
            'name' => 'numeroPedido',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'numeroPedido',
                'class' => 'form-control',
                'disabled' => true
            ),
            'options' => array(
                'label' => 'Nº do Pedido:'
            ),
        ));

        //Adiciona o campo que recebe o OFN
        $this->add(array(
            'name' => 'ofn',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'ofn',
                'class' => 'form-control',
            ),
            'options' => array(
                'label' => 'Oº Fº Nº:'
            ),
        ));

        //Adiciona o select de fornecedor
        $this->add(array(
            'name' => 'fornecedor',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'fornecedor',
                'class' => 'form-control autocomplete-select2'
            ),
            'options' => array(
                'label' => 'Fornecedor*:',
                'value_options' => $fornecedores,
                'disable_inarray_validator' => true
            ),
        ));

        //Adiciona o select de contato
        $this->add(array(
            'name' => 'contrato',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'contrato',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Contrato*:',
                'value_options' => $contratos,
                'disable_inarray_validator' => true
            ),
        ));

        //Adiciona o select de empenhos
        $this->add(array(
            'name' => 'empenho',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'empenho',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Empenho*:',
                'value_options' => $empenhos,
                'disable_inarray_validator' => true
            ),
        ));

        $this->add(array(
            'name' => 'dataSolicitacao',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'dataSolicitacao',
                'class' => 'form-control datepicker',
                'value'=> date('d/m/Y')
            ),
            'options' => array(
                'label' => 'Data de Solicitação:',
                
            ),
        ));

        $this->add(array(
            'name' => 'dataEntrega',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'dataEntrega',
                'class' => 'form-control datepicker'
            ),
            'options' => array(
                'label' => 'Data de Entrega:'
            ),
        ));

        $this->add(array(
            'name' => 'produto',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'produto',
                'class' => 'form-control autocomplete-select'
            ),
            'options' => array(
                'label' => 'Item*:',
                'value_options' => $produtos,
                'disable_inarray_validator' => true
            ),
        ));

        $this->add(array(
            'name' => 'precoUnitario',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'precoUnitario',
                'class' => 'form-control money'
            ),
            'options' => array(
                'label' => 'Preço Unit. *:'
            ),
        ));

        $this->add(array(
            'name' => 'quantidade',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'quantidade',
                'class' => 'form-control number-meddium'
            ),
            'options' => array(
                'label' => 'Quantidade. *:'
            ),
        ));

        //Adiciona o campo hidden que leva os ids dos itens excluídos
        $this->add(array(
            'name' => 'idItensExcluidos',
            'type' => 'Zend\Form\Element\Hidden',
            'attributes' => array(
                'id' => 'idItensExcluidos'
            )
        ));

        //Adiciona o campo hidden que leva o valor do empenho disponível
        $this->add(array(
            'name' => 'valor-empenho-disponivel',
            'type' => 'Zend\Form\Element\Hidden',
            'attributes' => array(
                'id' => 'valor-empenho-disponivel',
                'value' => 0.0
            )
        ));

        //Adiciona o campo hidden que leva o id
        $this->add(array(
            'name' => 'id',
            'type' => 'Zend\Form\Element\Hidden'
        ));
    }

    /**
     * Realiza as validações padrões mais as validações referentes a itens de produto
     * 
     * @param array $itensQuantidade
     * @param array $itensPrecoUnitario
     * @param type $valorEmpenho
     * @return boolean
     */
    public function valid(array $itensQuantidade, array $itensPrecoUnitario, $valorEmpenho) {
        $this->getInputFilter()->remove('produto');

        //Faz as validações principais
        $valid = parent::isValid();

        //Verifica se ao menos um produto foi selecionado
        if ($itensQuantidade) {

            //Refaz o calculo do valor disponivel para empenho
            foreach ($itensQuantidade as $key => $item) {

                //Calcula o valor e subtrai do valor disponivel para empenho
                $valor = $itensPrecoUnitario[$key] * $itensQuantidade[$key];

                $valorEmpenho = $valorEmpenho - $valor;
            }


            if ($valorEmpenho < 0) {
                $valid = false;
                $this->get('produto')->setMessages(array('A soma dos valores solicitados é maior do que o empenho disponível'));
            }
        } else {
            $valid = false;
            $this->get('produto')->setMessages(array('Selecione pelo menos um item para o pedido'));
        }

        //Retorna o resultado da validação
        return $valid;
    }

    /**
     * Retira opções do select de produtos
     * 
     * @param type $produtos
     */
    public function retirarOpcoesProdutos($produtos) {
        $optionsProdutos = $this->get('produto')->getOption('value_options');

        foreach ($produtos as $id) {
            unset($optionsProdutos[$id]);
        }

        $this->get('produto')->setValueOptions($optionsProdutos);
    }

}
