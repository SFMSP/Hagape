<?php

namespace Estoque\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Contrato
 *
 * @ORM\Table(name="tb_contrato", indexes={@ORM\Index(name="fk_id_fonecedor_idx", columns={"id_fornecedor"})})
 * @ORM\Entity(repositoryClass="Estoque\Repository\ContratoRepository")
 **/
class Contrato extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_contrato", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_num_contrato", type="string", length=45, nullable=false)
     */
    private $numContrato;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_num_processo_licitatorio", type="string", length=45, nullable=false)
     */
    private $numProcessoLicitatorio;

    /**
     * @var float
     *
     * @ORM\Column(name="valor", type="float", precision=10, scale=0, nullable=false)
     */
    private $valor;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_vencimento", type="datetime", nullable=false)
     */
    private $dataVencimento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=false)
     */
    private $dataCadastro;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_ativo", type="boolean", nullable=false)
     */
    private $ativo;

    /**
     * @var \Estoque\Entity\Fornecedor
     *
     * @ORM\ManyToOne(targetEntity="Estoque\Entity\Fornecedor")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_fornecedor", referencedColumnName="id_fornecedor")
     * })
     */
    private $fornecedor;

    /**
     * @ORM\OneToMany(targetEntity="Estoque\Entity\Empenho", mappedBy="contrato")
     */
    private $empenhos;

    /**
     * @return mixed
     */
    public function getEmpenhos()
    {
        return $this->empenhos;
    }

    /**
     * @param mixed $empenhos
     */
    public function setEmpenhos($empenhos = null)
    {
        $this->empenhos = $empenhos;
    }

    public function __construct()
    {
        $this->dataCadastro = new \DateTime('now');
    }

    /**
     * Get id_contrato
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set num_contrato
     *
     * @param string $numContrato
     * @return Contrato
     */
    public
    function setNumContrato($numContrato)
    {
        $this->numContrato = $numContrato;

        return $this;
    }

    /**
     * Get num_contrato
     *
     * @return string
     */
    public
    function getNumContrato()
    {
        return $this->numContrato;
    }

    /**
     * Set num_processo_licitatorio
     *
     * @param string $numProcessoLicitatorio
     * @return Contrato
     */
    public
    function setNumProcessoLicitatorio($numProcessoLicitatorio)
    {
        $this->numProcessoLicitatorio = $numProcessoLicitatorio;

        return $this;
    }

    /**
     * Get num_processo_licitatorio
     *
     * @return string
     */
    public
    function getNumProcessoLicitatorio()
    {
        return $this->numProcessoLicitatorio;
    }

    /**
     * Set valor
     *
     * @param float $valor
     * @return Contrato
     */
    public
    function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float
     */
    public
    function getValor()
    {
        return $this->valor;
    }

    /**
     * Set data_vencimento
     *
     * @param \DateTime $dataVencimento
     * @return Contrato
     */
    public
    function setDataVencimento($dataVencimento)
    {
        $this->dataVencimento = $dataVencimento;

        return $this;
    }

    /**
     * Get data_vencimento
     *
     * @return \DateTime
     */
    public
    function getDataVencimento()
    {
        return $this->dataVencimento;
    }

    /**
     * Set data_cadastro
     *
     * @param \DateTime $dataCadastro
     * @return Contrato
     */
    public
    function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;

        return $this;
    }

    /**
     * Get data_cadastro
     *
     * @return \DateTime
     */
    public
    function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * Set bool_ativo
     *
     * @param boolean $ativo
     * @return Contrato
     */
    public
    function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get bool_ativo
     *
     * @return boolean
     */
    public
    function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set id_fornecedor
     *
     * @param \Estoque\Entity\Fornecedor $idFornecedor
     * @return Contrato
     */
    public
    function setFornecedor(\Estoque\Entity\Fornecedor $fornecedor = null)
    {
        $this->fornecedor = $fornecedor;

        return $this;
    }

    /**
     * Get id_fornecedor
     *
     * @return \Estoque\Entity\Fornecedor
     */
    public
    function getFornecedor()
    {
        return $this->fornecedor;
    }

    /**
     * @return array
     */
    public
    function toArray()
    {
        return array(
            'id' => $this->id,
            'numContrato' => $this->numContrato,
            'valor' => $this->valor,
            'numProcessoLicitatorio' => $this->numProcessoLicitatorio,
            'dataVencimento' => $this->dataVencimento->format('d/m/Y'),
            'dataCadastro' => $this->dataCadastro,
            'ativo' => $this->ativo,
            'fornecedor' => $this->fornecedor
        );
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->numContrato;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->__toString();
    }
}
