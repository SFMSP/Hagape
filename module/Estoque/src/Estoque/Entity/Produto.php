<?php

namespace Estoque\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Produto
 *
 * @ORM\Table(name="tb_produto", indexes={@ORM\Index(name="IDX_237B9375CE25AE0A", columns={"id_categoria"}), @ORM\Index(name="FK_tb_produto_tb_unidade_medida", columns={"id_medida"})})
 * @ORM\Entity(repositoryClass="Estoque\Repository\ProdutoRepository")
 */
class Produto extends BaseEntity implements EntityInterface
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id_produto", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=false)
     */
    private $dataCadastro;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome", type="string", length=255, nullable=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_codigo", type="string", length=45, nullable=false)
     */
    private $codigo;

    /**
     * @var \Estoque\Entity\UnidadeMedida
     *
     * @ORM\ManyToOne(targetEntity="Estoque\Entity\UnidadeMedida")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_medida", referencedColumnName="id_unidade_medida")
     * })
     */
    private $medida;

    /**
     * @var integer
     *
     * @ORM\Column(name="nu_qtd_inicial", type="integer", nullable=false)
     */
    private $qtdInicial;

    /**
     * @var integer
     *
     * @ORM\Column(name="nu_qtd_atual", type="integer", nullable=false)
     */
    private $qtdAtual;

    /**
     * @var integer
     *
     * @ORM\Column(name="nu_qtd_minima", type="integer", nullable=false)
     */
    private $qtdMinima;

    /**
     * @var string
     *
     * @ORM\Column(name="nu_preco_inicial", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $precoInicial;

    /**
     * @var string
     *
     * @ORM\Column(name="nu_tamanho", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $tamanho;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_observacao", type="text", nullable=false)
     */
    private $observacao;

    /**
     * @var string
     *
     * @ORM\Column(name="nu_preco_medio_atual", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $precoMedioAtual;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_ativo", type="boolean", nullable=false)
     */
    private $ativo;

    /**
     * @var CategoriaProduto
     *
     * @ORM\ManyToOne(targetEntity="CategoriaProduto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_categoria", referencedColumnName="id_categoria")
     * })
     */
    private $categoria;

    /**
     * @var integer
     *
     * @ORM\Column(name="nu_qtd_reservada", type="integer", nullable=true)
     */
    private $qtdReservada;

    /**
     * @return int
     */
    public function getQtdReservada()
    {
        return $this->qtdReservada;
    }

    /**
     * @param int $qtdReservada
     */
    public function setQtdReservada($qtdReservada)
    {
        $this->qtdReservada = $qtdReservada;
    }

    public function __construct()
    {
        $this->dataCadastro = new \DateTime('now');
        $this->ativo = true;
    }

    function getId()
    {
        return $this->id;
    }

    function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    function getNome()
    {
        return $this->nome;
    }

    function getCodigo()
    {
        return $this->codigo;
    }

    function getMedida()
    {
        return $this->medida;
    }

    function getQtdInicial()
    {
        return $this->qtdInicial;
    }

    function getQtdAtual()
    {
        return $this->qtdAtual;
    }

    function getQtdMinima()
    {
        return $this->qtdMinima;
    }

    function getPrecoInicial()
    {
        return $this->precoInicial;
    }

    function getTamanho()
    {
        return $this->tamanho;
    }

    function getObservacao()
    {
        return $this->observacao;
    }

    function getPrecoMedioAtual()
    {
        return $this->precoMedioAtual;
    }

    function getAtivo()
    {
        return $this->ativo;
    }

    function getCategoria()
    {
        return $this->categoria;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setDataCadastro(\DateTime $dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
    }

    function setNome($nome)
    {
        $this->nome = $nome;
    }

    function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    function setMedida(\Estoque\Entity\UnidadeMedida $medida)
    {
        $this->medida = $medida;
    }

    function setQtdInicial($qtdInicial)
    {
        $this->qtdInicial = $qtdInicial;
    }

    function setQtdAtual($qtdAtual)
    {
        $this->qtdAtual = $qtdAtual;
    }

    function setQtdMinima($qtdMinima)
    {
        $this->qtdMinima = $qtdMinima;
    }

    function setPrecoInicial($precoInicial)
    {
        $this->precoInicial = $precoInicial;
    }

    function setTamanho($tamanho)
    {
        $this->tamanho = $tamanho;
    }

    function setObservacao($observacao)
    {
        $this->observacao = $observacao;
    }

    function setPrecoMedioAtual($precoMedioAtual)
    {
        $this->precoMedioAtual = $precoMedioAtual;
    }

    function setAtivo($ativo)
    {
        $this->ativo = $ativo;
    }

    function setCategoria(CategoriaProduto $categoria)
    {
        $this->categoria = $categoria;
    }

    public function __toString()
    {
        return $this->nome;
    }

    public function getLabel()
    {
        return $this->codigo . " - " . $this->__toString();
    }

    public function toArray()
    {

        return array(
            'id' => $this->id,
            'dataCadastro' => $this->dataCadastro,
            'nome' => $this->nome,
            'codigo' => $this->codigo,
            'medida' => $this->medida,
            'qtdInicial' => $this->qtdInicial,
            'qtdAtual' => $this->qtdAtual,
            'qtdMinima' => $this->qtdMinima,
            'precoInicial' => $this->precoInicial,
            'tamanho' => $this->tamanho,
            'observacao' => $this->observacao,
            'precoMedioAtual' => $this->precoMedioAtual,
            'ativo' => $this->ativo,
            'categoria' => $this->categoria,
            'qtdReservada' => $this->qtdReservada
        );
    }

}
