<?php

namespace Estoque\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * CategoriaProduto
 *
 * @ORM\Table(name="tb_categoria_produto")
 * @ORM\Entity(repositoryClass="Estoque\Repository\CategoriaProdutoRepository")
 */
class CategoriaProduto extends BaseEntity implements EntityInterface
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id_categoria", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=false)
     */
    private $dataCadastro;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome", type="string", length=255, nullable=false)
     */
    private $nome;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_categoria_pai", type="integer", length=11, nullable=false)
     */
    private $categoriaPai;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_observacao", type="text", nullable=false)
     */
    private $observacao;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_ativo", type="boolean", nullable=false)
     */
    private $ativo;
    /**
     * @var integer
     *
     * @ORM\Column(name="situacao", type="integer", nullable=false)
     */
    private $situacao;

    function getSituacao()
    {
        return $this->situacao;
    }

    function setSituacao($situacao)
    {
        $this->situacao = $situacao;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="txt_tipo", type="integer", nullable=false)
     */
    private $Tipo;
    /**
     * @var \Admin\Entity\TipoNegocio
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\TipoNegocio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_negocio", referencedColumnName="id_tipo_negocio")
     * })
     */
    private $tipo_negocio;

    function getTipo_negocio()
    {
        return $this->tipo_negocio;
    }

    function setTipo_negocio(\Admin\Entity\TipoNegocio $tipo_negocio)
    {
        $this->tipo_negocio = $tipo_negocio;
    }

    public function __construct()
    {
        $this->dataCadastro = new \DateTime('now');
        $this->ativo = true;
    }

    function getId()
    {
        return $this->id;
    }

    function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    function getNome()
    {
        return $this->nome;
    }

    /**
     * @return int
     */
    public function getCategoriaPai()
    {
        return $this->categoriaPai;
    }

    function getObservacao()
    {
        return $this->observacao;
    }

    function getStatus()
    {
        return $this->ativo;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setDataCadastro(\DateTime $dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
    }

    function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @param int $categoriaPai
     */
    public function setCategoriaPai($categoriaPai)
    {
        $this->categoriaPai = $categoriaPai;
    }

    function setObservacao($observacao)
    {
        $this->observacao = $observacao;
    }

    function setStatus($ativo)
    {
        $this->ativo = $ativo;
    }

    public function __toString()
    {
        return $this->nome;
    }

    public function getLabel()
    {
        return $this->__toString();
    }

    function getAtivo()
    {
        return $this->ativo;
    }

    function getTipo()
    {
        return $this->Tipo;
    }

    function setAtivo($ativo)
    {
        $this->ativo = $ativo;
    }

    function setTipo($Tipo)
    {
        $this->Tipo = $Tipo;
    }

    public function toArray()
    {
        return array(
            'id' => $this->id,
            'dataCadastro' => $this->dataCadastro,
            'nome' => $this->nome,
            'pai' => $this->categoriaPai,
            'observacao' => $this->observacao,
            'ativo' => $this->ativo,
            'tipo' => $this->Tipo,
            "situacao" => $this->situacao,
            "tipoNegocio" => $this->tipo_negocio
        );
    }

}
