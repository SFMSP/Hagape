<?php

namespace Estoque\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * ItemDescarte
 *
 * @ORM\Table(name="tb_item_descarte", indexes={@ORM\Index(name="FK1_tb_item_descarte_tb_solicitacao_descarte", columns={"id_solicitacao_descarte"}), @ORM\Index(name="FK2_tb_item_descarte_tb_produto", columns={"id_produto"})})
 * @ORM\Entity(repositoryClass="Estoque\Repository\ItemDescarteRepository")
 */
class ItemDescarte extends BaseEntity implements EntityInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_item_descarte", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="nu_quantidade", type="integer", nullable=true)
     */
    private $qtd;

    /**
     * @var string
     *
     * @ORM\Column(name="nu_custo_medio", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $custoMedio;

    /**
     * @var string
     *
     * @ORM\Column(name="nu_valor_total", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $valorTotal;

    /**
     * @var SolicitacaoDescarte
     *
     * @ORM\ManyToOne(targetEntity="SolicitacaoDescarte")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_solicitacao_descarte", referencedColumnName="id_solicitacao_descarte")
     * })
     */
    private $solicitacaoDescarte;

    /**
     * @var Produto
     *
     * @ORM\ManyToOne(targetEntity="Produto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_produto", referencedColumnName="id_produto")
     * })
     */
    private $produto;

    /**
     * @var ItemRecebidoDescarte
     *
     * @ORM\OneToOne(targetEntity="ItemRecebidoDescarte", mappedBy="itemDescarte")
     */
    private $itemRecebido;

    /**
     * @var EstornoDescarte
     *
     * @ORM\OneToOne(targetEntity="EstornoDescarte", mappedBy="itemDescarte")
     */
    private $estornoDescarte;

    public function getId() {
        return $this->id;
    }

    public function getQtd() {
        return $this->qtd;
    }

    public function getCustoMedio() {
        return $this->custoMedio;
    }

    public function getValorTotal() {
        return $this->valorTotal;
    }

    public function getSolicitacaoDescarte() {
        return $this->solicitacaoDescarte;
    }

    public function getProduto() {
        return $this->produto;
    }

    public function getItemRecebido() {
        return $this->itemRecebido;
    }

    public function getEstornoDescarte() {
        return $this->estornoDescarte;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setQtd($qtd) {
        $this->qtd = $qtd;
    }

    public function setCustoMedio($custoMedio) {
        $this->custoMedio = $custoMedio;
    }

    public function setValorTotal($valorTotal) {
        $this->valorTotal = $valorTotal;
    }

    public function setSolicitacaoDescarte(SolicitacaoDescarte $solicitacaoDescarte) {
        $this->solicitacaoDescarte = $solicitacaoDescarte;
    }

    public function setProduto(Produto $produto) {
        $this->produto = $produto;
    }

    public function setItemRecebido(ItemRecebidoTransferencia $itemRecebido) {
        $this->itemRecebido = $itemRecebido;
        return $this;
    }

    public function setEstornoDescarte(EstornoDescarte $estornoDescarte) {
        $this->estornoDescarte = $estornoDescarte;
        return $this;
    }

    public function __toString() {
        return $this->produto->getNome();
    }

    public function getLabel() {
        return $this->__toString();
    }

    public function toArray() {
        return array(
            "id" => $this->id,
            "qtd" => $this->qtd,
            "custoMedio" => $this->custoMedio,
            "valorTotal" => $this->valorTotal,
            "solicitacaoDescarte" => $this->solicitacaoDescarte,
            "produto" => $this->produto,
            "itemRecebido" => $this->itemRecebido,
            "estornoDescarte" => $this->estornoDescarte
        );
    }

}
