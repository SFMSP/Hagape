<?php

namespace Estoque\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * SituacaoDescarte
 *
 * @ORM\Table(name="tb_situacao_descarte")
 * @ORM\Entity(repositoryClass="Estoque\Repository\SituacaoDescarteRepository")
 */
class SituacaoDescarte extends BaseEntity implements EntityInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_situacao_descarte", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome", type="string", length=255, nullable=false)
     */
    private $nome;

    public function getId() {
        return $this->id;
    }

    public function getNome() {
        return $this->nome;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function __toString() {
        return $this->nome;
    }

    public function getLabel() {
        return $this->__toString();
    }

    public function toArray() {
        return array(
            "id" => $this->id,
            "nome" => $this->nome
        );
    }

}
