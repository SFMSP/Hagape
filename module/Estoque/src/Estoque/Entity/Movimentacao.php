<?php

namespace Estoque\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Movimentacao
 *
 * @ORM\Table(name="tb_movimentacao", indexes={@ORM\Index(name="FK_movimentacao_produto", columns={"id_produto"}), @ORM\Index(name="FK_movimentacao_tipo_movimentacao", columns={"id_tipo_movimentacao"}), @ORM\Index(name="FK_acao_movimentacao", columns={"id_acao_movimentacao"}), @ORM\Index(name="FK_movimentacao_usuario", columns={"id_usuario"}), @ORM\Index(name="FK_movimentacao_estoque", columns={"id_estoque"}), @ORM\Index(name="FK_movimentacao_recebimento", columns={"id_recebimento"}), @ORM\Index(name="FK_movimentacao_descarte", columns={"id_solicitacao_descarte"}) })
 * @ORM\Entity(repositoryClass="Estoque\Repository\MovimentacaoRepository")
 */
class Movimentacao extends BaseEntity implements EntityInterface {
    /*
     * Ações de Movimentação Presentes na Entidade AcaoMovimentacao
     */

    const ACAO_MOVIMENTACAO_COMPRA = 1;
    const ACAO_MOVIMENTACAO_TRANSFERENCIA = 2;
    const ACAO_MOVIMENTACAO_ESTORNO = 3;
    const ACAO_MOVIMENTACAO_DESCARTE = 4;

    /**
     * Tipos de Movimentação Presentes na Entidade TipoMovimentacao
     */
    const TIPO_MOVIMENTACAO_ENTRADA = 1;
    const TIPO_MOVIMENTACAO_SAIDA = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_movimentacao", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="nu_qtd", type="integer", nullable=false)
     */
    private $qtd;

    /**
     * @var string
     *
     * @ORM\Column(name="nu_valor_total", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valorTotal;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_movimentacao", type="datetime", nullable=false)
     */
    private $dataMovimentacao;

    /**
     * @var PedidoTransferencia
     *
     * @ORM\ManyToOne(targetEntity="PedidoTransferencia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pedido_transferencia", referencedColumnName="id_pedido_transferencia")
     * })
     */
    private $pedidoTransferencia;

    /**
     * @var RecebimentoTransferencia
     *
     * @ORM\ManyToOne(targetEntity="RecebimentoTransferencia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_recebimento_transferencia", referencedColumnName="id_recebimento_transferencia")
     * })
     */
    private $recebimentoTransferencia;

    /**
     * @var AcaoMovimentacao
     *
     * @ORM\ManyToOne(targetEntity="AcaoMovimentacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_acao_movimentacao", referencedColumnName="id_acao_movimentacao")
     * })
     */
    private $acaoMovimentacao;

    /**
     * @var \Admin\Entity\Estoque
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Estoque")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estoque", referencedColumnName="id_estoque")
     * })
     */
    private $estoque;

    /**
     * @var Produto
     *
     * @ORM\ManyToOne(targetEntity="Produto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_produto", referencedColumnName="id_produto")
     * })
     */
    private $produto;

    /**
     * @var Recebimento
     *
     * @ORM\ManyToOne(targetEntity="Recebimento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_recebimento", referencedColumnName="id_recebimento")
     * })
     */
    private $recebimento;

    /**
     * @var SolicitacaoDescarte
     *
     * @ORM\ManyToOne(targetEntity="SolicitacaoDescarte")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_solicitacao_descarte", referencedColumnName="id_solicitacao_descarte")
     * })
     */
    private $descarte;

    /**
     * @var TipoMovimentacao
     *
     * @ORM\ManyToOne(targetEntity="TipoMovimentacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipo_movimentacao", referencedColumnName="id_tipo_movimentacao")
     * })
     */
    private $tipoMovimentacao;

    /**
     * @var \Application\Entity\Usuario
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     * })
     */
    private $usuario;

    public function getId() {
        return $this->id;
    }

    public function getQtd() {
        return $this->qtd;
    }

    public function getValorTotal() {
        return $this->valorTotal;
    }

    public function getDataMovimentacao() {
        return $this->dataMovimentacao;
    }

    public function getPedidoTransferencia() {
        return $this->pedidoTransferencia;
    }

    public function getRecebimentoTransferencia() {
        return $this->recebimentoTransferencia;
    }

    public function getAcaoMovimentacao() {
        return $this->acaoMovimentacao;
    }

    public function getEstoque() {
        return $this->estoque;
    }

    public function getProduto() {
        return $this->produto;
    }

    public function getRecebimento() {
        return $this->recebimento;
    }

    public function getDescarte() {
        return $this->descarte;
    }

    public function getTipoMovimentacao() {
        return $this->tipoMovimentacao;
    }

    public function getUsuario() {
        return $this->usuario;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setQtd($qtd) {
        $this->qtd = $qtd;
    }

    public function setValorTotal($valorTotal) {
        $this->valorTotal = $valorTotal;
    }

    public function setDataMovimentacao(\DateTime $dataMovimentacao) {
        $this->dataMovimentacao = $dataMovimentacao;
    }

    public function setPedidoTransferencia(PedidoTransferencia $pedidoTransferencia) {
        $this->pedidoTransferencia = $pedidoTransferencia;
    }

    public function setRecebimentoTransferencia(RecebimentoTransferencia $recebimentoTransferencia = null) {
        $this->recebimentoTransferencia = $recebimentoTransferencia;
    }

    public function setAcaoMovimentacao(AcaoMovimentacao $acaoMovimentacao) {
        $this->acaoMovimentacao = $acaoMovimentacao;
    }

    public function setEstoque(\Admin\Entity\Estoque $estoque) {
        $this->estoque = $estoque;
    }

    public function setProduto(Produto $produto) {
        $this->produto = $produto;
    }

    public function setRecebimento(Recebimento $recebimento = null) {
        $this->recebimento = $recebimento;
    }

    public function setDescarte(SolicitacaoDescarte $descarte) {
        $this->descarte = $descarte;
    }

    public function setTipoMovimentacao(TipoMovimentacao $tipoMovimentacao) {
        $this->tipoMovimentacao = $tipoMovimentacao;
    }

    public function setUsuario(\Application\Entity\Usuario $usuario) {
        $this->usuario = $usuario;
    }

    public function __toString() {
        return $this->acaoMovimentacao->getNome();
    }

    public function getLabel() {
        return $this->__toString();
    }

    public function toArray() {
        return array(
            'id' => $this->id,
            'qtd' => $this->qtd,
            'valorTotal' => $this->valorTotal,
            'dataMovimentacao' => $this->dataMovimentacao,
            'pedidoTransferencia' => $this->pedidoTransferencia,
            'recebimentoTransferencia' => $this->recebimentoTransferencia,
            'acaoMovimentacao' => $this->acaoMovimentacao,
            'estoque' => $this->estoque,
            'produto' => $this->produto,
            'descarte' => $this->descarte,
            'recebimento' => $this->recebimento,
            'tipoMovimentacao' => $this->tipoMovimentacao,
            'usuario' => $this->usuario
        );
    }

}
