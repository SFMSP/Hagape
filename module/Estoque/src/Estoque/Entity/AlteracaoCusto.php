<?php

namespace Estoque\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;
/**
 * AlteracaoCusto
 *
 * @ORM\Table(name="tb_alteracao_custo", indexes={@ORM\Index(name="fk__idx", columns={"id_produto"}), @ORM\Index(name="fk_fechamento_alteracao_custo_idx", columns={"id_fechamento"})})
 * @ORM\Entity
 */
class AlteracaoCusto extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_alteracao_custo", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nu_custo_medio_anterior", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $numCustoMedioAnterior;

    /**
     * @var string
     *
     * @ORM\Column(name="nu_custo_medio_atual", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $numCustoMedioAtual;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_observacao", type="string", length=250, nullable=false)
     */
    private $observacao;

    /**
     * @var \Estoque\Entity\Produto
     *
     * @ORM\ManyToOne(targetEntity="Estoque\Entity\Produto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_produto", referencedColumnName="id_produto")
     * })
     */
    private $produto;

    /**
     * @var \Estoque\Entity\FechamentoPeriodo
     *
     * @ORM\ManyToOne(targetEntity="Estoque\Entity\FechamentoPeriodo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_fechamento", referencedColumnName="id_fechamento_periodo")
     * })
     */
    private $fechamento;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNumCustoMedioAnterior()
    {
        return $this->numCustoMedioAnterior;
    }

    /**
     * @param string $numCustoMedioAnterior
     */
    public function setNumCustoMedioAnterior($numCustoMedioAnterior)
    {
        $this->numCustoMedioAnterior = $numCustoMedioAnterior;
    }

    /**
     * @return string
     */
    public function getNumCustoMedioAtual()
    {
        return $this->numCustoMedioAtual;
    }

    /**
     * @param string $numCustoMedioAtual
     */
    public function setNumCustoMedioAtual($numCustoMedioAtual)
    {
        $this->numCustoMedioAtual = $numCustoMedioAtual;
    }

    /**
     * @return string
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * @param string $observacao
     */
    public function setObservacao($observacao)
    {
        $this->observacao = $observacao;
    }

    /**
     * @return Produto
     */
    public function getProduto()
    {
        return $this->produto;
    }

    /**
     * @param Produto $produto
     */
    public function setProduto($produto)
    {
        $this->produto = $produto;
    }

    /**
     * @return FechamentoPeriodo
     */
    public function getFechamento()
    {
        return $this->fechamento;
    }

    /**
     * @param FechamentoPeriodo $fechamento
     */
    public function setFechamento($fechamento)
    {
        $this->fechamento = $fechamento;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->observacao;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->__toString();
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array(
            "id" => $this->id,
            "numCustoMedioAnterior" => $this->numCustoMedioAnterior,
            "numCustoMedioAtual" => $this->numCustoMedioAtual,
            "observacao" => $this->observacao,
            "produto" => $this->produto,
            "fechamento" => $this->fechamento
        );
    }


}
