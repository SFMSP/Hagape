<?php

namespace Estoque\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;
/**
 * FechamentoPeriodo
 *
 * @ORM\Table(name="tb_fechamento_periodo")
 * @ORM\Entity(repositoryClass="Estoque\Repository\FechamentoPeriodoRepository")
 */
class FechamentoPeriodo extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_fechamento_periodo", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_periodo_inicial", type="datetime", nullable=true)
     */
    private $dataPeriodoInicial;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_periodo_final", type="datetime", nullable=true)
     */
    private $dataPeriodoFinal;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_execucao", type="datetime", nullable=true)
     */
    private $dataExecucao;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getDataPeriodoInicial()
    {
        return $this->dataPeriodoInicial;
    }

    /**
     * @param \DateTime $dataPeriodoInicial
     */
    public function setDataPeriodoInicial($dataPeriodoInicial)
    {
        $this->dataPeriodoInicial = $dataPeriodoInicial;
    }

    /**
     * @return \DateTime
     */
    public function getDataPeriodoFinal()
    {
        return $this->dataPeriodoFinal;
    }

    /**
     * @param \DateTime $dataPeriodoFinal
     */
    public function setDataPeriodoFinal($dataPeriodoFinal)
    {
        $this->dataPeriodoFinal = $dataPeriodoFinal;
    }

    /**
     * @return \DateTime
     */
    public function getDataExecucao()
    {
        return $this->dataExecucao;
    }

    /**
     * @param \DateTime $dataExecucao
     */
    public function setDataExecucao($dataExecucao)
    {
        $this->dataExecucao = $dataExecucao;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return '';
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->__toString();
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array(
            "id" => $this->id,
            "dataPeriodoInicial" => $this->dataPeriodoInicial->format('d/m/Y'),
            "dataPeriodoFinal" => $this->dataPeriodoFinal->format('d/m/Y'),
            "dataExecucao" => $this->dataExecucao->format('d/m/Y')
        );
    }
}
