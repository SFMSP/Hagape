<?php

namespace Estoque\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;
use Admin\Entity\Estoque;

/**
 * SolicitacaoDescarte
 *
 * @ORM\Table(name="tb_solicitacao_descarte", indexes={@ORM\Index(name="FK1_tb_solicitacao_descarte_tb_estoque", columns={"id_origem"}), @ORM\Index(name="FK2_tb_solicitacao_descarte_tb_estoque", columns={"id_destino"}), @ORM\Index(name="FK3_tb_solicitacao_descarte_tb_usuario", columns={"id_usuario"}), @ORM\Index(name="FK4_tb_solicitacao_descarte_tb_situacao_descarte", columns={"id_situacao_descarte"})})
 * @ORM\Entity(repositoryClass="Estoque\Repository\SolicitacaoDescarteRepository")
 */
class SolicitacaoDescarte extends BaseEntity implements EntityInterface {

    /**
     * Lista de status disponíveis para um pedido de transferência
     */
    const STATUS_CADASTRADO = 1;
    const STATUS_SOLICITADO = 2;
    const STATUS_DESCARTADO = 3;
    const STATUS_PENDENTE = 4;
    const STATUS_CANCELADO = 5;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_solicitacao_descarte", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_descarte", type="date", nullable=false)
     */
    private $dataDescarte;


    /**
     * @var string
     *
     * @ORM\Column(name="txt_numero_descarte", type="string", length=50, nullable=false)
     */
    private $numeroDescarte;

    /**
     * @var Admin\Entity\Estoque
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Estoque")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_origem", referencedColumnName="id_estoque")
     * })
     */
    private $estoqueOrigem;

    /**
     * @var Admin\Entity\Estoque
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Estoque")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_destino", referencedColumnName="id_estoque")
     * })
     */
    private $estoqueDestino;

    /**
     * @var \Application\Entity\Usuario
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     * })
     */
    private $usuario;

    /**
     * @var SituacaoDescarte
     *
     * @ORM\ManyToOne(targetEntity="SituacaoDescarte")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_situacao_descarte", referencedColumnName="id_situacao_descarte")
     * })
     */
    private $situacaoDescarte;

    /**
     * @ORM\OneToMany(targetEntity="Estoque\Entity\ItemDescarte", mappedBy="solicitacaoDescarte")
     */
    private $itensDescarte;

    /**
     * @var RecebimentoDescarte
     *
     * @ORM\OneToOne(targetEntity="RecebimentoDescarte", mappedBy="solicitacaoDescarte")
     */
    private $recebimentoDescarte;

    public function getId() {
        return $this->id;
    }

    public function getDataDescarte() {
        return $this->dataDescarte;
    }
    
    public function getNumeroDescarte() {
        return $this->numeroDescarte;
    }

    public function getEstoqueOrigem() {
        return $this->estoqueOrigem;
    }

    public function getEstoqueDestino() {
        return $this->estoqueDestino;
    }

    public function getUsuario() {
        return $this->usuario;
    }

    public function getSituacaoDescarte() {
        return $this->situacaoDescarte;
    }

    public function getItensDescarte() {
        return $this->itensDescarte;
    }

    public function getRecebimentoDescarte() {
        return $this->recebimentoDescarte;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setDataDescarte(\DateTime $dataDescarte) {
        $this->dataDescarte = $dataDescarte;
    }

    public function setNumeroDescarte($numeroDescarte) {
        $this->numeroDescarte = $numeroDescarte;
    }

    public function setEstoqueOrigem(Estoque $estoqueOrigem) {
        $this->estoqueOrigem = $estoqueOrigem;
    }

    public function setEstoqueDestino(Estoque $estoqueDestino) {
        $this->estoqueDestino = $estoqueDestino;
    }

    public function setUsuario(\Application\Entity\Usuario $usuario) {
        $this->usuario = $usuario;
    }

    public function setSituacaoDescarte(SituacaoDescarte $situacaoDescarte) {
        $this->situacaoDescarte = $situacaoDescarte;
    }

    public function setItensDescarte($itensDescarte) {
        $this->itensDescarte = $itensDescarte;
    }

    public function setRecebimentoDescarte(RecebimentoDescarte $recebimentoDescarte) {
        $this->recebimentoDescarte = $recebimentoDescarte;
    }

    public function __toString() {
        return $this->numeroDescarte;
    }

    public function getLabel() {
        return $this->__toString();
    }

    public function toArray() {
        $array = array(
            "id" => $this->id,
            "dataDescarte" => $this->dataDescarte->format('d/m/Y'),
            "numeroDescarte" => $this->numeroDescarte,
            "estoqueOrigem" => $this->estoqueOrigem->getId(),
            "estoqueDestino" => $this->estoqueDestino->getId(),
            "usuario" => $this->usuario,
            "situacaoDescarte" => $this->situacaoDescarte,
            "itensDescarte" => $this->itensDescarte,
            "recebimentoDescarte" => $this->recebimentoDescarte
        );

        return $array;
    }

    /**
     * @return array
     */
    public function getIdProdutosDescarte() {
        $array = array();

        foreach ($this->itensDescarte as $item) {
            $array[] = $item->getProduto()->getId();
        }

        return $array;
    }

    /**
     * @return float
     */
    public function getValorTotalSolicitacaoDescarte() {
        $total = 0;

        foreach ($this->itensDescarte as $item) {
            $total += $item->getValorTotal();
        }

        return $total;
    }

    /**
     * @return int
     */
    public function getTotalRecebido() {
        $totalRecebido = 0;

        foreach ($this->itensDescarte as $item) {
            $totalRecebido += $item->getItemRecebido() ? $item->getItemRecebido()->getQtd() : 0;
        }

        return $totalRecebido;
    }

    /**
     * @return int
     */
    public function getTotalAReceber() {
        $total = 0;

        foreach ($this->itensDescarte as $item) {
            $total += $item->getQtd();
        }

        return $total;
    }
    
    public function getTotalEstornado(){
        $total = 0;
        
        foreach($this->itensDescarte as $item){
            if($item->getEstornoDescarte()){
                $total += $item->getEstornoDescarte()->getQtd();
            }
        }
        
        return $total;
    }

}
