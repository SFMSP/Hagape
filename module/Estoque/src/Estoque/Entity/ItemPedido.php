<?php

namespace Estoque\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * ItemPedido
 *
 * @ORM\Table(name="tb_item_pedido", indexes={@ORM\Index(name="fk_tb_item_pedido_tb_produto1_idx", columns={"id_produto"}), @ORM\Index(name="fk_tb_item_pedido_tb_pedido1_idx", columns={"id_pedido"})})
 * @ORM\Entity(repositoryClass="Estoque\Repository\ItemPedidoRepository")
 */
class ItemPedido extends BaseEntity implements EntityInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_item_pedido", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="nu_qtd", type="integer", nullable=false)
     */
    private $qtd;

    /**
     * @var string
     *
     * @ORM\Column(name="nu_preco_unitario", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $precoUnitario;

    /**
     * @var Pedido
     *
     * @ORM\ManyToOne(targetEntity="Pedido")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pedido", referencedColumnName="id_pedido")
     * })
     */
    private $pedido;

    /**
     * @var Produto
     *
     * @ORM\ManyToOne(targetEntity="Produto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_produto", referencedColumnName="id_produto")
     * })
     */
    private $produto;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ItemRecebido", mappedBy="itemPedido")
     */
    private $itensRecebidos;

    function getId() {
        return $this->id;
    }

    function getQtd() {
        return $this->qtd;
    }

    function getPrecoUnitario() {
        return $this->precoUnitario;
    }

    function getPedido() {
        return $this->pedido;
    }

    function getProduto() {
        return $this->produto;
    }

    function getItensRecebidos() {
        return $this->itensRecebidos;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setQtd($qtd) {
        $this->qtd = $qtd;
    }

    function setPrecoUnitario($precoUnitario) {
        $this->precoUnitario = $precoUnitario;
    }

    function setPedido(Pedido $pedido) {
        $this->pedido = $pedido;
    }

    function setProduto(Produto $produto) {
        $this->produto = $produto;
    }

    function setItensRecebidos(\Doctrine\Common\Collections\ArrayCollection $itensRecebidos) {
        $this->itensRecebidos = $itensRecebidos;
    }

    public function __toString() {
        return $this->produto->getNome();
    }

    public function getLabel() {
        return $this->__toString();
    }

    public function toArray() {
        return array(
            'id' => $this->id,
            'qtd' => $this->qtd,
            'precoUnitario' => $this->precoUnitario,
            'pedido' => $this->pedido,
            'produto' => $this->produto,
            'itensRecebidos' => $this->itensRecebidos
        );
    }

    public function getQuantidadeRecebida() {
        $qtdRecebida = 0;
 
        if ($this->itensRecebidos) {
            foreach ($this->itensRecebidos as $item) {
                $qtdRecebida += $item->getQtdRecebida();
            }
        }

        return $qtdRecebida;
    }

}
