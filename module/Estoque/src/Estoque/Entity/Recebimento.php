<?php

namespace Estoque\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Recebimento
 *
 * @ORM\Table(name="tb_recebimento", indexes={@ORM\Index(name="FK_pedido_recebimento", columns={"id_pedido"})})
 * @ORM\Entity(repositoryClass="Estoque\Repository\RecebimentoRepository")
 */
class Recebimento extends BaseEntity implements EntityInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_recebimento", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=false)
     */
    private $dataCadastro;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_recebimento", type="datetime", nullable=false)
     */
    private $dataRecebimento;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_responsavel_entrega", type="string", length=255, nullable=false)
     */
    private $responsavelEntrega;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_cpf_rg", type="string", length=20, nullable=false)
     */
    private $cpfRg;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_observacao", type="text", nullable=false)
     */
    private $observacao;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nota", type="string", length=50, nullable=false)
     */
    private $numeroNota;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_arquivo_nota", type="string", length=255, nullable=false)
     */
    private $notaFiscal;

    /**
     * @var Pedido
     *
     * @ORM\ManyToOne(targetEntity="Pedido", inversedBy="recebimento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pedido", referencedColumnName="id_pedido")
     * })
     */
    private $pedido;

    /**
     * @var string
     *
     * @ORM\Column(name="nu_valor_total", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valorTotal;

    /**
     * @var \Application\Entity\Usuario
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     * })
     */
    private $usuario;

    /**
     * @ORM\OneToMany(targetEntity="Estoque\Entity\ItemRecebido", mappedBy="recebimento")
     */
    private $itensRecebidos;

    public function __construct() {
        $this->dataCadastro = new \DateTime('now');
    }

    public function getId() {
        return $this->id;
    }

    public function getDataCadastro() {
        return $this->dataCadastro;
    }

    public function getDataRecebimento() {
        return $this->dataRecebimento;
    }

    public function getResponsavelEntrega() {
        return $this->responsavelEntrega;
    }

    public function getCpfRg() {
        return $this->cpfRg;
    }

    public function getObservacao() {
        return $this->observacao;
    }

    public function getNumeroNota() {
        return $this->numeroNota;
    }

    public function getNotaFiscal() {
        return $this->notaFiscal;
    }

    public function getPedido() {
        return $this->pedido;
    }

    public function getValorTotal() {
        return $this->valorTotal;
    }

    public function getUsuario() {
        return $this->usuario;
    }

    public function getItensRecebidos() {
        return $this->itensRecebidos;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setDataCadastro(\DateTime $dataCadastro) {
        $this->dataCadastro = $dataCadastro;
    }

    public function setDataRecebimento(\DateTime $dataRecebimento) {
        $this->dataRecebimento = $dataRecebimento;
    }

    public function setResponsavelEntrega($responsavelEntrega) {
        $this->responsavelEntrega = $responsavelEntrega;
    }

    public function setCpfRg($cpfRg) {
        $this->cpfRg = $cpfRg;
    }

    public function setObservacao($observacao) {
        $this->observacao = $observacao;
    }

    public function setNumeroNota($numeroNota) {
        $this->numeroNota = $numeroNota;
    }

    public function setNotaFiscal($notaFiscal) {
        $this->notaFiscal = $notaFiscal;
    }

    public function setPedido(Pedido $pedido) {
        $this->pedido = $pedido;
    }

    public function setValorTotal($valorTotal) {
        $this->valorTotal = $valorTotal;
    }

    public function setUsuario(\Application\Entity\Usuario $usuario) {
        $this->usuario = $usuario;
    }

    public function setItensRecebidos($itensRecebidos) {
        $this->itensRecebidos = $itensRecebidos;
    }

    public function __toString() {
        return $this->numeroNota;
    }

    public function getLabel() {
        return $this->__toString();
    }

    public function toArray() {
        return array(
            'id' => $this->id,
            'dataCadastro' => $this->dataCadastro,
            'dataRecebimento' => $this->dataRecebimento,
            'responsavelEntrega' => $this->responsavelEntrega,
            'cpfRg' => $this->cpfRg,
            'observacao' => $this->observacao,
            'numeroNota' => $this->numeroNota,
            'notaFiscal' => $this->notaFiscal,
            'pedido' => $this->pedido,
            'valorTotal' => $this->valorTotal,
            'usuario' => $this->usuario
        );
    }

}
