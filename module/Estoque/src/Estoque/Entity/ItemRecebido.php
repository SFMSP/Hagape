<?php

namespace Estoque\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * ItemRecebido
 *
 * @ORM\Table(name="tb_item_recebido", indexes={@ORM\Index(name="FK_item_pedido_item_recebido", columns={"id_item_pedido"}), @ORM\Index(name="FK_item_recebido_recebimento", columns={"id_recebimento"})})
 * @ORM\Entity(repositoryClass="Estoque\Repository\ItemRecebidoRepository")
 */
class ItemRecebido extends BaseEntity implements EntityInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_item_recebido", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="nu_qtd_recebida", type="integer", nullable=false)
     */
    private $qtdRecebida;

    /**
     * @var ItemPedido
     *
     * @ORM\ManyToOne(targetEntity="ItemPedido", inversedBy="itensRecebidos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_item_pedido", referencedColumnName="id_item_pedido")
     * })
     */
    private $itemPedido;

    /**
     * @var Recebimento
     *
     * @ORM\ManyToOne(targetEntity="Recebimento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_recebimento", referencedColumnName="id_recebimento")
     * })
     */
    private $recebimento;

    function getId() {
        return $this->id;
    }

    function getQtdRecebida() {
        return $this->qtdRecebida;
    }

    function getItemPedido() {
        return $this->itemPedido;
    }

    function getRecebimento() {
        return $this->recebimento;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setQtdRecebida($qtdRecebida) {
        $this->qtdRecebida = $qtdRecebida;
    }

    function setItemPedido(ItemPedido $itemPedido) {
        $this->itemPedido = $itemPedido;
    }

    function setRecebimento(Recebimento $recebimento) {
        $this->recebimento = $recebimento;
    }

    public function __toString() {
        return $this->itemPedido->getLabel();
    }

    public function getLabel() {
        return $this->__toString();
    }

    public function toArray() {
        return array(
            'id' => $this->id,
            'qtdRecebida' => $this->qtdRecebida,
            'itemPedido' => $this->itemPedido,
            'recebimento' => $this->recebimento
        );
    }

}
