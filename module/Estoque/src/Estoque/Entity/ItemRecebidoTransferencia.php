<?php

namespace Estoque\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * ItemRecebidoTransferencia
 *
 * @ORM\Table(name="tb_item_recebido_transferencia", indexes={@ORM\Index(name="FK2_tb_item_recebido_transferencia_tb_recebimento_transferencia", columns={"id_recebimento_transferencia"}), @ORM\Index(name="FK2_tb_item_recebido_transferencia_tb_item_transferencia", columns={"id_item_transferencia"})})
 * @ORM\Entity(repositoryClass="Estoque\Repository\ItemRecebidoTransferenciaRepository")
 */
class ItemRecebidoTransferencia extends BaseEntity implements EntityInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_item_recebido_transferencia", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="nu_qtd_recebida", type="integer", nullable=false)
     */
    private $qtd;

    /**
     * @var ItemTransferencia
     *
     * @ORM\OneToOne(targetEntity="ItemTransferencia", inversedBy="itensRecebidos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_item_transferencia", referencedColumnName="id_item_transferencia")
     * })
     */
    private $itemTransferencia;

    /**
     * @var RecebimentoTransferencia
     *
     * @ORM\ManyToOne(targetEntity="RecebimentoTransferencia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_recebimento_transferencia", referencedColumnName="id_recebimento_transferencia")
     * })
     */
    private $recebimentoTransferencia;

    public function getId() {
        return $this->id;
    }

    public function getQtd() {
        return $this->qtd;
    }

    public function getItemTransferencia() {
        return $this->itemTransferencia;
    }

    public function getRecebimentoTransferencia() {
        return $this->recebimentoTransferencia;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setQtd($qtd) {
        $this->qtd = $qtd;
    }

    public function setItemTransferencia(ItemTransferencia $itemTransferencia) {
        $this->itemTransferencia = $itemTransferencia;
    }

    public function setRecebimentoTransferencia(RecebimentoTransferencia $recebimentoTransferencia) {
        $this->recebimentoTransferencia = $recebimentoTransferencia;
    }

    public function __toString() {
        return $this->itemTransferencia->getProduto()->getNome();
    }

    public function getLabel() {
        return $this->__toString();
    }

    public function toArray() {
        return array(
            "id" => $this->id,
            "qtd" => $this->qtd,
            "itemTransferencia" => $this->itemTransferencia,
            "recebimentoTransferencia" => $this->recebimentoTransferencia,
        );
    }

}
