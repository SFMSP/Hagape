<?php

namespace Estoque\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Medida
 *
 * @ORM\Table(name="tb_medida", indexes={@ORM\Index(name="FK_tb_medida_tb_tamanho", columns={"id_tamanho"})})
 * @ORM\Entity(repositoryClass="Estoque\Repository\MedidaRepository")
 */
class Medida
{ 
    /**
     * @var integer
     *
     * @ORM\Column(name="id_medida", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_medida", type="string", length=50, nullable=true)
     */
    private $medida;

    /**
     * @var \Estoque\Entity\Tamanho
     *
     * @ORM\ManyToOne(targetEntity="Estoque\Entity\Tamanho")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tamanho", referencedColumnName="id_tam")
     * })
     */
    private $id_Tamanho;
    
    function getId() {
        return $this->id;
    }

    function getMedida() {
        return $this->medida;
    }

    function getId_Tamanho() {
        return $this->id_Tamanho;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setMedida($medida) {
        $this->medida = $medida;
    }

    function setId_Tamanho(\Estoque\Entity\Tamanho $id_Tamanho) {
        $this->id_Tamanho = $id_Tamanho;
    }
    public function toArray() {
        return array(
            'id' => $this->id,
            'medida' => $this->medida            
            
        );
    }

    public function __toString() {
        return $this->medida;
    }

    public function getLabel() {
        return $this->__toString();
    }

    


}
