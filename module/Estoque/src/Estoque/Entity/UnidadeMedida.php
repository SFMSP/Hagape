<?php

namespace Estoque\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UnidadeMedida
 *
 * @ORM\Table(name="tb_unidade_medida", indexes={@ORM\Index(name="IDX_2A6E2CE74826061F", columns={"id_tamanho"})})
 * @ORM\Entity(repositoryClass="Estoque\Repository\UnidadeMedidaRepository")
 */
class UnidadeMedida
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_unidade_medida", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_medida", type="string", length=100, nullable=false)
     */
    private $medida;

    /**
     * @var \Estoque\Entity\TbTamanho
     *
     * @ORM\ManyToOne(targetEntity="Estoque\Entity\Tamanho")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tamanho", referencedColumnName="id_tam")
     * })
     */
    private $id_tamanho;

    function getId() {
        return $this->id;
    }

    function getMedida() {
        return $this->medida;
    }

    function getId_tamanho() {
        return $this->id_tamanho;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setMedida($medida) {
        $this->medida = $medida;
    }

    function setId_tamanho(\Estoque\Entity\Tamanho $id_tamanho) {
        $this->id_tamanho = $id_tamanho;
    }
    
    public function toArray() {
        return array(
            'id' => $this->id,
            'medida' => $this->medida,
            'id_tamanho' => $this->id_tamanho
            
        );
    }

    public function __toString() {
        return $this->medida;
    }

    public function getLabel() {
        return $this->__toString();
    }


}
