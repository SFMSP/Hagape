<?php

namespace Estoque\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Pedido
 *
 * @ORM\Table(name="tb_pedido", indexes={@ORM\Index(name="fk_tb_pedido_tb_contrato1_idx", columns={"id_contrato"}), @ORM\Index(name="fk_tb_pedido_tb_fornecedor1_idx", columns={"id_fornecedor"}), @ORM\Index(name="fk_tb_pedido_tb_empenho1_idx", columns={"id_empenho"}), @ORM\Index(name="fk_tb_pedido_tb_estoque1_idx", columns={"id_origem"}), @ORM\Index(name="fk_tb_pedido_tb_estoque2_idx", columns={"id_destino"})})
 * @ORM\Entity(repositoryClass="Estoque\Repository\PedidoRepository")
 */
class Pedido extends BaseEntity implements EntityInterface {

    /**
     * Lista de status disponíveis para um pedido
     */
    const STATUS_CADASTRADO = 1;
    /**
     *
     */
    const STATUS_SOLICITADO = 2;
    /**
     *
     */
    const STATUS_RECEBIDO_INCOMPLETO = 3;
    /**
     *
     */
    const STATUS_CONCLUIDO = 4;
    /**
     *
     */
    const STATUS_CANCELADO = 5;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_pedido", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=false)
     */
    private $dataCadastro;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_solicitacao", type="datetime", nullable=true)
     */
    private $dataSolicitacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_entrega", type="datetime", nullable=true)
     */
    private $dataEntrega;

    /**
     * @var string
     *
     * @ORM\Column(name="nu_pedido", type="string", length=45, nullable=false)
     */
    private $numeroPedido;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_ofn", type="string", length=45, nullable=false)
     */
    private $ofn;

    /**
     * @var Contrato
     *
     * @ORM\ManyToOne(targetEntity="Contrato")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrato", referencedColumnName="id_contrato")
     * })
     */
    private $contrato;

    /**
     * @var Empenho
     *
     * @ORM\ManyToOne(targetEntity="Empenho", inversedBy="pedidos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_empenho", referencedColumnName="id_empenho")
     * })
     */
    private $empenho;

    /**
     * @var Admin\Entity\Estoque
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Estoque")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_origem", referencedColumnName="id_estoque")
     * })
     */
    private $estoqueOrigem;

    /**
     * @var Admin\Entity\Estoque
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Estoque")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_destino", referencedColumnName="id_estoque")
     * })
     */
    private $estoqueDestino;

    /**
     * @var Fornecedor
     *
     * @ORM\ManyToOne(targetEntity="Fornecedor")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_fornecedor", referencedColumnName="id_fornecedor")
     * })
     */
    private $fornecedor;

    /**
     * @var SituacaoPedido
     *
     * @ORM\ManyToOne(targetEntity="SituacaoPedido")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_situacao_pedido", referencedColumnName="id_situacao_pedido")
     * })
     */
    private $situacao;

    /**
     * @ORM\OneToMany(targetEntity="Estoque\Entity\ItemPedido", mappedBy="pedido")
     */
    private $itensPedido;

    /**
     * @ORM\OneToMany(targetEntity="Estoque\Entity\Recebimento", mappedBy="pedido")
     */
    private $recebimento;

    /**
     * Pedido constructor.
     */
    function __construct() {
        $this->dataCadastro = new \DateTime('now');
        $this->dataSolicitacao = null;
        $this->dataEntrega = null;
    }

    /**
     * @return int
     */
    function getId() {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    function getDataCadastro() {
        return $this->dataCadastro;
    }

    /**
     * @return \DateTime|null
     */
    function getDataSolicitacao() {
        return $this->dataSolicitacao;
    }

    /**
     * @return \DateTime|null
     */
    function getDataEntrega() {
        return $this->dataEntrega;
    }

    /**
     * @return string
     */
    function getNumeroPedido() {
        return $this->numeroPedido;
    }

    /**
     * @return string
     */
    function getOfn() {
        return $this->ofn;
    }

    /**
     * @return Contrato
     */
    function getContrato() {
        return $this->contrato;
    }

    /**
     * @return Empenho
     */
    function getEmpenho() {
        return $this->empenho;
    }

    /**
     * @return Admin\Entity\Estoque
     */
    function getEstoqueOrigem() {
        return $this->estoqueOrigem;
    }

    /**
     * @return Admin\Entity\Estoque
     */
    function getEstoqueDestino() {
        return $this->estoqueDestino;
    }

    /**
     * @return Fornecedor
     */
    function getFornecedor() {
        return $this->fornecedor;
    }

    /**
     * @return SituacaoPedido
     */
    function getSituacao() {
        return $this->situacao;
    }

    /**
     * @return mixed
     */
    function getItensPedido() {
        return $this->itensPedido;
    }

    /**
     * @param $id
     */
    function setId($id) {
        $this->id = $id;
    }

    /**
     * @param \DateTime $dataCadastro
     */
    function setDataCadastro(\DateTime $dataCadastro) {
        $this->dataCadastro = $dataCadastro;
    }

    /**
     * @param \DateTime|null $dataSolicitacao
     */
    function setDataSolicitacao(\DateTime $dataSolicitacao = null) {
        $this->dataSolicitacao = $dataSolicitacao;
    }

    /**
     * @param \DateTime|null $dataEntrega
     */
    function setDataEntrega(\DateTime $dataEntrega = null) {
        $this->dataEntrega = $dataEntrega;
    }

    /**
     * @param $numeroPedido
     */
    function setNumeroPedido($numeroPedido) {
        $this->numeroPedido = $numeroPedido;
    }

    /**
     * @param $ofn
     */
    function setOfn($ofn) {
        $this->ofn = $ofn;
    }

    /**
     * @param Contrato $contrato
     */
    function setContrato(Contrato $contrato) {
        $this->contrato = $contrato;
    }

    /**
     * @param Empenho $empenho
     */
    function setEmpenho(Empenho $empenho) {
        $this->empenho = $empenho;
    }

    /**
     * @param \Admin\Entity\Estoque $estoqueOrigem
     */
    function setEstoqueOrigem(\Admin\Entity\Estoque $estoqueOrigem) {
        $this->estoqueOrigem = $estoqueOrigem;
    }

    /**
     * @param \Admin\Entity\Estoque $estoqueDestino
     */
    function setEstoqueDestino(\Admin\Entity\Estoque $estoqueDestino) {
        $this->estoqueDestino = $estoqueDestino;
    }

    /**
     * @param Fornecedor $fornecedor
     */
    function setFornecedor(Fornecedor $fornecedor) {
        $this->fornecedor = $fornecedor;
    }

    /**
     * @param SituacaoPedido $situacao
     */
    function setSituacao(SituacaoPedido $situacao) {
        $this->situacao = $situacao;
    }

    /**
     * @param $itensPedido
     */
    function setItensPedido($itensPedido) {
        $this->itensPedido = $itensPedido;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->numeroPedido;
    }

    /**
     * @return string
     */
    public function getLabel() {
        return $this->__toString();
    }

    /**
     * @return array
     */
    public function toArray() {
        $dados = array(
            'id' => $this->id,
            'dataCadastro' => $this->dataCadastro->format('d/m/Y'),
            'numeroPedido' => $this->numeroPedido,
            'ofn' => $this->ofn,
            'contrato' => $this->contrato,
            'empenho' => $this->empenho,
            'estoqueOrigem' => $this->estoqueOrigem,
            'estoqueDestino' => $this->estoqueDestino,
            'fornecedor' => $this->fornecedor,
            'situacao' => $this->situacao,
            'recebimento' => $this->recebimento
        );

        if ($this->dataSolicitacao) {
            $dados['dataSolicitacao'] = $this->dataSolicitacao->format('d/m/Y');
        }

        if ($this->dataEntrega) {
            $dados['dataEntrega'] = $this->dataEntrega->format('d/m/Y');
        }

        return $dados;
    }

    /**
     * @return array
     */
    public function getIdProdutosPedido() {
        $array = array();

        foreach ($this->itensPedido as $item) {
            $array[] = $item->getProduto()->getId();
        }

        return $array;
    }

    /**
     * @return int
     */
    public function getTotalRecebido() {
        $totalRecebido = 0;

        foreach ($this->itensPedido as $itemPedido) {
            $totalRecebido += $itemPedido->getQuantidadeRecebida();
        }

        return $totalRecebido;
    }

    /**
     * @return int
     */
    public function getTotalAReceber() {
        $total = 0;

        foreach ($this->itensPedido as $itemPedido) {
            $total += $itemPedido->getQtd();
        }

        return $total;
    }

    /**
     * @return int
     */
    public function getValorTotalPedido() {
        $total = 0;

        foreach ($this->itensPedido as $itemPedido) {
            $total += $itemPedido->getPrecoUnitario() * $itemPedido->getQtd();
        }

        return $total;
    }

    /**
     * @return int
     */
    public function getValorTotalRecebido() {
        $totalRecebido = 0;

        foreach ($this->itensPedido as $itemPedido) {
            foreach ($itemPedido->getItensRecebidos() as $itemRecebido) {
                $totalRecebido += $itemRecebido->getQtdRecebida() * $itemPedido->getPrecoUnitario();
            }
        }

        return $totalRecebido;
    }

    /**
     * @return mixed
     */
    public function getRecebimento()
    {
        return $this->recebimento;
    }

    /**
     * @param mixed $recebimento
     */
    public function setRecebimento($recebimento)
    {
        $this->recebimento = $recebimento;
    }
}
