<?php

namespace Estoque\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Empenho
 *
 * @ORM\Table(name="tb_empenho", indexes={@ORM\Index(name="fk_id_contrato_idx", columns={"id_contrato"})})
 * @ORM\Entity(repositoryClass="Estoque\Repository\EmpenhoRepository")
 */
class Empenho extends BaseEntity implements EntityInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_empenho", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_num_empenho", type="string", length=45, nullable=false)
     */
    private $numEmpenho;

    /**
     * @var float
     *
     * @ORM\Column(name="valor", type="float", precision=10, scale=0, nullable=false)
     */
    private $valor;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_empenho", type="datetime", nullable=true)
     */
    private $dataEmpenho;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_provisorio", type="boolean", nullable=true)
     */
    private $provisorio;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_observacao", type="text", nullable=true)
     */
    private $observacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=false)
     */
    private $dataCadastro;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_ativo", type="boolean", nullable=false)
     */
    private $ativo;

    /**
     * @var \Estoque\Entity\Contrato
     *
     * @ORM\ManyToOne(targetEntity="Estoque\Entity\Contrato", inversedBy="empenhos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrato", referencedColumnName="id_contrato")
     * })
     */
    private $contrato;

    /**
     * @ORM\OneToMany(targetEntity="Estoque\Entity\Pedido", mappedBy="empenho")
     */
    private $pedidos;

    public function __construct()
    {
        $this->dataCadastro = new \DateTime('now');
    }

    /**
     * Get id_empenho
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set num_empenho
     *
     * @param string $numEmpenho
     * @return Empenho
     */
    public function setNumEmpenho($numEmpenho) {
        $this->numEmpenho = $numEmpenho;

        return $this;
    }

    /**
     * Get num_empenho
     *
     * @return string
     */
    public function getNumEmpenho() {
        return $this->numEmpenho;
    }

    /**
     * Set valor
     *
     * @param float $valor
     * @return Empenho
     */
    public function setValor($valor) {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float
     */
    public function getValor() {
        return $this->valor;
    }

    /**
     * Set data_empenho
     *
     * @param \DateTime $dataEmpenho
     * @return Empenho
     */
    public function setDataEmpenho($dataEmpenho) {
        $this->dataEmpenho = $dataEmpenho;

        return $this;
    }

    /**
     * Get data_empenho
     *
     * @return \DateTime
     */
    public function getDataEmpenho() {
        return $this->dataEmpenho;
    }

    /**
     * Set bool_provisorio
     *
     * @param boolean $provisorio
     * @return Empenho
     */
    public function setProvisorio($provisorio) {
        $this->provisorio = $provisorio;

        return $this;
    }

    /**
     * Get bool_provisorio
     *
     * @return boolean
     */
    public function getProvisorio() {
        return $this->provisorio;
    }

    /**
     * Set observacao
     *
     * @param string $observacao
     * @return Empenho
     */
    public function setObservacao($observacao) {
        $this->observacao = $observacao;

        return $this;
    }

    /**
     * Get observacao
     *
     * @return string
     */
    public function getObservacao() {
        return $this->observacao;
    }

    /**
     * Set data_cadastro
     *
     * @param \DateTime $dataCadastro
     * @return Empenho
     */
    public function setDataCadastro($dataCadastro) {
        $this->dataCadastro = $dataCadastro;

        return $this;
    }

    /**
     * Get data_cadastro
     *
     * @return \DateTime
     */
    public function getDataCadastro() {
        return $this->dataCadastro;
    }

    /**
     * Get pedidos
     * 
     * @return \Doctrine\Common\Collections\Collection
     */
    function getPedidos() {
        return $this->pedidos;
    }

    /**
     * 
     * @param \Doctrine\Common\Collections\Collection $pedidos
     */
    function setPedidos(\Doctrine\Common\Collections\Collection $pedidos) {
        $this->pedidos = $pedidos;
    }

    /**
     * Set ativo
     *
     * @param boolean $ativo
     * @return Empenho
     */
    public function setAtivo($ativo) {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return boolean
     */
    public function getAtivo() {
        return $this->ativo;
    }

    /**
     * Set id_contrato
     *
     * @param \Estoque\Entity\Contrato $idContrato
     * @return Empenho
     */
    public function setContrato(\Estoque\Entity\Contrato $contrato = null) {
        $this->contrato = $contrato;

        return $this;
    }

    /**
     * Get id_contrato
     *
     * @return \Estoque\Entity\Contrato
     */
    public function getContrato() {
        return $this->contrato;
    }

    public function toArray() {
        return array(
            'id' => $this->id,
            'numEmpenho' => $this->numEmpenho,
            'valor' => $this->valor,
            'dataEmpenho' => $this->dataEmpenho,
            'provisorio' => $this->provisorio,
            'observacao' => $this->observacao,
            'dataCadastro' => $this->dataCadastro,
            'ativo' => $this->ativo,
            'contrato' => $this->contrato
        );
    }

    public function __toString() {
        return $this->observacao;
    }

    public function getLabel() {
        return $this->__toString();
    }

}
