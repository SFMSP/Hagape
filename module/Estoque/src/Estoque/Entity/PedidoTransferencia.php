<?php

namespace Estoque\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;
use Admin\Entity\Estoque;

/**
 * PedidoTransferencia
 *
 * @ORM\Table(name="tb_pedido_transferencia", indexes={@ORM\Index(name="FK1_tb_pedido_transferencia_tb_estoque", columns={"id_origem"}), @ORM\Index(name="FK2_tb_pedido_tranferencia_tb_estoque", columns={"id_destino"}), @ORM\Index(name="FK3_tb_pedido_transferencia_tb_usuario", columns={"id_usuario"}), @ORM\Index(name="FK4_tb_pedido_transferencia_tb_situacao_transferencia", columns={"id_situacao_transferencia"})})
 * @ORM\Entity(repositoryClass="Estoque\Repository\PedidoTransferenciaRepository")
 */
class PedidoTransferencia extends BaseEntity implements EntityInterface {

    /**
     * Lista de status disponíveis para um pedido de transferência
     */
    const STATUS_CADASTRADO = 1;
    const STATUS_EM_TRANSITO = 2;
    const STATUS_ENCERRADO = 3;
    const STATUS_RECEBIDO_INCOMPLETO = 4;
    const STATUS_CANCELADO = 5;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_pedido_transferencia", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_transferencia", type="date", nullable=false)
     */
    private $dataTransferencia;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_entrega", type="date", nullable=true)
     */
    private $dataEntrega;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_numero_transferencia", type="string", length=50, nullable=false)
     */
    private $numeroTransferencia;

    /**
     * @var Admin\Entity\Estoque
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Estoque")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_origem", referencedColumnName="id_estoque")
     * })
     */
    private $estoqueOrigem;

    /**
     * @var Admin\Entity\Estoque
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Estoque")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_destino", referencedColumnName="id_estoque")
     * })
     */
    private $estoqueDestino;

    /**
     * @var \Application\Entity\Usuario
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     * })
     */
    private $usuario;

    /**
     * @var SituacaoTransferencia
     *
     * @ORM\ManyToOne(targetEntity="SituacaoTransferencia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_situacao_transferencia", referencedColumnName="id_situacao_transferencia")
     * })
     */
    private $situacaoTransferencia;

    /**
     * @ORM\OneToMany(targetEntity="Estoque\Entity\ItemTransferencia", mappedBy="pedidoTransferencia")
     */
    private $itensTransferencia;

    /**
     * @var RecebimentoTransferencia
     *
     * @ORM\OneToOne(targetEntity="RecebimentoTransferencia", mappedBy="pedidoTransferencia")
     */
    private $recebimentoTransferencia;

    public function getId() {
        return $this->id;
    }

    public function getDataTransferencia() {
        return $this->dataTransferencia;
    }

    public function getDataEntrega() {
        return $this->dataEntrega;
    }

    public function getNumeroTransferencia() {
        return $this->numeroTransferencia;
    }

    public function getEstoqueOrigem() {
        return $this->estoqueOrigem;
    }

    public function getEstoqueDestino() {
        return $this->estoqueDestino;
    }

    public function getUsuario() {
        return $this->usuario;
    }

    public function getSituacaoTransferencia() {
        return $this->situacaoTransferencia;
    }

    public function getItensTransferencia() {
        return $this->itensTransferencia;
    }

    public function getRecebimentoTransferencia() {
        return $this->recebimentoTransferencia;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setDataTransferencia(\DateTime $dataTransferencia) {
        $this->dataTransferencia = $dataTransferencia;
    }

    public function setDataEntrega(\DateTime $dataEntrega = null) {
        $this->dataEntrega = $dataEntrega;
    }

    public function setNumeroTransferencia($numeroTransferencia) {
        $this->numeroTransferencia = $numeroTransferencia;
    }

    public function setEstoqueOrigem(Estoque $estoqueOrigem) {
        $this->estoqueOrigem = $estoqueOrigem;
    }

    public function setEstoqueDestino(Estoque $estoqueDestino) {
        $this->estoqueDestino = $estoqueDestino;
    }

    public function setUsuario(\Application\Entity\Usuario $usuario) {
        $this->usuario = $usuario;
    }

    public function setSituacaoTransferencia(SituacaoTransferencia $situacaoTransferencia) {
        $this->situacaoTransferencia = $situacaoTransferencia;
    }

    public function setItensTransferencia($itensTransferencia) {
        $this->itensTransferencia = $itensTransferencia;
    }

    public function setRecebimentoTransferencia(RecebimentoTransferencia $recebimentoTransferencia) {
        $this->recebimentoTransferencia = $recebimentoTransferencia;
    }

    public function __toString() {
        return $this->numeroTransferencia;
    }

    public function getLabel() {
        return $this->__toString();
    }

    public function toArray() {
        $array = array(
            "id" => $this->id,
            "dataTransferencia" => $this->dataTransferencia->format('d/m/Y'),
            "numeroTransferencia" => $this->numeroTransferencia,
            "estoqueOrigem" => $this->estoqueOrigem->getId(),
            "estoqueDestino" => $this->estoqueDestino->getId(),
            "usuario" => $this->usuario,
            "situacaoTransferencia" => $this->situacaoTransferencia,
            "itensTransferencia" => $this->itensTransferencia,
            "recebimentoTransferencia" => $this->recebimentoTransferencia
        );

        if ($this->dataEntrega) {
            $array['dataEntrega'] = $this->dataEntrega->format('d/m/Y');
        }

        return $array;
    }

    /**
     * @return array
     */
    public function getIdProdutosPedido() {
        $array = array();

        foreach ($this->itensTransferencia as $item) {
            $array[] = $item->getProduto()->getId();
        }

        return $array;
    }

    /**
     * @return float
     */
    public function getValorTotalPedidoTransferencia() {
        $total = 0;

        foreach ($this->itensTransferencia as $item) {
            $total += $item->getValorTotal();
        }

        return $total;
    }

    /**
     * @return int
     */
    public function getTotalRecebido() {
        $totalRecebido = 0;

        foreach ($this->itensTransferencia as $item) {
            $totalRecebido += $item->getItemRecebido() ? $item->getItemRecebido()->getQtd() : 0;
        }

        return $totalRecebido;
    }

    /**
     * @return int
     */
    public function getTotalAReceber() {
        $total = 0;

        foreach ($this->itensTransferencia as $item) {
            $total += $item->getQtd();
        }

        return $total;
    }

}
