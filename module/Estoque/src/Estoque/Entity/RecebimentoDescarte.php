<?php

namespace Estoque\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * RecebimentoDescarte
 *
 * @ORM\Table(name="tb_recebimento_descarte", indexes={@ORM\Index(name="FK1_tb_recebimento_descarte_tb_solicitacao_descarte", columns={"id_solicitacao_descarte"})})
 * @ORM\Entity(repositoryClass="Estoque\Repository\RecebimentoDescarteRepository")
 */
class RecebimentoDescarte extends BaseEntity implements EntityInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_recebimento_descarte", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_recebimento_descarte", type="date", nullable=false)
     */
    private $dataRecebimentoDescarte;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome_entregador", type="string", length=255, nullable=false)
     */
    private $nomeEntregador;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_cpf_rg", type="string", length=25, nullable=false)
     */
    private $cpfRg;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_observacao", type="text", nullable=true)
     */
    private $observacao;

    /**
     * @var SolicitacaoDescarte
     *
     * @ORM\OneToOne(targetEntity="SolicitacaoDescarte", inversedBy="recebimentoDescarte")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_solicitacao_descarte", referencedColumnName="id_solicitacao_descarte")
     * })
     */
    private $solicitacaoDescarte;

    public function getId() {
        return $this->id;
    }

    public function getDataRecebimentoDescarte() {
        return $this->dataRecebimentoDescarte;
    }

    public function getNomeEntregador() {
        return $this->nomeEntregador;
    }

    public function getCpfRg() {
        return $this->cpfRg;
    }

    public function getObservacao() {
        return $this->observacao;
    }

    public function getSolicitacaoDescarte() {
        return $this->solicitacaoDescarte;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setdataRecebimentoDescarte(\DateTime $dataRecebimentoDescarte) {
        $this->dataRecebimentoDescarte = $dataRecebimentoDescarte;
    }

    public function setNomeEntregador($nomeEntregador) {
        $this->nomeEntregador = $nomeEntregador;
    }

    public function setCpfRg($cpfRg) {
        $this->cpfRg = $cpfRg;
    }

    public function setObservacao($observacao) {
        $this->observacao = $observacao;
    }

    public function setSolicitacaoDescarte(SolicitacaoDescarte $solicitacaoDescarte) {
        $this->solicitacaoDescarte = $solicitacaoDescarte;
    }

    public function __toString() {
        return $this->cpfRg;
    }

    public function getLabel() {
        return $this->__toString();
    }

    public function toArray() {
        return array(
            "id" => $this->id,
            "dataRecebimentoDescarte" => $this->dataRecebimentoDescarte->format('d/m/Y'),
            "nomeEntregador" => $this->nomeEntregador,
            "cpfRg" => $this->cpfRg,
            "observacao" => $this->observacao,
            "solicitacaoDescarte" => $this->solicitacaoDescarte
        );
    }

}
