<?php

namespace Estoque\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * EstoqueProduto
 *
 * @ORM\Table(name="tb_estoque_produto", indexes={@ORM\Index(name="FK_estoque_produto_estoque", columns={"id_estoque"}), @ORM\Index(name="FK_estoque_produto_produto", columns={"id_produto"}), @ORM\Index(name="FK_estoque_produto_estoque2", columns={"id_origem"})})
 * @ORM\Entity(repositoryClass="Estoque\Repository\EstoqueProdutoRepository")
 */
class EstoqueProduto extends BaseEntity implements EntityInterface
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id_estoque_produto", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="nu_qtd", type="integer", nullable=false)
     */
    private $qtd;

    /**
     * @var \Admin\Entity\Estoque
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Estoque")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estoque", referencedColumnName="id_estoque")
     * })
     */
    private $estoque;

    /**
     * @var \Admin\Entity\Estoque
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Estoque")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_origem", referencedColumnName="id_estoque")
     * })
     */
    private $estoqueOrigem;

    /**
     * @var Produto
     *
     * @ORM\ManyToOne(targetEntity="Produto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_produto", referencedColumnName="id_produto")
     * })
     */
    private $produto;

    /**
     * @var integer
     *
     * @ORM\Column(name="nu_qtd_reservada", type="integer", nullable=true)
     */
    private $qtdReservada;

    /**
     * @return int
     */
    public function getQtdReservada()
    {
        return $this->qtdReservada;
    }

    /**
     * @param int $nuQtdReservada
     */
    public function setQtdReservada($qtdReservada)
    {
        $this->qtdReservada = $qtdReservada;
    }

    function getId()
    {
        return $this->id;
    }

    function getQtd()
    {
        return $this->qtd;
    }

    function getEstoque()
    {
        return $this->estoque;
    }

    function getEstoqueOrigem()
    {
        return $this->estoqueOrigem;
    }

    function getProduto()
    {
        return $this->produto;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setQtd($qtd)
    {
        $this->qtd = $qtd;
    }

    function setEstoque(\Admin\Entity\Estoque $estoque)
    {
        $this->estoque = $estoque;
    }

    function setEstoqueOrigem(\Admin\Entity\Estoque $estoqueOrigem)
    {
        $this->estoqueOrigem = $estoqueOrigem;
    }

    function setProduto(Produto $produto)
    {
        $this->produto = $produto;
    }

    public function __toString()
    {
        return $this->qtd;
    }

    public function getLabel()
    {
        return $this->__toString();
    }

    public function toArray()
    {
        return array(
            'id' => $this->id,
            'qtd' => $this->qtd,
            'estoque' => $this->estoque,
            'estoqueOrigem' => $this->estoqueOrigem,
            'produto' => $this->produto,
            'qtdReservada' => $this->qtdReservada
        );
    }

}
