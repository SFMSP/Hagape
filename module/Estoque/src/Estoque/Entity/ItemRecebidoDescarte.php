<?php

namespace Estoque\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * ItemRecebidoDescarte
 *
 * @ORM\Table(name="tb_item_recebido_descarte", indexes={@ORM\Index(name="FK2_tb_item_recebido_descarte_tb_recebimento_descarte", columns={"id_recebimento_descarte"}), @ORM\Index(name="FK2_tb_item_recebido_descarte_tb_item_descarte", columns={"id_item_descarte"})})
 * @ORM\Entity(repositoryClass="Estoque\Repository\ItemRecebidoDescarteRepository")
 */
class ItemRecebidoDescarte extends BaseEntity implements EntityInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_item_recebido_descarte", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="nu_qtd_recebida", type="integer", nullable=false)
     */
    private $qtd;

    /**
     * @var ItemDescarte
     *
     * @ORM\OneToOne(targetEntity="ItemDescarte", inversedBy="itemRecebido")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_item_descarte", referencedColumnName="id_item_descarte")
     * })
     */
    private $itemDescarte;

    /**
     * @var RecebimentoDescarte
     *
     * @ORM\ManyToOne(targetEntity="RecebimentoDescarte")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_recebimento_descarte", referencedColumnName="id_recebimento_descarte")
     * })
     */
    private $recebimentoDescarte;

    public function getId() {
        return $this->id;
    }

    public function getQtd() {
        return $this->qtd;
    }

    public function getItemDescarte() {
        return $this->itemDescarte;
    }

    public function getRecebimentoDescarte() {
        return $this->recebimentoDescarte;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setQtd($qtd) {
        $this->qtd = $qtd;
    }

    public function setItemDescarte(ItemDescarte $itemDescarte) {
        $this->itemDescarte = $itemDescarte;
    }

    public function setRecebimentoDescarte(RecebimentoDescarte $recebimentoDescarte) {
        $this->recebimentoDescarte = $recebimentoDescarte;
    }

    public function __toString() {
        return $this->itemDescarte->getProduto()->getNome();
    }

    public function getLabel() {
        return $this->__toString();
    }

    public function toArray() {
        return array(
            "id" => $this->id,
            "qtd" => $this->qtd,
            "itemDescarte" => $this->itemDescarte,
            "recebimentoDescarte" => $this->recebimentoDescarte,
        );
    }

}
