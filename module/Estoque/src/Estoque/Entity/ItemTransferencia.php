<?php

namespace Estoque\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * ItemTransferencia
 *
 * @ORM\Table(name="tb_item_transferencia", indexes={@ORM\Index(name="FK1_tb_item_transferencia_tb_pedido_transferencia", columns={"id_pedido_transferencia"}), @ORM\Index(name="FK2_tb_item_transferencia_tb_produto", columns={"id_produto"})})
 * @ORM\Entity(repositoryClass="Estoque\Repository\ItemTransferenciaRepository")
 */
class ItemTransferencia extends BaseEntity implements EntityInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_item_transferencia", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="nu_quantidade", type="integer", nullable=true)
     */
    private $qtd;

    /**
     * @var string
     *
     * @ORM\Column(name="nu_custo_medio", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $custoMedio;

    /**
     * @var string
     *
     * @ORM\Column(name="nu_valor_total", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $valorTotal;

    /**
     * @var PedidoTransferencia
     *
     * @ORM\ManyToOne(targetEntity="PedidoTransferencia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pedido_transferencia", referencedColumnName="id_pedido_transferencia")
     * })
     */
    private $pedidoTransferencia;

    /**
     * @var Produto
     *
     * @ORM\ManyToOne(targetEntity="Produto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_produto", referencedColumnName="id_produto")
     * })
     */
    private $produto;

    /**
     * @var ItemRecebidoTransferencia
     *
     * @ORM\OneToOne(targetEntity="ItemRecebidoTransferencia", mappedBy="itemTransferencia")
     */
    private $itemRecebido;

    public function getId() {
        return $this->id;
    }

    public function getQtd() {
        return $this->qtd;
    }

    public function getCustoMedio() {
        return $this->custoMedio;
    }

    public function getValorTotal() {
        return $this->valorTotal;
    }

    public function getPedidoTransferencia() {
        return $this->pedidoTransferencia;
    }

    public function getProduto() {
        return $this->produto;
    }

    public function getItemRecebido() {
        return $this->itemRecebido;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setQtd($qtd) {
        $this->qtd = $qtd;
    }

    public function setCustoMedio($custoMedio) {
        $this->custoMedio = $custoMedio;
    }

    public function setValorTotal($valorTotal) {
        $this->valorTotal = $valorTotal;
    }

    public function setPedidoTransferencia(PedidoTransferencia $pedidoTransferencia) {
        $this->pedidoTransferencia = $pedidoTransferencia;
    }

    public function setProduto(Produto $produto) {
        $this->produto = $produto;
    }

    public function setItemRecebido(ItemRecebidoTransferencia $itemRecebido) {
        $this->itemRecebido = $itemRecebido;
        return $this;
    }

    public function __toString() {
        return $this->produto->getNome();
    }

    public function getLabel() {
        return $this->__toString();
    }

    public function toArray() {
        return array(
            "id" => $this->id,
            "qtd" => $this->qtd,
            "custoMedio" => $this->custoMedio,
            "valorTotal" => $this->valorTotal,
            "pedidoTransferencia" => $this->pedidoTransferencia,
            "produto" => $this->produto,
            "itemRecebido" => $this->itemRecebido
        );
    }
}
