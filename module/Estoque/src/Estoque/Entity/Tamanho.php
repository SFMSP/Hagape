<?php

namespace Estoque\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tamanho
 *
 * @ORM\Table(name="tb_tamanho")
 * @ORM\Entity(repositoryClass="Estoque\Repository\TamanhoRepository")
 */
class Tamanho 
{
    /**
     * @var integers
     *
     * @ORM\Column(name="id_tam", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_tamanho", type="string", length=100, nullable=false)
     */
    private $tamanho;
    function getId() {
        return $this->id;
    }

    function getTamanho() {
        return $this->tamanho;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTamanho($tamanho) {
        $this->tamanho = $tamanho;
    }

    public function toArray() {
        return array(
            'id' => $this->id,
            'tamanho' => $this->tamanho            
            
        );
    }

    public function __toString() {
        return $this->tamanho;
    }

    public function getLabel() {
        return $this->__toString();
    }

}
