<?php

namespace Estoque\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * RecebimentoTransferencia
 *
 * @ORM\Table(name="tb_recebimento_transferencia", indexes={@ORM\Index(name="FK1_tb_recebimento_transferencia_tb_item_transferencia", columns={"id_pedido_transferencia"})})
 * @ORM\Entity(repositoryClass="Estoque\Repository\RecebimentoTransferenciaRepository")
 */
class RecebimentoTransferencia extends BaseEntity implements EntityInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_recebimento_transferencia", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_recebimento_transferencia", type="date", nullable=false)
     */
    private $dataRecebimentoTransferencia;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome_entregador", type="string", length=255, nullable=false)
     */
    private $nomeEntregador;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_cpf_rg", type="string", length=25, nullable=false)
     */
    private $cpfRg;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_observacao", type="text", nullable=true)
     */
    private $observacao;

    /**
     * @var PedidoTransferencia
     *
     * @ORM\OneToOne(targetEntity="PedidoTransferencia", inversedBy="recebimentoTransferencia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pedido_transferencia", referencedColumnName="id_pedido_transferencia")
     * })
     */
    private $pedidoTransferencia;

    public function getId() {
        return $this->id;
    }

    public function getDataRecebimentoTransferencia() {
        return $this->dataRecebimentoTransferencia;
    }

    public function getNomeEntregador() {
        return $this->nomeEntregador;
    }

    public function getCpfRg() {
        return $this->cpfRg;
    }

    public function getObservacao() {
        return $this->observacao;
    }

    public function getPedidoTransferencia() {
        return $this->pedidoTransferencia;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setDataRecebimentoTransferencia(\DateTime $dataRecebimentoTransferencia) {
        $this->dataRecebimentoTransferencia = $dataRecebimentoTransferencia;
    }

    public function setNomeEntregador($nomeEntregador) {
        $this->nomeEntregador = $nomeEntregador;
    }

    public function setCpfRg($cpfRg) {
        $this->cpfRg = $cpfRg;
    }

    public function setObservacao($observacao) {
        $this->observacao = $observacao;
    }

    public function setPedidoTransferencia(PedidoTransferencia $pedidoTransferencia) {
        $this->pedidoTransferencia = $pedidoTransferencia;
    }

    public function __toString() {
        return $this->cpfRg;
    }

    public function getLabel() {
        return $this->__toString();
    }

    public function toArray() {
        return array(
            "id" => $this->id,
            "dataRecebimentoTransferencia" => $this->dataRecebimentoTransferencia->format('d/m/Y'),
            "nomeEntregador" => $this->nomeEntregador,
            "cpfRg" => $this->cpfRg,
            "observacao" => $this->observacao,
            "pedidoTransferencia" => $this->pedidoTransferencia
        );
    }

}
