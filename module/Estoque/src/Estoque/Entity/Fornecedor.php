<?php

namespace Estoque\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;
/**
 * Fornecedor
 *
 * @ORM\Table(name="tb_fornecedor")
 * @ORM\Entity(repositoryClass="Estoque\Repository\FornecedorRepository")
 */
class Fornecedor extends BaseEntity implements EntityInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_fornecedor", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=false)
     */
    private $dataCadastro;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome", type="string", length=255, nullable=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_cnpj", type="string", length=255, nullable=false)
     */
    private $cnpj;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_endereco", type="text", nullable=false)
     */
    private $endereco;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_numero", type="string", length=12, nullable=false)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_bairro", type="string", length=255, nullable=false)
     */
    private $bairro;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_cep", type="string", length=12, nullable=false)
     */
    private $cep;

     /**
     * @var string
     *
     * @ORM\Column(name="txt_cidade", type="integer", length=11, nullable=false)
     */
    private $cidade;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_telefone1", type="string", length=50, nullable=false)
     */
    private $telefone1;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_telefone2", type="string", length=50, nullable=false)
     */
    private $telefone2;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_observacao", type="text", nullable=false)
     */
    private $observacao;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_ativo", type="boolean", nullable=false)
     */
    private $ativo;

     public function __construct() {
         
        $this->dataCadastro = new \DateTime('now');
       
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dataCadastro
     *
     * @param \DateTime $dataCadastro
     * @return Fornecedor
     */
   
    function setDataCadastro(\DateTime $dataCadastro) {
        $this->dataCadastro = $dataCadastro;
        return $this;
    }
    

    /**
     * Get dataCadastro
     *
     * @return \DateTime
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }
    

    /**
     * Set nome
     *
     * @param string $nome
     * @return Fornecedor
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set cnpj
     *
     * @param string $cnpj
     * @return Fornecedor
     */
    public function setCnpj($cnpj)
    {
        
        $this->cnpj = $cnpj;

        return $this;
    }

    /**
     * Get cnpj
     *
     * @return string
     */
    public function getCnpj()
    {
       return $this->cnpj = substr($this->cnpj, 0, 2) . '.' . substr($this->cnpj, 2, 3) . 
                '.' . substr($this->cnpj, 5, 3) . '/' . 
                substr($this->cnpj, 8, 4) . '-' . substr($this->cnpj, 12, 2);
        
    }

    /**
     * Set endereco
     *
     * @param string $endereco
     * @return Fornecedor
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;

        return $this;
    }

    /**
     * Get endereco
     *
     * @return string
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * Set numero
     *
     * @param string $numero
     * @return Fornecedor
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set bairro
     *
     * @param string $bairro
     * @return Fornecedor
     */
    public function setBairro($bairro)
    {
        $this->bairro = $bairro;

        return $this;
    }

    /**
     * Get bairro
     *
     * @return string
     */
    public function getBairro()
    {
        return $this->bairro;
    }

    /**
     * Set cep
     *
     * @param string $cep
     * @return Fornecedor
     */
    public function setCep($cep)
    {
        $this->cep = $cep;

        return $this;
    }

    /**
     * Get cep
     *
     * @return string
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * Set cidade
     *
     * @param string $cidade
     * @return Fornecedor
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;

        return $this;
    }

    /**
     * Get cidade
     *
     * @return string
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * Set telefone1
     *
     * @param string $telefone1
     * @return Fornecedor
     */
    public function setTelefone1($telefone1)
    {
        $this->telefone1 = $telefone1;

        return $this;
    }

    /**
     * Get telefone1
     *
     * @return string
     */
    public function getTelefone1()
    {
        return $this->telefone1;
    }

    /**
     * Set telefone2
     *
     * @param string $telefone2
     * @return Fornecedor
     */
    public function setTelefone2($telefone2)
    {
        $this->telefone2 = $telefone2;

        return $this;
    }

    /**
     * Get telefone2
     *
     * @return string
     */
    public function getTelefone2()
    {
        return $this->telefone2;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Fornecedor
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set observacao
     *
     * @param string $observacao
     * @return Fornecedor
     */
    public function setObservacao($observacao)
    {
        $this->observacao = $observacao;

        return $this;
    }

    /**
     * Get observacao
     *
     * @return string
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * Set ativo
     *
     * @param boolean $ativo
     * @return Fornecedor
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return boolean
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    public function __toString()
    {
        return $this->nome;
    }

    public function getLabel()
    {
        return $this->__toString();
    }

    public function toArray()
    {
        return array(
            "id" => $this->id,
            "dataCadastro" => $this->dataCadastro,
            "nome" => $this->nome,
            "cnpj" => $this->cnpj,
            "endereco" => $this->endereco,
            "numero" => $this->numero,
            "bairro" => $this->bairro,
            "cep" => $this->cep,
            "cidade" => $this->cidade,
            "telefone1" => $this->telefone1,
            "telefone2" => $this->telefone2,
            "email" => $this->email,
            "observacao" => $this->observacao,
            "ativo" => $this->ativo
        );
    }
}
