<?php

namespace Estoque\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Estorno
 *
 * @ORM\Table(name="tb_estorno_descarte", indexes={@ORM\Index(name="FK1_tb_estorno_tb_estoque", columns={"id_origem"}), @ORM\Index(name="FK2_tb_estorno_tb_produto", columns={"id_produto"}), @ORM\Index(name="FK3_tb_estorno_tb_item_descarte", columns={"id_item_descarte"})})
 * @ORM\Entity(repositoryClass="Estoque\Repository\EstornoDescarteRepository")
 */
class EstornoDescarte extends BaseEntity implements EntityInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_estorno", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_operacao", type="integer", nullable=false)
     */
    private $operacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_estorno", type="date", nullable=false)
     */
    private $dataEstorno;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_motivo", type="text", nullable=false)
     */
    private $motivo;

    /**
     * @var integer
     *
     * @ORM\Column(name="nu_qtd", type="integer", nullable=false)
     */
    private $qtd;

    /**
     * @var \Admin\Entity\Estoque
     *
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Estoque")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_origem", referencedColumnName="id_estoque")
     * })
     */
    private $estoqueOrigem;

    /**
     * @var Produto
     *
     * @ORM\ManyToOne(targetEntity="Produto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_produto", referencedColumnName="id_produto")
     * })
     */
    private $produto;

    /**
     * @var ItemDescarte
     *
     * @ORM\OneToOne(targetEntity="ItemDescarte", inversedBy="estornoDescarte")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_item_descarte", referencedColumnName="id_item_descarte")
     * })
     */
    private $itemDescarte;

    public function getId() {
        return $this->id;
    }

    public function getOperacao() {
        return $this->operacao;
    }

    public function getDataEstorno() {
        return $this->dataEstorno;
    }

    public function getMotivo() {
        return $this->motivo;
    }

    public function getQtd() {
        return $this->qtd;
    }

    public function getEstoqueOrigem() {
        return $this->estoqueOrigem;
    }

    public function getProduto() {
        return $this->produto;
    }

    public function getItemDescarte() {
        return $this->itemDescarte;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setOperacao($operacao) {
        $this->operacao = $operacao;
    }

    public function setDataEstorno(\DateTime $dataEstorno) {
        $this->dataEstorno = $dataEstorno;
    }

    public function setMotivo($motivo) {
        $this->motivo = $motivo;
    }

    public function setQtd($qtd) {
        $this->qtd = $qtd;
    }

    public function setEstoqueOrigem(\Admin\Entity\Estoque $estoqueOrigem) {
        $this->estoqueOrigem = $estoqueOrigem;
    }

    public function setProduto(Produto $produto) {
        $this->produto = $produto;
    }

    public function setItemDescarte(ItemDescarte $itemDescarte) {
        $this->itemDescarte = $itemDescarte;
    }

    public function __toString() {
        return $this->produto->getNome();
    }

    public function getLabel() {
        return $this->__toSring();
    }

    public function toArray() {
        return array(
            "id" => $this->id,
            "operacao" => $this->operacao,
            "dataEstorno" => $this->dataEstorno->format('d/m/Y'),
            "motivo" => $this->motivo,
            "qtd" => $this->qtd,
            "estoqueOrigem" => $this->estoqueOrigem,
            "produto" => $this->produto,
            "itemDescarte" => $this->itemDescarte
        );
    }

}
