<?php
namespace Estoque\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;

/**
 * Description of ContratoRepository
 *
 * @autor Evandro Noguêz Melos <evandro.noguez@jointecnologia.com>
 */
class ContratoRepository extends BaseRepository implements RepositoryInterface
{
    public function buscaContratos($filtros, $limit, $offset, $order = array(), $total = false)
    {
        try {
            $parametros = array();
            //Monta a query dql
            if ($total) {
                $dql = "SELECT count(c) total FROM Estoque\Entity\Contrato c WHERE 1=1 ";
            } else {
                $dql = "SELECT c.id
                          ,c.numContrato
                          ,f.nome
                          ,f.cnpj
                          ,c.numProcessoLicitatorio
                          ,c.ativo
                          ,c.valor - SUM(e.valor) as disponivel
                          ,SUM(e.valor) as valor
                      FROM Estoque\Entity\Contrato c 
                      JOIN c.fornecedor f
                      LEFT JOIN c.empenhos e
                      WHERE 1=1
                      ";
            }

            //Aplica os filtros
            if (!empty($filtros['busca'])) {
                $dql .= " AND (LOWER(c.numContrato) LIKE :busca
                                OR LOWER(c.numProcessoLicitatorio) LIKE :busca
                                OR LOWER(f.nome) LIKE :busca )";
                $parametros['busca'] = '%' . $filtros['busca'] . '%';
            }

            if (!$total) {
                $dql .= " GROUP BY c.id 
                               ,c.numContrato 
                               ,f.nome
                               ,c.numProcessoLicitatorio
                               ,c.ativo ";
            }

            if (count($order)) {
                $dql .= " ORDER BY {$order['field']} {$order['order']} ";
            }

            //Retorna os dados para a busca ou o total encontrado de acordo com os filtros
            if ($total) {
                $result = $this->getEntityManager()->createQuery($dql)->setParameters($parametros)->getResult();
                return $result[0]['total'];
            } else {
                //Cria e executa query
                $query = $this->getEntityManager()->createQuery($dql);
                $query->setMaxResults($limit);
                $query->setFirstResult($offset);
                $query->setParameters($parametros);
                return $query->getResult();
            }
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }


}