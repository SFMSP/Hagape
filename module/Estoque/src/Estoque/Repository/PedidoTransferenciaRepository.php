<?php

namespace Estoque\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;
use Estoque\Entity\PedidoTransferencia as PedidoTransferenciaEntity;

/**
 * Description of PedidoTransferenciaRepository
 *
 * @autor Eduardo Praxedes Heinske <eduardo.praxedes@jointecnologia.com>
 */
class PedidoTransferenciaRepository extends BaseRepository implements RepositoryInterface {

    /**
     *
     * @param array $filtros
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function busca($filtros, $limit, $offset, $order = array(), $total = false) {
        try {
            //Monta a query dql buscando os dados ou buscando o total de acordo com os filtros
            if ($total) {
                $dql = "SELECT count(pt) total FROM Estoque\Entity\PedidoTransferencia pt LEFT JOIN pt.estoqueDestino e LEFT JOIN pt.situacaoTransferencia s WHERE pt.estoqueOrigem IN (:estoques) ";
            } else {
                $dql = "SELECT pt 
                        FROM Estoque\Entity\PedidoTransferencia pt 
                        LEFT JOIN pt.estoqueDestino e 
                        LEFT JOIN pt.situacaoTransferencia s 
                        WHERE pt.estoqueOrigem IN (:estoques)  ";
            }

            $parametros['estoques'] = $filtros['estoques'];

            //Aplica os filtros
            if (!empty($filtros['busca'])) {
                $dql .= " AND (LOWER(pt.numeroTransferencia) LIKE :busca 
                                OR LOWER(pt.dataTransferencia) LIKE :busca 
                                OR LOWER(e.nome) LIKE :busca 
                                OR LOWER(s.nome) LIKE :busca) ";
                $parametros['busca'] = '%' . $filtros['busca'] . '%';
            }

            if (!empty($filtros['situacao'])) {
                $dql .= " AND s.id = :situacao ";
                $parametros['situacao'] = $filtros['situacao'];
            }

            if (count($order)) {
                $dql .= " ORDER BY {$order['field']} {$order['order']} ";
            }else{
                $dql .= " ORDER BY pt.dataTransferencia,pt.numeroTransferencia DESC ";
            }

            //Retorna os dados para a busca ou o total encontrado de acordo com os filtros
            if ($total) {
                $result = $this->getEntityManager()->createQuery($dql)->setParameters($parametros)->getResult();
                return $result[0]['total'];
            } else {

                //Cria e executa query
                $query = $this->getEntityManager()->createQuery($dql);
                $query->setMaxResults($limit);
                $query->setFirstResult($offset);
                $query->setParameters($parametros);
                return $query->getResult();
            }
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }

    /**
     *
     * @param array $filtros
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function buscaRecebimentos($filtros, $limit, $offset, $order = array(), $total = false) {
        try {
            //Monta a query dql buscando os dados ou buscando o total de acordo com os filtros
            if ($total) {
                $dql = "SELECT count(pt) total FROM Estoque\Entity\PedidoTransferencia pt JOIN pt.usuario us JOIN pt.situacaoTransferencia s LEFT JOIN pt.recebimentoTransferencia rt WHERE pt.estoqueDestino IN (".implode(',', $filtros['estoques']).") AND s.id IN (".PedidoTransferenciaEntity::STATUS_EM_TRANSITO.",".PedidoTransferenciaEntity::STATUS_RECEBIDO_INCOMPLETO.",".PedidoTransferenciaEntity::STATUS_ENCERRADO.")  ";
            } else {
                $dql = "SELECT pt
                        FROM Estoque\Entity\PedidoTransferencia pt 
                        JOIN pt.usuario us 
                        JOIN pt.situacaoTransferencia s 
                        LEFT JOIN pt.recebimentoTransferencia rt 
                        WHERE pt.estoqueDestino IN (".implode(',', $filtros['estoques']).")
                        AND s.id IN (".PedidoTransferenciaEntity::STATUS_EM_TRANSITO.",".PedidoTransferenciaEntity::STATUS_RECEBIDO_INCOMPLETO.",".PedidoTransferenciaEntity::STATUS_ENCERRADO.")
                         ";
            }
            $parametros = array();
              
            //Aplica os filtros
            if (!empty($filtros['busca'])) {
                $dql .= " AND (LOWER(pt.numeroTransferencia) LIKE :busca 
                                    OR LOWER(us.nome) LIKE :busca 
                                    OR LOWER(s.nome) LIKE :busca                                    
                                    OR LOWER(pt.dataTransferencia) LIKE :busca 
                                    OR LOWER(rt.dataRecebimentoTransferencia) LIKE :busca                                    
                               ) 
                          ";
                $parametros['busca'] = '%' . $filtros['busca'] . '%';
            }

            if (!empty($filtros['situacao'])) {
                $dql .= " AND s.id = :situacao ";
                $parametros['situacao'] = $filtros['situacao'];
            }

            if (count($order)) {
                $dql .= " ORDER BY {$order['field']} {$order['order']} ";
            }else{
                $dql .= " ORDER BY pt.dataTransferencia ";
            }

            //Retorna os dados para a busca ou o total encontrado de acordo com os filtros
            if ($total) {
                $result = $this->getEntityManager()->createQuery($dql)->setParameters($parametros)->getResult();
                return $result[0]['total'];
            } else {

                //Cria e executa query
                $query = $this->getEntityManager()->createQuery($dql);
                $query->setMaxResults($limit);
                $query->setFirstResult($offset);
                $query->setParameters($parametros);
                return $query->getResult();
            }
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }

    /**
     * Obtem o número disponível para transferência
     * 
     * @return type
     */
    public function getNumeroPedidoTransferenciaDisponivel() {
        $result = $this->getEntityManager()->createQuery('SELECT pt.id FROM Estoque\Entity\PedidoTransferencia pt ORDER BY pt.id DESC ')->setMaxResults(1)->getResult();

        return isset($result[0]['id']) ? $result[0]['id'] . "/" . date('Y') : "1/" . date('Y');
    }

    /**
     * Verifica se um pedido de transferência pertence a um grupo de estoques
     * @param integer $pedidoTransferencia
     * @param array $estoques
     * @return boolean
     */
    public function verificaPedidoTransferenciaEstoques($pedidoTransferencia, array $estoques) {
        $query = $this->getEntityManager()->createQuery('SELECT pt FROM Estoque\Entity\PedidoTransferencia pt WHERE pt.id = :pedidoTransferencia AND pt.estoqueDestino IN ('.implode(',', $estoques).') ');
        $query->setParameter('pedidoTransferencia', $pedidoTransferencia);

        $result = $query->getResult();

        return $result ? true : false;
    }

}
