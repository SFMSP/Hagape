<?php
namespace Estoque\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;

/**
 * Description of CategoriaProdutoRepository
 *
 * @autor Evandro Noguêz Melos <evandro.noguez@jointecnologia.com>
 */
class CategoriaProdutoRepository extends BaseRepository implements RepositoryInterface
{

}