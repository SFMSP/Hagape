<?php
namespace Estoque\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;

/**
 * Description of EstornoRepository
 *
 * @autor Eduardo Praxedes Heinske <eduardo.praxedes@jointecnologia.com>
 */
class EstornoRepository extends BaseRepository implements RepositoryInterface
{   
    public function getEstornosPedidoTransferencia($pedidoTransferencia){
        
        $dql = "SELECT e FROM Estoque\Entity\Estorno e JOIN e.itemTransferencia i WHERE i.pedidoTransferencia = :pedidoTransferencia ";
        
        $result = $this->getEntityManager()->createQuery($dql)->setParameter('pedidoTransferencia', $pedidoTransferencia)->getResult();
        $array = array();
        
        foreach($result as $r){
            $array[$r->getItemTransferencia()->getId()] = $r;
        }
        
        return $array;
    }
}