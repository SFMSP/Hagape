<?php

namespace Estoque\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;

/**
 * Description of RecebimentoDescarteRepository
 *
 * @autor Eduardo Praxedes Heinske <eduardo.praxedes@jointecnologia.com.br>
 */
class RecebimentoDescarteRepository extends BaseRepository implements RepositoryInterface {
    
}
