<?php

namespace Estoque\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;

/**
 * Description of RecebimentoRepository
 *
 * @author Eduardo Praxedes Heinske (eduardo.praxedes@jointecnologia.com.br)
 */
class RecebimentoRepository extends BaseRepository implements RepositoryInterface {

    /**
     * 
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function busca($filtros, $limit, $offset, $order = array(), $total = false) {
        try {
            //Monta a query dql buscando os dados ou buscando o total de acordo com os filtros
            if ($total) {
                $dql = "SELECT count(r) total FROM Estoque\Entity\Recebimento r JOIN r.usuario u WHERE r.pedido = :pedido ";
            } else {
                $dql = "SELECT r FROM Estoque\Entity\Recebimento r LEFT JOIN r.usuario u WHERE r.pedido = :pedido";
            }
            $parametros['pedido'] = $filtros['pedido'];

            //Aplica os filtros
            if (!empty($filtros['busca'])) {
                $dql .= " AND (LOWER(r.numeroNota) LIKE :busca OR LOWER(r.dataRecebimento) LIKE :busca OR LOWER(u.nome) LIKE :busca OR LOWER(r.valorTotal) LIKE :busca ) ";
                $parametros['busca'] = '%' . $filtros['busca'] . '%';
            }

            if (count($order)) {
                $dql .= " ORDER BY {$order['field']} {$order['order']} ";
            }

            //Retorna os dados para a busca ou o total encontrado de acordo com os filtros
            if ($total) {
                $result = $this->getEntityManager()->createQuery($dql)->setParameters($parametros)->getResult();
                return $result[0]['total'];
            } else {

                //Cria e executa query
                $query = $this->getEntityManager()->createQuery($dql);
                $query->setMaxResults($limit);
                $query->setFirstResult($offset);
                $query->setParameters($parametros);
                return $query->getResult();
            }
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }

}
