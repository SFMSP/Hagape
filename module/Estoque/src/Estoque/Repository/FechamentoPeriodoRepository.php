<?php


namespace Estoque\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;
use DateInterval;
use Doctrine\DBAL\Driver\PDOConnection;
use Estoque\Entity\Movimentacao;

/**
 * Class FechamentoPeriodoRepository
 * @package Estoque\Repository
 */
class FechamentoPeriodoRepository extends BaseRepository implements RepositoryInterface
{

    public function buscaPedidosRecebidosFechamentoPeriodo(\DateTime $dataFechamento)
    {
        $retorno = array();
        //Caso recebe o valor vazio ou null recebe o dia e hora atual
        if (!$dataFechamento) {
            $dataFechamento = new \DateTime('now');
        }
        $dataExecucaoRotina = $dataFechamento->format('Y-m-d');
        $diaAtual = $dataFechamento->format('d');
        $mesAtual = $dataFechamento->format('m');
        $AnoAtual = $dataFechamento->format('Y');

        //Calculo para pegar o primeiro dia do mes anterior da qual está rodando a rotina
        //A rotina está programa para rodar dia 04 de cada mês
        $dataInicioPeriodoAnterior = $dataFechamento->sub(new DateInterval("P1M" . ($diaAtual - 1) . "D"))->format('Y-m-d');
        $dataFinalPeriodoAnterior = $dataFechamento->add(new DateInterval("P1M"));
        $dataFinalPeriodoAnterior = $dataFinalPeriodoAnterior->sub(new DateInterval("P1D"))->format('Y-m-d');

        //Consulta para retornar pedidos com recebimento dentro do periodo de movimentacao
        $stSql = "SELECT movimentacao.*
                        ,((valor_estoque_anterior+valor_total_entrada_produto)/quantidade_produto_estoque) AS valor_preco_medio_atual
                  FROM (
                      SELECT
                          tb_movimentacao.id_produto AS produto
                          ,tb_produto.nu_qtd_atual - tb_movimentacao.nu_qtd AS quantidade_anterior_produto
                          ,tb_movimentacao.nu_qtd AS quantidade_total_recebida_produto
                          ,tb_produto.nu_qtd_atual AS quantidade_produto_estoque
                          ,tb_produto.nu_preco_medio_atual AS preco_medio_anterior
                          ,((tb_produto.nu_qtd_atual - tb_movimentacao.nu_qtd) * tb_produto.nu_preco_medio_atual) AS valor_estoque_anterior
                          ,tb_movimentacao.nu_qtd * tb_item_pedido.nu_preco_unitario AS valor_total_entrada_produto
                          ,'".$dataInicioPeriodoAnterior."' as data_inicial_periodo
                          ,'".$dataFinalPeriodoAnterior."' as data_final_periodo
                          ,'".$dataExecucaoRotina."' as data_execucao_fechamento

                      FROM tb_movimentacao

                      JOIN tb_produto
                        ON tb_produto.id_produto = tb_movimentacao.id_produto

                      JOIN tb_item_pedido
                        ON tb_item_pedido.id_produto = tb_produto.id_produto

                      WHERE tb_movimentacao.id_tipo_movimentacao = 1
                      AND tb_movimentacao.id_acao_movimentacao = 1
                      AND tb_movimentacao.dt_movimentacao BETWEEN '" . $dataInicioPeriodoAnterior . "' AND '" . $dataFinalPeriodoAnterior . "'
                      GROUP BY 1
                  )AS movimentacao
                ";
        try {
            $connection = $this->getEntityManager()->getConnection();
            if ($connection){
                $stmt = $connection->query($stSql);
                return $stmt->fetchAll();
            }

        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }

    }


    public function buscaMovimentacaoSaidaInicioPeriodo(\DateTime $dataExecucao,$idProduto)
    {
        $dtExecucaoFechamento = $dataExecucao->format('Y-m-d');
        $dia = $dataExecucao->format('d');
        $dtInicioMesPeriodoExecucao = $dataExecucao->sub(new DateInterval("P" . ($dia-1) . "D"))->format('Y-m-d');

        $stSql = "SELECT id_movimentacao as id
                        ,id_produto 
                        ,nu_qtd
                  FROM sfmsp.tb_movimentacao 
                  WHERE id_acao_movimentacao = ".Movimentacao::ACAO_MOVIMENTACAO_TRANSFERENCIA."
                  AND id_tipo_movimentacao = ".Movimentacao::TIPO_MOVIMENTACAO_SAIDA."
                  AND id_produto = ".$idProduto."                  
                  AND dt_movimentacao 
                    BETWEEN '" . $dtInicioMesPeriodoExecucao . "' 
                        AND '" . $dtExecucaoFechamento . " 23:59:59';
        ";

        try {
            $connection = $this->getEntityManager()->getConnection();
            if ($connection){
                $stmt = $connection->query($stSql);
                return $stmt->fetchAll();
            }

        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }

    }



}