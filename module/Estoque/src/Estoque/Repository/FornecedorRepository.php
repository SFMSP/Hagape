<?php
namespace Estoque\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;

  
/**
 * Description of FornecedorRepository
 *
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class FornecedorRepository extends BaseRepository implements RepositoryInterface {
    
      /**
     * 
     * @param array $filtros
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    
      public function buscaFornecedor($filtros, $limit, $offset, $order = array(), $total = false) {
        try {
            //Monta a query dql
           if ($total) {
               $dql = "SELECT count(f) total FROM Estoque\Entity\Fornecedor f  WHERE 1 = 1";
               
           }else{
               
               $dql = "SELECT f FROM Estoque\Entity\Fornecedor f  WHERE 1 = 1";
               
           }
           
          
           
          
          if(is_numeric($filtros['busca'])){
              /*se for numerico cai aqui*/
                $filtros['busca'] = $filtros['busca'];
             
              
          }else {
         
                $filtros['busca'] = str_replace('.', '', $filtros['busca']);
                $filtros['busca'] = str_replace('/', '', $filtros['busca']);
                $filtros['busca'] = str_replace('-', '', $filtros['busca']);
         
          }
           
            $parametros = array();
           
           
            if (!empty($filtros['busca'])) {
                $dql .= " AND (LOWER(f.nome) LIKE :busca ) "
                        . "OR (LOWER(f.cnpj) LIKE :busca ) ";
                $parametros['busca'] = '%' . $filtros['busca'] . '%';
            }
           
            if (count($order)) {
                $dql .= " ORDER BY f.{$order['field']} {$order['order']} ";
            }
            
        
            //Retorna os dados para a busca ou o total encontrado de acordo com os filtros
            if ($total) {
                $result = $this->getEntityManager()->createQuery($dql)->setParameters($parametros)->getResult();
                return $result[0]['total'];
            } else {
                //Cria e executa query
                $query = $this->getEntityManager()->createQuery($dql);
                $query->setMaxResults($limit);
                $query->setFirstResult($offset);
                $query->setParameters($parametros);
                return $query->getResult();
            }
                
          
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }

   
    public function buscaFornecedorCnpj($filtros) {
      
        try {
            //Monta a query dql
          
                $filtros['cnpj'] = str_replace('.', '', $filtros['cnpj']);
                $filtros['cnpj'] = str_replace('/', '', $filtros['cnpj']);
                $filtros['cnpj'] = str_replace('-', '', $filtros['cnpj']);
         
            
            //print_r($filtros);die;
            $dql = "SELECT f FROM Estoque\Entity\Fornecedor f WHERE ";
            
            $parametros = array();

            
            if (!empty($filtros['cnpj'])) {
                $dql .= " f.cnpj = :cnpj ";
                $parametros['cnpj'] = $filtros['cnpj'];
            }
           
            $limit =1;
           //Cria e executa query
            $query = $this->getEntityManager()->createQuery($dql);
            $query->setMaxResults($limit);
            $query->setParameters($parametros);

            return $query->getResult();
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }
    public function buscaFornecedorExistente($filtros) {
      
        try {
            //Monta a query dql
            
            //print_r($filtros);die;
            $dql = "SELECT f FROM Estoque\Entity\Fornecedor f WHERE ";
            
            $parametros = array();

            
           
            if (!empty($filtros['nome'])) {
                $dql .= " f.nome = :nome ";
                $parametros['nome'] = $filtros['nome'];
            }
           
            $limit =1;
           //Cria e executa query
            $query = $this->getEntityManager()->createQuery($dql);
            $query->setMaxResults($limit);
            $query->setParameters($parametros);

            return $query->getResult();
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }
    public function buscaIdFornecedor($filtros) {
      
        try {
            //Monta a query dql
            
            //print_r($filtros);die;
            $dql = "SELECT f FROM Estoque\Entity\Fornecedor f WHERE ";
            
            $parametros = array();

            
           
            if (!empty($filtros['id'])) {
                $dql .= " f.id = :id ";
                $parametros['id'] = $filtros['id'];
            }
           
            $limit =1;
           //Cria e executa query
            $query = $this->getEntityManager()->createQuery($dql);
            $query->setMaxResults($limit);
            $query->setParameters($parametros);

            return $query->getResult();
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }
    

}
