<?php

namespace Estoque\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;

/**
 * Description of PedidoRepository
 *
 * @autor Evandro Noguêz Melos <evandro.noguez@jointecnologia.com>
 */
class PedidoRepository extends BaseRepository implements RepositoryInterface
{

    /**
     *
     * @param array $filtros
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function busca($filtros, $limit, $offset, $order = array(), $total = false)
    {
        try {
            //Monta a query dql buscando os dados ou buscando o total de acordo com os filtros
            if ($total) {
                $dql = "SELECT count(p) total 
                        FROM Estoque\Entity\Pedido p 
                        LEFT JOIN p.empenho e  
                        LEFT JOIN p.fornecedor f 
                        LEFT JOIN p.situacao s 
                        WHERE 1 = 1 ";
            } else {
                $dql = "SELECT p 
                        FROM Estoque\Entity\Pedido p 
                        LEFT JOIN p.empenho e  
                        LEFT JOIN p.fornecedor f 
                        LEFT JOIN p.situacao s WHERE 1 = 1 ";
            }
            $parametros = array();

            //Aplica os filtros
            if (!empty($filtros['busca'])) {
                $dql .= " AND (LOWER(p.numeroPedido) LIKE :busca 
                                OR LOWER(e.numEmpenho) LIKE :busca 
                                OR LOWER(p.dataSolicitacao) LIKE :busca 
                                OR LOWER(f.nome) LIKE :busca 
                                OR LOWER(s.nome) LIKE :busca) ";
                $parametros['busca'] = '%' . $filtros['busca'] . '%';
            }

            if (!empty($filtros['situacao'])) {
                $dql .= " AND s.id = :situacao ";
                $parametros['situacao'] = $filtros['situacao'];
            }

            if (count($order)) {
                $dql .= " ORDER BY {$order['field']} {$order['order']} , p.dataSolicitacao DESC  ";
            }else{
                $dql .= " ORDER BY p.dataSolicitacao,p.numeroPedido DESC  ";
            }

            //Retorna os dados para a busca ou o total encontrado de acordo com os filtros
            if ($total) {
                $result = $this->getEntityManager()->createQuery($dql)->setParameters($parametros)->getResult();
                return $result[0]['total'];
            } else {

                //Cria e executa query
                $query = $this->getEntityManager()->createQuery($dql);
                $query->setMaxResults($limit);
                $query->setFirstResult($offset);
                $query->setParameters($parametros);
                return $query->getResult();
            }
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }

    /**
     * Obtem o número disponível para pedido
     *
     * @return type
     */
    public function getNumeroPedidoDisponivel()
    {
        $result = $this->getEntityManager()->createQuery('SELECT p.id FROM Estoque\Entity\Pedido p ORDER BY p.id DESC ')->setMaxResults(1)->getResult();

        return isset($result[0]['id']) ? $result[0]['id'] . "/" . date('Y') : "1/" . date('Y');
    }

}
