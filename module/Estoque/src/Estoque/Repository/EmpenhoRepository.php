<?php

namespace Estoque\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;

/**
 * Description of EmpenhoRepository
 *
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class EmpenhoRepository extends BaseRepository implements RepositoryInterface
{

    /**
     * Retorna o valor atual de empenho disponível
     *
     * @param integer $empenho
     * @return float
     */
    public function getEmpenhoDisponivelAtual($empenho, $pedidoRetirado = false)
    {

        //Inicia o valor como zero
        $valor = 0;

        //Monta a query dql
        $dql = "SELECT e.valor, SUM(it.precoUnitario * it.qtd) somaPrecos FROM Estoque\Entity\Empenho e LEFT JOIN e.pedidos p LEFT JOIN p.itensPedido it WHERE e.id = :id AND p.situacao <>'5' ";

        if ($pedidoRetirado) {
            $dql .= " AND p.id != {$pedidoRetirado} ";
        }

        //Solicita o resultado da consulta
        $result = $this->getEntityManager()->createQuery($dql)->setParameter('id', $empenho)->getResult();

        //Verifica se existe resultado
        if ($result) {

            //Realiza o calculo do empenho restante
            $valorEmpenho = $result[0]['valor'];
            $somaPrecos = $result[0]['somaPrecos'] ? $result[0]['somaPrecos'] : 0;
            $valor = $valorEmpenho - $somaPrecos;
        }

        //Retorna o valor
        return $valor ? $valor : 0;
    }

    public function getEmpenhosPorContrato($contrato)
    {
        $empenhos = array();
        $result = $this->findBy(["contrato" => $contrato]);
        if ($result) {
            foreach ($result as $key => $value) {
                $aux = $value->toArray();
                $empenhos[$key+1]['empenho'] = $aux['id'];
                $empenhos[$key+1]['numEmpenho'] = $aux['numEmpenho'];
                $empenhos[$key+1]['dataEmpenho'] = $aux['dataEmpenho']->format('d/m/Y');
                $empenhos[$key+1]['hiddenValorEmpenho'] = $aux['valor'];
                $empenhos[$key+1]['valorEmpenho'] = number_format($aux['valor'],2,',','.');
                $empenhos[$key+1]['provisorios'] = $aux['provisorio'] ? 'true' : 'false';
                $empenhos[$key+1]['observacao'] = $aux['observacao'];
            }
        }

        return $empenhos ? $empenhos : [];
    }

}
