<?php

namespace Estoque\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;

/**
 * Description of EstoqueProdutoRepository
 *
 * @autor Eduardo Praxedes Heinske <eduardo.praxedes@jointecnologia.com>
 */
class EstoqueProdutoRepository extends BaseRepository implements RepositoryInterface {

    /**
     * Retorna todos os produtos disponíveis em um estoque
     * 
     * @param integer $estoque
     * @return array
     */
    public function getProdutosEstoque($estoque) {

        $dql = "SELECT pe FROM Estoque\Entity\EstoqueProduto pe INNER JOIN pe.produto p WHERE pe.estoque = :estoque ORDER BY p.codigo, p.nome ASC ";
        $parametros['estoque'] = $estoque;

        $result = $this->getEntityManager()->createQuery($dql)->setParameters($parametros)->getResult();
        $result = $result ? $result : array();

        $produtos = array();

        foreach ($result as $r) {
            $produtos[] = $r->getProduto();
        }

        return $produtos;
    }

    /**
     * Retorna a quantidade disponível de um produto dentro de um estoque
     * 
     * @param integer $produto
     * @param integer $estoque
     * @return integer
     */
    public function getQuantidadeDisponivelEstoque($produto, $estoque) {
        $dql = "SELECT ep.qtd FROM Estoque\Entity\EstoqueProduto ep WHERE ep.produto = :produto AND ep.estoque = :estoque ";
        $parametros['produto'] = $produto;
        $parametros['estoque'] = $estoque;

        $result = $this->getEntityManager()->createQuery($dql)->setParameters($parametros)->getResult();

        return isset($result[0]['qtd']) ? $result[0]['qtd'] : 0;
    }

}
