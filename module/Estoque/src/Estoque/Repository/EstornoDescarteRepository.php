<?php
namespace Estoque\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;

/**
 * Description of EstornoDescarteRepository
 *
 * @autor Eduardo Praxedes Heinske <eduardo.praxedes@jointecnologia.com>
 */
class EstornoDescarteRepository extends BaseRepository implements RepositoryInterface
{   
    public function getEstornosSolicitacaoDescarte($solicitacaoDescarte){
        
        $dql = "SELECT e FROM Estoque\Entity\EstornoDescarte e JOIN e.itemDescarte i WHERE i.solicitacaoDescarte = :solicitacaoDescarte ";
        
        $result = $this->getEntityManager()->createQuery($dql)->setParameter('solicitacaoDescarte', $solicitacaoDescarte)->getResult();
        $array = array();
        
        foreach($result as $r){
            $array[$r->getItemDescarte()->getId()] = $r;
        }
        
        return $array;
    }
}