<?php
namespace Estoque\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;

/**
 * Description of ProdutoRepository
 *
 * @autor Evandro Noguêz Melos <evandro.noguez@jointecnologia.com>
 */
class  ProdutoRepository extends BaseRepository implements RepositoryInterface
{

    public function buscaUrna($idConvenio = '')
    {
        $stSql = "SELECT
                        tb_produto.id_produto AS id
                        ,CONCAT(tb_classificacao_venda.txt_nome,' - ', tb_unidade_medida.txt_medida,' - ', tb_produto.txt_codigo) AS nome
                        ,tb_classificacao_venda.id_classificacao_venda AS classificacaoVenda
                  FROM tb_produto
                  INNER JOIN tb_classificacao_venda
                    ON tb_classificacao_venda.id_produto = tb_produto.id_produto
                  INNER JOIN tb_unidade_medida
                    ON tb_unidade_medida.id_unidade_medida = tb_produto.id_medida
                  ";
        if ($idConvenio) {
            $stSql .= " INNER JOIN tb_convenio_tb_produto
	                                   ON tb_convenio_tb_produto.id_produto = tb_produto.id_produto ";
        }
        $stSql .= " WHERE id_categoria = 4
                    ORDER BY tb_classificacao_venda . txt_nome ASC ";

        try {
            $connection = $this->getEntityManager()->getConnection();
            if ($connection) {
                $stmt = $connection->query($stSql);
                //Percorre e organiza os resultados
                return $stmt->fetchAll();
            }

        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }

    }

    public function buscaProdutoPorCategoria($idCategoria)
    {
        $stSql = "SELECT
                        tb_produto.id_produto AS id
                        ,CONCAT(tb_produto.txt_nome,' - ', tb_unidade_medida.txt_medida,' - ', tb_produto.txt_codigo) AS nome
                  FROM tb_produto
                  INNER JOIN tb_unidade_medida
                    ON tb_unidade_medida.id_unidade_medida = tb_produto.id_medida
                  ";
        if ($idCategoria) {
            $stSql .= "WHERE id_categoria = " . $idCategoria . " ";
        }
        $stSql .= "ORDER BY tb_produto . txt_nome ASC ";

        try {
            $connection = $this->getEntityManager()->getConnection();
            if ($connection) {
                $stmt = $connection->query($stSql);
                //Percorre e organiza os resultados
                return $stmt->fetchAll();
            }

        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }


    public function buscaDadosProduto($produto = '', $idClassificacaoVenda = '')
    {
        if ($produto) {
            $stSql = "SELECT 
                        tb_unidade_medida . txt_medida
                        ,tb_produto . nu_preco_medio_atual AS valor
                        ,((tb_produto . nu_qtd_atual - tb_produto . nu_qtd_reservada) - tb_estoque_produto . nu_qtd_reservada) AS quantidadeDisponivel
                        ,COALESCE(tb_robin_hood.fator,1) as fator
                  FROM tb_produto
                  INNER JOIN tb_unidade_medida
	                ON tb_unidade_medida . id_unidade_medida = tb_produto . id_medida
                  INNER JOIN tb_estoque_produto
	                ON tb_estoque_produto . id_produto = tb_produto . id_produto
	              LEFT JOIN tb_classificacao_venda
	                ON tb_classificacao_venda.id_produto = tb_produto.id_produto
                  LEFT JOIN tb_robin_hood
	                ON tb_robin_hood.id_classificacao_venda = tb_classificacao_venda.id_classificacao_venda
                  WHERE tb_produto . bool_ativo = 1
                  AND tb_produto . id_produto = " . $produto . "
                ";
            if ($idClassificacaoVenda) {
                $stSql .= "\n AND tb_classificacao_venda.id_classificacao_venda = " . $idClassificacaoVenda . " ";
            }

            try {
                $connection = $this->getEntityManager()->getConnection();
                if ($connection) {
                    $stmt = $connection->query($stSql);
                    //Percorre e organiza os resultados
                    return $stmt->fetchAll();
                }

            } catch (\Exception $ex) {
                $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            }
        } else {
            return array();
        }
    }

    public function buscaCategoriasEnfeite($idCategoriaPai = '')
    {
        if ($idCategoriaPai) {
            $stSql = "
                SELECT tb_categoria_produto.id_categoria AS id 
                      ,tb_categoria_produto.txt_nome  AS nome
                FROM tb_categoria_produto 
                WHERE id_categoria_pai = " . $idCategoriaPai . ";
                ";
            try {
                $connection = $this->getEntityManager()->getConnection();
                if ($connection) {
                    $stmt = $connection->query($stSql);
                    //Percorre e organiza os resultados
                    return $stmt->fetchAll();
                }

            } catch (\Exception $ex) {
                $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            }
        } else {
            return [];
        }
    }

    public function buscaProdutoEnfeite($idCategoria = '')
    {
        if ($idCategoria) {
            $stSql = "
                SELECT 
                        CONCAT(tb_produto.txt_nome,' - ',tb_produto.txt_codigo) AS nome
                        ,tb_produto . id_produto AS id
                        ,tb_unidade_medida . txt_medida AS medida
                        ,tb_produto . nu_preco_medio_atual AS valor
                        ,((tb_produto . nu_qtd_atual - tb_produto . nu_qtd_reservada) - tb_estoque_produto . nu_qtd_reservada) AS quantidadeDisponivel
                        ,COALESCE(tb_robin_hood.fator,1) as fator
                  FROM tb_produto
                  INNER JOIN tb_unidade_medida
	                ON tb_unidade_medida . id_unidade_medida = tb_produto . id_medida
                  INNER JOIN tb_estoque_produto
	                ON tb_estoque_produto . id_produto = tb_produto . id_produto
	              LEFT JOIN tb_robin_hood
	                ON tb_robin_hood.id_categoria = tb_produto.id_categoria
                  WHERE tb_produto . bool_ativo = 1
                  AND tb_produto.id_categoria = " . $idCategoria . "
                ";
            try {
                $connection = $this->getEntityManager()->getConnection();
                if ($connection) {
                    $stmt = $connection->query($stSql);
                    //Percorre e organiza os resultados
                    return $stmt->fetchAll();
                }

            } catch (\Exception $ex) {
                $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            }
        } else {
            return [];
        }
    }




}//END OF CLASS