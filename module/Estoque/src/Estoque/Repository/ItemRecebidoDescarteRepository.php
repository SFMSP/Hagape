<?php

namespace Estoque\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;

/**
 * Description of ItemRecebidoDescarteRepository
 *
 * @autor Eduardo Praxedes Heinske <eduardo.praxedes@jointecnologia.com.br>
 */
class ItemRecebidoDescarteRepository extends BaseRepository implements RepositoryInterface {
    
}
