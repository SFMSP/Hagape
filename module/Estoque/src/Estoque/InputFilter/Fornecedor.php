<?php

namespace Estoque\InputFilter;

use Base\Validator\BaseValidator;

/**
 * Classe que faz as validações do formulário de usuários
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Fornecedor extends BaseValidator {

    /**
     * Monta os filtros e as validações
     */
    public function __construct() {
        //Monta as validações de campos obrigatórios padrão
        $obrigatoriosPadrao = array(
            "nome" => "Campo obrigatório",
            "cnpj" => "Campo obrigatório",
            "endereco" => "Campo obrigatório",
            "numero" => "Campo obrigatório",
            "bairro" => "Campo obrigatório",
            "cep" => "Campo obrigatório",
            "estado" => "Campo obrigatório",
            "cidade" => "Campo obrigatório",
            "telefone1" => "Campo obrigatório",
            "email" => "Campo obrigatório",
            "ativo"=>"Campo obrigatório"
        );

        $this->addEmptyValidators($obrigatoriosPadrao);
    }
    

}
