<?php

namespace Estoque\InputFilter;

use Base\Validator\BaseValidator;

/**
 * Classe que faz as validações do formulário de transferencia
 *
 * @autor Eduardo Praxedes Heinske <eduardo.praxedes@jointecnologia.com.br>
 */
class Transferencia extends BaseValidator {

    /**
     * Monta os filtros e as validações
     */
    public function __construct() {
        //Monta as validações de campos obrigatórios padrão
        $obrigatoriosPadrao = array(
            "estoqueDestino" => "Campo Obrigatório",
            "dataTransferencia" => "Campo Obrigatório"
        );

        $this->addEmptyValidators($obrigatoriosPadrao);
    }

}
