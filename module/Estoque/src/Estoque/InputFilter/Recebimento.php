<?php

namespace Estoque\InputFilter;

use Base\Validator\BaseValidator;
use Zend\InputFilter\FileInput;
use Zend\Filter\File\RenameUpload;
use Zend\Validator\File\Size;
use Zend\Validator\File\MimeType ;
/**
 * Classe que faz as validações do formulário de Recebimento
 *
 * @autor Evandro Noguez Melos <evandro.noguez@jointecnologia.com.br>
 */
class Recebimento extends BaseValidator {

    /**
     * Monta os filtros e as validações
     */
    public function __construct() {

        $camposObrigatorios = array(
            "dataRecebimento" => "Campo Obrigatório",
            "numeroNota" => "Campo Obrigatório"
        );

        $this->addEmptyValidators($camposObrigatorios);

        $notaFiscal = new FileInput('notaFiscal');
        $notaFiscal->setRequired(false);
        $notaFiscal->getFilterChain()->attach(new RenameUpload(array(
            'target' => "./tmp/nota_recebimento_",
            'randomize' => true,
            'use_upload_extension' => true,
            'use_upload_name' => true
        )));

        $uploadMaxSize = substr(ini_get('upload_max_filesize'), 0, -1) . "MB";

         //EXEMPLO DE VALIDAÇÕES ÚTEIS
        $notaFiscal->getValidatorChain()->attach(new Size(array('max' => $uploadMaxSize, 'message' => 'Imagem ultrapassa os limites de ' . $uploadMaxSize . ' permitidos')));


        $this->add($notaFiscal);
    }

}
