<?php

namespace Estoque\InputFilter;

use Base\Validator\BaseValidator;

/**
 * Classe que faz as validações do formulário de decarte
 *
 * @autor Eduardo Praxedes Heinske <eduardo.praxedes@jointecnologia.com.br>
 */
class Descarte extends BaseValidator {

    /**
     * Monta os filtros e as validações
     */
    public function __construct() {
        //Monta as validações de campos obrigatórios padrão
        $obrigatoriosPadrao = array(
            "dataDescarte" => "Campo Obrigatório"
        );

        $this->addEmptyValidators($obrigatoriosPadrao);
    }

}
