<?php

namespace Estoque\InputFilter;

use Base\Validator\BaseValidator;

/**
 * Classe que faz as validações do formulário de Pedido
 *
 * @autor Evandro Noguez Melos <evandro.noguez@jointecnologia.com.br>
 */
class Pedido extends BaseValidator {

    /**
     * Monta os filtros e as validações
     */
    public function __construct() {
        //Monta as validações de campos obrigatórios padrão
        $obrigatoriosPadrao = array(
            "ofn" => "Campo obrigatório",
            "contrato" => "Campo obrigatório",
            "fornecedor" => "Campo obrigatório",
            "empenho" => "Campo obrigatório"
        );

        $this->addEmptyValidators($obrigatoriosPadrao);
    }

}
