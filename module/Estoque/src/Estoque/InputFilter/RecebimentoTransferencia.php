<?php

namespace Estoque\InputFilter;

use Base\Validator\BaseValidator;

/**
 * Classe que faz as validações do formulário de Recebimento Transferencia
 *
 * @autor Eduardo Praxedes Heinske <eduardo.praxedes@jointecnolonogia.com>
 */
class RecebimentoTransferencia extends BaseValidator {

    /**
     * Monta os filtros e as validações
     */
    public function __construct() {

        $camposObrigatorios = array(
            "dataRecebimento" => "Campo Obrigatório"
        );

        $this->addEmptyValidators($camposObrigatorios);
    }

}
