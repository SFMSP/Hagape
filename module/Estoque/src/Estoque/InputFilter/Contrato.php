<?php

namespace Estoque\InputFilter;

use Base\Validator\BaseValidator;

/**
 * Classe que faz as validações do formulário de Contrato
 *
 * @autor Evandro Noguez Melos <evandro.noguez@jointecnologia.com.br>
 */
class Contrato extends BaseValidator
{

    /**
     * Monta os filtros e as validações
     */
    public function __construct()
    {
        //Monta as validações de campos obrigatórios padrão
        $obrigatoriosPadrao = array(
            "fornecedor" => "Campo obrigatório",
            "numContrato" => "Campo obrigatório",
            "numProcessoLicitatorio" => "Campo obrigatório",
            "valor" => "Campo obrigatório",
        );

        $this->addEmptyValidators($obrigatoriosPadrao);
    }

}
