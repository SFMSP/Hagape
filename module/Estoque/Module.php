<?php

namespace Estoque;

use Base\Module\BaseModule;
use Estoque\Container\Container;

class Module extends BaseModule {

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    /**
     * Monta o container de serviços a partir da classe Container desse módulo
     * @return |Base\Module\type
     */
    public function getServiceConfig() {
        return parent::composeServiceConfig(new Container());
    }

}
