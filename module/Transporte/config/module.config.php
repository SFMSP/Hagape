<?php

return array(
    'router' => array(
        'routes' => array(
            'transporte' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/transporte',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Transporte\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        )
                    )
                )
            )
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Transporte\Controller\Index' => 'Transporte\Controller\IndexController'
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Transporte' => __DIR__ . '/../view',
        )
    )
);
