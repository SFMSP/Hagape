<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Application\Container\Container;
use Base\Module\BaseModule;

class Module extends BaseModule {

    public function onBootstrap(MvcEvent $e) {

        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($e->getApplication()->getEventManager());

        //Toda vez que um controller for instanciado
        $e->getApplication()->getEventManager()->getSharedManager()->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function(MvcEvent $e) {
            //Busca o nome do controller
            $controller = $e->getTarget();
            
            $controllerClass = get_class($controller);
            $controllerArray = explode("\\", $controllerClass);
            $controllerName = $controllerArray[2];

            //Armazena o namespace do controller
            $controllerNamespace = $controllerArray[0];

            //Busca as configurações
            $config = $e->getApplication()->getServiceManager()->get('config');
           
            //Busca os dados da rota
            $route = $e->getTarget()->getEvent()->getRouteMatch()->getParams();
  
            //Envia o controlle e action atual para o layout
            $viewModel = $e->getApplication()->getMvcEvent()->getViewModel();
              
            if (array_key_exists('__CONTROLLER__',$route)){
                $viewModel->controller = $route['__CONTROLLER__'];
            }else{
                $viewModel->controller = $route['controller'];
            }
            $viewModel->action = $route['action'];
            $viewModel->baseUrlPath = $config['baseUrlPath'];

            //Valida a autenticação
            if (!$this->validaAuth($controllerNamespace, $controllerName) && $controllerName !== 'FechamentoPeriodoController') {
                return $controller->redirect()->toRoute('home', array('controller' => 'login', 'action' => 'index'));
            }

            //Verifica se é necessário fazer a verificação
            if ($controllerNamespace == "Application" AND ( $controllerName !== "IndexController" AND $controllerName !== "LembrarSenhaController" AND $controllerName !== 'FechamentoPeriodoController')) {

                //Faz a validação do permissionamento
                $this->validaPermissionamento($e);
            }

            if($controllerName !== 'FechamentoPeriodoController'){
                //Realiza a mudança do layout
                $this->mudarLayout($controller, $controllerName, $config);

                //
                $iterator = new \RecursiveIteratorIterator($e->getApplication()->getServiceManager()->get('navigation'), \RecursiveIteratorIterator::SELF_FIRST);
              
                foreach ($iterator as $page) {
                                   
                    if ($page->getController() == $route['__CONTROLLER__'] && $page->getAction() == $route['action']) {
                      
                        $page->setClass('active open');

                        while ($page = $page->getParent()) {
                            
                            if ($page instanceof \Zend\Navigation\Navigation) {
                                 
                                break;
                            }
                            $page->setClass('active open');
                        }
                        break;
                    }
                }
            }

        }, 98);
    }

    /**
     * Método responsável por fazer a verificação de permissionamento no acesso a cada página do sistema
     * @param \Zend\Mvc\MvcEvent $e
     * @return mixed
     */
    public function validaPermissionamento(MvcEvent $e) {
        //Objeto que contém os atributos da rota
        $route = $e->getTarget()->getEvent()->getRouteMatch();
        
        //Verifica se a rota é permissionada
        if ($route->getMatchedRouteName() == 'home/default') {

            //Busca os dados do usuário 
            $sessionStorage = new \Zend\Authentication\Storage\Session('Application');
            $dadosUser = $sessionStorage->read();

            //Busca os dados da rota
            $dadosRota = $route->getParams();
            $controller = $dadosRota['controller'];
            $action = $dadosRota['action'] == 'formulario' ? isset($dadosRota['id']) ? 'editar' : 'incluir' : $dadosRota['action'];

            //Instancia e ACL
            $acl = new \Application\Security\Acl($dadosUser['perfil'], $dadosUser['privilegios']);
            
            //Verifica a existência do resource
            if ($acl->hasResource($controller) && isset($dadosUser['privilegios'][$controller][$action])) {

                //Verifica permissão
                if (!$acl->isAllowed($dadosUser['perfil'], $controller, $action)) {
                    return $e->getTarget()->redirect()->toRoute("home/default", array('controller' => 'login'));
                }
            }
        }
    }

    /**
     * Método que valida se o usuário está logado
     * 
     * @param string $controllerNamespace
     * @param string $controllerName
     * @return mixed
     */
    public function validaAuth($controllerNamespace, $controllerName) {

        //Cria um objeto da sessionStorage com o nome da sessão Application, que é a sessão criada na autenticação
        $sessionStorage = new \Zend\Authentication\Storage\Session('Application');

        //Instancia o serviço de autenticação do ZF2 passando essa sessão
        $auth = new \Zend\Authentication\AuthenticationService();
        $auth->setStorage($sessionStorage);

        //Verifica se o usuário está logado e se o controler que ele está tentando acessar é um controle permissionado, ou seja, diferente do controller de login ou lembrar senha
        if (!$auth->hasIdentity() and ( $controllerNamespace . "\\Controller\\" . $controllerName !== "Application\\Controller\\IndexController" && $controllerNamespace . "\\Controller\\" . $controllerName !== "Application\\Controller\\LembrarSenhaController" )) {
            return false;
            
        } else if ($controllerNamespace . "\\Controller\\" . $controllerName !== "Application\\Controller\\IndexController" && $controllerNamespace . "\\Controller\\" . $controllerName !== "Application\\Controller\\LembrarSenhaController"){
            
            $identity = $sessionStorage->read();
            
            if($identity['possuiFiltros']){
                foreach($identity['filtros'] as $filtro){
                    if(!isset($identity['filtrosSelecionados'][$filtro])){
                        return false;
                    }
                }
            }
            
           if(!$identity['trocaSenha']){
                return true;
            }else{
                return false;
            }
           
        }else{
            return true;
        }
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    /**
     * Método que realiza a mudança de layout de acordo com o namespace do módulo
     * 
     * @param string $controller
     * @param string $controllerName
     * @param array $config
     */
    public function mudarLayout($controller, $controllerName, $config) {
        //Direciona o layout
        if ($controllerName == 'IndexController' || $controllerName == 'LembrarSenhaController') {
            $controller->layout($config['module_layouts']['login']);
        } else {
            $controller->layout($config['module_layouts']['principal']);
        }
    }

    /**
     * Monta o container de serviços a partir da classe Container desse módulo
     * @return array
     */
    public function getServiceConfig() {
        return parent::composeServiceConfig(new Container());
    }

    /**
     * Chama todos os helpers de view
     * 
     * @return array
     */
    public function getViewHelperConfig() {
        return array(
            'factories' => array(
                'User' => function ($sm) {
                    return new \Application\View\Helper\User();
                },
                'Acl' => function ($sm) {
                    return new \Application\View\Helper\Acl();
                },
                'MyRoute' => function ($sm) {
                    return new \Application\View\Helper\MyRoute();
                }
            )
        );
    }

}
