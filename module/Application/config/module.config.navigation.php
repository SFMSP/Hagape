<?php

return array(
    array(
        'label' => 'Configuração',
        'controller' => 'home',
        'action' => 'index',
        'route' => 'home/default',
        'pages' => array(
            array(
                'label' => 'Usuário',
                'controller' => 'usuario',
                'action' => 'index',
                'route' => 'home/default',
                'pages' => array(
                    array(
                        'label' => 'Cadastro',
                        'controller' => 'usuario',
                        'action' => 'formulario'
                    )
                )
            ),
            array(
                'label' => 'Perfil',
                'controller' => 'perfil',
                'action' => 'index',
                'route' => 'home/default',
                'pages' => array(
                    array(
                        'label' => 'Cadastro',
                        'controller' => 'perfil',
                        'action' => 'formulario'
                    ),
                    array(
                        'label' => 'Meu perfil',
                        'controller' => 'perfil',
                        'action' => 'config'
                    )
                )
            ),
            array(
                'label' => 'Agência',
                'controller' => 'agencia',
                'action' => 'index',
                'route' => 'admin/default',
                'pages' => array(
                    array(
                        'label' => 'Cadastro',
                        'controller' => 'agencia',
                        'action' => 'formulario'
                    )
                )
            ),
            array(
                'label' => 'Cemitério/Crematório',
                'controller' => 'cemiterio',
                'action' => 'index',
                'route' => 'admin/default',
                'pages' => array(
                    array(
                        'label' => 'Cadastro',
                        'controller' => 'cemiterio',
                        'action' => 'formulario'
                    )
                )
            ),
            array(
                'label' => 'Estoque',
                'controller' => 'estoque',
                'action' => 'index',
                'route' => 'admin/default',
                'pages' => array(
                    array(
                        'label' => 'Cadastro',
                        'controller' => 'estoque',
                        'action' => 'formulario'
                    )
                )
            ),
            array(
                'label' => 'Tráfego',
                'controller' => 'trafego',
                'action' => 'index',
                'route' => 'admin/default',
                'pages' => array(
                    array(
                        'label' => 'Cadastro',
                        'controller' => 'trafego',
                        'action' => 'formulario'
                    )
                )
            ),
            array(
                'label' => 'Tesouraria',
                'controller' => 'tesouraria',
                'action' => 'index',
                'route' => 'admin/default',
                'pages' => array(
                    array(
                        'label' => 'Cadastro',
                        'controller' => 'tesouraria',
                        'action' => 'formulario'
                    )
                )
            ),
        )
    ),
    array(
        'label' => 'Estoque',
        'route' => 'estoque',
        'controller' => 'fornecedor',
        'action' => 'index',
        'pages' => array(
            array(
                'label' => 'Fornecedor',
                'controller' => 'fornecedor',
                'action' => 'index',
                'route' => 'estoque/default',
                'pages' => array(
                    array(
                        'label' => 'Cadastro',
                        'controller' => 'fornecedor',
                        'action' => 'formulario'
                    ),
                )
            ),
            array(
                'label' => 'Contrato',
                'controller' => 'contrato',
                'action' => 'index',
                'route' => 'estoque/default',
                'pages' => array(
                    array(
                        'label' => 'Contrato',
                        'controller' => 'contrato',
                        'action' => 'formulario'
                    )
                )
            ),
            array(
                'label' => 'Descarte',
                'controller' => 'descarte',
                'action' => 'index',
                'route' => 'estoque/default',
                'pages' => array(
                    array(
                        'label' => 'Cadastro',
                        'controller' => 'descarte',
                        'action' => 'formulario',
                    ),
                    array(
                        'label' => 'Recebimento',
                        'controller' => 'recebimentoDescarte',
                        'action' => 'index',
                        'pages' => array(
                            array(
                                'label' => 'Cadastro',
                                'controller' => 'recebimentoDescarte',
                                'action' => 'formulario',
                            )
                        )
                    ),
                )
            ),
            array(
                'label' => 'Pedidos/Recebimento',
                'controller' => 'pedido',
                'action' => 'index',
                'route' => 'estoque/default',
                'pages' => array(
                    array(
                        'label' => 'Cadastro',
                        'controller' => 'pedido',
                        'action' => 'formulario',
                        'pages' => array(
                            array(
                                'label' => 'Recebimentos',
                                'controller' => 'recebimento',
                                'action' => 'index',
                                'pages' => array(
                                    array(
                                        'label' => 'Cadastro',
                                        'controller' => 'recebimento',
                                        'action' => 'formulario'
                                    )
                                )
                            )
                        )
                    )
                )
            ),
            array(
                'label' => 'Transferência',
                'controller' => 'transferencia',
                'action' => 'index',
                'route' => 'estoque/default',
                'pages' => array(
                    array(
                        'label' => 'Cadastro',
                        'controller' => 'transferencia',
                        'action' => 'formulario',
                    ),
                    array(
                        'label' => 'Recebimento',
                        'controller' => 'recebimentoTransferencia',
                        'action' => 'index',
                        'pages' => array(
                            array(
                                'label' => 'Cadastro',
                                'controller' => 'recebimentoTransferencia',
                                'action' => 'formulario',
                            )
                        )
                    ),
                )
            ),
        )
    ),
    array(
        'label' => 'Agência',
        'route' => 'agencia',
        'controller' => 'agencia',
        'action' => 'index',
        'pages' => array(
            array(
                'label' => 'Contratação',
                'controller' => 'contratacao',
                'action' => 'index',
                'route' => 'agencia/default',
                'pages' => array(
                    array(
                        'label' => 'Contratação',
                        'controller' => 'contratacao',
                        'action' => 'formulario'
                    ),
                )
            ),
        )
    )
);
