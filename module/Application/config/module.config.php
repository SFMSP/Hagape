<?php

namespace Application;

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller' => 'Index',
                        'action' => 'index'
                    )
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '[:controller[/:action[/:id]]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '\d+'
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'Application\Controller',
                                'controller' => 'Index',
                                'action' => 'index',
                            )
                        )
                    ),
                )
            ),
            'auth' => array(
                'type' => 'Literal',
                'priority' => 1000,
                'options' => array(
                    'route' => '/auth',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Base\Controller',
                        'controller' => 'Index',
                        'action' => 'index'
                    )
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'login' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/login',
                            'defaults' => array(
                                'controller' => 'zfcuser',
                                'action' => 'login',
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'factories' => array(
            'navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory',
            'Base\Log\Log' => 'Base\Factory\Log',
            'Mail' => 'Base\Factory\Mail',
            'FloatFormat' => 'Base\Factory\FloatFormat',
            'Zend\Authentication\AuthenticationService' => 'Base\Factory\Authentication'
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => 'Application\Controller\IndexController',
            'Application\Controller\Home' => 'Application\Controller\HomeController',
            'Application\Controller\Perfil' => 'Application\Controller\PerfilController',
            'Application\Controller\Usuario' => 'Application\Controller\UsuarioController',
            'Application\Controller\Search' => 'Application\Controller\SearchController'
        )
    ),
    'module_layouts' => array(
        'login' => 'layout/layout-login',
        'principal' => 'layout/layout-principal',
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'XHTML1_TRANSITIONAL',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'layout/layout' => __DIR__ . '//..//..//..//module//Base//view//layout//layout-principal.phtml',
            'error/404' => __DIR__ . '//..//..//..//module//Base//view//error//404.phtml',
            'error/index' => __DIR__ . '//..//..//..//module//Base//view//error//index.phtml',
        ),
        'template_path_stack' => array(
            'Application' => __DIR__ . '/../view',
        ),
    ),
    'view_helpers' => array(
        'invokables' => array(
            'formelementerrors' => 'Application\View\Helper\FormElementErrors',
        )
    ),
    'view_helper_config' => array(
        'flashmessenger' => array(
            'message_open_format' => '<div class="alert alert-success fade in"%s><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">Ã—</a>',
            'message_close_string' => '</div>',
            'message_separator_string' => '<br>'
        )
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),
    ),
    'navigation' => array(
        'default' => __DIR__ . '/module.config.navigation.php'
    )
);
