<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Cidade
 *
 * @ORM\Table(name="tb_cidade", indexes={@ORM\Index(name="FK_uf_cidade", columns={"id_uf"})})
 * @ORM\Entity(repositoryClass="Application\Repository\CidadeRepository")
 */
class Cidade extends BaseEntity implements EntityInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_cidade", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome", type="string", length=255, nullable=false)
     */
    private $nome;

    /**
     * @var Uf
     *
     * @ORM\ManyToOne(targetEntity="Uf")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_uf", referencedColumnName="id_uf")
     * })
     */
    private $uf;

    function getId() {
        return $this->id;
    }

    function getNome() {
        return $this->nome;
    }

    function getUf() {
        return $this->uf; 
    }

    function setId($id) {
        $this->id = $id;
        return $this;
    }

    function setNome($nome) {
        $this->nome = $nome;
        return $this;
    }

    function setUf(Uf $uf) {
        $this->uf = $uf;
        return $this;
    }

    public function __toString() {
        return $this->nome;
    }

    public function getLabel() {
        return $this->__toString();
    }

    public function toArray() {
        return array(
            "id" => $this->id,
            "nome" => $this->nome,
            "uf" => $this->uf
        );
    }

}
