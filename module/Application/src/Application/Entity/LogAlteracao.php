<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * LogAlteracao
 *
 * @ORM\Table(name="tb_log_alteracao", indexes={@ORM\Index(name="fk_tb_log_alteracao_tb_usuario1_idx", columns={"id_usuario"})})
 * @ORM\Entity(repositoryClass="Application\Repository\LogAlteracaoRepository")
 */
class LogAlteracao extends BaseEntity implements EntityInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_log_alteracao", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_alteracao", type="datetime", nullable=false)
     */
    private $dataAlteracao;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_funcionalidade", type="string", length=50, nullable=false)
     */
    private $funcionalidade;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_ip", type="string", length=30, nullable=false)
     */
    private $ip;

    /**
     * @var string
     *
     * @ORM\Column(name="st_tipo_alteracao", type="string", length=1, nullable=false)
     */
    private $tipo;

    /**
     * @var \Application\Entity\Usuario
     *
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="id_usuario", onDelete="SET NULL")
     * })
     */
    private $usuario;
    
     /**
     * @var string
     *
     * @ORM\Column(name="id_registro", type="integer", length=11, nullable=false)
     */
    private $idRegistro;

    /**
     * Método construtor que seta automáticamento a data da alteração
     */
    public function __construct() {
        $this->dataAlteracao = new \DateTime('now');
    }

    /**
     * 
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * 
     * @return \DateTime
     */
    public function getDataAlteracao() {
        return $this->dataAlteracao;
    }

    /**
     * 
     * @return string
     */
    public function getFuncionalidade() {
        return $this->funcionalidade;
    }

    /**
     * 
     * @return string
     */
    public function getIp() {
        return $this->ip;
    }

    /**
     * 
     * @return integer
     */
    public function getTipo() {
        return $this->tipo;
    }

    /**
     * 
     * @return \Application\Entity\Usuario
     */
    public function getUsuario() {
        return $this->usuario;
    }

    /**
     * 
     * @param integer $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * 
     * @param \DateTime $dataAlteracao
     */
    public function setDataAlteracao(\DateTime $dataAlteracao) {
        $this->dataAlteracao = $dataAlteracao;
    }

    /**
     * 
     * @param string $funcionalidade
     */
    public function setFuncionalidade($funcionalidade) {
        $this->funcionalidade = $funcionalidade;
    }

    /**
     * 
     * @param string $ip
     */
    public function setIp($ip) {
        $this->ip = $ip;
    }

    /**
     * 
     * @param integer $tipo
     */
    public function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    /**
     * 
     * @param \Application\Entity\Usuario $usuario
     */
    public function setUsuario(\Application\Entity\Usuario $usuario) {
        $this->usuario = $usuario;
    }

    /**
     * 
     * @return integer
     */
    public function getIdRegistro() {
        return $this->idRegistro;
    }

    /**
     * 
     * @param integer $idRegistro
     */
    public function setIdRegistro($idRegistro) {
        $this->idRegistro = $idRegistro;
    }

        /**
     * 
     * @return string
     */
    public function __toString() {
        return $this->ip;
    }

    /**
     * 
     * @return string
     */
    public function getLabel() {
        return $this->ip;
    }

    /**
     * 
     * @return array
     */
    public function toArray() {
        return array(
            "id" => $this->ip,
            "dataAlteracao" => $this->dataAlteracao,
            "funcionalidade" => $this->funcionalidade,
            "tipo" => $this->tipo,
            "usuario" => $this->usuario,
            "ip" => $this->ip,
            "idRegistro" => $this->idRegistro
        );
    }

}
