<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Funcionalidade
 *
 * @ORM\Table(name="tb_funcionalidade")
 * @ORM\Entity
 */
class Funcionalidade extends BaseEntity implements EntityInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_funcionalidade", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome", type="string", length=45, nullable=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_controller", type="string", length=45, nullable=false)
     */
    private $controller;

    /**
     * 
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * 
     * @param integer $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * 
     * @return string
     */
    public function getNome() {
        return $this->nome;
    }

    /**
     * 
     * @param string $nome
     */
    public function setNome($nome) {
        $this->nome = $nome;
    }

    /**
     * 
     * @return string
     */
    public function getController() {
        return $this->controller;
    }

    /**
     * 
     * @param string $controller
     */
    public function setController($controller) {
        $this->controller = $controller;
    }

        /**
     * 
     * @return string
     */
    public function __toString() {
        return $this->nome;
    }

    /**
     * 
     * @return string
     */
    public function getLabel() {
        return $this->nome;
    }

    /**
     * 
     * @return array
     */
    public function toArray() {
        return array(
            "id" => $this->id,
            "nome" => $this->nome,
            "controller" => $this->controller
        );
    }

}
