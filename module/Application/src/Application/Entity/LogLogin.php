<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * LogLogin
 *
 * @ORM\Table(name="tb_log_login", indexes={@ORM\Index(name="fk_tb_log_login_tb_usuario1_idx", columns={"id_usuario"})})
 * @ORM\Entity(repositoryClass="Application\Repository\LogLoginRepository")
 */
class LogLogin extends BaseEntity implements EntityInterface 
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_log_login", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_tentativa", type="datetime", nullable=false)
     */
    private $dataTentativa;

    /**
     * @var string
     *
     * @ORM\Column(name="nu_ip", type="string", length=30, nullable=false)
     */
    private $ip;

    /**
     * @var \Application\Entity\Usuario
     *
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="id_usuario", onDelete="SET NULL")
     * })
     */
    private $usuario;
    
    /**
     * Método construtor que seta automáticamento a data de tentativa
     */
    public function __construct() {
        $this->dataTentativa = new \DateTime('now');
        
    }
    
    /**
     * 
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * 
     * @return \DateTime
     */
    public function getDataTentativa() {
        return $this->dataTentativa;
    }

    /**
     * 
     * @return string
     */
    public function getIp() {
        return $this->ip;
    }

    /**
     * 
     * @return \Application\Entity\Usuario
     */
    public function getUsuario() {
        return $this->usuario;
    }

    /**
     * 
     * @param integer $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * 
     * @param \DateTime $dataTentativa
     */
    public function setDataTentativa(\DateTime $dataTentativa) {
        $this->dataTentativa = $dataTentativa;
    }

      /**
     * 
     * @param integer $ip
     */
    public function setIp($ip) {
        $this->ip = $ip;
    }

    /**
     * 
     * @param \Application\Entity\Usuario $usuario
     */
    public function setUsuario(\Application\Entity\Usuario $usuario) {
        $this->usuario = $usuario;
    }
        
    /**
     * 
     * @return string
     */
    public function __toString() {
        return $this->ip;
    } 

    /**
     * 
     * @return string
     */
    public function getLabel() {
        return $this->ip;
    }

    /**
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id' => $this->id,
            'dataTentatica' => $this->dataTentativa,
            'ip' => $this->ip,
            'usuario' => $this->usuario
        );
    }

}

