<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Uf
 *
 * @ORM\Table(name="tb_uf")
 * @ORM\Entity(repositoryClass="Application\Repository\UfRepository")
 */
class Uf extends BaseEntity implements EntityInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_uf", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="sg_sigla", type="string", length=2, nullable=false)
     */
    private $sigla;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome", type="string", length=50, nullable=false)
     */
    private $nome;

    function getId() {
        return $this->id;
    }

    function getSigla() {
        return $this->sigla;
    }

    function getNome() {
        return $this->nome;
    }

    function setId($id) {
        $this->id = $id;
        return $this;
    }

    function setSigla($sigla) {
        $this->sigla = $sigla;
        return $this;
    }

    function setNome($nome) {
        $this->nome = $nome;
        return $this;
    }

    public function __toString() {
        return $this->nome;
    }

    public function getLabel() {
        return $this->__toString();
    }

    public function toArray() {
        return array(
            "id" => $this->id,
            "sigla" => $this->sigla,
            "nome" => $this->nome
        );
    }

}
