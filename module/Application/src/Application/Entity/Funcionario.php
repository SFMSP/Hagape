<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Funcionario
 *
 * @ORM\Table(name="tb_funcionario")
 * @ORM\Entity(repositoryClass="Application\Repository\FuncionarioRepository")
 */
class Funcionario extends BaseEntity implements EntityInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_funcionario", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=false)
     */
    private $dataCadastro;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255, nullable=false)
     */
    private $nome;

    public function __construct() {
        $this->dataCadastro = new \DateTime('now');
    }

    function getId() {
        return $this->id;
    }

    function getDataCadastro() {
        return $this->dataCadastro;
    }

    function getNome() {
        return $this->nome;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setDataCadastro(\DateTime $dataCadastro) {
        $this->dataCadastro = $dataCadastro;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    public function __toString() {
        return $this->nome;
    }

    public function getLabel() {
        return $this->nome;
    }

    public function toArray() {
        return array(
            "id" => $this->id,
            "dataCadastro" => $this->dataCadastro,
            "nome" => $this->getNome()
        );
    }

}
