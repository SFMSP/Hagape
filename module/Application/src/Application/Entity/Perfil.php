<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Perfil
 *
 * @ORM\Table(name="tb_perfil")
 * @ORM\Entity(repositoryClass="Application\Repository\PerfilRepository")
 */
class Perfil extends BaseEntity implements EntityInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_perfil", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_perfil", type="string", length=100, nullable=false)
     */
    private $nome;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Privilegio", inversedBy="perfis")
     * @ORM\JoinTable(name="tb_privilegio_perfil",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_perfil", referencedColumnName="id_perfil")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_privilegio", referencedColumnName="id_privilegio")
     *   }
     * )
     */

    private $privilegios;

    public function __construct() {
        $this->privilegios = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * 
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * 
     * @return string
     */
    public function getNome() {
        return $this->nome;
    }

    /**
     * 
     * @param integer $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * 
     * @param string $nome
     */
    public function setNome($nome) {
        $this->nome = $nome;
    }

    /**
     * 
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrivilegios() {
        return $this->privilegios;
    }

    /**
     * 
     * @param \Doctrine\Common\Collections\Collection $privilegios
     */
    public function setPrivilegios(\Doctrine\Common\Collections\Collection $privilegios) {
        $this->privilegios = $privilegios;
    }

    /**
     * 
     * @return string
     */
    public function __toString() {
        return $this->nome;
    }

    /**
     * 
     * @return string
     */
    public function getLabel() {
        return $this->nome;
    }

    /**
     * 
     * @return array
     */
    public function toArray() {
        return array(
            'id' => $this->id,
            'nome' => $this->nome,
            'privilegios' => $this->privilegios
        );
    }

}
