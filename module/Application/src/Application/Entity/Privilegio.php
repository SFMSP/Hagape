<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Privilegio
 *
 * @ORM\Table(name="tb_privilegio", indexes={@ORM\Index(name="fk_tb_privilegio_tb_funcionalidade1_idx", columns={"id_funcionalidade"}), @ORM\Index(name="fk_tb_privilegio_tb_acao1_idx", columns={"id_acao"})})
 * @ORM\Entity(repositoryClass="Application\Repository\PrivilegioRepository")
 */
class Privilegio extends BaseEntity implements EntityInterface {

    const FILTRO_AGENCIA = 1;
    const FILTRO_CEMITERIO = 2;
    const FILTRO_ESTOQUE = 3;
    const FILTRO_TRAFEGO = 4;
    const FILTRO_TESOURARIA = 5;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_privilegio", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Application\Entity\Acao
     *
     * @ORM\ManyToOne(targetEntity="Acao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_acao", referencedColumnName="id_privilegio")
     * })
     */
    private $acao;

    /**
     * @var \Application\Entity\Funcionalidade
     *
     * @ORM\ManyToOne(targetEntity="Funcionalidade")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_funcionalidade", referencedColumnName="id_funcionalidade")
     * })
     */
    private $funcionalidade;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Perfil", mappedBy="privilegios")
     * @ORM\JoinTable(name="tb_privilegio_perfil",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_privilegio", referencedColumnName="id_privilegio")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_perfil", referencedColumnName="id_perfil")
     *   }
     * )
     */
    private $perfis;

    /**
     * @var \Application\Entity\
     *
     * @ORM\ManyToOne(targetEntity="Filtro")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_filtro", referencedColumnName="id_filtro")
     * })
     */
    private $filtro;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_banco", type="boolean", nullable=false)
     */
    private $banco;

    /**
     * Constructor
     */
    public function __construct() {
        $this->perfis = new \Doctrine\Common\Collections\ArrayCollection();
        $this->banco = false;
    }

    /**
     * 
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * 
     * @return \Application\Entity\Acao
     */
    public function getAcao() {
        return $this->acao;
    }

    /**
     * 
     * @return \Application\Entity\Funcionalidade
     */
    public function getFuncionalidade() {
        return $this->funcionalidade;
    }

    /**
     * 
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPerfis() {
        return $this->perfis;
    }

    /**
     * 
     * @return type
     */
    function getFiltro() {
        return $this->filtro;
    }

    function getBanco() {
        return $this->banco;
    }

    /**
     * 
     * @param integer $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * 
     * @param \Application\Entity\Acao $acao
     */
    public function setAcao(\Application\Entity\Acao $acao) {
        $this->acao = $acao;
    }

    /**
     * 
     * @param \Application\Entity\Funcionalidade $funcionalidade
     */
    public function setFuncionalidade(\Application\Entity\Funcionalidade $funcionalidade) {
        $this->funcionalidade = $funcionalidade;
    }

    /**
     * 
     * @param \Doctrine\Common\Collections\Collection $perfis
     */
    public function setPerfis(\Doctrine\Common\Collections\Collection $perfis) {
        $this->perfis = $perfis;
    }

    /**
     * 
     * @param integer $filtro
     */
    function setFiltro($filtro) {
        $this->filtro = $filtro;
    }

    /**
     * 
     * @param type $banco
     */
    function setBanco($banco) {
        $this->banco = $banco;
    }

    /**
     * 
     * @return string
     */
    public function __toString() {
        return $this->perfis;
    }

    /**
     * 
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLabel() {
        return $this->perfis;
    }

    /**
     * 
     * @return array
     */
    public function toArray() {
        return array(
            "id" => $this->id,
            "acao" => $this->acao,
            "funcionalidade" => $this->funcionalidade,
            "perfis" => $this->perfis,
            "banco" => $this->banco
        );
    }

}
