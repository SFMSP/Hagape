<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;
use Zend\Crypt\Key\Derivation\Pbkdf2;
use Zend\Math\Rand;

/**
 * Usuario
 *
 * @ORM\Table(name="tb_usuario")
 * @ORM\Entity(repositoryClass="Application\Repository\UsuarioRepository")
 * 
 */
class Usuario extends BaseEntity implements EntityInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_usuario", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Perfil
     *
     * @ORM\ManyToOne(targetEntity="Perfil")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_perfil", referencedColumnName="id_perfil")
     * })
     */
    private $perfil;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="datetime", nullable=false)
     */
    private $dataCadastro;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome", type="string", length=150, nullable=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_cpf", type="string", length=20, nullable=false)
     */
    private $cpf;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_telefone", type="string", length=50, nullable=true)
     */
    private $telefone;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_email", type="string", length=100, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_login", type="string", length=50, nullable=false)
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_senha", type="string", length=255, nullable=false)
     */
    private $senha;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_avatar", type="string", length=255, nullable=true)
     */
    private $avatar;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_salt", type="string", length=50, nullable=false)
     */
    private $salt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_ativo", type="boolean", nullable=false)
     */
    private $ativo;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_troca_senha", type="boolean", nullable=false)
     */
    private $trocaSenha;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bool_excluido", type="boolean", nullable=true)
     */
    private $excluido;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Admin\Entity\Agencia", inversedBy="id")
     * @ORM\JoinTable(name="tb_agencia_usuario",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_agencia", referencedColumnName="id_agencia")
     *   }
     * )
     */
    private $agencia;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Admin\Entity\Estoque", inversedBy="id")
     * @ORM\JoinTable(name="tb_estoque_usuario",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_estoque", referencedColumnName="id_estoque")
     *   }
     * )
     */
    private $estoque;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Admin\Entity\Cemiterio", inversedBy="id")
     * @ORM\JoinTable(name="tb_cemiterio_usuario",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_cemiterio", referencedColumnName="id_cemiterio")
     *   }
     * )
     */
    private $cemiterio;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Admin\Entity\Trafego", inversedBy="id")
     * @ORM\JoinTable(name="tb_trafego_usuario",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_trafego", referencedColumnName="id_trafego")
     *   }
     * )
     */
    private $trafego;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Admin\Entity\Tesouraria", inversedBy="id")
     * @ORM\JoinTable(name="tb_tesouraria_usuario",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_tesouraria", referencedColumnName="id_tesouraria")
     *   }
     * )
     */
    private $tesouraria;

    /**
     * 
     */
    public function __construct() {
        $this->dataCadastro = new \DateTime('now');
        $this->salt = base64_encode(Rand::getBytes(8, true));
        $this->excluido = false;
    }

    /**
     * 
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * 
     * @return \DateTime
     */
    public function getDataCadastro() {
        return $this->dataCadastro;
    }

    /**
     * 
     * @return string
     */
    public function getNome() {
        return $this->nome;
    }

    /**
     * 
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * 
     * @return string
     */
    public function getLogin() {
        return $this->login;
    }

    /**
     * 
     * @return string
     */
    public function getSenha() {
        return $this->senha;
    }

    /**
     * 
     * @return string
     */
    public function getSalt() {
        return $this->salt;
    }

    /**
     * 
     * @return integer
     */
    public function getAtivo() {
        return $this->ativo;
    }

    /**
     * 
     * @return Agencia
     */
    function getAgencia() {
        return $this->agencia;
    }

    /**
     * 
     * @return type
     */
    function getCemiterio() {
        return $this->cemiterio;
    }

    /**
     * 
     * @return type
     */
    function getEstoque() {
        return $this->estoque;
    }

    /**
     * 
     * @return type
     */
    function getTrafego() {
        return $this->trafego;
    }

    /**
     * 
     * @return type
     */
    function getTesouraria() {
        return $this->tesouraria;
    }

    /**
     * 
     * @return type
     */
    function getCpf() {
        return $this->cpf;
    }

    /**
     * 
     * @return type
     */
    function getAvatar() {
        return $this->avatar;
    }

    /**
     * 
     * @return type
     */
    function getTelefone() {
        return $this->telefone;
    }

    /**
     * 
     * @return type
     */
    function getExcluido() {
        return $this->excluido;
    }
    
    /**
     * 
     * @return type
     */
    function getTrocaSenha() {
        return $this->trocaSenha;
    }

    /**
     * 
     * @param integer $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * 
     * @param \DateTime $dataCadastro
     */
    public function setDataCadastro(\DateTime $dataCadastro) {
        $this->dataCadastro = $dataCadastro;
    }

    /**
     * 
     * @param string $nome
     */
    public function setNome($nome) {
        $this->nome = $nome;
    }

    /**
     * 
     * @param string $email
     */
    public function setEmail($email) {
        $this->email = $email;
    }

    /**
     * 
     * @param string $login
     */
    public function setLogin($login) {
        $this->login = $login;
    }

    /**
     * 
     * @param string $senha
     */
    public function setSenha($senha) {
        $this->senha = $this->encryptPassword($senha);
    }

    /**
     * 
     * @param string $salt
     */
    public function setSalt($salt) {
        $this->salt = $salt;
    }

    /**
     * 
     * @return \Application\Entity\Perfil
     */
    public function getPerfil() {
        return $this->perfil;
    }

    /**
     * 
     * @param \Application\Entity\Perfil $perfil
     */
    public function setPerfil(Perfil $perfil) {
        $this->perfil = $perfil;
    }

    /**
     * 
     * @param integer $ativo
     */
    public function setAtivo($ativo) {
        $this->ativo = $ativo;
    }

    /**
     * 
     * @param \Doctrine\Common\Collections\Collection $agencia
     */
    function setAgencia(\Doctrine\Common\Collections\Collection $agencia) {
        $this->agencia = $agencia;
    }

    /**
     * 
     * @param \Doctrine\Common\Collections\Collection $agencia
     */
    function setEstoque(\Doctrine\Common\Collections\Collection $estoque) {
        $this->estoque = $estoque;
    }

    /**
     * 
     * @param \Doctrine\Common\Collections\Collection $agencia
     */
    function setCemiterio(\Doctrine\Common\Collections\Collection $cemiterio) {
        $this->cemiterio = $cemiterio;
    }

    /**
     * 
     * @param \Doctrine\Common\Collections\Collection $agencia
     */
    function setTrafego(\Doctrine\Common\Collections\Collection $trafego) {
        $this->trafego = $trafego;
    }

    /**
     * 
     * @param \Doctrine\Common\Collections\Collection $agencia
     */
    function setTesouraria(\Doctrine\Common\Collections\Collection $tesouraria) {
        $this->tesouraria = $tesouraria;
    }

    /**
     * 
     * @param type $cpf
     */
    function setCpf($cpf) {
        $this->cpf = $cpf;
    }

    /**
     * 
     * @param type $telefone
     */
    function setTelefone($telefone) {
        $this->telefone = $telefone;
    }

    /**
     * 
     * @param type $excluido
     */
    function setExcluido($excluido) {
        $this->excluido = $excluido;
    }
    
    /**
     * 
     * @param type $trocaSenha
     */
    function setTrocaSenha($trocaSenha) {
        $this->trocaSenha = $trocaSenha;
    }

    /**
     * 
     * @param string $avatar
     */
    function setAvatar($avatar) {
        $this->avatar = $avatar;
    }

    /**
     * 
     * @param string $password
     */
    public function encryptPassword($password) {
        return base64_encode(Pbkdf2::calc('sha256', $password, $this->salt, 10000, strlen($password) * 2));
    }

    /**
     * 
     * @return string
     */
    public function __toString() {
        return $this->login;
    }

    /**
     * 
     * @return string
     */
    public function getLabel() {
        return $this->nome;
    }

    /**
     * 
     * @return array
     */
    public function toArray() {
        $dados = array(
            'id' => $this->id,
            'perfil' => $this->perfil,
            'nome' => $this->nome,
            'email' => $this->email,
            'senha' => $this->senha,
            'salt' => $this->salt,
            'ativo' => $this->ativo,
            'login' => $this->login,
            'cpf' => $this->cpf,
            'telefone' => $this->telefone,
            'excluido' => $this->excluido,
            'trocaSenha' => $this->trocaSenha,
            'avatar' => $this->avatar
        );

        if ($this->agencia) {
            $agencias = array();

            foreach ($this->agencia as $agencia) {
                $agencias[] = $agencia->getId();
            }

            $dados['agencia'] = $agencias;
        }

        if ($this->cemiterio) {
            $cemiterios = array();

            foreach ($this->cemiterio as $cemiterio) {
                $cemiterios[] = $cemiterio->getId();
            }

            $dados['cemiterio'] = $cemiterios;
        }

        if ($this->estoque) {
            $estoques = array();

            foreach ($this->estoque as $estoque) {
                $estoques[] = $estoque->getId();
            }

            $dados['estoque'] = $estoques;
        }

        if ($this->trafego) {
            $trafegos = array();

            foreach ($this->trafego as $trafego) {
                $trafegos[] = $trafego->getId();
            }

            $dados['trafego'] = $trafegos;
        }

        if ($this->tesouraria) {
            $tesourarias = array();

            foreach ($this->tesouraria as $tesouraria) {
                $tesourarias[] = $tesouraria->getId();
            }

            $dados['tesouraria'] = $tesourarias;
        }

        return $dados;
    }

}
