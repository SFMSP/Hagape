<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\EntityInterface;
use Base\Entity\BaseEntity;

/**
 * Acao
 *
 * @ORM\Table(name="tb_acao")
 * @ORM\Entity
 */
class Acao extends BaseEntity implements EntityInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_privilegio", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_nome", type="string", length=50, nullable=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="txt_action", type="string", length=50, nullable=false)
     */
    private $action;

    /**
     * 
     * @param integer $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * 
     * @return string
     */
    public function getNome() {
        return $this->nome;
    }

    /**
     * 
     * @param string $nome
     */
    public function setNome($nome) {
        $this->nome = $nome;
    }
    
    /**
     * 
     * @return string
     */
    public function getAction() {
        return $this->action;
    }

    /**
     * 
     * @param string $action
     */
    public function setAction($action) {
        $this->action = $action;
    }

    
    /**
     * 
     * @return string
     */
    public function __toString() {
        return $this->nome;
    }

    /**
     * 
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * 
     * @return string
     */
    public function getLabel() {
        return $this->nome;
    }

    /**
     * 
     * @return array
     */
    public function toArray() {
        return array(
            "id" => $this->id,
            "nome" => $this->nome,
            "action" => $this->action
        );
    }

}
