<?php

namespace Application\Service;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Classe que contém as operações realizadas na entidade de usuários
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Usuario extends BaseApplicationService {

    /**
     *
     * @var \Application\Repository\PrivilegioRepository 
     */
    private $privilegioRepository;

    /**
     * 
     * @param \Application\Repository\PrivilegioRepository $privilegioRepository
     */
    function setPrivilegioRepository(\Application\Repository\PrivilegioRepository $privilegioRepository) {
        $this->privilegioRepository = $privilegioRepository;
    }

    /**
     * Método de salvamento, tanto inclusão como edição
     * 
     * @param array $dados
     */
    public function save(array $dados) {

        //Retira os elementos que não irão para o banco
        unset($dados['enviar']);
        unset($dados['confirmaSenha']);

        $arrFiltros = $this->privilegioRepository->getFiltrosPerfil($dados['perfil']);
        
       
        $filtros = array();

        foreach ($arrFiltros as $filt) {
            $filtros[$filt['filtro']] = true;
        }

        $dados['agencia'] = isset($filtros['agencia']) ? $this->getCollections($dados['agencia'], 'Admin\Entity\Agencia') : new ArrayCollection();
        $dados['cemiterio'] = isset($filtros['cemiterio']) ? $this->getCollections($dados['cemiterio'], 'Admin\Entity\Cemiterio') : new ArrayCollection();
        $dados['estoque'] = isset($filtros['estoque']) ? $this->getCollections($dados['estoque'], 'Admin\Entity\Estoque') : new ArrayCollection();
        $dados['trafego'] = isset($filtros['trafego']) ? $this->getCollections($dados['trafego'], 'Admin\Entity\Trafego') : new ArrayCollection();
        $dados['tesouraria'] = isset($filtros['tesouraria']) ? $this->getCollections($dados['tesouraria'], 'Admin\Entity\Tesouraria') : new ArrayCollection();

        //Seta o perfil
        $dados['perfil'] = $this->getEm()->getReference('Application\Entity\Perfil', $dados['perfil']);
        
        if(empty($dados['id'])){
            $dados['trocaSenha'] = true;
        }
        
        return parent::save($dados);
    }

    /**
     * 
     * @param integer $idUsuario
     * @param string $novaSenha
     * @return boolean
     */
    public function alterarSenha($idUsuario, $novaSenha) {
        try {

            //Busca a referencia do usuario 
            $user = $this->em->find($this->getEntityName(), $idUsuario);
            $user->setSenha($novaSenha);

            //Faz a alteração
            $this->em->persist($user);
            $this->em->flush();

            //Seta como usuário de alteração o próprio usuário
            $this->dadosUser['idUsuario'] = $idUsuario;

            //Registro o log da alteração
            $this->logAlteracao->save($this->getDadosAlteracao('A', $idUsuario));

            return true;
        } catch (\Exception $ex) {
            //Registra o log de erro
            $this->log->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            return false;
        }
    }

}
