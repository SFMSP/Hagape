<?php
namespace Application\Service;

/**
 * Classe que contém as operações realizadas na entidade de usuários
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Perfil extends BaseApplicationService {

    /**
     * 
     * @param array $data
     * @return string
     */
    public function save(array $data) {
        //Cria uma coleção de privilégios
        $privilegios = new \Doctrine\Common\Collections\ArrayCollection();
            
        //Percorre os privilégios informados
        foreach($data['privilegios'] as $privilegio){
            $privilegio = $this->em->getReference('Application\Entity\Privilegio', $privilegio);
            $privilegios->add($privilegio);
        }
              
        //Seta os privilégios
        $data['privilegios'] = $privilegios;

        //Realiza a operação
        return parent::save($data);
    }
 

}
