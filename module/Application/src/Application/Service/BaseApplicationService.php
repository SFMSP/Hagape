<?php

namespace Application\Service;

use Base\Service\BaseService;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Classe base para service que estende a base service 
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
abstract class BaseApplicationService extends BaseService {

    /**
     *
     * @var \Application\Service\LogAlteracao
     */
    protected $logAlteracao;

    /**
     *
     * @var array
     */
    protected $dadosUser;

    /**
     *
     * @var \Base\Helper\DatetimeFormat
     */
    protected $dateTimeFormat;

    /**
     *
     * @var \Base\Helper\FloatFormat
     */
    protected $floatFormat;

    /**
     *
     * @var \Base\Helper\StringFormat
     */
    protected $stringFormat;

    /**
     * 
     * @param \Application\Service\LogAlteracao $logAlteracao
     */
    public function setLogAlteracao(\Application\Service\LogAlteracao $logAlteracao) {
        $this->logAlteracao = $logAlteracao;
        return $this;
    }

    /**
     * 
     * @param array $dadosUser
     */
    public function setDadosUser($dadosUser) {
        $this->dadosUser = $dadosUser;
        return $this;
    }

    /**
     * 
     * @param \Base\Helper\StringFormat $stringFormat
     * @return \Application\Service\BaseApplicationService
     */
    function setStringFormat(\Base\Helper\StringFormat $stringFormat) {
        $this->stringFormat = $stringFormat;
        return $this;
    }

    /**
     * 
     * @return \Base\Helper\DatetimeFormat
     */
    public function getDateTimeFormat() {
        return $this->dateTimeFormat;
    }

    /**
     * 
     * @param \Base\Helper\DatetimeFormat $dateTimeFormat
     */
    public function setDateTimeFormat(\Base\Helper\DatetimeFormat $dateTimeFormat) {
        $this->dateTimeFormat = $dateTimeFormat;
        return $this;
    }

    /**
     * 
     * @return \Application\Service\LogAlteracao
     */
    public function getLogAlteracao() {
        return $this->logAlteracao;
    }

    /**
     * 
     * @return array
     */
    public function getDadosUser() {
        return $this->dadosUser;
    }

    /**
     * 
     * @return \Base\Helper\FloatFormat
     */
    public function getFloatFormat() {
        return $this->floatFormat;
    }

    /**
     * 
     * @return \Base\Helper\StringFormat
     */
    function getStringFormat() {
        return $this->stringFormat;
    }

    /**
     * 
     * @param \Base\Helper\FloatFormat $floatFormat
     */
    public function setFloatFormat(\Base\Helper\FloatFormat $floatFormat) {
        $this->floatFormat = $floatFormat;
        return $this;
    }

    /**
     * 
     * @param string $tipo
     * @return array
     */
    public function getDadosAlteracao($tipo, $id_registro) {
        if (array_key_exists('REMOTE_ADDR',$_SERVER)){
            $ip = $_SERVER['REMOTE_ADDR'];
        }else{
            $ip = '127.0.0.1';
        }
        //Seta os dados padrões da alteração
        $dadosAlteracao = array(
            'id' => '',
            'usuario' => $this->em->getReference('Application\Entity\Usuario', $this->dadosUser['idUsuario']),
            'funcionalidade' => $this->getEntityName(),
            'ip' => $ip,
            'tipo' => $tipo,
            'idRegistro' => $id_registro
        );

        //Retorna os dados
        return $dadosAlteracao;
    }

    /**
     * 
     * @param array $data
     */
    public function save(array $data) {
        //Armazena o objeto que foi persistido
        $entity = parent::save($data);

        //Verifica se a persistencia ocorreu de forma correta
        if ($entity) {
            //Organiza os dados de alteração
            $dadosAlteracao = empty($data['id']) ? $this->getDadosAlteracao('I', $entity->getId()) : $this->getDadosAlteracao('A', $entity->getId());

            //Registra a alteração
            $this->logAlteracao->save($dadosAlteracao);

            return $entity;
        } else {
            return false;
        }
    }

    /**
     * 
     * @param integer $id
     */
    public function delete($id) {
        //Caso o exclusão de um registro tenha ocorrido de forma correta, um log de exclusão é registrado
        if (parent::delete($id)) {
            $this->logAlteracao->save($this->getDadosAlteracao('X', $id));

            return true;
        } else {
            return false;
        }
    }

    /**
     * Função que deleta registros em massa
     * 
     * @param array $ids
     * @return boolean
     */
    public function deleteAll($ids, $logic = false) {

        //Inicia uma transação
        $this->getEm()->beginTransaction();

        //Deleta um a um
        foreach ($ids as $id) {

            $delete = $logic ? $this->logicDelete($id) : $this->delete($id);

            //Executa e verifica se o resultado foi negativo. Caso tenha sido, da roolback na transação
            if (!$delete) {

                $this->getEm()->rollback();
                return false;
            }
        }

        //Comita a transação
        $this->getEm()->commit();

        return true;
    }

    /**
     * Função que trata a exclusão lógica
     * 
     * @param integer $id
     */
    public function logicDelete($id, $field = 'excluido') {
        //Caso o exclusão de um registro tenha ocorrido de forma correta, um log de exclusão é registrado
        if (parent::logicDelete($id, $field)) {
            $this->logAlteracao->save($this->getDadosAlteracao('X', $id));

            return true;
        } else {
            return false;
        }
    }

    /**
     * Função que trata das referências manyToMany
     * 
     * @param array $data
     * @param string $referenceName
     * @return ArrayCollection
     */
    public function getCollections($data, $referenceName) {
        $collection = new ArrayCollection();

        foreach ($data as $id) {
            $collection->add($this->getEm()->getReference($referenceName, $id));
        }

        return $collection;
    }

    /**
     * Muda o status em massa
     * 
     * @param array $ids
     * @param boolean $status
     */
    public function mudarStatus($ids, $status) {
        foreach ($ids as $id) {
            $this->update(array('id' => $id, 'ativo' => $status));
        }

        return true;
    }

}
