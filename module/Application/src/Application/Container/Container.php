<?php

namespace Application\Container;

use Zend\Session\SessionManager;
use Base\Container\BaseContainer as BaseContainerInterface;

/**
 * Classe que armazena todas as dependências para serem chamados na classe Module
 *
 * @author Praxedes
 */
class Container implements BaseContainerInterface
{

    /**
     *
     * Retorna as instâncias dos repositórios
     * @return array
     */
    public function getForms()
    {
        return array(
            'Application\Form\Usuario' => function ($sm) {
                $repository = $sm->get('Application\Repository\PerfilRepository');

                $inputFilter = new \Application\InputFilter\Usuario();
                $agencias = $sm->get('Admin\Repository\AgenciaRepository')->getArraySelect('', array(), false, " WHERE t.excluido = 0 AND t.ativo = 1");
                $cemiterios = $sm->get('Admin\Repository\CemiterioRepository')->getArraySelect('', array(), false, " WHERE t.excluido = 0 AND t.ativo = 1");
                $estoques = $sm->get('Admin\Repository\EstoqueRepository')->getArraySelect('', array(), false, " WHERE t.excluido = 0 AND t.ativo = 1");
                $trafegos = $sm->get('Admin\Repository\TrafegoRepository')->getArraySelect('', array(), false, " WHERE t.excluido = 0 AND t.ativo = 1");
                $tesourarias = $sm->get('Admin\Repository\TesourariaRepository')->getArraySelect('', array(), false, " WHERE t.excluido = 0 AND t.ativo = 1");
                $privilegioRepository = $sm->get('Application\Repository\PrivilegioRepository');
                $usuarioRepository = $sm->get('Application\Repository\UsuarioRepository');

                $form = new \Application\Form\Usuario($inputFilter, $repository->getArraySelect(), $agencias, $cemiterios, $estoques, $trafegos, $tesourarias);
                $form->setPrivilegioRepository($privilegioRepository)
                    ->setUsuarioRepository($usuarioRepository);

                return $form;
            },
            'Application\Form\Perfil' => function ($sm) {
                $inputFilter = new \Application\InputFilter\Perfil();
                return new \Application\Form\Perfil($inputFilter);
            },
            'Application\Form\AlterarSenha' => function ($sm) {
                $inputFilter = new \Application\InputFilter\AlterarSenha();
                return new \Application\Form\AlterarSenha($inputFilter, $sm->get('Application\Repository\UsuarioRepository'), $sm->get('DadosUser'));
            },
            'Application\Form\LembrarSenha' => function ($sm) {
                $em = $sm->get('Doctrine\ORM\EntityManager');

                $repository = $sm->get('Application\Repository\UsuarioRepository');
                $repository->setLogger($sm->get('Base\Log\Log'));

                $inputFilter = new \Application\InputFilter\LembrarSenha();
                $form = new \Application\Form\LembrarSenha($inputFilter);
                $form->setUserRepository($repository);

                return $form;
            },
            'Application\Form\UsuarioFiltro' => function ($sm) {
                $repository = $sm->get('Application\Repository\PerfilRepository');

                return new \Application\Form\UsuarioFiltro($repository->getArraySelect('Perfil'));
            },
            'Application\Form\Avatar' => function ($sm) {
                return new \Application\Form\Avatar(new \Application\InputFilter\Avatar());
            },
            'Application\Form\Filtro' => function ($sm) {

                $dadosUser = $sm->get('DadosUser');


                if (array_search('agencia', $dadosUser['filtros']) !== false) {
                    $agencias = $sm->get('Admin\Repository\AgenciaRepository')->findInArrayLabel($dadosUser['agencias']);
                    $agencias[''] = 'Selecione uma Agência';
                    ksort($agencias);
                } else {
                    $agencias = false;
                }

                if (array_search('cemiterio', $dadosUser['filtros']) !== false) {
                    $cemiterios = $sm->get('Admin\Repository\CemiterioRepository')->findInArrayLabel($dadosUser['cemiterios']);
                    $cemiterios[''] = 'Selecione um Cemitério';
                    ksort($cemiterios);
                } else {
                    $cemiterios = false;
                }

                if (array_search('estoque', $dadosUser['filtros']) !== false) {
                    $estoques = $sm->get('Admin\Repository\EstoqueRepository')->findInArrayLabel($dadosUser['estoques']);
                    $estoques[''] = 'Selecione um Estoque';
                    ksort($estoques);
                } else {
                    $estoques = false;
                }

                if (array_search('trafego', $dadosUser['filtros']) !== false) {
                    $trafegos = $sm->get('Admin\Repository\TrafegoRepository')->findInArrayLabel($dadosUser['trafegos']);
                    $trafegos[''] = 'Selecione um Tráfego';
                    ksort($trafegos);
                } else {
                    $trafegos = false;
                }

                if (array_search('tesouraria', $dadosUser['filtros']) !== false) {
                    $tesourarias = $sm->get('Admin\Repository\TesourariaRepository')->findInArrayLabel($dadosUser['tesourarias']);
                    $tesourarias[''] = 'Selecione uma Tesouraria';
                    ksort($tesourarias);
                } else {
                    $tesourarias = false;
                }

                return new \Application\Form\Filtro(new \Application\InputFilter\Filtro(), $agencias, $cemiterios, $estoques, $trafegos, $tesourarias);

            },
        );
    }

    /**
     * Método que configura as dependências
     * @return array
     */
    public function getAuthServices()
    {
        return array(
            'Application\Auth\Adapter' => function ($sm) {
                return new \Application\Auth\Adapter($sm->get('Application/Repository/UsuarioRepository'), $sm->get('Application/Repository/PrivilegioRepository'), $sm->get('Application\Service\LogLogin'));
            },
            'Application\Security\Acl' => function ($sm) {
                //Busca os dados do usuário
                $dadosUser = $sm->get('DadosUser');

                //Instancia e retorna a ACL
                return new \Application\Security\Acl($dadosUser['perfil'], $dadosUser['privilegios']);
            }
        );
    }

    /**
     *
     * Retorna os services com as instâncias dos repositories
     * @return array
     */
    public function getRepositories()
    {
        return array(
            'Application\Repository\LogAlteracaoRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Application\Entity\LogAlteracao');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Application\Repository\LogLoginRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Application\Entity\LogLogin');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Application\Repository\PerfilRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Application\Entity\Perfil');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Application\Repository\PrivilegioRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Application\Entity\Privilegio');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Application\Repository\UsuarioRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Application\Entity\Usuario');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Application\Repository\FuncionarioRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Application\Entity\Funcionario');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Application\Repository\UfRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Application\Entity\Uf');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            },
            'Application\Repository\CidadeRepository' => function ($sm) {
                $repository = $sm->get('Doctrine\ORM\EntityManager')->getRepository('Application\Entity\Cidade');
                $repository->setLogger($sm->get('Base\Log\Log'));

                return $repository;
            }
        );
    }

    /**
     *
     * @return array
     */
    public function getFormsFilter()
    {
        return array();
    }

    /**
     *
     * Retorna as instâncias de services
     * @return array
     */
    public function getServices()
    {
        return array(
            'Application\Service\Usuario' => function ($sm) {
                $privilegioRepository = $sm->get('Application\Repository\PrivilegioRepository');
                $service = new \Application\Service\Usuario($sm->get('Doctrine\ORM\EntityManager'), new \Application\Entity\Usuario(), $sm->get('Base\Log\Log'));
                $service->setPrivilegioRepository($privilegioRepository);

                //Verifica se existe usuário logado
                if ($sm->get('ValidaAuth')) {
                    $service->setDadosUser($sm->get('DadosUser'));
                } else {
                    $service->setDadosUser(array());
                }

                $service->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());

                return $service;
            },
            'Application\Service\Perfil' => function ($sm) {
                $service = new \Application\Service\Perfil($sm->get('Doctrine\ORM\EntityManager'), new \Application\Entity\Perfil(), $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());


                return $service;
            },
            'Application\Service\LogLogin' => function ($sm) {
                return new \Application\Service\LogLogin($sm->get('Doctrine\ORM\EntityManager'), new \Application\Entity\LogLogin(), $sm->get('Base\Log\Log'));
            },
            'Application\Service\LogAlteracao' => function ($sm) {
                return new \Application\Service\LogAlteracao($sm->get('Doctrine\ORM\EntityManager'), new \Application\Entity\LogAlteracao(), $sm->get('Base\Log\Log'));
            },
            'Application\Service\Funcionario' => function ($sm) {
                $service = new \Application\Service\Funcionario($sm->get('Doctrine\ORM\EntityManager'), new \Application\Entity\Funcionario, $sm->get('Base\Log\Log'));
                $service->setDadosUser($sm->get('DadosUser'))
                    ->setLogAlteracao($sm->get('Application\Service\LogAlteracao'))
                    ->setDateTimeFormat(new \Base\Helper\DatetimeFormat());

                return $service;
            }
        );
    }

    /**
     *
     * Retorna os serviços básicos
     * @return array
     */
    public function getBaseServices()
    {
        return array(
            'SessionManager' => function ($sm) {
                $manager = new SessionManager();
                $manager->rememberMe(1800000);

                return $manager;
            },
            'DadosUser' => function ($sm) {
                //Busca e retorna os dados do usuário logado armazenado na sessão
                $sessionStorage = new \Zend\Authentication\Storage\Session('Application');
                return $sessionStorage->read();
            },
            'ValidaAuth' => function ($sm) {
                $sessionStorage = new \Zend\Authentication\Storage\Session('Application');
                $auth = new \Zend\Authentication\AuthenticationService();
                $auth->setStorage($sessionStorage);

                return $auth->hasIdentity();
            },
            'BaseConfig' => function ($sm) {
                return include __DIR__ . '/../../../../../config/autoload/global.php';
            }
        );
    }

    /**
     * Retorna os serviços do PHPOffice
     * @return array
     */
    public function getOfficeServices()
    {
        return array();
    }

    public function getMailConfig()
    {
        return array(
            'Application\Mail\LembrarSenha' => function ($sm) {
                return new \Application\Mail\LembrarSenha($sm->get('Mail'), $sm->get('ViewRenderer'));
            },
            'Application\Mail\NovaSenha' => function ($sm) {
                return new \Application\Mail\NovaSenha($sm->get('Mail'), $sm->get('ViewRenderer'));
            }
        );
    }

}
                