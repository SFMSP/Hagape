<?php
namespace Application\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;


/**
 * Classe Responsável por todas as consultas na tabela de logs de alterações
 *
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class LogAlteracaoRepository extends BaseRepository implements RepositoryInterface {


}
