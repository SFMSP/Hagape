<?php

namespace Application\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;
use Application\Entity\Privilegio;

/**
 * Classe Responsável por todas as consultas na tabela de logs de alterações
 *
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class PrivilegioRepository extends BaseRepository implements RepositoryInterface {

    /**
     * Consulta, monta e retorna o html com todos os privilégios do Application
     * @return String
     */
    public function getHtmlPrivilegios($idPerfil = null) {
        //Cria a variável que receberá o array com os privilégios
        $dados = array();

        //Monta o array
        foreach ($this->findAll() as $privilegio) {
            $dados[$privilegio->getFuncionalidade()->getNome()][$privilegio->getAcao()->getNome()] = $privilegio->getId();
        }

        //Cria a variável que receberá os privilégios do perfil
        $privilegiosPerfil = array();

        //Verifica se o perfil foi informado
        if (!is_null($idPerfil)) {
            //Monta o array
            foreach ($this->SelectPrivilegioPerfil($idPerfil) as $privilegio) {
                $privilegiosPerfil[$privilegio->getId()] = $privilegio->getId();
            }
        }

        //Monta o html das funcionálidades
        $html = "";
       
        $contar = 0;
        //for($i;$i>count($dados);$i++){
        foreach ($dados as $funcionalidade => $acoes) {
            $contar = $contar + 1;
            $html .= "<br>
    <div classe=\"caption portlet light bordered\">
    <input type=\"checkbox\" name=\"privilegio[]\" id=\"check-all\" class=\"grupo$contar\" />
    <span class=\"caption-subject font-blue-sharp bold uppercase\">{$funcionalidade}</span></div><div class=\"portlet-body\">";

 
            foreach ($acoes as $acao => $privilegio) {
                if (isset($privilegiosPerfil[$privilegio])) {
                    $html .= "<br /><input type='checkbox' name='privilegios[]' class='filho$contar' value={$privilegio} checked />&nbsp;{$acao}";
                } else {
                    $html .= "<br /><input type='checkbox' name='privilegios[]' class ='filho$contar' value={$privilegio} />&nbsp;{$acao}";
                }
            }
            $html.="</div><hr>";
        }

        return $html;
    }

    /**
     * Retorna todos os privilegios de um perfil
     * @param integer $idPerfil
     * @return array
     */
    public function SelectPrivilegioPerfil($idPerfil) {
        //Cria a query
        $dql = "SELECT pri FROM Application\Entity\Privilegio pri INNER JOIN pri.perfis pfl WHERE pfl.id = :id_perfil ";
        $query = $this->getEntityManager()->createQuery($dql);

        //Seta os parâmetros, executa e retorna os resultados
        $query->setParameter('id_perfil', $idPerfil);
        return $query->getResult();
    }

    /**
     * Retorna os filtros de um perfil
     * 
     * @param integer $perfil
     * @return array
     */
    public function getFiltrosPerfil($perfil) {

        $funcionalidades = $this->createQueryBuilder('f')
                ->join('f.perfis', 'p')
                ->where('p.id = :perfil')
                ->setParameter('perfil', $perfil)
                ->getQuery()
                ->getResult();
        $filtros = array();

        foreach ($funcionalidades as $func) {
            if ($func->getFiltro()) {
                switch ($func->getFiltro()->getId()) {
                    case Privilegio::FILTRO_AGENCIA: $filtros[Privilegio::FILTRO_AGENCIA]['filtro'] = 'agencia';
                        break;
                    case Privilegio::FILTRO_CEMITERIO: $filtros[Privilegio::FILTRO_CEMITERIO]['filtro'] = 'cemiterio';
                        break;
                    case Privilegio::FILTRO_ESTOQUE: $filtros[Privilegio::FILTRO_ESTOQUE]['filtro'] = 'estoque';
                        break;
                    case Privilegio::FILTRO_TESOURARIA: $filtros[Privilegio::FILTRO_TESOURARIA]['filtro'] = 'tesouraria';
                        break;
                    case Privilegio::FILTRO_TRAFEGO: $filtros[Privilegio::FILTRO_TRAFEGO]['filtro'] = 'trafego';
                        break;
                }
            }
        }

        return $filtros;
    }

}
