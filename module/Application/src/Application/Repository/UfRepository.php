<?php
namespace Application\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;


/**
 * Classe Responsável por todas as consultas na tabela de uf
 *
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class UfRepository extends BaseRepository implements RepositoryInterface {
    
    
}
