<?php

namespace Application\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;

/**
 * Classe reponsável por todas as consultas realizadas na entidade usuário
 *
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class FuncionarioRepository extends BaseRepository implements RepositoryInterface {
        
    /**
     * 
     * @param array $filtros
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function busca($filtros, $limit, $offset) {
        try {
            //Monta a query dql
            $dql = "SELECT u FROM {$this->getEntityName()} u WHERE 1 = 1 ";
            $parametros = array();

            if (!empty($filtros['busca'])) {
                $dql .= " AND (LOWER(u.nome) LIKE :nome) ";
                $parametros['nome'] = '%' . $filtros['busca'] . '%';
            }


            //Cria e executa query
            $query = $this->getEntityManager()->createQuery($dql);
            $query->setMaxResults($limit);
            $query->setFirstResult($offset);
            $query->setParameters($parametros);

            return $query->getResult();
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }

    /**
     * 
     * @param array $filtros
     * @return integer
     */
    public function buscaTotal($filtros) {
        try {
            //Monta a query dql
            $dql = "SELECT count(u) total FROM {$this->getEntityName()} u WHERE 1 = 1 ";
            $parametros = array();

            if (!empty($filtros['busca'])) {
                $dql .= " AND (LOWER(u.nome) LIKE :nome) ";
                $parametros['nome'] = '%' . $filtros['busca'] . '%';
            }
            //Cria e executa query
            $result = $this->getEntityManager()->createQuery($dql)->setParameters($parametros)->getResult();

            //Retorna o total para a query
            return $result[0]['total'];
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }
           

}
