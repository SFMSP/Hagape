<?php
namespace Application\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;


/**
 * Classe Responsável por todas as consultas na tabela de cidades
 *
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class CidadeRepository extends BaseRepository implements RepositoryInterface {

  

}
