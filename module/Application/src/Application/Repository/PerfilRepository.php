<?php
namespace Application\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;


/**
 * Description of PerfilRepository
 *
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class PerfilRepository extends BaseRepository implements RepositoryInterface {

    /**
     * 
     * @param array $filtros
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function buscaPerfil($filtros, $limit, $offset, $order = array()) {
        try {
            //Monta a query dql
            $dql = "SELECT p FROM {$this->getEntityName()} p WHERE 1 = 1 ";
            $parametros = array();

            if (!empty($filtros['busca'])) {
                $dql .= " AND (LOWER(p.nome) LIKE :nome) ";
                $parametros['nome'] = '%' . $filtros['busca'] . '%';
            }

            if (count($order)) {
                $dql .= " ORDER BY p.{$order['field']} {$order['order']} ";
            }

            //Cria e executa query
            $query = $this->getEntityManager()->createQuery($dql);
            $query->setMaxResults($limit);
            $query->setFirstResult($offset);
            $query->setParameters($parametros);

            return $query->getResult();
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }

    /**
     * 
     * @param array $filtros
     * @return integer
     */
    public function buscaPerfilTotal($filtros) {
        try {
            //Monta a query dql
            $dql = "SELECT count(p) total FROM {$this->getEntityName()} p WHERE 1 = 1 ";
            $parametros = array();

            if (!empty($filtros['busca'])) {
                $dql .= " AND (LOWER(p.nome) LIKE :nome) ";
                $parametros['nome'] = '%' . $filtros['busca'] . '%';
            }

            //Cria e executa query
            $result = $this->getEntityManager()->createQuery($dql)->setParameters($parametros)->getResult();

            //Retorna o total para a query
            return $result[0]['total'];
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }

}
