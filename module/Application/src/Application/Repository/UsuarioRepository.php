<?php

namespace Application\Repository;

use Base\Repository\BaseRepository;
use Base\Repository\RepositoryInterface;

/**
 * Classe reponsável por todas as consultas realizadas na entidade usuário
 *
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class UsuarioRepository extends BaseRepository implements RepositoryInterface {

    /**
     * Realiza as consultas de autenticação
     * 
     * @param string $login
     * @param string $senha
     * @return mixed
     */
    public function autenticar($login, $senha) {
        try {
            //Busca o usuário com o login informado
            $usuario = $this->findOneBy(array('login' => $login, 'ativo' => 1, 'excluido' => 0));
            
            //Verifica se foi encontrado
            if ($usuario) {
                return $this->verificarSenhaAutenticacao($usuario, $senha);
            } else {
                return false;
            }
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            return false;
        }
    }

    /**
     * Compara uma string de senha a de um objeto usuário
     * @param \Application\Entity\Usuario $usuario
     * @param string $senha
     * @return mixed
     */
    public function verificarSenhaAutenticacao(\Application\Entity\Usuario $usuario, $senha) {
        if ($usuario->encryptPassword($senha) == $usuario->getSenha()) {
            return $usuario;
        } else {
            return false;
        }
    }

    /**
     * 
     * @param array $filtros
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function buscaUsuario($filtros, $limit, $offset, $order = array()) {
        try {
            //Monta a query dql
            $dql = "SELECT u FROM Application\Entity\Usuario u WHERE u.excluido = 0 ";
            $parametros = array();

            if (!empty($filtros['busca'])) {
                $dql .= " AND (LOWER(u.nome) LIKE :nome OR LOWER(u.email) LIKE :email) ";
                $parametros['nome'] = '%' . $filtros['busca'] . '%';
                $parametros['email'] = '%' . $filtros['busca'] . '%';
            }

            if (!empty($filtros['perfil'])) {
                $dql .= " AND u.perfil = :perfil ";
                $parametros['perfil'] = $filtros['perfil'];
            }

            if (count($order)) {
                $dql .= " ORDER BY u.{$order['field']} {$order['order']} ";
            }

            //Cria e executa query
           
            $query = $this->getEntityManager()->createQuery($dql);
            $query->setMaxResults($limit);
            $query->setFirstResult($offset);
            $query->setParameters($parametros);

            return $query->getResult();
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }

    /**
     * 
     * @param array $filtros
     * @return integer
     */
    public function buscaUsuarioTotal($filtros) {
        try {
            //Monta a query dql
            $dql = "SELECT count(u) total FROM Application\Entity\Usuario u WHERE u.excluido = 0 ";
            $parametros = array();

            if (!empty($filtros['busca'])) {
                $dql .= " AND (LOWER(u.nome) LIKE :nome OR LOWER(u.email) LIKE :email) ";
                $parametros['nome'] = '%' . $filtros['busca'] . '%';
                $parametros['email'] = '%' . $filtros['busca'] . '%';
            }

            if (!empty($filtros['perfil'])) {
                $dql .= " AND u.perfil = :perfil ";
                $parametros['perfil'] = $filtros['perfil'];
            }

            //Cria e executa query
            $result = $this->getEntityManager()->createQuery($dql)->setParameters($parametros)->getResult();

            //Retorna o total para a query
            return $result[0]['total'];
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
        }
    }

}
