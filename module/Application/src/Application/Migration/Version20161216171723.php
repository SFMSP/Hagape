<?php

namespace Application\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20161216171723 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is autogenerated, please modify it to your needs
        $sqlo = 
           "INSERT INTO `tb_campo_tb_aba` (`id_aba_campo`,`id_campo`,`id_aba`) VALUES ('678','70','5');
            INSERT INTO `tb_campo_tb_aba` (`id_aba_campo`,`id_campo`,`id_aba`) VALUES ('679','1','5');
            INSERT INTO `tb_campo_tb_aba` (`id_aba_campo`,`id_campo`,`id_aba`) VALUES ('680','69','5');
            INSERT INTO `tb_processo_campo` (`campo_opcao`,`id_campo`,`id_processo`,`bool_obrigatorio`) VALUES ('','70','5',0);
            INSERT INTO `tb_processo_campo` (`campo_opcao`,`id_campo`,`id_processo`,`bool_obrigatorio`) VALUES ('','1','5',0);
            INSERT INTO `tb_processo_campo` (`campo_opcao`,`id_campo`,`id_processo`,`bool_obrigatorio`) VALUES ('','69','5',0);
            INSERT INTO `tb_categoria_produto` (`tipo_negocio`, `dt_cadastro`, `txt_nome`) VALUES (2, '2016-12-16 13:17:25', 'Tipo de Transporte');
            INSERT INTO `tb_categoria_produto` (`tipo_negocio`) VALUES (2);
            INSERT INTO `tb_servico` (`id_categoria`, `txt_nome`) VALUES (2, 'Aério');
            INSERT INTO `tb_servico` (`id_categoria`, `txt_nome`) VALUES (2, 'Terrestre');
            INSERT INTO `tb_servico` (`id_categoria`, `txt_nome`) VALUES (3, 'Luxo');
            INSERT INTO `tb_servico` (`id_categoria`, `txt_nome`) VALUES (3, 'Convencional');";
        $this->addSql($sqlo);
    }

    public function down(Schema $schema)
    {
        // this down() migration is autogenerated, please modify it to your needs

    }
}
