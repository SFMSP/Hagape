<?php

namespace Application\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20161227101744 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is autogenerated, please modify it to your needs
        $sqla = "INSERT INTO `tb_campo_tb_aba` (`id_campo`, `id_aba`) VALUES (1, 9);
                UPDATE `tb_campo_tb_aba` SET `id_aba_campo`=834 WHERE  `id_aba_campo`=0 AND `id_campo`=1 AND `id_aba`=9;

                INSERT INTO `tb_campo_tb_aba` (`id_aba_campo`, `id_campo`, `id_aba`) VALUES (835, 1, 2);
                INSERT INTO `tb_processo_campo` (`id_processo`, `id_campo`, `bool_obrigatorio`) VALUES (2, 1, 0);
                INSERT INTO `tb_campo_tb_aba` (`id_aba_campo`, `id_campo`, `id_aba`) VALUES (836, 136, 7);";
        $this->addSql($sqla);
    }

    public function down(Schema $schema)
    {
        // this down() migration is autogenerated, please modify it to your needs

    }
}
