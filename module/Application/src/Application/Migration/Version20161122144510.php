<?php

namespace Application\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20161122144510 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is autogenerated, please modify it to your needs
            $sqlt="INSERT INTO `tb_tipo_negocio` (`txt_tipo`) VALUES ('Produto');"
                . "INSERT INTO `tb_tipo_negocio` (`txt_tipo`) VALUES ('Seviço');"
                . "INSERT INTO `tb_categoria_produto` (`dt_cadastro`, `txt_nome`) VALUES ('2016-11-22 14:49:09', 'Tipo de Sepultamento');"
                . "UPDATE `tb_categoria_produto` SET `tipo_negocio`=2 WHERE  `id_categoria`=1;"
                    . "INSERT INTO `tb_servico` (`txt_nome`) VALUES ('Concessão');"
                    . "INSERT INTO `tb_servico` (`txt_nome`) VALUES ('Quadra geral (Terra)');"
                    . "INSERT INTO `tb_servico` (`txt_nome`) VALUES ('Quadra geral (Gaveta)');"
                    . "INSERT INTO `tb_servico` (`txt_nome`) VALUES ('Columbário');"
                    . "UPDATE `tb_servico` SET `id_categoria`=1 WHERE  `id_servico`=1;"
                    . "UPDATE `tb_servico` SET `id_categoria`=1 WHERE  `id_servico`=2;"
                    . "UPDATE `tb_servico` SET `id_categoria`=1 WHERE  `id_servico`=3;"
                    . "UPDATE `tb_servico` SET `id_categoria`=1 WHERE  `id_servico`=4;";
            $this->addSql($sqlt);
    }

    public function down(Schema $schema)
    {
        // this down() migration is autogenerated, please modify it to your needs

    }
}
