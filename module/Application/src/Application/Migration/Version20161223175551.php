<?php

namespace Application\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20161223175551 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is autogenerated, please modify it to your needs
$sqls ="UPDATE `tb_campo` SET `campo_variavel`='bandeira' WHERE  `id_campo`=117;
            UPDATE `tb_campo` SET `campo_variavel`='pinpad' WHERE  `id_campo`=119;
            INSERT INTO `tb_campo` (`txt_campo`, `campo_variavel`) VALUES ('Autorização', 'autorizacao');
            INSERT INTO `tb_campo` (`txt_campo`, `campo_variavel`) VALUES (' CV/DOC', 'cvdoc');
            UPDATE `tb_campo` SET `campo_variavel`='valor' WHERE  `id_campo`=122;
            UPDATE `tb_campo` SET `campo_variavel`='emitente' WHERE  `id_campo`=113;";
 $this->addSql($sqls);
    }

    public function down(Schema $schema)
    {
        // this down() migration is autogenerated, please modify it to your needs

    }
}
