<?php

namespace Application\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20161024131256 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $sql = "INSERT INTO `tb_funcionalidade` 
                (`id_funcionalidade`, `txt_nome`, `txt_controller`) 
                VALUES ('16', 'Contrato', 'contrato');
        
                INSERT INTO `tb_privilegio` (`id_privilegio`, `id_acao`, `id_funcionalidade`, `id_filtro`, `bool_banco`) 
                VALUES ('66', '1', '16', '3', '0');
                INSERT INTO `tb_privilegio` (`id_privilegio`, `id_acao`, `id_funcionalidade`, `id_filtro`, `bool_banco`) 
                VALUES ('67', '2', '16', '3', '1');
                INSERT INTO `tb_privilegio` (`id_privilegio`, `id_acao`, `id_funcionalidade`, `id_filtro`, `bool_banco`) 
                VALUES ('68', '3', '16', '3', '1');
                INSERT INTO `tb_privilegio` (`id_privilegio`, `id_acao`, `id_funcionalidade`, `id_filtro`, `bool_banco`) 
                VALUES ('69', '4', '16', '3', '1');
                INSERT INTO `tb_privilegio` (`id_privilegio`, `id_acao`, `id_funcionalidade`, `id_filtro`, `bool_banco`) 
                VALUES ('70', '5', '16', '3', '1');

                
                INSERT INTO `tb_privilegio_perfil` (`id_perfil`, `id_privilegio`) 
                VALUES ('1', '66');
                INSERT INTO `tb_privilegio_perfil` (`id_perfil`, `id_privilegio`) 
                VALUES ('1', '67');
                INSERT INTO `tb_privilegio_perfil` (`id_perfil`, `id_privilegio`) 
                VALUES ('1', '68');
                INSERT INTO `tb_privilegio_perfil` (`id_perfil`, `id_privilegio`) 
                VALUES ('1', '69');
                INSERT INTO `tb_privilegio_perfil` (`id_perfil`, `id_privilegio`) 
                VALUES ('1', '70');
                INSERT INTO `tb_privilegio_perfil` (`id_perfil`, `id_privilegio`) ;
               
               ";
        $this->addSql($sql);

    }

    public function down(Schema $schema)
    {
        // this down() migration is autogenerated, please modify it to your needs

    }
}
