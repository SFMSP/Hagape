<?php

namespace Application\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20161023233555 extends AbstractMigration {

    public function up(Schema $schema) {
        $sql = "INSERT INTO tb_funcionalidade VALUES (14, 'Pedidos', 'pedido');
                INSERT INTO tb_funcionalidade VALUES (15, 'Recebimentos de Pedidos', 'recebimento');

                INSERT INTO tb_acao VALUES (7, 'visualizar', 'Visualizar');
                INSERT INTO tb_acao VALUES (8, 'cancelarPedido', 'Cancelar Pedido');
                INSERT INTO tb_acao VALUES (9, 'enviarPedido', 'Enviar Pedido');
                INSERT INTO tb_acao VALUES (10, 'gerarPdf', 'Gerar PDF');
                INSERT INTO tb_acao VALUES (11, 'gerarAtestado', 'Gerar Atestado');

                INSERT INTO tb_privilegio VALUES (52, 1, 14, 3, 0);
                INSERT INTO tb_privilegio VALUES (53, 2, 14, 3, 0);
                INSERT INTO tb_privilegio VALUES (54, 3, 14, 3, 0);
                INSERT INTO tb_privilegio VALUES (55, 4, 14, 3, 0);
                INSERT INTO tb_privilegio VALUES (56, 7, 14, 3, 0);
                INSERT INTO tb_privilegio VALUES (57, 8, 14, 3, 0);
                INSERT INTO tb_privilegio VALUES (58, 9, 14, 3, 0);
                INSERT INTO tb_privilegio VALUES (59, 10, 14, 3, 0);
                INSERT INTO tb_privilegio VALUES (60, 1, 15, 3, 0);
                INSERT INTO tb_privilegio VALUES (61, 2, 15, 3, 0);
                INSERT INTO tb_privilegio VALUES (62, 3, 15, 3, 0);
                INSERT INTO tb_privilegio VALUES (63, 4, 15, 3, 0);
                INSERT INTO tb_privilegio VALUES (64, 7, 15, 3, 0);
                INSERT INTO tb_privilegio VALUES (65, 11, 15, 3, 0);

                INSERT INTO tb_privilegio_perfil VALUES (1, 52);
                INSERT INTO tb_privilegio_perfil VALUES (1, 53);
                INSERT INTO tb_privilegio_perfil VALUES (1, 54);
                INSERT INTO tb_privilegio_perfil VALUES (1, 55);
                INSERT INTO tb_privilegio_perfil VALUES (1, 56);
                INSERT INTO tb_privilegio_perfil VALUES (1, 57);
                INSERT INTO tb_privilegio_perfil VALUES (1, 58);
                INSERT INTO tb_privilegio_perfil VALUES (1, 59);
                INSERT INTO tb_privilegio_perfil VALUES (1, 60);
                INSERT INTO tb_privilegio_perfil VALUES (1, 61);
                INSERT INTO tb_privilegio_perfil VALUES (1, 62);
                INSERT INTO tb_privilegio_perfil VALUES (1, 63);
                INSERT INTO tb_privilegio_perfil VALUES (1, 64);
                INSERT INTO tb_privilegio_perfil VALUES (1, 65); ";
        
        $this->addSql($sql);
    }

    public function down(Schema $schema) {
        // this down() migration is autogenerated, please modify it to your needs
    }

}
