<?php

namespace Application\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161017024950 extends AbstractMigration {

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) {

        $sql = "
INSERT INTO `tb_acao` (`id_privilegio`, `txt_nome`, `txt_action`) VALUES
	(1, 'Listar', 'index'),
	(2, 'Incluir', 'incluir'),
	(3, 'Alterar', 'editar'),
	(4, 'Excluir', 'excluir'),
	(5, 'Ativar/Inativar', 'mudarStatus'),
	(6, 'Gerar Nova Senha', 'gerarNovaSenha');

INSERT INTO `tb_filtro` (`id_filtro`, `nome_filtro`) VALUES
	(1, 'Agência'),
	(2, 'Cemitério'),
	(3, 'Estoque'),
	(4, 'Tráfego'),
	(5, 'Tesouraria');

INSERT INTO `tb_funcionalidade` (`id_funcionalidade`, `txt_nome`, `txt_controller`) VALUES
	(1, 'CONFIGURAÇÃO - Perfis', 'perfil'),
	(2, 'CONFIGURAÇÃO - Usuários', 'usuario'),
	(3, 'CONFIGURAÇÃO - Agência', 'agencia'),
	(4, 'CONFIGURAÇÃO - Cemitério', 'cemiterio'),
	(5, 'CONFIGURAÇÃO - Estoque', 'estoque'),
	(6, 'CONFIGURAÇÃO - Tesouraria', 'tesouraria'),
	(7, 'CONFIGURAÇÃO - Tráfego', 'trafego'),
	(8, 'AGÊNCIA - Funcionalidade Demonstrativa', 'agencia_teste'),
	(9, 'CEMITÉRIO - Funcionalidade Demonstrativa', 'cemiterio_teste'),
	(10, 'ESTOQUE - Funcionalidade Demonstrativa', 'estoque_teste'),
	(11, 'TRÁFEGO - Funcionalidade Demonstrativa', 'trafego_teste'),
	(12, 'TESOURARIA - Funcionalidade Demonstrativa', 'tesouraria_teste');

INSERT INTO `tb_perfil` (`id_perfil`, `txt_perfil`) VALUES
	(1, 'Administrador'),
	(13, 'Visualizador');

INSERT INTO `tb_privilegio` (`id_privilegio`, `id_acao`, `id_funcionalidade`, `id_filtro`) VALUES
	(1, 1, 1, NULL),
	(2, 2, 1, NULL),
	(3, 3, 1, NULL),
	(4, 4, 1, NULL),
	(5, 1, 2, NULL),
	(6, 2, 2, NULL),
	(7, 3, 2, NULL),
	(8, 4, 2, NULL),
	(9, 1, 3, NULL),
	(10, 2, 3, NULL),
	(11, 3, 3, NULL),
	(12, 4, 3, NULL),
	(13, 1, 4, NULL),
	(14, 2, 4, NULL),
	(15, 3, 4, NULL),
	(16, 4, 4, NULL),
	(17, 1, 5, NULL),
	(18, 2, 5, NULL),
	(19, 3, 5, NULL),
	(20, 4, 5, NULL),
	(21, 1, 6, NULL),
	(22, 2, 6, NULL),
	(23, 3, 6, NULL),
	(24, 4, 6, NULL),
	(25, 1, 7, NULL),
	(26, 2, 7, NULL),
	(27, 3, 7, NULL),
	(28, 4, 7, NULL),
	(29, 5, 3, NULL),
	(31, 5, 4, NULL),
	(33, 5, 5, NULL),
	(35, 5, 6, NULL),
	(38, 5, 7, NULL),
	(39, 5, 2, NULL),
	(41, 6, 2, NULL),
	(42, 1, 8, NULL),
	(43, 1, 9, NULL),
	(44, 1, 10, 3),
	(45, 1, 11, NULL),
	(46, 1, 12, 5);

INSERT INTO `tb_privilegio_perfil` (`id_perfil`, `id_privilegio`) VALUES
	(1, 1),
	(1, 2),
	(1, 3),
	(1, 4),
	(1, 5),
	(1, 6),
	(1, 7),
	(1, 8),
	(1, 9),
	(1, 10),
	(1, 11),
	(1, 12),
	(1, 13),
	(1, 14),
	(1, 15),
	(1, 16),
	(1, 17),
	(1, 18),
	(1, 19),
	(1, 20),
	(1, 21),
	(1, 22),
	(1, 23),
	(1, 24),
	(1, 25),
	(1, 26),
	(1, 27),
	(1, 28),
	(1, 29),
	(1, 31),
	(1, 33),
	(1, 35),
	(1, 38),
	(1, 39),
	(1, 41),
	(1, 42),
	(1, 43),
	(1, 44),
	(1, 45),
	(1, 46),
	(13, 1),
	(13, 5);

INSERT INTO `tb_usuario` (`id_usuario`, `id_perfil`, `dt_cadastro`, `txt_nome`, `txt_email`, `txt_login`, `txt_senha`, `txt_salt`, `txt_cpf`, `txt_telefone`, `bool_ativo`, `bool_excluido`, `txt_avatar`, `bool_troca_senha`) VALUES
	(1, 1, '2016-08-10 01:44:24', 'User Teste', 'teste@teste.com', 'teste', 'QokFLOUfPhYjEZB2', '6UXzdl1seZs=', '016.836.010-17', '(51) 5564-5645', 1, 0, 'avatar.png', 0);
";

        $this->addSql($sql);
       
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) {
        // this down() migration is auto-generated, please modify it to your needs
    }

}
