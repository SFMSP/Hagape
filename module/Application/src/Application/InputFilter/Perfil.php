<?php
namespace Application\InputFilter;

use Base\Validator\BaseValidator;

/**
 * Classe que faz as validações do formulário de perfil
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Perfil extends BaseValidator {

    /**
     * Monta os filtros e as validações
     */
    public function __construct() {
        //Monta as validações de campos obrigatórios padrão
        $obrigatoriosPadrao = array(
            "nome" => "Campo obrigatório",
        );
        
        $this->addEmptyValidators($obrigatoriosPadrao);

       
    }

}
