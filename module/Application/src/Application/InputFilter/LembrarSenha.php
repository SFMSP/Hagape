<?php
namespace Application\InputFilter;

use Base\Validator\BaseValidator;

/**
 * Classe que faz as validações do formulário de lembrar senha
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class LembrarSenha extends BaseValidator {

    /**
     * Monta as validações
     */
    public function __construct() {
        $this->addEmptyValidators(array("user" => "Informe o usuario"));
    }

}
