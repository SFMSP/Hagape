<?php
namespace Application\InputFilter;

use Base\Validator\BaseValidator;

/**
 * Classe que faz as validações do formulário de alteração de senha
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class AlterarSenha extends BaseValidator {

    /**
     * Monta os filtros e as validações
     */
    public function __construct() {
        //Monta as validações de campos obrigatórios padrão
        $obrigatoriosPadrao = array(
            "senhaAntiga" => "Campo obrigatório",
            "novaSenha" => "Campo obrigatório",
            "confirmaSenha" => "Campo obrigatório",
        );
        
        $this->addEmptyValidators($obrigatoriosPadrao);

       
    }

}
