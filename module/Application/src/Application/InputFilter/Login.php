<?php
namespace Application\InputFilter;

use Base\Validator\BaseValidator;

/**
 * Classe que faz as validações do formulário de login
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Login extends BaseValidator {

    /**
     * Monta as validações
     */
    public function __construct() {
        $this->addEmptyValidators(array("user" => "Informe o usuário", "password" => "Informe a senha"));
    }

}
