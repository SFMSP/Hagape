<?php

namespace Application\InputFilter;

use Base\Validator\BaseValidator;

/**
 * Classe que faz as validações do formulário de filtros
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Filtro extends BaseValidator {

    /**
     * Monta os filtros e as validações
     */
    public function __construct() {
        //Monta as validações de campos obrigatórios padrão
        $obrigatoriosPadrao = array(
            "agencia" => "Campo obrigatório",
            "cemiterio" => "Campo obrigatório",
            "estoque" => "Campo obrigatório",
            "trafego" => "Campo obrigatório",
            "tesouraria" => "Campo obrigatório"
        );

        $this->addEmptyValidators($obrigatoriosPadrao);
    }

}
