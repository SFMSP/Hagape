<?php

namespace Application\InputFilter;

use Base\Validator\BaseValidator;
use Zend\InputFilter\FileInput;
use Zend\Filter\File\RenameUpload;
use Zend\Validator\File\MimeType;
use Zend\Validator\File\Size;
use Zend\Validator\File\ImageSize;

/**
 * Classe que faz as validações do formulário de avatar
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Avatar extends BaseValidator {

    /**
     * Monta os filtros e as validações
     */
    public function __construct() {

        $avatar = new FileInput('avatar');
        $avatar->setRequired(true);
        $avatar->getFilterChain()->attach(new RenameUpload(array(
            'target' => "./public/upload/avatar/avatar_",
            'randomize' => true,
            'use_upload_extension' => true,
            'use_upload_name' => true
        )));

        $uploadMaxSize = substr(ini_get('upload_max_filesize'), 0, -1) . "MB";

        //EXEMPLO DE VALIDAÇÕES ÚTEIS
        $avatar->getValidatorChain()
                ->attach(new \Zend\Validator\File\UploadFile(array('message' => 'Selecione uma Imagem')))
                ->attach(new MimeType(array('mimeType' => 'image/png,image/x-png,image/jpg,image/jpeg,image/gif', 'message' => 'A imagem precisa estar nos formatos PNG, JPG ou GIF')))
                ->attach(new Size(array('max' => $uploadMaxSize, 'message' => 'Imagem ultrapassa os limites de ' . $uploadMaxSize . ' permitidos')));


        $this->add($avatar);
    }

}
