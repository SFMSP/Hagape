<?php
namespace Application\Form;

use Zend\Form\Form;

/**
 * Classe que abstrai o formulário de perfil
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Perfil extends Form {

    /**
     * Monta o formulário de perfil
     * @param \Application\InputFilter\Perfil $inputFilter
     */
    public function __construct(\Application\InputFilter\Perfil $inputFilter) {
        //Seta o nome do formulário
        parent::__construct('perfil');

        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'form-horizontal form-bordered form-label-stripped'));
        $this->setInputFilter($inputFilter);

        //Adiciona o campo que recebe o login do usuário
        $this->add(array(
            'name' => 'nome',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'nome',
                'class' => 'form-control',
                'placeholder' => 'Informe o Perfil'
            ),
            'options' => array(
                'label' => 'Perfil',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
                
            )
        ));

        //Adiciona o campo hidden que leva o id
        $this->add(array(
            'name' => 'id',
            'type' => 'Zend\Form\Element\Hidden'
        ));

        //Monta o submit
        $this->add(array(
            'name' => 'enviar',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => 'SALVAR',
                'class' => 'btn btn-default blue'
            )
        ));
    }

}
