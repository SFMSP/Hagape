<?php
namespace Application\Form;

use Zend\Form\Form;

/**
 * Classe que abstrai o formulário de perfil
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Funcionario extends Form {

    /**
     * Monta o formulário de perfil
     * @param \Application\InputFilter\Perfil $inputFilter
     */
    public function __construct(\Application\InputFilter\Funcionario $inputFilter) {
        //Seta o nome do formulário
        parent::__construct('funcionario');

        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'stdform stdform2'));
        $this->setInputFilter($inputFilter);

        //Adiciona o campo que recebe o login do usuário
        $this->add(array(
            'name' => 'nome',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'nome',
            ),
            'options' => array(
                'label' => 'Nome do Perfil'
            )
        ));

        //Adiciona o campo hidden que leva o id
        $this->add(array(
            'name' => 'id',
            'type' => 'Zend\Form\Element\Hidden'
        ));

        //Monta o submit
        $this->add(array(
            'name' => 'enviar',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => 'ENVIAR',
                'class' => 'btn btn-primary submit-input-loading'
            )
        ));
    }

}
