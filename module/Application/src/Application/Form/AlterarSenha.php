<?php

namespace Application\Form;

use Zend\Form\Form;

/**
 * Classe que abstrai o formulário de alteração de senha
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class AlterarSenha extends Form {

    /**
     *
     * @var  \Application\Repository\UsuarioRepository  
     */
    protected $usuarioRepository;

    /**
     *
     * @var string
     */
    protected $usuario;

    /**
     * Monta o formulário de alteração de senha
     * @param \Application\InputFilter\Perfil $inputFilter
     */
    public function __construct(\Application\InputFilter\AlterarSenha $inputFilter, \Application\Repository\UsuarioRepository $usuarioReposotiry, $usuario) {
        //Seta o repository
        $this->usuarioRepository = $usuarioReposotiry;
        $this->usuario = $usuario;

        //Seta o nome do formulário
        parent::__construct('alterarSenha');

        //Seta o method do formulário
        $this->setAttribute('method', 'post');
        $this->setInputFilter($inputFilter);

        //Adiciona o campo da senha antiga
        $this->add(array(
            'name' => 'senhaAntiga',
            'type' => 'password',
            'attributes' => array(
                'id' => 'senhaAntiga'
            ),
            'options' => array(
                'label' => 'Informe a senha antiga',
            ),
        ));

        //Adiciona o campo de nova senha
        $this->add(array(
            'name' => 'novaSenha',
            'type' => 'password',
            'attributes' => array(
                'id' => 'novaSenha'
            ),
            'options' => array(
                'label' => 'Informe a nova senha',
            ),
        ));

        //Adiciona o campo para confirmar a nova senha
        $this->add(array(
            'name' => 'confirmaSenha',
            'type' => 'password',
            'attributes' => array(
                'id' => 'confirmaSenha'
            ),
            'options' => array(
                'label' => 'Confirme a nova senha',
            ),
        ));
    }

    /**
     * Sobescreve o método de validação
     */
    public function isValid() {
        //Faz a validação
        $valid = parent::isValid();


        //Armazena os elementos e os dados para fazer as validações complementares
        $elements = $this->getElements();
        $dados = $this->getData();

        //Valida se as novas senhas informadas estão iguais
        if ($dados['novaSenha'] !== $dados['confirmaSenha']) {
            $confirmaSenha = $elements['confirmaSenha'];
            $confirmaSenha->messages = array('naoConfere' => 'As senhas não conferem');
            $valid = false;
        }

        //Valida a senha antiga
        if (!empty($dados['senhaAntiga'])) {
            if (!$this->usuarioRepository->autenticar($this->usuario['login'], $dados['senhaAntiga'])) {
                $senhaAntiga = $elements['senhaAntiga'];
                $senhaAntiga->messages = array('errada' => 'A senha atual não está correta!');
                $valid = false;
            }
        }

        return $valid;
    }

}
