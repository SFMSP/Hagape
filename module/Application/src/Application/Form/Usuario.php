<?php

namespace Application\Form;

use Zend\Form\Form;

/**
 * Classe que abstrai o formulário de usuários
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Usuario extends Form {

    /**
     *
     * @var \Application\Repository\PrivilegioRepository 
     */
    private $privilegioRepository;

    /**
     *
     * @var \Application\Repository\UsuarioRepository 
     */
    private $usuarioRepository;

    /**
     * 
     * @param \Application\Repository\PrivilegioRepository $privilegioRepository
     * @return \Application\Form\Usuario
     */
    function setPrivilegioRepository(\Application\Repository\PrivilegioRepository $privilegioRepository) {
        $this->privilegioRepository = $privilegioRepository;
        return $this;
    }

    /**
     * 
     * @param \Application\Repository\UsuarioRepository $usuarioRepository
     * @return \Application\Form\Usuario
     */
    function setUsuarioRepository(\Application\Repository\UsuarioRepository $usuarioRepository) {
        $this->usuarioRepository = $usuarioRepository;
        return $this;
    }

    /**
     * Monta o formulário de usuários
     * @param array $perfis
     */
    public function __construct(\Application\InputFilter\Usuario $inputFilter, $perfis = array(), $agencias = array(), $cemiterios = array(), $estoques = array(), $trafego = array(), $tesouraria = array()) {
        //Seta o nome do formulário
        parent::__construct('usuario');

        $this->setLabel('Usuário');
        $this->setAttribute('icon', 'icon-user');
        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'form-horizontal form-bordered form-label-stripped'));

        $this->setInputFilter($inputFilter);

        //Adiciona o select de perfis
        $this->add(array(
            'name' => 'perfil',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'perfil'
            ),
            'options' => array(
                'label' => 'Perfil do usuário *:',
                'value_options' => $perfis,
                'disable_inarray_validator' => true,
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));

        //Adiciona o select de agencia
        $this->add(array(
            'name' => 'agencia',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'agencia',
                'multiple' => true
            ),
            'options' => array(
                'label' => 'Agência *:',
                'value_options' => $agencias,
                'disable_inarray_validator' => true,
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));

        //Adiciona o select de cemitério
        $this->add(array(
            'name' => 'cemiterio',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'cemiterio',
                'multiple' => true
            ),
            'options' => array(
                'label' => 'Cemitério *:',
                'value_options' => $cemiterios,
                'disable_inarray_validator' => true,
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));

        //Adiciona o select de estoque
        $this->add(array(
            'name' => 'estoque',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'estoque',
                'multiple' => true
            ),
            'options' => array(
                'label' => 'Estoque *:',
                'value_options' => $estoques,
                'disable_inarray_validator' => true,
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));

        //Adiciona o select de trafego
        $this->add(array(
            'name' => 'trafego',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'trafego',
                'multiple' => true
            ),
            'options' => array(
                'label' => 'Tráfego *:',
                'value_options' => $trafego,
                'disable_inarray_validator' => true,
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));

        //Adiciona o select de tesouraria
        $this->add(array(
            'name' => 'tesouraria',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'tesouraria',
                'multiple' => true
            ),
            'options' => array(
                'label' => 'Tesouraria *:',
                'value_options' => $tesouraria,
                'disable_inarray_validator' => true,
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));


        //Adiciona o campo que recebe o nome do usuário
        $this->add(array(
            'name' => 'nome',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'nome',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Nome *:',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));

        //Adiciona o campo que recebe o CPF
        $this->add(array(
            'name' => 'cpf',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'cpf',
                'class' => 'form-control cpf'
            ),
            'options' => array(
                'label' => 'CPF *:',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));

        //Adiciona o campo que recebe o telefone
        $this->add(array(
            'name' => 'telefone',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'telefone',
                'class' => 'form-control phone_with_ddd'
            ),
            'options' => array(
                'label' => 'Telefone:',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));


        //Adiciona o campo que recebe o e-mail
        $this->add(array(
            'name' => 'email',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'email',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Email:',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));


        //Adiciona o campo que recebe o login do usuário
        $this->add(array(
            'name' => 'login',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'id' => 'login',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Usuário RF *: ',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));

        //Adiciona o campo de senha do usuário
        $this->add(array(
            'name' => 'senha',
            'type' => 'Zend\Form\Element\Password',
            'attributes' => array(
                'id' => 'senha',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Senha *:',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));

        //Adiciona o campo de confirmação de senha
        $this->add(array(
            'name' => 'confirmaSenha',
            'type' => 'Zend\Form\Element\Password',
            'attributes' => array(
                'id' => 'confirmaSenha',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Confirmação da Senha *:',
                'class' => 'form-control',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label'
                )
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'ativo',
            'options' => array(
                'label' => 'Status *:',
                'label_attributes' => array(
                    'class' => 'col-md-3 control-label',
                    'style' => 'width: 100px; padding-top: 0;'
                ),
                'value_options' => array(
                    '1' => 'Ativo',
                    '0' => 'Inativo',
                ),
            ),
            'attributes' => array(
                'id' => 'status',
                'value' => 1
            )
        ));

        //Adiciona o campo hidden que leva o id
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'id' => 'id'
            ),
            'type' => 'Zend\Form\Element\Hidden'
        ));

        //Monta o submit
        $this->add(array(
            'name' => 'enviar',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => 'ENVIAR',
                'class' => 'btn btn-primary submit-input-loading'
            )
        ));
    }

    /**
     * Faz a validação do formulário
     */
    public function isValid() {

        $this->getInputFilter()->remove('agencia');
        $this->getInputFilter()->remove('cemiterio');
        $this->getInputFilter()->remove('estoque');
        $this->getInputFilter()->remove('trafego');
        $this->getInputFilter()->remove('tesouraria');

        //Armazena o valor 
        $id = $this->get('id')->getValue();

        //Verifica se o campo de id está preenchido para remover as validações das senhas
        if (!empty($id)) {
            $this->getInputFilter()->remove('senha');
            $this->getInputFilter()->remove('confirmaSenha');
        }

        //Faz as validações padrões do input filter
        $valid = parent::isValid();

        //Caso seja um formulário de inclusão, valida a senha e sua confirmação
        if (empty($id)) {
            if ($this->get('senha')->getValue() !== $this->get('confirmaSenha')->getValue()) {
                $valid = false;
                $this->get('confirmaSenha')->setMessages(array('As senhas devem ser iguais!'));
            }
        }

        //Busca os filtros do perfil selecionado 
        if (!empty($this->get('perfil')->getValue())) {
            $filtros = $this->privilegioRepository->getFiltrosPerfil($this->get('perfil')->getValue());

            foreach ($filtros as $filtro) {
                if (!$this->get($filtro['filtro'])->getValue()) {
                    $this->get($filtro['filtro'])->setMessages(array('Campo Obrigatório'));
                }
            }
        }

        //Verifica se o login já está sendo utilizado pelo usuário
        if (!empty($this->get('login')->getValue())) {
            $userLogin = $this->usuarioRepository->findOneBy(array('login' => $this->get('login')->getValue()));
  
            if ($userLogin) {
                if ($userLogin->getId() != $this->get('id')->getValue()) {
                    $valid = false;
                    $this->get('login')->setMessages(array('Esse usuário RF já está sendo utilizado por outro usuário'));
                }
            }
        }
        
        //Retora o resultado da validação
        return $valid;
    }

}
