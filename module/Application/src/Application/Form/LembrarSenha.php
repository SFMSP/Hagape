<?php
namespace Application\Form;

use Zend\Form\Form;

/**
 * Classe que abstrai o formulário de lembrar senha
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class LembrarSenha extends Form {

    /**
     *
     * @var \Application\Repository\UsuarioRepository
     */
    private $userRepository ;
    
    /**
     * 
     * @param \Application\Repository\UsuarioRepository $userRepository
     */
    public function setUserRepository(\Application\Repository\UsuarioRepository $userRepository) {
        $this->userRepository = $userRepository;
   
    }

        
    /**
     * Monta o formulário de login
     * @param \Application\InputFilter\Login $filter
     */
    public function __construct(\Application\InputFilter\LembrarSenha $filter) {
        parent::__construct('lembarSenha');

        $this->setInputFilter($filter);

        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'user',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'placeholder' => 'Informe o usuário',
            )
        ));

    }
    
    /**
     * Customiza a validação do formulário
     * 
     * @return boolean
     */
    public function isValid() {
        $valid = parent::isValid();
        
        //Verifica se o usuário informado está cadastrado
        if($this->get('user')->getValue()){
            $user = $this->userRepository->findByLogin($this->get('user')->getValue());
            
            if(!$user){
                $valid = false;
                $this->get('user')->setMessages(array('Usuário não encontrado!'));
            }
        }
        
        
        return $valid;
    }

}
