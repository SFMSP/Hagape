<?php

namespace Application\Form;

use Zend\Form\Form;

/**
 * Classe que abstrai o formulário de Login
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Login extends Form {

    /**
     * Monta o formulário de login
     * @param \Application\InputFilter\Login $filter
     */
    public function __construct(\Application\InputFilter\Login $filter) {
        parent::__construct('login');

        $this->setInputFilter($filter);
        
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'formulario-login');
        
        $this->add(array(
            'name' => 'user',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'placeholder' => 'Informe o usuário RF',
                'class' => 'form-control form-control-solid placeholder-no-fix',
            )
        ));

        $this->add(array(
            'name' => 'password',
            'type' => 'Zend\Form\Element\Password',
            'attributes' => array(
                'placeholder' => 'Informe a senha',
                'class' => 'form-control form-control-solid placeholder-no-fix',
            )
        ));
    }

}
