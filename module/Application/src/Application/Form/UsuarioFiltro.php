<?php
namespace Application\Form;

use Zend\Form\Form;

/**
 * Classe que abstrai o filtro na listagem de usuários
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class UsuarioFiltro extends Form {


    /**
     * Monta o formulário de filtro de vendas
     * @param array 
     */
    public function __construct(array $perfis) {
        //Seta o nome do formulário
        parent::__construct('usuario');

        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'stdform stdform2'));

        //Adiciona o select de clientes
        $this->add(array(
            'name' => 'perfil',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'perfil'
            ),
            'options' => array(
                'label' => 'Perfil: ',
                'value_options' => $perfis,
                'disable_inarray_validator' => true,
            ),
        ));
    }

}
