<?php
namespace Application\Form;

use Zend\Form\Form;

/**
 * Classe que abstrai o formulário de mudança da foto de perfil
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Avatar extends Form {

    /**
     * Monta o formulário de mudança de foto do perfil
     * @param \Application\InputFilter\Avatar $inputFilter
     */
    public function __construct(\Application\InputFilter\Avatar $inputFilter) {
        //Seta o nome do formulário
        parent::__construct('avatar');

        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'role' => 'form'));
        $this->setInputFilter($inputFilter);

        //Monta o campo de upload
        $this->add(array(
            'name' => 'avatar',
            'type' => 'Zend\Form\Element\File'
          
        ));

        //Monta o submit
        $this->add(array(
            'name' => 'enviar',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => 'SALVAR',
                'class' => 'btn btn-default blue'
            )
        ));
    }

}
