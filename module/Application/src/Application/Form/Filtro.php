<?php

namespace Application\Form;

use Zend\Form\Form;

/**
 * Classe que abstrai o formulário de seleção de filtros do login
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Filtro extends Form {

    /**
     * Monta o formulário de usuários
     * @param array $perfis
     */
    public function __construct(\Application\InputFilter\Filtro $inputFilter, $agencias = array(), $cemiterios = array(), $estoques = array(), $trafego = array(), $tesouraria = array()) {
        //Seta o nome do formulário
        parent::__construct('filtro');

        $this->setLabel('Filtros');
        $this->setAttribute('icon', 'icon-user');
        //Seta o method do formulário
        $this->setAttributes(array('method' => 'post', 'class' => 'form-horizontal form-bordered form-label-stripped'));

        $this->setInputFilter($inputFilter);


        if ($agencias !== false) {
            //Adiciona o select de agencia
            $this->add(array(
                'name' => 'agencia',
                'type' => 'Zend\Form\Element\Select',
                'attributes' => array(
                    'class' => 'form-control',
                    'id' => 'agencia'
                ),
                'options' => array(
                    'label' => 'Agência *:',
                    'value_options' => $agencias,
                    'disable_inarray_validator' => true,
                    'label_attributes' => array(
                        'class' => 'col-md-3 control-label'
                    )
                ),
            ));
        }else{
            $this->getInputFilter()->remove('agencia');
        }

        if ($cemiterios !== false) {
            //Adiciona o select de cemitério
            $this->add(array(
                'name' => 'cemiterio',
                'type' => 'Zend\Form\Element\Select',
                'attributes' => array(
                    'class' => 'form-control',
                    'id' => 'cemiterio',
                ),
                'options' => array(
                    'label' => 'Cemitério *:',
                    'value_options' => $cemiterios,
                    'disable_inarray_validator' => true,
                    'label_attributes' => array(
                        'class' => 'col-md-3 control-label'
                    )
                ),
            ));
        }else{
            $this->getInputFilter()->remove('cemiterio');
        }

        if ($estoques !== false) {

            //Adiciona o select de estoque
            $this->add(array(
                'name' => 'estoque',
                'type' => 'Zend\Form\Element\Select',
                'attributes' => array(
                    'class' => 'form-control',
                    'id' => 'estoque',
                ),
                'options' => array(
                    'label' => 'Estoque *:',
                    'value_options' => $estoques,
                    'disable_inarray_validator' => true,
                    'label_attributes' => array(
                        'class' => 'col-md-3 control-label'
                    )
                ),
            ));
        }else{
            $this->getInputFilter()->remove('estoque');
        }

        if ($trafego !== false) {

            //Adiciona o select de trafego
            $this->add(array(
                'name' => 'trafego',
                'type' => 'Zend\Form\Element\Select',
                'attributes' => array(
                    'class' => 'form-control',
                    'id' => 'trafego'
                ),
                'options' => array(
                    'label' => 'Tráfego *:',
                    'value_options' => $trafego,
                    'disable_inarray_validator' => true,
                    'label_attributes' => array(
                        'class' => 'col-md-3 control-label'
                    )
                ),
            ));
        }else{
            $this->getInputFilter()->remove('trafego');
        }

        if ($tesouraria !== false) {

            //Adiciona o select de tesouraria
            $this->add(array(
                'name' => 'tesouraria',
                'type' => 'Zend\Form\Element\Select',
                'attributes' => array(
                    'class' => 'form-control',
                    'id' => 'tesouraria'
                ),
                'options' => array(
                    'label' => 'Tesouraria *:',
                    'value_options' => $tesouraria,
                    'disable_inarray_validator' => true,
                    'label_attributes' => array(
                        'class' => 'col-md-3 control-label'
                    )
                ),
            ));
        }else{
            $this->getInputFilter()->remove('tesouraria');
        }
    }

}
