<?php
namespace Application\Security;

use Zend\Permissions\Acl\Acl as ClassAcl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;

/**
 * Classe responsável por mapear o permissionamento
 *
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Acl extends ClassAcl{
    
    /**
     * Atributo que recebe o perfil do usuário
     * @var string 
     */
    protected $role;
    
    /**
     * Atributo que recebe o array de funcionálidades do sistema
     * @var array 
     */
    protected $funcionalidades;
    
    /**
     * Atributo que recebe o array de privilégios
     * @var array 
     */
    protected $privilegios;
    
    /**
     * Método construtor que recebe e seta os valores para os atributos da classe
     * @param string $perfil
     * @param array $privilegios
     */
    public function __construct($perfil, array $privilegios) {
        //Seta o perfil do usuário (role)
        $this->role = $perfil;
        
        //Seta as funcionálidades
        foreach($privilegios as $funcionalidade => $acoes){
            $this->funcionalidades[$funcionalidade] = $funcionalidade;
        }
        
        //Seta os privilégios
        $this->privilegios = $privilegios;
        
        //Carrega as roles, resources e privileges
        $this->loadRole();
        $this->loadResource();
        $this->loadPrivileges();
    
    }
    
    /**
     * Carrega o perfil do usuário
     */
    public function loadRole(){
        $this->addRole(new Role($this->role));
    }
    
    /**
     * Carrega as funcionálidades
     */
    public function loadResource(){
        foreach($this->funcionalidades as $funcionalidade){
            $this->addResource(new Resource($funcionalidade));
        }
    }
    
    /**
     * Carrega os privilégios do perfil do usuário
     */
    public function loadPrivileges(){
        //Percorre o array com os privilégios
        foreach($this->privilegios as $funcionalidade => $privilegios){
            
            //Percorre os privilégios da funcionálidade
            foreach($privilegios as $privilegio => $permissao){
                //Concede ou não acesso a página
                if($permissao){
                    $this->allow($this->role, $funcionalidade, $privilegio);
                }else{
                    $this->deny($this->role, $funcionalidade, $privilegio);
                }
            }
        }
    }
    
    
    /**
     * 
     * @param string $controller
     * @param string $action
     * @return \Sistema\Security\Acl 
     */
    public function verifyPermission($controller, $action){
        return $this->isAllowed($this->role, $controller, $action);
    }
   
}
