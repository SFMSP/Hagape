<?php

namespace Application\Controller;

use Base\Controller\BaseController;
use Zend\View\Model\ViewModel;
use Zend\Json\Json;

/**
 * Possui métodos que podem ser usados em qualquer controller do módulo ciemar
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
abstract class CrudApplicationController extends BaseController
{

    /**
     *
     * @var string
     */
    protected $nameRepository;

    /**
     *
     * @var string
     */
    protected $nameService;

    /**
     *
     * @var string
     */
    protected $controller;

    /**
     *
     * @var string
     */
    protected $nameFormFilter;

    /**
     *
     * @var string
     */
    protected $route;

    /**
     *
     * @var array
     */
    protected $jsIndex = array();

    /**
     *
     * @var array
     */
    protected $cssIndex = array();

    /**
     *
     * @var array
     */
    protected $jsFormulario = array();

    /**
     *
     * @var array
     */
    protected $cssFormulario = array();

    /**
     *
     * @var string
     */
    protected $msgDeleteSuccess = 'Registros excluídos com sucesso!';

    /**
     *
     * @var string
     */
    protected $msgDeleteError = 'Erro ao excluir registros!';

    /**
     *
     * @var string
     */
    protected $msgInsertSuccess = 'Registro inserido com sucesso!';

    /**
     *
     * @var string
     */
    protected $msgUpdateSuccess = 'Registro atualizado com sucesso!';

    /**
     *
     * @var string
     */
    protected $msgUpdateError = 'Erro ao atualizar registros!';

    /**
     * @var string
     */
    protected $logicDelete = false;

    /**
     *
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        //Seta os scripts e retorna para view
        if (!empty($this->jsIndex)) {
            $this->layout()->setVariable('scripts', $this->jsIndex);
        }

        if (!empty($this->cssIndex)) {
            $this->layout()->setVariable('styles', $this->cssIndex);
        }

        //Busca mensagens a serem exibidas na tela
        $messages = $this->flashMessenger()->setNamespace('Application')->getMessages();

        //Busca o formulário do filtro
        $formFilter = !empty($this->nameFormFilter) ? $this->getServiceLocator()->get($this->nameFormFilter) : null;

        //Retorna os dados para view
        return new ViewModel(array('messages' => $messages, 'formFilter' => $formFilter));
    }

    /**
     *
     * Método responsável por deletar um registro de uma determinada entidade e retornar o resultado
     * da operação via json
     * @return string
     */
    public function deletarAction()
    {
        //Instancia o service
        $service = $this->getServiceLocator()->get($this->nameService);

        //Busca os dados
        $dados = $this->getRequest()->getPost();

        $delete = $this->logicDelete ? $service->deleteAll($dados['ids'], true) : $service->deleteAll($dados['ids']);

        //Deleta os dados
        if ($delete) {
            echo \Zend\Json\Json::encode(array('success' => 1, 'msg' => $this->msgDeleteSuccess));
        } else {
            echo \Zend\Json\Json::encode(array('success' => 0, 'msg' => $this->msgDeleteError));
        }
        die();
    }

    /**
     *
     * Método responsável por ativar/desativar em massa
     * da operação via json
     * @return string
     */
    public function mudarStatusAction()
    {
        //Instancia o service
        $service = $this->getServiceLocator()->get($this->nameService);

        //Busca os dados
        $dados = $this->getRequest()->getPost();

        //Muda os status
        $mudaStatus = $service->mudarStatus($dados['ids'], $dados['status']);

        //Deleta os dados
        if ($mudaStatus) {
            echo \Zend\Json\Json::encode(array('success' => 1, 'msg' => $this->msgUpdateSuccess));
        } else {
            echo \Zend\Json\Json::encode(array('success' => 0, 'msg' => $this->msgUpdateError));
        }
        die();
    }

    /**
     * Método padrão de formulário
     * @return \Application\Controller\ViewModel
     */
    public function formularioAction()
    {

        //Instancia o formulário e a requisição
        $form = $this->getServiceLocator()->get($this->nameForm);
        $request = $this->getRequest();

        //Verifica se a requisição foi feita via post
        if ($request->isPost()) {

            //Busca os dados
            $dados = $request->getPost()->toArray();
            if ($this->params()->fromRoute('id')) {
                $dados['id'] = $this->params()->fromRoute('id');
            }
            $form->setData($dados);

            //Valida o formulário
            if ($form->isValid()) {
                $service = $this->getServiceLocator()->get($this->nameService);
                $success = $service->save($dados);
                $this->setConfirmMessages($success);

                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            } else {
                $this->flashMessenger()->addErrorMessage('Por favor, verifique os campos do formulário');
            }
        } elseif ($this->params()->fromRoute('id')) {

            //Busca os dados
            $dados = $this->getServiceLocator()->get($this->nameRepository)->find($this->params()->fromRoute('id'));
            if ($dados) {
                $form->setData($dados->toArray());
            }
        }

        //Seta os scripts e retorna para view
        if (!empty($this->jsFormulario)) {
            $this->layout()->setVariable('scripts', $this->jsFormulario);
        }

        if (!empty($this->cssFormulario)) {
            $this->layout()->setVariable('styles', $this->cssFormulario);
        }


        //Retorna os dados da view Model
        return new ViewModel(array('form' => $form));
    }

    /**
     * Método que seta a mensagem de confirmação de inclusão ou edição
     * @param boolean $success
     */
    public function setConfirmMessages($success = true)
    {
        if ($this->params()->fromRoute('id')) {
            $this->flashMessenger()->addSuccessMessage($this->msgUpdateSuccess);
        } else {
            $this->flashMessenger()->addSuccessMessage($this->msgInsertSuccess);
        }
    }

    /**
     * Método que retorna a ACL
     * @return \Application\Security\Acl
     */
    public function getAcl()
    {
        return $this->getServiceLocator()->get('Application\Security\Acl');
    }

    /**
     * Retorna o js da mensagem de retorno da requisição
     *
     * @param string $msg
     * @param boolean $success
     * @return string
     */
    public function getAlert($msg, $success)
    {

        //Cria a mensagem de alerta. O parâmetro success pode ser utilizado para se definir se o alerta é o alerta é para mensagem de sucesso ou fracasso.
        $js = "<script type='text/javascript'>";
        $js .= "jQuery.growl({ title: 'Grow', message: 'The kitten is awake!' });";
        $js .= "</script>";

        return $js;
    }

    /**
     * Verifica se o usuário possui permissão
     *
     * @param string $controller
     * @param string $action
     * @return boolean
     */
    public function verifyPermission($controller, $action)
    {
        return $this->getServiceLocator()->get('Application\Security\Acl')->verifyPermission($controller, $action);
    }

    /**
     * Retorna todas as cidades de um estado no formato passado por parâmetro
     *
     * @param boolean $estado
     * @param boolean $json
     * @return mixed
     */
    public function getCidadesAction($estado = false, $json = true)
    {

        //print_r($estado);die;
        if (!$estado) {
            //Busca os dados da requisição
            $request = $this->getRequest()->getPost()->toArray();
            $estado = $request['estado'];
        }

        //Busca as cidades
        $cidades = $this->getServiceLocator()->get('Application\Repository\CidadeRepository')->findBy(array('uf' => $estado));

        if ($json) {
            foreach ($cidades as $k => $cidade) {
                $array[$k]['id'] = $cidade->getId();
                $array[$k]['nome'] = $cidade->getNome();
            }
        } else {
            foreach ($cidades as $cidade) {
                $array[$cidade->getId()] = $cidade->getNome();
            }
        }


        if ($json) {
            echo Json::encode($array);
            die();
        } else {
            return $array;
        }
    }

    /**
     * Método que retorna os dados do usuário
     *
     * @return type
     */
    protected function getDadosUser()
    {
        return $this->getServiceLocator()->get('dadosUser');
    }

    /**
     * Método que retorna o filtro selecionado no login
     *
     * @return type
     */
    protected function getFiltrosSelecionadosUser()
    {
        $dadosUser = $this->getDadosUser();
        return $dadosUser['filtrosSelecionados'];
    }

    /**
     * Retorna todos os contratos de fornecedor
     *
     * @param boolean $fornecedor
     * @param boolean $json
     * @return mixed
     */
    public function getContratosAction($fornecedor = false, $json = true, $label = false)
    {
        if (!$fornecedor) {
            //Busca os dados da requisição
            $request = $this->getRequest()->getPost()->toArray();
            $fornecedor = $request['fornecedor'];
        }

        //Busca as cidades
        $contratos = $this->getServiceLocator()->get('Estoque\Repository\ContratoRepository')->findBy(array('fornecedor' => $fornecedor));
        $contratos = $contratos ? $contratos : array();
        $array = array();


        if ($json) {

            $k = 0;

            if ($label) {
                $array[$k]['id'] = '';
                $array[$k]['nome'] = $label;
                $k++;
            }

            foreach ($contratos as $contrato) {
                $array[$k]['id'] = $contrato->getId();
                $array[$k]['nome'] = $contrato->getNumContrato();
                $k++;
            }
        } else {

            if ($label) {
                $array[''] = $label;
            }

            foreach ($contratos as $contrato) {
                $array[$contrato->getId()] = $contrato->getNumContrato();
            }
        }


        if ($json) {
            echo Json::encode($array);
            die();
        } else {
            return $array;
        }
    }

    /**
     * Retorna todos os contratos de fornecedor
     *
     * @param boolean $fornecedor
     * @param boolean $json
     * @return mixed
     */
    public function getProdutosAction($produto = false, $json = true, $label = false)
    {

        $request = $this->getRequest()->getPost()->toArray();


        //Busca as cidades
        $produto = $this->getServiceLocator()->get('Estoque\Repository\ProdutoRepository')->findBy(array('id' => $request['produto']));

        $produtos = $produto ? $produto : array();
        $array = array();

        if ($json) {

            $k = 0;

            if ($label) {
                $array[$k]['id'] = '';
                $array[$k]['nome'] = $label;
                $k++;
            }

            foreach ($produtos as $produto) {

                $unidades = $this->getServiceLocator()->get('Estoque\Repository\UnidadeMedidaRepository')->findBy(array('id' => $produto->getMedida()));
                foreach ($unidades as $unidade) {
                    $unidade = $unidade->getMedida();
                }

                $array['medida'] = $unidade;


                $array++;
            }
        } else {

            if ($label) {
                $array[''] = $label;
            }

            foreach ($produtos as $produto) {
                $array[$produto->getId()] = $produto->getId();
            }
        }


        if ($json) {
            echo Json::encode($array);
            die();
        } else {
            return $array;
        }
    }

    /**
     * Retorna todos os empenhos de um contrato
     *
     * @param boolean $contrato
     * @param boolean $json
     * @return mixed
     */
    public function getEmpenhosAction($contrato = false, $json = true, $label = false)
    {
        if (!$contrato) {
            //Busca os dados da requisição
            $request = $this->getRequest()->getPost()->toArray();
            $contrato = $request['contrato'];
        }

        //Busca as cidades
        $empenhos = $this->getServiceLocator()->get('Estoque\Repository\EmpenhoRepository')->findBy(array('contrato' => $contrato));
        $empenhos = $empenhos ? $empenhos : array();
        $array = array();

        if ($json) {

            $k = 0;

            if ($label) {
                $array[$k]['id'] = '';
                $array[$k]['nome'] = $label;
                $k++;
            }

            foreach ($empenhos as $empenho) {
                $array[$k]['id'] = $empenho->getId();
                $array[$k]['nome'] = $empenho->getNumEmpenho();
                $array[$k]['valor'] = $empenho->getValor();
                $k++;
            }
        } else {

            if ($label) {
                $array[''] = $label;
            }

            foreach ($empenhos as $empenho) {
                $array[$empenho->getId()] = $empenho->getNumEmpenho();
            }
        }


        if ($json) {
            echo Json::encode($array);
            die();
        } else {
            return $array;
        }
    }

    /**
     * Retorna o valor de empenho disponível para uso via json para uma requisição ajax
     */
    public function getEmpenhoDisponivelAction($empenho = false, $json = true, $pedidoRetirado = false)
    {

        if (!$empenho) {
            $request = $this->getRequest()->getPost()->toArray();
            $empenho = $request['empenho'];
        }

        $valorDisponivel = $this->getServiceLocator()->get('Estoque\Repository\EmpenhoRepository')->getEmpenhoDisponivelAtual($empenho, $pedidoRetirado);

        if ($json) {
            echo Json::encode(array('valorDisponivel' => $valorDisponivel));
            die();
        } else {
            return $valorDisponivel;
        }
    }

    /**
     * Método chamado via ajax que retorna os dados de um produto
     */
    public function getDadosProdutoAction()
    {
        $request = $this->getRequest()->getPost()->toArray();
        $produto = $this->getServiceLocator()->get('Estoque\Repository\ProdutoRepository')->find($request['produto']);

        if ($produto) {
            die(\Zend\Json\Json::encode($produto->toArray()));
        } else {
            die(\Zend\Json\Json::encode(array()));
        }
    }

    /**
     * Método chamado para montar a situação
     */
    public function getLabelSituacao($situacao)
    {
        $class = "";
        switch ($situacao) {

            case 'Cadastrado':
                $class = 'label-situacao-cadastrado';
                break;
            case 'Em Trânsito':
                $class = 'label-situacao-transito';
                break;
            case 'Encerrado':
                $class = 'label-situacao-encerrado';
                break;
            case 'Pendente':
                $class = 'label-situacao-pendente';
                break;
            case 'Cancelado':
                $class = 'label-situacao-cancelado';
                break;
            case 'Solicitado':
                $class = 'label-situacao-solicitado';
                break;
            case 'Concluído':
                $class = 'label-situacao-concluido';
                break;
            case 'Recebimento Incompleto':
                $class = 'label-situacao-pendente';
                break;
            case 'Descartado':
                $class = 'label-situacao-concluido';
                break;


        }
        return $class;


    }

    public function getNotasAction($nota)
    {
        print_r($nota);
        die;
        //Busca as cidades
        $notas = $this->getServiceLocator()->get('Agencia\Repository\ContratacaoRepository')->notasComplementares(array('fornecedor' => $fornecedor));
        //$nota = $contratos ? $contratos : array();
    }

    
    function medicosAction()
    {

        $response = $this->getResponse();
        $request = $this->getRequest()->getPost();

        $busca = array(
            "busca" => mb_strtolower($request['medico'], 'UTF-8')
        );

        $order = array();

        $registros = $this->getServiceLocator()->get('Agencia\Repository\MedicoRepository')->busca($busca, $request['length'], $request['start'], $order);
       
        $array = array();

        $k = 0;
        foreach ($registros as $registro) {

            $array[$k]['medico1'] = $registro->getNomeMedico();
            $array[$k]['crm1'] = $registro->getCrm();
            $k++;
        }

        $response->setContent(\Zend\Json\Json::encode($array));
        return $response;


    }

    function localFalecimentoAction()
    {
        $response = $this->getResponse();
        $request = $this->getRequest()->getPost();

        $busca = array(
            "busca" => mb_strtolower($request['localFalecimento'], 'UTF-8')
        );

        $order = array();

        $registros = $this->getServiceLocator()->get('Agencia\Repository\LocalFalecimentoRepository')->busca($busca, $request['length'], $request['start'], $order);

        $array = array();

        $k = 0;
        foreach ($registros as $registro) {
            $cartorios = $this->getServiceLocator()->get('Agencia\Repository\CartorioRepository')->findBy(array('id' => $registro->getCartorio()));
            foreach ($cartorios as $cartorio) {
                $cart = $cartorio->getNome();
                $cartId =$cartorio->getId();
            }
            $array[$k]['localFalecimento'] = $registro->getLocal();
            $array[$k]['enderecoFalecimento'] = $registro->getEndereco();
            $array[$k]['id'] = $registro->getId();
            $array[$k]['cartorio'] = $cart;
            $array[$k]['cartorioId'] = $cartId;
            $k++;
        }

        $response->setContent(\Zend\Json\Json::encode($array));
        return $response;

    }

    
    function localVelorioAction()
    {
        $response = $this->getResponse();
        $request = $this->getRequest()->getPost();

        $busca = array(
            "busca" => mb_strtolower($request['localVelorio'], 'UTF-8')
        );

        $order = array();

        $registros = $this->getServiceLocator()->get('Agencia\Repository\VelorioRepository')->busca($busca, $request['length'], $request['start'], $order);

        $array = array();

        $k = 0;
        foreach ($registros as $registro) {

            $array[$k]['localVelorio'] = $registro->getLocalVelorio();
            $array[$k]['enderecoVelorio'] = $registro->getEndereco();

            $k++;
        }

        $response->setContent(\Zend\Json\Json::encode($array));
        return $response;

    }

    
    function crematorioAction()
    {
        $response = $this->getResponse();
        $request = $this->getRequest()->getPost();

        $busca = array(
            "busca" => mb_strtolower($request['crematorio'], 'UTF-8')
        );

        $order = array();

        $registros = $this->getServiceLocator()->get('Admin\Repository\CemiterioRepository')->buscarCrematorio($busca, $request['length'], $request['start'], $order);

        $array = array();

        $k = 0;
        foreach ($registros as $registro) {

            $array[$k]['endereco'] = $registro->getEndereco();
            $array[$k]['idCrematorio'] = $registro->getId();
            

            $k++;
        }

        $response->setContent(\Zend\Json\Json::encode($array));
        return $response;

    }

    function cemiterioAction()
    {
        $response = $this->getResponse();
        $request = $this->getRequest()->getPost();

        $busca = array(
            "busca" => mb_strtolower($request['cemiterio'], 'UTF-8')
        );
        //print_r($busca);die;
        $order = array();
        $registros = $this->getServiceLocator()->get('Admin\Repository\CemiterioRepository')->buscarCemiterio($busca, $request['length'], $request['start'], $order);
        $array = array();

        $k = 0;
        foreach ($registros as $registro) {

            $array[$k]['endereco'] = $registro->getEndereco();
            $array[$k]['idCemiterio'] = $registro->getId();

            $k++;
        }

        $response->setContent(\Zend\Json\Json::encode($array));
        return $response;

    }


    public function getDadosPorCpfAction()
    {
        $request = $this->getRequest()->getPost()->toArray();
        $arRetorno = array();
        if ($request['cpf'] && (strlen($request['cpf']) == 13)) {
            $contratante = $this->getServiceLocator()->get('Agencia\Repository\ContratanteRepository')->findOneBy(['cpf' => $request['cpf']]);
            if ($contratante) {
                $arRetorno['contratante'] = $contratante->toArray();

                $dadosLocalizacao = $this->getServiceLocator()->get('Agencia\Repository\LocalizacaoRepository')->findOneBy(['contratante' => $arRetorno['contratante']['id']]);
                if ($dadosLocalizacao) {
                    $estadoCidade = $this->getServiceLocator()->get('Application\Repository\CidadeRepository')->findOneBy(['id' => $dadosLocalizacao->getCidade()]);
                    $arRetorno['localizacao'] = $dadosLocalizacao->toArray();
                    $arRetorno['localizacao']['cidade'] = $dadosLocalizacao->getCidade();
                    $arRetorno['localizacao']['estado'] = $estadoCidade->getUf()->getId();
                }

                $dadosEmpresa = $this->getServiceLocator()->get('Agencia\Repository\DadosEmpresaRepository')->findOneBy(['contratante' => $arRetorno['contratante']['id']]);

                if ($dadosEmpresa) {
                    $arRetorno['empresa'] = $dadosEmpresa->toArray();
                }
            }

            if ($contratante) {
                die(\Zend\Json\Json::encode($arRetorno));
            } else {
                die(\Zend\Json\Json::encode(array()));
            }

        } else {
            die(\Zend\Json\Json::encode(array()));
        }

    }
    public function getDadosPorCpfDeclaranteAction()
    {
        $request = $this->getRequest()->getPost()->toArray();
        $arRetorno = array();
        
        if ($request['cpf'] && (strlen($request['cpf']) == 14)) {
           
            $declarante = $this->getServiceLocator()->get('Agencia\Repository\DeclaranteRepository')->findOneBy(['cpf' => $request['cpf']]);
            
            if ($declarante) {
                $arRetorno['declarante'] = $declarante->toArray();
                
                $dadosLocalizacao = $this->getServiceLocator()->get('Agencia\Repository\LocalizacaoRepository')->findOneBy(['declarante' => $arRetorno['declarante']['id']]);
               
                
                if ($dadosLocalizacao) {
                    $estadoCidade = $this->getServiceLocator()->get('Application\Repository\CidadeRepository')->findOneBy(['id' => $dadosLocalizacao->getCidade()]);
                    $arRetorno['localizacao'] = $dadosLocalizacao->toArray();
                    $arRetorno['localizacao']['cidade'] = $dadosLocalizacao->getCidade();
                    $arRetorno['localizacao']['estado'] = $estadoCidade->getUf()->getId();
                }
                //print_r($declarante->getContratacao()->getId());die;
                $dadosEmpresa = $this->getServiceLocator()->get('Agencia\Repository\DadosEmpresaRepository')->findOneBy(['contratacao' => $declarante->getContratacao()->getId()]);

                if ($dadosEmpresa) {
                    $arRetorno['empresa'] = $dadosEmpresa->toArray();
                }
            }
           
            if ($declarante) {
                die(\Zend\Json\Json::encode($arRetorno));
            } else {
                die(\Zend\Json\Json::encode(array()));
            }

        } else {
            
            die(\Zend\Json\Json::encode(array()));
        }

    }
     public function getContratanteAction()
    {
       $request = $this->getRequest()->getPost()->toArray();
        $arRetorno = array();
       
            $contratante = $this->getServiceLocator()->get('Agencia\Repository\ContratanteRepository')->findOneBy(['contratacao' => $request['contratante']]);
            
            if ($contratante) {
                //print_r($contratante);die;
                $arRetorno['nome'] = $contratante->getNomeContratante();
                $arRetorno['telefone'] = $contratante->getTelefoneResidencial();
                $arRetorno['cpf'] = $contratante->getCpf();
               
            }
           
            if ($contratante) {
                die(\Zend\Json\Json::encode($arRetorno));
            } else {
                die(\Zend\Json\Json::encode(array()));
            }

        
    }
    
    
    
}