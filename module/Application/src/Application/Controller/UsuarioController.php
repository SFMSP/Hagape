<?php

namespace Application\Controller;

use Zend\View\Model\ViewModel;
use Zend\Json\Json;

/**
 * Classe controle de usuários
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class UsuarioController extends CrudApplicationController {

    /**
     *
     * @var string
     */
    protected $nameRepository = 'Application\Repository\UsuarioRepository';

    /**
     *
     * @var string 
     */
    protected $nameService = 'Application\Service\Usuario';

    /**
     *
     * @var string 
     */
    protected $nameForm = 'Application\Form\Usuario';

    /**
     *
     * @var string 
     */
    protected $nameFormFilter = 'Application\Form\UsuarioFiltro';

    /**
     *
     * @var array 
     */
    protected $jsIndex = array('usuario/index.js');

    /**
     *
     * @var array 
     */
    protected $jsFormulario = array('usuario/formulario.js');

    /**
     *
     * @var string 
     */
    protected $controller = 'usuario';

    /**
     *
     * @var string 
     */
    protected $route = 'home/default';

    /**
     * @var string
     */
    protected $logicDelete = true;

    /**
     * Ação chamada pelo js para realizar a paginação
     * @return string
     */
    public function paginacaoAction() {
        //Armazena a resposta
        $response = $this->getResponse();

        //Armazena a requisição
        $request = $this->getRequest()->getPost();

        //Organiza os dados da busca
        $busca = array(
            "busca" => mb_strtolower($request['search']['value'], 'UTF-8'),
            "perfil" => $request['perfil']
        );

        //Cria a ordenação
        $order = array();

        if (isset($request['order'][0]['column'])) {
            switch ($request['order'][0]['column']) {
                case 1: $order = array('field' => 'nome', 'order' => $request['order'][0]['dir']);
                    break;
                case 2: $order = array('field' => 'email', 'order' => $request['order'][0]['dir']);
                    break;
                case 3: $order = array('field' => 'perfil', 'order' => $request['order'][0]['dir']);
                    break;
                case 4: $order = array('field' => 'ativo', 'order' => $request['order'][0]['dir']);
                    break;
            }
        }

        //Busca os dados
        $registros = $this->getServiceLocator()->get($this->nameRepository)->buscaUsuario($busca, $request['length'], $request['start'], $order);
        $registrosTotaisBusca = $this->getServiceLocator()->get($this->nameRepository)->buscaUsuarioTotal($busca);
        $registrosTotais = $this->getServiceLocator()->get($this->nameRepository)->countAll();
        $dados = array();

        //Armazena os dados retornados em um array
        foreach ($registros as $registro) {
            $valor = array();
            $valor[] = '<input type="checkbox" name="checkbox-list" class="checkboxes" value="' . $registro->getId() . '" />';

            if ($this->verifyPermission('usuario', 'editar')) {
                $valor[] = "<a href='usuario/formulario/{$registro->getId()}' title='Editar Item'>" . $registro->getNome() . "</a>";
            } else {
                $valor[] = $registro->getNome();
            }

            $valor[] = $registro->getEmail();
            $valor[] = $registro->getPerfil()->getNome();
            $valor[] = $registro->getAtivo() ? "Ativo" : "Inativo";


            $dados[] = $valor;
        }

        //Organiza o retorno
        $retorno['draw'] = $request['draw'];
        $retorno['recordsTotal'] = $registrosTotais;
        $retorno['recordsFiltered'] = $registrosTotaisBusca;
        $retorno['data'] = $dados;


        //Retorna a resposta
        $response->setContent(\Zend\Json\Json::encode($retorno));
        return $response;
    }

    /**
     * Função que gera uma nova senha 
     */
    public function gerarNovaSenhaAction() {

        //Busca os dados da requisição
        $request = $this->getRequest()->getPost()->toArray();

        //Busca os dados do usuário
        $usuario = $this->getServiceLocator()->get('Application\Repository\UsuarioRepository')->find($request['id'][0]);
        
        //Busca os dados do arquivo de configuração
        $config = $this->getServiceLocator()->get('Config');

        //Cria a nova senha
        $novaSenha = rand(1000000, 9999999);

        //Instancia o serviço do usuário
        if ($this->getServiceLocator()->get('Application\Service\Usuario')->alterarSenha($usuario->getId(), $novaSenha)) {
            
            //Realiza o envio da nova senha 
            $this->getServiceLocator()->get('Application\Mail\NovaSenha')->send($config['sistema']['email'], 'Nova Senha Gerada para Usuário', $usuario->getLogin(), $usuario->getNome(), $novaSenha);
            $this->getServiceLocator()->get('Application\Service\Usuario')->update(array('id' => $request['id'][0], 'trocaSenha' => true));
            
            echo \Zend\Json\Json::encode(array('success' => 1));
        } else {
            echo \Zend\Json\Json::encode(array('success' => 0));
        }
        exit;
    }

    /**
     * Busca os filtros de um perfil
     */
    public function filtroPerfilAction() {
        $request = $this->getRequest()->getPost()->toArray();

        $filtros = $this->getServiceLocator()->get('Application\Repository\PrivilegioRepository')->getFiltrosPerfil($request['perfil']);

        echo Json::encode($filtros);
        die();
    }

}
