<?php

namespace Application\Controller;

use Zend\View\Model\ViewModel;

/**
 * Classe controle de perfil
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class PerfilController extends CrudApplicationController {

    /**
     *
     * @var string
     */
    protected $nameRepository = 'Application\Repository\PerfilRepository';

    /**
     *
     * @var string 
     */
    protected $nameService = 'Application\Service\Perfil';

    /**
     *
     * @var string 
     */
    protected $nameForm = 'Application\Form\Perfil';

    /**
     *
     * @var array 
     */
    protected $jsIndex = array('perfil/index.js');

    /**
     *
     * @var array 
     */
    protected $jsFormulario = array('perfil/formulario.js');

    /**
     *
     * @var string 
     */
    protected $controller = 'perfil';

    /**
     *
     * @var string 
     */
    protected $route = 'home/default';

    /**
     *
     * @var string
     */
    protected $msgDeleteError = 'Erro ao excluir registro! Verique se não há nenhum usuário vinculado';

    /**
     * 
     * @return string
     */
    public function paginacaoAction() {
        //Armazena a resposta
        $response = $this->getResponse();

        //Armazena a requisição
        $request = $this->getRequest()->getPost();

        //Organiza os dados da busca
        $busca = array(
            "busca" => mb_strtolower($request['search']['value'], 'UTF-8')
        );

        $order = array();

        if (isset($request['order'][0]['column'])) {
            switch ($request['order'][0]['column']) {
                case 1: $order = array('field' => 'nome', 'order' => $request['order'][0]['dir']);
                    break;
            }
        }

        //Busca os dados
        $registros = $this->getServiceLocator()->get($this->nameRepository)->buscaPerfil($busca, $request['length'], $request['start'], $order);
        $registrosTotaisBusca = $this->getServiceLocator()->get($this->nameRepository)->buscaPerfilTotal($busca);
        $registrosTotais = $this->getServiceLocator()->get($this->nameRepository)->countAll();
        $dados = array();

        //Armazena os dados retornados em um array
        foreach ($registros as $registro) {
            $valor = array();
            $valor[] = '<input type="checkbox" name="checkbox-list" class="checkboxes" value="' . $registro->getId() . '" />';

            if ($this->verifyPermission('perfil', 'editar')) {
                $valor[] = "<a href='perfil/formulario/{$registro->getId()}' title='Editar Item'>" . $registro->getNome() . "</a>";
            } else {
                $valor[] = $registro->getNome();
            }

            $dados[] = $valor;
        }

        //Organiza o retorno
        $retorno['draw'] = $request['draw'];
        $retorno['recordsTotal'] = $registrosTotais;
        $retorno['recordsFiltered'] = $registrosTotaisBusca;
        $retorno['data'] = $dados;


        //Retorna a resposta
        $response->setContent(\Zend\Json\Json::encode($retorno));
        return $response;
    }

    /**
     * Método padrão de formulário
     * @return \Application\Controller\ViewModel
     */
    public function formularioAction() {
        //Instancia o formulário e a requisição
        $form = $this->getServiceLocator()->get($this->nameForm);
        $request = $this->getRequest();

        //Verifica se o formulário é de edição
        if ($this->params()->fromRoute('id')) {
            //Busca os dados
            $dados = $this->getServiceLocator()->get($this->nameRepository)->find($this->params()->fromRoute('id'));
            $form->setData($dados->toArray());

            //Busca os privilégios do application já com seu HTML montado
            $privilegios = $this->getEm()->getRepository('Application\Entity\Privilegio')->getHtmlPrivilegios($this->params()->fromRoute('id'));
        } else {
            //Busca os privilégios do sistema já com seu HTML montado
            $privilegios = $this->getEm()->getRepository('Application\Entity\Privilegio')->getHtmlPrivilegios();
        }

        //Verifica se a requisição foi feita via post
        if ($request->isPost()) {
            //Busca os dados
            $dados = $request->getPost()->toArray();
            $form->setData($dados);


            //Valida o formulário
            if ($form->isValid()) {
                $service = $this->getServiceLocator()->get($this->nameService);
                $success = $service->save($dados);
                $this->setConfirmMessages($success);

                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            }
        }

        //Seta os scripts e retorna para view
        if (!empty($this->jsFormulario)) {
            $this->layout()->setVariable('scripts', $this->jsFormulario);
        }



        //Retorna os dados da view Model
        return new ViewModel(array('form' => $form, 'privilegios' => $privilegios));
    }

    public function configAction() {

        return $this->redirect()->toRoute($this->route, array('controller' => $this->controller, 'action' => 'config-info'));
    }

    public function configInfoAction() {

        //Busca os dados do usuário e envia para a view
        $dadosUser = $this->getServiceLocator()->get('DadosUser');
        $user = $this->getServiceLocator()->get('Application\Repository\UsuarioRepository')->find($dadosUser['idUsuario']);

        return new ViewModel(array('user' => $user));
    }

    public function configAvatarAction() {

        //Instancia o formulário e a classe de requisição
        $form = $this->getServiceLocator()->get('Application\Form\Avatar');
        $request = $this->getRequest();

        //Busca o id do usuário na sessão e posteriormente seus dados no banco
        $dadosUser = $this->getServiceLocator()->get('DadosUser');
        $user = $this->getServiceLocator()->get('Application\Repository\UsuarioRepository')->find($dadosUser['idUsuario']);
        $avatar = $user->getAvatar() ? $user->getAvatar() : 'avatar.png';

        $form->setData(array('avatar' => $avatar));

        //Verifica se a requisição é POST para salvamento
        if ($request->isPost()) {

            $files = $request->getFiles()->toArray();

            if (isset($files['avatar']['name'])) {
                //Faz o merge entre todos os dados vindos via POST e FILE
                $post = array_merge_recursive(
                        $request->getPost()->toArray(), $request->getFiles()->toArray()
                );

                //Injeta os dados no form
                $form->setData($post);

                //Realiza a validação
                if ($form->isValid()) {

                    //Busca os dados do upload realizado
                    $data = $form->getData();
                    $name = end(explode("/", $data['avatar']['tmp_name']));
                    $name = end(explode("\\", $name));

                    //Altera o avatar no banco de dados
                    $this->getServiceLocator()->get('Application\Service\Usuario')->update(array('id' => $user->getId(), 'avatar' => $name));

                    //Altera o avatar na sessão
                    $dadosUser['avatar'] = $name;
                    $userStorage = new \Zend\Authentication\Storage\Session('Application');
                    $userStorage->write($dadosUser);

                    if ($avatar != 'avatar.png') {
                        unlink('./public/upload/avatar/' . $avatar);
                    }

                    $avatar = $name;
                }
            }
        }

        return new ViewModel(array('form' => $form, 'avatar' => $avatar));
    }

    public function configPasswordAction() {

        $request = $this->getRequest();
        $locator = $this->getServiceLocator();
        $form = $locator->get('Application\Form\AlterarSenha');

        if ($request->isPost()) {

            $data = $request->getPost()->toArray();
                      
            $form->setData($data);

            if ($form->isValid()) {

                $identity = $locator->get('Zend\Authentication\AuthenticationService')->getIdentity();

                $locator->get('Application\Service\Usuario')->alterarSenha($identity['idUsuario'], $data['novaSenha']);

                $this->flashMessenger()->addSuccessMessage('Sua senha foi alterada com sucesso');

                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller, 'action' => 'config-password'));
            } else {

                $this->flashMessenger()->addErrorMessage('Verifique o formulário');
            }
        }

        return new ViewModel(array('form' => $form));
    }

}
