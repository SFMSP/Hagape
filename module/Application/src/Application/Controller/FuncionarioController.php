<?php

namespace Application\Controller;

use Zend\View\Model\ViewModel;

/**
 * Classe controle de usuários
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class FuncionarioController extends CrudApplicationController {

    /**
     *
     * @var string
     */
    protected $nameRepository = 'Application\Repository\UsuarioRepository';

    /**
     *
     * @var string 
     */
    protected $nameService = 'Application\Service\Usuario';

    /**
     *
     * @var string 
     */
    protected $nameForm = 'Application\Form\Usuario';

    /**
     *
     * @var array 
     */
    protected $jsIndex = array('usuario/index.js');

    /**
     *
     * @var string 
     */
    protected $controller = 'usuario';

    /**
     *
     * @var string 
     */
    protected $route = 'home/default';

    /**
     * Ação chamada pelo js para realizar a paginação
     * @return string
     */
    public function paginacaoAction() {
        //Armazena a resposta
        $response = $this->getResponse();

        //Armazena a requisição
        $request = $this->getRequest()->getPost();

        //Organiza os dados da busca
        $busca = array(
            "busca" => mb_strtolower($request['search']['value'], 'UTF-8')
        );

        //Busca os dados
        $registros = $this->getServiceLocator()->get($this->nameRepository)->buscaUsuario($busca, $request['length'], $request['start']);
        $registrosTotaisBusca = $this->getServiceLocator()->get($this->nameRepository)->buscaUsuarioTotal($busca);
        $registrosTotais = $this->getServiceLocator()->get($this->nameRepository)->countAll();
        $dados = array();

        //Armazena os dados retornados em um array
        foreach ($registros as $registro) {
            $valor = array();
            $valor[] = $registro->getNome();
            $valor[] = $registro->getEmail();

            $links = '';
            
            if($this->verifyPermission('usuario', 'excluir')){
                $links .= "<a href='javascript:excluir(" . $registro->getId() . ")' title='Excluir item'>Excluir</a>" ;
            }
            
            if($this->verifyPermission('usuario', 'editar')){
                $links .= "&nbsp;&nbsp;&nbsp;<a href='usuario/formulario/{$registro->getId()}' title='Editar Item'>Editar</a>";
            }
            
            $valor[] = $links;

            $dados[] = $valor;
        }

        //Organiza o retorno
        $retorno['draw'] = $request['draw'];
        $retorno['recordsTotal'] = $registrosTotais;
        $retorno['recordsFiltered'] = $registrosTotaisBusca;
        $retorno['data'] = $dados;


        //Retorna a resposta
        $response->setContent(\Zend\Json\Json::encode($retorno));
        return $response;
    }

    

}
