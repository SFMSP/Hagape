<?php

namespace Application\Controller;

use Zend\View\Model\ViewModel;
use Application\Form\Login as LoginForm;
use Application\InputFilter\Login as LoginFilter;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;
use Zend\Json\Json;

class IndexController extends CrudApplicationController {

    /**
     *
     * @var string 
     */
    protected $errorAuth;

    /**
     *
     * @var type 
     */
    protected $trocaSenha = false;

    /**
     *
     * @var type 
     */
    protected $selecionaFiltro = false;

    /**
     * 
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction() {
        //Instancia o filter e o form
        $filter = new LoginFilter();
        $form = new LoginForm($filter);

        //Armazena a requisição
        $request = $this->getRequest();

        //Verifica se a mesma é feita via post
        if ($request->isPost()) {
            //Armazena os dados a autenticação
            $dados = $request->getPost();

            //Popula e valida o form
            $form->setData($dados);

            if ($form->isValid()) {
                //Realiza a autenticação
                $this->autenticar($dados->user, $dados->password);
            }

            //Adiciona a mensagem de erro da autenticão nas msgs do form
            if (!empty($this->errorAuth)) {
                $form->setMessages(array('password' => array('notValid' => $this->errorAuth)));
            }
        }


        //Manda o form para view
        return new ViewModel(array('form' => $form, "error" => $this->errorAuth, "trocaSenha" => $this->trocaSenha, "selecionaFiltro" => $this->selecionaFiltro));
    }

    /**
     * 
     * @param string $usuario
     * @param string $senha
     */
    public function autenticar($usuario, $senha) {
        //Instancia o adapter de autenticação
        $adapter = $this->getServiceLocator()->get('Application\Auth\Adapter');

        $adapter->setUsername($usuario)->setPassword($senha);

        //Cria a session storage
        $storage = new SessionStorage('Application', null, $this->getServiceLocator()->get('SessionManager'));

        //Instancia o serviço de autenticação
        $autenticationService = new AuthenticationService();
        $autenticationService->setStorage($storage)->setAdapter($adapter);

        //Realiza a autenticação
        $auth = $autenticationService->authenticate();

        //Realiza a autenticação
        if ($auth->isValid()) {
            //Busca os dados retornados para da autenticação e registra na storage
            $identity = $auth->getIdentity();
            $storage->write($identity);

            if ($identity['trocaSenha']) {
                $this->trocaSenha = true;
            }

            if ($identity['possuiFiltros']) {
                $this->selecionaFiltro = true;
            }

            //Caso o usuário não precise selecionar filtros ou realizar a troca da senha ele será redirecionado para a página incial do sistema
            if (!$this->selecionaFiltro && !$this->trocaSenha) {
                $this->redirect()->toRoute('home/default', array('controller' => 'home'));
            }
        } else {
            
            $user = $this->getServiceLocator()->get('Application\Repository\UsuarioRepository')->findOneBy(array('login' => $usuario));
            
            if($user){
                $this->errorAuth = !$user->getAtivo() ? "Usuário Inativado" :  "Usuário e/ou senha inválidos" ;
            }else{
                $this->errorAuth = "Usuário e/ou senha inválidos";
            }
        }
    }

    /**
     * 
     * @return mixed
     */
    public function logoutAction() {
        //Instancia o service de autenticação e destroi a session
        $auth = new AuthenticationService();
        $auth->setStorage(new SessionStorage('Application'));
        $auth->clearIdentity();

        //Redireciona para o login
        return $this->redirect()->toRoute('home');
    }

    public function esqueciMinhaSenhaAction() {

        //Busca o login vindo da requisição
        $request = $this->getRequest();
       
        //Verifica se a requisição é post
        if ($request->isPost()) {

            //Busca o login vindo da requisição
            $dados = $request->getPost()->toArray();
            
            //Verifica se o usuário informado de fato existe no sistema
            $usuario = $this->getServiceLocator()->get('Application\Repository\UsuarioRepository')->findOneBy(array('login' => $dados['login']));
            
            if ($usuario) {

                //Busca as configurações do sistema
                $config = $this->getServiceLocator()->get('Config');
                
                //Realiza o envio do e-mail para o administrador do sistema
                $this->getServiceLocator()->get('Application\Mail\LembrarSenha')->send($config['sistema']['email'][1],$config['sistema']['email'], '[Hagape] Solicitação de Nova Senha - '.$usuario->getNome(), $usuario->getLogin(), $usuario->getNome());
            }

            return true;
        }

        exit;
    }

    public function mudarSenhaColorboxAction() {
        //Instância o form
        $form = $this->getServiceLocator()->get("Application\Form\AlterarSenha");

        //Busca os dados da requisição
        $request = $this->getRequest();

        if ($request->isPost()) {

            $data = $request->getPost()->toArray();
            $form->setData($data);

            $messages['messages'] = array();

            if ($form->isValid()) {

                $identity = $this->getServiceLocator()->get('Zend\Authentication\AuthenticationService')->getIdentity();

                $this->getServiceLocator()->get('Application\Service\Usuario')->alterarSenha($identity['idUsuario'], $data['novaSenha']);
                $this->getServiceLocator()->get('Application\Service\Usuario')->update(array('id' => $identity['idUsuario'], 'trocaSenha' => false));

                $sessionStorage = new \Zend\Authentication\Storage\Session('Application');
                $identity['trocaSenha'] = false;
                $sessionStorage->write($identity);

                $messages['possuiFiltros'] = $identity['possuiFiltros'];
            } else {
                $messages['messages'] = $form->getMessages();
            }


            echo Json::encode($messages);
            die();
        } else {
            //Seta a view
            $viewModel = new ViewModel(array("form" => $form));
            $viewModel->setTerminal(true);
            return $viewModel;
        }
    }

    public function completarLoginColorboxAction() {
        //Instancia o formulário
        $form = $this->getServiceLocator()->get('Application\Form\Filtro');

        //Busca os dados da requisição
        $request = $this->getRequest();

        if ($request->isPost()) {

            $data = $request->getPost()->toArray();
            $form->setData($data);

            $messages['messages'] = array();

            if ($form->isValid()) {
                $sessionStorage = new \Zend\Authentication\Storage\Session('Application');
                $identity = $sessionStorage->read();
                
                foreach($data as $key => $fil){
                    $identity['filtrosSelecionados'][$key] = $fil;
                }
                
                if(isset($identity['filtrosSelecionados']['estoque'])){
                    $identity['filtrosSelecionados']['estoque'] = array('id' => $identity['filtrosSelecionados']['estoque'], 'central' => $this->getServiceLocator()->get('Admin\Repository\EstoqueRepository')->verificaEstoqueCentral($identity['filtrosSelecionados']['estoque']));
                }

                $sessionStorage->write($identity);
                
            }else{
                
                $messages['messages'] = $form->getMessages();

            }

            echo Json::encode($messages);
            die();
            
        } else {
            //Seta a view
            $viewModel = new ViewModel(array('form' => $form));
            $viewModel->setTerminal(true);
            return $viewModel;
        }
    }

}
