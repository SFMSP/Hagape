<?php

namespace Application\Controller;

use Zend\View\Model\ViewModel;

class SearchController extends CrudApplicationController {

    protected $controller = 'perfil';

    protected $route = 'home/default';
    
    public function indexAction(){
        
        return new ViewModel();
    }
}
