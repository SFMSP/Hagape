<?php
namespace Application\Mail;

use Base\Mail\Mail;

/**
 * Classe que realiza o envio do e-mail da nova senha do usuário para o administrador
 *
 * @author Praxedes
 */
class NovaSenha {

    /**
     *
     * @var \Base\Mail\Mail 
     */
    protected $mail;
    
    /**
     *
     * @var ViewRender
     */
    protected $viewRenderer;
    
    /**
     *
     * @var type 
     */
    protected $sessionLogin;

    /**
     * 
     * @param Mail $mail
     */
    public function __construct(Mail $mail, $viewRenderer) {
        $this->mail = $mail;
        $this->viewRenderer = $viewRenderer;
    }

    /**
     * Envia o e-mail
     * 
     * @param string $to
     * @param string $subject
     * @param string $message
     * @return boolean
     */
    public function send($to, $subject, $login, $nomeUsuario, $novaSenha) {
        try {

            //Cria o corpo da mensagem 
            $body = $this->viewRenderer->render('application/index/nova-senha', array('login' => $login, 'nome' => $nomeUsuario, 'novaSenha' => $novaSenha));
            
            //Envia a mensagem
            $this->mail->setTo($to)
                    ->setSubject($subject)
                    ->setBody($body)
                    ->prepare()
                    ->send();

            return true;
        } catch (\Exception $ex) {
            die($ex->getMessage());
            return false;
        }
    }

}
