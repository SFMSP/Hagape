<?php
namespace Application\Mail;

use Base\Mail\Mail;

/**
 * Classe que realiza o envio do e-mail de lembrar Senha
 *
 * @author Praxedes
 */
class LembrarSenha {

    /**
     *
     * @var \Base\Mail\Mail 
     */
    protected $mail;
    
    /**
     *
     * @var ViewRender
     */
    protected $viewRenderer;
    
    /**
     *
     * @var type 
     */
    protected $sessionLogin;

    /**
     * 
     * @param Mail $mail
     */
    public function __construct(Mail $mail, $viewRenderer) {
        $this->mail = $mail;
        $this->viewRenderer = $viewRenderer;
    }

    /**
     * Envia o e-mail
     * 
     * @param string $to
     * @param string $subject
     * @param string $message
     * @return boolean
     */
    public function send($to,$reply, $subject, $login, $nomeUsuario) {
        try {

            //Cria o corpo da mensagem 
            $body = $this->viewRenderer->render('application/index/lembrar-senha', array('login' => $login, 'nome' => $nomeUsuario));
           
            //Envia a mensagem
            $this->mail->setTo($to)
                    ->setReply($reply)
                    ->setSubject($subject)
                    ->setBody($body)
                    ->prepare()
                    ->send();

            return true;
        } catch (\Exception $ex) {
            die($ex->getMessage());
            return false;
        }
    }

}
