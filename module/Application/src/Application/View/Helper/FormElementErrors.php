<?php

namespace Application\View\Helper;

class FormElementErrors extends \Zend\Form\View\Helper\FormElementErrors {

    protected $messageCloseString = '</span>';
    protected $messageOpenFormat = '<span class="help-block">';
    protected $messageSeparatorString = '</span><span class="help-block">';
}