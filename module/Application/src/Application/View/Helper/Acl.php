<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * Classe Helper de view, que retorna o objeto acl que possibilita que a verificação de permissionamento
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class Acl extends AbstractHelper {

    /**
     *
     * @var \Application\Security\Acl 
     */
    protected $acl;
    
    /**
     *
     * @var string 
     */
    protected $perfilUser;
    

    /**
     * Método construtor que faz a chamada do método que monta a ACL
     */
    public function __construct() {
        $this->setAcl();
    }

    /**
     * Método __invoke que retorna a acl
     * @return \Application\Security\Acl
     */
    public function __invoke() {
        return $this->acl;
    }

    /**
     * 
     * @return \Application\Security\Acl
     */
    public function getAcl(){
        return $this->acl;
    }
    
    /**
     * Método que monta a acl
     */
    public function setAcl() {
        //Busca os dados do usuário 
        $sessionStorage = new \Zend\Authentication\Storage\Session('Application');
        $dadosUser = $sessionStorage->read();

        if (is_array($dadosUser)) {
            //Instancia a ACL
            $this->acl = new \Application\Security\Acl($dadosUser['perfil'], $dadosUser['privilegios']);
            $this->perfilUser = $dadosUser['perfil'];
        }
        
    }


    
}
