<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Authentication\Storage\Session as SessionStorage;

/**
 * Classe UserIdentity, responsável por retornar os dados do usuário para view
 * 
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
class User extends AbstractHelper {

    /**
     *
     * @var integer 
     */
    private $id;
    
    /**
     *
     * @var string 
     */
    private $nome;
    
    /**
     *
     * @var string 
     */
    private $email;
    
    /**
     *
     * @var string 
     */
    private $perfil;
    
    /**
     *
     * @var array 
     */
    private $filtrosSelecionados;
    
    /**
     * 
     * Retorna os dados da session de usuário, para serem utilizados na view
     * @return array
     */
    public function __invoke() {
        //Busca a sessão
        $sessionStorage = new SessionStorage('Application');
        
        //Armazena os dados
        $dados = $sessionStorage->read();
        
        //Passa os dados para os parâmetros do helper
        $this->id = $dados['idUsuario'];
        $this->nome = $dados['usuarioNome'];
        $this->email = $dados['usuarioEmail'];
        $this->perfil = $dados['perfil'];
        $this->filtrosSelecionados = $dados['filtrosSelecionados'];
        
        return $this;
        
    }
    
    /**
     * 
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * 
     * @return string
     */
    public function getNome() {
        return $this->nome;
    }

    /**
     * 
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * 
     * @return string
     */
    public function getPerfil() {
        return $this->perfil;
    }

  



}
