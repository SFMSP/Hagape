<?php
namespace Base\Container;

/**
 * Interface que contem os métodos a serem implementados em uma classe que retornara factories do service manager.
 * Esses métodos servem para agrupar os serviços de acordo com o seu tipo.
 * 
 * @author Praxedes
 */
interface BaseContainer {

    public function getBaseServices();

    public function getRepositories();

    public function getServices();

    public function getForms();

    public function getAuthServices();

    public function getFormsFilter();

    public function getOfficeServices();

    public function getMailConfig();
}
