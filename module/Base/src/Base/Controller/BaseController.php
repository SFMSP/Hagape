<?php

namespace Base\Controller;

use Zend\Mvc\Controller\AbstractActionController;

/**
 * Classe base que contém alguns métodos que facilitam o trabalho no controller
 *
 * @autor Eduardo Praxedes Heinske <praxeduardo@gmail.com>
 */
abstract class BaseController extends AbstractActionController {

    /**
     *
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * 
     * Método que retorna uma instância do EntityManager
     * 
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEm() {
        if ($this->em === null) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }

        return $this->em;
    }

    /**
     * Retorna um array em json
     * 
     * @param array $dados
     * @return string
     */
    public function returnJson($dados) {
        //Retorna os dados
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($dados));
        return $response;
    }
    
    /**
     * Busca os dados de um cep pelo ws dos correios e retorna em json
     * 
     * @param string $cep
     * @return json
     */
    public function getDadosCep($cep){
        $cep = str_replace('-', '', $cep);
        return file_get_contents("http://viacep.com.br/ws/{$cep}/json/");
    }
    
    /**
     * Chamada do WS dos correios via action para js
     */
    public function getDadosCepAction(){
        $cep = $this->params()->fromRoute('id');
        die($this->getDadosCep($cep));
    }
    public function urls($arr){
       
        $url = $this->getServiceLocator()->get('viewhelpermanager')->get('url');
        return $url($arr['route'], array('controller' => $arr['controller'], 'action' => $arr['action'], 'id' =>$arr['id']));
      
        
    }
    
        
    
    /**
     * 
     * @return \Base\Helper\DatetimeFormat
     */
    public function getDateTimeHelper(){
        return new \Base\Helper\DatetimeFormat();
    }
    
    /**
     * 
     * @return \Base\Helper\FloatFormat
     */
    public function getFloatFormatHelper(){
        return new \Base\Helper\FloatFormat();
    }
    
    /**
     * 
     * @return \Base\Helper\StringFormat
     */
    public function getStringFormat(){
        return new \Base\Helper\StringFormat();
    }
     
}
