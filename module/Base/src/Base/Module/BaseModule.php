<?php
namespace Base\Module;

/**
 * Classe base para todos as classe Module.php, contendo métodos que em comum para todas
 *
 * @author Praxedes
 */
abstract class BaseModule {

    /**
     * Método que monta o container de serviços a partir das classes container
     * @param \Base\Container\BaseContainer $container
     * @return type
     */
    public function composeServiceConfig(\Base\Container\BaseContainer $container = null) {
        
        if(is_null($container)){
            throw new \Exception('Informe o container');
        }
        
        //Cria o array que receberá as factories
        $factories = array();

        //Monta o array de services
        $baseServices = $container->getBaseServices();
        $baseRepositories = $container->getRepositories();
        $services = $container->getServices();
        $forms = $container->getForms();
        $auth = $container->getAuthServices();
        $formsFilter = $container->getFormsFilter();
        $phpoffice = $container->getOfficeServices();
        $mail = $container->getMailConfig();

        //Une os arrays
        $factories['factories'] = array_merge($baseServices, $baseRepositories, $services, $forms, $auth, $formsFilter, $phpoffice, $mail);

        //Retorna os services
        return $factories;
    }

}
