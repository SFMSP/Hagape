<?php
namespace Base\Validator;

use Zend\InputFilter\InputFilter;

/**
 * Classe BaseValidator, contém métodos base que facilitam a inserção de filtros de validação
 */
abstract class BaseValidator extends InputFilter {

    /**
     * Metodo addEmptyValidators, adiciona dinâmicamente campos obrigatórios
     * @param array $arrayCampos
     */
    public function addEmptyValidators(array $arrayCampos) {
        
        //Percorre e monta o filter padrão de validação de campos
        foreach ($arrayCampos as $element => $message) {
            $this->add(array(
                'name' => $element,
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => $message,
                            )
                        ),
                    ),
                )
            ));
        }
    }

}
