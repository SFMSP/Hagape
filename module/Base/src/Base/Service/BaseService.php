<?php
namespace Base\Service;

abstract class BaseService {

    /**
     *
     * @var \Zend\Log\Logger
     */
    protected $log;

    /**
     *
     * @var \Doctrine\ORM\EntityManager 
     */
    protected $em;

    /**
     *
     * @var \Base\Entity\EntityInterface
     */
    protected $entity;

    public function __construct(\Doctrine\ORM\EntityManager $em, \Base\Entity\EntityInterface $entity, \Zend\Log\Logger $log) {
        $this->em = $em;
        $this->entity = $entity;
        $this->log = $log;
    }

    function setEm(\Doctrine\ORM\EntityManager $em) {
        $this->em = $em;
        return $this;
    }

        /**
     * 
     * @return \Zend\Log\Logger
     */
    public function getLog() {
        return $this->log;
    }

    /**
     * 
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEm() {
        return $this->em;
    }

    /**
     * 
     * @return \Base\Entity\EntityInterface
     */
    public function getEntity() {
        return $this->entity;
    }

    /**
     * 
     * @return string
     */
    public function getEntityName() {
        return get_class($this->entity);
    }

    /**
     * 
     * @param array $data
     * @return boolean
     */
    public function save(array $data) {
        if (isset($data['enviar'])) {
            unset($data['enviar']);
        }

        if (!empty($data['id'])) {
           
            return $this->update($data);
        } else {
            return $this->insert($data);
        }
    }

    /**
     * 
     * @param array $data
     */
    public function insert(array $data) {
        try {
            
            $entity = $this->entity;
            $entity->hydrate($data);
            $this->em->persist($entity);
            $this->em->flush();
            $this->em->clear();

            return $entity;
        } catch (\Exception $ex) {
            $this->log->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            return false;
        }
    }

    public function update(array $dados) {
        try {
            $entity = $this->em->getReference($this->getEntityName(), $dados['id']);
            unset($dados['id']);

            $entity->hydrate($dados);
            
            $this->em->persist($entity);
            $this->em->flush();
            $this->em->clear();

            return $entity;
        } catch (\Exception $ex) {
            $this->log->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            return false;
        }
    }

    public function delete($id) {
        try {
            $entity = $this->em->getReference($this->getEntityName(), $id);

            $this->em->remove($entity);
            $this->em->flush();

            return true;
        } catch (\Exception $ex) {
            $this->log->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            return false;
        }
    }
    
    public function logicDelete($id, $field){
        return $this->update(array('id' => $id, $field => 1));
    }

}
