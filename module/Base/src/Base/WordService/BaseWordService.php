<?php

namespace Base\WordService;

use PhpOffice\PhpWord\PhpWord;

/**
 * Classe que recebe as dependências básicas do phpword
 * 
 * @author Praxedes
 */
abstract class BaseWordService {

    /**
     *
     * @var PhpWord
     */
    protected $phpWord;

    /**
     *
     * @var string
     */
    protected $tmpDir;

    /**
     *
     * @var string 
     */
    protected $saveDir;

    /**
     *
     * @var string
     */
    protected $templateDir;

    /**
     * 
     * @param PhpWord $phpWord
     * @param TemplateProcessor $templateProcessor
     */
    public function __construct(PhpWord $phpWord, $tmpDir, $saveDir, $templateDir) {
        $this->phpWord = $phpWord;
        $this->tmpDir = $tmpDir;
        $this->saveDir = $saveDir;
        $this->templateDir = $templateDir;
    }

}
