<?php

namespace Base\Factory;

/**
 * Implementa o serviço do Zend\Authentication\AuthenticationService para ser 
 * invocado pelo view helper Zend\View\Helper\Service\IdentityFactory
 * 
 * @author Michael
 */
class Authentication implements \Zend\ServiceManager\FactoryInterface {

    public function createService(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator) {
        
        $sessionStorage = new \Zend\Authentication\Storage\Session('Application');

        $auth = new \Zend\Authentication\AuthenticationService();
        $auth->setStorage($sessionStorage);

        return $auth;
    }
}