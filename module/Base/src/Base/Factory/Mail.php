<?php

namespace Base\Factory;

class Mail implements \Zend\ServiceManager\FactoryInterface {

    public function createService(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator) {

        //Busca as configurações
        $config = $serviceLocator->get('Config');

        //Cria o objeto de transporte
        $transport = new \Zend\Mail\Transport\Smtp();
        $options = new \Zend\Mail\Transport\SmtpOptions($config['mail']);
        $transport->setOptions($options);

        //Instancia o mail
        return new \Base\Mail\Mail($transport, new \Zend\Mail\Message());
    }
}