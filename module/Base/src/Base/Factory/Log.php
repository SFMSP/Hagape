<?php

namespace Base\Factory;

class Log implements \Zend\ServiceManager\FactoryInterface {

    public function createService(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator) {
        
        $config = $serviceLocator->get('config');

        //Instancia o log e stream
        $logger = new \Zend\Log\Logger;
        $writer = new \Zend\Log\Writer\Stream($config['log']);

        //Adiciona o writer e retorna o log
        $logger->addWriter($writer);
        return $logger;
    }
}