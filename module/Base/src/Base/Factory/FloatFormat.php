<?php

namespace Base\Factory;

class FloatFormat implements \Zend\ServiceManager\FactoryInterface {

    public function createService(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator) {
        
        return new \Base\Helper\FloatFormat();
    }
}