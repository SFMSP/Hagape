<?php
namespace Base\Repository;

/**
 * Interface que contém métodos padrões no uso de repository
 * @author Praxedes
 */
interface RepositoryInterface {
    
    /**
     * Deve retornar um logger
     */
    public function getLogger();
    
    /**
     * 
     * @param \Zend\Log\Logger $log
     */
    public function setLogger(\Zend\Log\Logger $log);
}
