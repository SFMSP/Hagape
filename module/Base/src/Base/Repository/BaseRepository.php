<?php

namespace Base\Repository;

use Doctrine\ORM\EntityRepository;
use Zend\Log\Logger;

/**
 * Classe base de um repository, contendo algums métodos de apoio
 *
 * @author Praxedes
 */
class BaseRepository extends EntityRepository {

    /**
     *
     * @var \Zend\Log\Logger
     */
    protected $logger;

    /**
     * 
     * @return \Zend\Log\Logger
     */
    public function getLogger() {
        return $this->logger;
    }

    /**
     * 
     * @param \Zend\Log\Logger $logger
     */
    public function setLogger(Logger $logger) {
        $this->logger = $logger;
    }

    /**
     * 
     * Retorna o total de registros da entidade
     * @return integer
     */
    public function countAll($logic = false, $fieldLogic = 'excluido') {
        try {
            //Monta o dql, executa a query e retorna o resultado
            $dql = "SELECT count(e) total FROM {$this->getEntityName()} e ";

            if ($logic) {
                $dql .= " WHERE e.{$fieldLogic} = 0 ";
            }

            $query = $this->getEntityManager()->createQuery($dql);
            $result = $query->getResult();

            return $result[0]['total'];
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            return false;
        }
    }

    /**
     * 
     * Retorna um array com id => nome
     * @return array
     */
    public function getArraySelect($default = "Selecione um Registro", $order = array(), $firtElement = true, $where = "") {
        try {
            //Busca os registros
            $dql = "SELECT t FROM {$this->getEntityName()} t ";
            $dql .= $where;

            if (count($order) > 0) {
                $dql .= " ORDER BY t.{$order[0]} {$order[1]} ";
            }

            $query = $this->getEntityManager()->createQuery($dql);
            $array = $firtElement ? array("" => $default == "Selecione um Registro" ? "--{$default}--" : $default) : array();


            //Percorre e organiza os resultados
            foreach ($query->getResult() as $obj) {
                $array[$obj->getId()] = $obj->getLabel();
            }

            //Retorna os dados
            return $array;
        } catch (\Exception $ex) {
            $this->getLogger()->log(\Zend\Log\Logger::ERR, $ex->getMessage());
            return false;
        }
    }

    /**
     * 
     * @param array $array
     * @return object
     */
    public function findIn(array $array) {
        $result = array();

        if ($array) {
            $result = $this->createQueryBuilder('q')->where('q.id IN (' . implode(',', $array) . ') ')->getQuery()->getResult();
        }

        return $result;
    }

    /**
     * 
     * @param array $array
     * @return object
     */
    public function findInArrayLabel(array $array, $label = "") {
        $result = $this->findIn($array);
        $fields = array();

        if (!empty($label)) {
            $fields[""] = $label;
        }

        foreach ($result as $res) {
            $fields[$res->getId()] = $res->getLabel();
        }

        return $fields;
    }

    /**
     * Recebe um array de entidades e retorna um array chave => valor para popular um select
     * 
     * @param array $array
     * @param string $label
     * @return array
     */
    public function arrayObjectToSelect(array $array, $label = "") {
        $fields = array();

        if (!empty($label)) {
            $fields[""] = $label;
        }

        foreach ($array as $field) {
            $fields[$field->getId()] = $field->getLabel();
        }
        
        return $fields;
    }

}
