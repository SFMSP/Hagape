<?php
namespace Base\Helper;

/**
 * Helper responsável pela formatação de data e hora em string
 * 
 * @author Praxedes
 */
class DatetimeFormat {

    /**
     * Metodo format
     * 
     * Faz a formatação de uma data 
     * @param String $date
     * @param String $format
     * @return String
     */
    public function format($date, $format) {

        try {
            //Faz a formatação da data utilizando a classe datetime
            $datetime = new \DateTime($date);
            return $datetime->format($format);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Metodo formatUs
     * 
     * Retorna um objeto datetime de uma data no formato brasileiro para o formato americano
     * @param String $date
     * @return String|boolean
     */
    public function formatUs($date) {
        try {
            //Verifica se a data possui 10 caracteres
            if (strlen($date) == 10) {
                //Explode a data 
                $arrayData = explode("/", $date);

                //Verifica se a data está no formato brasileiro
                if (count($arrayData) == 3) {
                    return $arrayData[2] . "-" . $arrayData[1] . "-" . $arrayData[0];
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Metodo diff
     * 
     * Calcula e retorna a diferença e data e hora
     * Formata as datas
     * @param String $d1
     * @param String $d2
     */
    public function diff($d1, $d2) {
        try {
            //Calcula a diferença entre duas data/hora utulizando a classe datetime
            $dataUm = new \DateTime($d2);
            $dataDois = new \DateTime($d1);

            return $dataUm->diff($dataDois);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Subtrai dias de uma data
     *  
     * @param string $date
     * @param string $days
     * @return string
     */
    public function subDays($date, $days, $format = 'Y-m-d') {

        //Converte a data em segundos 
        $segundosData = strtotime($date);

        //Obtem o número de segundos dos dias
        $segundosDias = $days * 86400;

        //Retorna a subtração entre as duas datas
        return date($format, ($segundosData - $segundosDias));
    }

    /**
     * Soma dias de uma data
     *  
     * @param string $date
     * @param string $days
     * @return string
     */
    public function sumDays($date, $days, $format = 'Y-m-d') {

        //Converte a data em segundos 
        $segundosData = strtotime($date);

        //Obtem o número de segundos dos dias
        $segundosDias = $days * 86400;
       
        //Retorna a subtração entre as duas datas
        return date($format, ($segundosData + $segundosDias));
    }
    

}
