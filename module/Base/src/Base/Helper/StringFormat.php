<?php

namespace Base\Helper;

/**
 * Description of StringFormat
 *
 * @author Eduardo Praxedes Heinske (eduardo.praxedes@jointecnologia.com.br)
 */
class StringFormat {
    
    /**
     * Retorna somente o nome de um arquivo que está em uma string com seu path
     * @param type $path
     * @return type
     */
    public function getNameFilePath($path) {
        $arquivo = end(explode("/", $path));
        $arquivo = end(explode("\\", $arquivo));

        return $arquivo;
    }
    
    

}
