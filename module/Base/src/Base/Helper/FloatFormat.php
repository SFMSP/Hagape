<?php

namespace Base\Helper;

/**
 * Classe para formatação de valores em ponto flutuante
 *
 * @author Praxedes
 */
class FloatFormat {

    
    /**
     * Formata valores com máscara monetária para o float padrão do PHP
     * 
     * @param string $value
     * @return float
     */
    public function formatMoneyToFloat($value) {
        $value = str_replace('.', '', $value);
        $value = str_replace(',', '.', $value);
        
        return $value;
        
    }

}
