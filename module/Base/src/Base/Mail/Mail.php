<?php

namespace Base\Mail;

use Zend\Mail\Transport\Smtp;
use Zend\Mail\Message;
use Zend\Mime\Part as MimePart;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Mime;

/**
 * Classe que realiza o envio de e-mails
 *
 * @author Praxedes
 */
class Mail {

    /**
     *
     * @var Smtp
     */
    protected $transport;

    /**
     *
     * @var Message 
     */
    protected $message;

    /**
     *
     * @var string 
     */
    protected $to;

    /**
     *
     * @var string 
     */
    protected $subject;

    /**
     *
     * @var string
     */
    protected $body;

    /**
     *
     * @var string 
     */
    protected $header;

    /**
     *
     * @var type array
     */
    protected $reply = array();

    /**
     *
     * @var array 
     */
    protected $attachments = array();

    /**
     * 
     * @param Smtp $transport
     */
    public function setTransport(Smtp $transport) {
        $this->transport = $transport;
        return $this;
    }

    /**
     * 
     * @param Message $message
     */
    public function setMessage(Message $message) {
        $this->message = $message;
        return $this;
    }

    /**
     * 
     * @param string $to
     */
    public function setTo($to) {
        $this->to = $to;
        return $this;
    }

    /**
     * 
     * @param string $subject
     */
    public function setSubject($subject) {
        $this->subject = $subject;
        return $this;
    }

    /**
     * 
     * @param string $body
     */
    public function setBody($body) {
        $this->body = $body;
        return $this;
    }

    /**
     * 
     * @param string $header
     * @return \Base\Mail\Mail
     */
    public function setHeader($header) {
        $this->header = $header;
        return $this;
    }

    /**
     * 
     * @param array $reply
     */
    public function setReply(array $reply) {
        $this->reply = $reply;
        return $this;
    }

    /**
     * 
     * @param array $attachments
     */
    public function setAttachments(array $attachments) {
        $this->attachments = $attachments;
        return $this;
    }

    /**
     * 
     * @param Smtp $transport
     * @param Message $message
     */
    public function __construct(Smtp $transport, Message $message) {
        $this->transport = $transport;
        $this->message = $message;
    }

    /**
     * Método que prepara uma mensagem simples para ser enviada
     * @return \Base\Mail\Mail
     */
    public function prepare() {

        //Busca as configurações de e-mail
        $config = $this->transport->getOptions()->toArray();
        
        //Seta os parâmetros da mensagem
        $this->message->addFrom($config['connection_config']['from'])
                ->addTo($this->to)
                ->setSubject($this->subject);


        //Verifica se possi cópias
        /*foreach ($this->reply as $copy) {
            $this->message->addReplyTo($copy);
        }*/
        foreach ($this->reply as $copy) {
            $this->message->addCc($copy);
        }

        //Cria o objeto mimeMessagem que irá compor o corpo da mensagem e seus anexos
        $mimeMessage = new MimeMessage();

        //Adiciona o corpo da mensagem
        $bodyPart = new MimePart($this->body);
        $bodyPart->type = 'text/html';

        //Adiciona a parte referente ao corpo da mensagem
        $mimeMessage->addPart($bodyPart);

        //Verifica se existem anexos
        if ($this->attachments) {

            //Percorre os anexos e os adiciona um a um
            foreach ($this->attachments as $fileName => $attachment) {

                //Cria o mimePart do anexo
                $attachmentPart = new MimePart(fopen($attachment, 'r'));
                $attachmentPart->type = Mime::TYPE_OCTETSTREAM;
                $attachmentPart->filename = $fileName;
                $attachmentPart->encoding = Mime::ENCODING_BASE64;
                $attachmentPart->disposition = Mime::DISPOSITION_ATTACHMENT;

                //Adiciona o anexo a mensagem
                $mimeMessage->addPart($attachmentPart);
            }
        }

        //Seta o corpo da mensagem
        $this->message->setBody($mimeMessage);

        //Verifica se o tipo de cabeçalho foi setado
        if ($this->header) {
            $this->message->getHeaders()->get('content-type')->setType($this->header);
        }


        return $this;
    }

    /**
     * Faz o envio da mensagem
     */
    public function send() {
        $this->transport->send($this->message);
    }

}
