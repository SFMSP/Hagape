<?php

namespace Base\Entity;

use Zend\Stdlib\Hydrator;

abstract class BaseEntity {

    /**
     * 
     * @param array $options
     */
    public function hydrate(array $options = array()) {
        $hydrator = new Hydrator\ClassMethods();
        $hydrator->hydrate($options, $this);
    }

}
