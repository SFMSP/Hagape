<?php
namespace Base\Entity;

/**
 * Interface EntityInterface
 * @package Base\Entity
 */
interface EntityInterface {

    /**
     * @return mixed
     */
    public function toArray();

    /**
     * @return mixed
     */
    public function __toString();

    /**
     * @return mixed
     */
    public function hydrate();

    /**
     * @return mixed
     */
    public function getLabel();

    /**
     * @return mixed
     */
    public function getId();
}
