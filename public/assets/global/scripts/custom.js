/**
 * Função para redirecionamento
 * @param {type} url
 * @returns {undefined}
 */
function redirecionar(url){
    location.href = url;
}

/**
 * Função que captura todos os checkbox marcados
 * @param {type} idTable
 * @returns {Array}
 */
function getCheckboxMarcados(idTable){
    var count = 0;
    var sel = new Array();
    var ch = jQuery('#' + idTable).find('tbody input:checkbox');     //get each checkbox in a table

    //check if there is/are selected row in table
    ch.each(function (){
        if (jQuery(this).is(':checked')) {
            sel[count] = jQuery(this).val();
            count++;
        }
    });

    return sel;
}

jQuery(document).ready(function (){

    //Aplica as máscaras
    jQuery('.data').mask('00/00/0000');
    jQuery('.time').mask('00:00:00');
    jQuery('.hora').mask('00:00');
    jQuery('.date_time').mask('00/00/0000 00:00:00');
    jQuery('.cep').mask('00000-000');
    jQuery('.phone').mask('0000-00000');
    jQuery('.phone_with_ddd').mask('(00) 0000-00000');
    jQuery('.phone_us').mask('(000) 000-0000');
    jQuery('.mixed').mask('AAA 000-S0S');
    jQuery('.cpf').mask('000.000.000-00', {reverse: true});
    jQuery('.money').mask('000.000.000.000.000,00', {reverse: true});
    jQuery('.number-meddium').mask('000000000000', {reverse: true});
    jQuery('.percentual').mask('00.00', {reverse: true});
    jQuery('.temperatura').mask('00.00', {reverse: true});
    jQuery('.cnpj').mask('00.000.000/0000-00', {reverse: true});
    jQuery('.numero-parcelas').mask('00', {reverse: true});
    jQuery('.numero-nota').mask('000000000', {reverse: true});
    jQuery('.dia-padrao').mask('00', {reverse: true});


    jQuery.fn.datepicker.dates['pt'] = {
        format: 'dd/mm/yyyy',
        days: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        daysShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
        daysMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
        months: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        monthsShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        today: "Hoje",
        clear: "Limpar",
        titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
        weekStart: 0
    };

    //Adiciona o datepicker
    jQuery('.datepicker').datepicker({
        language: 'pt', calendarWeeks: true, autoclose: true
    });

    jQuery(".default-touchspin").TouchSpin({
        verticalbuttons: true,
        verticalupclass: 'glyphicon glyphicon-plus',
        verticaldownclass: 'glyphicon glyphicon-minus'
    });
});

/**
 * Converte um valor com máscara monetária para float
 *
 * @param {string} value
 * @returns {float}
 */
function convertMaskToFloat(value){
    value = value.replace('.', '').replace('.', '').replace('.', '').replace('.', '');
    value = value.replace(',', '.');
    parseFloat(value);

    return value;
}

/**
 * Formata um valor float para monetário
 *
 * @param {float} value
 * @param {integer} c
 * @param {string} d
 * @param {string} t
 * @returns {String}
 */
function formatMoney(value, c, d, t){
    var n = value, c = isNaN(c = Math.abs(c)) ? 2 :c, d = d == undefined ? "." :d, t = t == undefined ? "," :t, s = n < 0 ? "-" :"", i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), j = (j = i.length) > 3 ? j % 3 :0;
    return s + (j ? i.substr(0, j) + t :"") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) :"");
}

function ordenarSelects(idSelect){
    var options = jQuery("#" + idSelect + " option");                    // Collect options         
    options.detach().sort(function (a, b){               // Detach from select, then Sort
        var at = jQuery(a).text();
        var bt = jQuery(b).text();
        return (at > bt) ? 1 :((at < bt) ? -1 :0);            // Tell the sort function how to order
    });
    options.appendTo("#" + idSelect);
}

function bloqueiaTela(){
    jQuery.blockUI({
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }, message: '<h2>Aguarde...</h2><img src="../../assets/global/img/ajax-modal-loading.gif" />'
    });
}
function liberaTela(){
    jQuery.unblockUI();
}