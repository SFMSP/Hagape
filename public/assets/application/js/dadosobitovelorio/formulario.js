$(document).ready(function () {
    $('.mostra').css('display','none');
    $('.mostraTransporte').css('display','block');
    $('.mostraCarro').css('display','none');
    $('.mostraSe').css('display','none');
    $('.mostraCr').css('display','none');  
    
    $('#seraVelado').change(function () {
        
        if($('#seraVelado').val()==1){
             $('.mostra').css('display','block');
        }else if($('#seraVelado').val()==2){
             $('.mostra').css('display','none');
        }else if($('#seraVelado').val()==0){
             $('.mostra').css('display','none');
        }
      
    });
    
    $('#tipoTransporte').change(function () {
        if($('#tipoTransporte').val()==6){
        $('.mostraCarro').css('display','block');
        }else{
            $('.mostraCarro').css('display','none');
        }
    });
    $('#destinoFinal').change(function(){
        if($('#destinoFinal').val()==0){
            $('.mostraSe').css('display','none');
            $('.mostraCr').css('display','none');  
      
        }
        if($('#destinoFinal').val()==1){
            $('.mostraSe').css('display','block');
            $('.mostraCr').css('display','none');  
      
        }
        if($('#destinoFinal').val()==2){
            $('.mostraSe').css('display','none');
            $('.mostraCr').css('display','block');  
      
        }
    });
    
     jQuery('#BuscaCemiterio').on('click', function (){
        bloqueiaTela();
        if (jQuery('#cemiterio').val() !== "") {
            jQuery.post(url + '/agencia/dadosObitoVelorio/cemiterio', {cemiterio: jQuery('#cemiterio').val()}, function (dados){
                liberaTela();
                if (dados) {
                   // jQuery('#enderecoCemiterio').val(dados['contratante']['nomeContratante']);
                  for (var i = 0; i < dados.length; i++) {
                       
                        $('#enderecoCemiterio').val(dados[i]['endereco']);
                        $('#hiddenCemiterio').val(dados[i]['idCemiterio']);
                       

                     }
                }

            }, 'json');
        }
    });
    jQuery('#BuscaCrematorio').on('click', function (){
        bloqueiaTela();
        if (jQuery('#crematorio').val() !== "") {
            jQuery.post(url + '/agencia/dadosObitoVelorio/crematorio', {crematorio: jQuery('#crematorio').val()}, function (dados){
                liberaTela();
                if (dados) {
                   // jQuery('#enderecoCemiterio').val(dados['contratante']['nomeContratante']);
                  for (var i = 0; i < dados.length; i++) {
                       
                        $('#enderecoCrematorio').val(dados[i]['endereco']);
                        $('#hiddenCrematorio').val(dados[i]['idCrematorio']);
                       

                     }
                }

            }, 'json');
        }
    });
    jQuery('#BuscaMedico1').on('click', function (){
        bloqueiaTela();
        if (jQuery('#medico1').val() !== "") {
            jQuery.post(url + '/agencia/dadosObitoVelorio/medicos', {medico: jQuery('#medico1').val()}, function (dados){
                liberaTela();
                if (dados) {
                   // jQuery('#enderecoCemiterio').val(dados['contratante']['nomeContratante']);
                  for (var i = 0; i < dados.length; i++) {
                        $('#medico1').val(dados[i]['medico1']);
                        $('#crm1').val(dados[i]['crm1']);
                   }
                }

            }, 'json');
        }
    });
     jQuery('#BuscaMedico2').on('click', function (){
        bloqueiaTela();
        if (jQuery('#medico2').val() !== "") {
            jQuery.post(url + '/agencia/dadosObitoVelorio/medicos', {medico: jQuery('#medico2').val()}, function (dados){
                liberaTela();
                if (dados) {
                
                  for (var i = 0; i < dados.length; i++) {
                        $('#medico2').val(dados[i]['medico1']);
                        $('#crm2').val(dados[i]['crm1']);
                   }
                }

            }, 'json');
        }
    });
        jQuery('#BuscaVelorio').on('click', function (){
        bloqueiaTela();
        if (jQuery('#localVelorio').val() !== "") {
            jQuery.post(url + '/agencia/dadosObitoVelorio/localVelorio', {localVelorio: jQuery('#localVelorio').val()}, function (dados){
                liberaTela();
                if (dados) {
                 
                  for (var i = 0; i < dados.length; i++) {
                      $('#localVelorio').val(dados[i]['localVelorio']);
                      $('#enderecoVelorio').val(dados[i]['enderecoVelorio']);
                   }
                }

            }, 'json');
        }
    });
        jQuery('#BuscaLocalFalecimento').on('click', function (){
        bloqueiaTela();
        if (jQuery('#localFalecimento').val() !== "") {
            jQuery.post(url + '/agencia/dadosObitoVelorio/localFalecimento', {localFalecimento: jQuery('#localFalecimento').val()}, function (dados){
                liberaTela();
                if (dados) {
                 
                  for (var i = 0; i < dados.length; i++) {
                       $('#localFalecimento').val(dados[i]['localFalecimento']);
                       $('#cartorio').val(dados[i]['cartorio']);
                       $('#cartorioId').val(dados[i]['cartorioId']);
                       $('#enderecoFalecimento').val(dados[i]['enderecoFalecimento']);
                       $('#localFalecimentoId').val(dados[i]['id']);
                   }
                }

            }, 'json');
        }
    });
   
    
    
   
    
    $("#remocao").click(function () {  
      if( $("#remocao").is(':checked') ){
              $('.mostraTransporte').css('display','none');
           
      } else {
               $('.mostraTransporte').css('display','block');
      }
  
   });

     
});
