jQuery(document).ready(function () {
    jQuery("input[name='notaFiscal']").on('change', function () {
        jQuery('#hdnNomeNota').val("");
        jQuery('#hdnNotaFiscal').val("");
    });
    
    jQuery("#cpfRg").keypress(function(){
    try {
        jQuery("#cpfRg").unmask();
    } catch (e) {}

    var tamanho = jQuery("#cpfRg").val().length;

    if(tamanho < 10){
        jQuery("#cpfRg").mask("99999999999");
    } else {
        jQuery("#cpfRg").mask("999.999.999-99");
    }                   
});
});
