var table;

jQuery(document).ready(function () {
    table = jQuery('#recebimentos').dataTable({
        "processing": true,
        "serverSide": true,
        "language": {
            "url": url + "/assets/global/scripts/traducao.json"
        },
        "ajax": {
            "url": url + "/estoque/recebimento/paginacao",
            "type": "POST",
            "data": function (d) {
                
            }
        },
        "drawCallback": function (oSettings) {
            App.initUniform($('input[type="checkbox"]', table));

        },
        "columns": [
            {"orderable": false},
            {"orderable": true},
            {"orderable": true},
            {"orderable": true},
            {"orderable": true}
        ],
        "order": [],
        "sDom": '<"top"fl>rt<"bottom"ip><"clear">',
    });


    jQuery('.group-checkable', table).change(function () {
        var set = table.find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
        var checked = jQuery(this).prop("checked");
        jQuery(set).each(function () {
            jQuery(this).prop("checked", checked);
        });
        jQuery.uniform.update(set);
    });
    
    if(jQuery('#atestado').val() !== ""){
        //location.href=url+'/estoque/recebimento/gerarAtestado/'+jQuery('#atestado').val();
    }

    jQuery('#btn-recebimento-export-doc').click(function(e){
        
        var selected = jQuery('table#recebimentos').find('input[name="checkbox-list"]:checked');
        
        if(selected.length > 0) {
            
            window.location = url + "/estoque/recebimento/exportar-doc/" + selected.first().val();
        }
        else {
            
            alert('Selecione um recebimento antes de tentar exportar o DOC');
        }
    });

});

function excluir() {

    var ids = getCheckboxMarcados('recebimentos');

    if (ids == "") {
        return false;
    }

    jQuery.post(url + "/estoque/recebimento/deletar", {'ids': ids}, function (data) {
        var title;

        if (data.success === 1) {
            jQuery("#modalSuccess").modal();
        } else {
            //jQuery("#modalError").modal();
            jQuery("#modalError").modal().find(".modal-title").text("Aviso");
            jQuery("#modalError").modal().find(".modal-body").text("Não foi possível completar a ação! Não é possível excluir recebimentos de períodos fechados.");
        }
        table.fnDraw();
    }, 'json');


}
