var table;

jQuery(document).ready(function () {
    table = jQuery('#recebimentosTransferencia').dataTable({
        "processing": true,
        "serverSide": true,
        "language": {
            "url": url + "/assets/global/scripts/traducao.json"
        },
        "ajax": {
            "url": url + "/estoque/recebimentoTransferencia/paginacao",
            "type": "POST",
            "data": function (d) {
                d.situacao = jQuery("#situacao").val();
            }
        },
        "drawCallback": function (oSettings) {
            App.initUniform($('input[type="checkbox"]', table));

        },
        "columns": [
            {"orderable": false},
            {"orderable": true},
            {"orderable": true},
            {"orderable": false},
            {"orderable": true},
            {"orderable": true},
            {"orderable": true},
            {"orderable": false}
        ],
        "order": [],
        "sDom": '<"top"fl>rt<"bottom"ip><"clear">',
    });


    jQuery('.group-checkable', table).change(function () {
        var set = table.find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
        var checked = jQuery(this).prop("checked");
        jQuery(set).each(function () {
            jQuery(this).prop("checked", checked);
        });
        jQuery.uniform.update(set);
    });
    
    if(jQuery('#atestado').val() !== ""){
        //location.href=url+'/estoque/recebimento/gerarAtestado/'+jQuery('#atestado').val();
    }

    jQuery("#situacao").change(function (){
        table.fnDraw();
    });

});

