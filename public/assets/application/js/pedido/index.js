var table;

jQuery(document).ready(function () {
    table = jQuery('#pedidos').dataTable({
        "processing": true,
        "serverSide": true,
        "language": {
            "url": url + "/assets/global/scripts/traducao.json"
        },
        "ajax": {
            "url": url + "/estoque/pedido/paginacao",
            "type": "POST",
            "data": function (d) {
                d.situacao = jQuery("#situacao").val();
            }
        },
        "drawCallback": function (oSettings) {
            App.initUniform($('input[type="checkbox"]', table));

        },
        "columns": [
            {"orderable": false},
            {"orderable": true},
            {"orderable": true},
            {"orderable": true},
            {"orderable": true},
            {"orderable": true},
            {"orderable": false},
            {"orderable": false}


        ],
        "order": [],
        "sDom": '<"top"fl>rt<"bottom"ip><"clear">',
    });


    jQuery('.group-checkable', table).change(function () {
        var set = table.find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
        var checked = jQuery(this).prop("checked");
        jQuery(set).each(function () {
            jQuery(this).prop("checked", checked);
        });
        jQuery.uniform.update(set);
    });

    jQuery("#situacao").change(function (){
        table.fnDraw();
    });
  
});

function excluir()
{
    var ids = getCheckboxMarcados('pedidos');
    if (ids == "") {
        return false;
    }
    jQuery.post(url + "/estoque/pedido/deletar", {'ids': ids}, function (data) {
        var title;

        if (data.success === 1) {
            jQuery("#modalSuccess").modal();
        } else {
            jQuery("#modalError").modal().find(".modal-title").text("Aviso");
            jQuery("#modalError").modal().find(".modal-body").text("Apenas pedidos não concluídos poderam ser excluidos.");
        }
        table.fnDraw();
    }, 'json');
}

function showModalCancelarPedido(id)
{
    jQuery("#modalDelete").modal();
    jQuery(".btnConfirmarDelete").attr("onclick","cancelarPedido("+id+")");
}

function cancelarPedido(id)
{
    jQuery.post(url + "/estoque/pedido/cancelarPedido", {'id': id}, function (data) {
        if (data.success === 1) {
            jQuery("#modalSuccess").modal();
        } else {
            jQuery("#modalErrorCancelar").modal();
        }
        table.fnDraw();
    }, 'json');
}