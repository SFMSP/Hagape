jQuery(document).ready(function () {


    jQuery('#fornecedor').change(function () {

        if (jQuery('#fornecedor').val() !== "") {
            popularContratos(jQuery('#fornecedor').val(), 0);

        } else {
            jQuery('#contrato').html("");
            jQuery('#contrato').append("<option value=''>Selecione um Contrato</option>");

            jQuery('#estoque').html("");
            jQuery('#estoque').append("<option value=''>Selecione um Estoque</option>");
        }

    });

    jQuery('#contrato').change(function () {
        if (jQuery('#fornecedor').val() !== "") {
            popularEmpenhos(jQuery('#contrato').val(), 0);

        } else {
            jQuery('#estoque').html("");
            jQuery('#estoque').append("<option value=''>Selecione um Estoque</option>");
        }

    });

    jQuery('#empenho').change(function () {
        selecionarEmpenho(jQuery('#empenho').val());
    });

    if (jQuery('input[name="id-item-pedido[]"]').length > 0) {
        jQuery('#fornecedor').attr('disabled', true);
        jQuery('#contrato').attr('disabled', true);
        jQuery('#empenho').attr('disabled', true);
    }


    jQuery('.btn-enviar').click(function () {
        jQuery('#acao').val((jQuery(this).val()));

        jQuery('#fornecedor').attr('disabled', false);
        jQuery('#contrato').attr('disabled', false);
        jQuery('#empenho').attr('disabled', false);

        jQuery('#pedido').submit();
    });

    jQuery('.autocomplete-select').select2({
        placeholder: "Selecione um Item",
        width: 'auto',
        allowClear: true
    });
     jQuery('.autocomplete-select2').select2({
        placeholder: "Selecione um Fornecedor",
        width: 'auto',
        allowClear: true
    });

    jQuery('.autocomplete-select').change(function () {
       popularMedida(jQuery('#produto').val());
       
    });
});

/**
 * Popula o menu select de contratos
 * 
 * @param {integer} id_fornecedor
 */
function popularContratos(id_fornecedor) {

    jQuery.post(url + '/estoque/pedido/getContratos', {fornecedor: id_fornecedor}, function (dados) {
        jQuery('#contrato').html("");
        jQuery('#contrato').append("<option value=''>Selecione um Contrato</option>");

        for (var i = 0; i < dados.length; i++) {
            jQuery('#contrato').append("<option value=" + dados[i]['id'] + ">" + dados[i]['nome'] + "</option>");
        }

    }, 'json');
}

/**
 * Popula medida
 * 
 * @param {integer} id_produto
 */
function popularMedida(id_produto) {
   jQuery.post(url + '/estoque/pedido/getProdutos', {produto: jQuery('#produto').val()}, function (medidas) {
       jQuery('#medida').val(medidas.medida);
   }, 'json');
}
/**
 * Popula o menu select de empenhos
 *
 * @param {integer} id_contrato
 */
function popularEmpenhos(id_contrato) {
    jQuery.post(url + '/estoque/pedido/getEmpenhos', {contrato: id_contrato}, function (dados) {
        jQuery('#empenho').html("");
        jQuery('#empenho').append("<option value=''>Selecione um Empenho</option>");

        for (var i = 0; i < dados.length; i++) {
            jQuery('#empenho').append("<option value=" + dados[i]['id'] + ">" + dados[i]['nome'] + "  (R$ " + formatMoney(dados[i]['valor'], 2, ',', '.') + ")</option>");
        }

    }, 'json');
}

/**
 * Função que adiciona o valor do empenho na área de pedido após o usuário ter selecionado o empenho
 * 
 * @param {integer} empenho
 */
function selecionarEmpenho(empenho) {
    if (empenho !== "") {
        jQuery.post(url + '/estoque/pedido/getEmpenhoDisponivel', {empenho: empenho}, function (dados) {
            jQuery('#label-valor-empenho-disponivel').html("R$ " + formatMoney(dados.valorDisponivel, 2, ',', '.'));
            jQuery('#valor-empenho-disponivel').val(dados.valorDisponivel);
        }, 'json');
    } else {
        jQuery('#label-valor-empenho-disponivel').html("R$ 0,00");
        jQuery('#valor-empenho-disponivel').val(0);
    }

}

/**
 * Função que altera o valor do empenho disponível na label do empenho e em seu hidden
 * 
 * @param {float} valor
 */
function alterarEmpenhoDisponivel(valor) {
    jQuery('#valor-empenho-disponivel').val(valor);
    jQuery('#label-valor-empenho-disponivel').html("R$ " + formatMoney(valor, 2, ',', '.'));
}

/**
 * Função que adiciona um produto na lista de itens do pedido
 * 
 * @returns {Boolean}
 */
function adicionarProduto() {

    //Valida se um empenho foi selecionado
    if (jQuery('#empenho').val() == "" || jQuery('#empenho').val() == null) {
        alert('Selecione um Empenho');
        return false;
    }

    //Valida se um produto foi selecionado
    if (jQuery('#produto').val() == "" || jQuery('#produto').val() == null) {
        alert('Selecione um Produto');
        return false;
    }

    //Valida se o preço unitário foi informado
    if (jQuery('#precoUnitario').val() == "") {
        alert('Informe o Preço Unitário');
        return false;
    }

    //Valida se a quantidade foi informada
    if (jQuery('#quantidade').val() == "") {
        alert('Informe a Quantidade');
        return false;
    }

    var empenhoRestante = jQuery('#valor-empenho-disponivel').val() - (convertMaskToFloat(jQuery('#quantidade').val()) * convertMaskToFloat(jQuery('#precoUnitario').val()));
    empenhoRestante = empenhoRestante.toFixed(2);

    if (empenhoRestante < 0) {
        alert('O valor total ultrapassa o limite do empenho');
        return false;
    }

    //Realiza o calculo da quantidade total de  
    var valorTotal = convertMaskToFloat(jQuery('#precoUnitario').val()) * jQuery('#quantidade').val();

    //Cria as variáveis que receberão o código e nome do produto
    var produto = jQuery('#produto option[value="' + jQuery('#produto').val() + '"]').text();
    produto = produto.split('-');

    var codigoProduto = produto[0];
    produto.shift();

    var nomeProduto = produto.join();

    //Deleta a linha que indica que nenhum produto foi selecionado caso ela exista
    if (jQuery('#nenhum-produto-selecionado')) {
        jQuery('#nenhum-produto-selecionado').remove();
    }

    //Cria a variável que irá armazenar o html da tabela
    var html = "";

    //Abre a tr referente da linha que será adicionada
    html += "<tr>";

    //Cria a td que recebe o código do produto e os hiddens com seus dados
    html += "<td>";
    html += "<input type='hidden' name='id-item-pedido[]' value='0' />";
    html += "<input type='hidden' name='id-produto[]' value='" + jQuery('#produto').val() + "' />";
    html += "<input type='hidden' name='codigo-produto[]' value='" + codigoProduto + "' />";
    html += "<input type='hidden' name='nome-produto[]' value='" + nomeProduto + "' />";
    html += "<input type='hidden' name='preco-unitario-produto[]' value='" + convertMaskToFloat(jQuery('#precoUnitario').val()) + "' />";
    html += "<input type='hidden' name='qtd-produto[]' value='" + jQuery('#quantidade').val() + "' />";
    html += "<input type='hidden' name='total-valor-produto[]' value='" + valorTotal + "' />";
    html += codigoProduto;
    html += "</td>";

    html += "<td>" + nomeProduto + "</td>";
    html += "<td>R$ " + jQuery('#precoUnitario').val() + "</td>";
    html += "<td>" + jQuery('#quantidade').val() + "</td>";
    html += "<td>R$ " + formatMoney(valorTotal, 2, ',', '.') + "</td>";
    html += "<td><a onclick='excluirItemPedido(jQuery(this));' class='glyphicon glyphicon-trash btn-excluir-tabela'></a></td>";
    html += "</tr>";

    //Adiciona a nova linha a tabela e remove o produto das opções
    jQuery('#tabela-produtos tbody').append(html);
    jQuery('#produto option[value="' + jQuery('#produto').val() + '"]').remove();

    //Desabilita os campos do fornecedor
    jQuery('#fornecedor').attr('disabled', true);
    jQuery('#contrato').attr('disabled', true);
    jQuery('#empenho').attr('disabled', true);

    //Atualiza o valor do empenho disponivel
    alterarEmpenhoDisponivel(empenhoRestante);

    jQuery('.autocomplete-select').select2({
        language: {
            noResults: function () {
                return "Nenhum Resultado Encontrado";
            }
        },
        placeholder: "Selecione um Item",
        width: 'auto',
        allowClear: true
    });
}

/**
 * Função que exclui um produto da lista de itens do pedido 
 * 
 * @param {object} linha
 */
function excluirItemPedido(linha) {
    //Armazena o id do item do pedido
    var idItemPedido = linha.parent().parent().find("input[name='id-item-pedido[]']").val();

    //Armazena os dados do produto para recompor o select
    var idProduto = linha.parent().parent().find("input[name='id-produto[]']").val();
    var codigoProduto = linha.parent().parent().find("input[name='codigo-produto[]']").val();
    var nomeProduto = linha.parent().parent().find("input[name='nome-produto[]']").val();

    //Armazena o valor total do item
    var valorTotal = new Number(linha.parent().parent().find("input[name='total-valor-produto[]']").val());

    //Readiciona o produto ao select de produtos e o reordena
    jQuery('#produto').append('<option value="' + idProduto + '">' + codigoProduto + ' - ' + nomeProduto + '</option>');
    ordenarSelectProdutos();
    jQuery('#produto').val('');

    //Deleta a linha da tabela
    linha.parent().parent().remove();

    //Verifica se existe algum produto
    if (!jQuery('#tabela-produtos').find('tbody tr').length) {

        //Adiciona a linha indicando que não há nenhum produto selecionado
        jQuery('#tabela-produtos').append('<tr id="nenhum-produto-selecionado"><td colspan="6"><i>Nenhum Produto Selecionado</i></td></tr>');

        //Habilita os campos do fornecedor
        jQuery('#fornecedor').attr('disabled', false);
        jQuery('#contrato').attr('disabled', false);
        jQuery('#empenho').attr('disabled', false);
    }

    //Caso o item do pedido já esteja cadastrado no banco de dados, ele é adicionado a lista de itens a serem excluídos
    if (idItemPedido != 0) {
        jQuery('#idItensExcluidos').val(jQuery('#idItensExcluidos').val() + idItemPedido + ",");
    }


    //Altera o valor do empenho disponível
    var valorDisponivelEmpenho = new Number(jQuery('#valor-empenho-disponivel').val());
    valorDisponivelEmpenho = valorTotal + valorDisponivelEmpenho;

    alterarEmpenhoDisponivel(valorDisponivelEmpenho);


}

function ordenarSelectProdutos() {
    ordenarSelects('produto');
}