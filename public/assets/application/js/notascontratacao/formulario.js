jQuery(document).ready(function (){


    jQuery('#cpf').on('change', function (){
        bloqueiaTela();
        if (jQuery('#cpf').val() !== "") {
            jQuery.post(url + '/agencia/contratante/getDadosPorCpf', {cpf: jQuery('#cpf').val()}, function (dados){
                liberaTela();
                if (dados) {
                    jQuery('#nome').val(dados['contratante']['nomeContratante']);
                    jQuery('#grauParentesco').val(dados['contratante']['grauParentesco']);
                    jQuery('#telefoneResidencial').val(dados['contratante']['telefoneResidencial']);
                    jQuery('#telefoneCelular').val(dados['contratante']['telefoneCelular']);
                    jQuery('#email').val(dados['contratante']['email']);
                    jQuery('#rg').val(dados['contratante']['rg']);
                    jQuery('#profissao').val(dados['contratante']['profissao']);
                    jQuery('#nomeMae').val(dados['contratante']['nomeMae']);

                    jQuery('#endereco').val(dados['localizacao']['endereco']);
                    jQuery('#bairro').val(dados['localizacao']['bairro']);
                    jQuery('#numero').val(dados['localizacao']['numero']);
                    jQuery('#complemento').val(dados['localizacao']['complemento']);
                    jQuery('#cep').val(dados['localizacao']['cep']);
                    jQuery('#estado').val(dados['localizacao']['estado']);
                    mudarEstado(dados['localizacao']['cidade']);
                    jQuery('#empresa').val(dados['empresa']['nomeEmpresarial']);
                    jQuery('#cnpj').val(dados['empresa']['cnpj']);
                    jQuery('#enderecoEmpresa').val(dados['empresa']['endereco']);
                    jQuery('#telefoneEmpresa').val(dados['empresa']['telefone']);
                    jQuery('#representante').val(dados['empresa']['representante']);
                }

            }, 'json');
        }
    });


    if (!jQuery('#empresaAcompanha').parent().hasClass('checked')) {
        jQuery('#dadosEmpresa').fadeOut('fast');
    }
    jQuery('#empresaAcompanha').on('change', function (){
        if (jQuery('#empresaAcompanha').parent().hasClass('checked')) {
            jQuery('#dadosEmpresa').fadeIn('fast');
        } else {
            jQuery('#dadosEmpresa').fadeOut('fast');
        }
    });


});//End jQuery Ready

function mudarEstado(cidade){
    if (jQuery('#estado').val() !== "") {
        jQuery('#cidade').attr('disabled', true);
        bloqueiaTela();
        //Função padrão que realiza a busca das cidades de um estado
        jQuery.post(url + '/admin/estoque/getCidades', {estado: jQuery('#estado').val()}, function (dados){
            liberaTela();
            jQuery('#cidade').html("");

            for (var i = 0; i < dados.length; i++) {
                if (cidade == dados[i]['id']) {
                    jQuery('#cidade').append("<option value=" + dados[i]['id'] + " selected>" + dados[i]['nome'] + "</option>");
                } else {
                    jQuery('#cidade').append("<option value=" + dados[i]['id'] + ">" + dados[i]['nome'] + "</option>");
                }
            }
            jQuery('#cidade').removeAttr('disabled');

        }, 'json');
    } else {
        jQuery('#cidade').html("");
        jQuery('#cidade').append("<option value=''>Escolha uma Cidade</option>");
    }
}