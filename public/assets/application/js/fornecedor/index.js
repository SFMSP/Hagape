
var table;

jQuery(document).ready(function () {
    

//: center;
  jQuery("thead > tr > th").css('text-align', 'center');

         //table.table-bordered.dataTable tbody th, table.table-bordered.dataTable tbody td
    table = jQuery('#fornecedores').dataTable({
         
        "processing": true,
        "serverSide": true,
        
        "language": {
            "url": url + "/assets/global/scripts/traducao.json"
        },
        "ajax": {
            "url": url + "/estoque/fornecedor/paginacao",
            "type": "POST",
            "data": function (d) {
               d.nome = jQuery('#nome').val()
              
                       
            }
        },
        "drawCallback": function (oSettings) {
            App.initUniform($('input[type="checkbox"]', table));

        },
      "columns": [
            {"orderable": false},
            {"orderable": true},
            {"orderable": true},
            {"orderable": true},
            {"orderable": true}
            
        ],
           
        
     
        "sDom": '<"top"fl>rt<"bottom"ip><"clear">',
        "order": []
      
    });
    jQuery('#nome').change(function () {
        table.fnDraw();
    });
    
   
     jQuery('.group-checkable', table).change(function () {
        var set = table.find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
        var checked = jQuery(this).prop("checked");
        
        jQuery(set).each(function () {
            jQuery(this).prop("checked", checked);
        });
        jQuery.uniform.update(set);
        countSelectedRecords();
    });

});

    
function excluir() {

    var ids = getCheckboxMarcados('fornecedores');

    if (ids == "") {
       
        return false;
    }

    jQuery.post(url + "/estoque/fornecedor/deletar", {'ids': ids, 'status':status}, function (data) {
        var title;

        if (data.success === 1) {
            jQuery("#modalSuccess").modal();
        } else {
           // jQuery("#modalError").modal();
           
            jQuery("#modalError").modal().find(".modal-title").text("Aviso");
            jQuery("#modalError").modal().find(".modal-body").text("Não é possível alterar o excluír do registro, o mesmo possuí contrato ativo.");
        }

        table.fnDraw();
    }, 'json');


}


function mudarStatus(status) {

    var ids = getCheckboxMarcados('fornecedores');

    if (ids == "") {
        return false;
    }

    jQuery.post(url + "/estoque/fornecedor/mudarStatus", {'ids': ids, 'status':status}, function (data) {
        var title;

        if (data.success === 1) {
            jQuery("#modalSuccess").modal();
        } else {
            jQuery("#modalError").modal().find(".modal-title").text("Aviso");
            jQuery("#modalError").modal().find(".modal-body").text("Não é possível alterar o status do registro, o mesmo possuí contrato ativo.");
        }
        

        table.fnDraw();
    }, 'json');


}