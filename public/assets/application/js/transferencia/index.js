var table;

jQuery(document).ready(function () {
    table = jQuery('#transferencias').dataTable({
        "processing": true,
        "serverSide": true,
        "language": {
            "url": url + "/assets/global/scripts/traducao.json"
        },
        "ajax": {
            "url": url + "/estoque/transferencia/paginacao",
            "type": "POST",
            "data": function (d) {
                d.situacao = jQuery('#situacao').val();
            }
        },
        "drawCallback": function (oSettings) {
            App.initUniform($('input[type="checkbox"]', table));
            jQuery('.colorbox').colorbox({width: '60%'});

        },
        "columns": [
            {"orderable": false},
            {"orderable": true},
            {"orderable": true},
            {"orderable": true},
            {"orderable": true},
            {"orderable": true},
            {"orderable": false},
            {"orderable": false}


        ],
        "order": [],
        "sDom": '<"top"fl>rt<"bottom"ip><"clear">',
    });


    jQuery('.group-checkable', table).change(function () {
        var set = table.find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
        var checked = jQuery(this).prop("checked");
        jQuery(set).each(function () {
            jQuery(this).prop("checked", checked);
        });
        jQuery.uniform.update(set);
    });

    jQuery('#situacao').change(function () {
        table.fnDraw();
    });

});

function excluir() {

    var ids = getCheckboxMarcados('transferencias');

    if (ids == "") {
        return false;
    }

    jQuery.post(url + "/estoque/transferencia/deletar", {'ids': ids}, function (data) {
        var title;

        if (data.success === 1) {
            jQuery("#modalSuccess").modal();
        } else {
            jQuery("#modalErrorExcluirTransferencia").modal();
        }
        table.fnDraw();
    }, 'json');


}

function cancelarTransferencia(id) {
    jQuery.post(url + "/estoque/transferencia/cancelarTransferencia", {'id': id}, function (data) {
        if (data.success === 1) {
            jQuery("#modalSuccess").modal();
        } else {
            jQuery("#modalErrorCancelar").modal();
        }
        table.fnDraw();
    }, 'json');
}