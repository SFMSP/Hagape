var table;

jQuery(document).ready(function () {
    table = jQuery('#descartes1').dataTable({
        "processing": true,
        "serverSide": true,
        "language": {
            "url": url + "/assets/global/scripts/traducao.json"
        },
        "ajax": {
            "url": url + "/estoque/descarte/paginacao",
            "type": "POST",
            "data": function (d) {
               d.nome = jQuery('#nome').val()
            }
        },
        "drawCallback": function (oSettings) {
            App.initUniform($('input[type="checkbox"]', table));

        },
      
        "order": [],
        "sDom": '<"top"fl>rt<"bottom"ip><"clear">',
    });

   

    jQuery('.group-checkable', table).change(function () {
        var set = table.find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
        var checked = jQuery(this).prop("checked");
        jQuery(set).each(function () {
            jQuery(this).prop("checked", checked);
        });
        jQuery.uniform.update(set);
    });

});
jQuery(document).ready(function () {
    table = jQuery('#descartes').dataTable({
        "processing": true,
        "serverSide": true,
        "language": {
            "url": url + "/assets/global/scripts/traducao.json"
        },
        "ajax": {
            "url": url + "/estoque/descarte/paginacao",
            "type": "POST",
            "data": function (d) {
               d.nome = jQuery('#nome').val()
            }
        },
        "drawCallback": function (oSettings) {
            App.initUniform($('input[type="checkbox"]', table));

        },
      
        "order": [],
        "sDom": '<"top"fl>rt<"bottom"ip><"clear">',
    });

   

    jQuery('.group-checkable', table).change(function () {
        var set = table.find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
        var checked = jQuery(this).prop("checked");
        jQuery(set).each(function () {
            jQuery(this).prop("checked", checked);
        });
        jQuery.uniform.update(set);
    });

});

function excluir() {

    var ids = getCheckboxMarcados('descartes');

    if (ids == "") {
        return false;
    }

    jQuery.post(url + "/estoque/descarte/deletar", {'ids': ids, 'status':status}, function (data) {
        var title;

        if (data.success === 1) {
            jQuery("#modalSuccess").modal();
        } else {
            jQuery("#modalError").modal();
        }

        table.fnDraw();
    }, 'json');


}
function mudarStatus(status) {

    var ids = getCheckboxMarcados('descartes');

    if (ids == "") {
        return false;
    }

    jQuery.post(url + "/estoque/descarte/mudarStatus", {'ids': ids, 'status':status}, function (data) {
        var title;

        if (data.success === 1) {
            jQuery("#modalSuccess").modal();
        } else {
            jQuery("#modalError").modal();
        }
        

        table.fnDraw();
    }, 'json');


}


