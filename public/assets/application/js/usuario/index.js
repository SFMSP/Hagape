var table;

jQuery(document).ready(function () {
    table = jQuery('#usuarios').dataTable({
        "processing": true,
        "serverSide": true,
        "language": {
            "url": url + "/assets/global/scripts/traducao.json"
        },
        "ajax": {
            "url": url + "/usuario/paginacao",
            "type": "POST",
            "data": function (d) {
                d.perfil = jQuery('#perfil').val()
            }
        },
        "drawCallback": function (oSettings) {
            App.initUniform($('input[type="checkbox"]', table));

        },
        "columns": [
            {"orderable": false},
            {"orderable": true},
            {"orderable": true},
            {"orderable": true},
            {"orderable": true}
        ],
        "sDom": '<"top"fl>rt<"bottom"ip><"clear">',
        "order": []

    });

    jQuery('#perfil').change(function () {
        table.fnDraw();
    });

    jQuery('.group-checkable', table).change(function () {
        var set = table.find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
        var checked = jQuery(this).prop("checked");
        jQuery(set).each(function () {
            jQuery(this).prop("checked", checked);
        });
        jQuery.uniform.update(set);
        countSelectedRecords();
    });

});

function excluir() {

    var ids = getCheckboxMarcados('usuarios');

    if (ids == "") {
        return false;
    }

    jQuery.post(url + "/usuario/deletar", {'ids': ids}, function (data) {
        var title;

        if (data.success === 1) {
            jQuery("#modalSuccess").modal();
        } else {
            jQuery("#modalError").modal();
        }


        table.fnDraw();
    }, 'json');


}


function mudarStatus(status) {

    var ids = getCheckboxMarcados('usuarios');

    if (ids == "") {
        return false;
    }

    jQuery.post(url + "/usuario/mudarStatus", {'ids': ids, 'status': status}, function (data) {
        var title;

        if (data.success === 1) {
            jQuery("#modalSuccess").modal();
        } else {
            jQuery("#modalError").modal();
        }


        table.fnDraw();
    }, 'json');


}

function mudarSenha() {

    var id = getCheckboxMarcados('usuarios');

    if (id == "") {
        return false;
    }
    
    if(id.length > 1){
        jQuery("#modalSelecionados").modal();
        return false;
    }

    jQuery.post(url + "/usuario/gerarNovaSenha", {'id': id}, function (data) {
        var title;

        if (data.success === 1) {
            jQuery("#modalSuccess").modal();
        } else {
            jQuery("#modalError").modal();
        }


        table.fnDraw();
    }, 'json');


}