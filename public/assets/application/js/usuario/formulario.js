var table;

jQuery(document).ready(function () {

    esconderTodosFiltros();
    montarFiltrosPerfil();

    jQuery('#perfil').change(function () {
        montarFiltrosPerfil();
    });
    
    if(jQuery('#id').val() !== ""){
        jQuery('#senha').parent().parent().remove();
        jQuery('#confirmaSenha').parent().parent().remove();
    }
});

function montarFiltrosPerfil() {
    esconderTodosFiltros();

    if (jQuery('#perfil').val()) {
        jQuery.post(url + '/usuario/filtroPerfil', {'perfil': jQuery('#perfil').val()}, function (data) {

            jQuery.each(data, function (idx, obj) {
                jQuery('#'+obj.filtro).parent().parent().show();
            });

        }, 'json');
    }

}

function esconderTodosFiltros() {
    jQuery('#agencia').parent().parent().hide();
    jQuery('#cemiterio').parent().parent().hide();
    jQuery('#estoque').parent().parent().hide();
    jQuery('#trafego').parent().parent().hide();
    jQuery('#tesouraria').parent().parent().hide();
}