$(document).ready(function () {
    
    $("#parcelamento").attr('disabled',true);
    $("#bandeira").attr('disabled',true);
    
    $('#tipoPagamento').on('change', function (){
      $("#bandeira").val('');
      $("#parcelamento").val('');
      $("#parcelamento").attr('disabled',true);
      
        if($("#tipoPagamento").val()==0){
      $("#bandeira").attr('disabled',true);
     
      }else if($("#tipoPagamento").val()==1){
      $("#bandeira").attr('disabled',true);
      }else if($("#tipoPagamento").val()==2){
      $("#bandeira").attr('disabled',false);
      }else if($("#tipoPagamento").val()==3){
        $("#bandeira").attr('disabled',false);  
      }
        
        
    });
    $('#bandeira').on('change', function (){
       if($("#bandeira").val()==0){
      $("#parcelamento").attr('disabled',false);
      }else if($("#bandeira").val()==1){
      $("#parcelamento").attr('disabled',false);
      }else if($("#bandeira").val()==3){
      $("#parcelamento").attr('disabled',false);
      } 
    });

  $('#adicionar').on('click', function (){
      
      
      
      
       if($("#emitente").val()==1){
      
        var emitente = 'Contratante';  
      }else if($("#emitente").val()==2){
        var emitente = 'Convenio';   
      }else if($("#emitente").val()==3){
          var emitente = 'Representante';   
      }else if($("#emitente").val()==4){
          var emitente = 'Outros';   
      }else if($("#emitente").val()==0){
          alert('Selecione um Emitente');
        return false;
      }
      
      if($("#tipoPagamento").val()==1){
      
        var tipoPagamento = 'Dinheiro';  
      }else if($("#tipoPagamento").val()==2){
        var tipoPagamento = 'Cartão de Crédito';   
      }else if($("#tipoPagamento").val()==3){
        var tipoPagamento = 'Cartão de Débito';   
      }else if($("#tipoPagamento").val()==0){
        var tipoPagamento = '';   
      }
      
      if($("#bandeira").val()==1){
          var bandeira = 'Visa';
      }else if($("#bandeira").val()==2){
          var bandeira = 'Master';
      }else if($("#bandeira").val()==''){
          var bandeira = '';
          
        }
        if($("#parcelamento").val()==1){
          var parcelamento = '1X';
      }else if($("#parcelamento").val()==2){
          var parcelamento = '2X';
      }else if($("#parcelamento").val()==3){
          var parcelamento = '3X';
      }else {
          var parcelamento = '';
      }
      
      if($("#nome").val()==''){
          alert('Informe um Nome');
          return false
       }
       if($("#documento").val()==''){
          alert('Informe um Documento');
          return false
       }
      if($("#telefoneResidencial").val()==''){
          alert('Informe um Telefone');
          return false
      }
      if($("#endereco").val()==''){
          alert('Informe um Endereço');
          return false
        }
      if($("#tipoPagamento").val()==''){
          alert('Informe Forma de Pagamento');
          return false
      } 
      if($("#pinpad").val()==''){
          alert('Informe o PinPad');
          return false
      }
      if($("#cvdoc").val()==''){
          alert('Informe um CV/DOC');
          return false
      }
       if($("#valor").val()==''){
          alert('Informe o Valor');
          return false
      }
  
        bloqueiaTela();
     var html = "";

    //html +='<table>';
    html +='<tr style="width:100%">';
    html +='<td width="5%"><input type="checkbox" class="group-checkable" /></td>';
    html +='<td width="45%">';
    html +='<table>';
    html +='<tr>';
    html +='<td width="50%">Emitente:</td><td><input type="hidden" name="emitente[]" value="'+ $("#emitente").val() +'">'+ emitente +'</td>';
    html +='</tr>';
    html +='<tr>';
    html +='<td width="50%">Documento:</td><td><input type="hidden" name="documento[]" value="'+ $("#documento").val() +'">'+ $("#documento").val() +'</td>';
    html +='</tr>';
    html +='<tr>';
    html +='<td width="20%">Tipo Pagamento:</td><td><input type="hidden" name="tipoPagamento[]" value="'+ $("#tipoPagamento").val() +'">'+ tipoPagamento +'</td>';
    html +='</tr>';
    html +='<tr>';
    html +='<td width="20%">PinPad:</td><td><input type="hidden" name="pinpad[]" value="'+ $("#pinpad").val() +'">'+ $("#pinpad").val() +'</td>';
    html +='</tr>';
    html +='<tr>';
    html +='<td width="20%">Valor:</td><td><input type="hidden" name="valor[]" value="'+ $("#valor").val() +'">'+ $("#valor").val() +'</td>';
    html +='</tr>';
    html +='<tr>';
    html +='<td width="20%">Nome:</td><td><input type="hidden" name="nome[]" value="'+ $("#nome").val() +'">'+ $("#nome").val() +'</td>';
    html +='</tr>';
    html +='</table>';
    html +='</td>';
    html +='<td width="45%">';
    html +='<table>';
    html +='<tr>';
    html +='<td width="50%">Telefone:</td><td><input type="hidden" name="telefoneResidencial[]" value="'+ $("#telefoneResidencial").val() +'">'+ $("#telefoneResidencial").val() +'</td>';
    html +='</tr>';
    html +='<tr>';
    html +='<td width="50%">Endereço:</td><td><input type="hidden" name="endereco[]" value="'+ $("#endereco").val() +'">'+ $("#endereco").val() +'</td>';
    html +='</tr>';
    html +='<tr>';
    html +='<td width="20%">Bandeira:</td><td><input type="hidden" name="bandeira[]" value="'+ $("#bandeira").val() +'">'+ bandeira +'</td>';
    html +='</tr>';
    html +='<tr>';
    html +='<td width="20%">Parcelamento:</td><td><input type="hidden" name="parcelamento[]" value="'+ $("#parcelamento").val() +'">'+ parcelamento +'</td>';
    html +='</tr>';
    html +='<tr>';
    html +='<td width="20%">Autorização:</td><td><input type="hidden" name="autorizacao[]" value="'+ $("#autorizacao").val() +'">'+ $("#autorizacao").val() +'</td>';
    html +='</tr>';
    html +='<tr>';
    html +='<td width="20%">CV/DOC:</td><td><input type="hidden" name="cvdoc[]" value="'+ $("#cvdoc").val() +'">'+ $("#cvdoc").val() +'</td>';
    html +='</tr>';
    html +='</table>';
    html +='</td>';
    html +='</tr>';
    //html +='</table>';
       

            $('#pagamentos').append(html);   
      
      liberaTela();
             });
             
             
   $('#emitente').on('change', function (){
        bloqueiaTela();
        if ($('#emitente').val() ==1) {
            $.post(url + '/agencia/pagamento/getContratante', {contratante: $('#contratante').val()}, function (dados){
                liberaTela();
                if (dados) {
                   $('#nome').val(dados['nome']);
                   $('#telefoneResidencial').val(dados['telefone']);
                   $('#documento').val(dados['cpf']);
                  }

            }, 'json');
        }else if ($('#emitente').val() ==2) {
            $.post(url + '/agencia/pagamento/getContratante', {contratante: $('#contratante').val()}, function (dados){
                liberaTela();
                if (dados) {
                   $('#nome').val(dados['nome']);
                   $('#telefoneResidencial').val(dados['telefone']);
                   $('#documento').val(dados['cpf']);
                  }

            }, 'json');
        }else if ($('#emitente').val() ==3) {
                liberaTela();
            
                   $('#nome').val('');
                   $('#telefoneResidencial').val('');
                   $('#documento').val('');
        }else if ($('#emitente').val() ==4) {
                liberaTela();
            
                   $('#nome').val('');
                   $('#telefoneResidencial').val('');
                   $('#documento').val('');
        }else if($("#emitente").val()==0){
       liberaTela();
      }
    });        
             
});