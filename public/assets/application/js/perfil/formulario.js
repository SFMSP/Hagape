jQuery(document).ready(function () {

    jQuery('#check-all').click(function () {
        var set = jQuery('input[name="privilegio[]"],input[name="privilegios[]"]');
        var checked = jQuery(this).prop("checked");
        jQuery(set).each(function () {
            jQuery(this).prop("checked", checked);
        });
        jQuery.uniform.update(set);
    });
  
    var dados = [
    "1","2","3","4","5",
    "6","7","8","9","10",
    "11","12","13","14","15",
    "16","17","18","19","20",
    "21","22","23","24","25"];

    jQuery.each(dados, function(index, item) {
      jQuery('.grupo' + item).click(function() {     
    var set = jQuery('.filho' + item);
        var checked = jQuery(this).prop("checked");
        jQuery(set).each(function () {
            jQuery(this).prop("checked", checked);
        });
        jQuery.uniform.update(set);

    });
      })
    
    
});
