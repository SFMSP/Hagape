var table;

jQuery(document).ready(function () {
    table = jQuery('#perfis').dataTable({
        "processing": true,
        "serverSide": true,
        "language": {
            "url": url + "/assets/global/scripts/traducao.json"
        },
        "ajax": {
            "url": url + "/perfil/paginacao",
            "type": "POST",
            "data": function (d) {


            }
        },
        "drawCallback": function (oSettings) {
            App.initUniform($('input[type="checkbox"]', table));
            

        },
        "columns": [
            {"orderable": false},
            {"orderable": true}
        ],
        "order": [],
        "sDom": '<"top"fl>rt<"bottom"ip><"clear">',
    });

    jQuery('.group-checkable', table).change(function () {
        var set = table.find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
        var checked = jQuery(this).prop("checked");
        jQuery(set).each(function () {
            jQuery(this).prop("checked", checked);
        });
        jQuery.uniform.update(set);
        countSelectedRecords();
    });
    
});

function excluir(id) {

    var ids = getCheckboxMarcados('perfis');

    if (ids == "") {
        return false;
    }

    jQuery.post(url + "/perfil/deletar", {'ids': ids}, function (data) {
        var title;

        if (data.success === 1) {
            jQuery("#modalSuccess").modal();
        } else {
            jQuery("#modalError").modal();
        }


        table.fnDraw();
    }, 'json');


}