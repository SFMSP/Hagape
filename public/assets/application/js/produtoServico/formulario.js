var valorUrna = 0;
var valorRevestimento = 0;
var quantidadeRevestimento = 0;
var valorProdutoEnfeite = [];
var quantidadeEnfeite = [];
var valorVeu = 0;
var quantidadeVeu = 0;
var valorIluminacao = 0;
var quantidadeIluminacao = 0;
var valorSepultamento = 0;
var valorTransporteCarreto = 0;
var valorTransporteEnterro = 0;
var valorTransporteRemocao = 0;
var valorTransporteCremacao = 0;
var valorTransporteViagem = 0;
var valorTotal = 0;
var valorEssaParamento = 0;
var valorVelorio = 0;
var valorCremacao = 0;
var valorCondolencia = 0;
var valorViagem = 0;
var valorCamaraFria = 0;
var valorOutros = 0;

function bloqueiaCampos(){
    //Radios do botão OUTROS
    jQuery("#creditarDebitarField").hide();
    jQuery("#convenioSelect").hide();
    jQuery("#quantidadeUrna").prop('readOnly', true);
    jQuery("#valorConjuntoUrna").prop('readOnly', true);
    jQuery("#valorConjuntoRevestimento").prop('readOnly', true);
    jQuery("#valorConjuntoVeu").prop('readOnly', true);
    jQuery("#valorConjuntoIluminacao").prop('readOnly', true);
    jQuery("#valorConjuntoSepultamento").prop('readOnly', true);
    jQuery("#valorConjuntoTransporteCarreto").prop('readOnly', true);
    jQuery("#valorConjuntoTransporteEnterro").prop('readOnly', true);
    jQuery("#valorConjuntoTransporteRemocao").prop('readOnly', true);
    jQuery("#valorConjuntoTransporteCremacao").prop('readOnly', true);
    jQuery("#valorConjuntoTransporteViagem").prop('readOnly', true);
    jQuery("#valorTotal").prop('readOnly', true);
    jQuery("#quantidadeRevestimento").prop('readOnly', true);
    jQuery("#quantidadeVeu").prop('readOnly', true);
    jQuery("#quantidadeIluminacao").prop('readOnly', true);
    jQuery("#quantidadeTransporteCarreto").prop('readOnly', true);
    jQuery("#quantidadeTransporteEnterro").prop('readOnly', true);
    jQuery("#quantidadeTransporteRemocao").prop('readOnly', true);
    jQuery("#quantidadeTransporteCremacao").prop('readOnly', true);
    jQuery("#quantidadeTransporteViagem").prop('readOnly', true);
}


jQuery(document).ready(function (){

    jQuery("#convenio").parent().addClass('checked');
    jQuery("#remocaoCorpoLocalFalecimento").parent().addClass('checked');
    jQuery("#remocaoMembroLocalFalecimento").parent().addClass('checked');
    //Bloqueia Campos
    bloqueiaCampos();

    if (jQuery("input[name='convenio'][value='1']").parent().hasClass('checked')) {
        jQuery('#convenioSelect').show();
        jQuery('#convenioSelect').removeAttr('disabled');
    } else {
        jQuery('#convenioSelect').hide();
        jQuery('#convenioSelect').attr('disabled', true);
    }

    if (jQuery("input[name='remocaoCorpoLocalFalecimento'][value='1']").parent().hasClass('checked')) {
        jQuery('#localCorpoRemocao').removeAttr('disabled');
        jQuery('#enderecoCorpoRemocao').removeAttr('disabled');
    } else {
        jQuery('#localCorpoRemocao').val('');
        jQuery('#localCorpoRemocao').attr('disabled', true);
        jQuery('#enderecoCorpoRemocao').val('');
        jQuery('#enderecoCorpoRemocao').attr('disabled', true);
    }

    if (jQuery("input[name='remocaoMembroLocalFalecimento'][value='1']").parent().hasClass('checked')) {
        jQuery('#localMembroRemocao').removeAttr('disabled');
        jQuery('#enderecoMembroRemocao').removeAttr('disabled');
    } else {
        jQuery('#localMembroRemocao').val('');
        jQuery('#localMembroRemocao').attr('disabled', true);
        jQuery('#enderecoMembroRemocao').val('');
        jQuery('#enderecoMembroRemocao').attr('disabled', true);
    }

    jQuery("input[name='convenio']").on('change', function (){
        if (jQuery("input[name='convenio'][value='1']").parent().hasClass('checked')) {
            jQuery('#convenioSelect').show();
            jQuery('#convenioSelect').removeAttr('disabled');
        } else {
            jQuery('#convenioSelect').hide();
            jQuery('#convenioSelect').attr('disabled', true);
        }
    });

    jQuery("input[name='remocaoCorpoLocalFalecimento']").on('change', function (){
        if (jQuery("input[name='remocaoCorpoLocalFalecimento'][value='1']").parent().hasClass('checked')) {
            jQuery('#localCorpoRemocao').removeAttr('disabled');
            jQuery('#enderecoCorpoRemocao').removeAttr('disabled');
        } else {
            jQuery('#localCorpoRemocao').val('');
            jQuery('#localCorpoRemocao').attr('disabled', true);
            jQuery('#enderecoCorpoRemocao').val('');
            jQuery('#enderecoCorpoRemocao').attr('disabled', true);
        }
    });

    jQuery("input[name='remocaoMembroLocalFalecimento']").on('change', function (){
        if (jQuery("input[name='remocaoMembroLocalFalecimento'][value='1']").parent().hasClass('checked')) {
            jQuery('#localMembroRemocao').removeAttr('disabled');
            jQuery('#enderecoMembroRemocao').removeAttr('disabled');
        } else {
            jQuery('#localMembroRemocao').val('');
            jQuery('#localMembroRemocao').attr('disabled', true);
            jQuery('#enderecoMembroRemocao').val('');
            jQuery('#enderecoMembroRemocao').attr('disabled', true);
        }
    });

    /*
     MANIPULACAO DOS CAMPOS DA FIELD URNA
     */

    jQuery("#urna").select2({
        placeholder: 'Selecione uma Urna'
    });

    jQuery("#urna").on('change', function (){
        bloqueiaTela();
        jQuery.post(url + '/agencia/produtoServico/buscadadosproduto', {produto: jQuery("#urna").val()}, function (dados){
            liberaTela();
            if (dados) {
                //retira valor anterior
                valorTotal = (+valorTotal-(convertMaskToFloat(jQuery("#valorConjuntoUrna").val())));
                var fatorRobinHood = +dados[0].valor*(+dados[0].fator/100);
                jQuery("#quantidadeUrna").val(1);
                valorUrna = (+dados[0].valor) + (+fatorRobinHood);
                var valorConjunto = valorUrna;
                valorTotal = (+valorTotal)+(+valorUrna);
                jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
                jQuery("#valorConjuntoUrna").val(jQuery.number(valorConjunto, 2, ',', '.'));
                jQuery("#unidadeMedidaUrna").html(dados[0].txt_medida);
                jQuery("#disponivelUrna").html(dados[0].quantidadeDisponivel);
            }
        }, 'json');
    });

    /*
     MANIPULACAO DOS CAMPOS DA FIELD REVESTIMENTO
     */

    jQuery("#revestimento").select2({
        placeholder: 'Selecione um Revestimento', allowClear: true
    });

    jQuery("#revestimento").on('change', function (){
        if (jQuery("#revestimento").val() != '') {
            bloqueiaTela();
            jQuery.post(url + '/agencia/produtoServico/buscadadosproduto', {produto: jQuery("#revestimento").val()}, function (dados){
                liberaTela();
                if (dados) {
                    //retira valor anterior
                    valorTotal = (+valorTotal-(convertMaskToFloat(jQuery("#valorConjuntoRevestimento").val())));
                    var valorConjunto = 0;
                    if (jQuery("#quantidadeRevestimento").val() == '') {
                        jQuery("#quantidadeRevestimento").removeProp('readOnly').val(1);
                    }
                    var fatorRobinHood = +dados[0].valor*(+dados[0].fator/100);
                    valorConjunto = jQuery("#quantidadeRevestimento").val() * (+dados[0].valor+(+fatorRobinHood));
                    valorRevestimento = +dados[0].valor+(+fatorRobinHood);
                    quantidadeRevestimento = dados[0].quantidadeDisponivel;
                    valorTotal = (+valorTotal)+(+valorRevestimento);
                    jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
                    jQuery("#unidadeMedidaRevestimento").html(dados[0].txt_medida);
                    jQuery("#valorConjuntoRevestimento").val(jQuery.number(valorConjunto, 2, ',', '.'));
                    jQuery("#disponivelRevestimento").html(dados[0].quantidadeDisponivel);
                    jQuery("#quantidadeRevestimento").attr("max", dados[0].quantidadeDisponivel);
                }
            }, 'json');
        } else {
            valorTotal = (+valorTotal-(convertMaskToFloat(jQuery("#valorConjuntoRevestimento").val())));
            jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
            jQuery("#valorConjuntoRevestimento").val('');
            jQuery("#disponivelRevestimento").html('&nbsp;');
            jQuery("#quantidadeRevestimento").val('');
            jQuery("#unidadeMedidaRevestimento").html('&nbsp;');
        }
    });

    jQuery("#quantidadeRevestimento").on('change', function (){
        if (jQuery(this).val() == 0) {
            jQuery(this).val(1);
        }
        var novoValor = valorRevestimento * jQuery("#quantidadeRevestimento").val();
        var valorAnterior = convertMaskToFloat(jQuery("#valorConjuntoRevestimento").val());
        valorTotal = (+valorTotal-(+valorAnterior))+(+novoValor);
        jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
        jQuery("#valorConjuntoRevestimento").val(jQuery.number(novoValor, 2, ',', '.'));
        jQuery("#disponivelRevestimento").html((quantidadeRevestimento - jQuery("#quantidadeRevestimento").val()));
    });

    /*
     MANIPULACAO DOS CAMPOS DA FIELD ENFEITE
     */
    jQuery("#enfeite").select2({
        placeholder: 'Selecione um Enfeite', allowClear: true
    });

    jQuery("#enfeite").on('change', function (){
        if (jQuery("#enfeite").val() != '') {
            bloqueiaTela();
            jQuery.post(url + '/agencia/produtoServico/buscadadosproduto', {categoria: jQuery("#enfeite").val()}, function (dados){
                liberaTela();
                if (dados) {
                    jQuery("#produtosEnfeiteField").html('&nbsp;');
                    dados.forEach(function (dado, index){
                        //CHECKBOX
                        var htmlCheckBox = '<div class="row"><div class="col-md-12">';
                        htmlCheckBox += '   <div class="col-md-5 col-md-offset-1">';
                        htmlCheckBox += '       <div class="form-group">';
                        htmlCheckBox += '           <input type="hidden" name="produtoEnfeite_' + index + '" value="N">';
                        htmlCheckBox += '           <div class="checker">';
                        htmlCheckBox += '               <span>';
                        htmlCheckBox += '                   <input type="checkbox" name="produtoEnfeite_' + index + '" id="produtoEnfeite_' + index + '" value="' + dado.id + '">';
                        htmlCheckBox += '               </span>';
                        htmlCheckBox += '           </div>';
                        htmlCheckBox += '       <label class="control-label" for="produtoEnfeite_' + index + '">' + dado.nome + '</label>';
                        htmlCheckBox += '       </div>';
                        htmlCheckBox += '   </div>';

                        //QUANTIDADE
                        var htmlQuantidade = '<div class="col-md-1">';
                        htmlQuantidade += '<div class="form-group">';
                        htmlQuantidade += '<input type="number" name="quantidadeEnfeite_' + index + '" id="quantidadeEnfeite_' + index + '" class="form-control" min="1" max="' + dado.quantidadeDisponivel + '" step="1" value=""></div>';
                        htmlQuantidade += '</div>';
                        //UNIDADE
                        var htmlUnidade = '<div class="col-md-1" style="padding-top: 5px;">';
                        htmlUnidade += '<span id="unidadeEnfeite_' + index + '">' + dado.medida + '</span></div>';
                        //Valor Conjunto
                        var htmlValor = '<div class="col-md-2">';
                        htmlValor += '<div class="form-group">';
                        htmlValor += '<input type="text" name="valorConjuntoEnfeite_' + index + '" id="valorConjuntoEnfeite_' + index + '" class="form-control money" value="" disabled=""></div></div>';
                        //Disponivel
                        var htmlDisponivel = '<div class="col-md-1" style="padding-top: 5px;">';
                        htmlDisponivel += '<span id="disponivelEnfeite_' + index + '">' + dado.quantidadeDisponivel + '</span></div>';

                        var stHtml = htmlCheckBox + htmlQuantidade + htmlUnidade + htmlValor + htmlDisponivel + '</div></div>';
                        jQuery("#produtosEnfeiteField").append(stHtml);

                        var fatorRobinHood = +dado.valor*(+dado.fator/100);
                        valorProdutoEnfeite[index] = +dado.valor+(+fatorRobinHood);
                        quantidadeEnfeite[index] = dado.quantidadeDisponivel;

                        //retira valor anterior
                        valorTotal = (+valorTotal-(convertMaskToFloat(jQuery("#valorConjuntoEnfeite_" + index + "").val())));

                        //MANIPULACAO DOS CAMPOS
                        jQuery("#quantidadeEnfeite_" + index + "").on('change', function (){
                            if (jQuery(this).val() == 0 || jQuery(this).val() == '') {
                                valorTotal = (+valorTotal-(convertMaskToFloat(jQuery("#valorConjuntoEnfeite_" + index + "").val())));
                                jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
                                jQuery("#valorConjuntoEnfeite_" + index + "").val('');
                                jQuery("#disponivelEnfeite_" + index + "").html(quantidadeEnfeite[index]);
                                jQuery("#produtoEnfeite_" + index + "").parent().removeClass('checked');
                            } else {
                                jQuery("#produtoEnfeite_" + index + "").parent().addClass('checked');
                                var novoValor = valorProdutoEnfeite[index] * jQuery("#quantidadeEnfeite_" + index + "").val();
                                var valorAnterior = convertMaskToFloat(jQuery("#valorConjuntoEnfeite_" + index + "").val());
                                valorTotal = (+valorTotal-(+valorAnterior))+(+novoValor);
                                jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
                                jQuery("#valorConjuntoEnfeite_" + index + "").val(jQuery.number(novoValor, 2, ',', '.'));
                                jQuery("#disponivelEnfeite_" + index + "").html((quantidadeEnfeite[index] - jQuery("#quantidadeEnfeite_" + index + "").val()));
                            }
                        });

                        jQuery("#produtoEnfeite_" + index + "").on('click', function (){
                            if (jQuery(this).parent().hasClass('checked')) {
                                jQuery(this).parent().removeClass('checked');
                                valorTotal = (+valorTotal-(convertMaskToFloat(jQuery("#valorConjuntoEnfeite_" + index + "").val())));
                                jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
                                jQuery("#quantidadeEnfeite_" + index + "").val('');
                                jQuery("#valorConjuntoEnfeite_" + index + "").val('');
                            } else {
                                jQuery("#quantidadeEnfeite_" + index + "").val(1);
                                jQuery("#valorConjuntoEnfeite_" + index + "").val(jQuery.number(valorProdutoEnfeite[index],2,',','.'));
                                valorTotal = +valorTotal+(+valorProdutoEnfeite[index]);
                                jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
                                jQuery(this).parent().addClass('checked');
                            }
                        });

                    });//END FOREACH
                }
            }, 'json');// END AJAX
        } else {
            //TODO atualizacao do valor total validando para zerar todos os campos de valores

            jQuery("#produtosEnfeiteField").html('&nbsp;');
        }
    });

    /*
     MANIPULACAO DOS CAMPOS DA FIELD VEU
     */
    jQuery("#veu").select2({
        placeholder: 'Selecione um Véu', allowClear: true
    });
    jQuery("#veu").on('change', function (){
        if (jQuery("#veu").val() != '') {
            bloqueiaTela();
            jQuery.post(url + '/agencia/produtoServico/buscadadosproduto', {produto: jQuery("#veu").val()}, function (dados){
                liberaTela();
                if (dados) {
                    //retira valor anterior
                    valorTotal = (+valorTotal-(convertMaskToFloat(jQuery("#valorConjuntoVeu").val())));
                    var valorConjunto = 0;
                    var fatorRobinHood = dados[0].valor*(dados[0].fator/100);
                    if (jQuery("#quantidadeVeu").val() == '') {
                        jQuery("#quantidadeVeu").removeProp('readOnly').val(1);
                    }
                    valorConjunto = jQuery("#quantidadeVeu").val() * (+dados[0].valor+fatorRobinHood);
                    valorVeu = +dados[0].valor + (+fatorRobinHood);
                    quantidadeVeu = dados[0].quantidadeDisponivel;
                    valorTotal = (+valorTotal+(+valorVeu));
                    jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
                    jQuery("#unidadeMedidaVeu").html(dados[0].txt_medida);
                    jQuery("#valorConjuntoVeu").val(jQuery.number(valorConjunto, 2, ',', '.'));
                    jQuery("#disponivelVeu").html(dados[0].quantidadeDisponivel);
                    jQuery("#quantidadeVeu").attr("max", dados[0].quantidadeDisponivel);
                }
            }, 'json');
        } else {
            valorTotal = (+valorTotal-(convertMaskToFloat(jQuery("#valorConjuntoVeu").val())));
            jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
            jQuery("#valorConjuntoVeu").val('');
            jQuery("#disponivelVeu").html('&nbsp;');
            jQuery("#quantidadeVeu").val('');
            jQuery("#unidadeMedidaVeu").html('&nbsp;');
        }
    });


    jQuery("#quantidadeVeu").on('change', function (){
        if (jQuery(this).val() == 0) {
            jQuery(this).val(1);
        }
        var novoValor = valorVeu * jQuery("#quantidadeVeu").val();
        var valorAnterior = convertMaskToFloat(jQuery("#valorConjuntoVeu").val());
        valorTotal = ((+valorTotal-(+valorAnterior))+novoValor);
        jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
        jQuery("#valorConjuntoVeu").val(jQuery.number(novoValor, 2, ',', '.'));
        jQuery("#disponivelVeu").html((quantidadeRevestimento - jQuery("#quantidadeVeu").val()));
    });

    /*
     MANIPULACAO DOS CAMPOS DA FIELD ILUMINACAO
     */
    jQuery("#iluminacao").select2({
        placeholder: 'Selecione um Véu', allowClear: true
    });
    jQuery("#iluminacao").on('change', function (){
        if (jQuery("#iluminacao").val() != '') {
            bloqueiaTela();
            jQuery.post(url + '/agencia/produtoServico/buscadadosproduto', {produto: jQuery("#iluminacao").val()}, function (dados){
                liberaTela();
                if (dados) {
                    //retira valor anterior
                    valorTotal = (+valorTotal-(convertMaskToFloat(jQuery("#valorConjuntoIluminacao").val())));
                    var valorConjunto = 0;
                    var fatorRobinHood = dados[0].valor*(dados[0].fator/100);
                    if (jQuery("#quantidadeIluminacao").val() == '') {
                        jQuery("#quantidadeIluminacao").removeProp('readOnly').val(1);
                    }
                    valorConjunto = jQuery("#quantidadeIluminacao").val() * (+dados[0].valor+fatorRobinHood);
                    valorIluminacao = +dados[0].valor+fatorRobinHood;
                    quantidadeIluminacao = dados[0].quantidadeDisponivel;
                    valorTotal = (+valorTotal+valorIluminacao);
                    jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
                    jQuery("#unidadeMedidaIluminacao").html(dados[0].txt_medida);
                    jQuery("#valorConjuntoIluminacao").val(jQuery.number(valorConjunto, 2, ',', '.'));
                    jQuery("#disponivelIluminacao").html(dados[0].quantidadeDisponivel);
                    jQuery("#quantidadeIluminacao").attr("max", dados[0].quantidadeDisponivel);
                }
            }, 'json');
        } else {
            valorTotal = (+valorTotal-(convertMaskToFloat(jQuery("#valorConjuntoIluminacao").val())));
            jQuery("#valorConjuntoIluminacao").val('');
            jQuery("#disponivelIluminacao").html('&nbsp;');
            jQuery("#quantidadeIluminacao").val('');
            jQuery("#unidadeMedidaIluminacao").html('&nbsp;');
            jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
        }
    });

    jQuery("#quantidadeIluminacao").on('change', function (){
        if (jQuery(this).val() == 0) {
            jQuery(this).val(1);
        }
        var novoValor = valorIluminacao * jQuery("#quantidadeIluminacao").val();
        var valorAnterior = convertMaskToFloat(jQuery("#valorConjuntoIluminacao").val());
        valorTotal = ((+valorTotal-(+valorAnterior))+novoValor);
        jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
        jQuery("#valorConjuntoIluminacao").val(jQuery.number(novoValor, 2, ',', '.'));
        jQuery("#disponivelIluminacao").html((quantidadeRevestimento - jQuery("#quantidadeIluminacao").val()));
    });


    /*
     MANIPULACAO DOS CAMPOS DA FIELD SERVIÇOS - TIPO SEPULTAMENTO
     */
    jQuery("#tipoSepultamento").select2({
        placeholder: 'Selecione um Tipo de Sepultamento', allowClear: true
    });

    jQuery("#tipoSepultamento").on('change', function (){
        if (jQuery("#tipoSepultamento").val() != '') {
            bloqueiaTela();
            jQuery.post(url + '/agencia/produtoServico/buscadadosservico', {servico: jQuery("#tipoSepultamento").val()}, function (dados){
                liberaTela();
                if (dados) {
                    //retira valor anterior
                    valorTotal = (+valorTotal-(convertMaskToFloat(jQuery("#valorConjuntoSepultamento").val())));
                    var fatorRobinHood = +dados[0].valor*(dados[0].fator/100);
                    jQuery("#quantidadeSepultamento").html(1);
                    valorSepultamento = (+dados[0].valor)+(+fatorRobinHood);
                    valorTotal = (+valorTotal+(+valorSepultamento));
                    jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
                    jQuery("#valorConjuntoSepultamento").val(jQuery.number(valorSepultamento, 2, ',', '.'));
                }
            }, 'json');
        } else {
            valorTotal = (+valorTotal-(convertMaskToFloat(jQuery("#valorConjuntoSepultamento").val())));
            jQuery("#quantidadeSepultamento").html('&nbsp;');
            jQuery("#valorConjuntoSepultamento").val('');
            jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
        }
    });

    /*
     MANIPULACAO DOS CAMPOS TRANSPORTE - CHECK BOX Carro CARRETO
     */
    jQuery("#transporteCarreto").on('change', function (){
        if (jQuery(this).parent().hasClass('checked')) {
            jQuery("#quantidadeTransporteCarreto").removeProp('readOnly');
            bloqueiaTela();
            jQuery.post(url + '/agencia/produtoServico/buscadadosservico', {servico: 5,categoria: 2}, function (dados){
                liberaTela();
                if (dados) {
                    var fatorRobinHood = dados[0].valor*(dados[0].fator/100);
                    jQuery("#quantidadeTransporteCarreto").val(1);
                    valorTransporteCarreto = +dados[0].valor+fatorRobinHood;
                    valorTotal = (+valorTotal+valorTransporteCarreto);
                    jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
                    jQuery("#valorConjuntoTransporteCarreto").val(jQuery.number(valorTransporteCarreto, 2, ',', '.'));
                }
            }, 'json');
        }else{
            valorTotal = (+valorTotal-(convertMaskToFloat(jQuery("#valorConjuntoTransporteCarreto").val())));
            jQuery("#quantidadeTransporteCarreto").val('').prop('disabled',true);
            jQuery("#valorConjuntoTransporteCarreto").val('');
            jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));

        }
    });

    jQuery("#quantidadeTransporteCarreto").on('change', function (){
        if (jQuery(this).val() == 0) {
            jQuery(this).val(1);
        }
        var novoValor = valorTransporteCarreto * jQuery("#quantidadeTransporteCarreto").val();
        var valorAnterior = convertMaskToFloat(jQuery("#valorConjuntoTransporteCarreto").val());
        valorTotal = ((+valorTotal-(+valorAnterior))+novoValor);
        jQuery("#valorConjuntoTransporteCarreto").val(jQuery.number(novoValor, 2, ',', '.'));
        jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
    });

    /*
     MANIPULACAO DOS CAMPOS TRANSPORTE - CHECK BOX Carro ENTERRO
     */
    jQuery("#transporteEnterro").on('change', function (){
        if (jQuery(this).parent().hasClass('checked')) {
            jQuery("#quantidadeTransporteEnterro").removeProp('readOnly');
            bloqueiaTela();
            jQuery.post(url + '/agencia/produtoServico/buscadadosservico', {servico: 6,categoria: 2}, function (dados){
                liberaTela();
                if (dados) {
                    var fatorRobinHood = dados[0].valor*(dados[0].fator/100);
                    jQuery("#quantidadeTransporteEnterro").val(1);
                    valorTransporteEnterro = +dados[0].valor+fatorRobinHood;
                    valorTotal = (+valorTotal+valorTransporteEnterro);
                    jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
                    jQuery("#valorConjuntoTransporteEnterro").val(jQuery.number(valorTransporteEnterro, 2, ',', '.'));
                }
            }, 'json');
        }else{
            valorTotal = (+valorTotal-(convertMaskToFloat(jQuery("#valorConjuntoTransporteEnterro").val())));
            jQuery("#quantidadeTransporteEnterro").val('').prop('readOnly',true);
            jQuery("#valorConjuntoTransporteEnterro").val('');
            jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
        }
    });
    jQuery("#quantidadeTransporteEnterro").on('change', function (){
        if (jQuery(this).val() == 0) {
            jQuery(this).val(1);
        }
        var novoValor = valorTransporteEnterro * jQuery("#quantidadeTransporteEnterro").val();
        var valorAnterior = convertMaskToFloat(jQuery("#valorConjuntoTransporteEnterro").val());
        valorTotal = ((+valorTotal-(+valorAnterior))+novoValor);
        jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
        jQuery("#valorConjuntoTransporteEnterro").val(jQuery.number(novoValor, 2, ',', '.'));
    });

    /*
     MANIPULACAO DOS CAMPOS TRANSPORTE - CHECK BOX Carro REMOÇÃO
     */
    jQuery("#transporteRemocao").on('change', function (){
        if (jQuery(this).parent().hasClass('checked')) {
            jQuery("#quantidadeTransporteRemocao").removeProp('readOnly');
            bloqueiaTela();
            jQuery.post(url + '/agencia/produtoServico/buscadadosservico', {servico: 7,categoria: 2}, function (dados){
                liberaTela();
                if (dados) {
                    var fatorRobinHood = dados[0].valor*(dados[0].fator/100);
                    jQuery("#quantidadeTransporteRemocao").val(1);
                    valorTransporteRemocao = +dados[0].valor+fatorRobinHood;
                    valorTotal = (+valorTotal+valorTransporteRemocao);
                    jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
                    jQuery("#valorConjuntoTransporteRemocao").val(jQuery.number(valorTransporteRemocao, 2, ',', '.'));
                }
            }, 'json');
        }else{
            valorTotal = (+valorTotal-(convertMaskToFloat(jQuery("#valorConjuntoTransporteRemocao").val())));
            jQuery("#quantidadeTransporteRemocao").val('').prop('readOnly',true);
            jQuery("#valorConjuntoTransporteRemocao").val('');
            jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
        }
    });
    jQuery("#quantidadeTransporteRemocao").on('change', function (){
        if (jQuery(this).val() == 0) {
            jQuery(this).val(1);
        }
        var novoValor = valorTransporteRemocao * jQuery("#quantidadeTransporteRemocao").val();
        var valorAnterior = convertMaskToFloat(jQuery("#valorConjuntoTransporteRemocao").val());
        valorTotal = ((+valorTotal-(+valorAnterior))+novoValor);
        jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
        jQuery("#valorConjuntoTransporteRemocao").val(jQuery.number(novoValor, 2, ',', '.'));
    });

    /*
     MANIPULACAO DOS CAMPOS TRANSPORTE - CHECK BOX Carro CREMAÇÃO
     */
    jQuery("#transporteCremacao").on('change', function (){
        if (jQuery(this).parent().hasClass('checked')) {
            jQuery("#quantidadeTransporteCremacao").removeProp('readOnly');
            bloqueiaTela();
            jQuery.post(url + '/agencia/produtoServico/buscadadosservico', {servico: 8,categoria: 2}, function (dados){
                liberaTela();
                if (dados) {
                    var fatorRobinHood = dados[0].valor*(dados[0].fator/100);
                    jQuery("#quantidadeTransporteCremacao").val(1);
                    valorTransporteCremacao = +dados[0].valor+fatorRobinHood;
                    valorTotal = (+valorTotal+valorTransporteCremacao);
                    jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
                    jQuery("#valorConjuntoTransporteCremacao").val(jQuery.number(valorTransporteCremacao, 2, ',', '.'));
                }
            }, 'json');
        }else{
            valorTotal = (+valorTotal-(convertMaskToFloat(jQuery("#valorConjuntoTransporteCremacao").val())));
            jQuery("#quantidadeTransporteCremacao").val('').prop('readOnly',true);
            jQuery("#valorConjuntoTransporteCremacao").val('');
            jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
        }
    });
    jQuery("#quantidadeTransporteCremacao").on('change', function (){
        if (jQuery(this).val() == 0) {
            jQuery(this).val(1);
        }
        var novoValor = valorTransporteCremacao * jQuery("#quantidadeTransporteCremacao").val();
        var valorAnterior = convertMaskToFloat(jQuery("#valorConjuntoTransporteCremacao").val());
        valorTotal = ((+valorTotal-(+valorAnterior))+novoValor);
        jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
        jQuery("#valorConjuntoTransporteCremacao").val(jQuery.number(novoValor, 2, ',', '.'));
    });

    /*
     MANIPULACAO DOS CAMPOS TRANSPORTE - CHECK BOX Carro VIAGEM
     */
    jQuery("#transporteViagem").on('change', function (){
        if (jQuery(this).parent().hasClass('checked')) {
            jQuery("#quantidadeTransporteViagem").removeProp('readOnly');
            bloqueiaTela();
            jQuery.post(url + '/agencia/produtoServico/buscadadosservico', {servico: 9,categoria: 2}, function (dados){
                liberaTela();
                if (dados) {
                    var fatorRobinHood = dados[0].valor*(dados[0].fator/100);
                    jQuery("#quantidadeTransporteViagem").val(1);
                    valorTransporteViagem = +dados[0].valor+fatorRobinHood;
                    valorTotal = (+valorTotal+valorTransporteViagem);
                    jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
                    jQuery("#valorConjuntoTransporteViagem").val(jQuery.number(valorTransporteViagem, 2, ',', '.'));
                }
            }, 'json');
        }else{
            valorTotal = (+valorTotal-(convertMaskToFloat(jQuery("#valorConjuntoTransporteViagem").val())));
            jQuery("#quantidadeTransporteViagem").val('').prop('readOnly',true);
            jQuery("#valorConjuntoTransporteViagem").val('');
            jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
        }
    });
    jQuery("#quantidadeTransporteViagem").on('change', function (){
        if (jQuery(this).val() == 0) {
            jQuery(this).val(1);
        }
        var novoValor = valorTransporteViagem * jQuery("#quantidadeTransporteViagem").val();
        var valorAnterior = convertMaskToFloat(jQuery("#valorConjuntoTransporteViagem").val());
        valorTotal = ((+valorTotal-(+valorAnterior))+novoValor);
        jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
        jQuery("#valorConjuntoTransporteViagem").val(jQuery.number(novoValor, 2, ',', '.'));
    });

    /*
     MANIPULACAO DOS CAMPOS TAXA - CHECK BOX Sepultamento
     */
    jQuery("#taxaSepultamento").on('change', function (){
        if (jQuery(this).parent().hasClass('checked')) {
            bloqueiaTela();
            jQuery.post(url + '/agencia/produtoServico/buscadadosservico', {servico: 10,categoria: 3}, function (dados){
                liberaTela();
                if (dados) {
                    var fatorRobinHood = dados[0].valor*(dados[0].fator/100);
                    valorSepultamento = +dados[0].valor+fatorRobinHood;
                    valorTotal = (+valorTotal+valorSepultamento);
                    jQuery("#valorTotal").val(jQuery.number(valorTotal, 2, ',', '.'));
                }
            }, 'json');
        }else{
            valorTotal = (+valorTotal-(+valorSepultamento));
            jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
        }
    });

    /*
     MANIPULACAO DOS CAMPOS TAXA - CHECK BOX Essa/Paramento
     */
    jQuery("#taxaEssaParamento").on('change', function (){
        if (jQuery(this).parent().hasClass('checked')) {
            bloqueiaTela();
            jQuery.post(url + '/agencia/produtoServico/buscadadosservico', {servico: 11,categoria: 3}, function (dados){
                liberaTela();
                if (dados) {
                    var fatorRobinHood = dados[0].valor*(dados[0].fator/100);
                    valorEssaParamento = +dados[0].valor+fatorRobinHood;
                    valorTotal = (+valorTotal+valorEssaParamento);
                    jQuery("#valorTotal").val(jQuery.number(valorTotal, 2, ',', '.'));
                }
            }, 'json');
        }else{
            valorTotal = (+valorTotal-(+valorEssaParamento));
            jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
        }
    });

    /*
     MANIPULACAO DOS CAMPOS TAXA - CHECK BOX Velorio
     */
    jQuery("#taxaVelorio").on('change', function (){
        if (jQuery(this).parent().hasClass('checked')) {
            bloqueiaTela();
            jQuery.post(url + '/agencia/produtoServico/buscadadosservico', {servico: 12,categoria: 3}, function (dados){
                liberaTela();
                if (dados) {
                    var fatorRobinHood = dados[0].valor*(dados[0].fator/100);
                    valorVelorio = +dados[0].valor+fatorRobinHood;
                    valorTotal = (+valorTotal+valorVelorio);
                    jQuery("#valorTotal").val(jQuery.number(valorTotal, 2, ',', '.'));
                }
            }, 'json');
        }else{
            valorTotal = (+valorTotal-(+valorVelorio));
            jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
        }
    });

    /*
     MANIPULACAO DOS CAMPOS TAXA - CHECK BOX Cremacao
     */
    jQuery("#taxaCremacao").on('change', function (){
        if (jQuery(this).parent().hasClass('checked')) {
            bloqueiaTela();
            jQuery.post(url + '/agencia/produtoServico/buscadadosservico', {servico: 13,categoria: 3}, function (dados){
                liberaTela();
                if (dados) {
                    var fatorRobinHood = dados[0].valor*(dados[0].fator/100);
                    valorCremacao = +dados[0].valor+fatorRobinHood;
                    valorTotal = (+valorTotal+valorCremacao);
                    jQuery("#valorTotal").val(jQuery.number(valorTotal, 2, ',', '.'));
                }
            }, 'json');
        }else{
            valorTotal = (+valorTotal-(+valorCremacao));
            jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
        }
    });

    /*
     MANIPULACAO DOS CAMPOS TAXA - CHECK BOX Mesa Condelencia
     */
    jQuery("#taxaCondolencia").on('change', function (){
        if (jQuery(this).parent().hasClass('checked')) {
            bloqueiaTela();
            jQuery.post(url + '/agencia/produtoServico/buscadadosservico', {servico: 14,categoria: 3}, function (dados){
                liberaTela();
                if (dados) {
                    var fatorRobinHood = dados[0].valor*(dados[0].fator/100);
                    valorCondolencia = +dados[0].valor+fatorRobinHood;
                    valorTotal = (+valorTotal+valorCondolencia);
                    jQuery("#valorTotal").val(jQuery.number(valorTotal, 2, ',', '.'));
                }
            }, 'json');
        }else{
            valorTotal = (+valorTotal-(+valorCondolencia));
            jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
        }
    });

    /*
     MANIPULACAO DOS CAMPOS TAXA - CHECK BOX Mesa Viagem
     */
    jQuery("#taxaViagem").on('change', function (){
        if (jQuery(this).parent().hasClass('checked')) {
            bloqueiaTela();
            jQuery.post(url + '/agencia/produtoServico/buscadadosservico', {servico: 15,categoria: 3}, function (dados){
                liberaTela();
                if (dados) {
                    var fatorRobinHood = dados[0].valor*(dados[0].fator/100);
                    valorViagem = +dados[0].valor+fatorRobinHood;
                    valorTotal = (+valorTotal+valorViagem);
                    jQuery("#valorTotal").val(jQuery.number(valorTotal, 2, ',', '.'));
                }
            }, 'json');
        }else{
            valorTotal = (+valorTotal-(+valorViagem));
            jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
        }
    });

    /*
     MANIPULACAO DOS CAMPOS TAXA - CHECK BOX Camara Fria
     */
    jQuery("#taxaCamaraFria").on('change', function (){
        if (jQuery(this).parent().hasClass('checked')) {
            jQuery("#quantidadeTaxaCamaraFria").removeProp('readOnly');
            bloqueiaTela();
            jQuery.post(url + '/agencia/produtoServico/buscadadosservico', {servico: 16,categoria: 2}, function (dados){
                liberaTela();
                if (dados) {
                    var fatorRobinHood = dados[0].valor*(dados[0].fator/100);
                    jQuery("#quantidadeTaxaCamaraFria").val(1);
                    valorCamaraFria = +dados[0].valor+fatorRobinHood;
                    valorTotal = +valorTotal+valorCamaraFria;
                    jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
                    jQuery("#valorConjuntoTaxaCamaraFria").val(jQuery.number(valorCamaraFria, 2, ',', '.'));
                }
            }, 'json');
        }else{
            valorTotal = (+valorTotal-(convertMaskToFloat(jQuery("#valorConjuntoTaxaCamaraFria").val())));
            jQuery("#quantidadeTaxaCamaraFria").val('');
            jQuery("#valorConjuntoTaxaCamaraFria").val('');
            jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
        }
    });
    jQuery("#quantidadeTaxaCamaraFria").on('change', function (){
        if (jQuery(this).val() == 0) {
            jQuery(this).val(1);
        }
        var novoValor = valorCamaraFria * jQuery("#quantidadeTaxaCamaraFria").val();
        var valorAnterior = convertMaskToFloat(jQuery("#valorConjuntoTaxaCamaraFria").val());
        valorTotal = ((+valorTotal-(+valorAnterior))+novoValor);
        jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
        jQuery("#valorConjuntoTaxaCamaraFria").val(jQuery.number(novoValor, 2, ',', '.'));
    });

    /*
     MANIPULACAO DOS CAMPOS TAXA - CHECK BOX Outros
     */
    jQuery("#taxaOutros").on('change', function (){
        if (jQuery(this).parent().hasClass('checked')) {
            jQuery("#creditarDebitarField").show();
        }else{
            if ( jQuery("input[value='creditar']").parent().hasClass('checked') ){
                valorTotal = (+valorTotal-(+valorOutros));
                jQuery("input[value='creditar']").parent().removeClass('checked')
            }else if (jQuery("input[value='debitar']").parent().hasClass('checked')){
                valorTotal = (+valorTotal+(+valorOutros));
                jQuery("input[value='debitar']").parent().removeClass('checked')
            }
            jQuery("#valorConjuntoOutros").val('');
            valorOutros = 0;
            jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
            jQuery("#creditarDebitarField").hide();
        }
    });

    jQuery("#valorConjuntoOutros").on('change',function (){
        if (jQuery("#taxaOutros").parent().hasClass('checked')) {
            if ( jQuery("input[value='creditar']").parent().hasClass('checked') ){
                //retirando valor antigo para nao acumular
                valorTotal = (+valorTotal-(+valorOutros));
                valorOutros = convertMaskToFloat(jQuery(this).val());
                //Atribuindo novo valor
                valorTotal = (+valorTotal+(+valorOutros));
            }else if (jQuery("input[value='debitar']").parent().hasClass('checked')){
                //retirando valor antigo para nao acumular
                valorTotal = (+valorTotal+(+valorOutros));
                valorOutros = convertMaskToFloat(jQuery(this).val());
                //Atribuindo novo valor
                valorTotal = (+valorTotal-(+valorOutros));
            }
            jQuery("#valorTotal").val(jQuery.number(valorTotal,2,',','.'));
        }
    });


});//END OF JQUERY READY
