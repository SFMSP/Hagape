var table;
jQuery(document).ready(function () {
    table = jQuery('#contratos').dataTable({
        "processing": true,
        "serverSide": true,
        "language": {
            "url": url + "/assets/global/scripts/traducao.json"
        },
        "ajax": {
            "url": url + "/estoque/contrato/paginacao",
            "type": "POST",
            "data": function (d) {
                //Manipulação dos dados retornados, caso necessario
            }
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
            jQuery(nRow).attr('id', 'linhaTable_' + iDataIndex); // or whatever you choose to set as the id
        },
        "drawCallback": function (oSettings) {
            App.initUniform($('input[type="checkbox"]', table));

        },
        "columns": [
            {"orderable": false},
            {"orderable": true},
            {"orderable": true},
            {"orderable": true},
            {"orderable": true},
            {"orderable": true},
            {"orderable": true}
        ],
        "order": [],
        "sDom": '<"top"fl>rt<"bottom"ip><"clear">',
    });


    jQuery('.group-checkable', table).change(function () {
        var set = table.find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
        var checked = jQuery(this).prop("checked");
        jQuery(set).each(function () {
            jQuery(this).prop("checked", checked);
        });
        jQuery.uniform.update(set);
    });


});

function mudarStatus(status) {

    var ids = getCheckboxMarcados('contratos');

    if (ids == "") {
        return false;
    }

    jQuery.post(url + "/estoque/contrato/mudarStatus", {'ids': ids, 'status':status}, function (data) {
        var title;
        if (data.success === 1) {
            jQuery("#modalSuccess").modal();
        } else {
            jQuery("#modalError").modal().find(".modal-body").text("Apenas contratos não utilizados por pedidos, poderam ser desativados.");
        }
        table.fnDraw();
    }, 'json');

}

function excluir() {

    var ids = getCheckboxMarcados('contratos');

    if (ids == "") {
        return false;
    }

    jQuery.post(url + "/estoque/contrato/deletar", {'ids': ids}, function (data) {
        if (data.success === 1) {
            jQuery("#modalSuccess").modal();
        } else {
            jQuery("#modalError").modal().find(".modal-body").text("Apenas contratos não utilizados por pedidos, poderam ser excluidos.");
        }
        table.fnDraw();
    }, 'json');

}
