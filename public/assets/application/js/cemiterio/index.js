var table;

jQuery(document).ready(function () {
    table = jQuery('#cemiterios').dataTable({
        "processing": true,
        "serverSide": true,
        "language": {
            "url": url + "/assets/global/scripts/traducao.json"
        },
        "ajax": {
            "url": url + "/admin/cemiterio/paginacao",
            "type": "POST",
            "data": function (d) {
                d.status = jQuery('#status').val();
            }
        },
        "drawCallback": function (oSettings) {
            App.initUniform($('input[type="checkbox"]', table));

        },
        "columns": [
            {"orderable": false},
            {"orderable": true},
            {"orderable": true},
            {"orderable": true}

        ],
        "order": [],
        "sDom": '<"top"fl>rt<"bottom"ip><"clear">',
    });

    jQuery('#status').change(function () {
        table.fnDraw();
    });

    jQuery('.group-checkable', table).change(function () {
        var set = table.find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
        var checked = jQuery(this).prop("checked");
        jQuery(set).each(function () {
            jQuery(this).prop("checked", checked);
        });
        jQuery.uniform.update(set);
    });


});

function excluir() {

    var ids = getCheckboxMarcados('cemiterios');

    if (ids == "") {
        return false;
    }

    jQuery.post(url + "/admin/cemiterio/deletar", {'ids': ids, 'status':status}, function (data) {
        var title;

        if (data.success === 1) {
            jQuery("#modalSuccess").modal();
        } else {
            jQuery("#modalError").modal();
        }
        table.fnDraw();
    }, 'json');


}

function mudarStatus(status) {

    var ids = getCheckboxMarcados('cemiterios');

    if (ids == "") {
        return false;
    }

    jQuery.post(url + "/admin/cemiterio/mudarStatus", {'ids': ids, 'status':status}, function (data) {
        var title;

        if (data.success === 1) {
            jQuery("#modalSuccess").modal();
        } else {
            jQuery("#modalError").modal();
        }
        

        table.fnDraw();
    }, 'json');


}