jQuery(document).ready(function () {

    if (jQuery('input[name="id"]').val() == "") {
        mudarEstado(5270);
    }

    jQuery('#estado').change(function () {
        mudarEstado(0);
    });

    disabilitarTipoSepultamento();

    if (jQuery('#tipo').val() == 2) {
        jQuery('input[name="tipoSepultamento[]"]').attr('disabled', true);
    } else {
        jQuery('input[name="tipoSepultamento[]"]').attr('disabled', false);
    }

    jQuery('#tipo').change(function () {
        disabilitarTipoSepultamento();
    });

});

function disabilitarTipoSepultamento() {
    if (jQuery('#tipo').val() == 2) {
        jQuery('input[name="tipoSepultamento[]"]').attr('disabled', true);
    } else {
        jQuery('input[name="tipoSepultamento[]"]').attr('disabled', false);
    }
}

function mudarEstado(cidade) {
    if (jQuery('#estado').val() !== "") {

        //Função padrão que realiza a busca das cidades de um estado
        jQuery.post(url + '/admin/cemiterio/getCidades', {estado: jQuery('#estado').val()}, function (dados) {
            jQuery('#cidade').html("");

            for (var i = 0; i < dados.length; i++) {
                if (cidade == dados[i]['id']) {
                    jQuery('#cidade').append("<option value=" + dados[i]['id'] + " selected>" + dados[i]['nome'] + "</option>");
                } else {
                    jQuery('#cidade').append("<option value=" + dados[i]['id'] + ">" + dados[i]['nome'] + "</option>");
                }
            }


        }, 'json');
    } else {
        jQuery('#cidade').html("");
        jQuery('#cidade').append("<option value=''>Escolha uma Cidade</option>");
    }
}