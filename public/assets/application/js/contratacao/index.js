var table;

jQuery(document).ready(function (){


//: center;
    jQuery("thead > tr > th").css('text-align', 'center');


    //table.table-bordered.dataTable tbody th, table.table-bordered.dataTable tbody td
    table = jQuery('#contratacoes').dataTable({

        "processing": true, "serverSide": true,

        "language": {
            "url": url + "/assets/global/scripts/traducao.json"
        }, "ajax": {
            "url": url + "/agencia/contratacao/paginacao", "type": "POST", "data": function (d){

                d.agencia = jQuery('#agencia').val(), d.filtroData = jQuery('#filtroData').val()

            }
        }, "drawCallback": function (oSettings){
            App.initUniform($('input[type="checkbox"]', table));


        }, "footerCallback": function (row, data, start, end, display){

            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function (i){

                return typeof i === 'string' ? i.replace(/[\R$,.]/g, '') * 1 :typeof i === 'number' ? i :0;
            };

            // Total over all pages

            total = api
                .column(8)
                .data()
                .reduce(function (a, b){

                    return intVal(a) + intVal(b);
                }, 0);
            function mascaraValor(valor){
                valor = valor.toString().replace(/\D/g, "");
                valor = valor.toString().replace(/(\d)(\d{8})$/, "$1.$2");
                valor = valor.toString().replace(/(\d)(\d{5})$/, "$1.$2");
                valor = valor.toString().replace(/(\d)(\d{2})$/, "$1,$2");
                return valor
            }

            jQuery(api.column(8).footer()).html('R$ ' + mascaraValor(total));
        },

        "columns": [{"orderable": false}, {"orderable": true}, {"orderable": true}, {"orderable": true}, {"orderable": true}, {"orderable": true}, {"orderable": true}, {"orderable": true}, {"orderable": false}

        ],


        "sDom": '<"top"fl>rt<"bottom"ip><"clear">', "order": []

    });
    jQuery('#agencia').change(function (){
        table.fnDraw();
    });
    jQuery('#filtroData').change(function (){
        table.fnDraw();
    });

    jQuery('.group-checkable', table).change(function (){
        var set = table.find('tbody > tr > td:nth-child(1) input[type="checkbox"]');
        var checked = jQuery(this).prop("checked");

        jQuery(set).each(function (){
            jQuery(this).prop("checked", checked);
        });

        jQuery.uniform.update(set);
        countSelectedRecords();
    });

});


function verConteudo(id, obj){

    $('.ver').fadeOut();
    var html = "";
    $.post(url + '/agencia/contratacao/notas', {nota: id}, function (dados){
        for (var i = 0; i < dados.length; i++) {
            html += "<tr class='ver'>";
            html += "<td></td><td>" + dados[i]['txt_numero_nota'] + "</td><td>" + dados[i]['nu_preco_medio_atual'] + "</td><td>" + dados[i]['data'] + "</td><td></td><td></td><td></td><td></td><td></td>";
            html += "</tr>";
        }
        $(html).insertAfter($(obj).closest('tr')).show();
    }, 'json');


}

function excluir(){
    var ids = getCheckboxMarcados('contratacoes');
    if (ids == "") {
        return false;
    }
    jQuery.post(url + "/agencia/contratacao/deletar", {'ids': ids, 'status': status}, function (data){
        var title;
        if (data.success === 1) {
            jQuery("#modalSuccess").modal();
        } else {
            jQuery("#modalError").modal().find(".modal-title").text("Aviso");
            jQuery("#modalError").modal().find(".modal-body").text("Não é possível excluír o registro!");
        }
        table.fnDraw();
    }, 'json');


}

