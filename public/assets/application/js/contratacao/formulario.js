jQuery(document).ready(function (){

    if (jQuery("#operacao").val() != '' && jQuery("#tipoContratacao").val() == '') {
        buscaTipoContratacao(jQuery("#operacao").val());
    }

});


function buscaTipoContratacao(id){
    bloqueiaTela();
    jQuery("#tipoContratacao").attr('disabled', true);
    jQuery.post(url + '/agencia/contratacao/buscaTipoContratacao', {tipoOperacao: id}, function (dados){
        if ( (id == 2) || (id == 3) ) {
            jQuery('#destino').val('');
            jQuery('#destino').attr('disabled', true);
            if (id == 3) {
                jQuery('#tipoContratacao').attr('disabled', true);
                jQuery('#tipoContratacao').val('');
            }
        } else {
            jQuery('#destino').removeAttr('disabled');
            jQuery('#tipoContratacao').removeAttr('disabled');
        }
        liberaTela();
        if (dados) {
            jQuery("#tipoContratacao").empty();
            jQuery("#tipoContratacao").append(new Option('Selecione', ''));
            for (var i = 0; i < dados.length; i++) {
                jQuery("#tipoContratacao").append(new Option(dados[i]['nome'], dados[i]['id']));
            }
            jQuery("#tipoContratacao").attr('disabled', false);
        }
    }, 'json');
}