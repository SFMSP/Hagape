var tableFilhos;
var tableCasamento;

function mudarEstado(cidade){
    if (jQuery('#estado').val() !== "") {
        jQuery('#cidade').attr('disabled', true);
        bloqueiaTela();
        //Função padrão que realiza a busca das cidades de um estado
        jQuery.post(url + '/admin/estoque/getCidades', {estado: jQuery('#estado').val()}, function (dados){
            liberaTela();
            jQuery('#cidade').html("");

            for (var i = 0; i < dados.length; i++) {
                if (cidade == dados[i]['id']) {
                    jQuery('#cidade').append("<option value=" + dados[i]['id'] + " selected>" + dados[i]['nome'] + "</option>");
                } else {
                    jQuery('#cidade').append("<option value=" + dados[i]['id'] + ">" + dados[i]['nome'] + "</option>");
                }
            }
            jQuery('#cidade').removeAttr('disabled');

        }, 'json');
    } else {
        jQuery('#cidade').html("");
        jQuery('#cidade').append("<option value=''>Escolha uma Cidade</option>");
    }
}

var TableDatatablesEditable = function (){

    var handleTable = function (){

        function restoreRow(oTable, nRow){
            var aData = oTable.fnGetData(nRow);
            var jqTds = jQuery('>td', nRow);

            for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                oTable.fnUpdate(aData[i], nRow, i, false);
            }

            oTable.fnDraw();
        }

        function editRow(oTable, nRow){
            var aData = oTable.fnGetData(nRow);
            var jqTds = jQuery('>td', nRow);
            jqTds[0].innerHTML = '<input type="text" class="form-control input" value="' + aData[0] + '">';
            jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
            jqTds[2].innerHTML = '<input type="text" class="form-control input" value="' + aData[2] + '">';
            jqTds[3].innerHTML = '<a class="edit btn btn-success" href="">Salvar</a>';
            jqTds[4].innerHTML = '<a class="btn btn-danger cancel" href="">Cancelar</a>';
        }

        function saveRow(oTable, nRow){
            var jqInputs = jQuery('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
            oTable.fnUpdate('<a class="edit btn btn-warning" href="">Editar</a>', nRow, 3, false);
            oTable.fnUpdate('<a class="delete btn btn-danger" href="">Deletar</a>', nRow, 4, false);
            //Update hidden values
            jQuery(jqInputs[3]).val(jqInputs[0].value);
            jQuery(jqInputs[4]).val(jqInputs[1].value);
            jQuery(jqInputs[5]).val(jqInputs[2].value);
            oTable.fnDraw();
        }

        function cancelEditRow(oTable, nRow){
            var jqInputs = jQuery('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
            oTable.fnUpdate('<a class="btn btn-warning edit" href="">Editar</a>', nRow, 3, false);
            //Update hidden values
            jQuery(jqInputs[3]).val(jqInputs[0].value);
            jQuery(jqInputs[4]).val(jqInputs[1].value);
            jQuery(jqInputs[5]).val(jqInputs[2].value);
            oTable.fnDraw();
        }

        var table = jQuery('#listaFilhos');

        var oTable = table.dataTable({
            "searching": false,
            "paging": false,
            "language": {
                "url": url + "/assets/global/scripts/traducao.json"
            },
            "fnCreatedRow": function (nRow, aData, iDataIndex){
                //Add hidden inputs when add new row
                if (aData[0] == '') {
                    var htmlHidden = '<input type="hidden" id="nomeFilhos[]" name="nomeFilhos[]" value="' + aData[0] + '" >';
                    htmlHidden += '<input type="hidden" id="maoiridadeFilhos[]" name="maoiridadeFilhos[]" value="' + aData[1] + '" >';
                    htmlHidden += '<input type="hidden" id="observacaoFilhos[]" name="observacaoFilhos[]" value="' + aData[2] + '" >';
                    jQuery(nRow).append(htmlHidden);
                }
                //jQuery(nRow).attr('id', 'linhaFilhos_' + (iDataIndex+1)); // or whatever you choose to set as the id
            },
            "columnDefs": [{className: "text-center", "targets": [0, 1, 2, 3, 4]}],
            "columns": [{"orderable": false}, {"orderable": false}, {"orderable": false}, {"orderable": false}, {"orderable": false}],
            "sDom": '<"top"fl>rt<"bottom"ip><"clear">',
            "order": []

        });

        var tableWrapper = jQuery("#sample_editable_1_wrapper");

        var nEditing = null;
        var nNew = false;

        jQuery('#sample_editable_1_new').click(function (e){
            e.preventDefault();

            if (nNew && nEditing) {
                if (confirm("Linha anterior não foi salva. Deseja salva-lá?")) {
                    saveRow(oTable, nEditing); // save
                    jQuery(nEditing).find("td:first").html("Untitled");
                    nEditing = null;
                    nNew = false;

                } else {
                    oTable.fnDeleteRow(nEditing); // cancel
                    nEditing = null;
                    nNew = false;

                    return;
                }
            }

            var aiNew = oTable.fnAddData(['', '', '', '', '']);
            var nRow = oTable.fnGetNodes(aiNew[0]);
            editRow(oTable, nRow);
            nEditing = nRow;
            nNew = true;
        });

        table.on('click', '.delete', function (e){
            e.preventDefault();

            if (confirm("Você tem certeza que deseja deletar esta linha?") == false) {
                return;
            }

            var nRow = jQuery(this).parents('tr')[0];
            oTable.fnDeleteRow(nRow);
            //alert("Deleted! Do not forget to do some ajax to sync with backend :)");
        });

        table.on('click', '.cancel', function (e){
            e.preventDefault();
            if (!nNew) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        table.on('click', '.edit', function (e){
            e.preventDefault();

            /* Get the row as a parent of the link that was clicked on */
            var nRow = jQuery(this).parents('tr')[0];

            if (nEditing !== null && nEditing != nRow) {
                /* Currently editing - but not this row - restore the old before continuing to edit mode */
                restoreRow(oTable, nEditing);
                editRow(oTable, nRow);
                nEditing = nRow;
            } else if (nEditing == nRow && this.innerHTML == "Salvar") {
                /* Editing this row and want to save it */
                saveRow(oTable, nEditing);
                nEditing = null;
                //alert("Updated! Do not forget to do some ajax to sync with backend :)");
            } else {
                /* No edit in progress - let's start one */
                editRow(oTable, nRow);
                nEditing = nRow;
            }
        });
    };

    return {

        //main function to initiate the module
        init: function (){
            handleTable();
        }

    };

}();

var TableDatatablesEditable2 = function (){

    var handleTable2 = function (){

        function restoreRow(oTable, nRow){
            var aData = oTable.fnGetData(nRow);
            var jqTds = jQuery('>td', nRow);

            for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                oTable.fnUpdate(aData[i], nRow, i, false);
            }

            oTable.fnDraw();
        }

        function editRow(oTable, nRow){
            var aData = oTable.fnGetData(nRow);
            var jqTds = jQuery('>td', nRow);
            jqTds[0].innerHTML = '<input type="text" class="text-left form-control input" value="' + aData[0] + '">';
            jqTds[1].innerHTML = '<input type="text" class="text-left form-control data"  value="' + aData[1] + '">';
            jqTds[2].innerHTML = '<input type="text" class="text-left form-control input" value="' + aData[2] + '">';
            jqTds[3].innerHTML = '<input type="text" class="text-left form-control input" value="' + aData[3] + '">';
            jqTds[4].innerHTML = '<input type="text" class="text-left form-control input" value="' + aData[4] + '">';
            jqTds[5].innerHTML = '<input type="text" class="text-left form-control input" value="' + aData[5] + '">';
            jqTds[6].innerHTML = '<input type="text" class="text-left form-control input" value="' + aData[6] + '">';
            jqTds[7].innerHTML = '<input type="text" class="text-left form-control input" value="' + aData[7] + '">';
            jqTds[8].innerHTML = '<a class="edit btn btn-success" href="">Salvar</a>';
            jqTds[9].innerHTML = '<a class="btn btn-danger cancel" href="">Cancelar</a>';
        }

        function saveRow(oTable, nRow){
            var jqInputs = jQuery('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
            oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
            oTable.fnUpdate(jqInputs[4].value, nRow, 4, false);
            oTable.fnUpdate(jqInputs[5].value, nRow, 5, false);
            oTable.fnUpdate(jqInputs[6].value, nRow, 6, false);
            oTable.fnUpdate(jqInputs[7].value, nRow, 7, false);
            oTable.fnUpdate('<a class="edit btn btn-warning" href="">Editar</a>', nRow, 8, false);
            oTable.fnUpdate('<a class="delete btn btn-danger" href="">Deletar</a>', nRow, 9, false);
            //Update hidden values
            console.log(jqInputs);
            jQuery(jqInputs[8]).val(jqInputs[0].value);
            jQuery(jqInputs[9]).val(jqInputs[1].value);
            jQuery(jqInputs[10]).val(jqInputs[2].value);
            jQuery(jqInputs[11]).val(jqInputs[3].value);
            jQuery(jqInputs[12]).val(jqInputs[4].value);
            jQuery(jqInputs[13]).val(jqInputs[5].value);
            jQuery(jqInputs[14]).val(jqInputs[6].value);
            jQuery(jqInputs[15]).val(jqInputs[7].value);
            oTable.fnDraw();
        }

        function cancelEditRow(oTable, nRow){
            var jqInputs = jQuery('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
            oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
            oTable.fnUpdate(jqInputs[4].value, nRow, 4, false);
            oTable.fnUpdate(jqInputs[5].value, nRow, 5, false);
            oTable.fnUpdate(jqInputs[6].value, nRow, 6, false);
            oTable.fnUpdate(jqInputs[7].value, nRow, 7, false);
            oTable.fnUpdate('<a class="btn btn-warning edit" href="">Editar</a>', nRow, 8, false);
            //Update hidden values
            jQuery(jqInputs[8]).val(jqInputs[0].value);
            jQuery(jqInputs[9]).val(jqInputs[1].value);
            jQuery(jqInputs[10]).val(jqInputs[2].value);
            jQuery(jqInputs[11]).val(jqInputs[3].value);
            jQuery(jqInputs[12]).val(jqInputs[4].value);
            jQuery(jqInputs[13]).val(jqInputs[5].value);
            jQuery(jqInputs[14]).val(jqInputs[6].value);
            jQuery(jqInputs[15]).val(jqInputs[7].value);
            oTable.fnDraw();
        }

        var table2 = jQuery('#listaCasamentos');

        var oTable = table2.dataTable({
            "searching": false,
            "paging": false,
            "language": {
                "url": url + "/assets/global/scripts/traducao.json"
            },
            "fnCreatedRow": function (nRow, aData, iDataIndex){
                //Add hidden inputs when add new row
                if (aData[0] == '') {
                    var htmlHidden = '<input type="hidden" id="nomeConjuges[]" name="nomeConjuges[]" value="' + aData[0] + '" >';
                    htmlHidden += '<input type="hidden" id="dataCasamentos[]" name="dataCasamentos[]" value="' + aData[1] + '" >';
                    htmlHidden += '<input type="hidden" id="cartorioCasamentos[]" name="cartorioCasamentos[]" value="' + aData[2] + '" >';
                    htmlHidden += '<input type="hidden" id="cidadeCasamentos[]" name="cidadeCasamentos[]" value="' + aData[3] + '" >';
                    htmlHidden += '<input type="hidden" id="ufCasamentos[]" name="ufCasamentos[]" value="' + aData[4] + '" >';
                    htmlHidden += '<input type="hidden" id="livroCasamentos[]" name="livroCasamentos[]" value="' + aData[5] + '" >';
                    htmlHidden += '<input type="hidden" id="folhaCasamentos[]" name="folhaCasamentos[]" value="' + aData[6] + '" >';
                    htmlHidden += '<input type="hidden" id="numeroLivroCasamentos[]" name="numeroLivroCasamentos[]" value="' + aData[7] + '" >';
                    jQuery(nRow).append(htmlHidden);
                }
                //jQuery(nRow).attr('id', 'linhaFilhos_' + (iDataIndex+1)); // or whatever you choose to set as the id
            },
            "columnDefs": [{className: "text-center", "targets": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]}],
            "columns": [{"orderable": false}, {"orderable": false}, {"orderable": false}, {"orderable": false}, {"orderable": false}, {"orderable": false}, {"orderable": false}, {"orderable": false}, {"orderable": false}, {"orderable": false}],
            "sDom": '<"top"fl>rt<"bottom"ip><"clear">',
            "order": []

        });

        var tableWrapper2 = jQuery("#sample_editable_2_wrapper");

        var nEditing = null;
        var nNew = false;

        jQuery('#sample_editable_2_new').click(function (e){
            e.preventDefault();

            if (nNew && nEditing) {
                if (confirm("Linha anterior não foi salva. Deseja salva-lá?")) {
                    saveRow(oTable, nEditing); // save
                    jQuery(nEditing).find("td:first").html("Untitled");
                    nEditing = null;
                    nNew = false;

                } else {
                    oTable.fnDeleteRow(nEditing); // cancel
                    nEditing = null;
                    nNew = false;

                    return;
                }
            }

            var aiNew = oTable.fnAddData(['', '', '', '', '', '', '', '', '', '', '']);
            var nRow = oTable.fnGetNodes(aiNew[0]);
            editRow(oTable, nRow);
            nEditing = nRow;
            nNew = true;
        });

        table2.on('click', '.delete', function (e){
            e.preventDefault();

            if (confirm("Você tem certeza que deseja deletar esta linha?") == false) {
                return;
            }

            var nRow = jQuery(this).parents('tr')[0];
            oTable.fnDeleteRow(nRow);
            //alert("Deleted! Do not forget to do some ajax to sync with backend :)");
        });

        table2.on('click', '.cancel', function (e){
            e.preventDefault();
            var dada = oTable.fnGetData(nEditing);
            if (!nNew || dada[0] == '') {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        table2.on('click', '.edit', function (e){
            e.preventDefault();

            /* Get the row as a parent of the link that was clicked on */
            var nRow = jQuery(this).parents('tr')[0];

            if (nEditing !== null && nEditing != nRow) {
                /* Currently editing - but not this row - restore the old before continuing to edit mode */
                restoreRow(oTable, nEditing);
                editRow(oTable, nRow);
                nEditing = nRow;
            } else if (nEditing == nRow && this.innerHTML == "Salvar") {
                /* Editing this row and want to save it */
                saveRow(oTable, nEditing);
                nEditing = null;
                //alert("Updated! Do not forget to do some ajax to sync with backend :)");
            } else {
                /* No edit in progress - let's start one */
                editRow(oTable, nRow);
                nEditing = nRow;
            }
        });
    };

    return {

        //main function to initiate the module
        init: function (){
            handleTable2();
        }

    };

}();


jQuery(document).ready(function (){

    TableDatatablesEditable.init();
    TableDatatablesEditable2.init();

    jQuery("#certidaoNascimento").hide();
    jQuery("#casamentoField").hide();
    jQuery("#filhosField").hide();
    jQuery("#nascimentoObitoField").hide();

    jQuery("#nascimentoObito").on('change', function (){
        if (jQuery("#nascimentoObito").val() == 'S') {
            jQuery("#nascimentoObitoField").show();
        } else {
            jQuery("#nascimentoObitoField").hide();
        }
    });

    jQuery("#deixaFilhos").on('change', function (){
        if (jQuery("#deixaFilhos").val() == 1) {
            jQuery("#filhosField").show();
        } else {
            jQuery("#filhosField").hide();
        }
    });

    jQuery("#estadoCivil").on('change', function (){
        if (jQuery("#estadoCivil").val() == 2) {
            jQuery("#certidaoNascimento").hide();
            jQuery("#casamentoField").show();
        } else {
            jQuery("#certidaoNascimento").show();
            jQuery("#casamentoField").hide();
        }
    });

    jQuery("#sexo").on('change', function (){
        if (jQuery("#sexo").val() == 'F') {
            jQuery("#reservista").hide();
        } else {
            jQuery("#reservista").show();
        }
    });

    jQuery("#estadoCivilMae").on('change', function (){
        if (jQuery("#estadoCivilMae").val() == 3) {
            jQuery("#naturalidadeMae").val('');
            jQuery("#profissaoMae").val('');
            jQuery("#naturalidadeMae").attr('disabled', true);
            jQuery("#profissaoMae").attr('disabled', true);
        } else {
            jQuery("#naturalidadeMae").removeAttr('disabled');
            jQuery("#profissaoMae").removeAttr('disabled');
        }
    });

    jQuery("#estadoCivilPai").on('change', function (){
        if (jQuery("#estadoCivilPai").val() == 3) {
            jQuery("#naturalidadePai").val('');
            jQuery("#profissaoPai").val('');
            jQuery("#naturalidadePai").attr('disabled', true);
            jQuery("#profissaoPai").attr('disabled', true);
        } else {
            jQuery("#naturalidadePai").removeAttr('disabled');
            jQuery("#profissaoPai").removeAttr('disabled');
        }
    });

    //Checagem dos valores no caso de edicao
    if (jQuery("#estadoCivil").val() == 2) {
        jQuery("#certidaoNascimento").hide();
        jQuery("#casamentoField").show();
    } else {
        jQuery("#certidaoNascimento").show();
        jQuery("#casamentoField").hide();
    }
    if (jQuery("#estadoCivilMae").val() == 3) {
        jQuery("#naturalidadeMae").val('');
        jQuery("#profissaoMae").val('');
        jQuery("#naturalidadeMae").attr('disabled', true);
        jQuery("#profissaoMae").attr('disabled', true);
    } else {
        jQuery("#naturalidadeMae").removeAttr('disabled');
        jQuery("#profissaoMae").removeAttr('disabled');
    }
    if (jQuery("#estadoCivilPai").val() == 3) {
        jQuery("#naturalidadePai").val('');
        jQuery("#profissaoPai").val('');
        jQuery("#naturalidadePai").attr('disabled', true);
        jQuery("#profissaoPai").attr('disabled', true);
    } else {
        jQuery("#naturalidadePai").removeAttr('disabled');
        jQuery("#profissaoPai").removeAttr('disabled');
    }

    if (jQuery("#sexo").val() == 'F') {
        jQuery("#reservista").hide();
    } else {
        jQuery("#reservista").show();
    }
    if (jQuery("#deixaFilhos").val() == 1) {
        jQuery("#filhosField").show();
    } else {
        jQuery("#filhosField").hide();
    }

});

