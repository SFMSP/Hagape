


jQuery(document).ready(function () {
    
    jQuery('#cpf').on('change', function (){
        bloqueiaTela();
        if (jQuery('#cpf').val() !== "") {
            jQuery.post(url + '/agencia/declarante/getDadosPorCpfDeclarante', {cpf: jQuery('#cpf').val()}, function (dados){
                liberaTela();
                if (dados) {
                    jQuery('#nome').val(dados['declarante']['nome']);
                    jQuery('#grauParentesco').val(dados['declarante']['grauParentesco']);
                    jQuery('#telefoneResidencial').val(dados['declarante']['telefoneResidencial']);
                    jQuery('#telefoneCelular').val(dados['declarante']['telefoneCelular']);
                    jQuery('#email').val(dados['declarante']['email']);
                    jQuery('#rg').val(dados['declarante']['rg']);
                    jQuery('#profissao').val(dados['declarante']['profissao']);
                    jQuery('#nomeMae').val(dados['declarante']['nomeMae']);

                    jQuery('#endereco').val(dados['localizacao']['endereco']);
                    jQuery('#bairro').val(dados['localizacao']['bairro']);
                    jQuery('#numero').val(dados['localizacao']['numero']);
                    jQuery('#complemento').val(dados['localizacao']['complemento']);
                    jQuery('#cep').val(dados['localizacao']['cep']);
                    jQuery('#estado').val(dados['localizacao']['estado']);
                    mudarEstado(dados['localizacao']['cidade']);
                    jQuery('#empresa').val(dados['empresa']['nomeEmpresarial']);
                    jQuery('#cnpj').val(dados['empresa']['cnpj']);
                    jQuery('#enderecoEmpresa').val(dados['empresa']['endereco']);
                    jQuery('#telefoneEmpresa').val(dados['empresa']['telefone']);
                    jQuery('#representante').val(dados['empresa']['representante']);
                    jQuery('#placaVeiculo').val(dados['empresa']['placaVeiculo']);
                    jQuery('#nomeMotorista').val(dados['empresa']['nomeMotorista']);
                    jQuery('#rgMotorista').val(dados['empresa']['rgMotorista']);
                    
                    
                }

            }, 'json');
        }
    });
    

    if(jQuery('input[name="id"]').val() == ""){
        mudarEstado(5270);
    }
    
    jQuery('#estado').change(function () {
        mudarEstado(0);

    });

});

function mudarEstado(cidade) {
    if (jQuery('#estado').val() !== "") {

        //Função padrão que realiza a busca das cidades de um estado
        jQuery.post(url + '/agencia/declarante/getCidades', {estado: jQuery('#estado').val()}, function (dados) {
            jQuery('#cidade').html("");

            for (var i = 0; i < dados.length; i++) {
                if(cidade == dados[i]['id']){
                    jQuery('#cidade').append("<option value=" + dados[i]['id'] + " selected>" + dados[i]['nome'] + "</option>");
                }else{
                    jQuery('#cidade').append("<option value=" + dados[i]['id'] + ">" + dados[i]['nome'] + "</option>");
                }
            }


        }, 'json');
    } else {
        jQuery('#cidade').html("");
        jQuery('#cidade').append("<option value=''>Escolha uma Cidade</option>");
    }
}