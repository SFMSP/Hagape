jQuery(document).ready(function () {

    jQuery('.autocomplete-select').select2({
        placeholder: "Selecione um Item",
        width: 'auto',
        allowClear: true
    });

    jQuery('.btn-enviar').click(function () {
        jQuery('#acao').val((jQuery(this).val()));
        jQuery('#descarte').submit();
    });


    jQuery('#produto').change(function () {
        atualizarQuantidadeValor();

    });

    jQuery('#quantidade').change(function () {
        atualizarQuantidadeValor();
    });

    jQuery('.autocomplete-select').change(function () {
        popularMedida(jQuery('#produto').val());

    });

});

/**
 * Popula medida
 *
 * @param {integer} id_produto
 */
function popularMedida(id_produto) {
    jQuery.post(url + '/estoque/descarte/getProdutos', {produto: jQuery('#produto').val()}, function (medidas) {
        jQuery('#medida').val(medidas.medida);
    }, 'json');
}


/**
 * Atualiza a quantidade e o valor
 */
function atualizarQuantidadeValor() {
    jQuery.post(url + '/estoque/descarte/getQuantidadeDisponivel', {produto: jQuery('#produto').val(), solicitacaoDescarte: jQuery('#id').val()}, function (data) {
        jQuery('#quantidadeDisponivel').val(data.quantidade);

        if (jQuery('#quantidade').val() !== "") {

            jQuery.post(url + '/estoque/descarte/getDadosProduto', {produto: jQuery('#produto').val()}, function (produto) {
                var valor = jQuery('#quantidade').val() * produto.precoMedioAtual;
                jQuery('#valorTotal').val(formatMoney(valor, 2, ',', '.'));

            }, 'json');
        }
    }, 'json');
}

/**
 * Função que adiciona um produto na lista de itens do pedido
 *
 * @returns {Boolean}
 */
function adicionarProduto() {

    //Valida se um produto foi selecionado
    if (jQuery('#produto').val() == "" || jQuery('#produto').val() == null) {
        alert('Selecione um Produto');
        return false;
    }

    //Valida se a quantidade foi informada
    if (jQuery('#quantidade').val() == "") {
        alert('Informe a Quantidade');
        return false;
    }


   var quantidade = new Number(jQuery('#quantidade').val());
   var quantidadeDisponivel = new Number(jQuery('#quantidadeDisponivel').val());

    if (quantidade > quantidadeDisponivel) {
        alert('A quantidade informada é maior que a quantidade disponível');
        return false;
    }

    //Busca os dados do produto
    jQuery.post(url + '/estoque/descarte/getDadosProduto', {produto: jQuery('#produto').val()}, function (data) {
        if (data) {

            //Calcula o valor total
            var valorTotal = data.precoMedioAtual * jQuery('#quantidade').val();

            //Deleta a linha que indica que nenhum produto foi selecionado caso ela exista
            if (jQuery('#nenhum-produto-selecionado')) {
                jQuery('#nenhum-produto-selecionado').remove();
            }

            //Cria a variável que irá armazenar o html da tabela
            var html = "";
            html += "<tr>";
            html += "<td>";
            html += "<input type='hidden' name='id-item-descarte[]' value='0' />";
            html += "<input type='hidden' name='id-produto[]' value='" + jQuery('#produto').val() + "' />";
            html += "<input type='hidden' name='codigo-produto[]' value='" + data.codigo + "' />";
            html += "<input type='hidden' name='nome-produto[]' value='" + data.nome + "' />";
            html += "<input type='hidden' name='preco-medio[]' value='" + data.precoMedioAtual + "' />";
            html += "<input type='hidden' name='qtd-produto[]' value='" + jQuery('#quantidade').val() + "' />";
            html += "<input type='hidden' name='valor-total[]' value='" + valorTotal + "' />";
            html += data.codigo;
            html += "</td>";
            html += "<td>" + data.nome + "</td>";
            html += "<td>R$ " + formatMoney(data.precoMedioAtual, 2, ',', '.') + "</td>";
            html += "<td>" + jQuery('#quantidade').val() + "</td>";
            html += "<td>R$ " + formatMoney(valorTotal, 2, ',', '.') + "</td>";
            html += "<td><a onclick='excluirItemSolicitacao(jQuery(this));' class='glyphicon glyphicon-trash btn-excluir-tabela'></a></td>";
            html += "</tr>";

            //Adiciona a nova linha a tabela e remove o produto das opções
            jQuery('#tabela-produtos tbody').append(html);
            jQuery('#produto option[value="' + jQuery('#produto').val() + '"]').remove();

            //Atualizacao do footer total de produtos
            var valorTotalProdutos = jQuery("#hiddenValorTotalProdutos").val();
            valorTotalProdutos = Number(valorTotalProdutos) + Number(valorTotal);
            jQuery("#hiddenValorTotalProdutos").val(valorTotalProdutos);
            jQuery("#totalProdutos").html(formatMoney(valorTotalProdutos,2,',','.'));

            jQuery('.autocomplete-select').select2({
                language: {
                    noResults: function () {
                        return "Nenhum Resultado Encontrado";
                    }
                },
                placeholder: "Selecione um Item",
                width: 'auto',
                allowClear: true
            });

            jQuery("#quantidadeDisponivel").val("");
            jQuery("#quantidade").val("");
            jQuery("#valorTotal").val("");
            jQuery("#medida").val("");
        } else {
            alert('Produto não Encontrado');
        }
    }, 'json');

}

/**
 * Função que exclui um produto da lista de itens do pedido 
 * 
 * @param {object} linha
 */
function excluirItemSolicitacao(linha) {

    //Armazena o id do item do pedido
    var idItemDescarte = linha.parent().parent().find("input[name='id-item-descarte[]']").val();

    //Armazena os dados do produto para recompor o select
    var idProduto = linha.parent().parent().find("input[name='id-produto[]']").val();
    var codigoProduto = linha.parent().parent().find("input[name='codigo-produto[]']").val();
    var nomeProduto = linha.parent().parent().find("input[name='nome-produto[]']").val();

    //Armazena o valor total do item
    var valorTotal = new Number(linha.parent().parent().find("input[name='total-valor-produto[]']").val());

    //Readiciona o produto ao select de produtos e o reordena
    jQuery('#produto').append('<option value="' + idProduto + '">' + codigoProduto + ' - ' + nomeProduto + '</option>');
    ordenarSelects('produto');
    jQuery('#produto').val('');

    //Deleta a linha da tabela
    linha.parent().parent().remove();

    //Verifica se existe algum produto
    if (!jQuery('#tabela-produtos').find('tbody tr').length) {

        //Adiciona a linha indicando que não há nenhum produto selecionado
        jQuery('#tabela-produtos').append('<tr id="nenhum-produto-selecionado"><td colspan="6"><i>Nenhum Produto Selecionado</i></td></tr>');

    }

    //Caso o item da transferência já esteja cadastrado no banco de dados, ele é adicionado a lista de itens a serem excluídos
    if (idItemDescarte != 0) {
        jQuery('#idItensExcluidos').val(jQuery('#idItensExcluidos').val() + idItemDescarte + ",");
    }

}
